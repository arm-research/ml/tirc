<div align="center">
  <img src="doc/logo/logo.jpg" alt="" style="height: 100px; width:200px;"/>
</div>
<p>
<div style="text-align: justify">
tirc (TOSA IR Compiler) is a compiler built on top of Multi-Level Intermediate Representation (MLIR) targeting Arm® Ethos™-u55 NPU.
<p><p>
Leveraging MLIR a series of IR dialects, transformation passes, custom types and attributes are implemented that consume the Tensor operator set architecture (TOSA) dialect and perform scheduling, memory allocation and code generation producing an executable command stream for the Arm® Ethos™-u55 neural processor.
<p><p>
tirc is a research project, aimed at exploring the capabilities of MLIR. It is currently being released without any support or functional guarantee. It should be treated as exploratory research code.
</div>

# Build Instructions
#### **Operating System**

-   Ubuntu 18.04.3 LTS
-   Shell: bash

#### **Python environment**

-   python 3.6.9
-   python tensorflow = 2.6.0

#### **Working Directory**

-   mkdir /home/$USER/tirc_compiler
-   build_path="/home/$USER/tirc_compiler"

#### **Build LLVM & MLIR**

-   cd $build_path
-   git clone https://github.com/llvm/llvm-project
-   cd llvm-project
-   git checkout b9cfa016daae725e4ed8173b431afb5e01cb80a6
-   mkdir build
-   cd build
-   cmake -G Ninja ../llvm -DLLVM_ENABLE_PROJECTS=mlir -DLLVM_BUILD_EXAMPLES=ON -DLLVM_TARGETS_TO_BUILD="X86;NVPTX;AMDGPU" -DCMAKE_BUILD_TYPE=Release -DLLVM_REQUIRES_RTTI=ON -DLLVM_REQUIRES_EH=ON -DLLVM_ENABLE_RTTI=ON -DLLVM_ENABLE_EH=ON -DLLVM_ENABLE_ASSERTIONS=0 -DLLVM_ABI_BREAKING_CHECKS=FORCE_OFF
-   cmake --build . --target check-mlir

#### **Build Tensorflow**

-   cd $build_path
-   git clone https://github.com/tensorflow/tensorflow.git
-   cd tensorflow
-   git checkout 53b185e731bd92d762bc6bd0cac0a57402bbc54a
-   touch $build_path/llvm-project/WORKSPACE $build_path/llvm-project/BUILD
-   bazel build --config=v2 tensorflow/compiler/mlir/... -j 64
-   bazel build --config=v2 tensorflow/core/util/... -j 64


#### **Build tirc:**

-   cd $build_path
-   git clone git@gitlab.com:arm-research/ml/tirc.git
-   cd tirc
-   git submodule init
-   git submodule update
-   python scripts/setup_submodules.py
-   cd third_party
    ln -s $build_path/llvm-project/ llvm-project
    ln -s $build_path/tensorflow/ tensorflow
    cd ..
-   mkdir build
-   cd build
-   cmake .. -G Ninja
-   ninja -j 16

# Supported Networks

We have tested and successfully compiled and executed a variety of model networks including and not limited to:

Network       | Data Type
------------- |-------------  
MobileNet v1  | 8-bit
MobileNet v2  | 8-bit
Inception v3  | 8-bit
Resnet v50    | 8-bit

We currently support as input mlir files with a single main function containing tflite dialect ops.

# Running Networks

build/bin/tirc --input_filename model.mlir

# Contact

tirc@arm.com.

# Documentation
<a href="https://gitlab.com/arm-research/ml/tirc/-/blob/main/doc/TIRC%20-%20A%20MLIR%20based%20Neural%20Processor%20Compiler.pdf">tirc: A MLIR based Neural Processor Compiler</a>

# References
[1] https://developer.mlplatform.org/w/tosa/ \
[2] https://mlir.llvm.org/docs/Dialects/TOSA/ \
[3] https://mlir.llvm.org/ \
[4] https://www.arm.com/products/silicon-ip-cpu/ethos/ethos-u55

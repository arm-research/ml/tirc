#  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.
#
#  SPDX-License-Identifier: Apache-2.0
#
#  Licensed under the Apache License, Version 2.0 (the License); you may
#  not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#  www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an AS IS BASIS, WITHOUT
#  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

# Declare a TIRC dialect.
function(add_tirc_dialect_library name)
	set_property(GLOBAL APPEND PROPERTY TIRC_DIALECT_LIBS ${name})
  add_mlir_library(${ARGV} DEPENDS mlir-headers)
endfunction(add_tirc_dialect_library)

# Declare a TIRC legalization library
function(add_tirc_conversion_library name)
	set_property(GLOBAL APPEND PROPERTY TIRC_CONVERSION_LIBS ${name})
  add_mlir_library(${ARGV} DEPENDS mlir-headers)
endfunction(add_tirc_conversion_library)

# Declare a general TIRC library, neither pass nor dialect
function(add_tirc_general_library name)
	set_property(GLOBAL APPEND PROPERTY TIRC_GENERAL_LIBS ${name})
  add_mlir_library(${ARGV} DEPENDS mlir-headers)
endfunction(add_tirc_general_library)

# Declare a external library (e.g. compiled from Bazel tree)
function(add_ext_dialect_library name)
	set_property(GLOBAL APPEND PROPERTY TIRC_EXT_LIBS ${name})
  add_mlir_library(${ARGV} DEPENDS mlir-headers)
endfunction(add_ext_dialect_library)

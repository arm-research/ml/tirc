#!/usr/bin/python3

"""
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
"""

import os
import subprocess


def run_boost_setup():
    # Boost is not configured as a submodule because it has too many
    # submodules of its own.  Clone the first level of boost submodules
    # here, but no more.  Then run the bootstrap/compile.
    subprocess.Popen(
        [
            "curl",
            "-L",
            "https://boostorg.jfrog.io/artifactory/main/release/1.66.0/source/boost_1_66_0.tar.bz2",  # noqa: E501
            "-o",
            "boost.tar.bz2",
        ],
        cwd="third_party",
    ).communicate()
    subprocess.Popen(["tar", "-jxf", "boost.tar.bz2"], cwd="third_party").communicate()
    subprocess.Popen(["mv", "boost_1_66_0", "boost"], cwd="third_party").communicate()
    subprocess.Popen(
        [
            "./bootstrap.sh",
            "--prefix={}".format(os.path.join(os.getcwd(), "third_party", "boost")),
        ],
        shell=False,
        cwd="third_party/boost",
    ).communicate()
    subprocess.Popen(
        ["./b2", "install", "-j", "16"], shell=False, cwd="third_party/boost"
    ).communicate()


def main():
    run_boost_setup()


if __name__ == "__main__":
    main()

#  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.
#
#  SPDX-License-Identifier: Apache-2.0
#
#  Licensed under the Apache License, Version 2.0 (the License); you may
#  not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#  www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an AS IS BASIS, WITHOUT
#  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

add_tirc_general_library(compiler_interface
  compiler_cl.cc

  DEPENDS
  tirc_utils
  llcs_passes
  hlcs_lower

  LINK_LIBS PUBLIC
  LLVMSupport
  LLVMCore
  MLIRAnalysis
  MLIRDialect
  MLIRIR
  MLIRPass
  MLIRSupport
  MLIRTosa
)


get_property(dialect_libs GLOBAL PROPERTY MLIR_DIALECT_LIBS)
# We don't use any built in MLIR conversion libs at the moment
# get_property(conversion_libs GLOBAL PROPERTY MLIR_CONVERSION_LIBS)
get_property(tirc_dialects GLOBAL PROPERTY TIRC_DIALECT_LIBS)
get_property(tirc_conversions GLOBAL PROPERTY TIRC_CONVERSION_LIBS)
get_property(tirc_general GLOBAL PROPERTY TIRC_GENERAL_LIBS)
get_property(tirc_external GLOBAL PROPERTY TIRC_EXT_LIBS)
message(dialect_libs=${dialect_libs})
message(tirc_dialects=${tirc_dialects})
message(tirc_conversions=${tirc_conversions})
message(tirc_general=${tirc_general})
message(tirc_external=${tirc_external})
set(TIRC_LIBS
  ${tirc_dialects}
  ${tirc_conversions}
  ${tirc_general}
  ${tirc_external}
  )
set(MLIR_LIBS
  LLVMSupport
  LLVMCore
  MLIRAnalysis
  MLIRDialect
  MLIRIR
  MLIRPass
  MLIRSupport
  MLIROptLib
  MLIRDerivedAttributeOpInterface
  MLIRViewLikeInterface
  ${dialect_libs}
  )
set(TFMLIR_LIBS
  op_or_arg_name_mapper
  error_util
  name_utils
  status_macros
  serialize_mlir_module_utils
  )
set(TFLITE_LIBS
  tensorflow_lite
  tensorflow_lite_quantize
  tfl_to_std
  quantization_lib
  tensor_utils
  portable_tensor_utils
  neon_tensor_utils
  quantization_util
  quantization_utils
  error_reporter
  model_utils
  model_builder
  operator_property
  schema_utils
  arena_planner
  simple_memory_arena
  common
  string_util
  allocation
  util
  external_cpu_backend_context
  api
  stderr_reporter
  op_resolver
  minimal_logging
  schema_conversion_utils
  platform_profiler
  mutable_op_resolver
  )
set(TF_LIBS
  tensorflow
  tensorflow_ops
  tensorflow_ops_a_m
  tensorflow_ops_n_z
  tensorflow_remaining_ops
  tensorflow_tfrt_ops
  tensorflow_op_interfaces
  tensorflow_structs
  Dialect
  )
set(TFCORE_LIBS
  tensorflow_framework
  )
get_property(tosa_conversions GLOBAL PROPERTY TOSA_CONVERSION_LIBS)


add_executable(tirc
  tirc.cc
  execute.cc
  read_mlir.cc
)

llvm_update_compile_flags(tirc)
target_link_libraries(tirc npu_tfl_passes)
target_link_libraries(tirc tfl_passes)
target_link_libraries(tirc tf_passes)
target_link_libraries(tirc legalize_common)
target_link_libraries(tirc ${TOSA_CONVERSION_LIBS})
target_link_libraries(tirc ${TFLITE_LIBS})
target_link_libraries(tirc ${TF_LIBS})
target_link_libraries(tirc ${TFMLIR_LIBS})
target_link_libraries(tirc ${MLIR_LIBS})
target_link_libraries(tirc ${TFCORE_LIBS})
target_link_libraries(tirc ${TIRC_LIBS})
target_link_libraries(tirc jsoncpp)

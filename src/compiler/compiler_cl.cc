/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#include "src/compiler/compiler_cl.h"

llvm::cl::OptionCategory OptionalCompilerCategory(
    "Optional Compiler Options",
    "Optioanl Options for controlling the TIRC compiler.");
llvm::cl::OptionCategory MandatoryCompilerCategory(
    "Mandatory Compiler Options",
    "Mandatory Options for controlling the TIRC compiler.");

llvm::cl::opt<std::string> input_filename(
    "input_filename", llvm::cl::desc("input tflite mlir file"),
    llvm::cl::init("test/test"), llvm::cl::value_desc("input_filename"),
    llvm::cl::cat(MandatoryCompilerCategory), llvm::cl::Required);

llvm::cl::opt<int32_t>
    ir_visualization("ir_visualization", llvm::cl::desc("ir_visualization"),
                     llvm::cl::init(-1),
                     llvm::cl::value_desc("ir_visualization"),
                     llvm::cl::cat(OptionalCompilerCategory));

llvm::cl::opt<int32_t> sram_size("sram_size", llvm::cl::desc("sram_size"),
                                 llvm::cl::init(1048576),
                                 llvm::cl::value_desc("sram_size"),
                                 llvm::cl::cat(OptionalCompilerCategory));

llvm::cl::opt<std::string> compiler_stages(
    "compiler_stages",
    llvm::cl::desc("Compiler_stages selected in the pipeline. If not selected "
                   "the full tirc pipeline is executed. If set to -1 all "
                   "passes are run in synchronous mode"),
    llvm::cl::init(""), llvm::cl::value_desc("compiler_stages"),
    llvm::cl::cat(OptionalCompilerCategory));

llvm::cl::opt<bool> generate_hlcs_mlir_output(
    "generate_hlcs_mlir_output", llvm::cl::desc("dump hlcs mlir output"),
    llvm::cl::init(false), llvm::cl::cat(OptionalCompilerCategory));

llvm::cl::opt<bool> generate_mlir_output(
    "generate_mlir_output", llvm::cl::desc("dump mlir output"),
    llvm::cl::init(false), llvm::cl::cat(OptionalCompilerCategory));

llvm::cl::opt<bool> generate_register_stream(
    "generate_register_stream", llvm::cl::desc("dump register command stream"),
    llvm::cl::init(false), llvm::cl::cat(OptionalCompilerCategory));

llvm::cl::opt<bool> generate_json_stat_output(
    "generate_json_stat_output", llvm::cl::desc("dump json statistcis output"),
    llvm::cl::init(false), llvm::cl::cat(OptionalCompilerCategory));

llvm::cl::opt<bool> generate_network_performance(
    "generate_network_performance", llvm::cl::desc("dump network performance"),
    llvm::cl::init(false), llvm::cl::cat(OptionalCompilerCategory));

llvm::cl::opt<bool> generate_compilation_timing_numbers(
    "generate_compilation_timing_numbers",
    llvm::cl::desc("dump compilation timing"), llvm::cl::init(false),
    llvm::cl::cat(OptionalCompilerCategory));

llvm::cl::opt<std::string> tflite_legalization(
    "tflite_legalization", llvm::cl::desc("<tflite_legalization>"),
    llvm::cl::init(""), llvm::cl::value_desc("tflite_legalizations"),
    llvm::cl::cat(OptionalCompilerCategory));

llvm::cl::opt<std::string> operation_hw_mapping_filename(
    "operation_hw_mapping_filename",
    llvm::cl::desc("<operation_hw_mapping_filename>"), llvm::cl::init(""),
    llvm::cl::value_desc("operation_hw_mapping_filename"),
    llvm::cl::cat(OptionalCompilerCategory));

llvm::cl::opt<bool> disable_cascading("disable_cascading",
                                      llvm::cl::desc("disable_cascading"),
                                      llvm::cl::init(false),
                                      llvm::cl::cat(OptionalCompilerCategory));

llvm::cl::opt<bool> disable_weight_streaming(
    "disable_weight_streaming", llvm::cl::desc("disable_weight_streaming"),
    llvm::cl::init(false), llvm::cl::cat(OptionalCompilerCategory));

llvm::cl::opt<bool> disable_stationary("disable_stationary",
                                       llvm::cl::desc("disable_stationary"),
                                       llvm::cl::init(false),
                                       llvm::cl::cat(OptionalCompilerCategory));

llvm::cl::opt<bool>
    disable_block_config_search("disable_block_config_search",
                                llvm::cl::desc("disable_block_config_search"),
                                llvm::cl::init(false),
                                llvm::cl::cat(OptionalCompilerCategory));

llvm::cl::opt<std::string>
    custom_block("custom_block", llvm::cl::desc("custom_block"),
                 llvm::cl::init("128,128,128,128"),
                 llvm::cl::value_desc("custom_block"),
                 llvm::cl::cat(OptionalCompilerCategory));

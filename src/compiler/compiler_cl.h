/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#ifndef COMPILER_CL_H
#define COMPILER_CL_H

#include "llvm/Support/CommandLine.h"

using llvm::cl::opt;

extern llvm::cl::opt<std::string> input_filename;
extern llvm::cl::opt<int32_t> ir_visualization;
extern llvm::cl::opt<int32_t> sram_size;
extern llvm::cl::opt<std::string> compiler_stages;
extern llvm::cl::opt<bool> generate_hlcs_mlir_output;
extern llvm::cl::opt<bool> generate_mlir_output;
extern llvm::cl::opt<bool> generate_register_stream;
extern llvm::cl::opt<bool> generate_json_stat_output;
extern llvm::cl::opt<bool> generate_network_performance;
extern llvm::cl::opt<bool> generate_compilation_timing_numbers;
extern llvm::cl::opt<std::string> tflite_legalization;
extern llvm::cl::opt<std::string> operation_hw_mapping_filename;
extern llvm::cl::opt<bool> disable_cascading;
extern llvm::cl::opt<bool> disable_weight_streaming;
extern llvm::cl::opt<bool> disable_stationary;
extern llvm::cl::opt<bool> disable_block_config_search;
extern llvm::cl::opt<std::string> custom_block;

extern llvm::cl::OptionCategory OptionalCompilerCategory;
extern llvm::cl::OptionCategory MandatoryCompilerCategory;

#endif // COMPILER_CL_H

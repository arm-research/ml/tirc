/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#include "src/compiler/execute.h"

void compiler_stages_enabled_check_in_order(
    const std::vector<int> &passes_enabled) {
  ASSERT_COND(passes_enabled.size() == 0, "No Passes Selected");
  unsigned expected_next = passes_enabled[0] + 1;
  for (auto p = passes_enabled.begin() + 1; p != passes_enabled.end(); p++) {
    if (*p == expected_next)
      expected_next++;
    else
      ASSERT_COND(
          true,
          "The passes selected are not consecutive. This will not work !!!");
  }
}

std::string stage_to_string(compiler_stage s) {
  switch (s) {
  case legalize_to_tosa: {
    return std::string("legalize_to_tosa");
    break;
  }
  case optimization: {
    return std::string("optimization");
    break;
  }
  case legalize_to_scheduleir: {
    return std::string("legalize_to_scheduleir");
    break;
  }
  case ir_format: {
    return std::string("ir_format");
    break;
  }
  case weight_compression: {
    return std::string("weight_compression");
    break;
  }
  case kernel_calculations: {
    return std::string("kernel_calculations");
    break;
  }
  case scheduler: {
    return std::string("scheduler");
    break;
  }
  case weight_packing: {
    return std::string("weight_packing");
    break;
  }
  case memory_organizer: {
    return std::string("memory_organizer");
    break;
  }
  case hlcs_generation: {
    return std::string("hlcs_generation");
    break;
  }
  case llcs_generation: {
    return std::string("llcs_generation");
    break;
  }
  case tflite_generation: {
    return std::string("tflite_generation");
    break;
  }
  }
}

void Execute_Pass(
    compiler_stage s, mlir::MLIRContext *context, mlir::OwningModuleRef &module,
    mlir::ArchitectureConfig::ArchitectureConfigAttr &arch_config,
    mlir::CompilerConfig::CompilerConfigAttr &compiler_config,
    std::unordered_map<std::string, mlir::utils::Legalization>
        legalization_maplist,
    std::unordered_map<std::string, mlir::utils::MappingStructure> hw_maplist,
    bool verify) {
  mlir::PassManager pm(context, OpPassManager::Nesting::Implicit);
  pm.enableVerifier(verify);

  switch (s) {
  case legalize_to_tosa: {
    std::cout << "0: Legalization to TOSA" << std::endl;
    pm.addPass(mlir::createInlinerPass());
    // Now that there is only one function, run some MLIR passes on it.
    pm.addPass(mlir::createCanonicalizerPass());
    pm.addPass(mlir::createCSEPass());
    pm.addPass(mlir::createLoopFusionPass());

    // Removed from MLIR
    // pm.addPass(mlir::createMemRefDataFlowOptPass());
    //----------------------------------------------------------------------------
    // Perform main conversion (Forked from OpenSource)
    //----------------------------------------------------------------------------
    pm.addPass(mlir::tosa::createConvertTFLUint8Pass(legalization_maplist));
    pm.addPass(mlir::tosa::createLegalizeTosaNPUTFLPass(legalization_maplist));
    pm.addPass(mlir::tosa::createTosaMakeBroadcastablePass());
    //----------------------------------------------------------------------------
    // Post conversion cleanup.
    //----------------------------------------------------------------------------
    // Inline the call/return basic blocks within TOSA control flow ops.
    pm.addPass(mlir::createInlinerPass());
    // Clean up with DCE.
    pm.addPass(mlir::createSymbolDCEPass());
    break;
  }
  case optimization: {
    std::cout << "1: TOSA Graph Optimization" << std::endl;
    pm.addPass(mlir::tosa::CreateConstantTransposeFoldingTosaPass());
    pm.addPass(mlir::tosa::CreateConcatFoldingTosaPass());
    break;
  }
  case legalize_to_scheduleir: {
    std::cout << "2: Legalization to ScheduleIR & Partitioning" << std::endl;
    pm.addPass(
        mlir::planner::CreateLegalizeScheduleIR(arch_config, compiler_config));
    break;
  }
  case ir_format: {
    std::cout << "3: IRFormat" << std::endl;
    pm.addPass(mlir::planner::CreatePlannerIRFormat());
    break;
  }
  case weight_compression: {
    std::cout << "4: WeightCompression" << std::endl;
    pm.addPass(mlir::planner::CreatePlannerWeightCompression());
    break;
  }
  case kernel_calculations: {
    std::cout << "5: KernelCalculations" << std::endl;
    pm.addPass(mlir::planner::CreatePlannerKernelPacking(hw_maplist));
    pm.addPass(mlir::planner::CreatePlannerIRSort());
    pm.addPass(mlir::planner::CreatePlannerKernelRescaling());
    pm.addPass(mlir::planner::CreatePlannerKernelTransformation());
    break;
  }
  case scheduler: {
    std::cout << "6: Scheduler" << std::endl;
    pm.addPass(mlir::planner::CreatePlannerScheduler());
    pm.addPass(mlir::planner::CreatePlannerIRSort());
    break;
  }
  case weight_packing: {
    std::cout << "7: WeightPacking" << std::endl;
    pm.addPass(mlir::planner::CreatePlannerWeightPacking());
    break;
  }
  case memory_organizer: {
    std::cout << "8: MemoryOrganizer" << std::endl;
    pm.addPass(mlir::planner::CreatePlannerMemoryOrganizer());
    break;
  }
  case hlcs_generation: {
    std::cout << "9: HLCSGeneration" << std::endl;
    pm.addPass(mlir::planner::CreatePlannerHLCSGenerator());
    break;
  }
  case llcs_generation: {
    std::cout << "10: LLCS Generation" << std::endl;
    pm.addPass(mlir::hlcs::CreateHLCSOptimisationPass());
    pm.addPass(mlir::hlcs::CreateHLCSConvToNpuOpPass());
    pm.addPass(mlir::hlcs::CreateHLCSCalcDependencyPass());
    pm.addPass(mlir::hlcs::CreateHLCSNpuRegionLowerPass());
    pm.addPass(mlir::hlcs::CreateHLCSGenerateLLCSPass());
    break;
  }
  case tflite_generation: {
    std::cout << "11: TFLITE Generation" << std::endl;
    pm.addPass(mlir::binary_writer::CreateRuntimeBinaryGenerator());
    break;
  }
  default:
    ASSERT_COND(true, "Illegal Pass Selected");
  }

  if (mlir::failed(pm.run(*module))) {
    std::cout << "Error in executing the Passes " << std::endl;
    FATAL_ERROR("Compiler Stage Failed !!! ");
  }
}

void Execute_Pipeline(
    mlir::MLIRContext *context, mlir::OwningModuleRef &module,
    mlir::ArchitectureConfig::ArchitectureConfigAttr &arch_config,
    mlir::CompilerConfig::CompilerConfigAttr &compiler_config,
    std::unordered_map<std::string, mlir::utils::Legalization>
        legalization_maplist,
    std::unordered_map<std::string, mlir::utils::MappingStructure> hw_maplist,
    bool verify) {
  std::cout << "Executing TIRC Pipeline" << std::endl;

  mlir::PassManager pm(context, OpPassManager::Nesting::Implicit);
  pm.enableVerifier(verify);

  pm.addPass(mlir::createInlinerPass());
  pm.addPass(mlir::createCanonicalizerPass());
  pm.addPass(mlir::createCSEPass());
  pm.addPass(mlir::createLoopFusionPass());
  // Removed from MLIR
  // pm.addPass(mlir::createMemRefDataFlowOptPass());

  pm.addPass(mlir::tosa::createConvertTFLUint8Pass(legalization_maplist));
  pm.addPass(mlir::tosa::createLegalizeTosaNPUTFLPass(legalization_maplist));

  pm.addPass(mlir::tosa::createTosaMakeBroadcastablePass());
  pm.addPass(mlir::createInlinerPass());
  pm.addPass(mlir::createSymbolDCEPass());
  pm.addPass(mlir::tosa::CreateConstantTransposeFoldingTosaPass());
  pm.addPass(mlir::tosa::CreateConcatFoldingTosaPass());

  pm.addPass(
      mlir::planner::CreateLegalizeScheduleIR(arch_config, compiler_config));
  pm.addPass(mlir::planner::CreatePlannerIRFormat());
  pm.addPass(mlir::planner::CreatePlannerWeightCompression());
  pm.addPass(mlir::planner::CreatePlannerKernelPacking(hw_maplist));
  pm.addPass(mlir::planner::CreatePlannerIRSort());
  pm.addPass(mlir::planner::CreatePlannerKernelRescaling());
  pm.addPass(mlir::planner::CreatePlannerKernelTransformation());
  pm.addPass(mlir::planner::CreatePlannerScheduler());
  pm.addPass(mlir::planner::CreatePlannerIRSort());
  pm.addPass(mlir::planner::CreatePlannerWeightPacking());
  pm.addPass(mlir::planner::CreatePlannerMemoryOrganizer());
  pm.addPass(mlir::planner::CreatePlannerHLCSGenerator());

  pm.addPass(mlir::hlcs::CreateHLCSOptimisationPass());
  pm.addPass(mlir::hlcs::CreateHLCSConvToNpuOpPass());
  pm.addPass(mlir::hlcs::CreateHLCSCalcDependencyPass());
  pm.addPass(mlir::hlcs::CreateHLCSNpuRegionLowerPass());
  pm.addPass(mlir::hlcs::CreateHLCSGenerateLLCSPass());

  pm.addPass(mlir::binary_writer::CreateRuntimeBinaryGenerator());

  if (mlir::failed(pm.run(*module))) {
    std::cout << "Error in executing the Passes " << std::endl;
    FATAL_ERROR("Compiler Stage Failed !!! ");
  }
}

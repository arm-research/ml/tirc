/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#ifndef EXECUTE_H
#define EXECUTE_H

#include <unordered_map>

#include "mlir/Dialect/Shape/IR/Shape.h" // from @llvm-project
#include "mlir/Dialect/Tosa/IR/TosaOps.h"
#include "mlir/Dialect/Tosa/Transforms/Passes.h" // from @llvm-project
#include "mlir/ExecutionEngine/ExecutionEngine.h"
#include "mlir/ExecutionEngine/OptUtils.h"
#include "mlir/IR/Attributes.h" // TF:local_config_mlir
#include "mlir/IR/Dialect.h"
#include "mlir/IR/MLIRContext.h"
#include "mlir/InitAllDialects.h" // from @llvm-project
#include "mlir/InitAllPasses.h"   // from @llvm-project
#include "mlir/Parser.h"
#include "mlir/Pass/Pass.h"
#include "mlir/Pass/PassManager.h"
#include "mlir/Support/FileUtilities.h" // TF:local_config_mlir
#include "mlir/Support/ToolUtilities.h" // TF:local_config_mlir
#include "mlir/Transforms/Passes.h"

#include "llvm/ADT/StringRef.h"
#include "llvm/IR/Module.h"
#include "llvm/Support/CommandLine.h"
#include "llvm/Support/ErrorOr.h"
#include "llvm/Support/InitLLVM.h"
#include "llvm/Support/MemoryBuffer.h"
#include "llvm/Support/Regex.h" // TF:local_config_mlir
#include "llvm/Support/SourceMgr.h"
#include "llvm/Support/TargetSelect.h"
#include "llvm/Support/raw_ostream.h"

#include "src/ir/architecture_config/architecture_config.h"
#include "src/ir/compiler_config/compiler_config.h"

#include "src/transforms/passes.h"
#include "tensorflow/compiler/mlir/tosa/transforms/passes.h"

#include <string>

using namespace mlir;
using namespace mlir::utils;

enum compiler_stage {
  legalize_to_tosa,
  optimization,
  legalize_to_scheduleir,
  ir_format,
  weight_compression,
  kernel_calculations,
  scheduler,
  weight_packing,
  memory_organizer,
  hlcs_generation,
  llcs_generation,
  tflite_generation,
  compiler_stages_size
};
std::string stage_to_string(compiler_stage s);

void compiler_stages_enabled_check_in_order(
    const std::vector<int> &passes_enabled);

void Execute_Pass(compiler_stage s, mlir::MLIRContext *context,
                  mlir::OwningModuleRef &module,
                  mlir::ArchitectureConfig::ArchitectureConfigAttr &arch_config,
                  mlir::CompilerConfig::CompilerConfigAttr &compiler_config,
                  std::unordered_map<std::string, mlir::utils::Legalization>
                      legalization_maplist,
                  std::unordered_map<std::string, mlir::utils::MappingStructure>
                      hw_maplist = {},
                  bool verify = true);

void Execute_Pipeline(
    mlir::MLIRContext *context, mlir::OwningModuleRef &module,
    mlir::ArchitectureConfig::ArchitectureConfigAttr &arch_config,
    mlir::CompilerConfig::CompilerConfigAttr &compiler_config,
    std::unordered_map<std::string, mlir::utils::Legalization>
        legalization_maplist,
    std::unordered_map<std::string, mlir::utils::MappingStructure> hw_maplist =
        {},
    bool verify = true);

#endif // EXECUTE_H

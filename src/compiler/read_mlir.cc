/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#include "src/compiler/read_mlir.h"

int readMLIRFile(mlir::MLIRContext &context, mlir::OwningModuleRef &module,
                 const std::string &input_filename) {
  std::string ierror_message;
  auto file = mlir::openInputFile(input_filename, &ierror_message);
  if (!file) {
    llvm::errs() << ierror_message;
    return 1;
  }

  bool wasThreadingEnabled = context.isMultithreadingEnabled();
  context.disableMultithreading();

  // Parse the input mlir.
  llvm::SourceMgr sourceMgr;
  sourceMgr.AddNewSourceBuffer(std::move(file), llvm::SMLoc());
  module = mlir::parseSourceFile(sourceMgr, &context);
  context.enableMultithreading(wasThreadingEnabled);

  if (!module) {
    llvm::errs() << "Error can't load MLIR file " << input_filename << "\n";
    return 1;
  }
  return 0;
}

std::unordered_map<std::string, mlir::utils::Legalization>
read_legalization_maplist(std::string file_name) {
  std::unordered_map<std::string, mlir::utils::Legalization>
      legalization_maplist;

  io::CSVReader<2> in(file_name.c_str());
  in.read_header(io::ignore_extra_column, "op", "legalization");
  std::string op;
  std::string legalization;
  while (in.read_row(op, legalization)) {
    mlir::utils::Legalization ls;

    if (legalization == "TosaCommon")
      ls = mlir::utils::Legalization::TosaCommon;
    else if (legalization == "ScheduleIRCommon")
      ls = mlir::utils::Legalization::ScheduleIRCommon;
    else if (legalization == "Custom")
      ls = mlir::utils::Legalization::Custom;
    else if (legalization == "TFLite")
      ls = mlir::utils::Legalization::TFLite;
    else {
      std::cout << "legalization_maplist::unknown legalization option: "
                << legalization << std::endl;
      exit(1);
    }

    legalization_maplist[op] = ls;
  }

  return legalization_maplist;
}

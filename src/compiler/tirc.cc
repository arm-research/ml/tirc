/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#include <algorithm>
#include <chrono>
#include <fstream>
#include <iostream>
#include <unordered_map>

#include "src/transforms/passes.h"
#include "src/utils/json_writer.h"
#include "src/utils/printIR.h"
#include "src/utils/string_utils.h"

#include "src/ir/cascade_ir.h"
#include "src/ir/hlcs_ir.h"
#include "src/ir/kernel_ir.h"
#include "src/ir/llcs_ir.h"
#include "src/ir/schedule_ir.h"

#include "src/ir/architecture_config/architecture_config.h"
#include "src/ir/architecture_config/architecture_config_utils.h"
#include "src/ir/compiler_config/compiler_config.h"

#include "src/transforms/passes.h"
#include "src/transforms/scheduler/cost_model.h"
#include "src/transforms/scheduler/u55_cost_model/cost_model.h"

#include "src/compiler/compiler_cl.h"
#include "src/compiler/execute.h"
#include "src/compiler/read_mlir.h"

#include "mlir/Dialect/Shape/IR/Shape.h" // from @llvm-project
#include "mlir/Dialect/Tosa/IR/TosaOps.h"
#include "mlir/Dialect/Tosa/Transforms/Passes.h" // from @llvm-project
#include "mlir/ExecutionEngine/ExecutionEngine.h"
#include "mlir/ExecutionEngine/OptUtils.h"
#include "mlir/IR/Attributes.h" // TF:local_config_mlir
#include "mlir/IR/Dialect.h"
#include "mlir/IR/MLIRContext.h"
#include "mlir/InitAllDialects.h" // from @llvm-project
#include "mlir/InitAllPasses.h"   // from @llvm-project
#include "mlir/Parser.h"
#include "mlir/Pass/Pass.h"
#include "mlir/Pass/PassManager.h"
#include "mlir/Support/FileUtilities.h" // TF:local_config_mlir
#include "mlir/Support/MlirOptMain.h"
#include "mlir/Support/ToolUtilities.h" // TF:local_config_mlir
#include "mlir/Transforms/Passes.h"

#include "llvm/ADT/StringRef.h"
#include "llvm/IR/Module.h"
#include "llvm/Support/CommandLine.h"
#include "llvm/Support/ErrorOr.h"
#include "llvm/Support/InitLLVM.h"
#include "llvm/Support/MemoryBuffer.h"
#include "llvm/Support/Regex.h" // TF:local_config_mlir
#include "llvm/Support/SourceMgr.h"
#include "llvm/Support/TargetSelect.h"
#include "llvm/Support/raw_ostream.h"

#include "third_party/fast-cpp-csv-parser/csv.h"

using namespace mlir;
using namespace mlir::utils;

bool file_exists(const std::string &name) {
  ifstream f(name.c_str());
  return f.good();
}

int main(int argc, char *argv[]) {
  std::cout << "Compiler Driver Start" << std::endl << std::endl;

  registerMLIRContextCLOptions();
  registerPassManagerCLOptions();

  DialectRegistry registry;
  registerAllDialects(registry);
  registry.insert<mlir::hlcs::HLCS>();
  registry.insert<mlir::llcs::LLCS>();
  registry.insert<mlir::scheduleir::ScheduleIR>();
  registry.insert<mlir::kernelir::KernelIRDialect>();
  registry.insert<mlir::cascadeir::CascadeIRDialect>();
  registry.insert<mlir::TFL::TensorFlowLiteDialect>();

  MLIRContext context;
  OwningModuleRef module;

  context.appendDialectRegistry(registry);
  context.loadAllAvailableDialects();
  context.allowUnregisteredDialects(true);

  llvm::cl::HideUnrelatedOptions(
      {&MandatoryCompilerCategory, &OptionalCompilerCategory});
  llvm::cl::ParseCommandLineOptions(argc, argv);

  if (tflite_legalization.empty()) {
    tflite_legalization = "src/arch/tfl_legalization_map.csv";
    if (not(file_exists(tflite_legalization)))
      FATAL_ERROR(
          "TFLite Legalization file not found and default could not be set");
  }

  if (operation_hw_mapping_filename.empty()) {
    operation_hw_mapping_filename = "src/arch/operation_hw_map.csv";
    if (not(file_exists(operation_hw_mapping_filename)))
      FATAL_ERROR(
          "Operation HW mappingg file not found and default could not be set");
  }

  size_t lastindex = input_filename.find_last_of(".");
  std::string label = input_filename.substr(0, lastindex);

  bool run_async = false;
  std::vector<int> compiler_stages_enabled;
  if (compiler_stages.empty())
    run_async = true;
  else {
    if (compiler_stages == "-1") {
      for (int i = 0; i < compiler_stages_size; i++)
        compiler_stages_enabled.push_back(i);
    } else
      split_string(compiler_stages_enabled, compiler_stages, ',');
    compiler_stages_enabled_check_in_order(compiler_stages_enabled);
  }

  compiler_settings comp_config;
  comp_config.disable_cascading = disable_cascading;
  comp_config.disable_weight_streaming = disable_weight_streaming;
  comp_config.disable_stationary = disable_stationary;
  comp_config.disable_block_config_search = disable_block_config_search;
  comp_config.custom_block_struct = {0, 0, 0, 0};
  if (comp_config.disable_block_config_search) {
    std::vector<int> custom_block_config_v;
    split_string(custom_block_config_v, custom_block, ',');
    if (custom_block_config_v.size() != 4)
      FATAL_ERROR("custom block config needs to be size of 4");
    comp_config.custom_block_struct = {
        custom_block_config_v[0], custom_block_config_v[1],
        custom_block_config_v[2], custom_block_config_v[3]};
  }
  if (comp_config.disable_cascading and comp_config.disable_weight_streaming and
      comp_config.disable_stationary)
    FATAL_ERROR("Cascading or/and Single Node Weight and Stationary Streaming "
                "need to be enabled");

  std::cout
      << "##################################################################"
      << std::endl;
  std::cout << "Compiler Stages Enabled: ";
  for (int i : compiler_stages_enabled)
    std::cout << i << ",";
  std::cout << std::endl;
  std::cout << "disable_weight_streaming: "
            << comp_config.disable_weight_streaming << std::endl;
  std::cout << "disable_stationary: " << comp_config.disable_stationary
            << std::endl;
  std::cout << "disable_cascading: " << comp_config.disable_cascading
            << std::endl;
  std::cout << "disable_block_config_search: "
            << comp_config.disable_block_config_search << std::endl;

  std::cout << "IR print level: " << ir_visualization << std::endl;
  std::cout << "SRAM Size: " << (float(sram_size) / 1024 / 1024) << "MiB"
            << std::endl;
  if (sram_size != 1024 * 1024)
    std::cout << RED
              << "NOTE THAT THE SRAM SIZE IS NOT THE EXPECTED 1MiB for u55"
              << RESET << std::endl;
  std::cout
      << "##################################################################"
      << std::endl
      << std::endl;

  auto arch_config = CreateArchitectureConfigAttribute(&context, sram_size);
  auto compiler_config = CreateCompilerConfigAttribute(&context, comp_config);

  if ((readMLIRFile(context, module, input_filename)) != 0)
    return -1;

  std::unordered_map<std::string, mlir::utils::Legalization>
      legalization_maplist = read_legalization_maplist(tflite_legalization);

  // Generte OP->Hardware Unit Mapping
  std::unordered_map<std::string, mlir::utils::MappingStructure> hw_maplist;
  {
    io::CSVReader<4> in(operation_hw_mapping_filename.c_str());
    in.read_header(io::ignore_extra_column, "op", "ip_unit", "compute_unit",
                   "compute_sub_unit");
    std::string op;
    std::string ip_unit;
    std::string compute_unit;
    std::string compute_sub_unit;
    while (in.read_row(op, ip_unit, compute_unit, compute_sub_unit)) {
      mlir::utils::MappingStructure ms;
      ms.ip_unit = mlir::utils::ip_unit_string_to_enum(ip_unit);
      ms.compute_unit = mlir::utils::compute_unit_string_to_enum(compute_unit);
      ms.compute_sub_unit =
          mlir::utils::compute_sub_unit_string_to_enum(compute_sub_unit);
      hw_maplist[op] = ms;
    }
  }

  // Verify IR after each pass execution using MLIR verifier
  bool verify_passes = true;
  ASSERT_COND(not(verify_passes), "Pass Verification off !!!");

  if ((run_async and generate_network_performance) or
      (generate_network_performance and
       (std::find(compiler_stages_enabled.begin(),
                  compiler_stages_enabled.end(),
                  scheduler) == compiler_stages_enabled.end())))
    FATAL_ERROR(
        "generate_netork_perfromance can't be run in async mode and needs at "
        "least compiler_stages up to the scheduler to work !!!");

  Block *block = module.get().getBody();
  std::vector<double> compiler_stage_timing;
  if (run_async) {
    auto start = std::chrono::high_resolution_clock::now();
    Execute_Pipeline(&context, module, arch_config, compiler_config,
                     legalization_maplist, hw_maplist, verify_passes);
    auto finish = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double> elapsed = finish - start;
    compiler_stage_timing.push_back(elapsed.count());
  } else {
    for (auto s : compiler_stages_enabled) {
      auto start = std::chrono::high_resolution_clock::now();
      Execute_Pass((compiler_stage)s, &context, module, arch_config,
                   compiler_config, legalization_maplist, hw_maplist,
                   verify_passes);
      auto finish = std::chrono::high_resolution_clock::now();
      std::chrono::duration<double> elapsed = finish - start;
      compiler_stage_timing.push_back(elapsed.count());

      block->invalidateOpOrder();
      block->recomputeOpOrder();
      std::cout << std::endl;

      if (generate_hlcs_mlir_output and (s == hlcs_generation)) {
        std::string output_filename = label + std::string("_hlcs_tirc.mlir");
        std::error_code error_code;
        llvm::raw_fd_ostream output_ostream(output_filename.c_str(),
                                            error_code);
        module.get().print((llvm::raw_ostream &)output_ostream);
        output_ostream.flush();
      }

      // Dump Json Visualization Statistics
      if (generate_json_stat_output and (s == memory_organizer)) {
        size_t lastindex = label.find_last_of("/");
        std::string output_filename =
            label.substr(0, lastindex) + std::string("/network_stats.json");
        write_json(module, output_filename, label, arch_config);
      }

      // Show Performance Model estimation
      if (generate_network_performance and (s == scheduler)) {
        mlir::ArchitectureConfig::ArchitectureConfigAttr arch_c =
            AccessArchitectureConfigAttribute_From_Module(module);
        mlir::planner::u55_cost_model cost_model;
        cost_model.print_module_stats(std::cout, module, arch_c);
      }
    }
  }

  if (generate_mlir_output) {
    std::string output_filename = label + std::string("_output.mlir");
    std::error_code error_code;
    llvm::raw_fd_ostream output_ostream(output_filename.c_str(), error_code);
    module.get().print((llvm::raw_ostream &)output_ostream);
    output_ostream.flush();
  }

  if (ir_visualization > 0)
    std::cout << RED << "IR Printout" << RESET << std::endl;
  switch (ir_visualization) {
  case 0: {
    printIR(*block, mlir::utils::info_option::off);
    std::cout << std::endl;
    break;
  }
  case 1: {
    printIR(*block, mlir::utils::info_option::connections);
    std::cout << std::endl;
    break;
  }
  case 2: {
    printIR(*block, mlir::utils::info_option::tensor_implementations);
    std::cout << std::endl;
    break;
  }
  case 3: {
    printIR(*block, mlir::utils::info_option::op_detailed_info);
    std::cout << std::endl;
    break;
  }
  case 4: {
    printIR(*block, mlir::utils::info_option::op_tensors);
    std::cout << std::endl;
    break;
  }
  case 5: {
    printIR(*block, mlir::utils::info_option::kernel_tensors);
    std::cout << std::endl;
    break;
  }
  case 6: {
    printIR(*block, mlir::utils::info_option::cascade_tensors);
    std::cout << std::endl;
    break;
  }
  }

  // Show the compilation timepython scanf
  if (generate_compilation_timing_numbers) {
    ofstream compilation_timing_numbers;
    std::string compilation_timing_numbers_filename =
        input_filename.substr(0, input_filename.size() - 5) +
        "_compilation_timing_numbers.tflite";
    compilation_timing_numbers.open(
        compilation_timing_numbers_filename.c_str());

    if (run_async) {
      std::cout << std::endl
                << RED << "Compilation Execution Time" << RESET << std::endl;
      std::cout << "Total Execution Time: " << compiler_stage_timing[0]
                << std::endl
                << std::endl;
    } else {
      std::cout << std::endl
                << RED << "Compilation Execution Time" << RESET << std::endl;

      double total = 0;
      for (auto s : compiler_stages_enabled) {
        std::cout << std::setw(30) << stage_to_string((compiler_stage)s) << ": "
                  << compiler_stage_timing[s] << "s\n";
        total += compiler_stage_timing[s];

        compilation_timing_numbers << stage_to_string((compiler_stage)s) << ","
                                   << compiler_stage_timing[s] << "\n";
      }

      std::cout << "Total Execution Time: " << total << std::endl << std::endl;

      compilation_timing_numbers << "total," << total << "\n";
    }

    compilation_timing_numbers.close();
  }

  std::cout << std::endl << "Compiler Driver End" << std::endl << std::endl;
  return 0;
}

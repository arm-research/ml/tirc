/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#include "src/ir/architecture_config/architecture_config.h"
#include "src/ir/architecture_config/architecture_config_attr.cc.inc"

namespace mlir {
namespace utils {

std::string block_config_to_str(NpuBlockConfig block_config) {
  std::stringstream ss;
  ss << block_config[0] << "," << block_config[1] << "," << block_config[2]
     << "," << block_config[3];
  return ss.str();
}

std::string block_type_to_str(NpuBlockType bt) {
  switch (bt) {
  case Default:
    return std::string("Default");
    break;
  case ConvolutionMxN:
    return std::string("ConvolutionMxN");
    break;
  case VectorProduct:
    return std::string("VectorProduct");
    break;
  case Pooling:
    return std::string("Pooling");
    break;
  case ConvolutionDepthWise:
    return std::string("ConvolutionDepthWise");
    break;
  case ElementWise:
    return std::string("ElementWise");
    break;
  case ReduceSum:
    return std::string("ReduceSum");
    break;
  default:
    return std::string("Undefined");
    break;
  }
}

std::string mem_area_to_str(value_mem_area ma) {
  switch (ma) {
  case ma_Unknown:
    return std::string("Unknown");
    break;
  case ma_Sram:
    return std::string("Sram");
    break;
  case ma_Dram:
    return std::string("Dram");
    break;
  case ma_OnChipFlash:
    return std::string("OnChipFlash");
    break;
  case ma_OffChipFlash:
    return std::string("OffChipFlash");
    break;
  case ma_Shram:
    return std::string("Shram");
    break;
  default:
    return std::string("Undefined");
    break;
  }
}

std::string mem_type_to_str(value_mem_type mt) {
  switch (mt) {
  case mt_Unknown:
    return std::string("Unknown");
    break;
  case mt_Permanent_NPU:
    return std::string("Permanent_NPU");
    break;
  case mt_Permanent_CPU:
    return std::string("Permanent_CPU");
    break;
  case mt_Scratch:
    return std::string("Scratch");
    break;
  case mt_Scratch_fast:
    return std::string("Scratch_fast");
    break;
  default:
    return std::string("Undefined");
    break;
  }
}

std::string purpose_to_str(value_purpose purpose) {
  switch (purpose) {
  case vp_Unknown:
    return std::string("Unknown");
    break;
  case vp_Weights:
    return std::string("Weights");
    break;
  case vp_FeatureMap:
    return std::string("FeatureMap");
    break;
  case vp_Scratch:
    return std::string("Scratch");
    break;
  case vp_LUT:
    return std::string("LUT");
    break;
  case vp_FSBias:
    return std::string("FSBias");
    break;
  default:
    return std::string("Undefined");
    break;
  }
}

std::string sub_purpose_to_str(value_sub_purpose sub_purpose) {
  switch (sub_purpose) {
  case vsp_Unknown:
    return std::string("Unknown");
    break;
  case vsp_Standard:
    return std::string("Standard");
    break;
  case vsp_DoubleBuffer:
    return std::string("DoubleBuffer");
    break;
  case vsp_RollingBufferX:
    return std::string("RollingBufferX");
    break;
  case vsp_RollingBufferY:
    return std::string("RollingBufferY");
    break;
  case vsp_RollingBufferXY:
    return std::string("RollingBufferXY");
    break;
  default:
    return std::string("Undefined");
    break;
  }
}

std::string format_to_str(value_format format) {
  switch (format) {
  case vf_OHWI:
    return std::string("OHWI");
    break;
  case vf_HWIO:
    return std::string("HWIO");
    break;
  case vf_HWOI:
    return std::string("HWOI");
    break;
  case vf_NHWC:
    return std::string("NHWC");
    break;
  case vf_NHCWB16:
    return std::string("NHCWB16");
    break;
  case vf_WeightCompressed:
    return std::string("WeighCompressed");
    break;
  default:
    return std::string("Undefined");
    break;
  }
}
std::vector<int32_t> get_storage_rounding_quantums(value_format format) {
  // sizes as N x H x W x C. we need to round up to these when allocating
  // storage
  switch (format) {
  case vf_HWIO:
    return {1, 1, 1, 1};
  case vf_OHWI:
    return {1, 1, 1, 1};
  case vf_NHWC:
    return {1, 1, 1, 1};
  case vf_NHCWB16:
    return {1, 1, 1, 16};
  default:
    return {1, 1, 1, 1};
  }
}
std::vector<int32_t> get_brick_sizes(value_format format) {
  // brick sizes as N x H x W x C. We have to fetch whole bricks at a time
  switch (format) {
  case vf_HWIO:
    return {1, 1, 1, 1};
  case vf_OHWI:
    return {1, 1, 1, 1};
  case vf_NHWC:
    return {1, 1, 1, 1};
  case vf_NHCWB16:
    return {1, 1, 1, 16};
  default:
    return {1, 1, 1, 1};
  }
}

// Calculate block configuration for ALL known IFM operations and
// accumulator sizes. Consumers will need to select their preferred
// operation and bit-width at read-time.
SHRAMBlockConfig
get_block_config(mlir::ArchitectureConfig::ArchitectureConfigAttr arch,
                 uint32_t width, uint32_t height, uint32_t depth) {
  SHRAMElementsSizes SHRAM_ElementsSizes;

  // Number of bytes required for any SHRAM element for a FM of given
  // dimensions. For IFM: size = H*W*Align(D*BYTE_WIDTH, 8) For ACC: size =
  // H*W*Align(D,8)*BYTE_WIDTH

  std::vector<uint32_t> d1 =
      round_up<uint32_t>(depth, SHRAM_ElementsSizes.PreAlign);
  std::vector<uint32_t> d1_mul =
      mul<uint32_t>(d1, SHRAM_ElementsSizes.ByteSizes);

  std::vector<uint32_t> d2 =
      round_up<uint32_t>(d1_mul, SHRAM_ElementsSizes.PostAlign);
  std::vector<uint32_t> size_bytes = mul<uint32_t>((height * width), d2);

  auto accel_config = (AcceleratorConfig)arch.accel_config().getInt();

  uint32_t shram_bank_size = arch.shram_bank_size().getInt();
  std::vector<uint32_t> shram_bank_granules =
      get_acclerator_config(accel_config).shram_granules;

  // Convert byte size (rounded) to size in banks
  std::vector<uint32_t> size_banks =
      round_up_divide<uint32_t>(size_bytes, shram_bank_size);
  std::vector<uint32_t> size_banks_2 = mul<uint32_t>(
      2, size_banks); // Double buffer the IFM/Acc (need twice as many banks)

  // Round bank requirement to bank granularity
  std::vector<uint32_t> required_banks =
      round_up<uint32_t>(size_banks_2, shram_bank_granules);

  return SHRAMBlockConfig(size_bytes, required_banks);
}

uint32_t
calc_ifm_block_depth(mlir::ArchitectureConfig::ArchitectureConfigAttr arch,
                     uint32_t ifm_depth, uint32_t ifm_bits) {
  // std::cout << "calc_ifm_block_depth::Start" << std::endl;
  auto accel_config = (AcceleratorConfig)arch.accel_config().getInt();
  uint32_t ifm_ublock_depth =
      get_acclerator_config(accel_config).ifm_ublock.depth;
  uint32_t ifm_depth_rounded = round_up(ifm_depth, ifm_ublock_depth);
  uint32_t max_block_depth = ((8 * 32) / ifm_bits);
  // std::cout << "calc_ifm_block_depth::End" << std::endl;
  return min(max_block_depth, ifm_depth_rounded);
}

// Calculate the size of the IFM block given a depth, target OFM block and a
// kernel
BlockStruct
get_ifm_block_size(mlir::ArchitectureConfig::ArchitectureConfigAttr arch,
                   uint32_t ifm_block_depth, BlockStruct ofm_block,
                   FilterMetaData kernel, resampling_mode resampling_mode,
                   BlockStruct subkernel) {
  uint32_t upscaling = (resampling_mode == rm_NONE) ? 1 : 2;

  auto accel_config = (AcceleratorConfig)arch.accel_config().getInt();
  uint32_t ofm_ublock_height =
      get_acclerator_config(accel_config).ofm_ublock.height;
  uint32_t ofm_ublock_width =
      get_acclerator_config(accel_config).ofm_ublock.width;

  // Height
  uint32_t ifm_odd_2x_height_enable = 0;
  uint64_t dilated_kernel_height =
      ((kernel.height - 1) * kernel.dilation.y) + 1;
  float ifm_block_height_fp = float(
      (ofm_block.height - 1) * kernel.stride.y +
      min(subkernel.height, dilated_kernel_height) + ifm_odd_2x_height_enable);
  ifm_block_height_fp = floor(ifm_block_height_fp / upscaling);
  uint32_t ifm_block_height =
      round_up<uint32_t>(ifm_block_height_fp, ofm_ublock_height);

  // Width
  uint32_t ifm_odd_2x_width_enable = 0;
  uint64_t dilated_kernel_width = ((kernel.width - 1) * kernel.dilation.x) + 1;
  uint32_t ifm_block_width_fp = float(
      (ofm_block.width - 1) * kernel.stride.x +
      min(subkernel.width, dilated_kernel_width) + ifm_odd_2x_width_enable);
  ifm_block_width_fp = floor(ifm_block_width_fp / upscaling);
  uint32_t ifm_block_width =
      round_up<uint32_t>(ifm_block_width_fp, ofm_ublock_width);

  return BlockStruct(ifm_block_width, ifm_block_height, ifm_block_depth);
}

int64_t get_ifm_block_depth(NpuBlockType npu_block_type,
                            int64_t ifm_tensor_depth,
                            int64_t ifm_tensor_bit_depth,
                            NpuBlockTraversal block_traversal,
                            int64_t ofm_block_depth) {
  int64_t ifm_block_depth = ofm_block_depth;
  if ((npu_block_type == NpuBlockType::ConvolutionMxN) ||
      (npu_block_type == NpuBlockType::VectorProduct) ||
      (npu_block_type == NpuBlockType::ReduceSum)) {
    if ((ifm_tensor_bit_depth == 16) or
        (block_traversal == tbt_PartKernelFirst))
      ifm_block_depth = 16;
    else if (ifm_tensor_bit_depth == 8)
      ifm_block_depth = 32;
    else
      ifm_block_depth = 8;
  }
  return min(ifm_tensor_depth, ifm_block_depth);
}

// Returns available number of SHRAM banks depending on activation lookup table
// being used or not
unsigned available_shram_banks(
    bool is_lut_used,
    mlir::ArchitectureConfig::ArchitectureConfigAttr &arch_c) {
  return available_shram_banks(is_lut_used, arch_c.shram_total_banks().getInt(),
                               arch_c.shram_reserved_unused_banks().getInt());
}

value_mem_area tensor_storage_mem_area(
    value_purpose purpose,
    mlir::ArchitectureConfig::ArchitectureConfigAttr &arch_c, bool is_const) {
  switch (purpose) {
  case vp_Unknown:
    return ma_Unknown;
    break;
  case vp_Weights:
    return (value_mem_area)arch_c.permanent_storage_mem_area().getInt();
    break;
  case vp_FeatureMap:
    return is_const
               ? (value_mem_area)arch_c.permanent_storage_mem_area().getInt()
               : (value_mem_area)arch_c.feature_map_storage_mem_area().getInt();
    break;
  case vp_Scratch:
    return (value_mem_area)arch_c.feature_map_storage_mem_area().getInt();
    break;
  case vp_LUT:
    return (value_mem_area)arch_c.permanent_storage_mem_area().getInt();
    break;
  }
}

value_mem_type tensor_storage_mem_type(
    value_purpose purpose,
    mlir::ArchitectureConfig::ArchitectureConfigAttr &arch_c, bool is_const) {
  switch (purpose) {
  case vp_Unknown:
    return mt_Unknown;
    break;
  case vp_Weights:
    return mt_Permanent_NPU;
    break;
  case vp_FeatureMap:
    return is_const ? mt_Permanent_NPU : mt_Scratch;
    break;
  case vp_Scratch:
    return mt_Scratch;
    break;
  case vp_LUT:
    return mt_Scratch;
    break;
  }
}

SmallVector<int64_t, 2> get_sub_kernel_limits(NpuBlockType npu_block_type) {
  switch (npu_block_type) {
  case NpuBlockType::Default:
    return {8, 8};
  case NpuBlockType::VectorProduct:
    return {1, 1};
  case NpuBlockType::ConvolutionMxN:
    return {8, 8};
  case NpuBlockType::Pooling:
    return {8, 8};
  case NpuBlockType::ConvolutionDepthWise:
    return {8, 8};
  case NpuBlockType::ElementWise:
    return {1, 1};
  case NpuBlockType::ReduceSum:
    return {8, 8};
  }
}

std::string datatype_to_str(value_datatype data_type) {
  switch (data_type) {
  case int8:
    return std::string("int8");
    break;
  case uint8:
    return std::string("uint8");
    break;
  case int16:
    return std::string("int16");
    break;
  case uint16:
    return std::string("uint16");
    break;
  case int32:
    return std::string("int32");
    break;
  case uint32:
    return std::string("uint32");
    break;
  case unknown:
    return std::string("unknown");
    break;
  default:
    return std::string("undefined");
    break;
  }
}

HWConfig get_acclerator_config(AcceleratorConfig accel_config) {
  switch (accel_config) {
  case AcceleratorConfig::u55_32:
    return HWConfig(32, 1, BlockStruct(1, 1, 4), BlockStruct(1, 1, 8), 16,
                    {2, 2, 2, 2, 4, 4, 4, 4}, 1);
    break;
  case AcceleratorConfig::u55_64:
    return HWConfig(64, 1, BlockStruct(1, 1, 8), BlockStruct(1, 1, 8), 16,
                    {2, 2, 2, 2, 4, 4, 4, 8}, 2);
    break;
  case AcceleratorConfig::u55_128:
    return HWConfig(128, 1, BlockStruct(2, 1, 8), BlockStruct(2, 2, 8), 24,
                    {4, 4, 4, 4, 8, 4, 8, 12}, 4);
    break;
  case AcceleratorConfig::u55_256:
    return HWConfig(256, 1, BlockStruct(2, 2, 8), BlockStruct(2, 2, 8), 48,
                    {8, 8, 8, 8, 16, 8, 16, 20}, 8);
    break;
  case AcceleratorConfig::u65_256:
    return HWConfig(256, 1, BlockStruct(2, 2, 8), BlockStruct(2, 2, 8), 48,
                    {8, 8, 8, 8, 16, 8, 16, 20}, 8);
    break;
  case AcceleratorConfig::u65_512:
    return HWConfig(256, 2, BlockStruct(2, 2, 8), BlockStruct(2, 2, 8), 48,
                    {8, 8, 8, 8, 16, 8, 16, 20}, 8);
    break;
  default: {
    std::cout << "get_acclerator_config::Config not known" << std::endl;
    exit(1);
  }
  }
}

ip_unitE ip_unit_string_to_enum(std::string ip_unit) {
  if (ip_unit == "Npu")
    return ip_unitE::Npu;
  else if (ip_unit == "Cpu")
    return ip_unitE::Cpu;
}

compute_unitE compute_unit_string_to_enum(std::string compute_unit) {
  if (compute_unit == "Dpu")
    return compute_unitE::Dpu;
  else if (compute_unit == "VectorEngine")
    return compute_unitE::VectorEngine;
}

compute_sub_unitE
compute_sub_unit_string_to_enum(std::string compute_sub_unit) {
  if (compute_sub_unit == "MacEngine")
    return compute_sub_unitE::MacEngine;
  else if (compute_sub_unit == "OutputUnit")
    return compute_sub_unitE::OutputUnit;
}

// Returns available number of SHRAM banks depending on activation lookup table
// being used or not
uint32_t available_shram_banks(bool is_lut_used, int64_t shram_total_banks,
                               int64_t shram_reserved_unused_banks) {
  unsigned banks = shram_total_banks;
  if (is_lut_used and (shram_reserved_unused_banks == 0))
    banks -= 2;
  return banks;
}

uint32_t
available_shram_banks(mlir::ArchitectureConfig::ArchitectureConfigAttr arch,
                      bool uses_activation_lut) {
  uint32_t banks = arch.shram_total_banks().getInt();
  if (uses_activation_lut and
      (arch.shram_reserved_unused_banks().getInt() == 0))
    banks = (banks - 2);
  return banks;
}

BlockStruct get_ofm_block_max() { return BlockStruct(64, 32, 128); }

void architecture_config_print(
    mlir::ArchitectureConfig::ArchitectureConfigAttr &attr) {
  std::cout << "Architecture Config::" << std::endl;
  std::cout << "   Name               -> " << attr.name().getValue().str()
            << std::endl;
  std::cout << "   Accelerator Sku    -> "
            << ConvertToString((AcceleratorConfig)attr.accel_config().getInt())
                   .c_str()
            << std::endl;
  std::cout << "   Shared Buffer Area -> "
            << ConvertToString(
                   (SharedBufferArea)attr.shared_buffer_area().getInt())
                   .c_str()
            << std::endl;
}

std::string ConvertToString(SharedBufferArea shared_buffer) {
  switch (shared_buffer) {
  case OFM:
    return "OFM";
  case WEIGHTS:
    return "WEIGHTS";
  case IFM:
    return "IFM";
  case ACCUMULATORS:
    return "ACCUMULATORS";
  case SIZE:
    return "SIZE";
  case UNKNOWN_SHARED_BUFFER_AREA:
    return "UNKNOWN_SHARED_BUFFER_AREA";
  }
};

std::string ConvertToString(AcceleratorConfig accel_config) {
  switch (accel_config) {
  case u55_32:
    return "u55_32";
  case u55_64:
    return "u55_64";
  case u55_128:
    return "u55_128";
  case u55_256:
    return "u55_256";
  case u65_256:
    return "u65_256";
  case u65_512:
    return "u65_512";
  case unknown_accel_config:
    return "unknown_accel_config";
  }
};

std::vector<double> get_output_cycles_tables(
    mlir::ArchitectureConfig::ArchitectureConfigAttr &arch) {
  AcceleratorConfig accel_config =
      (AcceleratorConfig)arch.accel_config().getInt();
  switch (accel_config) {
  case u55_32:
    return {2.0, 3.0, 3.0, 3.0, 4.0, 6.0, 1.0, 2.0};
  case u55_64:
    return {1.0, 1.5, 1.5, 1.5, 2.0, 3.0, 0.5, 1.0};
  case u55_128:
    return {0.75, 1.25, 0.75, 0.75, 1.0, 1.5, 0.25, 0.5};
  case u55_256:
    return {0.625, 1.125, 0.5, 0.375, 0.5, 0.75, 0.125, 0.25};
  case u65_256:
    return {0.625, 1.125, 0.5, 0.375, 0.5, 0.75, 0.125, 0.25};
  case u65_512:
    return {0.3125, 0.5625, 0.25, 0.1875, 0.25, 0.375, 0.0625, 0.125};
  default:
    FATAL_ERROR("get_output_cycles_tables::unknown_accel_config");
  }
};

std::vector<double> get_activation_cycles_tables(
    mlir::ArchitectureConfig::ArchitectureConfigAttr &arch) {
  AcceleratorConfig accel_config =
      (AcceleratorConfig)arch.accel_config().getInt();
  switch (accel_config) {
  case u55_32:
    return {1.0, 1.0, 0.0};
  case u55_64:
    return {1.0, 1.0, 0.0};
  case u55_128:
    return {1.0, 0.5, 0.0};
  case u55_256:
    return {1.0, 0.25, 0.0};
  case u65_256:
    return {1.0, 0.25, 0.0};
  case u65_512:
    return {0.5, 0.125, 0.0};
  default:
    FATAL_ERROR("get_output_cycles_tables::unknown_accel_config");
  }
};

bool is_u55_system(mlir::ArchitectureConfig::ArchitectureConfigAttr &arch_c) {
  AcceleratorConfig accel_config =
      (AcceleratorConfig)arch_c.accel_config().getInt();
  switch (accel_config) {
  case u55_32:
    return true;
  case u55_64:
    return true;
  case u55_128:
    return true;
  case u55_256:
    return true;
  case u65_256:
    return false;
  case u65_512:
    return false;
  default:
    FATAL_ERROR("is_u55_system::unknown_accel_config");
  }
}

bool is_u65_system(mlir::ArchitectureConfig::ArchitectureConfigAttr &arch_c) {
  AcceleratorConfig accel_config =
      (AcceleratorConfig)arch_c.accel_config().getInt();
  switch (accel_config) {
  case u55_32:
    return false;
  case u55_64:
    return false;
  case u55_128:
    return false;
  case u55_256:
    return false;
  case u65_256:
    return true;
  case u65_512:
    return true;
  default:
    FATAL_ERROR("is_u55_system::unknown_accel_config");
  }
}

uint64_t get_memory_latency_u55(value_mem_area mem_area,
                                BandwidthDirection bd_dir) {
  if ((mem_area == ma_Sram) and (bd_dir == bd_Read))
    return 32;
  else if ((mem_area == ma_Sram) and (bd_dir == bd_Write))
    return 32;
  else if ((mem_area == ma_OffChipFlash) and (bd_dir == bd_Read))
    return 64;
  else if ((mem_area == ma_OffChipFlash) and (bd_dir == bd_Write))
    return 64;
  FATAL_ERROR("get_memory_latency_u55::unknown options");
}

ostream &operator<<(ostream &os, const value_datatype &b) {
  switch (b) {
  case int8: {
    os << "int8";
    break;
  }
  case uint8: {
    os << "uint8";
    break;
  }
  case int16: {
    os << "int16";
    break;
  }
  case uint16: {
    os << "uint16";
    break;
  }
  case int32: {
    os << "int32";
    break;
  }
  case uint32: {
    os << "uint32";
    break;
  }
  case unknown: {
    os << "unknown";
    break;
  }
  }
  return os;
}

} // namespace utils
} // namespace mlir

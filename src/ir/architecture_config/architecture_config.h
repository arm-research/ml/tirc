/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#ifndef ARCHITECTURE_CONFIG_H
#define ARCHITECTURE_CONFIG_H

#include <iostream>
#include <stdint.h>
#include <string>

#include "mlir/IR/AffineMap.h"  // from @llvm-project
#include "mlir/IR/Attributes.h" // from @llvm-project
#include "mlir/IR/Builders.h"
#include "mlir/IR/BuiltinAttributes.h"
#include "mlir/IR/BuiltinOps.h"
#include "mlir/IR/Location.h"    // from @llvm-project
#include "mlir/IR/MLIRContext.h" // from @llvm-project

#include "src/utils/data_structure_utils.h"
#include "src/utils/numeric_utils.h"

#include "src/ir/architecture_config/architecture_config_attr.h.inc"

namespace mlir {
namespace utils {

enum Legalization { TosaCommon, ScheduleIRCommon, Custom, TFLite };

#define TYPICAL_NO_INPUTS 4
#define TYPICAL_NO_OUTPUTS 4

enum SharedBufferArea {
  OFM,
  WEIGHTS,
  IFM,
  ACCUMULATORS,
  SIZE,
  UNKNOWN_SHARED_BUFFER_AREA
};

enum AcceleratorConfig {
  u55_32,
  u55_64,
  u55_128,
  u55_256,
  u65_256,
  u65_512,
  unknown_accel_config
};

enum Cycles {
  c_Npu = 0,
  c_Npu_ElementWise = 1,
  c_Cpu = 2,
  c_SramAccess = 3,
  c_TotalPerPass = 4,
  c_DramAccess = 5,
  c_OnChipFlashAccess = 6,
  c_OffChipFlashAccess = 7,
  c_Max_Component = 8,
  c_size = 9
};

enum MacCount { mc_NeuralNetworkMacs = 0, mc_HardwareMacs = 1, mc_size = 2 };

enum BandwidthDirection { bd_Read = 0, bd_Write = 1, bd_size = 2 };

enum StripePlacement {
  pp_Unknown,
  pp_Cpu,
  pp_Npu,
  pp_MemoryOnly,
  pp_StartupInit
};

// These are the datatypes we allow at the moment. Neet to 16bit, 32bit, 48bit
// and float 16 in the future
enum value_datatype { int8, uint8, int16, uint16, int32, uint32, unknown };
ostream &operator<<(ostream &os, const value_datatype &b);

enum Activation {
  ReluN,
  Sigmoid,
  Tanh,
};

enum NpuBlockType {
  Default,
  ConvolutionMxN,
  VectorProduct,
  Pooling,
  ConvolutionDepthWise,
  ElementWise,
  ReduceSum,
  Memory
};

enum NpuBlockTraversal {
  tbt_Default,
  tbt_DepthWise,
  tbt_DepthFirst,
  tbt_PartKernelFirst
};

enum resampling_mode { rm_NONE, rm_NEAREST, rm_TRANSPOSE };

enum value_mem_area {
  ma_Unknown,
  ma_Sram,
  ma_Dram,
  ma_OnChipFlash,
  ma_OffChipFlash,
  ma_Shram,
  ma_size
};

enum value_mem_type {
  mt_Unknown,
  mt_Permanent_NPU,
  mt_Permanent_CPU,
  mt_Scratch,
  mt_Scratch_fast,
  mt_size
};

enum value_purpose {
  vp_Unknown,
  vp_Weights,
  vp_FeatureMap,
  vp_Scratch,
  vp_LUT,
  vp_FSBias,
  vp_ScratchFast,
  vp_size
};

enum value_sub_purpose {
  vsp_Standard,
  vsp_DoubleBuffer,
  vsp_RollingBufferX,
  vsp_RollingBufferY,
  vsp_RollingBufferXY,
  vsp_Unknown,
  vsp_size
};

enum value_format {
  vf_Unknown,
  vf_WeightCompressed,
  vf_NHWC,
  vf_NHCWB16,
  vf_OHWI, // This may point to "value" and "value_compressed"
  vf_HWIO, // This may point to "value" and "value_compressed"
  vf_HWOI, // This may point to "value" and "value_compressed"
};

enum ip_unitE { Npu, Cpu };

enum compute_unitE { Dpu, VectorEngine };

enum compute_sub_unitE { MacEngine, OutputUnit };

enum SHRAMElements {
  IFM8,
  IFM16,
  IFM8_Elementwise,
  IFM16_Elementwise,
  IFM32,
  Acc16,
  Acc32,
  Acc40,
};

struct BlockStruct {
  uint64_t width;
  uint64_t height;
  uint64_t depth;
  BlockStruct(uint64_t w, uint64_t h, uint64_t d) {
    width = w;
    height = h;
    depth = d;
  }
  BlockStruct(SmallVector<uint64_t, 3> values) {
    width = values[0];
    height = values[1];
    depth = values[2];
  }
  BlockStruct() {}
};
using NpuBlockConfig = SmallVector<int32_t, 4>; // H, W, DI, DO

struct MappingStructure {
  ip_unitE ip_unit;
  compute_unitE compute_unit;
  compute_sub_unitE compute_sub_unit;
};

typedef struct Shape {
  union {
    struct {
      int d3;
      int d2;
      int d1;
      int d0;
    };
    struct {
      int depth;
      int width;
      int height;
      int batch;
    } nhwc;
    struct {
      int out;
      int in;
      int width;
      int height;
    } hwio;
    struct {
      int in;
      int width;
      int height;
      int out;
    } ohwi;
  };

  SmallVector<int64_t, 1> getShape() { return {d0, d1, d2, d3}; }

  value_format format = vf_Unknown;

} Shape;

class HWConfig {
public:
  uint32_t macs;
  uint32_t cores;
  BlockStruct ofm_ublock;
  BlockStruct ifm_ublock;
  uint32_t shram_banks;
  std::vector<uint32_t> shram_granules;
  uint32_t elem_units;
  HWConfig(uint32_t macs, uint32_t cores, BlockStruct ofm_ublock,
           BlockStruct ifm_ublock, uint32_t shram_banks,
           std::vector<uint32_t> shram_granules, uint32_t elem_units) {
    this->macs = macs;
    this->cores = cores;
    this->ofm_ublock = ofm_ublock;
    this->ifm_ublock = ifm_ublock;
    this->shram_banks = shram_banks;
    this->shram_granules = shram_granules;
    this->elem_units = elem_units;
  }

private:
  HWConfig();
};

class SHRAMElementsSizes {
public:
  SHRAMElementsSizes() {
    BitSizes = {8, 16, 8, 16, 32, 16, 32, 40};
    ByteSizes = {1, 2, 1, 2, 4, 2, 4, 5};
    PostAlign = {8, 8, 8, 8, 8, 1, 1, 1};
    PreAlign = {1, 1, 1, 1, 1, 8, 8, 8};
  }

  std::vector<uint32_t> BitSizes;
  std::vector<uint32_t> ByteSizes;
  std::vector<uint32_t> PostAlign;
  std::vector<uint32_t> PreAlign;
};

struct SHRAMBlockConfig {
  SHRAMBlockConfig(std::vector<uint32_t> sizes, std::vector<uint32_t> banks) {
    this->sizes = sizes;
    this->banks = banks;
  }
  std::vector<uint32_t> sizes;
  std::vector<uint32_t> banks;
};

std::string ConvertToString(AcceleratorConfig accel_config);
std::string ConvertToString(SharedBufferArea shared_buffer);

ip_unitE ip_unit_string_to_enum(std::string ip_unit);
compute_unitE compute_unit_string_to_enum(std::string compute_unit);
compute_sub_unitE compute_sub_unit_string_to_enum(std::string compute_sub_unit);
uint32_t available_shram_banks(bool is_lut_used, int64_t shram_total_banks,
                               int64_t shram_reserved_unused_banks);
uint32_t
available_shram_banks(mlir::ArchitectureConfig::ArchitectureConfigAttr arch,
                      bool uses_activation_lut);
std::string block_config_to_str(NpuBlockConfig block_config);
std::string mem_area_to_str(value_mem_area ma);
std::string mem_type_to_str(value_mem_type mt);
std::string purpose_to_str(value_purpose purpose);
std::string sub_purpose_to_str(value_sub_purpose sub_purpose);
std::string format_to_str(value_format format);
std::string block_type_to_str(NpuBlockType bt);
std::vector<int32_t> get_storage_rounding_quantums(value_format format);
std::vector<int32_t> get_brick_sizes(value_format format);
SHRAMBlockConfig
get_block_config(mlir::ArchitectureConfig::ArchitectureConfigAttr arch,
                 uint32_t width, uint32_t height, uint32_t depth);
uint32_t
calc_ifm_block_depth(mlir::ArchitectureConfig::ArchitectureConfigAttr arch,
                     uint32_t ifm_depth, uint32_t ifm_bits);
BlockStruct
get_ifm_block_size(mlir::ArchitectureConfig::ArchitectureConfigAttr arch,
                   uint32_t ifm_block_depth, BlockStruct ofm_block,
                   FilterMetaData kernel,
                   resampling_mode resampling_mode = rm_NONE,
                   BlockStruct subkernel = BlockStruct(8, 8, 65536));
int64_t get_ifm_block_depth(NpuBlockType npu_block_type,
                            int64_t ifm_tensor_depth,
                            int64_t ifm_tensor_bit_depth,
                            NpuBlockTraversal block_traversal,
                            int64_t ofm_block_depth);
unsigned
available_shram_banks(bool is_lut_used,
                      mlir::ArchitectureConfig::ArchitectureConfigAttr &arch_c);
value_mem_area tensor_storage_mem_area(
    value_purpose purpose,
    mlir::ArchitectureConfig::ArchitectureConfigAttr &arch_c,
    bool is_const = false);
value_mem_type tensor_storage_mem_type(
    value_purpose purpose,
    mlir::ArchitectureConfig::ArchitectureConfigAttr &arch_c,
    bool is_const = false);
BlockStruct get_ofm_block_max();
void architecture_config_print(
    mlir::ArchitectureConfig::ArchitectureConfigAttr &attr);
SmallVector<int64_t, 2> get_sub_kernel_limits(NpuBlockType npu_block_type);
std::string datatype_to_str(value_datatype data_type);
HWConfig get_acclerator_config(AcceleratorConfig accel_config);
std::vector<double> get_output_cycles_tables(
    mlir::ArchitectureConfig::ArchitectureConfigAttr &arch);
std::vector<double> get_activation_cycles_tables(
    mlir::ArchitectureConfig::ArchitectureConfigAttr &arch);

double get_memory_bandwidths_per_cycle(value_mem_area ma,
                                       AcceleratorConfig accel_config);

mlir::ArchitectureConfig::ArchitectureConfigAttr
AccessArchitectureConfigAttribute_From_Module(mlir::OwningModuleRef &module);
mlir::ArchitectureConfig::ArchitectureConfigAttr
AccessArchitectureConfigAttribute_From_NpuRegion(Operation *NpuRegion);

void AddArchitectureConfigAttribute_to_NpuRegions(mlir::FuncOp function,
                                                  mlir::MLIRContext *context);

bool is_u55_system(mlir::ArchitectureConfig::ArchitectureConfigAttr &arch_c);
bool is_u65_system(mlir::ArchitectureConfig::ArchitectureConfigAttr &arch_c);

uint64_t get_memory_latency_u55(value_mem_area mem_area,
                                BandwidthDirection bd_dir);

} // namespace utils
} // namespace mlir

#endif // ARCHITECTURE_CONFIG_H

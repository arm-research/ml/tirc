/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#include "src/ir/architecture_config/architecture_config_utils.h"
#include "src/utils/printIR.h"

namespace mlir {
namespace utils {

mlir::ArchitectureConfig::ArchitectureConfigAttr
AccessArchitectureConfigAttribute_From_Module(mlir::OwningModuleRef &module) {
  Block *block = module.get().getBody();
  llvm::iplist<Operation> &operations = block->getOperations();

  for (auto &op : operations) {
    if (llvm::isa<mlir::FuncOp>(op)) {
      auto function = dyn_cast<mlir::FuncOp>(op);
      if (function.getName() == "main") {
        Region *region = function.getCallableRegion();
        Block &block_f = region->front();
        llvm::iplist<Operation> &operations_f = block_f.getOperations();
        for (auto &op_f : operations_f) {
          if (llvm::isa<scheduleir::NpuRegionOp>(op_f)) {
            StringRef attr("architecture_config");
            return op_f.getAttr(attr)
                .cast<mlir::ArchitectureConfig::ArchitectureConfigAttr>();
          }
        }
      }
    }
  }

  FATAL_ERROR("AccessArchitectureConfigAttribute_From_Module::No Npu Regions "
              "found in Module");
}

mlir::ArchitectureConfig::ArchitectureConfigAttr
AccessArchitectureConfigAttribute_From_NpuRegion(Operation *NpuRegion) {
  if (not(llvm::isa<scheduleir::NpuRegionOp>(NpuRegion)))
    FATAL_ERROR("AccessCompilerConfigAttribute_From_NpuRegion::Operation must "
                "be a scheduleir::NpuRegionOp")
  StringRef attr("architecture_config");
  return NpuRegion->getAttr(attr)
      .cast<mlir::ArchitectureConfig::ArchitectureConfigAttr>();
}

mlir::ArchitectureConfig::ArchitectureConfigAttr
CreateArchitectureConfigAttribute(MLIRContext *context, int32_t c_sram_size) {
  /* These are currently hardcoded to a u55_256 and need to be made more
   * flexible in the futue */

  ::mlir::OpBuilder builder(context);

  float clock = 500000000;
  ::mlir::IntegerAttr npu_clock = builder.getI64IntegerAttr(clock);

  ::mlir::IntegerAttr no_cores = builder.getI64IntegerAttr(1);

  auto architecture_config_label = AcceleratorConfig::u55_256;

  ::mlir::IntegerAttr accel_config =
      builder.getI32IntegerAttr(int(architecture_config_label));

  ::mlir::StringAttr system_config =
      builder.getStringAttr(StringRef("internal-default"));

  ::mlir::IntegerAttr shared_buffer_area =
      builder.getI32IntegerAttr(int(SharedBufferArea::OFM));

  ::mlir::IntegerAttr allocation_alignement = builder.getI32IntegerAttr(16);

  uint32_t shram_banks =
      get_acclerator_config(architecture_config_label).shram_banks;

  ::mlir::IntegerAttr shram_bank_size = builder.getI32IntegerAttr(1024);
  ::mlir::IntegerAttr shram_size_bytes =
      builder.getI32IntegerAttr(shram_banks * shram_bank_size.getInt());
  ::mlir::IntegerAttr shram_reserved_output_banks =
      builder.getI32IntegerAttr(2);
  ::mlir::IntegerAttr shram_reserved_weight_banks =
      builder.getI32IntegerAttr(0);
  ::mlir::IntegerAttr shram_reserved_unused_banks =
      builder.getI32IntegerAttr((shram_banks > 16) ? 2 : 0);
  ::mlir::IntegerAttr shram_total_banks = builder.getI32IntegerAttr(
      shram_banks - shram_reserved_unused_banks.getInt());
  ::mlir::IntegerAttr shram_lut_size = builder.getI32IntegerAttr(2048);
  unsigned asbd = mlir::utils::available_shram_banks(
      true, shram_total_banks.getInt(), shram_reserved_unused_banks.getInt());
  ::mlir::IntegerAttr shram_lut_address =
      builder.getI32IntegerAttr(shram_bank_size.getInt() * asbd);

  ::mlir::IntegerAttr feature_map_storage_mem_area =
      builder.getI32IntegerAttr(mlir::utils::value_mem_area::ma_Sram);
  ::mlir::IntegerAttr permanent_storage_mem_area =
      builder.getI32IntegerAttr(mlir::utils::value_mem_area::ma_OffChipFlash);
  ::mlir::IntegerAttr fast_storage_mem_area =
      builder.getI32IntegerAttr(mlir::utils::value_mem_area::ma_Sram);

  ::mlir::IntegerAttr sram_size = builder.getI32IntegerAttr(c_sram_size);
  ::mlir::IntegerAttr OFMSplitDepth = builder.getI32IntegerAttr(16);
  ::mlir::StringAttr name =
      builder.getStringAttr(StringRef("test_llcs_dialect"));

  float global_mcs = 1.0;
  ::mlir::FloatAttr global_memory_clock_scale =
      builder.getF32FloatAttr(global_mcs);

  ::mlir::FloatAttr memory_clock_scales_Sram =
      builder.getF32FloatAttr(1.0 * global_mcs);
  ::mlir::FloatAttr memory_clock_Sram = builder.getF32FloatAttr(
      clock * memory_clock_scales_Sram.getValueAsDouble());
  ::mlir::IntegerAttr memory_port_widths_Sram = builder.getI32IntegerAttr(64);
  ::mlir::FloatAttr memory_bandwidths_per_cycle_Sram =
      builder.getF32FloatAttr((memory_port_widths_Sram.getInt() *
                               memory_clock_scales_Sram.getValueAsDouble()) /
                              8);
  ::mlir::FloatAttr memory_bandwidths_per_second_Sram = builder.getF32FloatAttr(
      memory_bandwidths_per_cycle_Sram.getValueAsDouble() * clock);

  ::mlir::FloatAttr memory_clock_scales_OnChipFlash =
      builder.getF32FloatAttr(1.0 * global_mcs);
  ::mlir::FloatAttr memory_clock_OnChipFlash = builder.getF32FloatAttr(
      clock * memory_clock_scales_OnChipFlash.getValueAsDouble());
  ::mlir::IntegerAttr memory_port_widths_OnChipFlash =
      builder.getI32IntegerAttr(64);
  ::mlir::FloatAttr memory_bandwidths_per_cycle_OnChipFlash =
      builder.getF32FloatAttr(
          (memory_port_widths_OnChipFlash.getInt() *
           memory_clock_scales_OnChipFlash.getValueAsDouble()) /
          8);
  ::mlir::FloatAttr memory_bandwidths_per_second_OnChipFlash =
      builder.getF32FloatAttr(
          memory_bandwidths_per_cycle_OnChipFlash.getValueAsDouble() * clock);

  ::mlir::FloatAttr memory_clock_scales_OffChipFlash =
      builder.getF32FloatAttr(0.25 * global_mcs);
  ::mlir::FloatAttr memory_clock_OffChipFlash = builder.getF32FloatAttr(
      clock * memory_clock_scales_OffChipFlash.getValueAsDouble());
  ::mlir::IntegerAttr memory_port_widths_OffChipFlash =
      builder.getI32IntegerAttr(32);
  ::mlir::FloatAttr memory_bandwidths_per_cycle_OffChipFlash =
      builder.getF32FloatAttr(
          (memory_port_widths_OffChipFlash.getInt() *
           memory_clock_scales_OffChipFlash.getValueAsDouble()) /
          8);
  ::mlir::FloatAttr memory_bandwidths_per_second_OffChipFlash =
      builder.getF32FloatAttr(
          memory_bandwidths_per_cycle_OffChipFlash.getValueAsDouble() * clock);

  ::mlir::FloatAttr memory_clock_scales_Dram =
      builder.getF32FloatAttr(1.0 * global_mcs);
  ::mlir::FloatAttr memory_clock_Dram = builder.getF32FloatAttr(
      clock * memory_clock_scales_Dram.getValueAsDouble());
  ::mlir::IntegerAttr memory_port_widths_Dram = builder.getI32IntegerAttr(32);
  ::mlir::FloatAttr memory_bandwidths_per_cycle_Dram =
      builder.getF32FloatAttr((memory_port_widths_Dram.getInt() *
                               memory_clock_scales_Dram.getValueAsDouble()) /
                              8);
  ::mlir::FloatAttr memory_bandwidths_per_second_Dram = builder.getF32FloatAttr(
      memory_bandwidths_per_cycle_Dram.getValueAsDouble() * clock);

  ::mlir::ArrayAttr subkernel_max = builder.getI32ArrayAttr({8, 8, 65536});

  mlir::ArchitectureConfig::ArchitectureConfigAttr arch_c_attr =
      mlir::ArchitectureConfig::ArchitectureConfigAttr::get(
          npu_clock, no_cores, accel_config, system_config, shared_buffer_area,
          allocation_alignement, shram_bank_size, shram_size_bytes,
          shram_reserved_output_banks, shram_reserved_weight_banks,
          shram_reserved_unused_banks, shram_total_banks, shram_lut_size,
          shram_lut_address, feature_map_storage_mem_area,
          permanent_storage_mem_area, fast_storage_mem_area, sram_size,
          OFMSplitDepth, name, global_memory_clock_scale,
          memory_clock_scales_Sram, memory_clock_Sram, memory_port_widths_Sram,
          memory_bandwidths_per_cycle_Sram, memory_bandwidths_per_second_Sram,
          memory_clock_scales_OnChipFlash, memory_clock_OnChipFlash,
          memory_port_widths_OnChipFlash,
          memory_bandwidths_per_cycle_OnChipFlash,
          memory_bandwidths_per_second_OnChipFlash,
          memory_clock_scales_OffChipFlash, memory_clock_OffChipFlash,
          memory_port_widths_OffChipFlash,
          memory_bandwidths_per_cycle_OffChipFlash,
          memory_bandwidths_per_second_OffChipFlash, memory_clock_scales_Dram,
          memory_clock_Dram, memory_port_widths_Dram,
          memory_bandwidths_per_cycle_Dram, memory_bandwidths_per_second_Dram,
          subkernel_max, builder.getContext());

  return arch_c_attr;
}

void AddArchitectureConfigAttribute_to_NpuRegions(
    mlir::ArchitectureConfig::ArchitectureConfigAttr &arch_config,
    mlir::FuncOp function, mlir::MLIRContext *context) {

  Region *region = function.getCallableRegion();
  llvm::iplist<Block> &blocks = region->getBlocks();
  if (blocks.size() > 1)
    FATAL_ERROR("AddArchitectureConfigAttribute_to_NpuRegions::More than a "
                "single block in main function callable region")
  Block &block = region->front();
  llvm::iplist<Operation>::iterator i;
  for (i = block.begin(); i != block.end(); i++) {
    Operation *op = &(*i);
    if (llvm::isa<scheduleir::NpuRegionOp>(op))
      op->setAttr("architecture_config", arch_config);
  }
}

} // namespace utils
} // namespace mlir

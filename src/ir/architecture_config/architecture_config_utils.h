/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#ifndef ARCHITECTURE_CONFIG_UTILS_H
#define ARCHITECTURE_CONFIG_UTILS_H

#include <iostream>
#include <stdint.h>
#include <string>

#include "mlir/IR/AffineMap.h"  // from @llvm-project
#include "mlir/IR/Attributes.h" // from @llvm-project
#include "mlir/IR/Builders.h"
#include "mlir/IR/BuiltinAttributes.h"
#include "mlir/IR/BuiltinOps.h"
#include "mlir/IR/Location.h"    // from @llvm-project
#include "mlir/IR/MLIRContext.h" // from @llvm-project

#include "src/ir/architecture_config/architecture_config.h"
#include "src/utils/data_structure_utils.h"
#include "src/utils/numeric_utils.h"

#include "src/ir/schedule_ir.h"

namespace mlir {
namespace utils {

mlir::ArchitectureConfig::ArchitectureConfigAttr
AccessArchitectureConfigAttribute_From_Module(mlir::OwningModuleRef &module);
mlir::ArchitectureConfig::ArchitectureConfigAttr
AccessArchitectureConfigAttribute_From_NpuRegion(Operation *NpuRegion);

mlir::ArchitectureConfig::ArchitectureConfigAttr
CreateArchitectureConfigAttribute(MLIRContext *context, int32_t sram_size);
void AddArchitectureConfigAttribute_to_NpuRegions(
    mlir::ArchitectureConfig::ArchitectureConfigAttr &arch_config,
    mlir::FuncOp function, mlir::MLIRContext *context);

} // namespace utils
} // namespace mlir

#endif // ARCHITECTURE_CONFIG_UTILS_H

/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#include "src/ir/cascade_ir.h"

using namespace mlir;
using namespace mlir::cascadeir;
using namespace mlir::TFL;

namespace cl = llvm::cl;

CascadeIRDialect::CascadeIRDialect(MLIRContext *context)
    : Dialect(getDialectNamespace(), context, TypeID::get<CascadeIRDialect>()) {
  addOperations<
#define GET_OP_LIST
#include "src/ir/cascade_ir.cc.inc"
      >();
}

#define GET_OP_CLASSES
#include "src/ir/cascade_ir.cc.inc"

namespace mlir {
namespace cascadeir {

SmallVector<Value, 1> ExtractCascadeInputValues(Operation *op) {
  SmallVector<Value, 1> inputs;
  Region &region_instruction = op->getRegion(0);
  Block &block_cascade_region = region_instruction.front();
  for (unsigned b = 0; b < block_cascade_region.getNumArguments(); b++)
    inputs.push_back(block_cascade_region.getArgument(b));
  return inputs;
}

SmallVector<Value, 1> ExtractCascadeOutputValues(Operation *op) {
  SmallVector<Value, 1> outputs;
  for (unsigned b = 0; b < op->getNumResults(); b++)
    outputs.push_back(op->getResult(b));
  return outputs;
}

SmallVector<Value, 1> ExtractCascadeIntermediateValues(Operation *op) {
  SmallVector<Value, 1> intermediates;
  Region &region_instruction = op->getRegion(0);
  Block &block_cascade_region = region_instruction.front();
  llvm::iplist<Operation> &cascade_operations =
      block_cascade_region.getOperations();
  for (auto &cascade_operation : cascade_operations) {
    if (llvm::isa<kernelir::KernelRegionOp>(cascade_operation)) {
      // 0. Kernel Outputs (Unless those going to a Yield as these get registerd
      // in ExtractCascadeOutputValues)
      //    Not that kernel output tensors may vave an invalid address assigned
      //    as memory organization works at the cascade level thorugh these
      //    interfaces i.e ExtractAllCascadeValues
      SmallVector<Value, 1> kernel_outputs =
          mlir::kernelir::ExtractKernelOutputValues(&cascade_operation);
      for (auto v : kernel_outputs) {
        ASSERT_COND(v.hasOneUse() == false,
                    " ExtractCascadeIntermediateValues: An Intermediate Tensor "
                    "inside a Cascade has multiple Users: This is not "
                    "supported until we have meshed cascades !!!");
        // If a Value is consumed by the Yield than do not add
        bool not_Yield = true;
        for (Operation *consumer : v.getUsers()) {
          if (llvm::isa<scheduleir::YieldOp>(consumer))
            not_Yield = false;
        }
        if (not_Yield)
          intermediates.push_back(v);
      }

      // 1. Kernel Intermediates (Weights/Biases in Primary Storage e.g SRAM)
      SmallVector<Value, 1> kernel_intermediates =
          mlir::kernelir::ExtractKernelIntermediateValues(&cascade_operation);
      for (auto v : kernel_intermediates)
        intermediates.push_back(v);
    }
  }
  return intermediates;
}

SmallVector<Value, 1> ExtractCascadeWeightValues(Operation *op) {
  // 0. Kernel Weights (2nd Storage)
  SmallVector<Value, 1> weights;
  Region &region_instruction = op->getRegion(0);
  Block &block_cascade_region = region_instruction.front();
  llvm::iplist<Operation> &cascade_operations =
      block_cascade_region.getOperations();
  for (auto &cascade_operation : cascade_operations) {
    if (llvm::isa<kernelir::KernelRegionOp>(cascade_operation)) {
      SmallVector<Value, 1> kernel_weights =
          mlir::kernelir::ExtractKernelWeightValues(&cascade_operation);
      for (auto v : kernel_weights)
        weights.push_back(v);
    }
  }
  return weights;
}

SmallVector<Value, 1> ExtractCascadeBiasValues(Operation *op) {
  // 0. Kernel Weights (2nd Storage)
  SmallVector<Value, 1> bias;
  Region &region_instruction = op->getRegion(0);
  Block &block_cascade_region = region_instruction.front();
  llvm::iplist<Operation> &cascade_operations =
      block_cascade_region.getOperations();
  for (auto &cascade_operation : cascade_operations) {
    if (llvm::isa<kernelir::KernelRegionOp>(cascade_operation)) {
      SmallVector<Value, 1> kernel_weights =
          mlir::kernelir::ExtractKernelBiasValues(&cascade_operation);
      for (auto v : kernel_weights)
        bias.push_back(v);
    }
  }
  return bias;
}

SmallVector<Value, 1> ExtractAllCascadeValues(Operation *op) {
  SmallVector<Value, 1> inputs = ExtractCascadeInputValues(op);
  SmallVector<Value, 1> weights = ExtractCascadeWeightValues(op);
  SmallVector<Value, 1> bias = ExtractCascadeBiasValues(op);
  SmallVector<Value, 1> intermediate = ExtractCascadeIntermediateValues(op);
  SmallVector<Value, 1> outputs = ExtractCascadeOutputValues(op);
  SmallVector<Value, 1> Cascade_values;
  for (auto v : inputs)
    Cascade_values.push_back(v);
  for (auto v : weights)
    Cascade_values.push_back(v);
  for (auto v : bias)
    Cascade_values.push_back(v);
  for (auto v : intermediate)
    Cascade_values.push_back(v);
  for (auto v : outputs)
    Cascade_values.push_back(v);
  return Cascade_values;
}

SmallVector<Operation *> ExtractCascadeKernels(cascadeir::CascadeRegionOp *op) {
  SmallVector<Operation *> kernels;
  Region &region_instruction = op->getRegion();
  Block &block_cascade_region = region_instruction.front();
  llvm::iplist<Operation> &cascade_operations =
      block_cascade_region.getOperations();
  for (auto &cascade_operation : cascade_operations) {
    if (llvm::isa<kernelir::KernelRegionOp>(cascade_operation)) {
      kernels.push_back(&cascade_operation);
    }
  }
  return kernels;
}

} // namespace cascadeir
} // end namespace mlir

#include "src/ir/cascade_ir_enum.cc.inc"

/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#ifndef CASCADE_IR_H
#define CASCADE_IR_H

#include <unordered_map>

#include "mlir/Dialect/Traits.h"                  // TF:local_config_mlir
#include "mlir/IR/AffineMap.h"                    // from @llvm-project
#include "mlir/IR/Attributes.h"                   // from @llvm-project
#include "mlir/IR/Attributes.h"                   // TF:local_config_mlir
#include "mlir/IR/BlockAndValueMapping.h"         // from @llvm-project
#include "mlir/IR/Builders.h"                     // from @llvm-project
#include "mlir/IR/Builders.h"                     // TF:local_config_mlir
#include "mlir/IR/Diagnostics.h"                  // TF:local_config_mlir
#include "mlir/IR/Location.h"                     // from @llvm-project
#include "mlir/IR/MLIRContext.h"                  // from @llvm-project
#include "mlir/IR/MLIRContext.h"                  // TF:local_config_mlir
#include "mlir/IR/Matchers.h"                     // TF:local_config_mlir
#include "mlir/IR/OpImplementation.h"             // TF:local_config_mlir
#include "mlir/IR/Operation.h"                    // from @llvm-project
#include "mlir/IR/PatternMatch.h"                 // from @llvm-project
#include "mlir/IR/PatternMatch.h"                 // TF:local_config_mlir
#include "mlir/IR/TypeUtilities.h"                // TF:local_config_mlir
#include "mlir/IR/Types.h"                        // TF:local_config_mlir
#include "mlir/IR/Value.h"                        // TF:local_config_mlir
#include "mlir/Interfaces/InferTypeOpInterface.h" // from @llvm-project
#include "mlir/Parser.h"                          // TF:local_config_mlir
#include "mlir/Pass/Pass.h"                       // from @llvm-project
#include "mlir/Support/LLVM.h"                    // TF:local_config_mlir
#include "mlir/Support/LogicalResult.h"           // TF:local_config_mlir
#include "mlir/Transforms/DialectConversion.h"    // from @llvm-project
#include "mlir/Transforms/FoldUtils.h"            // TF:local_config_mlir
#include "mlir/Transforms/InliningUtils.h"        // TF:local_config_mlir
#include "mlir/Transforms/RegionUtils.h"          // TF:local_config_mlir
#include "llvm/ADT/Sequence.h"
#include "llvm/ADT/SmallVector.h"
#include "llvm/ADT/StringExtras.h"
#include "llvm/ADT/StringRef.h"
#include "llvm/ADT/StringSwitch.h"
#include "llvm/ADT/iterator_range.h"
#include "llvm/Support/FormatVariadic.h"

#include "src/ir/schedule_ir.h"

#include "src/ir/cascade_ir_enum.h.inc"
#include "src/ir/cascade_ir_structs.h.inc"

#include "src/utils/assert.h"

#include "tensorflow/compiler/mlir/lite/ir/tfl_ops.h"

#include <algorithm>
#include <cstdint>
#include <fstream>
#include <functional>
#include <iomanip>
#include <iostream>
#include <numeric>
#include <string>
#include <type_traits>

#include "src/ir/kernel_ir.h"

namespace mlir {
namespace cascadeir {

class CascadeIRDialect : public Dialect {
public:
  explicit CascadeIRDialect(MLIRContext *context);

  static StringRef getDialectNamespace() { return "cascadeir"; }
};

SmallVector<Value, 1> ExtractAllCascadeValues(Operation *op);

SmallVector<Value, 1> ExtractCascadeWeightValues(Operation *op);
SmallVector<Value, 1> ExtractCascadeBiasValues(Operation *op);
SmallVector<Value, 1> ExtractCascadeIntermediateValues(
    Operation *op); // Returns the SRAM Weights, SRAM Bias, Internal Cascade FM
SmallVector<Value, 1> ExtractCascadeInputValues(Operation *op);
SmallVector<Value, 1> ExtractCascadeOutputValues(Operation *op);

} // namespace cascadeir
} // end namespace mlir

#define GET_OP_CLASSES
#include "src/ir/cascade_ir.h.inc"

namespace mlir {
namespace cascadeir {

SmallVector<Operation *>
ExtractCascadeKernels(cascadeir::CascadeRegionOp *cascade);

} // namespace cascadeir
} // end namespace mlir

#endif // CASCADE_IR_H

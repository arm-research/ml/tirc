/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#include "src/ir/compiler_config/compiler_config.h"
#include "src/ir/compiler_config/compiler_config_attr.cc.inc"
#include "src/ir/schedule_ir.h"

namespace mlir {
namespace utils {

mlir::CompilerConfig::CompilerConfigAttr
AccessCompilerConfigAttribute_From_NpuRegion(Operation *NpuRegion) {
  if (not(llvm::isa<scheduleir::NpuRegionOp>(NpuRegion)))
    FATAL_ERROR("AccessCompilerConfigAttribute_From_NpuRegion::Operation must "
                "be a scheduleir::NpuRegionOp")
  StringRef attr("compiler_config");
  return NpuRegion->getAttr(attr)
      .cast<mlir::CompilerConfig::CompilerConfigAttr>();
}

mlir::CompilerConfig::CompilerConfigAttr
CreateCompilerConfigAttribute(MLIRContext *context,
                              mlir::utils::compiler_settings &comp_config) {
  ::mlir::OpBuilder builder(context);

  ::mlir::BoolAttr cascade = builder.getBoolAttr(comp_config.disable_cascading);
  ::mlir::BoolAttr weight_streaming =
      builder.getBoolAttr(comp_config.disable_weight_streaming);
  ::mlir::BoolAttr stationary =
      builder.getBoolAttr(comp_config.disable_stationary);

  ::mlir::BoolAttr block_config_search =
      builder.getBoolAttr(comp_config.disable_block_config_search);
  SmallVector<int32_t, 3> cbc = {
      comp_config.custom_block_struct[0], comp_config.custom_block_struct[1],
      comp_config.custom_block_struct[2], comp_config.custom_block_struct[3]};
  ::mlir::ArrayAttr block_config = builder.getI32ArrayAttr(cbc);

  mlir::CompilerConfig::CompilerConfigAttr compiler_c_attr =
      mlir::CompilerConfig::CompilerConfigAttr::get(
          cascade, weight_streaming, stationary, block_config_search,
          block_config, builder.getContext());
  return compiler_c_attr;
}

void AddCompilerConfigAttribute_to_NpuRegions(
    mlir::CompilerConfig::CompilerConfigAttr &compiler_config,
    mlir::FuncOp function, mlir::MLIRContext *context) {
  Region *region = function.getCallableRegion();
  llvm::iplist<Block> &blocks = region->getBlocks();
  if (blocks.size() > 1)
    FATAL_ERROR("AddCompilerConfigAttribute_to_NpuRegions:::More than a single "
                "block in main function callable region")
  Block &block = region->front();
  llvm::iplist<Operation>::iterator i;
  for (i = block.begin(); i != block.end(); i++) {
    Operation *op = &(*i);
    if (llvm::isa<scheduleir::NpuRegionOp>(op))
      op->setAttr("compiler_config", compiler_config);
  }
}

} // namespace utils
} // namespace mlir

/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#ifndef COMPILER_CONFIG_H
#define COMPILER_CONFIG_H

#include <iostream>
#include <string>

#include "mlir/IR/AffineMap.h"  // from @llvm-project
#include "mlir/IR/Attributes.h" // from @llvm-project
#include "mlir/IR/BuiltinAttributes.h"
#include "mlir/IR/Location.h"    // from @llvm-project
#include "mlir/IR/MLIRContext.h" // from @llvm-project

#include "src/ir/architecture_config/architecture_config.h"
#include "src/ir/compiler_config/compiler_config_attr.h.inc"

namespace mlir {
namespace utils {

#define LOGGING_ENABLED 1
#define ASSERTS_ENABLED 1

#define ENABLE_LOGGING_ACTIVE_LEVEL 0

#define ENABLE_LOGGING_SEARCH_ENGINE_VERBOSE_LEVEL_1 1
#define ENABLE_LOGGING_SEARCH_ENGINE_VERBOSE_LEVEL_2 2

#define ENABLE_LOGGING_MEMORY_ORGANIZER_VERBOSE_LEVEL_1 1
#define ENABLE_LOGGING_MEMORY_ORGANIZER_VERBOSE_LEVEL_2 2

#define ENABLE_LOGGING_HLCS_GENERATOR_VERBOSE_LEVEL_1 1
#define ENABLE_LOGGING_HLCS_GENERATOR_VERBOSE_LEVEL_2 2

struct compiler_settings {
  NpuBlockConfig custom_block_struct;
  bool disable_cascading;
  bool disable_weight_streaming;
  bool disable_stationary;
  bool disable_block_config_search;
};

mlir::CompilerConfig::CompilerConfigAttr
AccessCompilerConfigAttribute_From_NpuRegion(Operation *NpuRegion);

mlir::CompilerConfig::CompilerConfigAttr
CreateCompilerConfigAttribute(MLIRContext *context,
                              mlir::utils::compiler_settings &comp_config);
void AddCompilerConfigAttribute_to_NpuRegions(
    mlir::CompilerConfig::CompilerConfigAttr &compiler_config,
    mlir::FuncOp function, mlir::MLIRContext *context);

} // namespace utils
} // namespace mlir

#endif // COMPILER_CONFIG_H

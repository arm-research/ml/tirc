/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

// This is the Compiler Config Dialect operator set

#ifdef COMPILER_CONFIG_OPS
#else
#define COMPILER_CONFIG_OPS

include "mlir/IR/OpBase.td"

def CompilerConfig_Dialect : Dialect {
  let name = "CompilerConfig";

  let description = [{
    Compiler Config.
  }];

  let cppNamespace = "mlir::CompilerConfig";
}

def CompilerConfigAttr : StructAttr<"CompilerConfigAttr",  CompilerConfig_Dialect, [
                StructFieldAttr<"disable_cascade",                BoolAttr>,

                StructFieldAttr<"disable_weight_streaming",       BoolAttr>,
                StructFieldAttr<"disable_stationary",             BoolAttr>,

                StructFieldAttr<"disable_block_config_search",    BoolAttr>,
                StructFieldAttr<"custom_block_config",           I32ArrayAttr>
                ] > {
}

#endif // COMPILER_CONFIG_OPS

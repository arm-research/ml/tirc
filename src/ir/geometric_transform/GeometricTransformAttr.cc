/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#include "src/ir/geometric_transform/operation_transform.h"

using namespace operation_transform;

namespace mlir {

namespace detail {
struct AffineTransformAttrStorage : public ::mlir::AttributeStorage {
  AffineTransformAttrStorage(attr_unique_id unique_id) : unique_id(unique_id) {}

  /// The hash key is a tuple of the parameter types.
  using KeyTy = std::tuple<attr_unique_id>;
  bool operator==(const KeyTy &key) const {
    if (!(unique_id == std::get<0>(key)))
      return false;
    return true;
  }
  static ::llvm::hash_code hashKey(const KeyTy &key) {
    return ::llvm::hash_combine(std::get<0>(key));
  }

  /// Define a construction method for creating a new instance of this storage.
  static AffineTransformAttrStorage *
  construct(::mlir::AttributeStorageAllocator &allocator, const KeyTy &key) {
    auto unique_id = std::get<0>(key);
    return new (allocator.allocate<AffineTransformAttrStorage>())
        AffineTransformAttrStorage(unique_id);
  }

  attr_unique_id getUniqueId() const { return unique_id; }

  std::shared_ptr<OperationTransform> getTranforms() const {
    return transforms;
  }

  void setTranforms(std::shared_ptr<OperationTransform> transforms) {
    this->transforms = transforms;
  }

  attr_unique_id unique_id;
  std::shared_ptr<OperationTransform> transforms;
};
} // namespace detail

static OTBoundingBox add_dim(const OTBoundingBox &bb, size_t axis,
                             int64_t low_val, int64_t high_val) {
  size_t len = bb.shape(1);
  OTBoundingBox res = xt::zeros<int64_t>({size_t(2), len + 1});
  xt::view(res, xt::all(), xt::range(0, axis)) =
      xt::view(bb, xt::all(), xt::range(0, axis));
  res(0, axis) = low_val;
  res(1, axis) = high_val;
  xt::view(res, xt::all(), xt::range(axis + 1, len + 1)) =
      xt::view(bb, xt::all(), xt::range(axis, len));
  return res;
}

static void setup_ifm_filter(
    OpToTensorTransform &tr,
    const xt::xtensor_fixed<int64_t, xt::xshape<4>> &kernel_size,
    const xt::xtensor_fixed<int64_t, xt::xshape<4>> &strides,
    const xt::xtensor_fixed<int64_t, xt::xshape<4>> &dilation,
    const xt::xtensor_fixed<int64_t, xt::xshape<4>> &explicit_padding) {

  tr.transform.multiply(strides);
  xtivec dilated_filter_size_m1 = (kernel_size - 1) * dilation;
  xtivec dilated_filter_size = dilated_filter_size_m1 + 1;

  xtivec top_left_pad = xt::zeros<int64_t>({4});
  top_left_pad[1] = explicit_padding[0];
  top_left_pad[2] = explicit_padding[1];

  tr.transform.remove_dim(
      4, 0, kernel_size[1] * kernel_size[2] - 1); /* ditch the kernel xy */
  tr.transform.expand_box(xt::zeros<int64_t>({4}), dilated_filter_size_m1);
  tr.transform.translate(-top_left_pad);
}

std::shared_ptr<OperationTransform>
generate_conv_transform(Operation *op, bool depthwise = false) {
  auto KernelFilter = mlir::kernelir::getKernelFilter(op);

  xt::xtensor_fixed<int64_t, xt::xshape<4>> kernel_size =
      xt::ones<int64_t>({4});
  kernel_size(1) = KernelFilter.height;
  kernel_size(2) = KernelFilter.width;
  xt::xtensor_fixed<int64_t, xt::xshape<4>> strides = xt::ones<int64_t>({4});
  strides(1) = KernelFilter.stride.y;
  strides(2) = KernelFilter.stride.x;
  xt::xtensor_fixed<int64_t, xt::xshape<4>> dilation = xt::ones<int64_t>({4});
  dilation(1) = KernelFilter.dilation.y;
  dilation(2) = KernelFilter.dilation.x;
  xt::xtensor_fixed<int64_t, xt::xshape<4>> explicit_padding =
      xt::ones<int64_t>({4});
  explicit_padding(0) = KernelFilter.padding.top;
  explicit_padding(1) = KernelFilter.padding.left;

  int64_t ifm_depth = op->getOperand(0)
                          .getType()
                          .dyn_cast<mlir::SchedTensorType>()
                          .getShape()[3];

  /* Operand Space N,H,W,CO,CI,Kx,Ky*/
  OTBoundingBox op_box = box_from_shape(
      op->getOperand(0).getType().dyn_cast<mlir::SchedTensorType>().getShape());
  op_box =
      add_dim(op_box, 4, 0, ifm_depth - 1); /* add input channels dimension*/
  op_box =
      add_dim(op_box, 5, 0,
              kernel_size(1) * kernel_size(2) - 1); /* add kernel xy dimension*/
  std::shared_ptr<OperationTransform> op_tr =
      std::make_shared<OperationTransform>(op_box);

  for (auto t : op->getOperands()) {
    OpToTensorTransform tr(
        op_box, box_from_shape(
                    t.getType().dyn_cast<mlir::SchedTensorType>().getShape()));
    tr.transform.remove_dim(3,
                            op_box); /* ditch the output channels dimension */
    setup_ifm_filter(tr, kernel_size, strides, dilation, explicit_padding);
    op_tr->push_back_input_transform(tr);
  }

  for (auto t : op->getResults()) {
    OpToTensorTransform tr(
        op_box, box_from_shape(
                    t.getType().dyn_cast<mlir::SchedTensorType>().getShape()));
    tr.transform.remove_dim(5, op_box); /* ditch the kernel xy dimension */
    tr.transform.remove_dim(4, op_box); /* ditch the input channels dimension */
    op_tr->push_back_output_transform(tr);
  }

  {
    SmallVector<Value, 1> weights = ExtractKernelWeightValues(op);
    auto weight_shape =
        weights[0].getType().dyn_cast<mlir::SchedTensorType>().getShape();
    OTBoundingBox weight_box = box_from_shape(weight_shape);
    OpToTensorTransform tr(op_box, weight_box);
    tr.transform.split_dim(
        5,
        weight_shape[1]); /* split kernel xy into kernel height, kernel width */
    tr.transform.remove_dim(2, op_box); /* output width */
    tr.transform.remove_dim(1, op_box); /* output height */
    tr.transform.remove_dim(0, op_box); /* batch */
    tr.transform.transpose(0,
                           1); /* switch output channel/input channel order */
    tr.transform.transpose(0, 2); /* switch input channel, kernel height */
    tr.transform.transpose(1, 3); /* switch output channel, kernel width */
    op_tr->push_back_const_transform(tr);
  }

  {
    SmallVector<Value, 1> biases = ExtractKernelBiasValues(op);
    auto bias_shape =
        biases[0].getType().dyn_cast<mlir::SchedTensorType>().getShape();
    OpToTensorTransform tr(op_box, box_from_shape(bias_shape));
    /* ditch all but the output channels */
    tr.transform.remove_dim(5, op_box);
    tr.transform.remove_dim(4, op_box);
    tr.transform.remove_dim(2, op_box);
    tr.transform.remove_dim(1, op_box);
    tr.transform.remove_dim(0, op_box);
    op_tr->push_back_input_transform(tr);
  }

  return op_tr;
}

std::shared_ptr<OperationTransform>
generate_elementwise_transform(Operation *op) {
  OTBoundingBox op_box = box_from_shape(
      op->getOperand(0).getType().dyn_cast<mlir::SchedTensorType>().getShape());
  std::shared_ptr<OperationTransform> op_tr =
      std::make_shared<OperationTransform>(op_box);

  for (auto t : op->getOperands()) {
    /* just identity */
    op_tr->push_back_output_transform(OpToTensorTransform(op_box, op_box));
  }

  for (auto t : op->getResults()) {
    /* just identity */
    op_tr->push_back_output_transform(OpToTensorTransform(op_box, op_box));
  }
  return op_tr;
}

std::shared_ptr<OperationTransform> generate_pooling_transform(Operation *op) {
  auto KernelFilter = mlir::kernelir::getKernelFilter(op);

  xt::xtensor_fixed<int64_t, xt::xshape<4>> kernel_size =
      xt::ones<int64_t>({4});
  kernel_size(1) = KernelFilter.height;
  kernel_size(2) = KernelFilter.width;
  xt::xtensor_fixed<int64_t, xt::xshape<4>> strides = xt::ones<int64_t>({4});
  strides(1) = KernelFilter.stride.y;
  strides(2) = KernelFilter.stride.x;
  xt::xtensor_fixed<int64_t, xt::xshape<4>> dilation = xt::ones<int64_t>({4});
  xt::xtensor_fixed<int64_t, xt::xshape<4>> explicit_padding =
      xt::ones<int64_t>({4});
  explicit_padding(0) = KernelFilter.padding.top;
  explicit_padding(1) = KernelFilter.padding.left;

  int64_t ifm_depth = op->getOperand(0)
                          .getType()
                          .dyn_cast<mlir::SchedTensorType>()
                          .getShape()[3];

  /* Operand Space N,H,W,CO,CI,Kx,Ky*/
  OTBoundingBox op_box = box_from_shape(
      op->getOperand(0).getType().dyn_cast<mlir::SchedTensorType>().getShape());
  op_box =
      add_dim(op_box, 4, 0, ifm_depth - 1); /* add input channels dimension*/
  op_box =
      add_dim(op_box, 5, 0,
              kernel_size(1) * kernel_size(2) - 1); /* add kernel xy dimension*/
  std::shared_ptr<OperationTransform> op_tr =
      std::make_shared<OperationTransform>(op_box);

  for (auto t : op->getOperands()) {
    OpToTensorTransform tr(
        op_box, box_from_shape(
                    t.getType().dyn_cast<mlir::SchedTensorType>().getShape()));
    tr.transform.remove_dim(3,
                            op_box); /* ditch the output channels dimension */
    setup_ifm_filter(tr, kernel_size, strides, dilation, explicit_padding);
    op_tr->push_back_input_transform(tr);
  }

  for (auto t : op->getResults()) {
    OpToTensorTransform tr(
        op_box, box_from_shape(
                    t.getType().dyn_cast<mlir::SchedTensorType>().getShape()));
    tr.transform.remove_dim(5, op_box); /* ditch the kernel xy dimension */
    tr.transform.remove_dim(4, op_box); /* ditch the input channels dimension */
    op_tr->push_back_output_transform(tr);
  }
  return op_tr;
}

std::shared_ptr<OperationTransform> generate_memory_transform(Operation *op) {
  OTBoundingBox op_box = box_from_shape(
      op->getOperand(0).getType().dyn_cast<mlir::SchedTensorType>().getShape());
  std::shared_ptr<OperationTransform> op_tr =
      std::make_shared<OperationTransform>(op_box);

  for (auto t : op->getOperands()) {
    /* just identity */
    op_tr->push_back_output_transform(OpToTensorTransform(op_box, op_box));
  }

  for (auto t : op->getResults()) {
    /* just identity */
    op_tr->push_back_output_transform(OpToTensorTransform(op_box, op_box));
  }
  return op_tr;
}

AffineTransformAttr AffineTransformAttr::get(Operation *kernel_op,
                                             attr_unique_id unique_id) {
  // For each Kernel based on the Major Op calculate the Geometric Transform
  Operation *kernel_major_op = getKernelMajorOp(kernel_op);

  std::shared_ptr<OperationTransform> tr;
  if (llvm::isa<scheduleir::Conv2DOp>(kernel_major_op))
    tr = generate_conv_transform(kernel_op);
  else if (llvm::isa<scheduleir::DepthwiseConv2DOp>(kernel_major_op))
    tr = generate_conv_transform(kernel_op, true);
  else if (llvm::isa<scheduleir::AddOp>(kernel_major_op))
    tr = generate_elementwise_transform(kernel_op);
  else if (llvm::isa<scheduleir::MulOp>(kernel_major_op))
    tr = generate_elementwise_transform(kernel_op);

  else if ((llvm::isa<scheduleir::MaxPool2dOp>(kernel_major_op)) or
           (llvm::isa<scheduleir::AvgPool2dOp>(kernel_major_op)))
    tr = generate_pooling_transform(kernel_op);
  else if (llvm::isa<scheduleir::MemoryOp>(kernel_major_op))
    tr = generate_memory_transform(kernel_op);
  else
    FATAL_ERROR(
        "AffineTransformAttr::get::Fatal Error unknown Kernel Major Op");

  auto attr = Base::get(kernel_op->getContext(), unique_id);
  attr.getImpl()->setTranforms(tr);
  return attr;
}

attr_unique_id AffineTransformAttr::getUniqueId() const {
  return getImpl()->getUniqueId();
}

} // namespace mlir

/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#ifndef GEOMETRIC_TRANSFORM_ATTR_H
#define GEOMETRIC_TRANSFORM_ATTR_H

namespace mlir {
class DialectAsmParser;
class DialectAsmPrinter;
} // namespace mlir

using attr_unique_id = uint64_t;

namespace mlir {
class AffineTransformAttr;

namespace detail {
struct AffineTransformAttrStorage;
} // end namespace detail

class AffineTransformAttr
    : public ::mlir::Attribute::AttrBase<AffineTransformAttr, ::mlir::Attribute,
                                         detail::AffineTransformAttrStorage> {
public:
  /// Inherit some necessary constructors from 'AttrBase'.
  using Base::Base;

  static AffineTransformAttr get(Operation *kernel_op,
                                 attr_unique_id unique_id);
  attr_unique_id getUniqueId() const;
};

} // namespace mlir

#endif // GEOMETRIC_TRANSFORM_ATTR_H

/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#include "src/ir/geometric_transform/bounding_box.h"

namespace mlir {
namespace planner {

ostream &operator<<(ostream &os, const BoundingBox &b) {
  os << "BoundingBox-> Start Coord: " << b.get_start_coord()
     << " End Coord: " << b.get_end_coord() << std::endl;
  return os;
}

} // namespace planner
} // namespace mlir

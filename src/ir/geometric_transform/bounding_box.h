/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#ifndef BOUNDING_BOX_H
#define BOUNDING_BOX_H

#include <iostream>
#include <unordered_map>
#include <vector>

#include "src/ir/architecture_config/architecture_config.h"

#include "src/utils/assert.h"
#include "src/utils/ir_utils.h"

#include "src/ir/geometric_transform/coordinate.h"

using namespace std;

namespace mlir {
namespace planner {

class BoundingBox {
public:
  BoundingBox() {}

  BoundingBox(coordinate start_coord, coordinate end_coord) {
    this->start_coord = start_coord;
    this->end_coord = end_coord;
    // std::cout << start_coord << " " << end_coord << std::endl;
    ASSERT_COND(this->start_coord.size() != this->end_coord.size(),
                "BoundingBox::Initializer::Start Coord Size != End Coord Size");
    for (int idx = 0; idx < this->start_coord.size(); idx++)
      ASSERT_COND(this->start_coord[idx] > this->end_coord[idx],
                  "BoundingBox::Initializer::StartCoord[i] > EndCoord[i]");
  }

  void clamp(llvm::ArrayRef<int64_t> shape) {
    ASSERT_COND(shape.size() != this->end_coord.size(),
                "BoundingBox::clamp::shapes do not match");
    for (int i = 0; i < start_coord.size(); i++) {
      if (start_coord[i] < 0)
        start_coord[i] = 0;
      if (end_coord[i] < 0)
        end_coord[i] = 0;
      if (start_coord[i] >= shape[i])
        start_coord[i] = shape[i];
      if (end_coord[i] >= shape[i])
        end_coord[i] = shape[i];
    }
  }

  coordinate get_start_coord() const { return this->start_coord; }
  coordinate get_end_coord() const { return this->end_coord; }

private:
  coordinate start_coord;
  coordinate end_coord;
};

ostream &operator<<(ostream &os, const BoundingBox &b);

} // namespace planner
} // namespace mlir

#endif // BOUNDING_BOX_H

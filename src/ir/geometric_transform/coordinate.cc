/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#include "src/ir/geometric_transform/coordinate.h"

namespace mlir {
namespace planner {

using coordinate = llvm::SmallVector<int64_t, 1>;

ostream &operator<<(ostream &os, const coordinate &c) {
  os << "[";
  for (int idx = 0; idx < c.size(); idx++)
    os << c[idx] << ",";
  os << "]";
  return os;
}

void initialize_coordinate(coordinate &c, uint64_t value, uint64_t len) {
  for (int i = 0; i < len; i++)
    c.push_back(value);
}

void load_coordinate(coordinate &c, ArrayRef<int64_t> data) {
  for (int i = 0; i < data.size(); i++)
    c.push_back(data[i]);
}

} // namespace planner
} // namespace mlir

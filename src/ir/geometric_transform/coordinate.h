/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#ifndef COORDINATE_H
#define COORDINATE_H

#include <iostream>
#include <unordered_map>
#include <vector>

#include "src/ir/architecture_config/architecture_config.h"

#include "src/utils/assert.h"
#include "src/utils/ir_utils.h"

#include <absl/container/inlined_vector.h>

#include <xtensor/xadapt.hpp>
#include <xtensor/xarray.hpp>
#include <xtensor/xio.hpp>
#include <xtensor/xmanipulation.hpp>
#include <xtensor/xnorm.hpp>
#include <xtensor/xpad.hpp>
#include <xtensor/xview.hpp>

using namespace std;

#if 1
template <class C, std::size_t NDims, std::size_t NPreallocElems>
using xtensor_prealloc =
    xt::xtensor_container<absl::InlinedVector<C, NPreallocElems>, NDims>;
#else
template <class C, std::size_t NDims, std::size_t NPreallocElems>
using xtensor_prealloc = xt::xtensor<C, NDims>;
#endif

namespace mlir {
namespace planner {

using coordinate = llvm::SmallVector<int64_t, 1>;

ostream &operator<<(ostream &os, const coordinate &c);
void initialize_coordinate(coordinate &c, uint64_t value, uint64_t len);
void load_coordinate(coordinate &c, ArrayRef<int64_t> data);

} // namespace planner
} // namespace mlir

#endif // COORDINATE_H

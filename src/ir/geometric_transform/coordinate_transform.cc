/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#include "src/ir/geometric_transform/coordinate_transform.h"

namespace mlir {
namespace coordinate_transform {
xtboximatrix matmul(const xtboximatrix &a, const xtboximatrix &b) {
  auto a_ext = xt::view(a, xt::all(), xt::all(), xt::all(), xt::newaxis());
  auto b_ext = xt::view(b, xt::all(), xt::newaxis(), xt::all(), xt::all());

  auto c = a_ext * b_ext;
  auto res = xt::sum(c, {2});
  return res;
}

xtboxivec matmul(const xtboximatrix &a, const xtboxivec &b) {
  xtboximatrix b_ext = xt::view(b, xt::all(), xt::all(), xt::newaxis());
  xtboximatrix res = matmul(a, b_ext);
  return xt::squeeze(res, 2);
}

xtboxivec matmul(const xtboxivec &a, const xtboximatrix &b) {
  xtboximatrix a_ext = xt::view(a, xt::newaxis(), xt::all());
  xtboximatrix res = matmul(a_ext, b);
  return xt::squeeze(res, 1);
}

std::ostream &operator<<(std::ostream &out, TransformKind kind) {
  switch (kind) {
  case TransformKind::Affine:
    out << "affine";
    break;
  case TransformKind::IntDiv:
    out << "int_div";
    break;
  case TransformKind::SplitDim:
    out << "split_dim";
    break;
  case TransformKind::CombineDims:
    out << "combine_dims";
    break;
  }
  return out;
}

TransformElem::TransformElem(xtboximatrix mat)
    : kind(TransformKind::Affine), matrix(std::move(mat)), axis(0), split(0) {}

TransformElem TransformElem::identity(size_t n_dims) {
  xtboxivec eye = xt::eye(n_dims + 1);
  xtboximatrix mat = xt::stack(xt::xtuple(eye, eye), 0);
  return TransformElem(std::move(mat));
}

TransformElem TransformElem::translate(size_t n_dims, const xtivec &offsets) {
  TransformElem res = TransformElem::identity(n_dims);

  xt::view(res.matrix, xt::all(), xt::range(0, offsets.shape(0)), n_dims) =
      offsets;
  return res;
}

TransformElem TransformElem::expand_box(size_t n_dims,
                                        const xtivec &low_offsets,
                                        const xtivec &high_offsets) {
  TransformElem res = TransformElem::identity(n_dims);

  xt::view(res.matrix, 0, xt::range(0, low_offsets.shape(0)), n_dims) =
      low_offsets;
  xt::view(res.matrix, 1, xt::range(0, high_offsets.shape(0)), n_dims) =
      high_offsets;
  return res;
}

TransformElem TransformElem::multiply(size_t n_dims, const xtivec &factors) {
  TransformElem res = TransformElem::identity(n_dims);
  for (unsigned b = 0; b < 2; ++b) {
    for (unsigned i = 0; i < factors.shape(0); ++i) {
      res.matrix(b, i, i) = factors(i);
    }
  }
  return res;
}

TransformElem TransformElem::multiply(size_t n_dims, const xtivec &low_factors,
                                      const xtivec &high_factors) {
  TransformElem res = TransformElem::identity(n_dims);
  for (unsigned i = 0; i < low_factors.shape(0); ++i) {
    res.matrix(0, i, i) = low_factors(i);
    res.matrix(1, i, i) = high_factors(i);
  }
  return res;
}

TransformElem TransformElem::divide(size_t n_dims, xtivec factors) {
  TransformElem res = TransformElem(TransformKind::IntDiv);
  res.low_divisors = factors;
  res.high_divisors = std::move(factors);
  return res;
}

TransformElem TransformElem::divide(size_t n_dims, xtivec low_factors,
                                    xtivec high_factors) {
  TransformElem res = TransformElem(TransformKind::IntDiv);
  res.low_divisors = low_factors;
  res.high_divisors = high_factors;
  return res;
}

TransformElem TransformElem::transpose(size_t n_dims, size_t axis_a,
                                       size_t axis_b) {
  TransformElem res = TransformElem::identity(n_dims);

  xt::view(res.matrix, xt::all(), axis_a, axis_a) = 0;
  xt::view(res.matrix, xt::all(), axis_b, axis_b) = 0;

  xt::view(res.matrix, xt::all(), axis_a, axis_b) = 1;
  xt::view(res.matrix, xt::all(), axis_b, axis_a) = 1;
  return res;
}

TransformElem TransformElem::transpose(size_t n_dims, const xtivec &perm) {
  TransformElem res = TransformElem::identity(n_dims);

  for (unsigned i = 0; i < perm.shape(0); ++i) {
    for (unsigned b = 0; b < 2; ++b) {
      res.matrix(b, i, i) = 0;
      res.matrix(b, i, perm(i)) = 1;
    }
  }
  return res;
}

TransformElem TransformElem::inverse_transpose(size_t n_dims,
                                               const xtivec &perm) {
  TransformElem res = TransformElem::identity(n_dims);

  for (unsigned i = 0; i < perm.shape(0); ++i) {
    for (unsigned b = 0; b < 2; ++b) {
      res.matrix(b, i, i) = 0;
      res.matrix(b, perm(i), i) = 1;
    }
  }
  return res;
}

TransformElem TransformElem::add_dim(size_t n_src_dims, size_t axis,
                                     int64_t low, int64_t high) {
  xtboximatrix mat =
      xt::zeros<int64_t>({size_t(2), n_src_dims + 2, n_src_dims + 1});

  for (size_t i = 0; i < n_src_dims + 1; ++i) {
    xt::view(mat, xt::all(), i + (i >= axis), i) = 1;
  }
  mat(0, axis, n_src_dims) = low;
  mat(1, axis, n_src_dims) = high;
  return TransformElem(std::move(mat));
}

TransformElem TransformElem::remove_dim(size_t n_src_dims, size_t axis) {
  xtboximatrix mat =
      xt::zeros<int64_t>({size_t(2), n_src_dims, n_src_dims + 1});

  for (size_t i = 0; i < n_src_dims; ++i) {
    xt::view(mat, xt::all(), i, i + (i >= axis)) = 1;
  }
  return TransformElem(std::move(mat));
}

TransformElem TransformElem::sum_dim(size_t n_dims, size_t dest_axis,
                                     size_t src_axis) {
  TransformElem res = TransformElem::identity(n_dims);

  xt::view(res.matrix, xt::all(), dest_axis, src_axis) = 1;

  return res;
}

TransformElem TransformElem::split_dim(size_t axis, int64_t split) {
  TransformElem res = TransformElem(TransformKind::SplitDim);
  res.axis = axis;
  res.split = split;
  return res;
}
TransformElem TransformElem::combine_dims(size_t low_axis, int64_t split) {
  TransformElem res = TransformElem(TransformKind::CombineDims);
  res.axis = low_axis;
  res.split = split;
  return res;
}

xtboxivec TransformElem::get_low_matrix() const {
  assert(is_affine());
  return xt::view(matrix, 0, xt::all(), xt::all());
}

xtboxivec TransformElem::get_high_matrix() const {
  assert(is_affine());
  return xt::view(matrix, 1, xt::all(), xt::all());
}

bool TransformElem::is_identity() const {
  if (kind != TransformKind::Affine)
    return false;
  if (matrix.shape(1) != matrix.shape(2))
    return false;
  TransformElem ident = TransformElem::identity(matrix.shape(1) - 1);
  return matrix == ident.matrix;
}

void TransformElem::print(std::ostream &os) const {
  switch (kind) {
  case TransformKind::Affine:
    os << "Affinetransform:\n";
    os << matrix << "\n";
    break;
  case TransformKind::IntDiv:
    os << "IntDivTransform: " << low_divisors << " " << high_divisors
       << std::endl;
    break;
  case TransformKind::SplitDim:
    os << "SplitDimTransform: axis " << axis << ", split " << split
       << std::endl;
    break;
  case TransformKind::CombineDims:
    os << "CombineDimsTransform: axis " << axis << ", split " << split
       << std::endl;
    break;
  }
}

static int64_t divide_rounds_toward_negative_infty(int64_t a, int64_t b) {
  assert(b != 0);
  int64_t res = a / b;
  if (a % b < 0) {
    /* handle negative division case */
    res -= 1;
  }
  return res;
}

xtboxivec TransformElem::apply(const xtboxivec &src) const {
  switch (kind) {
  case TransformKind::Affine:
    return matmul(matrix, src);

  case TransformKind::IntDiv: {
    xtboxivec res = src;
    for (unsigned i = 0; i < low_divisors.shape(0); ++i) {
      /* note that the high divisors is used for the lower bounds, and low
       * divisors used for upper bounds */
      res(0, i) =
          divide_rounds_toward_negative_infty(res(0, i), high_divisors(i));
      res(1, i) =
          divide_rounds_toward_negative_infty(res(1, i), low_divisors(i));
    }
    return res;
  } break;
  case TransformKind::CombineDims: {
    xtboxivec res = xt::zeros<int64_t>({src.shape(0), src.shape(1) - 1});
    for (unsigned b = 0; b < 2; ++b) {
      for (unsigned i = 0; i < res.shape(1); ++i) {
        int64_t val = 0;
        if (i < axis) {
          val = src(b, i);
        } else if (i == axis) {
          val = src(b, i) * split + src(b, i + 1);
        } else {
          val = src(b, i + 1);
        }
        res(b, i) = val;
      }
    }
    return res;
  } break;
  case TransformKind::SplitDim: {
    xtboxivec res = xt::zeros<int64_t>({src.shape(0), src.shape(1) + 1});
    for (unsigned b = 0; b < 2; ++b) {
      for (unsigned i = 0; i < res.shape(1); ++i) {
        if (i < axis) {
          res(b, i) = src(b, i);
        } else if (i == axis) {
          int64_t src_val = src(b, i);
          int64_t div = divide_rounds_toward_negative_infty(src_val, split);
          int64_t rem = src_val - div * split;
          res(b, i) = div;
          res(b, i + 1) = rem;
        } else if (i == axis + 1) {
          /* skip */
        } else {
          res(b, i) = src(b, i - 1);
        }
      }
    }

    /* see if we need to enlarge the box */
    if (res(0, axis) != res(1, axis)) {
      /* okay, we span multiple dimensions, need to expand the next dimension to
       * full split */
      res(0, axis + 1) = 0;
      res(1, axis + 1) = split - 1;
    }
    return res;
  } break;
  }
  return src;
}

void TransformElem::push_front(const TransformElem &o) {
  assert(kind == TransformKind::Affine && o.kind == TransformKind::Affine);
  matrix = matmul(matrix, o.matrix);
}

void TransformElem::push_back(const TransformElem &o) {
  assert(kind == TransformKind::Affine && o.kind == TransformKind::Affine);
  matrix = matmul(o.matrix, matrix);
}

void TransformElem::verify_invariant() const {
  switch (kind) {
  case TransformKind::Affine:
    break;
  case TransformKind::IntDiv:
    for (int64_t d : low_divisors) {
      if (d == 0) {
        throw std::runtime_error("IntDiv has zero divisors");
      }
    }
    for (int64_t d : high_divisors) {
      if (d == 0) {
        throw std::runtime_error("IntDiv has zero divisors");
      }
    }
    break;
  case TransformKind::SplitDim:
  case TransformKind::CombineDims:
    if (split < 1) {
      throw std::runtime_error("Incorrect split");
    }
  }
}

void TransformStack::push_front(TransformElem transform) {
  if (!stack.empty() && stack.front().is_affine() && transform.is_affine()) {
    stack.front().push_front(transform);
  } else {
    stack.insert(stack.begin(), std::move(transform));
  }
}

void TransformStack::push_back(TransformElem transform) {
  if (!stack.empty() && stack.back().is_affine() && transform.is_affine()) {
    stack.back().push_back(transform);
  } else {
    stack.emplace_back(std::move(transform));
  }
}

void TransformStack::append_front(const TransformStack &o) {
  for (auto it = o.stack.rbegin(); it != o.stack.rend(); ++it) {
    push_front(*it);
  }
}

void TransformStack::append_back(const TransformStack &o) {
  for (auto it = o.stack.begin(); it != o.stack.end(); ++it) {
    push_back(*it);
  }
}

xtboxivec TransformStack::apply(const xtboxivec &src) const {
  xtboxivec v = src;
  for (const auto &tr : stack) {
    v = tr.apply(v);
  }
  return v;
}

void TransformStack::verify_invariant() const {
  for (const auto &tr : stack) {
    tr.verify_invariant();
  }
}

bool TransformStack::is_identity() const {
  if (stack.empty())
    return true;
  if (stack.size() == 1) {
    return stack[0].is_identity();
  }
  return false;
}

bool TransformStack::is_affine() const {
  if (stack.empty())
    return true;
  if (stack.size() == 1) {
    return stack[0].is_affine();
  }
  return false;
}

xtboximatrix TransformStack::get_affine_matrix(size_t n_start_dims) const {
  assert(is_affine());
  if (stack.empty())
    return TransformElem::identity(n_start_dims).get_matrix();

  return stack[0].get_matrix();
}

std::ostream &operator<<(std::ostream &os, const TransformStack &stack) {
  for (const auto &tr : stack.get_stack()) {
    tr.print(os);
    os << std::endl;
  }
  return os;
}

CoordTransform::CoordTransform(size_t n_dims)
    : n_start_dims(n_dims), n_end_dims(n_dims) {}

CoordTransform CoordTransform::reverse() const {
  CoordTransform res(n_end_dims);
  res.n_start_dims = n_end_dims;
  res.n_end_dims = n_start_dims;
  res.forward_stack = backward_stack;
  res.backward_stack = forward_stack;
  return res;
}

void CoordTransform::push_pair(TransformElem forward, TransformElem backward) {
  forward_stack.push_back(std::move(forward));
  backward_stack.push_front(std::move(backward));
}

void CoordTransform::multiply(const xtivec &factors) {
  if (factors == xt::ones_like(factors))
    return;
  push_pair(TransformElem::multiply(n_end_dims, factors),
            TransformElem::divide(n_end_dims, factors));
}

void CoordTransform::divide(const xtivec &divisors) {
  if (divisors == xt::ones_like(divisors))
    return;
  push_pair(TransformElem::divide(n_end_dims, divisors),
            TransformElem::multiply(n_end_dims, divisors));
}

void CoordTransform::multiply(const xtivec &low_factors,
                              const xtivec &high_factors) {
  push_pair(TransformElem::multiply(n_end_dims, low_factors, high_factors),
            TransformElem::divide(n_end_dims, low_factors, high_factors));
}

void CoordTransform::divide(const xtivec &low_divisors,
                            const xtivec &high_divisors) {
  push_pair(TransformElem::divide(n_end_dims, low_divisors, high_divisors),
            TransformElem::multiply(n_end_dims, low_divisors, high_divisors));
}

void CoordTransform::multiply(size_t axis, int64_t factor) {
  xtivec factors = xt::ones<int64_t>({n_end_dims});
  factors(axis) = factor;
  multiply(factors);
}

void CoordTransform::divide(size_t axis, int64_t factor) {
  xtivec factors = xt::ones<int64_t>({n_end_dims});
  factors(axis) = factor;
  divide(factors);
}

void CoordTransform::multiply_use_all_elems(const xtivec &factors) {
  if (factors == xt::ones_like(factors))
    return;
  forward_stack.push_back(TransformElem::expand_box(
      n_end_dims, xt::zeros_like(factors), factors - 1));
  push_pair(TransformElem::multiply(n_end_dims, factors),
            TransformElem::divide(n_end_dims, factors));
}

void CoordTransform::divide_use_all_elems(const xtivec &divisors) {
  if (divisors == xt::ones_like(divisors))
    return;
  backward_stack.push_front(TransformElem::expand_box(
      n_end_dims, xt::zeros_like(divisors), divisors - 1));
  push_pair(TransformElem::divide(n_end_dims, divisors),
            TransformElem::multiply(n_end_dims, divisors));
}

void CoordTransform::multiply_use_all_elems(const xtivec &low_factors,
                                            const xtivec &high_factors) {
  forward_stack.push_back(TransformElem::expand_box(
      n_end_dims, xt::zeros_like(high_factors), high_factors - 1));
  push_pair(TransformElem::multiply(n_end_dims, low_factors, high_factors),
            TransformElem::divide(n_end_dims, low_factors, high_factors));
}

void CoordTransform::divide_use_all_elems(const xtivec &low_divisors,
                                          const xtivec &high_divisors) {
  backward_stack.push_front(TransformElem::expand_box(
      n_end_dims, xt::zeros_like(high_divisors), high_divisors - 1));
  push_pair(TransformElem::divide(n_end_dims, low_divisors, high_divisors),
            TransformElem::multiply(n_end_dims, low_divisors, high_divisors));
}

void CoordTransform::multiply_use_all_elems(size_t axis, int64_t factor) {
  xtivec factors = xt::ones<int64_t>({n_end_dims});
  factors(axis) = factor;
  multiply_use_all_elems(factors);
}

void CoordTransform::divide_use_all_elems(size_t axis, int64_t factor) {
  xtivec factors = xt::ones<int64_t>({n_end_dims});
  factors(axis) = factor;
  divide_use_all_elems(factors);
}

void CoordTransform::translate(const xtivec &offsets) {
  push_pair(TransformElem::translate(n_end_dims, offsets),
            TransformElem::translate(n_end_dims, -offsets));
}

void CoordTransform::translate(size_t axis, int64_t offset) {
  xtivec offsets = xt::zeros<int64_t>({n_end_dims});
  offsets(axis) = offset;
  translate(offsets);
}

void CoordTransform::expand_box(const xtivec &low_offsets,
                                const xtivec &high_offsets) {
  push_pair(TransformElem::expand_box(n_end_dims, low_offsets, high_offsets),
            TransformElem::expand_box(n_end_dims, -high_offsets, -low_offsets));
}

void CoordTransform::transpose(size_t axis_a, size_t axis_b) {
  TransformElem tr = TransformElem::transpose(n_end_dims, axis_a, axis_b);
  push_pair(tr, tr);
}

void CoordTransform::transpose(const xtivec &perm) {
  push_pair(TransformElem::transpose(n_end_dims, perm),
            TransformElem::inverse_transpose(n_end_dims, perm));
}

void CoordTransform::inverse_transpose(const xtivec &perm) {
  push_pair(TransformElem::inverse_transpose(n_end_dims, perm),
            TransformElem::transpose(n_end_dims, perm));
}

void CoordTransform::add_dim(size_t axis, int64_t low, int64_t high) {
  push_pair(TransformElem::add_dim(n_end_dims, axis, low, high),
            TransformElem::remove_dim(n_end_dims + 1, axis));
  ++n_end_dims;
}
void CoordTransform::add_dim(size_t axis, const xtboxivec &bbox) {
  add_dim(axis, bbox(0, axis), bbox(1, axis));
}

void CoordTransform::remove_dim(size_t axis, int64_t low, int64_t high) {
  push_pair(TransformElem::remove_dim(n_end_dims, axis),
            TransformElem::add_dim(n_end_dims - 1, axis, low, high));
  --n_end_dims;
}
void CoordTransform::remove_dim(size_t axis, const xtboxivec &bbox) {
  remove_dim(axis, bbox(0, axis), bbox(1, axis));
}

void CoordTransform::sum_dim(size_t dest_axis, size_t src_axis, int64_t low,
                             int64_t high) {
  xtivec low_offsets = xt::zeros<int64_t>({n_end_dims});
  xtivec high_offsets = xt::zeros<int64_t>({n_end_dims});
  low_offsets[dest_axis] = low;
  high_offsets[dest_axis] = high;

  push_pair(TransformElem::sum_dim(n_end_dims, dest_axis, src_axis),
            TransformElem::expand_box(n_end_dims, -high_offsets, -low_offsets));
}

void CoordTransform::sum_dim(size_t dest_axis, size_t src_axis,
                             const xtboxivec &bbox) {
  sum_dim(dest_axis, src_axis, bbox(0, src_axis), bbox(1, src_axis));
}

void CoordTransform::sum_remove_dim(size_t dest_axis, size_t src_axis,
                                    int64_t low, int64_t high) {
  sum_dim(dest_axis, src_axis, low, high);
  remove_dim(src_axis, low, high);
}

void CoordTransform::sum_remove_dim(size_t dest_axis, size_t src_axis,
                                    const xtboxivec &bbox) {
  sum_dim(dest_axis, src_axis, bbox(0, src_axis), bbox(1, src_axis));
}

void CoordTransform::remove_all_dims_for_box(const xtboxivec &bbox) {
  for (int64_t idx = int64_t(bbox.shape(1) - 1); idx >= 0; --idx) {
    remove_dim(size_t(idx), bbox(0, idx), bbox(1, idx));
  }
}

void CoordTransform::add_all_dims_for_box(const xtboxivec &bbox) {
  for (size_t idx = 0; idx < bbox.shape(1); ++idx) {
    add_dim(idx, bbox(0, idx), bbox(1, idx));
  }
}

void CoordTransform::split_dim(size_t axis, int64_t split) {
  push_pair(TransformElem::split_dim(axis, split),
            TransformElem::combine_dims(axis, split));
  ++n_end_dims;
}

void CoordTransform::combine_dims(size_t axis, int64_t split) {
  push_pair(TransformElem::combine_dims(axis, split),
            TransformElem::split_dim(axis, split));

  --n_end_dims;
}

void CoordTransform::append_coord_transform_normal(const CoordTransform &o) {
  forward_stack.append_back(o.forward_stack);
  backward_stack.append_front(o.backward_stack);
}

void CoordTransform::append_coord_transform_reverse(const CoordTransform &o) {
  forward_stack.append_back(o.backward_stack);
  backward_stack.append_front(o.forward_stack);
}

static xtboxivec make_affine(const xtboxivec &v, int64_t affine_val) {
  return xt::pad(v, {{0, 0}, {0, 1}}, xt::pad_mode::constant, affine_val);
}

static xtboxivec remove_affine(const xtboxivec &v, int64_t affine_val) {
  assert(v(0, v.shape(1) - 1) == affine_val &&
         v(1, v.shape(1) - 1) == affine_val);
  return xt::view(v, xt::all(), xt::range(0, v.shape(1) - 1));
}

xtboxivec CoordTransform::transform_forward(const xtboxivec &src) const {
  return remove_affine(forward_stack.apply(make_affine(src, 1)), 1);
}

xtboxivec CoordTransform::transform_backward(const xtboxivec &src) const {
  return remove_affine(backward_stack.apply(make_affine(src, 1)), 1);
}

xtivec CoordTransform::transform_direction_forward(const xtivec &src) const {
  xtboxivec v = xt::broadcast(src, {size_t(2), src.shape(0)});

  xtboxivec res = remove_affine(forward_stack.apply(make_affine(v, 0)), 0);
  return xt::view(res, 0, xt::all());
}

xtivec CoordTransform::transform_direction_backward(const xtivec &src) const {
  xtboxivec v = xt::broadcast(src, {size_t(2), src.shape(0)});

  xtboxivec res = remove_affine(backward_stack.apply(make_affine(v, 0)), 0);
  return xt::view(res, 0, xt::all());
}

static xtboxivec clip(const xtboxivec &v, const xtboxivec &clip_box) {
  xtboxivec dest = v;
  dest = xt::maximum(dest, xt::view(clip_box, xt::range(0, 1), xt::all()));
  dest = xt::minimum(dest, xt::view(clip_box, xt::range(1, 2), xt::all()));

  return dest;
}

xtboxivec
CoordTransform::transform_forward_clip(const xtboxivec &src,
                                       const xtboxivec &clip_box) const {
  xtboxivec dest = transform_forward(src);
  return clip(dest, clip_box);
}

xtboxivec
CoordTransform::transform_backward_clip(const xtboxivec &src,
                                        const xtboxivec &clip_box) const {
  xtboxivec dest = transform_backward(src);
  return clip(dest, clip_box);
}

void CoordTransform::verify_invariant() const {
  forward_stack.verify_invariant();
  backward_stack.verify_invariant();
}

std::ostream &operator<<(std::ostream &os, const CoordTransform &transform) {
  os << "CoordTransform forward:\n";
  os << transform.get_forward_stack();
  os << "CoordTransform backward:\n";
  os << transform.get_backward_stack() << std::endl;
  return os;
}
} // namespace coordinate_transform
} // namespace mlir

/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#ifndef COORD_TRANSFORM_H
#define COORD_TRANSFORM_H

#include <cassert>
#include <iosfwd>
#include <limits>
#include <memory>
#include <stdexcept>
#include <utility>

#include <absl/container/inlined_vector.h>

#include <xtensor/xadapt.hpp>
#include <xtensor/xarray.hpp>
#include <xtensor/xio.hpp>
#include <xtensor/xmanipulation.hpp>
#include <xtensor/xnorm.hpp>
#include <xtensor/xpad.hpp>
#include <xtensor/xview.hpp>

#include "src/ir/geometric_transform/coordinate.h"

namespace mlir {
namespace coordinate_transform {
constexpr size_t typical_coord_dims = 7;
using xtboximatrix = xtensor_prealloc<int64_t, 3, 2 * typical_coord_dims>;
using xtboxivec = xtensor_prealloc<int64_t, 2, 2 * typical_coord_dims>;
using xtivec = xtensor_prealloc<int64_t, 1, typical_coord_dims>;
using xtbvec = xtensor_prealloc<bool, 1, typical_coord_dims>;

enum class TransformKind { Affine, IntDiv, SplitDim, CombineDims };

std::ostream &operator<<(std::ostream &out, TransformKind kind);

/* tracking boxes using inclusive coordinates

    vectors are: <2, n_dims>

    Where the first dimension of 2 contains 0 - low bound, 1 - high bound

    matrices are <2, n_dest_dims, n_src_dims>
*/
class TransformElem {
public:
  TransformElem(xtboximatrix mat);
  TransformElem(TransformKind _kind) : kind(_kind) {}

  static TransformElem identity(size_t n_dims);

  static TransformElem multiply(size_t n_dims, const xtivec &scales);
  static TransformElem multiply(size_t n_dims, const xtivec &low_scales,
                                const xtivec &high_scales);

  static TransformElem divide(size_t n_dims, const xtivec factors);
  static TransformElem divide(size_t n_dims, const xtivec low_factors,
                              const xtivec high_factors);

  static TransformElem translate(size_t n_dims, const xtivec &offsets);
  static TransformElem expand_box(size_t n_dims, const xtivec &low_offsets,
                                  const xtivec &high_offsets);
  static TransformElem transpose(size_t n_dims, size_t axis_a, size_t axis_b);
  static TransformElem transpose(size_t n_dims, const xtivec &perm);
  static TransformElem inverse_transpose(size_t n_dims, const xtivec &perm);
  static TransformElem add_dim(size_t n_src_dims, size_t axis, int64_t low,
                               int64_t high);
  static TransformElem remove_dim(size_t n_src_dims, size_t axis);
  static TransformElem sum_dim(size_t n_dims, size_t dest_axis,
                               size_t src_axis);

  static TransformElem split_dim(size_t axis, int64_t split);
  static TransformElem combine_dims(size_t low_axis, int64_t split);

  xtboxivec apply(const xtboxivec &input) const;

  bool is_affine() const { return kind == TransformKind::Affine; }
  bool is_identity() const;
  const xtboximatrix &get_matrix() const {
    assert(is_affine());
    return matrix;
  }

  xtboxivec get_low_matrix() const;
  xtboxivec get_high_matrix() const;

  void push_front(const TransformElem &o);
  void push_back(const TransformElem &o);

  void print(std::ostream &os) const;

  void verify_invariant() const;

  const xtivec &get_low_divisors() const {
    assert(kind == TransformKind::IntDiv);
    return low_divisors;
  }
  const xtivec &get_high_divisors() const {
    assert(kind == TransformKind::IntDiv);
    return high_divisors;
  }

  size_t get_axis() const { return axis; }
  size_t get_split() const { return split; }

  TransformKind get_kind() const { return kind; }

private:
  TransformKind kind;

  /* for Affine */
  xtboximatrix matrix;

  /* for IntDiv */
  xtivec low_divisors;
  xtivec high_divisors;

  /* for SplitDim/CombineDim */
  size_t axis;
  int64_t split;
};

class TransformStack {
public:
  void push_front(TransformElem transform);
  void push_back(TransformElem transform);

  void append_front(const TransformStack &stack);
  void append_back(const TransformStack &stack);

  xtboxivec apply(const xtboxivec &src) const;

  void verify_invariant() const;

  bool is_identity() const;
  bool is_affine() const;
  xtboximatrix get_affine_matrix(size_t n_start_dims) const;

  const absl::InlinedVector<TransformElem, 1> &get_stack() const {
    return stack;
  }

private:
  absl::InlinedVector<TransformElem, 1> stack;
};

std::ostream &operator<<(std::ostream &os, const TransformStack &stack);

class CoordTransform {
public:
  CoordTransform(size_t n_dims);

  CoordTransform reverse() const;

  void multiply(const xtivec &low_factors, const xtivec &high_factors);
  void divide(const xtivec &low_factors, const xtivec &high_factors);
  void multiply(const xtivec &factors);
  void divide(const xtivec &factors);
  void multiply(size_t axis, int64_t factor);
  void divide(size_t axis, int64_t factor);
  void multiply_use_all_elems(const xtivec &factors);
  void divide_use_all_elems(const xtivec &factors);
  void multiply_use_all_elems(const xtivec &low_factors,
                              const xtivec &high_factors);
  void divide_use_all_elems(const xtivec &low_factors,
                            const xtivec &high_factors);
  void multiply_use_all_elems(size_t axis, int64_t factor);
  void divide_use_all_elems(size_t axis, int64_t factor);
  void translate(const xtivec &offsets);
  void translate(size_t axis, int64_t offset);
  void expand_box(const xtivec &low_offsets, const xtivec &high_offsets);
  void transpose(size_t axis_a, size_t axis_b);
  void transpose(const xtivec &perm);
  void inverse_transpose(const xtivec &perm);
  void add_dim(size_t axis, int64_t low, int64_t high);
  void remove_dim(size_t axis, int64_t low, int64_t high);
  void add_dim(size_t axis, const xtboxivec &bbox);
  void remove_dim(size_t axis, const xtboxivec &bbox);

  void sum_dim(size_t dest_axis, size_t src_axis, int64_t low, int64_t high);
  void sum_dim(size_t dest_axis, size_t src_axis, const xtboxivec &bbox);

  void sum_remove_dim(size_t dest_axis, size_t src_axis, int64_t low,
                      int64_t high);
  void sum_remove_dim(size_t dest_axis, size_t src_axis, const xtboxivec &bbox);

  void remove_all_dims_for_box(const xtboxivec &bbox);
  void add_all_dims_for_box(const xtboxivec &bbox);

  /* split a dimension into two, as if we're doing new_v(axis) = v(axis) //
   * split, new_v(axis+1) = v(axis) % split */
  void split_dim(size_t axis, int64_t split);
  /* combine two dimensions of low_axis,low_axis+1, as if we're doing
   * new_v(new_axis) = v(low_axis)*split + v(low_axis+1) */
  void combine_dims(size_t low_axis, int64_t split);

  xtboxivec transform_forward(const xtboxivec &src) const;
  xtboxivec transform_backward(const xtboxivec &src) const;

  xtivec transform_direction_forward(const xtivec &src) const;
  xtivec transform_direction_backward(const xtivec &src) const;

  xtboxivec transform_forward_clip(const xtboxivec &src,
                                   const xtboxivec &clip_box) const;
  xtboxivec transform_backward_clip(const xtboxivec &src,
                                    const xtboxivec &clip_box) const;

  const TransformStack &get_forward_stack() const { return forward_stack; }
  const TransformStack &get_backward_stack() const { return backward_stack; }

  void verify_invariant() const;

  void append_coord_transform_normal(const CoordTransform &o);
  void append_coord_transform_reverse(const CoordTransform &o);

  size_t get_n_start_dims() const noexcept { return n_start_dims; }
  size_t get_n_end_dims() const noexcept { return n_end_dims; }

  bool is_identity() const {
    return forward_stack.is_identity() && backward_stack.is_identity();
  }
  bool is_affine() const {
    return forward_stack.is_affine() && backward_stack.is_affine();
  }
  xtboximatrix get_forward_affine_matrix() const {
    assert(is_affine());
    return forward_stack.get_affine_matrix(n_start_dims);
  }
  xtboximatrix get_backward_affine_matrix() const {
    assert(is_affine());
    return backward_stack.get_affine_matrix(n_start_dims);
  }

private:
  void push_pair(TransformElem forward, TransformElem backward);

  size_t n_start_dims;
  size_t n_end_dims;
  TransformStack forward_stack;
  TransformStack backward_stack;
};

std::ostream &operator<<(std::ostream &os, const CoordTransform &transform);

} // namespace coordinate_transform
} // namespace mlir

#endif // COORD_TRANSFORM_H

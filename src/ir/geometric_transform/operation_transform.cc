/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#include <memory>
#include <unordered_set>

#include <xtensor/xbuilder.hpp>
#include <xtensor/xfixed.hpp>
#include <xtensor/xview.hpp>

#include "src/ir/geometric_transform/operation_transform.h"

#include "src/utils/bitvector.h"

namespace mlir {
namespace operation_transform {

std::ostream &operator<<(std::ostream &os, const AxisScaleOffset &as) {
  os << "<AxisScaleOffset axis=" << as.axis << " scale_down=" << as.scale_down
     << " scale_up=" << as.scale_up << " before_offset=" << as.before_offset
     << " after_offset=" << as.after_offset << ">";
  return os;
}

std::ostream &operator<<(std::ostream &os, const OpToTensorTransform &ott) {
  os << "OpToTensorTransform\n";
  os << ott.transform;
  os << "Bounding box:\n";
  os << ott.tensor_box << "\n";
  os << "Worth subdividing: " << ott.worth_subdividing << "\n";
  os << "Needs overlap: " << ott.needs_overlap << "\n";
  return os;
}

std::ostream &operator<<(std::ostream &os, const OperationTransform &ot) {
  os << "OperationTransform: operation box:\n";
  os << ot.get_operation_box() << "\n";
  for (unsigned i = 0; i < ot.get_n_output_transforms(); ++i) {
    os << "Output " << i << ": " << ot.get_output_transform(i);
  }
  for (unsigned i = 0; i < ot.get_n_input_transforms(); ++i) {
    os << "Input " << i << ": " << ot.get_input_transform(i);
  }
  return os;
}

void OpToTensorTransform::finalize(const OTBoundingBox &operation_box,
                                   const xtivec &op_required_scheduling_quantum,
                                   const xtbvec &axes_present_in_all_outputs) {
  /* some operations don't read or write their entire operands. by transforming
   * the operation box into the tensor space, and then using that as the new
   * tensor box, we are able to calculate the actual bounding box we operate on
   */
  tensor_box = transform.transform_forward_clip(operation_box, tensor_box);

  size_t n_axes = operation_box.shape(1);
  worth_subdividing = xt::zeros<bool>({n_axes});
  needs_overlap = xt::zeros<bool>({n_axes});

  scale_offsets_for_axis.resize(n_axes);

  /* funny business calculating the offset, as we're really
   * trying to establish the overlap, not the
   * extent. Therefore we calculate the difference between
   * max(-1)+1 and min(0). This matters for things like 2x2
   * pool with 2x2 stride, which has an extent of 2, but
   * overlap of 0 */

  xtboxivec output_offset_translation = xt::zeros<int64_t>({size_t(2), n_axes});
  xt::view(output_offset_translation, 1, xt::all()) -= 1;
  for (size_t i = 0; i < axes_present_in_all_outputs.size(); ++i) {
    if (!axes_present_in_all_outputs[i]) {
      xt::view(output_offset_translation, xt::all(), i) =
          xt::view(operation_box, xt::all(), i);
    }
  }
  xtboxivec output_offset_in_tensor_space =
      transform.transform_forward(output_offset_translation);
  xt::view(output_offset_in_tensor_space, 1, xt::all()) += 1;

  xtbvec needs_overlap_in_tensor_space =
      xt::not_equal(xt::view(output_offset_in_tensor_space, 0, xt::all()),
                    xt::view(output_offset_in_tensor_space, 1, xt::all()));

  size_t n_tens_axes = tensor_box.shape(1);

  constexpr size_t max_stride = 16;
  for (size_t axis = 0; axis < n_axes; ++axis) {
    BitVector not_handled = BitVector::ones(n_tens_axes);

    for (size_t stride = 1; stride <= max_stride && not_handled.any();
         ++stride) {
      size_t op_stride = op_required_scheduling_quantum(axis) * stride;

      xtivec scale_vec = xt::zeros<int64_t>({n_axes});
      scale_vec(axis) = op_stride;
      xtivec scale_in_tensor_space =
          transform.transform_direction_forward(scale_vec);
      xtivec double_scale_in_tensor_space =
          transform.transform_direction_forward(scale_vec * 2);

      for (size_t tens_axis = 0; tens_axis < n_tens_axes; ++tens_axis) {

        if (not_handled[tens_axis] && scale_in_tensor_space(tens_axis) != 0 &&
            2 * scale_in_tensor_space(tens_axis) ==
                double_scale_in_tensor_space(tens_axis)) {
          int64_t scale_up = scale_in_tensor_space(tens_axis);
          int64_t scale_down = op_stride;

          worth_subdividing(axis) = true;
          if (needs_overlap_in_tensor_space(tens_axis)) {
            needs_overlap(axis) = true;
          }
          scale_offsets_for_axis[axis].emplace_back(
              tens_axis, scale_down, scale_up,
              output_offset_in_tensor_space(0, tens_axis),
              output_offset_in_tensor_space(1, tens_axis));
          not_handled.clear(tens_axis);
        }
      }
    }
  }
}

void OperationTransform::finalize() {
  size_t n_axes = operation_box.shape(1);
  size_t n_buffers = output_transforms.size() + input_transforms.size();
  size_t buf_idx = 0;
  axes_present_in_all_outputs = xt::ones<bool>({n_axes});
  needs_overlap = xt::zeros<bool>({n_axes});
  worth_subdividing = xt::zeros<bool>({n_axes});

  subdivision_axes = xt::zeros<size_t>({n_buffers, n_axes});
  for (auto &tr : output_transforms) {
    tr.finalize(operation_box, op_required_scheduling_quantum,
                xt::ones<bool>({n_axes}));
    worth_subdividing = worth_subdividing || tr.worth_subdividing;
    axes_present_in_all_outputs =
        axes_present_in_all_outputs && tr.worth_subdividing;
    needs_overlap = needs_overlap || tr.needs_overlap;
    xt::view(subdivision_axes, buf_idx, xt::all()) = tr.worth_subdividing;
    ++buf_idx;
  }

  for (auto &tr : input_transforms) {
    tr.finalize(operation_box, op_required_scheduling_quantum,
                axes_present_in_all_outputs);
    worth_subdividing = worth_subdividing || tr.worth_subdividing;
    needs_overlap = needs_overlap || tr.needs_overlap;

    xt::view(subdivision_axes, buf_idx, xt::all()) = tr.worth_subdividing;
    ++buf_idx;
  }
  worth_subdividing_at_all = xt::amax(worth_subdividing)();
  assert(buf_idx == n_buffers);
}

size_t OperationTransform::n_subdivision_tensors(
    const absl::InlinedVector<size_t, 4> &op_axes) const {
  if (op_axes.empty())
    return 0;
  size_t res = 0;
  for (size_t dim = 0; dim < subdivision_axes.shape(0); ++dim) {
    size_t divides = 1;
    for (auto axis : op_axes) {
      if (!subdivision_axes(dim, axis)) {
        divides = 0;
        break;
      }
    }
    res += divides;
  }
  return res;
}

////////////////////////////////////////////////////////////////////////////
// Box Member Functions
////////////////////////////////////////////////////////////////////////////

bool boxes_are_combinable(const OTBoundingBox &a, const OTBoundingBox &b) {
  int expand_axis = -1;
  assert(a.shape() == b.shape());
  for (int axis = 0; axis < int(a.shape(1)); ++axis) {
    int64_t a_lo = a(0, axis);
    int64_t a_hi = a(1, axis);
    int64_t b_lo = b(0, axis);
    int64_t b_hi = b(1, axis);
    if (a_lo == b_lo && a_hi == b_hi) {
      /* axis identical, this fine */
    } else if (std::min(a_hi, b_hi) + 1 >= std::max(a_lo, b_lo)) {
      /* combining this axis will result in a contiguous
       * range. it's cool as long as we have just one of
       * these axis in the entire box */
      if (expand_axis == -1) {
        expand_axis = axis;
      } else {
        return false;
      }
    } else {
      /* combining this axis will result in non-contiguous range. definitely
       * no-no */
      return false;
    }
  }
  return true;
}

OTBoundingBox combine_boxes(const OTBoundingBox &a, const OTBoundingBox &b) {
  OTBoundingBox res = a;
  xt::view(res, 0, xt::all()) =
      xt::minimum(xt::view(a, 0, xt::all()), xt::view(b, 0, xt::all()));
  xt::view(res, 1, xt::all()) =
      xt::maximum(xt::view(a, 1, xt::all()), xt::view(b, 1, xt::all()));
  return res;
}

OTBoundingBox intersect_boxes(const OTBoundingBox &a, const OTBoundingBox &b) {
  OTBoundingBox res = a;
  xt::view(res, 0, xt::all()) =
      xt::maximum(xt::view(a, 0, xt::all()), xt::view(b, 0, xt::all()));
  xt::view(res, 1, xt::all()) =
      xt::minimum(xt::view(a, 1, xt::all()), xt::view(b, 1, xt::all()));
  return res;
}

bool is_non_zero_bounding_box(const OTBoundingBox &bbox) {
  return xt::all(xt::less_equal(xt::view(bbox, 0, xt::all()),
                                xt::view(bbox, 1, xt::all())));
}

bool boxes_intersect(const OTBoundingBox &a, const OTBoundingBox &b) {
  OTBoundingBox intersection = intersect_boxes(a, b);
  return is_non_zero_bounding_box(intersection);
}

bool box_contains_box(const OTBoundingBox &bigger,
                      const OTBoundingBox &smaller) {
  return combine_boxes(bigger, smaller) == bigger;
}

OTBoundingBox null_box() { return xt::zeros<int64_t>({2, 0}); }

OTBoundingBox prepend_to_box(const OTBoundingBox &bbox, int64_t min_val,
                             int64_t max_val) {
  OTBoundingBox res = xt::zeros<int64_t>({bbox.shape(0), bbox.shape(1) + 1});
  xt::view(res, xt::all(), xt::range(1, res.shape(1))) = bbox;
  res(0, 0) = min_val;
  res(1, 0) = max_val;
  return res;
}

OTBoundingBox box_from_shape(const OTShape &shp) {
  OTBoundingBox box = xt::zeros<int64_t>({size_t(2), shp.size()});
  for (size_t i = 0; i < shp.size(); ++i) {
    box(1, i) = shp[i] - 1;
  }
  return box;
}

OTBoundingBox box_from_quantum(
    const xtensor_prealloc<int64_t, 1, typical_bounding_box_dims> &quantum) {
  OTBoundingBox box = xt::zeros<int64_t>({size_t(2), quantum.size()});
  for (size_t i = 0; i < quantum.size(); ++i) {
    box(1, i) = quantum[i] - 1;
  }
  return box;
}

OTBoundingBox clip_box_to_box(OTBoundingBox bbox,
                              const OTBoundingBox &clip_box) {
  bbox = xt::maximum(bbox, xt::view(clip_box, xt::range(0, 1), xt::all()));
  bbox = xt::minimum(bbox, xt::view(clip_box, xt::range(1, 2), xt::all()));

  return bbox;
}

xtensor_prealloc<int64_t, 1, typical_bounding_box_dims>
sizes_of_bounding_box(const OTBoundingBox &bbox) {
  return (xt::view(bbox, 1, xt::all()) - xt::view(bbox, 0, xt::all())) + 1;
}

xtensor_prealloc<int64_t, 1, typical_bounding_box_dims>
start_of_box(const OTBoundingBox &bbox) {
  return xt::view(bbox, 0, xt::all());
}

int64_t n_elements_in_bbox(const OTBoundingBox &bbox) {
  int64_t res = 1;
  for (size_t i = 0; i < bbox.shape(1); ++i) {
    res *= (bbox(1, i) - bbox(0, i) + 1);
  }
  return res;
}

int64_t n_rounded_elements_in_bbox(const OTBoundingBox &bbox,
                                   OTShape round_quantum) {
  int64_t res = 1;
  for (size_t i = 0; i < bbox.shape(1); ++i) {
    int64_t len = (bbox(1, i) - bbox(0, i) + 1);
    len = round_up(len, round_quantum[i]);
    res *= len;
  }

  return res;
}

xtensor_prealloc<int64_t, 1, typical_bounding_box_dims>
get_low_coord(const OTBoundingBox &bbox) {
  return xt::view(bbox, 0, xt::all());
}

xtensor_prealloc<int64_t, 1, typical_bounding_box_dims>
get_high_coord(const OTBoundingBox &bbox) {
  return xt::view(bbox, 1, xt::all());
}

} // namespace operation_transform
} // namespace mlir

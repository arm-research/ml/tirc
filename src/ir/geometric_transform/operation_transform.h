/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#ifndef OPERATION_TRANSFORM_H
#define OPERATION_TRANSFORM_H

#include <iosfwd>

#include <absl/container/flat_hash_map.h>
#include <absl/container/inlined_vector.h>

#include "src/ir/geometric_transform/coordinate_transform.h"
using namespace mlir::coordinate_transform;

/*
     0. OTShape,  OFCoordinate, OTBoundingBox are used inside the coordinate
   system
     1. BoundingBox and Coordinate are used with the MLIR Dialect
     Imp: Consider Merging
 */

namespace mlir {
namespace operation_transform {
constexpr size_t typical_bounding_box_dims = 6;

using OTShape = llvm::ArrayRef<int64_t>;
using OTCoordinate = xtensor_prealloc<int64_t, 1, 2 * typical_coord_dims>;
using OTBoundingBox = xtensor_prealloc<
    int64_t, 2,
    2 * typical_bounding_box_dims>; // bounding box {2, N} with inclusive
                                    // coordinates. first dimension: lower/upper

OTBoundingBox box_from_shape(const OTShape &shp);
OTBoundingBox box_from_quantum(
    const xtensor_prealloc<int64_t, 1, typical_bounding_box_dims> &quantum);

OTBoundingBox null_box();

OTBoundingBox prepend_to_box(const OTBoundingBox &bbox, int64_t min_val,
                             int64_t max_val);

xtensor_prealloc<int64_t, 1, typical_bounding_box_dims>
get_low_coord(const OTBoundingBox &bbox);
xtensor_prealloc<int64_t, 1, typical_bounding_box_dims>
get_high_coord(const OTBoundingBox &bbox);

OTBoundingBox clip_box_to_box(OTBoundingBox bbox, const OTBoundingBox &clip_to);

int64_t n_elements_in_bbox(const OTBoundingBox &bbox);
int64_t n_rounded_elements_in_bbox(const OTBoundingBox &bbox,
                                   const OTShape &round_quantum);

xtensor_prealloc<int64_t, 1, typical_bounding_box_dims>
sizes_of_bounding_box(const OTBoundingBox &bbox);

xtensor_prealloc<int64_t, 1, typical_bounding_box_dims>
start_of_box(const OTBoundingBox &bbox);

bool boxes_are_combinable(const OTBoundingBox &a, const OTBoundingBox &b);
OTBoundingBox combine_boxes(const OTBoundingBox &a, const OTBoundingBox &b);
OTBoundingBox intersect_boxes(const OTBoundingBox &a, const OTBoundingBox &b);
bool is_non_zero_bounding_box(const OTBoundingBox &bbox);
bool boxes_intersect(const OTBoundingBox &a, const OTBoundingBox &b);
bool box_contains_box(const OTBoundingBox &bigger,
                      const OTBoundingBox &smaller);

struct AxisScaleOffset {
  AxisScaleOffset(size_t _axis, int64_t _scale_down, int64_t _scale_up,
                  int64_t _before_offset, int64_t _after_offset)
      : axis(_axis), scale_down(_scale_down), scale_up(_scale_up),
        before_offset(_before_offset), after_offset(_after_offset) {}

  size_t axis;
  int64_t scale_down;
  int64_t scale_up;
  int64_t before_offset;
  int64_t after_offset;
};

std::ostream &operator<<(std::ostream &os, const AxisScaleOffset &as);

class OpToTensorTransform {
public:
  OpToTensorTransform(const OTBoundingBox &operation_box,
                      OTBoundingBox _tensor_box)
      : transform(operation_box.shape(1)), tensor_box(std::move(_tensor_box)) {}

  void finalize(const OTBoundingBox &operation_box,
                const xtivec &op_required_scheduling_quantum,
                const xtbvec &axes_present_in_all_outputs);

  bool is_identity() const { return transform.is_identity(); }
  bool is_affine() const { return transform.is_affine(); }
  xtboximatrix get_forward_affine_matrix() const {
    return transform.get_forward_affine_matrix();
  }
  xtboximatrix get_backward_affine_matrix() const {
    return transform.get_backward_affine_matrix();
  }

  const OTBoundingBox &get_tensor_box() const { return tensor_box; }

  const CoordTransform &get_transform() const { return transform; }

public:
  CoordTransform transform;
  OTBoundingBox tensor_box;
  xtbvec worth_subdividing;
  xtbvec needs_overlap;
  std::vector<std::vector<AxisScaleOffset>>
      scale_offsets_for_axis; /* lookup on op axis */
};
std::ostream &operator<<(std::ostream &os, const OpToTensorTransform &ott);

class OperationTransform {
public:
  OperationTransform(const OTBoundingBox &_operation_box)
      : operation_box(_operation_box),
        op_required_scheduling_quantum(
            xt::ones<int64_t>({operation_box.shape(1)})) {}

  const std::vector<OpToTensorTransform> &get_input_transforms() const
      noexcept {
    return input_transforms;
  }
  std::vector<OpToTensorTransform> &get_input_transforms() noexcept {
    return input_transforms;
  }
  size_t get_n_input_transforms() const noexcept {
    return input_transforms.size();
  }
  void
  set_input_transforms(std::vector<OpToTensorTransform> _input_transforms) {
    input_transforms = std::move(_input_transforms);
  }
  void push_back_input_transform(OpToTensorTransform trans) {
    input_transforms.emplace_back(std::move(trans));
  }
  const OpToTensorTransform &get_input_transform(size_t idx) const noexcept {
    return input_transforms[idx];
  }
  void set_input_transform(size_t idx, OpToTensorTransform trans) {
    input_transforms[idx] = std::move(trans);
  }

  const std::vector<OpToTensorTransform> &get_output_transforms() const
      noexcept {
    return output_transforms;
  }
  std::vector<OpToTensorTransform> &get_output_transforms() noexcept {
    return output_transforms;
  }
  size_t get_n_output_transforms() const noexcept {
    return output_transforms.size();
  }
  void
  set_output_transforms(std::vector<OpToTensorTransform> _output_transforms) {
    output_transforms = std::move(_output_transforms);
  }
  void push_back_output_transform(OpToTensorTransform trans) {
    output_transforms.emplace_back(std::move(trans));
  }
  const OpToTensorTransform &get_output_transform(size_t idx) const noexcept {
    return output_transforms[idx];
  }
  void set_output_transform(size_t idx, OpToTensorTransform trans) {
    output_transforms[idx] = std::move(trans);
  }

  const std::vector<OpToTensorTransform> &get_const_transforms() const
      noexcept {
    return const_transforms;
  }
  std::vector<OpToTensorTransform> &get_const_transforms() noexcept {
    return const_transforms;
  }
  size_t get_n_const_transforms() const noexcept {
    return const_transforms.size();
  }
  void
  set_const_transforms(std::vector<OpToTensorTransform> _const_transforms) {
    const_transforms = std::move(_const_transforms);
  }
  void push_back_const_transform(OpToTensorTransform trans) {
    const_transforms.emplace_back(std::move(trans));
  }
  const OpToTensorTransform &get_const_transform(size_t idx) const noexcept {
    return const_transforms[idx];
  }
  void set_const_transform(size_t idx, OpToTensorTransform trans) {
    const_transforms[idx] = std::move(trans);
  }

  OTBoundingBox transform_op_to_input_tensor(size_t idx,
                                             const OTBoundingBox &src) const {
    return input_transforms[idx].transform.transform_forward_clip(
        src, input_transforms[idx].tensor_box);
  }

  OTBoundingBox transform_input_tensor_to_op(size_t idx,
                                             const OTBoundingBox &src) const {
    return input_transforms[idx].transform.transform_backward_clip(
        src, operation_box);
  }

  OTBoundingBox transform_op_to_output_tensor(size_t idx,
                                              const OTBoundingBox &src) const {
    return output_transforms[idx].transform.transform_forward_clip(
        src, output_transforms[idx].tensor_box);
  }

  OTBoundingBox transform_output_tensor_to_op(size_t idx,
                                              const OTBoundingBox &src) const {
    return output_transforms[idx].transform.transform_backward_clip(
        src, operation_box);
  }

  OTBoundingBox
  transform_op_to_input_tensor_no_clip(size_t idx,
                                       const OTBoundingBox &src) const {
    return input_transforms[idx].transform.transform_forward(src);
  }

  OTBoundingBox
  transform_input_tensor_to_op_no_clip(size_t idx,
                                       const OTBoundingBox &src) const {
    return input_transforms[idx].transform.transform_backward(src);
  }

  OTBoundingBox
  transform_op_to_output_tensor_no_clip(size_t idx,
                                        const OTBoundingBox &src) const {
    return output_transforms[idx].transform.transform_forward(src);
  }

  OTBoundingBox
  transform_output_tensor_to_op_no_clip(size_t idx,
                                        const OTBoundingBox &src) const {
    return output_transforms[idx].transform.transform_backward(src);
  }

  void finalize();
  void set_alternate_traffic_input_transform(size_t idx,
                                             const OpToTensorTransform &trans) {
    alternate_traffic_input_transforms.emplace(idx, trans);
  }
  void
  set_alternate_traffic_output_transform(size_t idx,
                                         const OpToTensorTransform &trans) {
    alternate_traffic_output_transforms.emplace(idx, trans);
  }
  const auto &get_alternate_traffic_input_transforms() const {
    return alternate_traffic_input_transforms;
  }
  const auto &get_alternate_traffic_output_transforms() const {
    return alternate_traffic_output_transforms;
  }
  const OTBoundingBox &get_operation_box() const { return operation_box; }
  void set_operation_box(const OTBoundingBox &box) { operation_box = box; }
  size_t get_n_operation_dims() const { return operation_box.shape(1); }
  xtivec get_op_required_scheduling_quantum() const {
    return op_required_scheduling_quantum;
  }
  void
  set_op_required_scheduling_quantum(xtivec _op_required_scheduling_quantum) {
    op_required_scheduling_quantum = std::move(_op_required_scheduling_quantum);
  }
  bool get_worth_subdividing_at_all() const { return worth_subdividing_at_all; }
  const xtbvec &get_worth_subdividing() const { return worth_subdividing; }
  const xtbvec &get_axes_present_in_all_outputs() const {
    return axes_present_in_all_outputs;
  }
  const xtbvec &get_needs_overlap() const { return needs_overlap; }
  size_t
  n_subdivision_tensors(const absl::InlinedVector<size_t, 4> &op_axes) const;

private:
  /* the standard transforms describe the dependencies accurately. they can also
   * mostly be used for traffic estimations */
  std::vector<OpToTensorTransform> input_transforms;
  std::vector<OpToTensorTransform> output_transforms;
  std::vector<OpToTensorTransform> const_transforms;

  /* however, sometimes the dependencies have to be very pessimistic which
   * overstates the traffic. we can specify an alternate traffic transform in
   * these cases */
  absl::flat_hash_map<size_t, OpToTensorTransform>
      alternate_traffic_input_transforms;

  OTBoundingBox operation_box;
  xtivec op_required_scheduling_quantum; /* this one is exclusive - all
                                            dimensions should be 1 or higher */

  absl::flat_hash_map<size_t, OpToTensorTransform>
      alternate_traffic_output_transforms;

  xtbvec worth_subdividing;
  xtbvec needs_overlap;
  xtbvec axes_present_in_all_outputs;
  bool worth_subdividing_at_all;

  xtensor_prealloc<size_t, 2, 4 * typical_coord_dims>
      subdivision_axes; /* buf_idx (out+in) x n_op_dims */
};

std::ostream &operator<<(std::ostream &os, const OperationTransform &ot);

OperationTransform concat_operation_transforms(const Operation *oa,
                                               const OperationTransform &a,
                                               const Operation *ob,
                                               const OperationTransform &b);

} // namespace operation_transform
} // namespace mlir

#endif // OPERATION_TRANSFORM_H

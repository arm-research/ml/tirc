/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#include <algorithm>
#include <cstdint>
#include <fstream>
#include <functional>
#include <iomanip>
#include <iostream>
#include <numeric>
#include <string>
#include <type_traits>

#include "mlir/Dialect/Traits.h"           // TF:local_config_mlir
#include "mlir/IR/Attributes.h"            // TF:local_config_mlir
#include "mlir/IR/Builders.h"              // TF:local_config_mlir
#include "mlir/IR/Diagnostics.h"           // TF:local_config_mlir
#include "mlir/IR/MLIRContext.h"           // TF:local_config_mlir
#include "mlir/IR/Matchers.h"              // TF:local_config_mlir
#include "mlir/IR/OpImplementation.h"      // TF:local_config_mlir
#include "mlir/IR/PatternMatch.h"          // TF:local_config_mlir
#include "mlir/IR/TypeUtilities.h"         // TF:local_config_mlir
#include "mlir/IR/Types.h"                 // TF:local_config_mlir
#include "mlir/IR/Value.h"                 // TF:local_config_mlir
#include "mlir/Parser.h"                   // TF:local_config_mlir
#include "mlir/Support/LLVM.h"             // TF:local_config_mlir
#include "mlir/Support/LogicalResult.h"    // TF:local_config_mlir
#include "mlir/Transforms/FoldUtils.h"     // TF:local_config_mlir
#include "mlir/Transforms/InliningUtils.h" // TF:local_config_mlir
#include "mlir/Transforms/RegionUtils.h"   // TF:local_config_mlir
#include "llvm/ADT/Sequence.h"
#include "llvm/ADT/SmallVector.h"
#include "llvm/ADT/StringExtras.h"
#include "llvm/ADT/StringRef.h"
#include "llvm/ADT/StringSwitch.h"
#include "llvm/ADT/iterator_range.h"
#include "llvm/Support/FormatVariadic.h"

#include "src/ir/kernel_ir.h"
#include "src/utils/data_structure_utils.h"
#include "src/utils/printIR.h"

using namespace mlir;
using namespace kernelir;
using namespace mlir::utils;

#include "src/ir/geometric_transform/GeometricTransformAttr.cc"
#include "src/ir/kernel_ir_structs.cc.inc"

KernelIRDialect::KernelIRDialect(MLIRContext *context)
    : Dialect(getDialectNamespace(), context, TypeID::get<KernelIRDialect>()) {
  addOperations<
#define GET_OP_LIST
#include "src/ir/kernel_ir.cc.inc"
      >();

  addAttributes<::mlir::AffineTransformAttr>();
}

namespace mlir {
namespace kernelir {

SmallVector<Value, 1> ExtractKernelInputValues(Operation *op) {
  SmallVector<Value, 1> inputs;
  Region &region_instruction = op->getRegion(0);
  Block &block_kernel_region = region_instruction.front();
  for (unsigned b = 0; b < block_kernel_region.getNumArguments(); b++)
    inputs.push_back(block_kernel_region.getArgument(b));
  return inputs;
}

SmallVector<Value, 1> ExtractKernelWeightValues(Operation *op) {
  SmallVector<Value, 1> outputs;

  Region &region_instruction = op->getRegion(0);
  Block &block_kernel_region = region_instruction.front();

  llvm::iplist<Operation>::iterator i;
  for (i = block_kernel_region.begin(); i != block_kernel_region.end(); i++) {
    if ((llvm::isa<scheduleir::Conv2DOp>(*i)) ||
        (llvm::isa<scheduleir::DepthwiseConv2DOp>(*i)) ||
        (llvm::isa<scheduleir::FullyConnectedOp>(*i))) {
      Operation *op_const = i->getOperand(1).getDefiningOp();
      if (llvm::isa<scheduleir::DmaOp>(op_const))
        op_const = op_const->getOperand(0).getDefiningOp();
      outputs.push_back(op_const->getResult(0));
    }
  }
  return outputs;
}

SmallVector<Value, 1> ExtractKernelBiasValues(Operation *op) {
  SmallVector<Value, 1> outputs;

  Region &region_instruction = op->getRegion(0);
  Block &block_kernel_region = region_instruction.front();

  llvm::iplist<Operation>::iterator i;
  for (i = block_kernel_region.begin(); i != block_kernel_region.end(); i++) {
    if ((llvm::isa<scheduleir::Conv2DOp>(*i)) ||
        (llvm::isa<scheduleir::DepthwiseConv2DOp>(*i)) ||
        (llvm::isa<scheduleir::FullyConnectedOp>(*i))) {
      Operation *op_const = i->getOperand(2).getDefiningOp();
      if (llvm::isa<scheduleir::DmaOp>(op_const))
        op_const = op_const->getOperand(0).getDefiningOp();
      outputs.push_back(op_const->getResult(0));
    }
  }
  return outputs;
}

SmallVector<Value, 1> ExtractEwiseKernelConstValues(Operation *op) {
  SmallVector<Value, 1> outputs;

  Region &region_instruction = op->getRegion(0);
  Block &block_kernel_region = region_instruction.front();

  llvm::iplist<Operation>::iterator i;
  for (i = block_kernel_region.begin(); i != block_kernel_region.end(); i++) {
    if (llvm::isa<scheduleir::AddOp>(*i) || llvm::isa<scheduleir::MulOp>(*i)) {
      for (auto operand : i->getOperands()) {
        Operation *op_const = operand.getDefiningOp();
        if (op_const) {
          if (llvm::isa<scheduleir::DmaOp>(op_const))
            op_const = op_const->getOperand(0).getDefiningOp();
          else if (llvm::isa<scheduleir::ConstOp>(op_const))
            op_const; // do nothing
          else
            continue;
          outputs.push_back(op_const->getResult(0));
        }
      }
    }
  }
  return outputs;
}

SmallVector<Value, 1> ExtractKernelIntermediateValues(Operation *op) {
  // std::cout << "ExtractKernelIntermediateValues::Start" << std::endl;
  SmallVector<Value, 1> intermediates;
  Region &region_instruction = op->getRegion(0);
  Block &block_kernel_region = region_instruction.front();
  llvm::iplist<Operation>::iterator i;
  for (i = block_kernel_region.begin(); i != block_kernel_region.end(); i++) {
    // std::cout << "ExtractKernelIntermediateValues::EvaluateOp" << std::endl;
    if ((llvm::isa<scheduleir::Conv2DOp>(*i)) ||
        (llvm::isa<scheduleir::DepthwiseConv2DOp>(*i)) ||
        (llvm::isa<scheduleir::FullyConnectedOp>(*i))) {
      Operation *op_const_weight;

      op_const_weight = i->getOperand(1).getDefiningOp();
      if (llvm::isa<scheduleir::DmaOp>(op_const_weight))
        intermediates.push_back(i->getOperand(1));

      op_const_weight = i->getOperand(2).getDefiningOp();
      if (llvm::isa<scheduleir::DmaOp>(op_const_weight))
        intermediates.push_back(i->getOperand(2));
    }
  }
  // std::cout << "ExtractKernelIntermediateValues::End" << std::endl;
  return intermediates;
}

SmallVector<Value, 1> ExtractKernelOutputValues(Operation *op) {
  SmallVector<Value, 1> outputs;
  for (unsigned b = 0; b < op->getNumResults(); b++)
    outputs.push_back(op->getResult(b));
  return outputs;
}

SmallVector<Value, 1> ExtractAllKernelValues(Operation *op) {
  SmallVector<Value, 1> inputs = ExtractKernelInputValues(op);
  SmallVector<Value, 1> weights = ExtractKernelWeightValues(op);
  SmallVector<Value, 1> bias = ExtractKernelBiasValues(op);
  SmallVector<Value, 1> intermediate = ExtractKernelIntermediateValues(op);
  SmallVector<Value, 1> outputs = ExtractKernelOutputValues(op);

  /*
  std::cout << "Inputs " << inputs.size() << std::endl;
  std::cout << "Weights " << weights.size() << std::endl;
  std::cout << "Bias " << bias.size() << std::endl;
  std::cout << "Intermediate " << intermediate.size() << std::endl;
  std::cout << "Outputs " << outputs.size() << std::endl;
  */

  SmallVector<Value, 1> Kernel_values;
  for (auto v : inputs)
    Kernel_values.push_back(v);
  for (auto v : weights)
    Kernel_values.push_back(v);
  for (auto v : bias)
    Kernel_values.push_back(v);
  for (auto v : intermediate)
    Kernel_values.push_back(v);
  for (auto v : outputs)
    Kernel_values.push_back(v);
  return Kernel_values;
}

Operation *getKernelMajorOp(Operation *op, bool check) {
  ASSERT_COND((op->getNumRegions() != 1),
              "getKernelMajorOp:illegal Number of regions");
  Region &region = op->getRegion(0);
  ASSERT_COND((region.getBlocks().size() != 1),
              "getKernelMajorOp:illegal Number of blocks");
  Block &block = region.front();

  llvm::iplist<Operation> &operations = block.getOperations();
  for (auto &o : operations) {
    if (llvm::isa<scheduleir::Conv2DOp>(o))
      return &o;
    else if (llvm::isa<scheduleir::DepthwiseConv2DOp>(o))
      return &o;
    else if (llvm::isa<scheduleir::FullyConnectedOp>(o))
      return &o;
    else if (llvm::isa<scheduleir::AddOp>(o))
      return &o;
    else if (llvm::isa<scheduleir::MulOp>(o))
      return &o;
    else if ((llvm::isa<scheduleir::MaxPool2dOp>(o)) or
             (llvm::isa<scheduleir::AvgPool2dOp>(o)))
      return &o;
    else if (llvm::isa<scheduleir::MemoryOp>(o))
      return &o;
    else if (llvm::isa<scheduleir::CastInOp>(o))
      return &o;
    else if (llvm::isa<scheduleir::CastOutOp>(o))
      return &o;
  }
  if (check) {
    printOp(*op);
    printNextlevelOp(*op, 1, info_option::off, 100);
    FATAL_ERROR(
        "getKernelMajorOp::Unable to derive the major Op of this Kernel")
  } else
    return nullptr;
}

Operation *getKernelOperationwithRescaleAttributes(Operation *op) {
  ASSERT_COND(not(llvm::isa<kernelir::KernelRegionOp>(op)),
              "getKernelRescaleOps can only be executed on kernel ops");
  SmallVector<Operation *, 1> rescale_ops;
  Region &kernel_region = op->getRegion(0);
  Block &kernel_block = kernel_region.front();
  llvm::iplist<Operation> &kernel_operations = kernel_block.getOperations();
  for (auto &o : kernel_operations) {
    if (llvm::isa<scheduleir::Conv2DOp>(o))
      return &o;
    else if (llvm::isa<scheduleir::DepthwiseConv2DOp>(o))
      return &o;
    else if (llvm::isa<scheduleir::FullyConnectedOp>(o))
      return &o;
    else if (llvm::isa<scheduleir::MaxPool2dOp>(o))
      return &o;
    else if (llvm::isa<scheduleir::AvgPool2dOp>(o))
      return &o;
    else if (llvm::isa<scheduleir::AddOp>(o))
      return &o;
    else if (llvm::isa<scheduleir::MulOp>(o))
      return &o;
  }
  return nullptr;
}

SmallVector<Operation *, 1> getKernelActivationsOps(Operation *op) {
  ASSERT_COND(not(llvm::isa<kernelir::KernelRegionOp>(op)),
              "getKernelActivation can only be executed on kernel ops");
  SmallVector<Operation *, 1> activations;
  Region &kernel_region = op->getRegion(0);
  Block &kernel_block = kernel_region.front();
  llvm::iplist<Operation> &kernel_operations = kernel_block.getOperations();
  for (auto &op_k : kernel_operations) {
    if (llvm::isa<scheduleir::ClampOp>(&op_k)) {
      activations.push_back(&op_k);
    }
  }
  return activations;
}

mlir::utils::NpuBlockType getKernelBlockType(Operation *op) {
  return (mlir::utils::NpuBlockType)dyn_cast<kernelir::KernelRegionOp>(op)
      .block_type();
}

mlir::utils::SliceViewMetaData getKernelSliceView(Operation *op) {
  int32_t axis = 0;
  int32_t offset = 0;

  Region &region = op->getRegion(0);
  Block &block = region.front();
  llvm::iplist<Operation> &operations = block.getOperations();
  int64_t sliceViewCount = 0;
  for (auto &o : operations) {
    if (llvm::isa<scheduleir::SliceViewOp>(o)) {
      sliceViewCount++;
      axis = o.getAttrOfType<mlir::IntegerAttr>("axis").getInt();
      offset = o.getAttrOfType<mlir::IntegerAttr>("start").getInt();
    }
  }
  ASSERT_COND(sliceViewCount > 1, "getKernelSliceView::Detected more than a "
                                  "Single SliceView within Kernel");
  return mlir::utils::SliceViewMetaData(axis, offset);
}

mlir::utils::FilterMetaData getKernelFilter(Operation *op) {
  int32_t w = 0;
  int32_t h = 0;
  int32_t sx = 1;
  int32_t sy = 1;
  int32_t dx = 1;
  int32_t dy = 1;
  int32_t top = 0;
  int32_t bottom = 0;
  int32_t left = 0;
  int32_t right = 0;
  Operation *kernel_major_op = mlir::kernelir::getKernelMajorOp(op);
  if ((llvm::isa<scheduleir::Conv2DOp>(*kernel_major_op)) ||
      (llvm::isa<scheduleir::DepthwiseConv2DOp>(*kernel_major_op)) ||
      (llvm::isa<scheduleir::FullyConnectedOp>(*kernel_major_op))) {
    std::vector<int32_t> strides = ArrayAttr_to_Vector(
        kernel_major_op->getAttrOfType<ArrayAttr>("stride"));
    sx = strides[1];
    sy = strides[0];
    std::vector<int32_t> dilation = ArrayAttr_to_Vector(
        kernel_major_op->getAttrOfType<ArrayAttr>("dilation"));
    dx = dilation[1];
    dy = dilation[0];
    ;
    std::vector<int32_t> padding =
        ArrayAttr_to_Vector(kernel_major_op->getAttrOfType<ArrayAttr>("pad"));
    top = padding[0];
    bottom = padding[1];
    left = padding[2];
    right = padding[3];
    ArrayRef<int64_t> filter_shape;
    if (kernel_major_op->getOperand(1)
            .getType()
            .isa<mlir::RankedTensorType>()) {
      filter_shape = kernel_major_op->getOperand(1)
                         .getType()
                         .dyn_cast<RankedTensorType>()
                         .getShape(); // OHWI
      w = filter_shape[2];
      h = filter_shape[1];
    } else {
      mlir::SchedTensorType type =
          kernel_major_op->getOperand(1).getType().dyn_cast<SchedTensorType>();
      filter_shape = type.getShape(); // OHWI or HWIO
      if (type.get_format() == value_format::vf_HWIO) {
        w = filter_shape[1];
        h = filter_shape[0];
      } else if (type.get_format() == value_format::vf_HWOI) {
        w = filter_shape[1];
        h = filter_shape[0];
      } else if ((type.get_format() == value_format::vf_WeightCompressed) and
                 (llvm::isa<scheduleir::DepthwiseConv2DOp>(*kernel_major_op))) {
        w = filter_shape[1];
        h = filter_shape[0]; // Assumed HWOI
      } else if ((type.get_format() == value_format::vf_WeightCompressed) and
                 (llvm::isa<scheduleir::Conv2DOp>(*kernel_major_op))) {
        w = filter_shape[1];
        h = filter_shape[0]; // Assumed HWIO
      } else
        FATAL_ERROR("mlir::utils::FilterMetaData getKernelFilter unknown "
                    "weight tensor format");
    }
  } else if ((llvm::isa<scheduleir::AvgPool2dOp>(*kernel_major_op)) ||
             (llvm::isa<scheduleir::MaxPool2dOp>(*kernel_major_op))) {
    std::vector<int32_t> kernel_size = ArrayAttr_to_Vector(
        kernel_major_op->getAttrOfType<ArrayAttr>("kernel"));
    w = kernel_size[1];
    h = kernel_size[0];
    std::vector<int32_t> strides = ArrayAttr_to_Vector(
        kernel_major_op->getAttrOfType<ArrayAttr>("stride"));
    sx = strides[1];
    sy = strides[0];
    std::vector<int32_t> padding =
        ArrayAttr_to_Vector(kernel_major_op->getAttrOfType<ArrayAttr>("pad"));
    top = padding[0];
    bottom = padding[1];
    left = padding[2];
    right = padding[3];
  }
  return mlir::utils::FilterMetaData(w, h, sx, sy, dx, dy, top, bottom, left,
                                     right);
}

bool isKernelMemoryOp(Operation *op) {
  ASSERT_COND(not(llvm::isa<kernelir::KernelRegionOp>(op)),
              "isKernelMemoryOp can only be executed on kernel ops");

  Region &kernel_region = op->getRegion(0);
  Block &kernel_block = kernel_region.front();

  llvm::iplist<Operation> &kernel_operations = kernel_block.getOperations();
  for (auto &op_k : kernel_operations) {
    if (llvm::isa<scheduleir::MemoryOp>(&op_k))
      return true;
  }
  return false;
}

} // namespace kernelir
} // end namespace mlir

#define GET_OP_CLASSES
#include "src/ir/kernel_ir.cc.inc"

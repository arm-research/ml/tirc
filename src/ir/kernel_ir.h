/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#ifndef KERNEL_IR_H
#define KERNEL_IR_H

#include <unordered_map>

#include "mlir/Dialect/Quant/QuantOps.h"          // TF:local_config_mlir
#include "mlir/Dialect/Traits.h"                  // TF:local_config_mlir
#include "mlir/IR/Attributes.h"                   // TF:local_config_mlir
#include "mlir/IR/Builders.h"                     // TF:local_config_mlir
#include "mlir/IR/Dialect.h"                      // TF:local_config_mlir
#include "mlir/IR/OpDefinition.h"                 // TF:local_config_mlir
#include "mlir/Interfaces/LoopLikeInterface.h"    // from @llvm-project
#include "mlir/Interfaces/SideEffectInterfaces.h" // from @llvm-project
#include "mlir/Support/LLVM.h"                    // TF:local_config_mlir

#include "src/ir/architecture_config/architecture_config.h"
#include "src/ir/kernel_ir_structs.h.inc"
#include "src/ir/schedule_ir.h"
#include "src/utils/assert.h"

namespace mlir {
namespace kernelir {

class KernelIRDialect : public Dialect {

public:
  explicit KernelIRDialect(MLIRContext *context);

  static StringRef getDialectNamespace() { return "kernelir"; }

  /// Parse an instance of a type registered to the toy dialect.
  mlir::Attribute parseAttribute(mlir::DialectAsmParser &parser) const {};

  /// Print an instance of a type registered to the toy dialect.
  void printAttribute(mlir::Attribute attr,
                      mlir::DialectAsmPrinter &printer) const override {
    printer << "GeometricTransformAttr";
  };
};

SmallVector<Value, 1> ExtractAllKernelValues(Operation *op);

SmallVector<Value, 1>
ExtractKernelWeightValues(Operation *op); // Return 2nd Storage Weights
SmallVector<Value, 1>
ExtractKernelBiasValues(Operation *op); // Return 2nd Storage Bias
SmallVector<Value, 1> ExtractEwiseKernelConstValues(Operation *op);

SmallVector<Value, 1> ExtractKernelIntermediateValues(
    Operation *op); // Returns the Primary Storage Weights, Bias

SmallVector<Value, 1> ExtractKernelInputValues(Operation *op);
SmallVector<Value, 1> ExtractKernelOutputValues(Operation *op);

Operation *getKernelMajorOp(Operation *op, bool check = true);
Operation *getKernelOperationwithRescaleAttributes(Operation *op);
SmallVector<Operation *, 1> getKernelActivationsOps(Operation *op);

mlir::utils::NpuBlockType getKernelBlockType(Operation *op);
mlir::utils::FilterMetaData getKernelFilter(Operation *op);
mlir::utils::SliceViewMetaData getKernelSliceView(Operation *op);

bool isKernelMemoryOp(Operation *op);

} // namespace kernelir
} // end namespace mlir

#define GET_OP_CLASSES
#include "src/ir/kernel_ir.h.inc"

#include "src/ir/geometric_transform/GeometricTransformAttr.h"

#endif // KERNEL_IR_H

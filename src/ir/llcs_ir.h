/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#ifndef LLCS_IR_H
#define LLCS_IR_H

#include "mlir/Dialect/Quant/QuantOps.h"          // TF:local_config_mlir
#include "mlir/Dialect/Traits.h"                  // TF:local_config_mlir
#include "mlir/IR/Attributes.h"                   // TF:local_config_mlir
#include "mlir/IR/Builders.h"                     // TF:local_config_mlir
#include "mlir/IR/Dialect.h"                      // TF:local_config_mlir
#include "mlir/IR/OpDefinition.h"                 // TF:local_config_mlir
#include "mlir/Interfaces/LoopLikeInterface.h"    // from @llvm-project
#include "mlir/Interfaces/SideEffectInterfaces.h" // from @llvm-project
#include "mlir/Support/LLVM.h"                    // TF:local_config_mlir

namespace mlir {
namespace llcs {
class LLCS : public mlir::Dialect {
public:
  explicit LLCS(mlir::MLIRContext *ctx);

  /// Provide a utility accessor to the dialect namespace. This is used by
  /// several utilities.
  static llvm::StringRef getDialectNamespace() { return "llcs"; }
};

} // namespace llcs
} // namespace mlir

#define GET_OP_CLASSES
#include "src/ir/llcs_ir.h.inc"

#endif // LLCS_IR_H

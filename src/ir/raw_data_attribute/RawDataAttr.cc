/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#include "src/ir/raw_data_attribute/RawDataAttr.h"

namespace mlir {

namespace detail {
struct RawDataAttrStorage : public ::mlir::AttributeStorage {
  RawDataAttrStorage(attr_unique_id unique_id) : unique_id(unique_id) {}

  /// The hash key is a tuple of the parameter types.
  using KeyTy = std::tuple<attr_unique_id>;
  bool operator==(const KeyTy &key) const {
    if (!(unique_id == std::get<0>(key)))
      return false;
    return true;
  }
  static ::llvm::hash_code hashKey(const KeyTy &key) {
    return ::llvm::hash_combine(std::get<0>(key));
  }

  /// Define a construction method for creating a new instance of this storage.
  static RawDataAttrStorage *
  construct(::mlir::AttributeStorageAllocator &allocator, const KeyTy &key) {
    auto unique_id = std::get<0>(key);
    return new (allocator.allocate<RawDataAttrStorage>())
        RawDataAttrStorage(unique_id);
  }

  attr_unique_id getUniqueId() const { return unique_id; }

  void *getData() const { return data.get(); }
  int64_t getSize() const { return size; }
  int64_t getElementSize() const { return element_size; }

  void setData(std::unique_ptr<unsigned char[]> data_o) {
    data = std::move(data_o);
  }
  void setSize(int64_t size_o) { size = size_o; }
  void setElementSize(int64_t element_size_o) { element_size = element_size_o; }

  std::unique_ptr<unsigned char[]> data;
  int64_t size;
  int64_t element_size;

  attr_unique_id unique_id;
};
} // namespace detail

RawDataAttr RawDataAttr::get(MLIRContext *context,
                             std::unique_ptr<unsigned char[]> data,
                             int64_t size, int64_t element_size,
                             attr_unique_id unique_id) {
  auto attr = Base::get(context, unique_id);
  attr.getImpl()->setData(std::move(data));
  attr.getImpl()->setSize(size);
  attr.getImpl()->setElementSize(element_size);
  return attr;
}

attr_unique_id RawDataAttr::getUniqueId() const {
  return getImpl()->getUniqueId();
}

void *RawDataAttr::getData() const { return getImpl()->getData(); }

int64_t RawDataAttr::getSize() const { return getImpl()->getSize(); }

int64_t RawDataAttr::getElementSize() const {
  return getImpl()->getElementSize();
}

} // namespace mlir

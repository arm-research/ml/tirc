/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#ifndef RAW_DATA_ATTR_H
#define RAW_DATA_ATTR_H

#include "mlir/IR/AffineMap.h"  // from @llvm-project
#include "mlir/IR/Attributes.h" // from @llvm-project
#include "mlir/IR/Builders.h"
#include "mlir/IR/BuiltinAttributes.h"
#include "mlir/IR/BuiltinOps.h"
#include "mlir/IR/Location.h"    // from @llvm-project
#include "mlir/IR/MLIRContext.h" // from @llvm-project

namespace mlir {
class DialectAsmParser;
class DialectAsmPrinter;
} // namespace mlir

using attr_unique_id = uint64_t;

namespace mlir {
class RawDataAttr;

namespace detail {
struct RawDataAttrStorage;
} // end namespace detail

class RawDataAttr
    : public ::mlir::Attribute::AttrBase<RawDataAttr, ::mlir::Attribute,
                                         detail::RawDataAttrStorage> {
public:
  /// Inherit some necessary constructors from 'AttrBase'.
  using Base::Base;

  static RawDataAttr get(MLIRContext *context,
                         std::unique_ptr<unsigned char[]> data, int64_t size,
                         int64_t element_size, attr_unique_id unique_id);

  attr_unique_id getUniqueId() const;
  void *getData() const;
  int64_t getSize() const;
  int64_t getElementSize() const;
};

} // namespace mlir

#endif // GEOMETRIC_TRANSFORM_ATTR_H

/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#include "src/ir/sched_tensor/SchedTensorType.h"

#include <algorithm>
#include <cstdint>
#include <functional>
#include <numeric>
#include <string>
#include <type_traits>

#include "mlir/Dialect/Traits.h"           // TF:local_config_mlir
#include "mlir/IR/Attributes.h"            // TF:local_config_mlir
#include "mlir/IR/Builders.h"              // TF:local_config_mlir
#include "mlir/IR/Diagnostics.h"           // TF:local_config_mlir
#include "mlir/IR/DialectImplementation.h" // TF:local_config_mlir
#include "mlir/IR/MLIRContext.h"           // TF:local_config_mlir
#include "mlir/IR/Matchers.h"              // TF:local_config_mlir
#include "mlir/IR/OpImplementation.h"      // TF:local_config_mlir
#include "mlir/IR/PatternMatch.h"          // TF:local_config_mlir
#include "mlir/IR/TypeUtilities.h"         // TF:local_config_mlir
#include "mlir/IR/Types.h"                 // TF:local_config_mlir
#include "mlir/IR/Value.h"                 // TF:local_config_mlir
#include "mlir/Parser.h"                   // TF:local_config_mlir
#include "mlir/Support/LLVM.h"             // TF:local_config_mlir
#include "mlir/Support/LogicalResult.h"    // TF:local_config_mlir
#include "mlir/Transforms/FoldUtils.h"     // TF:local_config_mlir
#include "mlir/Transforms/InliningUtils.h" // TF:local_config_mlir
#include "mlir/Transforms/RegionUtils.h"   // TF:local_config_mlir
#include "llvm/ADT/Sequence.h"
#include "llvm/ADT/SmallVector.h"
#include "llvm/ADT/StringExtras.h"
#include "llvm/ADT/StringRef.h"
#include "llvm/ADT/StringSwitch.h"
#include "llvm/ADT/iterator_range.h"
#include "llvm/Support/FormatVariadic.h"

using namespace mlir::utils;

namespace mlir {

// Check if "elementType" can be an element type of a tensor.
static LogicalResult
checkTensorElementType(function_ref<InFlightDiagnostic()> emitError,
                       Type elementType) {
  if (!TensorType::isValidElementType(elementType))
    return emitError() << "invalid tensor element type: " << elementType;
  return success();
}

namespace detail {

struct SchedTensorTypeStorage : public ::mlir::TypeStorage {
  SchedTensorTypeStorage(::llvm::ArrayRef<int64_t> shape, Type elementType,
                         value_unique_id unique_id)
      : shape(shape), elementType(elementType), unique_id(unique_id) {

    storage_shape = {0, 0, 0, 0};
    bandwidth_shape = {0, 0, 0, 0};

    mem_area = value_mem_area::ma_Unknown;
    mem_type = value_mem_type::mt_Unknown;
    purpose = value_purpose::vp_Unknown;
    sub_purpose = value_sub_purpose::vsp_Unknown;
    format = value_format::vf_Unknown;
    address = -1;
    start_time = -1;
    end_time = -1;

    storage_compression_scale = 1.0;

    block_traversal = NpuBlockTraversal::tbt_Default;

    resamplingmode = rm_NONE;

    compression_scale_for_worst_weight_stream = 1.0;
    storage_compression_scale = 1.0;
    bandwidth_compression_scale = 1.0;

    io_memory_op = false;

    block_type_cache = -1;
    ofm_block_depth_cache = -1;
    ofm_depth_step_cache = -1;
  }

  /// The hash key is a tuple of the parameter types.
  using KeyTy = std::tuple<::llvm::ArrayRef<int64_t>, Type, value_unique_id>;

  bool operator==(const KeyTy &key) const {
    if (!(shape == std::get<0>(key)))
      return false;
    if (!(elementType == std::get<1>(key)))
      return false;
    if (!(unique_id == std::get<2>(key)))
      return false;
    return true;
  }

  static ::llvm::hash_code hashKey(const KeyTy &key) {
    return ::llvm::hash_combine(std::get<0>(key), std::get<1>(key),
                                std::get<2>(key));
  }

  /// Define a construction method for creating a new instance of this storage.
  static SchedTensorTypeStorage *
  construct(::mlir::TypeStorageAllocator &allocator, const KeyTy &key) {

    auto shape = std::get<0>(key);
    auto elementType = std::get<1>(key);
    auto unique_id = std::get<2>(key);

    shape = allocator.copyInto(shape);

    return new (allocator.allocate<SchedTensorTypeStorage>())
        SchedTensorTypeStorage(shape, elementType, unique_id);
  }

  ArrayRef<int64_t> getShape() const { return shape; }

  value_unique_id getUniqueId() const { return unique_id; }

  /**************************************************************************/
  /****************  NPU Specific Tensor Type Access ************************/
  /**************************************************************************/

  ArrayRef<int64_t> getStorageShape() const { return storage_shape; }
  ArrayRef<int64_t> getBandwidthShape() const { return bandwidth_shape; }

  void setStorageShape(SmallVector<int64_t, 4> shape) { storage_shape = shape; }
  void setBandwidthShape(SmallVector<int64_t, 4> shape) {
    bandwidth_shape = shape;
  }

  value_mem_area get_mem_area() { return mem_area; }
  void set_mem_area(value_mem_area v) { mem_area = v; }

  value_mem_type get_mem_type() { return mem_type; }
  void set_mem_type(value_mem_type v) { mem_type = v; }

  value_purpose get_purpose() { return purpose; }
  void set_purpose(value_purpose v) { purpose = v; }

  value_sub_purpose get_sub_purpose() { return sub_purpose; }
  void set_sub_purpose(value_sub_purpose v) { sub_purpose = v; }

  value_format get_format() { return format; }
  void set_format(value_format v) { format = v; }

  int64_t get_address() { return address; }
  void set_address(int64_t a) { address = a; }

  int64_t get_start_time() { return start_time; }
  void set_start_time(int64_t a) { start_time = a; }

  int64_t get_end_time() { return end_time; }
  void set_end_time(int64_t a) { end_time = a; }

  int32_t get_alignment() { return alignment; }
  void set_alignment(unsigned a) { alignment = a; }

  array_1D_i32 get_storage_rounding_quantum() {
    return storage_rounding_quantum;
  }
  void set_storage_rounding_quantum(array_1D_i32 value) {
    storage_rounding_quantum = value;
  }

  array_1D_i32 get_brick_size() { return brick_size; }
  void set_brick_size(array_1D_i32 value) { brick_size = value; }

  NpuBlockTraversal get_NpuBlockTraversal() { return block_traversal; }
  void set_NpuBlockTraversal(NpuBlockTraversal value) {
    block_traversal = value;
  }

  resampling_mode get_resampling_mode() { return resamplingmode; }
  void set_resampling_mode(resampling_mode value) { resamplingmode = value; }

  bool get_avoid_NHCWB16() { return avoid_NHCWB16; }
  void set_avoid_NHCWB16(bool value) { avoid_NHCWB16 = value; }

  float get_compression_scale_for_worst_weight_stream() const {
    return compression_scale_for_worst_weight_stream;
  }
  void set_compression_scale_for_worst_weight_stream(float value) {
    compression_scale_for_worst_weight_stream = value;
  }

  bool get_weight_transpose_depthwise() const {
    return weight_transpose_depthwise;
  };
  void set_weight_transpose_depthwise(bool value) {
    weight_transpose_depthwise = value;
  };

  float get_storage_compression_scale() const {
    return storage_compression_scale;
  };
  void set_storage_compression_scale(float value) {
    storage_compression_scale = value;
  };

  float get_bandwidth_compression_scale() const {
    return bandwidth_compression_scale;
  };
  void set_bandwidth_compression_scale(float value) {
    bandwidth_compression_scale = value;
  };

  array_1D_fp64 get_weight_compression_scales() const {
    return weight_compression_scales;
  };
  void set_weight_compression_scales(array_1D_fp64 scales) {
    weight_compression_scales = scales;
  };

  weightcompression_config get_weight_compression_config() const {
    return weight_compression_config;
  };
  void set_weight_compression_config(weightcompression_config config) {
    weight_compression_config = config;
  };

  array_1D_i32 get_weight_compressed_offsets() const {
    return weight_compressed_offsets;
  };
  void set_weight_compressed_offsets(array_1D_i32 offsets) {
    weight_compressed_offsets = offsets;
  };

  array_2D_i32 get_compressed_values_substream_offsets() const {
    return compressed_values_substream_offsets;
  };
  void set_compressed_values_substream_offsets(array_2D_i32 offsets) {
    compressed_values_substream_offsets = offsets;
  };

  int64_t get_pinned_size() { return pinned_size; }
  void set_pinned_size(int64_t a) { pinned_size = a; }

  value_mem_area get_pinned_mem_area() { return pinned_mem_area; }
  void set_pinned_mem_area(value_mem_area v) { pinned_mem_area = v; }

  bool get_io_memory_op() { return io_memory_op; }
  void set_io_memory_op(bool v) { io_memory_op = v; }

  void setWeightCompressionCache_BlockType(int32_t v) { block_type_cache = v; }
  void setWeightCompressionCache_OFMBlockDepth(uint32_t v) {
    ofm_block_depth_cache = v;
  }
  void setWeightCompressionCache_OFMDepthStep(uint32_t v) {
    ofm_depth_step_cache = v;
  }

  int32_t getWeightCompressionCache_BlockType() { return block_type_cache; }
  uint32_t getWeightCompressionCache_OFMBlockDepth() {
    return ofm_block_depth_cache;
  }
  uint32_t getWeightCompressionCache_OFMDepthStep() {
    return ofm_depth_step_cache;
  }

  /*
     Shapes are Array Refs and not a compsite structure. as they just indicate
     the shape of a tensors The dimension labels are really a consuming operator
     linked and honestly should be held in the operator but to simplify we have
     the format field here which we may want to move in the future potentially
     using the IRFormat pass.
   */
  ::llvm::ArrayRef<int64_t> shape;
  SmallVector<int64_t, 4> storage_shape;
  SmallVector<int64_t, 4> bandwidth_shape;

  Type elementType;
  value_unique_id unique_id;

  value_mem_area mem_area;
  value_mem_type mem_type;
  value_purpose purpose;
  value_sub_purpose sub_purpose;
  value_format format;

  int32_t alignment;
  array_1D_i32 storage_rounding_quantum;
  array_1D_i32 brick_size;
  NpuBlockTraversal block_traversal;
  resampling_mode resamplingmode;
  bool avoid_NHCWB16;

  float compression_scale_for_worst_weight_stream;
  bool weight_transpose_depthwise;
  float storage_compression_scale;
  float bandwidth_compression_scale;
  array_1D_fp64 weight_compression_scales;
  weightcompression_config weight_compression_config;
  array_1D_i32 weight_compressed_offsets;
  array_2D_i32 compressed_values_substream_offsets;

  int64_t address;

  int32_t start_time;
  int32_t end_time;

  value_mem_area pinned_mem_area;
  int64_t pinned_size;

  bool io_memory_op;

  int32_t block_type_cache;
  uint32_t ofm_block_depth_cache;
  uint32_t ofm_depth_step_cache;

  /*
    QuantinzationInfo stored in Quantization portion of the Type
  */
};

} // namespace detail

//===----------------------------------------------------------------------===//
// SchedTensorType
//===----------------------------------------------------------------------===//
SchedTensorType SchedTensorType::get(ArrayRef<int64_t> shape, Type elementType,
                                     value_unique_id unique_id) {
  return Base::get(elementType.getContext(), shape, elementType, unique_id);
}

SchedTensorType SchedTensorType::getChecked(
    llvm::function_ref<::mlir::InFlightDiagnostic()> emitErrorFn,
    ArrayRef<int64_t> shape, Type elementType, value_unique_id unique_id) {
  return Base::getChecked(emitErrorFn, elementType.getContext(), shape,
                          elementType, unique_id);
}

::mlir::LogicalResult SchedTensorType::verify(
    ::llvm::function_ref<::mlir::InFlightDiagnostic()> emitError,
    ::llvm::ArrayRef<int64_t> shape, Type elementType,
    value_unique_id unique_id) {
  for (int64_t s : shape)
    if (s < -1)
      return emitError() << "invalid tensor dimension size";
  return checkTensorElementType(emitError, elementType);
}

Type SchedTensorType::getElementType() const { return getImpl()->elementType; }

ArrayRef<int64_t> SchedTensorType::getShape() const {
  return getImpl()->getShape();
}

uint64_t SchedTensorType::getUniqueId() const {
  return getImpl()->getUniqueId();
}

/**************************************************************************/
/****************  NPU Specific Tensor Type Access ************************/
/**************************************************************************/

ArrayRef<int64_t> SchedTensorType::getStorageShape() const {
  return getImpl()->getStorageShape();
}
ArrayRef<int64_t> SchedTensorType::getBandwidthShape() const {
  return getImpl()->getBandwidthShape();
}

void SchedTensorType::setStorageShape(SmallVector<int64_t, 4> shape) {
  getImpl()->setStorageShape(shape);
}
void SchedTensorType::setBandwidthShape(SmallVector<int64_t, 4> shape) {
  getImpl()->setBandwidthShape(shape);
}

value_mem_area SchedTensorType::get_mem_area() const {
  return getImpl()->get_mem_area();
}
void SchedTensorType::set_mem_area(value_mem_area v) {
  getImpl()->set_mem_area(v);
}

value_mem_type SchedTensorType::get_mem_type() const {
  return getImpl()->get_mem_type();
}
void SchedTensorType::set_mem_type(value_mem_type v) {
  getImpl()->set_mem_type(v);
}

value_purpose SchedTensorType::get_purpose() const {
  return getImpl()->get_purpose();
}
void SchedTensorType::set_purpose(value_purpose v) {
  getImpl()->set_purpose(v);
}

value_sub_purpose SchedTensorType::get_sub_purpose() const {
  return getImpl()->get_sub_purpose();
}
void SchedTensorType::set_sub_purpose(value_sub_purpose v) {
  getImpl()->set_sub_purpose(v);
}

value_format SchedTensorType::get_format() const {
  return getImpl()->get_format();
}
void SchedTensorType::set_format(value_format v) { getImpl()->set_format(v); }

float SchedTensorType::get_compression_scale_for_worst_weight_stream() const {
  return getImpl()->get_compression_scale_for_worst_weight_stream();
}
void SchedTensorType::set_compression_scale_for_worst_weight_stream(
    float value) {
  getImpl()->set_compression_scale_for_worst_weight_stream(value);
}

bool SchedTensorType::get_weight_transpose_depthwise() const {
  return getImpl()->get_weight_transpose_depthwise();
}
void SchedTensorType::set_weight_transpose_depthwise(bool value) {
  getImpl()->set_weight_transpose_depthwise(value);
}

float SchedTensorType::get_storage_compression_scale() const {
  return getImpl()->get_storage_compression_scale();
}
void SchedTensorType::set_storage_compression_scale(float value) {
  getImpl()->set_storage_compression_scale(value);
}

float SchedTensorType::get_bandwidth_compression_scale() const {
  return getImpl()->get_bandwidth_compression_scale();
}
void SchedTensorType::set_bandwidth_compression_scale(float value) {
  getImpl()->set_bandwidth_compression_scale(value);
}

array_1D_fp64 SchedTensorType::get_weight_compression_scales() const {
  return getImpl()->get_weight_compression_scales();
}
void SchedTensorType::set_weight_compression_scales(array_1D_fp64 scales) {
  getImpl()->set_weight_compression_scales(scales);
}

weightcompression_config
SchedTensorType::get_weight_compression_config() const {
  return getImpl()->get_weight_compression_config();
}
void SchedTensorType::set_weight_compression_config(
    weightcompression_config config) {
  getImpl()->set_weight_compression_config(config);
}

array_1D_i32 SchedTensorType::get_weight_compressed_offsets() const {
  return getImpl()->get_weight_compressed_offsets();
}
void SchedTensorType::set_weight_compressed_offsets(array_1D_i32 offsets) {
  getImpl()->set_weight_compressed_offsets(offsets);
}

array_2D_i32 SchedTensorType::get_compressed_values_substream_offsets() const {
  return getImpl()->get_compressed_values_substream_offsets();
}
void SchedTensorType::set_compressed_values_substream_offsets(
    array_2D_i32 offsets) {
  getImpl()->set_compressed_values_substream_offsets(offsets);
}

int64_t SchedTensorType::get_address() const {
  return getImpl()->get_address();
}
void SchedTensorType::set_address(int64_t a) { getImpl()->set_address(a); }

int64_t SchedTensorType::get_start_time() const {
  return getImpl()->get_start_time();
}
void SchedTensorType::set_start_time(int64_t a) {
  getImpl()->set_start_time(a);
}

int64_t SchedTensorType::get_end_time() const {
  return getImpl()->get_end_time();
}
void SchedTensorType::set_end_time(int64_t a) { getImpl()->set_end_time(a); }

int32_t SchedTensorType::get_alignment() { return getImpl()->get_alignment(); }
void SchedTensorType::set_alignment(unsigned a) { getImpl()->set_alignment(a); }

array_1D_i32 SchedTensorType::get_storage_rounding_quantum() {
  return getImpl()->get_storage_rounding_quantum();
}
void SchedTensorType::set_storage_rounding_quantum(array_1D_i32 value) {
  getImpl()->set_storage_rounding_quantum(value);
}

array_1D_i32 SchedTensorType::get_brick_size() {
  return getImpl()->get_brick_size();
}
void SchedTensorType::set_brick_size(array_1D_i32 value) {
  getImpl()->set_brick_size(value);
}

NpuBlockTraversal SchedTensorType::get_NpuBlockTraversal() {
  return getImpl()->get_NpuBlockTraversal();
}
void SchedTensorType::set_NpuBlockTraversal(NpuBlockTraversal value) {
  getImpl()->set_NpuBlockTraversal(value);
}

resampling_mode SchedTensorType::get_resampling_mode() {
  return getImpl()->get_resampling_mode();
}
void SchedTensorType::set_resampling_mode(resampling_mode value) {
  getImpl()->set_resampling_mode(value);
}

bool SchedTensorType::get_avoid_NHCWB16() {
  return getImpl()->get_avoid_NHCWB16();
}
void SchedTensorType::set_avoid_NHCWB16(bool value) {
  getImpl()->set_avoid_NHCWB16(value);
}

int64_t SchedTensorType::get_pinned_size() {
  return getImpl()->get_pinned_size();
}
void SchedTensorType::set_pinned_size(int64_t a) {
  getImpl()->set_pinned_size(a);
}

value_mem_area SchedTensorType::get_pinned_mem_area() {
  return getImpl()->pinned_mem_area;
}
void SchedTensorType::set_pinned_mem_area(value_mem_area v) {
  getImpl()->set_pinned_mem_area(v);
}

bool SchedTensorType::get_io_memory_op() {
  return getImpl()->get_io_memory_op();
}
void SchedTensorType::set_io_memory_op(bool v) {
  getImpl()->set_io_memory_op(v);
}

void SchedTensorType::setWeightCompressionCache_BlockType(int32_t block_type) {
  getImpl()->setWeightCompressionCache_BlockType(block_type);
}
void SchedTensorType::setWeightCompressionCache_OFMBlockDepth(
    uint32_t ofm_block_depth) {
  getImpl()->setWeightCompressionCache_OFMBlockDepth(ofm_block_depth);
}
void SchedTensorType::setWeightCompressionCache_OFMDepthStep(
    uint32_t ofm_depth_step) {
  getImpl()->setWeightCompressionCache_OFMDepthStep(ofm_depth_step);
}

int32_t SchedTensorType::getWeightCompressionCache_BlockType() {
  return getImpl()->getWeightCompressionCache_BlockType();
}
uint32_t SchedTensorType::getWeightCompressionCache_OFMBlockDepth() {
  return getImpl()->getWeightCompressionCache_OFMBlockDepth();
}
uint32_t SchedTensorType::getWeightCompressionCache_OFMDepthStep() {
  return getImpl()->getWeightCompressionCache_OFMDepthStep();
}

namespace scheduleir {

bool weights_fit_sram(
    mlir::SchedTensorType type,
    mlir::ArchitectureConfig::ArchitectureConfigAttr &arch_c) {
  if (type.get_purpose() != vp_Weights)
    return true;

  unsigned min_weight_size = 0;
  ArrayRef<int64_t> shape = type.getShape();

  if (shape.size() == 4)
    min_weight_size =
        shape[0] * shape[1] * shape[2] * arch_c.OFMSplitDepth().getInt();
  else if (shape.size() == 2)
    min_weight_size = shape[0] * arch_c.OFMSplitDepth().getInt();

  // Need to be fit into Sram, as a double buffer
  float worst_buffer_size =
      (type.get_compression_scale_for_worst_weight_stream() * min_weight_size *
       2);
  if (worst_buffer_size > arch_c.sram_size().getInt())
    return false;
  return true;
}

void set_format(Value v, value_format format) {
  mlir::SchedTensorType v_t = v.getType().dyn_cast<mlir::SchedTensorType>();
  v_t.set_format(format);

  std::vector<int32_t> storage_rounding_quantum =
      get_storage_rounding_quantums(format);
  std::vector<int32_t> brick_size = get_brick_sizes(format);

  auto shape = v_t.getShape();

  SmallVector<int64_t, 4> storage_shape;
  for (int32_t i = 0; i < shape.size(); i++)
    storage_shape.push_back(
        round_up<int32_t>(shape[i], storage_rounding_quantum[i]));
  v_t.setStorageShape(storage_shape);

  SmallVector<int64_t, 4> bandwidth_shape;
  for (int32_t i = 0; i < shape.size(); i++)
    bandwidth_shape.push_back(round_up<int32_t>(shape[i], brick_size[i]));
  v_t.setBandwidthShape(bandwidth_shape);
}

void set_purpose(Value v, value_purpose purpose) {
  v.getType().dyn_cast<mlir::SchedTensorType>().set_purpose(purpose);
}

void set_sub_purpose(Value v, value_sub_purpose sub_purpose) {
  v.getType().dyn_cast<mlir::SchedTensorType>().set_sub_purpose(sub_purpose);
}

void set_alignment(Value v, int32_t alignment) {
  v.getType().dyn_cast<mlir::SchedTensorType>().set_alignment(alignment);
}

void SetParameters(Value v, value_purpose purpose,
                   value_sub_purpose sub_purpose, value_format format,
                   value_mem_area mem_area, value_mem_type mem_type,
                   int32_t alignment) {
  set_purpose(v, purpose);
  set_sub_purpose(v, sub_purpose);
  v.getType().dyn_cast<mlir::SchedTensorType>().set_mem_area(mem_area);
  v.getType().dyn_cast<mlir::SchedTensorType>().set_mem_type(mem_type);
  set_format(v, format);
  set_alignment(v, alignment);
}

SmallVector<int64_t, 1> storage_shape_for_sub_purpose(ArrayRef<int64_t> shape,
                                                      value_sub_purpose vsp,
                                                      int64_t param_a,
                                                      int64_t param_b) {
  SmallVector<int64_t, 1> shape_return;
  for (int i = 0; i < shape.size(); i++)
    shape_return.push_back(shape[i]);

  if (vsp == vsp_DoubleBuffer) {
    shape_return[shape.size() - 1] =
        min(shape_return[shape_return.size() - 1], param_a * 2);
  } else {
    if (vsp == vsp_RollingBufferX) {
      shape_return[0] = 1;
      shape_return[2] = min(shape_return[2], param_a);
    } else if (vsp == vsp_RollingBufferY) {
      shape_return[0] = 1;
      shape_return[1] = min(shape_return[1], param_a);
    } else if (vsp == vsp_RollingBufferXY) {
      shape_return[0] = 1;
      shape_return[2] = min(shape_return[2], param_a);
      shape_return[1] = min(shape_return[1], param_b);
    }
  }
  return shape_return;
}

int32_t max_area(llvm::ArrayRef<long int> t0, llvm::ArrayRef<long int> t1) {
  auto t0_area = (t0[0] * t0[1] * t0[2] * t0[3]);
  auto t1_area = (t1[0] * t1[1] * t1[2] * t1[3]);
  return (t0_area > t1_area) ? 0 : 1;
}

void pair_values(mlir::Value &t0, mlir::Value &t1, mlir::Value &t2,
                 int32_t index) {
  if (index == 0) {
    auto t0_sc = t0.getType().dyn_cast<mlir::SchedTensorType>();
    SchedTensorType common_tensor = SchedTensorType::get(
        t0_sc.getShape(), t0_sc.getElementType(), getUniqueId());
    common_tensor.setStorageShape(
        {t0_sc.getStorageShape()[0], t0_sc.getStorageShape()[1],
         t0_sc.getStorageShape()[2], t0_sc.getStorageShape()[3]});
    common_tensor.setBandwidthShape(
        {t0_sc.getBandwidthShape()[0], t0_sc.getBandwidthShape()[1],
         t0_sc.getBandwidthShape()[2], t0_sc.getBandwidthShape()[3]});
    common_tensor.set_mem_area(t0_sc.get_mem_area());
    common_tensor.set_mem_type(t0_sc.get_mem_type());
    common_tensor.set_format(t0_sc.get_format());
    common_tensor.set_purpose(t0_sc.get_purpose());
    common_tensor.set_sub_purpose(t0_sc.get_sub_purpose());
    common_tensor.set_compression_scale_for_worst_weight_stream(
        t0_sc.get_compression_scale_for_worst_weight_stream());
    common_tensor.set_weight_transpose_depthwise(
        t0_sc.get_weight_transpose_depthwise());
    common_tensor.set_storage_compression_scale(
        t0_sc.get_storage_compression_scale());
    common_tensor.set_bandwidth_compression_scale(
        t0_sc.get_bandwidth_compression_scale());
    common_tensor.set_weight_compression_scales(
        t0_sc.get_weight_compression_scales());
    common_tensor.set_weight_compression_config(
        t0_sc.get_weight_compression_config());
    common_tensor.set_weight_compressed_offsets(
        t0_sc.get_weight_compressed_offsets());
    common_tensor.set_compressed_values_substream_offsets(
        t0_sc.get_compressed_values_substream_offsets());
    common_tensor.set_address(t0_sc.get_address());
    common_tensor.set_start_time(t0_sc.get_start_time());
    common_tensor.set_end_time(t0_sc.get_end_time());
    common_tensor.set_alignment(t0_sc.get_alignment());
    common_tensor.set_storage_rounding_quantum(
        t0_sc.get_storage_rounding_quantum());
    common_tensor.set_brick_size(t0_sc.get_brick_size());
    common_tensor.set_NpuBlockTraversal(t0_sc.get_NpuBlockTraversal());
    common_tensor.set_resampling_mode(t0_sc.get_resampling_mode());
    common_tensor.set_avoid_NHCWB16(t0_sc.get_avoid_NHCWB16());
    common_tensor.set_pinned_size(t0_sc.get_pinned_size());
    common_tensor.set_pinned_mem_area(t0_sc.get_pinned_mem_area());
    t2.setType(common_tensor);
    t1.setType(common_tensor);
    t0.setType(common_tensor);
  } else {
    auto t1_sc = t1.getType().dyn_cast<mlir::SchedTensorType>();
    SchedTensorType common_tensor = SchedTensorType::get(
        t1_sc.getShape(), t1_sc.getElementType(), getUniqueId());
    common_tensor.setStorageShape(
        {t1_sc.getStorageShape()[0], t1_sc.getStorageShape()[1],
         t1_sc.getStorageShape()[2], t1_sc.getStorageShape()[3]});
    common_tensor.setBandwidthShape(
        {t1_sc.getBandwidthShape()[0], t1_sc.getBandwidthShape()[1],
         t1_sc.getBandwidthShape()[2], t1_sc.getBandwidthShape()[3]});
    common_tensor.set_mem_area(t1_sc.get_mem_area());
    common_tensor.set_mem_type(t1_sc.get_mem_type());
    common_tensor.set_format(t1_sc.get_format());
    common_tensor.set_purpose(t1_sc.get_purpose());
    common_tensor.set_sub_purpose(t1_sc.get_sub_purpose());
    common_tensor.set_compression_scale_for_worst_weight_stream(
        t1_sc.get_compression_scale_for_worst_weight_stream());
    common_tensor.set_weight_transpose_depthwise(
        t1_sc.get_weight_transpose_depthwise());
    common_tensor.set_storage_compression_scale(
        t1_sc.get_storage_compression_scale());
    common_tensor.set_bandwidth_compression_scale(
        t1_sc.get_bandwidth_compression_scale());
    common_tensor.set_weight_compression_scales(
        t1_sc.get_weight_compression_scales());
    common_tensor.set_weight_compression_config(
        t1_sc.get_weight_compression_config());
    common_tensor.set_weight_compressed_offsets(
        t1_sc.get_weight_compressed_offsets());
    common_tensor.set_compressed_values_substream_offsets(
        t1_sc.get_compressed_values_substream_offsets());
    common_tensor.set_address(t1_sc.get_address());
    common_tensor.set_start_time(t1_sc.get_start_time());
    common_tensor.set_end_time(t1_sc.get_end_time());
    common_tensor.set_alignment(t1_sc.get_alignment());
    common_tensor.set_storage_rounding_quantum(
        t1_sc.get_storage_rounding_quantum());
    common_tensor.set_brick_size(t1_sc.get_brick_size());
    common_tensor.set_NpuBlockTraversal(t1_sc.get_NpuBlockTraversal());
    common_tensor.set_resampling_mode(t1_sc.get_resampling_mode());
    common_tensor.set_avoid_NHCWB16(t1_sc.get_avoid_NHCWB16());
    common_tensor.set_pinned_size(t1_sc.get_pinned_size());
    common_tensor.set_pinned_mem_area(t1_sc.get_pinned_mem_area());
    t2.setType(common_tensor);
    t1.setType(common_tensor);
    t0.setType(common_tensor);
  }
}

} // namespace scheduleir

} // namespace mlir

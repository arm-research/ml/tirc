/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#ifndef SCHED_TENSOR_TYPE_H
#define SCHED_TENSOR_TYPE_H

#include "mlir/Dialect/Quant/QuantOps.h"          // TF:local_config_mlir
#include "mlir/Dialect/Traits.h"                  // TF:local_config_mlir
#include "mlir/IR/Attributes.h"                   // TF:local_config_mlir
#include "mlir/IR/Builders.h"                     // TF:local_config_mlir
#include "mlir/IR/Dialect.h"                      // TF:local_config_mlir
#include "mlir/IR/OpDefinition.h"                 // TF:local_config_mlir
#include "mlir/Interfaces/LoopLikeInterface.h"    // from @llvm-project
#include "mlir/Interfaces/SideEffectInterfaces.h" // from @llvm-project
#include "mlir/Support/LLVM.h"                    // TF:local_config_mlir

#include "src/ir/architecture_config/architecture_config.h"
#include "src/utils/numeric_utils.h"
#include "src/utils/unique_id.h"

#include <vector>
using namespace std;
using namespace mlir::utils;

using value_unique_id = uint64_t;

namespace mlir {

struct weightcompression_config {
  NpuBlockType npu_block_type;
  int32_t ofm_block_depth;
  int32_t ofm_depth_step;
  SmallVector<int32_t, 4> dilation;

  weightcompression_config(NpuBlockType npuBlockType, int32_t ofmBlockDepth,
                           int32_t ofmDepthStep,
                           SmallVector<int32_t, 4> Dilation) {
    npu_block_type = npuBlockType;
    ofm_block_depth = ofmBlockDepth;
    ofm_depth_step = ofmDepthStep;
    dilation = Dilation;
  }

  weightcompression_config() {}
};

using array_1D_fp64 = std::vector<double>;
using array_2D_fp64 = std::vector<std::vector<double>>;

using array_1D_i32 = std::vector<int32_t>;
using array_2D_i32 = std::vector<std::vector<int32_t>>;

//===----------------------------------------------------------------------===//
// SchedTensorType
//===----------------------------------------------------------------------===//
namespace detail {
struct SchedTensorTypeStorage;
} // namespace detail

class SchedTensorType
    : public ::mlir::Type::TypeBase<SchedTensorType, TensorType,
                                    detail::SchedTensorTypeStorage> {
public:
  /// Inherit some necessary constructors from 'TypeBase'.
  using Base::Base;

  /// Get or create a new SchedTensorType of the provided shape and element
  /// type. Assumes the arguments define a well-formed type.
  static SchedTensorType get(ArrayRef<int64_t> shape, Type elementType,
                             value_unique_id unique_id);

  /// Get or create a new SchedTensorType of the provided shape and element
  /// type declared at the given, potentially unknown, location.  If the
  /// SchedTensorType defined by the arguments would be ill-formed, emit
  /// errors
  /// and return a nullptr-wrapping type.
  static SchedTensorType
  getChecked(llvm::function_ref<mlir::InFlightDiagnostic()> emitError,
             ArrayRef<int64_t> shape, Type elementType,
             value_unique_id unique_id);

  /// Verify the construction of a schedule tensor type.
  using Base::getChecked;
  static ::mlir::LogicalResult
  verify(::llvm::function_ref<::mlir::InFlightDiagnostic()> emitError,
         ::llvm::ArrayRef<int64_t> shape, Type elementType,
         value_unique_id unique_id);

  ArrayRef<int64_t> getShape() const;
  value_unique_id getUniqueId() const;
  Type getElementType() const;

  ArrayRef<int64_t> getStorageShape() const;
  ArrayRef<int64_t> getBandwidthShape() const;

  void setStorageShape(SmallVector<int64_t, 4> shape);
  void setBandwidthShape(SmallVector<int64_t, 4> shape);

  value_mem_area get_mem_area() const;
  void set_mem_area(value_mem_area v);

  value_mem_type get_mem_type() const;
  void set_mem_type(value_mem_type v);

  value_format get_format() const;
  void set_format(value_format v);

  value_purpose get_purpose() const;
  void set_purpose(value_purpose v);

  value_sub_purpose get_sub_purpose() const;
  void set_sub_purpose(value_sub_purpose v);

  /*
     QuantinzationInfo stored in Quantization portion of the Type
  */

  float get_compression_scale_for_worst_weight_stream() const;
  void set_compression_scale_for_worst_weight_stream(float value);

  bool get_weight_transpose_depthwise() const;
  void set_weight_transpose_depthwise(bool value);

  float get_storage_compression_scale() const;
  void set_storage_compression_scale(float value);

  float get_bandwidth_compression_scale() const;
  void set_bandwidth_compression_scale(float value);

  array_1D_fp64 get_weight_compression_scales() const;
  void set_weight_compression_scales(array_1D_fp64 scales);

  weightcompression_config get_weight_compression_config() const;
  void set_weight_compression_config(weightcompression_config config);

  array_1D_i32 get_weight_compressed_offsets() const;
  void set_weight_compressed_offsets(array_1D_i32 offsets);

  array_2D_i32 get_compressed_values_substream_offsets() const;
  void set_compressed_values_substream_offsets(array_2D_i32 offsets);

  int64_t get_address() const;
  void set_address(int64_t a);

  int64_t get_start_time() const;
  void set_start_time(int64_t a);

  int64_t get_end_time() const;
  void set_end_time(int64_t a);

  int32_t get_alignment();
  void set_alignment(unsigned a);

  array_1D_i32 get_storage_rounding_quantum();
  void set_storage_rounding_quantum(array_1D_i32 value);

  array_1D_i32 get_brick_size();
  void set_brick_size(array_1D_i32 value);

  NpuBlockTraversal get_NpuBlockTraversal();
  void set_NpuBlockTraversal(NpuBlockTraversal value);

  mlir::utils::resampling_mode get_resampling_mode();
  void set_resampling_mode(mlir::utils::resampling_mode value);

  bool get_avoid_NHCWB16();
  void set_avoid_NHCWB16(bool value);

  int64_t get_pinned_size();
  void set_pinned_size(int64_t a);

  value_mem_area get_pinned_mem_area();
  void set_pinned_mem_area(value_mem_area v);

  bool get_io_memory_op();
  void set_io_memory_op(bool v);

  void setWeightCompressionCache_BlockType(int32_t block_type);
  void setWeightCompressionCache_OFMBlockDepth(uint32_t ofm_block_depth);
  void setWeightCompressionCache_OFMDepthStep(uint32_t ofm_depth_step);

  int32_t getWeightCompressionCache_BlockType();
  uint32_t getWeightCompressionCache_OFMBlockDepth();
  uint32_t getWeightCompressionCache_OFMDepthStep();
};

namespace scheduleir {

bool weights_fit_sram(mlir::SchedTensorType type,
                      mlir::ArchitectureConfig::ArchitectureConfigAttr &arch_c);
void set_format(Value v, value_format format);
void set_purpose(Value v, value_purpose purpose);
void set_sub_purpose(Value v, value_sub_purpose sub_purpose);
void set_alignment(Value v, int32_t alignment);
void SetParameters(Value v, value_purpose purpose,
                   value_sub_purpose sub_purpose, value_format format,
                   value_mem_area mem_area, value_mem_type mem_type,
                   int32_t alignment = 16);
SmallVector<int64_t, 1> storage_shape_for_sub_purpose(ArrayRef<int64_t> shape,
                                                      value_sub_purpose vsp,
                                                      int64_t param_a = -1,
                                                      int64_t param_b = -1);
int32_t max_area(llvm::ArrayRef<long int> t0, llvm::ArrayRef<long int> t1);
void pair_values(mlir::Value &t0, mlir::Value &t1, mlir::Value &t2,
                 int32_t index);

} // namespace scheduleir

} // namespace mlir

#endif // SCHED_TENSOR_TYPE_H

/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#include "src/ir/schedule_ir.h"

using namespace mlir;
using namespace mlir::scheduleir;

class DialectAsmPrinter;

#include "src/ir/raw_data_attribute/RawDataAttr.cc"
#include "src/ir/sched_tensor/SchedTensorType.cc"

ScheduleIR::ScheduleIR(MLIRContext *context)
    : Dialect(getDialectNamespace(), context, TypeID::get<ScheduleIR>()) {
  addOperations<
#define GET_OP_LIST
#include "src/ir/schedule_ir.cc.inc"
      >();

  addTypes<::mlir::SchedTensorType>();

  addAttributes<::mlir::RawDataAttr>();
}

mlir::Type ScheduleIR::parseType(mlir::DialectAsmParser &parser) const {
  FATAL_ERROR(
      "Parsing of ArchitectureConfig, CompilerConfig, KernelRegion, "
      "CascadeRegion, GeometricTransform, SchedTensorType not yet implemented");
}

void ScheduleIR::printType(Type type, DialectAsmPrinter &printer) const {

  auto schedType = type.dyn_cast<mlir::SchedTensorType>();

  printer << "SchedTensorType<";
  for (auto si : schedType.getShape())
    printer << si << "x";
  printer << schedType.getElementType();
  printer << ">";
}

void ScheduleIR::printAttribute(Attribute type,
                                DialectAsmPrinter &printer) const {
  auto rawAttr = type.dyn_cast<mlir::RawDataAttr>();
  printer << "RawDataAttr<>";
}

#define GET_OP_CLASSES
#include "src/ir/schedule_ir.cc.inc"

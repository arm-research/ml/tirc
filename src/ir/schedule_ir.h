/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#ifndef SCHEDULE_IR_H
#define SCHEDULE_IR_H

#include "mlir/Dialect/Quant/QuantOps.h"          // TF:local_config_mlir
#include "mlir/Dialect/Traits.h"                  // TF:local_config_mlir
#include "mlir/IR/Attributes.h"                   // TF:local_config_mlir
#include "mlir/IR/Builders.h"                     // TF:local_config_mlir
#include "mlir/IR/Diagnostics.h"                  // TF:local_config_mlir
#include "mlir/IR/Dialect.h"                      // TF:local_config_mlir
#include "mlir/IR/DialectImplementation.h"        // TF:local_config_mlir
#include "mlir/IR/MLIRContext.h"                  // TF:local_config_mlir
#include "mlir/IR/Matchers.h"                     // TF:local_config_mlir
#include "mlir/IR/OpDefinition.h"                 // TF:local_config_mlir
#include "mlir/IR/OpImplementation.h"             // TF:local_config_mlir
#include "mlir/IR/PatternMatch.h"                 // TF:local_config_mlir
#include "mlir/IR/TypeUtilities.h"                // TF:local_config_mlir
#include "mlir/IR/Types.h"                        // TF:local_config_mlir
#include "mlir/IR/Value.h"                        // TF:local_config_mlir
#include "mlir/Interfaces/LoopLikeInterface.h"    // from @llvm-project
#include "mlir/Interfaces/SideEffectInterfaces.h" // from @llvm-project
#include "mlir/Parser.h"                          // TF:local_config_mlir
#include "mlir/Support/LLVM.h"                    // TF:local_config_mlir
#include "mlir/Support/LogicalResult.h"           // TF:local_config_mlir
#include "mlir/Transforms/FoldUtils.h"            // TF:local_config_mlir
#include "mlir/Transforms/InliningUtils.h"        // TF:local_config_mlir
#include "mlir/Transforms/RegionUtils.h"          // TF:local_config_mlir
#include "llvm/ADT/Sequence.h"
#include "llvm/ADT/SmallVector.h"
#include "llvm/ADT/StringExtras.h"
#include "llvm/ADT/StringRef.h"
#include "llvm/ADT/StringSwitch.h"
#include "llvm/ADT/iterator_range.h"
#include "llvm/Support/FormatVariadic.h"

#include <algorithm>
#include <cstdint>
#include <functional>
#include <numeric>
#include <string>
#include <type_traits>

#include "src/utils/assert.h"

namespace mlir {
namespace scheduleir {

class ScheduleIR : public mlir::Dialect {
public:
  explicit ScheduleIR(mlir::MLIRContext *ctx);

  /// Provide a utility accessor to the dialect namespace. This is used by
  /// several utilities.
  static llvm::StringRef getDialectNamespace() { return "scheduleir"; }

  /// Parse an instance of a type registered to the toy dialect.
  mlir::Type parseType(mlir::DialectAsmParser &parser) const override;

  /// Print an instance of a type registered to the toy dialect.
  void printType(mlir::Type type,
                 mlir::DialectAsmPrinter &printer) const override;

  /// Print an instance of a type registered to the toy dialect.
  void printAttribute(mlir::Attribute type,
                      mlir::DialectAsmPrinter &printer) const override;
};

} // namespace scheduleir
} // namespace mlir

#define GET_OP_CLASSES
#include "src/ir/schedule_ir.h.inc"

#include "src/ir/sched_tensor/SchedTensorType.h"

#endif // SCHEDULE_IR_H

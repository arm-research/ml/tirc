/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#include "src/transforms/binary_writer/driver_actions.h"
#include <cmath>

namespace mlir {
namespace llcs {

void append(llvm::SmallVector<uint8_t, 8> &vec, uint32_t param) {
  uint8_t *p = reinterpret_cast<uint8_t *>(&param);
  vec.push_back(p[0]);
  vec.push_back(p[1]);
  vec.push_back(p[2]);
  vec.push_back(p[3]);
}

uint32_t make_da_tag(uint8_t id, uint8_t reserved, uint16_t param) {
  uint32_t tag = id;
  tag |= reserved << 8;
  tag |= param << 16;
  return tag;
}

void emit_fourcc(llvm::SmallVector<uint8_t, 8> &command_list_payload,
                 const std::string &fourcc) {
  assert(fourcc.length() == 4);
  command_list_payload.push_back(fourcc[0]);
  command_list_payload.push_back(fourcc[1]);
  command_list_payload.push_back(fourcc[2]);
  command_list_payload.push_back(fourcc[3]);
}

uint32_t build_id_word() {
  id_r id = 0;
  id.set_arch_major_rev(NNX_ARCH_VERSION_MAJOR);
  id.set_arch_minor_rev(NNX_ARCH_VERSION_MINOR);
  id.set_arch_patch_rev(6 /*NNX_ARCH_VERSION_PATCH*/);
  return id;
}

uint32_t build_config_word() {
  const uint32_t arch_config_macs = 0x100;
  const uint32_t arch_shram_size_bytes = 0xc000;
  const uint32_t arch_ncores = 0x1;
  uint32_t macs_cc = arch_ncores * arch_config_macs;
  uint32_t log2_macs_cc = static_cast<int>(log2(macs_cc) + 0.5);
  uint32_t shram_size = arch_shram_size_bytes / 1024;
  config_r cfg;
  cfg.set_shram_size((::shram_size)(shram_size));
  cfg.set_cmd_stream_version(0);
  cfg.set_macs_per_cc(::macs_per_cc(log2_macs_cc));
  return cfg;
}

void emit_config(llvm::SmallVector<uint8_t, 8> &data, uint32_t rel,
                 uint32_t patch, uint32_t arch) {
  uint32_t cc = 0;
  append(data, make_da_tag(DACommands::Config, 0,
                           (patch << DACommands::Config_PatchShift) | rel));
  append(data, build_config_word());
  append(data, build_id_word());
}

void emit_cmd_stream_header(llvm::SmallVector<uint8_t, 8> &data,
                            uint32_t length) {
  int size = data.size();
  assert((size & 0x3) == 0);
  // Insert NOPs to align start of command stream to 16 bytes
  uint32_t num_nops = 4 - ((size / 4 + 1) % 4);
  while (num_nops--) {
    append(data, make_da_tag(DACommands::NOP, 0, 0));
  }

  // Use the reserved 8 bit as the length high
  assert((length & 0x3) == 0);
  length /= 4;
  uint8_t length_high = (length & 0x00FF0000) >> 16;
  uint16_t length_low = length & 0x0000FFFF;
  append(data, make_da_tag(DACommands::CmdStream, length_high, length_low));
}

void emit_reg_read(llvm::SmallVector<uint8_t, 8> &data, uint32_t reg_index,
                   uint32_t reg_count) {
  assert(reg_count >= 1);
  uint32_t payload = (reg_index & DACommands::ReadAPB_IndexMask) |
                     ((reg_count << DACommands::ReadAPB_CountShift) - 1);
  append(data, make_da_tag(DACommands::ReadAPB, 0, payload));
}

void emit_dump_shram(llvm::SmallVector<uint8_t, 8> &data) {
  append(data, make_da_tag(DACommands::DumpSHRAM, 0, 0));
}

} // namespace llcs
} // namespace mlir

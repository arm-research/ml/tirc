/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#ifndef DRIVER_ACTIONS_H
#define DRIVER_ACTIONS_H

#include "src/arch/reg_interface.h"
#include "llvm/ADT/SmallVector.h"
#include <cmath>

namespace mlir {
namespace llcs {

enum DACommands {
  Reserved = 0x00,
  Config = 0x01,
  Config_PatchShift = 4,
  CmdStream = 0x02,
  ReadAPB = 0x03,
  ReadAPB_CountShift = 12,
  ReadAPB_IndexMask = (1 << ReadAPB_CountShift) - 1,
  DumpSHRAM = 0x04,
  NOP = 0x05,
};

void append(llvm::SmallVector<uint8_t, 8> &vec, uint32_t param);

uint32_t make_da_tag(uint8_t id, uint8_t reserved, uint16_t param);

void emit_fourcc(llvm::SmallVector<uint8_t, 8> &command_list_payload,
                 const std::string &fourcc);

uint32_t build_id_word();

uint32_t build_config_word();

void emit_config(llvm::SmallVector<uint8_t, 8> &data, uint32_t rel,
                 uint32_t patch, uint32_t arch);

void emit_cmd_stream_header(llvm::SmallVector<uint8_t, 8> &data,
                            uint32_t length);

void emit_reg_read(llvm::SmallVector<uint8_t, 8> &data, uint32_t reg_index,
                   uint32_t reg_count = 1);

void emit_dump_shram(llvm::SmallVector<uint8_t, 8> &data);

} // namespace llcs
} // namespace mlir
#endif // DRIVER_ACTIONS_H

/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#include "src/transforms/binary_writer/generate_model.h"

namespace mlir {
namespace binary_writer {

unsigned array_prod(ArrayRef<int64_t> s) {
  if (s.size() == 0)
    return 0;
  else if (s.size() == 1)
    return s[0];
  else if (s.size() == 2)
    return s[0] * s[1];
  else if (s.size() == 4)
    return s[0] * s[1] * s[2] * s[3];
  else {
    std::cout << "array_prod::Unknown size"
              << std::endl; // Chanhe to Faulty Assert
    exit(1);
  }
}

Operation *setup_tflite_custom_op(mlir::OpBuilder &bldr,
                                  mlir::Operation *llcs_call_op,
                                  Operation *flash_tensor,
                                  std::vector<Operation *> &scratch_tensors,
                                  Operation *command_stream) {
  auto loc = llcs_call_op->getLoc();
  SmallVector<mlir::Value> operands = {
      command_stream->getResult(0), flash_tensor->getResult(0),
      scratch_tensors[0]->getResult(0), scratch_tensors[1]->getResult(0),
      llcs_call_op->getOperand(1)}; // Command, Flash, Scratch, Scratch, Input
  auto output_types = llcs_call_op->getResultTypes();
  mlir::StringAttr custom_code = bldr.getStringAttr("ethos-u");

  ArrayRef<int64_t> i_shape = {0};
  Type i_type = bldr.getIntegerType(sizeof(uint8_t) * 8);
  ShapedType type = RankedTensorType::get(i_shape, i_type);
  StringRef bytes("");
  OpaqueElementsAttr custom_option =
      OpaqueElementsAttr::get(llcs_call_op->getDialect(), type, bytes);

  bldr.setInsertionPoint(llcs_call_op);
  auto custom_op = bldr.create<TFL::CustomOp>(loc, output_types, operands,
                                              custom_code, custom_option);

  llcs_call_op->getResult(0).replaceAllUsesWith(custom_op.getResult(0));

  auto input_address =
      llcs_call_op->getOperand(0).getDefiningOp()->getAttrOfType<IntegerAttr>(
          "Input_0_Address");

  Operation *custom_op_l = custom_op;
  if (input_address)
    custom_op_l->setAttr("Input_0_Address", input_address);
  auto output_address =
      llcs_call_op->getOperand(0).getDefiningOp()->getAttrOfType<IntegerAttr>(
          "Output_0_Address");
  if (output_address)
    custom_op_l->setAttr("Output_0_Address", output_address);

  return custom_op;
}

void generate_tflite_custom_op(mlir::MLIRContext &ctx, mlir::FuncOp &func) {
  OpBuilder bldr(&ctx);

  mlir::Region *region = func.getCallableRegion();
  assert(region->getBlocks().size() == 1);
  mlir::Block &block = region->getBlocks().front();

  std::vector<mlir::Operation *> llcs_call_ops = {};
  std::vector<mlir::Operation *> cpu_ops = {};

  for (mlir::Operation &op : block.getOperations()) {
    if (llvm::isa<llcs::CallNpuRegionCmd>(op)) {
      llcs_call_ops.push_back(&op);
    } else {
      if (llvm::isa<TFL::SoftmaxOp>(op) or llvm::isa<TFL::ReshapeOp>(op) or
          llvm::isa<TFL::SqueezeOp>(op) or
          llvm::isa<TFL::FullyConnectedOp>(op)) {
        cpu_ops.push_back(&op);
      }
    }
  }

  /*
    Each LLCS Custom Op
      -> Flash (Hoisted to the top of the block as std.constant)
      -> Scratch Tensor
      -> Scratch Fast Tensor
      -> Command Stream

    Each CPU Op
      -> Inputs
      -> Outputs
      -> Const
  */

  // Step0: Lower llcs call and region to tflite custom op
  for (auto llcs_call_op : llcs_call_ops) {
    Operation *flash_tensor = setup_flash_op(bldr, llcs_call_op);

    std::vector<Operation *> scratch_tensors =
        setup_scratch_tensors(bldr, llcs_call_op);
    Operation *command_stream = setup_command_tensor(bldr, llcs_call_op);

    Operation *custom_op = setup_tflite_custom_op(
        bldr, llcs_call_op, flash_tensor, scratch_tensors, command_stream);
  }
  // Delete Redundant llcs call & region ops
  for (auto llcs_call_op : llcs_call_ops) {
    llcs_call_op->getOperand(0).getDefiningOp()->erase();
    llcs_call_op->erase();
  }
}

} // namespace binary_writer
} // namespace mlir

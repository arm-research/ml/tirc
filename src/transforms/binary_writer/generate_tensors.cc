/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#include "src/transforms/binary_writer/generate_tensors.h"

namespace mlir {
namespace binary_writer {

mlir::Operation *setup_flash_op(mlir::OpBuilder &bldr,
                                mlir::Operation *llcs_call_op) {
  std::string prefix = get_str_from_attr(
      *(llcs_call_op->getOperand(0).getDefiningOp()), "label");
  mlir::Operation &cst =
      *(llcs_call_op->getOperand(2).getDefiningOp()); // flash_tensor
  // -1 indicates that the tensor will be allocated online by TFLite Micro
  set_i32_to_attr(bldr, cst, "address", -1);
  set_str_to_attr(bldr, cst, "label", prefix + "flash");
  set_i32_to_attr(bldr, cst, "mem_area",
                  static_cast<int32_t>(MemArea::OffChipFlash));
  set_i32_to_attr(bldr, cst, "mem_type",
                  static_cast<int32_t>(MemoryType::PERMANENT_CPU));
  return &cst;
}

std::vector<Operation *> setup_scratch_tensors(mlir::OpBuilder &bldr,
                                               mlir::Operation *llcs_call_op) {
  mlir::Operation &npu_region_op =
      *(llcs_call_op->getOperand(0).getDefiningOp());
  std::vector<Operation *> scratch_tensors;

  std::string prefix = get_str_from_attr(npu_region_op, "label");
  int64_t scratch_tensor_size =
      get_i64_from_attr(npu_region_op, "scratch_tensor_size");
  int64_t scratch_fast_tensor_size =
      get_i64_from_attr(npu_region_op, "scratch_fast_tensor_size");

#if ENABLE_LOGGING
  printf("Scratch Tensor: %lld Scratch Fast Tensor: %lld\n",
         scratch_tensor_size, scratch_fast_tensor_size);
#endif

  mlir::Location loc = npu_region_op.getLoc();
  llvm::SmallVector<uint8_t, 8> dummy;
  Type type = bldr.getIndexType();
  Type element_type = bldr.getIntegerType(sizeof(uint8_t) * 8, false);

  RankedTensorType scratch_tensor_type =
      RankedTensorType::get({scratch_tensor_size}, element_type);
  DenseElementsAttr scratch_tensor_data = DenseElementsAttr::get(
      scratch_tensor_type, llvm::makeArrayRef<uint8_t>(dummy));

  bldr.setInsertionPoint(llcs_call_op);
  auto scratch_const_op =
      bldr.create<TFL::ConstOp>(loc, scratch_tensor_type, scratch_tensor_data);

  set_i32_to_attr(bldr, *scratch_const_op, "address", 0);
  set_str_to_attr(bldr, *scratch_const_op, "label", prefix + "scratch");
  set_i32_to_attr(bldr, *scratch_const_op, "mem_area",
                  static_cast<int32_t>(MemArea::Sram));
  set_i32_to_attr(bldr, *scratch_const_op, "mem_type",
                  static_cast<int32_t>(MemoryType::SCRATCH));
  set_i32_to_attr(bldr, *scratch_const_op, "buf_area", scratch_buf_area);
  if (scratch_fast_tensor_size == 0) {
    // share Sram for both scratch and scratch_fast
    scratch_fast_buf_area = scratch_buf_area;
    scratch_fast_tensor_size = scratch_tensor_size;
  }
  RankedTensorType scratch_fast_tensor_type =
      RankedTensorType::get({scratch_fast_tensor_size}, element_type);
  DenseElementsAttr scratch_fast_tensor_data = DenseElementsAttr::get(
      scratch_fast_tensor_type, llvm::makeArrayRef<uint8_t>(dummy));

  bldr.setInsertionPoint(llcs_call_op);
  auto scratch_fast_const_op = bldr.create<TFL::ConstOp>(
      loc, scratch_fast_tensor_type, scratch_fast_tensor_data);

  /* Attributes */
  set_i32_to_attr(bldr, *scratch_fast_const_op, "address", 0);
  set_str_to_attr(bldr, *scratch_fast_const_op, "label",
                  prefix + "scratch_fast");
  set_i32_to_attr(bldr, *scratch_fast_const_op, "mem_area",
                  static_cast<int32_t>(MemArea::Sram));
  set_i32_to_attr(bldr, *scratch_fast_const_op, "mem_type",
                  static_cast<int32_t>(MemoryType::SCRATCH_FAST));
  set_i32_to_attr(bldr, *scratch_fast_const_op, "buf_area",
                  scratch_fast_buf_area);

  scratch_tensors.push_back(scratch_const_op);
  scratch_tensors.push_back(scratch_fast_const_op);

  return scratch_tensors;
}

Operation *setup_command_tensor(mlir::OpBuilder &bldr,
                                mlir::Operation *llcs_call_op) {
  mlir::Operation &npu_region_op =
      *(llcs_call_op->getOperand(0).getDefiningOp());

  std::string prefix = get_str_from_attr(npu_region_op, "label");
  mlir::Block &block = npu_region_op.getRegions().front().getBlocks().front();
  StringRef attr_c("code");
  StringRef attr_p0("param_0");
  StringRef attr_p1("param_1");
  llvm::SmallVector<uint8_t, 8> command_list_payload;

  for (auto &cmd : block) {
    if (llvm::isa<llcs::Cmd0>(cmd)) {
      int16_t code = cmd.getAttrOfType<IntegerAttr>(attr_c).getInt();
      int16_t param_0 = cmd.getAttrOfType<IntegerAttr>(attr_p0).getInt();
      command_list_payload.push_back(code & 0xFF);
      command_list_payload.push_back((code >> 8) & 0xFF);
      command_list_payload.push_back(param_0 & 0xFF);
      command_list_payload.push_back((param_0 >> 8) & 0xFF);
    } else if (llvm::isa<llcs::Cmd1>(cmd)) {
      int16_t code = cmd.getAttrOfType<IntegerAttr>(attr_c).getInt();
      int16_t param_0 = cmd.getAttrOfType<IntegerAttr>(attr_p0).getInt();
      int32_t param_1 = cmd.getAttrOfType<IntegerAttr>(attr_p1).getInt();
      command_list_payload.push_back(code & 0xFF);
      command_list_payload.push_back((code >> 8) & 0xFF);
      command_list_payload.push_back(param_0 & 0xFF);
      command_list_payload.push_back((param_0 >> 8) & 0xFF);
      command_list_payload.push_back(param_1 & 0xFF);
      command_list_payload.push_back((param_1 >> 8) & 0xFF);
      command_list_payload.push_back((param_1 >> 16) & 0xFF);
      command_list_payload.push_back((param_1 >> 24) & 0xFF);
    } else if (llvm::isa<llcs::YieldOp>(cmd)) {
      continue;
    } else {
      assert(!"unknown command in llcs npu_region_op");
    }
  }

  llvm::SmallVector<uint8_t, 8> command_list;
  emit_fourcc(command_list, "COP1");
  emit_config(command_list, 0, 1, 0);
  emit_cmd_stream_header(command_list, command_list_payload.size());

  command_list.insert(command_list.end(), command_list_payload.begin(),
                      command_list_payload.end());
  command_list_payload.clear();

  mlir::Location loc = npu_region_op.getLoc();
  Type type = bldr.getIndexType();
  Type element_type = bldr.getIntegerType(sizeof(uint8_t) * 8, false);
  RankedTensorType tensor_type = RankedTensorType::get(
      {static_cast<long int>(command_list.size())}, element_type);
  DenseElementsAttr data = DenseElementsAttr::get(
      tensor_type, llvm::makeArrayRef<uint8_t>(command_list));

  bldr.setInsertionPoint(llcs_call_op);
  auto cmd_const_op = bldr.create<TFL::ConstOp>(loc, tensor_type, data);

  /* Attributes */
  set_i32_to_attr(bldr, *cmd_const_op, "address", -1);
  set_str_to_attr(bldr, *cmd_const_op, "label", prefix + "command_stream");
  set_i32_to_attr(bldr, *cmd_const_op, "mem_area",
                  static_cast<int32_t>(MemArea::OffChipFlash));
  set_i32_to_attr(bldr, *cmd_const_op, "mem_type",
                  static_cast<int32_t>(MemoryType::PERMANENT_CPU));
  return cmd_const_op;
}

} // namespace binary_writer
} // namespace mlir

/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#include "src/transforms/binary_writer/serialize_model.h"

namespace mlir {
namespace binary_writer {

/* Each Tensor is associated with a buffer are */
int buf_index = 0;
int buf_area = 2; // We start from 2 as 0,1 are reserved for the scratch and
                  // scratch_fast indexes.
int npu_idx = 0;
int cpu_idx = 0;

std::vector<TensorTuple> tuples;
std::vector<op_info> s_operations;
std::vector<int32_t> subgraph_inputs;
std::vector<int32_t> subgraph_outputs;

void add_output_as_subgraph_output_if_needed(
    std::vector<int32_t> &subgraph_outputs, mlir::Value v, int32_t buf_index) {
  auto consumers = ExtractConsumers(v);
  for (auto &op_con : consumers) {
    if (op_con.external_operation->getName().getStringRef().str() ==
        "std.return") {
      subgraph_outputs.push_back(buf_index);
    }
  }
}

int32_t search_input(std::vector<TensorTuple> &tuples, mlir::Value input_v) {
  for (auto &tuple : tuples) {
    if (input_v == tuple.v) {
      return tuple.buf_index;
    }
  }
  FATAL_ERROR("serialize_model::Input Tensor tuple not found");
}

void cpu_add_operands(mlir::Operation &op, std::string op_label,
                      std::vector<int32_t> &inputs) {
  for (int i = 1; i < op.getNumOperands(); i++) {
    tuples.push_back(TensorTuple{
        op_label + std::string("_input_") + std::to_string(i), buf_area++,
        buf_index, 0xffffffff,
        op.getOperand(i).getType().dyn_cast<mlir::RankedTensorType>(),
        op.getOperand(i),
        op.getOperand(i).getDefiningOp()->getAttrOfType<DenseElementsAttr>(
            "value")});
    inputs.push_back(buf_index++);
  }
}

void cpu_add_input_output(mlir::Operation &op, std::string op_label,
                          std::vector<int32_t> &inputs,
                          std::vector<int32_t> &outputs) {
  // 1. Register Input
  if (op.getOperand(0).getDefiningOp() == nullptr) {
    // Input is from Basic Block so create it
    tuples.push_back(TensorTuple{
        op_label + std::string("_input_0"), buf_area++, buf_index, 0xffffffff,
        op.getOperand(0).getType().dyn_cast<mlir::RankedTensorType>(),
        op.getOperand(0)});
    subgraph_inputs.push_back(buf_index);
    inputs.push_back(buf_index++);
  } else {
    // Input is from Operation so we need to search for it:
    auto input_buf_index = search_input(tuples, op.getOperand(0));
    inputs.push_back(input_buf_index);
  }

  // 2. Add Operands
  cpu_add_operands(op, op_label, inputs);

  // 3. Register Output
  tuples.push_back(TensorTuple{
      op_label + std::string("_output_0"), buf_area++, buf_index, 0xffffffff,
      op.getResult(0).getType().dyn_cast<mlir::RankedTensorType>(),
      op.getResult(0)});
  add_output_as_subgraph_output_if_needed(subgraph_outputs, op.getResult(0),
                                          buf_index);
  outputs.push_back(buf_index++);
}

flatbuffers::Offset<tflite::Model> serialise_model(FlatBufferBuilder &builder,
                                                   mlir::MLIRContext &ctx,
                                                   mlir::FuncOp &func) {
  flatbuffers::Offset<Vector<Offset<tflite::OperatorCode>>>
      operator_codes_offset = 0;
  flatbuffers::Offset<Vector<Offset<tflite::SubGraph>>> subgraphs_offset = 0;
  flatbuffers::Offset<Vector<Offset<tflite::Buffer>>> buffers_offset = 0;
  flatbuffers::Offset<Vector<Offset<tflite::Metadata>>> metadata_offset = 0;

  std::vector<std::string> op_types;
  std::vector<std::string> op_codes;
  std::set<string> op_type_set;

  /* These get Serialized by geting be pulled in by their consuming Ops */
  std::set<string> ops_to_ignore = {"tfl.pseudo_const", "tfl.pseudo_qconst",
                                    "std.constant", "std.return",
                                    "arith.constant"};

  mlir::Region *region = func.getCallableRegion();
  assert(region->getBlocks().size() == 1);
  mlir::Block &block = region->getBlocks().front();

  // Collect the nodes and create a simpe DAG constructing Tensor Tuples
  for (mlir::Operation &op : block.getOperations()) {
    std::string op_name = op.getName().getStringRef().str();
    if (ops_to_ignore.count(op_name))
      continue;
    op_type_set.insert(op_name);
    op_types.push_back(op_name);

    string custom_code;
    auto custom_code_attr = op.getAttrOfType<StringAttr>("custom_code");
    if (custom_code_attr)
      custom_code = custom_code_attr.getValue().str();
    op_codes.push_back(custom_code);

    if (llvm::isa<TFL::CustomOp>(op)) {
      std::vector<int32_t> inputs;
      std::vector<int32_t> outputs;

      std::string op_label = "npu_custom_op_" + to_string(npu_idx++);

      // 0. Flash, Scratch, Scratch, Command Stream
      std::cout << std::endl;

      tuples.push_back(TensorTuple{
          op_label + std::string("_command_stream"), buf_area++, buf_index,
          0xffffffff,
          op.getOperand(0).getType().dyn_cast<mlir::RankedTensorType>(),
          op.getOperand(0),
          op.getOperand(0).getDefiningOp()->getAttrOfType<DenseElementsAttr>(
              "value")});
      inputs.push_back(buf_index++);

      tuples.push_back(TensorTuple{
          op_label + std::string("_flash"), buf_area++, buf_index, 0xffffffff,
          op.getOperand(1).getType().dyn_cast<mlir::RankedTensorType>(),
          op.getOperand(1),
          op.getOperand(1).getDefiningOp()->getAttrOfType<DenseElementsAttr>(
              "value")});
      inputs.push_back(buf_index++);

      tuples.push_back(TensorTuple{
          op_label + std::string("_scratch"), scratch_buf_area, buf_index, 0x0,
          op.getOperand(2).getType().dyn_cast<mlir::RankedTensorType>(),
          op.getOperand(2),
          op.getOperand(2).getDefiningOp()->getAttrOfType<DenseElementsAttr>(
              "value")});
      inputs.push_back(buf_index++);

      tuples.push_back(TensorTuple{
          op_label + std::string("_scratch_fast"), scratch_fast_buf_area,
          buf_index, 0x0,
          op.getOperand(3).getType().dyn_cast<mlir::RankedTensorType>(),
          op.getOperand(3),
          op.getOperand(3).getDefiningOp()->getAttrOfType<DenseElementsAttr>(
              "value")});
      inputs.push_back(buf_index++);

      /* Note these are not part of the dialect but it is easier to pass it this
       * way rather than create more indirection */
      auto input_address = op.getAttrOfType<IntegerAttr>("Input_0_Address");
      if (not(input_address))
        FATAL_ERROR("serialize_model::did not find the input_address");
      auto output_address = op.getAttrOfType<IntegerAttr>("Output_0_Address");
      if (not(output_address))
        FATAL_ERROR("serialize_model::did not find the output_address");

      // 1. Register Input
      if (op.getOperand(4).getDefiningOp() == nullptr) {
        // Input is from Basic Block so create it
        tuples.push_back(TensorTuple{
            op_label + std::string("_input_0"), scratch_buf_area, buf_index,
            input_address.getInt(),
            op.getOperand(4).getType().dyn_cast<mlir::RankedTensorType>(),
            op.getOperand(4)});
        subgraph_inputs.push_back(buf_index);
        inputs.push_back(buf_index++);
      } else {
        // Input is from Operation so we need to search for it:
        // 1. get it buffer index and add it to inputs
        auto input_buf_index = search_input(tuples, op.getOperand(4));
        inputs.push_back(input_buf_index);
        // 2. Update its input_address (We can only do this here as we are
        // passing it as an attribute in the custom tflite op)
        for (auto &tuple : tuples) {
          if (tuple.buf_index == input_buf_index)
            tuple.offset = input_address.getInt();
        }
      }

      // 2. Register Output
      tuples.push_back(TensorTuple{
          op_label + std::string("_output_0"), scratch_buf_area, buf_index,
          output_address.getInt(),
          op.getResult(0).getType().dyn_cast<mlir::RankedTensorType>(),
          op.getResult(0)});
      add_output_as_subgraph_output_if_needed(subgraph_outputs, op.getResult(0),
                                              buf_index);
      outputs.push_back(buf_index++);

      s_operations.push_back(std::make_tuple(&op, inputs, outputs));
    } else if (llvm::isa<TFL::SoftmaxOp>(op)) {
      std::string op_label = "cpu_softmax_op_" + to_string(cpu_idx++);
      std::vector<int32_t> inputs;
      std::vector<int32_t> outputs;
      cpu_add_input_output(op, op_label, inputs, outputs);
      s_operations.push_back(std::make_tuple(&op, inputs, outputs));
    } else if (llvm::isa<TFL::FullyConnectedOp>(op)) {
      std::string op_label = "cpu_fully_connected_op_" + to_string(cpu_idx++);
      std::vector<int32_t> inputs;
      std::vector<int32_t> outputs;
      cpu_add_input_output(op, op_label, inputs, outputs);
      s_operations.push_back(std::make_tuple(&op, inputs, outputs));
    } else if (llvm::isa<TFL::ReshapeOp>(op)) {
      std::string op_label = "cpu_reshape_op_" + to_string(cpu_idx++);
      std::vector<int32_t> inputs;
      std::vector<int32_t> outputs;
      cpu_add_input_output(op, op_label, inputs, outputs);
      s_operations.push_back(std::make_tuple(&op, inputs, outputs));
    } else if (llvm::isa<TFL::SqueezeOp>(op)) {
      std::string op_label = "cpu_squeeze_op_" + to_string(cpu_idx++);
      std::vector<int32_t> inputs;
      std::vector<int32_t> outputs;
      cpu_add_input_output(op, op_label, inputs, outputs);
      s_operations.push_back(std::make_tuple(&op, inputs, outputs));
    } else {
      std::cout << op_name << std::endl;
      FATAL_ERROR("llcs_generate_runtime_tflite::Unknown Node (1)");
    }
  }

  // Print Tuples
  std::cout << std::endl;
  std::cout << "Tensor tuples ->" << std::endl;
  for (auto t : tuples) {
    std::string value_present = t.data ? "Data Present" : "Data Not Present";
    printf("## %d %-50s %d %08x %s\n", t.buf_index, t.name.c_str(), t.buf_area,
           t.offset, value_present.c_str());
  }
  std::cout << std::endl;

  // 0. operator_codes_offset
  std::vector<flatbuffers::Offset<tflite::OperatorCode>> operator_codes;
  for (int i = 0; i < op_codes.size(); i++) {
    auto offset = serialise_operator_code(builder, i, op_types[i], op_codes[i]);
    operator_codes.push_back(offset);
  }
  operator_codes_offset = builder.CreateVector(operator_codes);

  // 1. description_offset
  auto description_offset = builder.CreateString("TIRC Compiled Network");

  // 2. subgraphs_offset
  std::vector<flatbuffers::Offset<tflite::SubGraph>> subgraphs;
  subgraphs.push_back(serialise_subgraph(builder, s_operations, tuples,
                                         subgraph_inputs, subgraph_outputs));
  subgraphs_offset = builder.CreateVector(subgraphs);

  std::vector<MetadataTuple> metadata;

  // 3. prepare metadata buffer
  char meta_version[16] = "1.5.0";
  metadata.push_back(
      MetadataTuple{"min_runtime_version",
                    std::make_pair((uint8_t *)(meta_version),
                                   (uint32_t)(strlen(meta_version))),
                    -1});

  // 4. create addresses
  std::vector<int32_t> meta_off_mem_alloc;
  meta_off_mem_alloc.push_back(0);             // version
  meta_off_mem_alloc.push_back(1);             // subgraph count
  meta_off_mem_alloc.push_back(tuples.size()); // tensor count
  for (auto &tuple : tuples) {
    meta_off_mem_alloc.push_back(tuple.offset);
  }
  metadata.push_back(MetadataTuple{
      "OfflineMemoryAllocation",
      std::make_pair((uint8_t *)(meta_off_mem_alloc.data()),
                     (uint32_t)(meta_off_mem_alloc.size() * sizeof(int32_t))),
      -1});

  // 5. prepare buffers_to_write
  int buffer_count = 0;
  for (auto &tuple : tuples) {
    buffer_count = max(buffer_count, tuple.buf_area + 1);
  }
  std::vector<buffer_t> buffers_to_write(buffer_count);

  // 6. Go through Buffer Tuples
  for (auto &tuple : tuples) {
    if (not(tuple.data))
      continue;

    uint8_t *data = nullptr;
    int32_t data_size = 0;
    auto vtype = tuple.type;
    auto shape = vtype.getShape();
    uint32_t elem_count = 1;
    uint32_t elem_size = 0;
    for (auto &s : shape)
      elem_count *= s;
    auto etype = vtype.getElementType();
    if (etype.isa<mlir::quant::UniformQuantizedType>()) {
      auto uqtype = etype.dyn_cast<mlir::quant::UniformQuantizedType>();
      etype = uqtype.getStorageType();
    }
    if (etype.isInteger(8)) {
      elem_size = 1;
    } else if (etype.isInteger(16)) {
      elem_size = 2;
    } else if (etype.isInteger(32)) {
      elem_size = 4;
    } else {
      etype.dump();
      assert(0);
    }
    uint32_t type_size = elem_size * elem_count;

    auto raw = tuple.data.getRawData();
    data_size = raw.size();

    data = (uint8_t *)(raw.data());
    if (data_size != type_size) {
      if (data_size > 0) {
        assert(elem_size == data_size);
        uint8_t *raw_data = data;
        data = new uint8_t[type_size];
        for (uint32_t b = 0; b < type_size; b += elem_size) {
          memcpy(data + b, raw_data, elem_size);
        }
      }
      data_size = type_size;
    }

    bool is_origin_scartch = false;
    size_t len = tuple.name.size();
    if (len > 12) {
      is_origin_scartch = (tuple.name.substr(len - 7) == "scratch") ||
                          (tuple.name.substr(len - 12) == "scratch_fast");
    }

    if (is_origin_scartch) {
      assert(data == nullptr);
      auto vtype = tuple.type;
      auto shape = vtype.getShape();
      assert(shape.size() == 1);
      data_size = static_cast<int32_t>(shape[0]);
    }
    if (tuple.buf_area > 1 || is_origin_scartch) {
      buffers_to_write[tuple.buf_area] = std::make_pair(data, data_size);
    }
  }

  // 7. append metadata buffers that we prepared earlier but are placed after
  // the command stream, flash etc ...
  for (MetadataTuple &meta : metadata) {
    meta.buf_area = buffers_to_write.size();
    buffers_to_write.push_back(meta.buffer);
  }

  // 8. buffer_offsets
  std::vector<flatbuffers::Offset<tflite::Buffer>> buffers;
  for (auto &bf : buffers_to_write) {
    auto offset = serialise_buffer(builder, bf.first, bf.second);
    buffers.push_back(offset);
  }
  buffers_offset = builder.CreateVector(buffers);

  // 9. metadata_offsets
  std::vector<flatbuffers::Offset<tflite::Metadata>> metadatas;
  for (MetadataTuple &meta : metadata) {
    auto offset = serialise_metadata(builder, meta.name, meta.buf_area);
    metadatas.push_back(offset);
  }
  metadata_offset = builder.CreateVector(metadatas);

  // 10. Build the actual model
  tflite::ModelBuilder model(builder);
  const uint32_t tflite_version = 3;
  model.add_version(tflite_version);
  model.add_operator_codes(operator_codes_offset);
  model.add_subgraphs(subgraphs_offset);
  model.add_description(description_offset);
  model.add_buffers(buffers_offset);
  model.add_metadata(metadata_offset);
  auto model_offset = model.Finish();
  return model_offset;
}

} // namespace binary_writer
} // namespace mlir

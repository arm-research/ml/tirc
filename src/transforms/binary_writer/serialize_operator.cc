/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#include "src/transforms/binary_writer/serialize_operator.h"

namespace mlir {
namespace binary_writer {

flatbuffers::Offset<tflite::OperatorCode>
serialise_operator_code(flatbuffers::FlatBufferBuilder &builder, uint32_t idx,
                        std::string &op_type, std::string &custom_code) {
  auto tf_code = tflite::BuiltinOperator_CUSTOM;
  if ((op_type == "tfl.custom") && (custom_code != "ethos-u")) {
    assert(false);
  } else {
    tf_code = name_to_builtin_oper[op_type];
  }
  flatbuffers::Offset<String> custom_code_offset = 0;
  if ((tf_code == tflite::BuiltinOperator_CUSTOM) && !custom_code.empty()) {
    custom_code_offset = builder.CreateString(custom_code);
  }

  tflite::OperatorCodeBuilder ocb(builder);
  ocb.add_deprecated_builtin_code(tf_code);
  if (!custom_code_offset.IsNull())
    ocb.add_custom_code(custom_code_offset);
  return ocb.Finish();
}

/* s_operations is created in the same way then opcodes list is created so we
 * just need to walk this liearly */
int32_t global_index = 0;

flatbuffers::Offset<tflite::Operator>
serialise_cpu_operator(FlatBufferBuilder &builder, mlir::Operation *cpu_op,
                       std::vector<int32_t> inputs_o,
                       std::vector<int32_t> outputs_o) {
  std::string op_name = cpu_op->getName().getStringRef().str();
  if (op_name == "tfl.softmax") {
    std::vector<int32_t> inputs = inputs_o;
    std::vector<int32_t> outputs = outputs_o;
    auto inputs_offset = builder.CreateVector<int32_t>(inputs);
    auto outputs_offset = builder.CreateVector<int32_t>(outputs);

    float beta = cpu_op->getAttrOfType<FloatAttr>("beta").getValueAsDouble();
    tflite::SoftmaxOptionsBuilder sob(builder);
    sob.add_beta(beta);
    flatbuffers::Offset<tflite::SoftmaxOptions> builtin_option_offset =
        sob.Finish();

    // Common
    uint8_t dummy[2] = {0};
    auto mutating_variable_inputs_offset =
        builder.CreateVector<uint8_t>(dummy, 0);
    tflite::OperatorBuilder ob(builder);
    ob.add_opcode_index(global_index++);
    ob.add_inputs(inputs_offset);
    ob.add_outputs(outputs_offset);
    ob.add_builtin_options_type(name_to_builtin_option[op_name]);
    ob.add_builtin_options(builtin_option_offset.Union());
    // non custom_opt
    ob.add_mutating_variable_inputs(mutating_variable_inputs_offset);

    return ob.Finish();
  } else if (op_name == "tfl.squeeze") {
    std::vector<int32_t> inputs = inputs_o;
    std::vector<int32_t> outputs = outputs_o;
    auto inputs_offset = builder.CreateVector<int32_t>(inputs);
    auto outputs_offset = builder.CreateVector<int32_t>(outputs);

    auto axis_attr = mlir::utils::ArrayAttr_to_Vector(
        cpu_op->getAttrOfType<ArrayAttr>("squeeze_dims"));
    flatbuffers::Offset<flatbuffers::Vector<int32_t>> axis =
        builder.CreateVector<int32_t>(axis_attr);

    tflite::SqueezeOptionsBuilder sob(builder);
    sob.add_squeeze_dims(axis);
    flatbuffers::Offset<tflite::SqueezeOptions> builtin_option_offset =
        sob.Finish();

    // Common
    uint8_t dummy[2] = {0};
    auto mutating_variable_inputs_offset =
        builder.CreateVector<uint8_t>(dummy, 0);
    tflite::OperatorBuilder ob(builder);
    ob.add_opcode_index(global_index++);
    ob.add_inputs(inputs_offset);
    ob.add_outputs(outputs_offset);
    ob.add_builtin_options_type(name_to_builtin_option[op_name]);
    ob.add_builtin_options(builtin_option_offset.Union());
    // non custom_opt
    ob.add_mutating_variable_inputs(mutating_variable_inputs_offset);

    return ob.Finish();
  } else if (op_name == "tfl.fully_connected") {
    std::vector<int32_t> inputs = inputs_o;
    std::vector<int32_t> outputs = outputs_o;
    auto inputs_offset = builder.CreateVector<int32_t>(inputs);
    auto outputs_offset = builder.CreateVector<int32_t>(outputs);

    tflite::FullyConnectedOptionsBuilder sob(builder);
    sob.add_fused_activation_function(
        tflite::ActivationFunctionType::ActivationFunctionType_NONE);
    sob.add_weights_format(tflite::FullyConnectedOptionsWeightsFormat::
                               FullyConnectedOptionsWeightsFormat_DEFAULT);
    sob.add_keep_num_dims(false);
    // sob.add_asymmetric_quantize_inputs(true);
    flatbuffers::Offset<tflite::FullyConnectedOptions> builtin_option_offset =
        sob.Finish();

    // Common
    uint8_t dummy[2] = {0};
    auto mutating_variable_inputs_offset =
        builder.CreateVector<uint8_t>(dummy, 0);

    tflite::OperatorBuilder ob(builder);
    ob.add_opcode_index(global_index++);
    ob.add_inputs(inputs_offset);
    ob.add_outputs(outputs_offset);
    ob.add_builtin_options_type(name_to_builtin_option[op_name]);
    ob.add_builtin_options(builtin_option_offset.Union());
    // non custom_opt
    ob.add_mutating_variable_inputs(mutating_variable_inputs_offset);

    return ob.Finish();
  } else if (op_name == "tfl.reshape") {
    std::vector<int32_t> inputs = inputs_o;
    std::vector<int32_t> outputs = outputs_o;
    auto inputs_offset = builder.CreateVector<int32_t>(inputs);
    auto outputs_offset = builder.CreateVector<int32_t>(outputs);

    flatbuffers::Offset<flatbuffers::Vector<int32_t>> new_shape;

    tflite::ReshapeOptionsBuilder sob(builder);
    sob.add_new_shape(new_shape);
    flatbuffers::Offset<tflite::ReshapeOptions> builtin_option_offset =
        sob.Finish();

    // Common
    uint8_t dummy[2] = {0};
    auto mutating_variable_inputs_offset =
        builder.CreateVector<uint8_t>(dummy, 0);

    tflite::OperatorBuilder ob(builder);
    ob.add_opcode_index(global_index++);
    ob.add_inputs(inputs_offset);
    ob.add_outputs(outputs_offset);
    ob.add_builtin_options_type(name_to_builtin_option[op_name]);
    ob.add_builtin_options(builtin_option_offset.Union());
    // non custom_opt
    ob.add_mutating_variable_inputs(mutating_variable_inputs_offset);

    return ob.Finish();
  }
}

flatbuffers::Offset<tflite::Operator>
serialise_npu_operator(FlatBufferBuilder &builder, mlir::Operation *npu_op,
                       std::vector<int32_t> inputs_o,
                       std::vector<int32_t> outputs_o) {
  auto inputs_offset = builder.CreateVector<int32_t>(inputs_o);
  auto outputs_offset = builder.CreateVector<int32_t>(outputs_o);

  // NpuOp=1, FlexbufferFormat.UINT8=4, byte length=1
  static const uint8_t custom_option_bytes[3] = {0x01, 0x04, 0x01};
  auto custom_option_bytes_offset =
      builder.CreateVector<uint8_t>(custom_option_bytes, 3);
  auto mutating_variable_inputs_offset =
      builder.CreateVector<uint8_t>(custom_option_bytes, 0);
  static const tflite::CustomOptionsFormat custom_opt_format =
      tflite::CustomOptionsFormat::CustomOptionsFormat_FLEXBUFFERS;

  tflite::OperatorBuilder ob(builder);
  ob.add_opcode_index(global_index++);
  ob.add_inputs(inputs_offset);
  ob.add_outputs(outputs_offset);
  // non builtin_opt
  ob.add_custom_options(custom_option_bytes_offset);
  ob.add_custom_options_format(custom_opt_format);
  ob.add_mutating_variable_inputs(mutating_variable_inputs_offset);
  return ob.Finish();
}

} // namespace binary_writer
} // namespace mlir

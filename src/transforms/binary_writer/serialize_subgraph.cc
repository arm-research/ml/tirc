/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#include "src/transforms/binary_writer/serialize_subgraph.h"

#include "src/transforms/binary_writer/serialize_operator.h"
#include "src/transforms/binary_writer/serialize_tensor.h"
#include "src/transforms/binary_writer/serialize_utils.h"

namespace mlir {
namespace binary_writer {

flatbuffers::Offset<tflite::SubGraph> serialise_subgraph(
    FlatBufferBuilder &builder, std::vector<op_info> s_operations,
    std::vector<TensorTuple> &tuples, std::vector<int32_t> &subgraph_inputs,
    std::vector<int32_t> &subgraph_outputs) {
  flatbuffers::Offset<Vector<Offset<tflite::Tensor>>> tensors_offset = 0;
  flatbuffers::Offset<Vector<int32_t>> inputs_offset = 0;
  flatbuffers::Offset<Vector<int32_t>> outputs_offset = 0;
  flatbuffers::Offset<Vector<Offset<tflite::Operator>>> operators_offset = 0;

  // 0. Serialize tensors
  std::vector<flatbuffers::Offset<tflite::Tensor>> tensors;
  for (auto &tuple : tuples) {
    tensors.push_back(serialise_tensor(builder, tuple));
  }
  tensors_offset = builder.CreateVector(tensors);

  // 1. Add the graph input tensors
  inputs_offset = builder.CreateVector<int32_t>(subgraph_inputs);
  std::cout << "TFLite: Global Subgraph Inputs: ";
  for (auto m : subgraph_inputs)
    std::cout << m << ",";
  std::cout << std::endl;

  // 2. Add the graph output tensors
  outputs_offset = builder.CreateVector<int32_t>(subgraph_outputs);
  std::cout << "TFLite: Global Subgraph Outputs: ";
  for (auto m : subgraph_outputs)
    std::cout << m << ",";
  std::cout << std::endl;

  // 3. Add Operators
  std::cout << "TFLite: SubGraph Ops Inputs and Outputs " << std::endl;
  std::vector<flatbuffers::Offset<tflite::Operator>> operators;
  for (auto &opers : s_operations) {
    auto *op = std::get<0>(opers);
    auto inputs = std::get<1>(opers);
    auto outputs = std::get<2>(opers);

    std::cout << op->getName().getStringRef().str() << std::endl;
    std::cout << "  Inputs: ";
    for (auto m : inputs)
      std::cout << m << ",";
    std::cout << std::endl;
    std::cout << "  Outputs: ";
    for (auto m : outputs)
      std::cout << m << ",";
    std::cout << std::endl;

    if (llvm::isa<TFL::CustomOp>(op)) {
      auto oper_offset = serialise_npu_operator(builder, op, inputs, outputs);
      operators.push_back(oper_offset);
    } else {
      auto cpu_oper_offset =
          serialise_cpu_operator(builder, op, inputs, outputs);
      operators.push_back(cpu_oper_offset);
    }
  }
  operators_offset = builder.CreateVector(operators);
  std::cout << std::endl;

  tflite::SubGraphBuilder sgb(builder);
  sgb.add_tensors(tensors_offset);
  sgb.add_inputs(inputs_offset);
  sgb.add_outputs(outputs_offset);
  sgb.add_operators(operators_offset);

  return sgb.Finish();
}

} // namespace binary_writer
} // namespace mlir

/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#include "src/transforms/binary_writer/serialize_tensor.h"

namespace mlir {
namespace binary_writer {

flatbuffers::Offset<tflite::QuantizationParameters>
serialise_quantization_parameters(FlatBufferBuilder &builder,
                                  mlir::RankedTensorType vtype) {
  auto etype = vtype.getElementType();

  flatbuffers::Offset<flatbuffers::Vector<float>> min_offset = 0;
  flatbuffers::Offset<flatbuffers::Vector<float>> max_offset = 0;
  flatbuffers::Offset<flatbuffers::Vector<float>> scale_offset = 0;
  flatbuffers::Offset<flatbuffers::Vector<int64_t>> zero_pt_offset = 0;

  if (etype.isa<mlir::quant::UniformQuantizedType>()) {
    auto uqtype = etype.dyn_cast<mlir::quant::UniformQuantizedType>();
    int64_t qmin = uqtype.getStorageTypeMin();
    int64_t qmax = uqtype.getStorageTypeMax();
    float scale = uqtype.getScale();
    int64_t zero_pt = uqtype.getZeroPoint();
    float fmin = (qmin - zero_pt) * scale;
    float fmax = (qmax - zero_pt) * scale;
    min_offset = builder.CreateVector<float>({fmin});
    max_offset = builder.CreateVector<float>({fmax});
    scale_offset = builder.CreateVector<float>({scale});
    zero_pt_offset = builder.CreateVector<int64_t>({zero_pt});
  } else if (etype.isa<mlir::quant::UniformQuantizedPerAxisType>()) {
    // TODO: support per axis quantization.
    assert(!"unsupport per axis quantization!");
  } else if (etype.isInteger(8)) {
    // do nothing here
  } else {
    // do nothng here
    // etype.dump();
    // assert(!"unknown qtype!");
  }

  tflite::QuantizationParametersBuilder qpb(builder);
  if (!min_offset.IsNull())
    qpb.add_min(min_offset);
  if (!max_offset.IsNull())
    qpb.add_max(max_offset);
  if (!scale_offset.IsNull())
    qpb.add_scale(scale_offset);
  if (!zero_pt_offset.IsNull())
    qpb.add_zero_point(zero_pt_offset);
  return qpb.Finish();
}

flatbuffers::Offset<tflite::Tensor> serialise_tensor(FlatBufferBuilder &builder,
                                                     TensorTuple &tensor) {
  flatbuffers::Offset<Vector<int32_t>> shape_offset = 0;
  flatbuffers::Offset<String> name_offset = 0;
  flatbuffers::Offset<tflite::QuantizationParameters> quant_offset = 0;

  tflite::TensorType tfl_type = tflite::TensorType::TensorType_UINT8;
  auto vtype = tensor.type;
  auto etype = vtype.getElementType();
  if (etype.isa<mlir::quant::UniformQuantizedType>() ||
      etype.isa<mlir::quant::UniformQuantizedPerAxisType>()) {
    etype = etype.dyn_cast<mlir::quant::QuantizedType>().getStorageType();
  }

  if (etype.isSignlessInteger(8)) {
    // std::cout << "Signed 8 bits" << std::endl;
    tfl_type = tflite::TensorType::TensorType_INT8;
  } else if (etype.isUnsignedInteger(8)) {
    // std::cout << "UnSigned 8 bits" << std::endl;
    tfl_type = tflite::TensorType::TensorType_UINT8;
  } else if (etype.isSignlessInteger(32)) {
    // std::cout << "Signed 32 bits" << std::endl;
    tfl_type = tflite::TensorType::TensorType_INT32;
  } else {
    assert(!"unknown tensor type.");
  }

  auto tensor_shape = conv_to_shape_vector(vtype.getShape());

  shape_offset =
      builder.CreateVector<int32_t>(conv_to_shape_vector(vtype.getShape()));
  name_offset = builder.CreateString(tensor.name);
  quant_offset = serialise_quantization_parameters(builder, tensor.type);

  tflite::TensorBuilder tb(builder);
  tb.add_shape(shape_offset);
  tb.add_type(tfl_type);
  // All tensors must have a valid backing buffer, even if it is empty.
  // Empty buffers should be kept unique for TensorFlow Lite Micro
  tb.add_buffer(tensor.buf_area);
  tb.add_name(name_offset);
  tb.add_quantization(quant_offset);
  return tb.Finish();
}

} // namespace binary_writer
} // namespace mlir

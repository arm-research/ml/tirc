/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#ifndef SERIALIZE_TENSORS_H
#define SERIALIZE_TENSORS_H

#include "absl/memory/memory.h"
#include "mlir/Dialect/Quant/QuantOps.h"
#include "mlir/Dialect/Quant/QuantTypes.h"
#include "mlir/Dialect/StandardOps/IR/Ops.h"      // from @llvm-project
#include "mlir/IR/AffineMap.h"                    // from @llvm-project
#include "mlir/IR/Attributes.h"                   // from @llvm-project
#include "mlir/IR/BlockAndValueMapping.h"         // from @llvm-project
#include "mlir/IR/Builders.h"                     // from @llvm-project
#include "mlir/IR/Location.h"                     // from @llvm-project
#include "mlir/IR/MLIRContext.h"                  // from @llvm-project
#include "mlir/IR/Operation.h"                    // from @llvm-project
#include "mlir/IR/PatternMatch.h"                 // from @llvm-project
#include "mlir/Interfaces/InferTypeOpInterface.h" // from @llvm-project
#include "mlir/Pass/Pass.h"                       // from @llvm-project
#include "mlir/Transforms/DialectConversion.h"    // from @llvm-project

#include "src/ir/hlcs_ir.h"
#include "src/ir/llcs_ir.h"

#include "src/transforms/binary_writer/driver_actions.h"
#include "src/transforms/llcs_generator/hlcs_mlir_utils.h"
#include "src/transforms/llcs_generator/range_set.h"
#include "src/transforms/passes.h"

#include "src/utils/assert.h"
#include "src/utils/data_structure_utils.h"
#include "src/utils/ir_utils.h"
#include "src/utils/printIR.h"
#include "src/utils/string_utils.h"

#include "src/compiler/compiler_cl.h"

#include <set>
#include <unordered_map>
#include <vector>

#include "tensorflow/compiler/mlir/lite/ir/tfl_ops.h"

#include "tensorflow/lite/schema/schema_generated.h"

#include "flatbuffers/flatbuffers.h"

#include "src/transforms/binary_writer/utils.h"

using namespace std;
using namespace flatbuffers;
using namespace mlir;
using namespace std;

namespace mlir {
namespace binary_writer {

flatbuffers::Offset<tflite::QuantizationParameters>
serialise_quantization_parameters(FlatBufferBuilder &builder,
                                  mlir::RankedTensorType vtype);

flatbuffers::Offset<tflite::Tensor> serialise_tensor(FlatBufferBuilder &builder,
                                                     TensorTuple &tensor);

} // namespace binary_writer
} // namespace mlir

#endif // SERIALIZE_TENSORS_H

/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#include "src/transforms/binary_writer/serialize_utils.h"

namespace mlir {
namespace binary_writer {

flatbuffers::Offset<tflite::Buffer>
serialise_buffer(FlatBufferBuilder &builder, uint8_t *data, int32_t length) {
  flatbuffers::Offset<flatbuffers::Vector<uint8_t>> data_offset = 0;
  if (data != nullptr || length != 0) {
    data_offset = write_aligned_bytes(builder, data, length);
  }

  tflite::BufferBuilder bb(builder);
  if (!data_offset.IsNull()) {
    bb.add_data(data_offset);
  }
  auto ret = bb.Finish();
  return ret;
}

flatbuffers::Offset<tflite::Metadata>
serialise_metadata(FlatBufferBuilder &builder, string &name, int32_t buf_idx) {
  auto name_offset = builder.CreateString(name);
  tflite::MetadataBuilder mb(builder);
  mb.add_name(name_offset);
  mb.add_buffer(buf_idx);
  return mb.Finish();
}

flatbuffers::Offset<flatbuffers::Vector<uint8_t>>
write_aligned_bytes(FlatBufferBuilder &builder, uint8_t *data, int32_t length) {
  // printf("\n ========> offset %d\n", builder.GetSize());
  static uint8_t dummy[4] = {0};
  if (data == nullptr) { // for scrach_tensor
    assert(length > 0);
    data = dummy;
    length = 0;
  }
  // printf("align: %p %d\n", data, length);
  builder.ForceVectorAlignment(length, sizeof(uint8_t), 16);
  flatbuffers::Offset<flatbuffers::Vector<uint8_t>> data_offset =
      builder.CreateVector<uint8_t>(data, length);
  return data_offset;
}

} // namespace binary_writer
} // namespace mlir

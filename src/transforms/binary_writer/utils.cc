/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#include "src/transforms/binary_writer/utils.h"

namespace mlir {
namespace binary_writer {

std::unordered_map<std::string, tflite::BuiltinOperator> name_to_builtin_oper{
    {"tfl.custom", tflite::BuiltinOperator_CUSTOM},
    {"tfl.softmax", tflite::BuiltinOperator_SOFTMAX},
    {"tfl.squeeze", tflite::BuiltinOperator_SQUEEZE},
    {"tfl.reshape", tflite::BuiltinOperator_RESHAPE},
    {"tfl.fully_connected", tflite::BuiltinOperator_FULLY_CONNECTED}};

std::unordered_map<std::string, tflite::BuiltinOptions> name_to_builtin_option{
    {"tfl.softmax", tflite::BuiltinOptions_SoftmaxOptions},
    {"tfl.squeeze", tflite::BuiltinOptions_SqueezeOptions},
    {"tfl.reshape", tflite::BuiltinOptions_ReshapeOptions},
    {"tfl.fully_connected", tflite::BuiltinOptions_FullyConnectedOptions}};

int32_t scratch_buf_area = 0;
int32_t scratch_fast_buf_area = 1;

std::vector<int32_t> conv_to_shape_vector(ArrayRef<int64_t> &&shape) {
  std::vector<int32_t> std_shape;
  for (auto d : shape)
    std_shape.push_back(d);
  return std_shape;
}

void Prep(FlatBufferBuilder &builder, uint32_t size,
          uint32_t additional_bytes) {
  // Track the biggest thing we've ever aligned to.
  builder.TrackMinAlign(size);
  uint32_t buf_size = builder.GetSize();
  uint32_t align_size = PaddingBytes(buf_size + additional_bytes, size);
  builder.Pad(align_size);
  // Find the amount of alignment needed such that 'size' is properly aligned
  // after 'additional_bytes'
}

void show_buffer(uint8_t *buffer, int32_t size) {
  assert(size % 4 == 0);
  uint32_t *ubuf = (uint32_t *)(buffer);
  int32_t addr = 0, u = 0;

  for (; addr + 16 <= size; addr += 16, u += 4) {
    printf("%06x: %08x %08x %08x %08x\n", addr, ubuf[u], ubuf[u + 1],
           ubuf[u + 2], ubuf[u + 3]);
  }
  if (addr != size) {
    printf("%06x:", addr);
    for (; addr < size; addr += 4)
      printf(" %08x", ubuf[u++]);
    printf("\n");
  }

  uint32_t checksum = 0;
  for (int i = 0; i < size; i++) {
    checksum += buffer[i];
  }
  printf("checksum: %08x   size: %d\n", checksum, size);
}

} // namespace binary_writer
} // namespace mlir

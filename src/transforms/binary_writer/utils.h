/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#ifndef UTILS_H
#define UTILS_H

#include "absl/memory/memory.h"
#include "mlir/Dialect/Quant/QuantOps.h"
#include "mlir/Dialect/Quant/QuantTypes.h"
#include "mlir/Dialect/StandardOps/IR/Ops.h"      // from @llvm-project
#include "mlir/IR/AffineMap.h"                    // from @llvm-project
#include "mlir/IR/Attributes.h"                   // from @llvm-project
#include "mlir/IR/BlockAndValueMapping.h"         // from @llvm-project
#include "mlir/IR/Builders.h"                     // from @llvm-project
#include "mlir/IR/Location.h"                     // from @llvm-project
#include "mlir/IR/MLIRContext.h"                  // from @llvm-project
#include "mlir/IR/Operation.h"                    // from @llvm-project
#include "mlir/IR/PatternMatch.h"                 // from @llvm-project
#include "mlir/Interfaces/InferTypeOpInterface.h" // from @llvm-project
#include "mlir/Pass/Pass.h"                       // from @llvm-project
#include "mlir/Transforms/DialectConversion.h"    // from @llvm-project

#include "src/ir/hlcs_ir.h"
#include "src/ir/llcs_ir.h"

#include "src/transforms/binary_writer/driver_actions.h"
#include "src/transforms/llcs_generator/hlcs_mlir_utils.h"
#include "src/transforms/llcs_generator/range_set.h"
#include "src/transforms/passes.h"

#include "src/utils/assert.h"
#include "src/utils/data_structure_utils.h"
#include "src/utils/printIR.h"
#include "src/utils/string_utils.h"

#include "src/compiler/compiler_cl.h"

#include <set>
#include <unordered_map>
#include <vector>

#include "tensorflow/compiler/mlir/lite/ir/tfl_ops.h"

#include "tensorflow/lite/schema/schema_generated.h"

#include "flatbuffers/flatbuffers.h"

using namespace std;
using namespace flatbuffers;
using namespace mlir;
using namespace hlcs;
using namespace llcs;
using namespace std;

namespace mlir {
namespace binary_writer {

typedef pair<uint8_t *, uint32_t> buffer_t;

using op_info =
    std::tuple<mlir::Operation *, std::vector<int32_t>, std::vector<int32_t>>;

struct TensorTuple {
  std::string name;
  int32_t buf_area;
  int32_t buf_index;
  int32_t offset;
  RankedTensorType type;
  mlir::Value v;
  DenseElementsAttr data;
};

struct MetadataTuple {
  std::string name;
  buffer_t buffer;
  int32_t buf_area;
};

extern int32_t scratch_buf_area;
extern int32_t scratch_fast_buf_area;
extern std::unordered_map<std::string, tflite::BuiltinOperator>
    name_to_builtin_oper;
extern std::unordered_map<std::string, tflite::BuiltinOptions>
    name_to_builtin_option;

std::vector<int32_t> conv_to_shape_vector(ArrayRef<int64_t> &&shape);
void Prep(FlatBufferBuilder &builder, uint32_t size, uint32_t additional_bytes);

void show_buffer(uint8_t *buffer, int32_t size);

} // namespace binary_writer
} // namespace mlir

#endif // UTILS_H

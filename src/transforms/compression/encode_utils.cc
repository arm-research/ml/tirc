/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#include "src/transforms/compression/encode_weights.h"

using namespace std;
#include <iostream>
#include <string>

namespace mlir {
namespace planner {

void compress_weights_local(
    int32_t start, int32_t end,
    mlir::ArchitectureConfig::ArchitectureConfigAttr &arch_c,
    mlir::OpBuilder &builder,
    std::vector<weight_compression_parameters> &parameters) {
  for (int32_t i = start; i < end; i++) {
    mlir::planner::compress_weights(
        builder, parameters[i].op_const_weight, parameters[i].block_type,
        parameters[i].ofm_block_depth, parameters[i].ofm_depth_step,
        parameters[i].dilation, arch_c, parameters[i].plan_type,
        parameters[i].is_depthwise_op, parameters[i].weight_buffer_count,
        parameters[i].compression_or_packing);
  }
}

void compress_biases_local(
    int32_t start, int32_t end,
    mlir::ArchitectureConfig::ArchitectureConfigAttr &arch_c,
    mlir::OpBuilder &builder,
    std::vector<bias_compression_parameters> &parameters) {
  for (int32_t i = start; i < end; i++) {
    mlir::planner::calc_scales_and_pack_biases(
        builder, parameters[i].op_const_biases, parameters[i].kernel,
        parameters[i].ofm_depth_step, parameters[i].full_ofm_depth, arch_c,
        parameters[i].rescale_for_faf, parameters[i].weight_buffer_count);
  }
}

void extend(bytearray &a, bytearray b) {
  for (int i = 0; i < b.size(); i++)
    a.push_back(b[i]);
}

void DumpWeights(int16_t *data, ArrayRef<int64_t> shape, std::string filename) {
  int index = 0;
  int line_count = 0;
  ofstream dump;
  dump.open(filename.c_str());

  for (int h = 0; h < shape[0]; h++) {
    for (int w = 0; w < shape[1]; w++) {
      for (int i = 0; i < shape[2]; i++) {
        for (int o = 0; o < shape[3]; o++) {
          int16_t d = data[index++];
          dump << d << ",";

          line_count++;
          if (line_count == 16) {
            dump << std::endl;
            line_count = 0;
          }
        }
      }
    }
  }
  dump.close();
}

void DumpWeights(std::vector<uint8_t> stream, std::string filename) {
  int index = 0;
  int line_count = 0;
  ofstream dump;
  dump.open(filename.c_str());
  for (int i = 0; i < stream.size(); i++) {
    uint8_t d = stream[i];
    dump << uint32_t(d) << ",";
    line_count++;
    if (line_count == 16) {
      dump << std::endl;
      line_count = 0;
    }
  }
  dump.close();
}

void extract_sub_region_from_ohwi_to_ohwi(
    const int16_t *weights, uint32_t height_start, uint32_t height_end,
    uint32_t width_start, uint32_t width_end, uint32_t input_start,
    uint32_t input_end, uint32_t output_start, uint32_t output_end,
    ArrayRef<int64_t> weights_shape_ohwi, weight_volume &wv) {
  int32_t size = ((height_end - height_start) * (width_end - width_start) *
                  (input_end - input_start) * (output_end - output_start));

  int16_t *weights_roi = new int16_t[size];

  wv.set_weights(weights_roi);

  Shape shape;
  shape.ohwi.height = (height_end - height_start);
  shape.ohwi.width = (width_end - width_start);
  shape.ohwi.in = (input_end - input_start);
  shape.ohwi.out = (output_end - output_start);
  wv.set_shape(shape);

  int32_t shape_ohwi_out = weights_shape_ohwi[0];
  int32_t shape_ohwi_height = weights_shape_ohwi[1];
  int32_t shape_ohwi_width = weights_shape_ohwi[2];
  int32_t shape_ohwi_in = weights_shape_ohwi[3];

  int32_t count = 0;
  for (int o = output_start; o < output_end; o++)
    for (int h = height_start; h < height_end; h++)
      for (int w = width_start; w < width_end; w++)
        for (int i = input_start; i < input_end; i++) {
          int32_t index_ohwi =
              (o * shape_ohwi_height * shape_ohwi_width * shape_ohwi_in) +
              (h * shape_ohwi_width * shape_ohwi_in) + (w * shape_ohwi_in) + i;
          weights_roi[count] = weights[index_ohwi];
          count++;
        }
}

void extract_sub_region_from_ohwi_to_ihwo(
    const int16_t *weights, uint32_t height_start, uint32_t height_end,
    uint32_t width_start, uint32_t width_end, uint32_t input_start,
    uint32_t input_end, uint32_t output_start, uint32_t output_end,
    ArrayRef<int64_t> weights_shape_ohwi, weight_volume &wv) {
  int32_t size = ((height_end - height_start) * (width_end - width_start) *
                  (input_end - input_start) * (output_end - output_start));

  int16_t *weights_roi = new int16_t[size];

  wv.set_weights(weights_roi);

  Shape shape;
  shape.ohwi.out = (input_end - input_start);
  shape.ohwi.height = (height_end - height_start);
  shape.ohwi.width = (width_end - width_start);
  shape.ohwi.in = (output_end - output_start);
  wv.set_shape(shape);

  int32_t shape_ohwi_out = weights_shape_ohwi[0];
  int32_t shape_ohwi_height = weights_shape_ohwi[1];
  int32_t shape_ohwi_width = weights_shape_ohwi[2];
  int32_t shape_ohwi_in = weights_shape_ohwi[3];

  int32_t count = 0;
  for (int i = input_start; i < input_end; i++)
    for (int h = height_start; h < height_end; h++)
      for (int w = width_start; w < width_end; w++)
        for (int o = output_start; o < output_end; o++) {
          int32_t index_ohwi =
              (o * shape_ohwi_height * shape_ohwi_width * shape_ohwi_in) +
              (h * shape_ohwi_width * shape_ohwi_in) + (w * shape_ohwi_in) + i;
          weights_roi[count] = weights[index_ohwi];
          count++;
        }
}

void weight_volume::core_deinterleaved(int32_t core, int32_t no_cores,
                                       weight_volume &wV_core_interleaved) {
  int16_t *weights_hwio = (int16_t *)get_weights();
  int16_t *weights_ohwi = new int16_t[get_size()];

  wV_core_interleaved.set_weights(weights_ohwi);

  Shape shape_ohwi;
  Shape shape_hwio = get_shape();
  shape_ohwi.ohwi.out = shape_hwio.hwio.out;
  shape_ohwi.ohwi.height = shape_hwio.hwio.height;
  shape_ohwi.ohwi.width = shape_hwio.hwio.width;
  shape_ohwi.ohwi.in = shape_hwio.hwio.in;
  wV_core_interleaved.set_shape(shape_ohwi);

  for (int h = 0; h < shape_hwio.hwio.height; h++) {
    for (int w = 0; w < shape_hwio.hwio.width; w++) {
      for (int i = 0; i < shape_hwio.hwio.in; i++) {
        for (int o = 0; o < shape_hwio.hwio.out; o++) {
          int32_t index_hwio = (h * shape_hwio.hwio.width * shape_hwio.hwio.in *
                                shape_hwio.hwio.out) +
                               (w * shape_hwio.hwio.in * shape_hwio.hwio.out) +
                               (i * shape_hwio.hwio.out) + o;

          int32_t index_ohwi =
              (o * shape_ohwi.ohwi.height * shape_ohwi.ohwi.width *
               shape_ohwi.ohwi.in) +
              (h * shape_ohwi.ohwi.width * shape_ohwi.ohwi.in) +
              (w * shape_ohwi.ohwi.in) + i;

          weights_ohwi[index_ohwi] = weights_hwio[index_hwio];
        }
      }
    }
  }

  ASSERT_COND(
      no_cores != 1,
      "encode_weights::core_dienterleaved not supported with cores > 1");
}

void weight_volume::dump(std::string filename) {
  int index = 0;
  int line_count = 0;
  ofstream dump;
  dump.open(filename.c_str());
  int16_t *weights = (int16_t *)get_weights();
  for (int i = 0; i < shape.hwio.in; i++)
    for (int w = 0; w < shape.hwio.width; w++)
      for (int h = 0; h < shape.hwio.height; h++)
        for (int o = 0; o < shape.hwio.out; o++) {
          int16_t d = weights[index++];
          dump << d << ",";

          line_count++;
          if (line_count == 16) {
            dump << std::endl;
            line_count = 0;
          }
        }
  dump.close();
}

void convert_OHWI_to_HWIO(int16_t *data, ArrayRef<int64_t> weight_shape_ohwi,
                          ArrayRef<int64_t> weight_shape_hwio) {
  int n = (weight_shape_ohwi[0] * weight_shape_ohwi[1] * weight_shape_ohwi[2] *
           weight_shape_ohwi[3]);

  int16_t *hwio = new int16_t[n];

  int index_ohwi = 0;
  for (int o = 0; o < weight_shape_ohwi[0]; o++) {
    for (int h = 0; h < weight_shape_ohwi[1]; h++) {
      for (int w = 0; w < weight_shape_ohwi[2]; w++) {
        for (int i = 0; i < weight_shape_ohwi[3]; i++) {
          int64_t vr = data[index_ohwi++];

          // o H W I
          // 0 1 2 3

          // H W I O
          // 0 1 2 3

          int index_hwio = (h * weight_shape_hwio[1] * weight_shape_hwio[2] *
                            weight_shape_hwio[3]) +
                           (w * weight_shape_hwio[2] * weight_shape_hwio[3]) +
                           (i * weight_shape_hwio[3]) + o;
          hwio[index_hwio] = vr;
        }
      }
    }
  }

  memcpy(data, hwio, n * 2);
  delete[] hwio;
}

} // namespace planner
} // namespace mlir

/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#include "src/transforms/compression/encode_weights.h"

using namespace std;
#include <iostream>
#include <string>

namespace mlir {
namespace planner {

raw_stream reorder_weight_volume(weight_volume &weightVolume,
                                 const Shape &ifm_ublock,
                                 const Shape &ofm_ublock,
                                 int32_t ofm_block_depth,
                                 const Shape &subkernel_max, bool is_depthwise,
                                 bool is_partkernel, int ifm_bitdepth) {
  int16_t *weights_input = weightVolume.get_weights();
  const Shape shape = weightVolume.get_shape();

  int kernel_width = shape.ohwi.width;
  int kernel_height = shape.ohwi.height;
  int ifm_depth = shape.ohwi.in;
  int ofm_depth = shape.ohwi.out;

  int ifm_block_depth;
  if (is_partkernel or (ifm_bitdepth == 16))
    ifm_block_depth = 16;
  else if (ifm_bitdepth == 8)
    ifm_block_depth = 32;
  else
    assert(false);

  std::vector<int16_t> stream;

  // Top level striping - OFM blocks in the entire brick's depth
  for (int ofm_block_z = 0; ofm_block_z < ofm_depth;
       ofm_block_z += ofm_block_depth) {
    int clipped_ofm_block_depth =
        std::min(ofm_block_depth, ofm_depth - ofm_block_z);
    // IFM blocks required for the brick
    for (int ifm_block_z = 0; ifm_block_z < (is_depthwise ? 1 : ifm_depth);
         ifm_block_z += ifm_block_depth) {
      int clipped_ifm_block_depth = ifm_ublock.nhwc.depth;
      if (!is_depthwise) {
        clipped_ifm_block_depth =
            is_partkernel ? std::min(ifm_block_depth, ifm_depth - ifm_block_z)
                          : ifm_block_depth;
      }
      // Weight decomposition (see DS_TOP section 5.1.6)
      // Subkernel Splitting  (H)
      for (int subkernel_y = 0; subkernel_y < kernel_height;
           subkernel_y += subkernel_max.nhwc.height) {
        int sub_height =
            std::min(kernel_height - subkernel_y, subkernel_max.nhwc.height);
        // Subkernel splitting (W)
        for (int subkernel_x = 0; subkernel_x < kernel_width;
             subkernel_x += subkernel_max.nhwc.width) {
          int sub_width =
              std::min(kernel_width - subkernel_x, subkernel_max.nhwc.width);
          int subkernel_elements = sub_width * sub_height;

          int ifm_block_depth_outer = 1;
          int ifm_block_depth_inner = clipped_ifm_block_depth;

          // Part kernel first works across the kernel H/W and needs padding
          // (see DS_MAC section 5.1.14)
          if (is_partkernel) {
            if ((ifm_bitdepth == 16) && (subkernel_elements % 2 != 0)) {
              subkernel_elements = (int)(ceil(subkernel_elements / 2.0) * 2);
            } else if ((ifm_bitdepth == 8) && (subkernel_elements % 4 != 0)) {
              subkernel_elements = (int)(ceil(subkernel_elements / 4.0) * 4);
            }
            ifm_block_depth_outer = clipped_ifm_block_depth;
            ifm_block_depth_inner = 1;
          }
          // Depthwise Conv requires multiple of 4 kernel elements in its weight
          // block
          //(see DS_MAC section 5.1.15) this is different from normal conv which
          // is
          // considered "weights depth-first"
          else if (is_depthwise) {
            subkernel_elements = (int)(ceil(subkernel_elements / 4.0) * 4);
          }
          // Partkernel outer loop (traverses planar rather than depth)
          for (int ifm_outer = 0; ifm_outer < ifm_block_depth_outer;
               ifm_outer += ifm_ublock.nhwc.depth) {
            // OFM Ublocks in OFM-block over depth
            for (int ofm_ublk = 0; ofm_ublk < clipped_ofm_block_depth;
                 ofm_ublk += ofm_ublock.nhwc.depth) {
              // HW Kernel element traversal - cannot be a H/W loop due to
              // element padding requirement on depthwise/part-kernel
              // configurations
              for (int element = 0; element < subkernel_elements; element++) {
                // Source position within the current subkernel
                int kx = element % sub_width;
                int ky = element / sub_width;
                int wx = subkernel_x + kx;
                int wy = subkernel_y + ky;
                // IFM ublocks in IFM-block over depth (only 1 ublock if
                // depthwise)
                for (int ifm_ublk = 0; ifm_ublk < ifm_block_depth_inner;
                     ifm_ublk += ifm_ublock.nhwc.depth) {
                  // Feed OFM ublock elements
                  for (int ofm_ublock_z = 0;
                       ofm_ublock_z < ofm_ublock.nhwc.depth; ofm_ublock_z++) {
                    // Source IFM ublock elements (only 1 element deep if
                    // depthwise)
                    for (int ifm_ublock_z = 0;
                         ifm_ublock_z <
                         (is_depthwise ? 1 : ifm_ublock.nhwc.depth);
                         ifm_ublock_z++) {
                      // Source IFM/OFM slices
                      int ifm_z =
                          ifm_block_z + ifm_ublk + ifm_outer + ifm_ublock_z;
                      int ofm_z = ofm_block_z + ofm_ublk + ofm_ublock_z;
                      // printf("Idx [%2d, %2d] [%3d, %3d] [%3d, %3d, %3d]\n",
                      // subkernel_x, subkernel_y, ofm_ublk, element, ifm_ublk,
                      // ofm_ublock_z, ifm_ublock_z);

                      // printf("%d %d %d %d \n", wx, wy, ifm_z, ofm_z);

                      if ((ifm_z >= ifm_depth) || (ofm_z >= ofm_depth) ||
                          (ky >= sub_height)) {
                        stream.push_back(0);
                      } else {
                        int index = (ofm_z * shape.ohwi.height *
                                     shape.ohwi.width * shape.ohwi.in) +
                                    (wy * shape.ohwi.width * shape.ohwi.in) +
                                    (wx * shape.ohwi.in) + ifm_z;
                        stream.push_back(weights_input[index]);
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }

  return stream;
}

void set_storage_shape(mlir::Value v, uint32_t no_compressed_streams = 1,
                       uint32_t doubleBuffer_longest_compressed_stream = 0) {
  SchedTensorType t = v.getType().dyn_cast<mlir::SchedTensorType>();
  uint32_t offset = 0;
  if ((t.get_purpose() == vsp_DoubleBuffer) and (no_compressed_streams > 2)) {
    offset = (2 * doubleBuffer_longest_compressed_stream);
    ASSERT_COND(offset % 16 != 0,
                "set_storage_shape::offset not multiple of 16");
  } else {
    array_1D_i32 weight_compressed_offsets = t.get_weight_compressed_offsets();
    offset = weight_compressed_offsets[weight_compressed_offsets.size() - 1];
  }
  SmallVector<int64_t, 4> storage_shape = {1, 1, 1, offset};

  t.setStorageShape(storage_shape);
}

bytearray encode(raw_stream r_stream) {
  bytearray e_stream;
  if (r_stream.size() == 0)
    return e_stream;

  int verbose = 0;
  bool int16_format = false;

  // for (auto r: r_stream)
  //    std::cout << r << ",";
  // std::cout << std::endl;

  // 0. Check Range of Input
  int32_t max = *max_element(std::begin(r_stream), std::end(r_stream));
  if (not(max <= 255)) {
    // std::cout << "Max: " << max << std::endl;
    ASSERT_COND(true, "encode_weights::encode::Max of Weight Stream > 255");
  }
  int32_t min = *min_element(std::begin(r_stream), std::end(r_stream));
  if (not(min >= -255)) {
    // std::cout << "Min: " << min << std::endl;
    ASSERT_COND(true, "encode_weights::encode::Min of Weight Stream < -255");
  }

  // 1. Create Temporary Storage
  int16_t *weights = new int16_t[r_stream.size() * sizeof(int16_t)];
  int16_t *weights_rebuilt = new int16_t[r_stream.size() * sizeof(int16_t)];

  // 2. Copy from raw stream into weights
  unsigned i = 0;
  for (int16_t element : r_stream)
    weights[i++] = element;

  // 3. Encode flattened signed weight stream
  int outbuf_size = 0;
  uint8_t *outbuf = nullptr;

#if COMPRESSION_ON
  outbuf_size = mlw_encode(weights, r_stream.size(), &outbuf, verbose);

  /*
  outbuf_size = mlw_encode(weights, r_stream.size(), &outbuf, DO_VITERBI_SEARCH,
  verbose); mlw_decode(outbuf, outbuf_size, &weights_rebuilt, verbose); bool
  equal = true; for (int i = 0; i < r_stream.size(); i++) if (weights[i] !=
  weights_rebuilt[i]) equal = false; std::cout << "Equal: "  << (equal ?
  "true":"false") << std::endl;
  */

  // 4. Copy from outbuf into Encoded Stream
  for (uint32_t i = 0; i < outbuf_size; i++)
    e_stream.push_back(outbuf[i]);
#else
  outbuf_size = r_stream.size();
  for (uint32_t i = 0; i < outbuf_size; i++)
    e_stream.push_back(r_stream[i]);
#endif

  // 5. Pad with 0xFF as needed so the length of the weight stream is a multiple
  // of 16
  while ((e_stream.size() % 16) != 0) {
    e_stream.push_back(0xFF);
  }

  // 6. Delete Intermediate Copies. We have alot of copying around here, there
  // must be a cleaner way of doing it !!!!
  delete[] weights;
  delete[] weights_rebuilt;

#if COMPRESSION_ON
  mlw_free_outbuf(outbuf);
#endif

  /*
  for (auto v: e_stream)
      std::cout << int(v) << ",";
  std::cout << std::endl;
  std::cout << std::endl;
  */

  return e_stream;
}

bytearray
encode_weights(weight_volume &weightVolume, SmallVector<int32_t, 2> dilation,
               uint32_t ifm_bitdepth, uint32_t ofm_block_depth,
               bool is_depthwise, NpuBlockTraversal block_traversal,
               mlir::ArchitectureConfig::ArchitectureConfigAttr &arch_c) {
  ASSERT_COND(ifm_bitdepth != 8,
              "encode_weights::reorder_weight_volume::Expected to be 8 if not "
              "assert as code is not checked to work with non 8");

  /*
    Internal implementation of the public facing API to use weight encoding.

    Parameters:
      accelerator: architecture_features.Accelerator enum to pick the correct
    npu accelerator weights_volume: weights in OHWI layout with a shape of
    four dilation_xy: a two element tuple of dilation attributes in x,y
    dimension ifm_bitdepth: the bitdepth of input feature map ofm_block_depth:
    the depth of blocks for npu processing is_depthwise: a boolean
    indicating these weights are used for a depthwise traversal block_traversal:
    indicates how these weights are traversed on sub-kernel basis

    Returns
    :return: a weight stream of compressed values
  */

  bool is_partkernel =
      (block_traversal == NpuBlockTraversal::tbt_PartKernelFirst);
  ASSERT_COND(
      (is_depthwise and is_partkernel),
      "encode_weights :: partkernel and depthwise are mutually exclusive");

  // Check valid values for dilation
  ASSERT_COND(dilation.size() != 2,
              "encode_weights::encode_weights::dilation size != 2");
  ASSERT_COND(not((dilation[0] == 1) or (dilation[0] == 2)),
              "encode_weights::encode_weights::dilation[0] not 1 or 2");
  ASSERT_COND(not((dilation[1] == 1) or (dilation[1] == 2)),
              "encode_weights::encode_weights::dilation[1] not 1 or 2");

  AcceleratorConfig Accelerator =
      (AcceleratorConfig)arch_c.accel_config().getInt();
  HWConfig hw_config = get_acclerator_config(Accelerator);
  Shape ifm_ublock;
  ifm_ublock.nhwc.depth = hw_config.ifm_ublock.depth;
  ifm_ublock.nhwc.width = hw_config.ifm_ublock.width;
  ifm_ublock.nhwc.height = hw_config.ifm_ublock.height;

  Shape ofm_ublock;
  ofm_ublock.nhwc.depth = hw_config.ofm_ublock.depth;
  ofm_ublock.nhwc.width = hw_config.ofm_ublock.width;
  ofm_ublock.nhwc.height = hw_config.ofm_ublock.height;

  Shape subkernel_max;
  auto subkernelMax = ArrayAttr_to_SmallVector(arch_c.subkernel_max());
  subkernel_max.nhwc.height =
      floor(float(subkernelMax[0]) / float(dilation[0]));
  subkernel_max.nhwc.width = floor(float(subkernelMax[1]) / float(dilation[1]));

  raw_stream r_stream = reorder_weight_volume(
      weightVolume, ifm_ublock, ofm_ublock, ofm_block_depth, subkernel_max,
      is_depthwise, is_partkernel, ifm_bitdepth);
  return encode(r_stream);
}

int compress_weights(OpBuilder &builder, Operation *op_const_weight,
                     NpuBlockType npu_block_type, uint32_t ofm_block_depth,
                     uint32_t ofm_depth_step, SmallVector<int32_t, 2> dilation,
                     mlir::ArchitectureConfig::ArchitectureConfigAttr &arch_c,
                     mlir::cascadeir::PlanType plan_type, bool is_depthwise_op,
                     unsigned int weight_buffer_count,
                     bool compression_or_packing) {
  /*
     The Formats may be different from the underlying data. The IR Format pass
     has normalized to HWIO or HWOI but the underlying data is still OHWI
   */

  mlir::SchedTensorType weightType =
      op_const_weight->getResult(0).getType().dyn_cast<mlir::SchedTensorType>();
  int32_t size = array_ref_prod<int64_t>(weightType.getShape());

  // 1. Load Weights from Attribute
  const int8_t *weights =
      (int8_t *)op_const_weight->getAttrOfType<mlir::RawDataAttr>("value")
          .getData();
  // 1.1 Convert to 16 Bit
  int16_t *weights_local_copy = new int16_t[size * sizeof(int16_t)];
  for (int i = 0; i < size; i++)
    weights_local_copy[i] = weights[i];

  ArrayRef<int64_t> weights_shape = weightType.getShape();

#if ENABLE_DUMPING
  {
    stringstream ss_dump_weights;
    ss_dump_weights << "CompressWeights_HWIO_Input_" << weight_buffer_count
                    << "_"
                    << (compression_or_packing ? "compression" : "packing")
                    << ".dat";
    DumpWeights(weights, weights_shape, ss_dump_weights.str());
  }
#endif

  // 2. <- Do Compression ->

  // 2.1 Check format is HWIO and Shape 4D
  ASSERT_COND(((weightType.get_format() != vf_HWIO) and
               (weightType.get_format() != vf_HWOI)),
              "encode_weights::compress_weights::Format expected is HWIO");
  ASSERT_COND(weights_shape.size() != 4,
              "encode_weights::compress_weights::Shape is not of size 4");

#if ENABLE_DUMPING
  {
    stringstream ss_dump_weights;
    ss_dump_weights << "CompressWeights_HWIO_PostZeroPoint_"
                    << weight_buffer_count << "_"
                    << (compression_or_packing ? "compression" : "packing")
                    << ".dat";
    DumpWeights(weights, weights_shape, ss_dump_weights.str());
  }
#endif

  // 2.1 Prepare MetaData
  int32_t ifm_bitdepth = getBitDepthType(weightType);
  int32_t full_ifm_depth = weights_shape[weights_shape.size() - 2]; // HWIO
  int32_t full_ofm_depth = weights_shape[weights_shape.size() - 1]; // HWIO

  if (npu_block_type == ConvolutionDepthWise)
    weightType.set_NpuBlockTraversal(tbt_DepthWise);

  if (npu_block_type == ConvolutionMxN) {
    // Determine which block traversal strategy has better DPU utilization
    int32_t kernel_size = weights_shape[0] * weights_shape[1]; // HWIO

    int32_t round_param_depth_utilization = (ifm_bitdepth == 8) ? 32 : 16;
    float depth_utilization =
        float(weights_shape[2]) /
        float(
            round_up<int32_t>(weights_shape[2], round_param_depth_utilization));

    int32_t round_param_part_kernel_utilization = (ifm_bitdepth == 8) ? 4 : 2;
    float part_kernel_utilization =
        (((float)weights_shape[2] /
          (float)round_up<int32_t>(weights_shape[2], 8)) *
         ((float)kernel_size /
          (float)round_up<int32_t>(kernel_size,
                                   round_param_part_kernel_utilization)));

    if ((part_kernel_utilization >= depth_utilization) or (full_ifm_depth <= 8))
      // Part-kernel first is always better for ifm depths <= 8
      weightType.set_NpuBlockTraversal(tbt_PartKernelFirst);
    else
      weightType.set_NpuBlockTraversal(tbt_DepthFirst);
  }

  bool is_depthwise = (weightType.get_NpuBlockTraversal() == tbt_DepthWise);

  NpuBlockTraversal block_traversal;
  if (weightType.get_NpuBlockTraversal() == tbt_PartKernelFirst)
    block_traversal = tbt_PartKernelFirst;
  else
    block_traversal = tbt_DepthFirst; /* DepthFirst and Depthwise seem to have
                                         the same semantic meaning */

  // Calculate brick size (A brick is a subregion of a weight stream)
  std::vector<int32_t> brick_size = {
      (int32_t)weights_shape[0], (int32_t)weights_shape[1],
      (int32_t)weights_shape[2],
      (int32_t)min((int64_t)weights_shape[3], (int64_t)ofm_depth_step)};
  float elements_in_brick = vector_prod<int32_t>(brick_size);

  /* The compression scale of each encoded stream */
  array_1D_fp64 compression_scales;

  /* The Offset of the encoded stream */
  array_1D_i32 compressed_offsets;

  /* Encoded Streams, There canbe more than one !!! */
  std::vector<bytearray> encoded_streams;

  /* Within each encodedstream we also track the offsets of each core  */
  array_2D_i32 encoded_streams_substream_offsets;

  // Prepare Zero Point
  std::vector<int64_t> zero_point_vector = getZeroPoint(weightType);
  int32_t depth = weights_shape[3];
  if (zero_point_vector.size() == 1) {
    for (int i = 0; i < depth - 1; i++)
      zero_point_vector.push_back(zero_point_vector[0]);
  }
  // 2.3 Subtract Zero Point from weights::Early zero-point correction
  if (is_depthwise) {
    int index = 0;
    for (int o = 0; o < weights_shape[2]; o++) {
      for (int h = 0; h < weights_shape[0]; h++) {
        for (int w = 0; w < weights_shape[1]; w++) {
          for (int i = 0; i < weights_shape[3]; i++) {
            weights_local_copy[index] =
                (weights_local_copy[index] - zero_point_vector[i]);
            index++;
          }
        }
      }
    }
  } else {
    int index = 0;
    for (int o = 0; o < weights_shape[3]; o++) {
      for (int h = 0; h < weights_shape[0]; h++) {
        for (int w = 0; w < weights_shape[1]; w++) {
          for (int i = 0; i < weights_shape[2]; i++) {
            weights_local_copy[index] =
                (weights_local_copy[index] - zero_point_vector[o]);
            index++;
          }
        }
      }
    }
  }

  // 2.4 Weight Compression
  int32_t offset = 0;
  int32_t max_single_buffer_len = 0;

  float max_compression_scale = 0.0;
  float avg_compression_scale = 0.0;

  // Slice weight stream up depth-ways into bricks and compress
  for (uint32_t idx = 0; idx < full_ofm_depth; idx += ofm_depth_step) {
    // Get the weights necessary for this brick
    int32_t count = min(full_ofm_depth - idx, ofm_depth_step);

#if ENABLE_DUMPING
    std::cout << "START DEPTH COMPRESSION " << full_ofm_depth
              << " :: " << ofm_depth_step << std::endl;
    std::cout << idx << std::endl;
    std::cout << idx + count << std::endl;
#endif

    array_1D_i32 substream_offsets;
    substream_offsets.push_back(0);

    bytearray encoded_stream;

    weight_volume weightVolume_core_deinterleaved;

    if (is_depthwise) {
      // Underlying Data: Extract & Reshape Brick Weights (OHWI -> Extract &
      // Reshape -> IHWO):
      // -> Type Shape & Format HWOI
      extract_sub_region_from_ohwi_to_ihwo(weights_local_copy, 0,
                                           weights_shape[0],    // Height
                                           0, weights_shape[1], // Width
                                           idx, idx + count,    // Input
                                           0, weights_shape[2], // Output
                                           {weights_shape[2], weights_shape[0],
                                            weights_shape[1], weights_shape[3]},
                                           weightVolume_core_deinterleaved);
    } else {
      // Underlying Data: Extract Brick Weights (OHWI -> Extract -> OHWI):
      // -> Type Shape & Format: HWIO
      extract_sub_region_from_ohwi_to_ohwi(weights_local_copy, 0,
                                           weights_shape[0],    // Height
                                           0, weights_shape[1], // Width
                                           0, weights_shape[2], // Input
                                           idx, idx + count,    // Output
                                           {weights_shape[3], weights_shape[0],
                                            weights_shape[1], weights_shape[2]},
                                           weightVolume_core_deinterleaved);
    }

#if ENABLE_DUMPING
    {
      stringstream ss_dump_weights;
      ss_dump_weights << "CompressWeights_Brick_Weights_" << weight_buffer_count
                      << "_" << idx << "_"
                      << (compression_or_packing ? "compression" : "packing")
                      << ".dat";
      weightVolume_core_deinterleaved.dump(ss_dump_weights.str());
    }
#endif

    int32_t no_cores = arch_c.no_cores().getInt();

    // For each core, deinterleave weights from the larger volume and generate
    // separate compressed streams
    for (int32_t core = 0; core < min(no_cores, full_ofm_depth); core++) {
#if ENABLE_DUMPING
      std::cout << "Shape & Strides: weightVolume" << std::endl;
      std::cout << "Min: " << weightVolume.max() << std::endl;
      std::cout << "Max: " << weightVolume.min() << std::endl;
      {
        const Shape shape = weightVolume.get_shape();
        std::cout << shape.hwio.height << "," << shape.hwio.width << ","
                  << shape.hwio.in << "," << shape.hwio.out << std::endl;
      }
      std::cout << "Shape & Strides: weightVolume_core_deinterleaved"
                << std::endl;
      std::cout << "Min: " << weightVolume_core_deinterleaved.max()
                << std::endl;
      std::cout << "Max: " << weightVolume_core_deinterleaved.min()
                << std::endl;
      {
        const Shape shape = weightVolume_core_deinterleaved.get_shape();
        std::cout << shape.ohwi.out << "," << shape.ohwi.height << ","
                  << shape.ohwi.width << "," << shape.ohwi.in << std::endl;
      }
#endif
      int block_depth =
          floor(float(ofm_block_depth + no_cores - 1 - core) / float(no_cores));
      bytearray encoded_substream;
      if (block_depth != 0) {
        encoded_substream = encode_weights(
            weightVolume_core_deinterleaved, dilation, ifm_bitdepth,
            block_depth, is_depthwise, block_traversal, arch_c);
      }
#if ENABLE_DUMPING
      std::cout << "encoded_substream: " << encoded_substream.size()
                << std::endl;
#endif
      extend(encoded_stream, encoded_substream);
      substream_offsets.push_back(encoded_stream.size());
    }

    // Store the encoded stream for the brick
    encoded_streams.push_back(encoded_stream);
    encoded_streams_substream_offsets.push_back(substream_offsets);

    float compression_scale =
        (float(encoded_stream.size()) /
         float(weightVolume_core_deinterleaved.get_size()));
    if (compression_scale > max_compression_scale)
      max_compression_scale = compression_scale;
    avg_compression_scale += compression_scale;
#if ENABLE_DUMPING
    std::cout << "weightVolume_core_deinterleaved: " << weightVolume.get_size()
              << std::endl;
    std::cout << "encoded_substream size: " << encoded_stream.size()
              << std::endl;
    std::cout << "compression_scale: " << compression_scale << std::endl;
    std::cout << std::endl;
#endif
    // Remember where we put it for linear addressing
    compressed_offsets.push_back(offset);
    offset += encoded_stream.size();
    ASSERT_COND((offset % 16 != 0),
                "compress_weights::offset not a multiple of 16");

    // Compression scale tracking
    compression_scales.push_back(encoded_stream.size() / elements_in_brick);
  }

  avg_compression_scale = avg_compression_scale / float(encoded_streams.size());

  // Track total length as last element of the offsets array
  compressed_offsets.push_back(offset);

  // 3. Add the Compressed Weight Attribute to the Const: Save parameters to the
  // SchedTensorType Type (weight compression ratio etc ...)

  /* We Store all the streams linearised immediately */
  mlir::SmallVector<uint8_t, 64> encoded_streams_flatten;
  for (auto stream : encoded_streams) {
    for (uint8_t v : stream) {
      encoded_streams_flatten.push_back(v);
    }
  }

  auto raw_data =
      (std::make_unique<unsigned char[]>(encoded_streams_flatten.size()));
  auto raw_data_p = raw_data.get();
  for (int i = 0; i < encoded_streams_flatten.size(); i++)
    raw_data_p[i] = encoded_streams_flatten[i];
  auto raw_data_attr =
      RawDataAttr::get(op_const_weight->getContext(), std::move(raw_data),
                       encoded_streams_flatten.size(), 1, getUniqueId());
  op_const_weight->setAttr("value_compressed", raw_data_attr);

  int32_t no_compressed_streams = encoded_streams.size();

  int32_t longest_compression_stream = 0;
  for (auto stream : encoded_streams) {
    if (stream.size() > longest_compression_stream) {
      longest_compression_stream = stream.size();
    }
  }
  int32_t doubleBuffer_longest_compressed_stream = longest_compression_stream;

  weightType.set_weight_compression_scales(compression_scales);
  weightType.set_weight_compressed_offsets(compressed_offsets);

  weightType.set_compression_scale_for_worst_weight_stream(
      max_compression_scale);
  weightType.set_storage_compression_scale(avg_compression_scale);
  weightType.set_bandwidth_compression_scale(avg_compression_scale);

  weightType.set_compressed_values_substream_offsets(
      encoded_streams_substream_offsets);
  weightType.set_brick_size(brick_size);

  SchedTensorType t =
      op_const_weight->getResult(0).getType().dyn_cast<mlir::SchedTensorType>();
  SmallVector<int64_t, 4> storage_shape = {1, 1, 1,
                                           encoded_streams_flatten.size()};
  t.setStorageShape(storage_shape);

#if ENABLE_LOGGING
  // 0. Dump MetaData
  std::cout << BLUE << "compress_weights: " << RESET << GREEN
            << " Plan: " << RESET << ConvertToString(plan_type).str() << GREEN
            << " Shape: " << RESET << array_ref_to_string(weightType.getShape())
            << GREEN << " Format: " << RESET
            << format_to_str(value_format(weightType.get_format())) << GREEN
            << " DataType: " << RESET
            << datatype_to_str(getDataType(weightType)) << GREEN
            << " BlockType: " << RESET << block_type_to_str(npu_block_type)
            << GREEN << " ofm_block_depth: " << RESET << ofm_block_depth
            << GREEN << " ofm_depth_step: " << RESET << ofm_depth_step << GREEN
            << " dilation: " << RESET << dilation[0] << "," << dilation[1]
            << GREEN << " SIZE: " << RESET << encoded_streams_flatten.size()
            << std::endl;
#endif

// 1. Check & Match Final Weight Stream
#if ENABLE_DUMPING_FINAL
  {
    stringstream ss_dump_weights;
    ss_dump_weights << "CompressWeights_Final_" << weight_buffer_count << "_"
                    << (compression_or_packing ? "compression" : "packing")
                    << ".dat";
    DumpWeights(encoded_streams_flatten, ss_dump_weights.str());
  }
#endif

  delete[] weights_local_copy;
}

/*******************************************/
/*                Biases                   */
/*******************************************/

int calc_scales_and_pack_biases(
    OpBuilder &builder, Operation *op_const_bias, Operation *kernel,
    uint32_t ofm_depth_step, uint32_t full_ofm_depth,
    mlir::ArchitectureConfig::ArchitectureConfigAttr &arch_c,
    bool rescale_for_faf, unsigned int bias_buffer_count) {

  mlir::SchedTensorType biasType =
      op_const_bias->getResult(0).getType().dyn_cast<mlir::SchedTensorType>();
  int32_t size = array_ref_prod<int64_t>(biasType.getShape());

  // 1. Load Biases from Attribute
  const int32_t *bias =
      (int32_t *)op_const_bias->getAttrOfType<mlir::RawDataAttr>("value")
          .getData();
  // 1.1 Convert to 32 Bit
  std::vector<int64_t> biases;
  for (int i = 0; i < size; i++)
    biases.push_back(bias[i]);

  // 2. No of Cores
  int32_t no_cores = arch_c.no_cores().getInt();
  ASSERT_COND(no_cores != 1,
              "calc_scales_and_pack_biases only works with 1 core");

  // 2. . Get Scaling Parameters from kernel
  std::vector<int32_t> shift = ArrayAttr_to_Vector(
      kernel->getAttrOfType<mlir::ArrayAttr>("output_shift"));
  std::vector<int32_t> scale = ArrayAttr_to_Vector(
      kernel->getAttrOfType<mlir::ArrayAttr>("output_multiplier"));

  // 3. Checks data structures
  //    * biases, shift, scale all same length otherwise normalize
  ASSERT_COND(shift.size() != scale.size(),
              "encode_weights:: Fatal Error shift and scale data structures "
              "have different sizes");

  // We Have no Bias. Generate a Value of 1 for each ofm_full_depth
  if (biases.size() == 0) {
    for (int i = 0; i < full_ofm_depth; i++)
      biases.push_back(1);
  }
  // We do not have per-channel quantization so we need to replicate scale and
  // shift
  if ((biases.size() > 1) and (scale.size() == 1)) {
    for (int i = 1; i < biases.size(); i++) {
      scale.push_back(scale[0]);
      shift.push_back(shift[0]);
    }
  }
  ASSERT_COND(biases.size() != scale.size(),
              "encode_weights:: Fatal Error bias and scale data structures "
              "have different sizes");
  ASSERT_COND(biases.size() != shift.size(),
              "encode_weights:: Fatal Error bias and shift data structures "
              "have different sizes");

  // std::cout << count++ << std::endl;
  // for (int i = 1; i < 10/*biases.size()*/; i++) {
  //    std::cout << biases[i] << " " << scale[i] << " " << shift[i] <<
  //    std::endl;
  //}
  // std::cout << std::endl;

  // 4. Loop over every brick
  bytearray encoded_stream;
  array_2D_i32 compressed_offsets_final;
  for (int32_t i = 0; i < full_ofm_depth; i += ofm_depth_step) {
    // std::cout << full_ofm_depth << " " << ofm_depth_step << std::endl;
    array_1D_i32 compressed_offsets;
    compressed_offsets.push_back(0);

    bytearray stream;
    int32_t max_len = min(ofm_depth_step, full_ofm_depth - i);

    for (int32_t j = i; j < (i + max_len); j++) {
      bytearray bv = bias_value(biases[j], scale[j], shift[j]).get_byte_array();
      extend(stream, bv);
    }

    // Align each stream to 16 and 10 bytes
    while (((stream.size() % 16) != 0) or ((stream.size() % 10) != 0)) {
      stream.push_back(0);
    }

    // Add to compressed values with their substream offset lists to the tensor
    extend(encoded_stream, stream);

    compressed_offsets.push_back(stream.size());
    compressed_offsets_final.push_back(compressed_offsets);
  }

  auto raw_data = (std::make_unique<unsigned char[]>(encoded_stream.size()));
  auto raw_data_p = raw_data.get();
  for (int i = 0; i < encoded_stream.size(); i++)
    raw_data_p[i] = encoded_stream[i];
  auto raw_data_attr =
      RawDataAttr::get(op_const_bias->getContext(), std::move(raw_data),
                       encoded_stream.size(), 1, getUniqueId());
  op_const_bias->setAttr("value_compressed", raw_data_attr);

#if ENABLE_DUMPING
  {
    stringstream ss_dump_weights;
    ss_dump_weights << "CompressBias_Final_" << bias_buffer_count << "_"
                    << "packing"
                    << ".dat";
    DumpWeights(encoded_stream, ss_dump_weights.str());
  }
#endif

  biasType.set_compressed_values_substream_offsets(compressed_offsets_final);

  SmallVector<int64_t, 4> storage_shape = {(int64_t)encoded_stream.size()};
  biasType.setStorageShape(storage_shape);

#if ENABLE_LOGGING
  std::cout << BLUE << "pack_biases_and_scales: " << GREEN
            << " ofm_depth_step: " << RESET << ofm_depth_step << GREEN
            << " Shape: " << RESET << array_ref_to_string(biasType.getShape())
            << GREEN << " Format: " << RESET
            << format_to_str(value_format(biasType.get_format())) << GREEN
            << " DataType: " << RESET << datatype_to_str(getDataType(biasType))
            << GREEN << " rescale_for_faf: " << RESET << rescale_for_faf
            << GREEN << " SIZE: " << RESET << encoded_stream.size() << RESET
            << std::endl;
#endif

  // std::cout << encoded_stream.size() << std::endl;
}

} // namespace planner
} // namespace mlir

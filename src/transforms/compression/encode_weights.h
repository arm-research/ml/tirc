/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#ifndef ENCODE_WEIGHTS_H
#define ENCODE_WEIGHTS_H

#include "absl/memory/memory.h"
#include "mlir/Dialect/Quant/QuantOps.h"
#include "mlir/Dialect/Quant/QuantTypes.h"
#include "mlir/Dialect/StandardOps/IR/Ops.h"      // from @llvm-project
#include "mlir/IR/AffineMap.h"                    // from @llvm-project
#include "mlir/IR/Attributes.h"                   // from @llvm-project
#include "mlir/IR/BlockAndValueMapping.h"         // from @llvm-project
#include "mlir/IR/Builders.h"                     // from @llvm-project
#include "mlir/IR/Location.h"                     // from @llvm-project
#include "mlir/IR/MLIRContext.h"                  // from @llvm-project
#include "mlir/IR/Operation.h"                    // from @llvm-project
#include "mlir/IR/PatternMatch.h"                 // from @llvm-project
#include "mlir/Interfaces/InferTypeOpInterface.h" // from @llvm-project
#include "mlir/Pass/Pass.h"                       // from @llvm-project
#include "mlir/Transforms/DialectConversion.h"    // from @llvm-project

#include "src/transforms/compression/mlw_codec/mlw_decode.h"
#include "src/transforms/compression/mlw_codec/mlw_encode.h"

#include "src/transforms/passes.h"

#include "src/ir/architecture_config/architecture_config.h"
#include "src/ir/raw_data_attribute/RawDataAttr.h"
#include "src/ir/schedule_ir.h"

#include "tensorflow/compiler/mlir/lite/ir/tfl_ops.h"

#include "src/utils/assert.h"
#include "src/utils/checkIR.h"
#include "src/utils/colors.h"
#include "src/utils/dag.h"
#include "src/utils/data_structure_utils.h"
#include "src/utils/ir_utils.h"
#include "src/utils/numeric_utils.h"
#include "src/utils/printIR.h"

namespace mlir {
namespace planner {

#define ENABLE_DUMPING 0
#define ENABLE_DUMPING_FINAL 0
#define ENABLE_LOGGING 0
#define COMPRESSION_ON 1
#define DO_VITERBI_SEARCH 0

using bytearray = mlir::SmallVector<uint8_t, 64>;
using raw_stream = std::vector<int16_t>;

class weight_volume {
public:
  Shape get_shape() { return shape; };
  int16_t *get_weights() { return weights; };

  int32_t get_size() { return (shape.d0 * shape.d1 * shape.d2 * shape.d3); }

  void set_shape(Shape s) { shape = s; };
  void set_weights(int16_t *w) { weights = w; };

  void core_deinterleaved(int32_t core, int32_t no_cores,
                          weight_volume &wV_core_interleaved);

  weight_volume() { weights = nullptr; }

  void dump(std::string filename);

  ~weight_volume() {
    if (weights)
      delete[] weights;
  }

private:
  int16_t *weights; /* Linear Arrangment in HWIO of weight sub region        */
  Shape shape;
};

struct bias_value {
  /*
    Internal implementation of public facing API to pack bias and scale values
    as required by the npu :param bias: 64bit signed number that includes
    40bit signed bias :param scale: 32bit scale value :param shift: 6bit shift
    value :return: packed 80bit
    [0(2-bits),shift(6-bits),scale(32-bits),bias(40-bits)]
  */
  bias_value(uint64_t bias, uint32_t scale, uint32_t shift) {
    byte_array.push_back((bias >> (0 * 8)) & 0xFF);
    byte_array.push_back((bias >> (1 * 8)) & 0xFF);
    byte_array.push_back((bias >> (2 * 8)) & 0xFF);
    byte_array.push_back((bias >> (3 * 8)) & 0xFF);
    byte_array.push_back((bias >> (4 * 8)) & 0xFF);
    byte_array.push_back((scale >> (0 * 8)) & 0xFF);
    byte_array.push_back((scale >> (1 * 8)) & 0xFF);
    byte_array.push_back((scale >> (2 * 8)) & 0xFF);
    byte_array.push_back((scale >> (3 * 8)) & 0xFF);
    byte_array.push_back(shift & 0x3F);
  }

  bytearray get_byte_array() { return byte_array; };

private:
  bias_value();
  bytearray byte_array;
};

struct weight_compression_parameters {
  Operation *op_const_weight;
  NpuBlockType block_type;
  uint32_t ofm_block_depth;
  uint32_t ofm_depth_step;
  SmallVector<int32_t, 2> dilation;
  bool is_depthwise_op;
  ::mlir::cascadeir::PlanType plan_type;
  int weight_buffer_count;
  bool compression_or_packing;
};

struct bias_compression_parameters {
  Operation *op_const_biases;
  Operation *kernel;
  uint32_t ofm_depth_step;
  uint32_t full_ofm_depth;
  bool rescale_for_faf;
  unsigned int weight_buffer_count;
};

void compress_weights_local(
    int32_t start, int32_t end,
    mlir::ArchitectureConfig::ArchitectureConfigAttr &arch_c,
    mlir::OpBuilder &builder,
    std::vector<weight_compression_parameters> &parameters);

void compress_biases_local(
    int32_t start, int32_t end,
    mlir::ArchitectureConfig::ArchitectureConfigAttr &arch_c,
    mlir::OpBuilder &builder,
    std::vector<bias_compression_parameters> &parameters);

int compress_weights(OpBuilder &builder, Operation *op_const_weight,
                     NpuBlockType block_type, uint32_t ofm_block_depth,
                     uint32_t ofm_depth_step, SmallVector<int32_t, 2> dilation,
                     mlir::ArchitectureConfig::ArchitectureConfigAttr &arch_c,
                     ::mlir::cascadeir::PlanType plan_type,
                     bool is_depthwise_op, unsigned int weight_buffer_count = 0,
                     bool compression_or_packing = true);

int calc_scales_and_pack_biases(
    OpBuilder &builder, Operation *op_const_biases, Operation *kernel,
    uint32_t ofm_depth_step, uint32_t full_ofm_depth,
    mlir::ArchitectureConfig::ArchitectureConfigAttr &arch_c,
    bool rescale_for_faf = false, unsigned int weight_buffer_count = 0);

void extract_sub_region_from_hwio(int16_t *weights, uint32_t height_start,
                                  uint32_t height_end, uint32_t width_start,
                                  uint32_t width_end, uint32_t input_start,
                                  uint32_t input_end, uint32_t output_start,
                                  uint32_t output_end,
                                  ArrayRef<int64_t> weights_shape_hwio, // hwio
                                  weight_volume &wv);

void extract_sub_region_from_ohwi_to_ohwi(
    const int16_t *weights, uint32_t height_start, uint32_t height_end,
    uint32_t width_start, uint32_t width_end, uint32_t input_start,
    uint32_t input_end, uint32_t output_start, uint32_t output_end,
    ArrayRef<int64_t> weights_shape_ohwi, weight_volume &wv);

void extract_sub_region_from_ohwi_to_ihwo(
    const int16_t *weights, uint32_t height_start, uint32_t height_end,
    uint32_t width_start, uint32_t width_end, uint32_t input_start,
    uint32_t input_end, uint32_t output_start, uint32_t output_end,
    ArrayRef<int64_t> weights_shape_ohwi, weight_volume &wv);

void extend(bytearray &a, bytearray b);
void DumpWeights(std::vector<uint8_t> stream, std::string filename);
void DumpWeights(int16_t *data, ArrayRef<int64_t> shape, std::string filename);

void convert_OHWI_to_HWIO(int16_t *data, ArrayRef<int64_t> weight_shape_ohwi,
                          ArrayRef<int64_t> weight_shape_hwio);

} // namespace planner
} // namespace mlir

#endif // ENCODE_WEIGHTS_H

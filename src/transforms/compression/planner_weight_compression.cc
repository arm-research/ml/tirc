/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#include "absl/memory/memory.h"
#include "mlir/Dialect/Quant/QuantOps.h"
#include "mlir/Dialect/Quant/QuantTypes.h"
#include "mlir/Dialect/StandardOps/IR/Ops.h"      // from @llvm-project
#include "mlir/IR/AffineMap.h"                    // from @llvm-project
#include "mlir/IR/Attributes.h"                   // from @llvm-project
#include "mlir/IR/BlockAndValueMapping.h"         // from @llvm-project
#include "mlir/IR/Builders.h"                     // from @llvm-project
#include "mlir/IR/Location.h"                     // from @llvm-project
#include "mlir/IR/MLIRContext.h"                  // from @llvm-project
#include "mlir/IR/Operation.h"                    // from @llvm-project
#include "mlir/IR/PatternMatch.h"                 // from @llvm-project
#include "mlir/Interfaces/InferTypeOpInterface.h" // from @llvm-project
#include "mlir/Pass/Pass.h"                       // from @llvm-project
#include "mlir/Transforms/DialectConversion.h"    // from @llvm-project

#include "src/transforms/compression/encode_weights.h"
#include "src/transforms/passes.h"

#include "src/ir/architecture_config/architecture_config.h"
#include "src/ir/schedule_ir.h"

#include "src/utils/checkIR.h"
#include "src/utils/dag.h"
#include "src/utils/ir_utils.h"
#include "src/utils/printIR.h"
#include "src/utils/threading.h"
#include "src/utils/timing.h"

#include "tensorflow/compiler/mlir/lite/ir/tfl_ops.h"

#define ENABLE_TIMING 0

using namespace std;
#include <iostream>

#define PASS_NAME "planner-compress-weights"
#define DEBUG_TYPE PASS_NAME

using namespace mlir;

namespace mlir {
namespace planner {
namespace {

int apply_pass(FuncOp function, MLIRContext *context) {
  OpBuilder builder(context);

  Region *region = function.getCallableRegion();
  llvm::iplist<Block> &blocks = region->getBlocks();
  if (blocks.size() > 1) {
    std::cout
        << "PlannerWeightCompression:::More than a single block in function"
        << std::endl;
    return 1;
  }

  int weight_buffer_count = 0;

  llvm::iplist<Operation> &operations = region->front().getOperations();
  for (auto &op : operations) {
    // Detect schedule_ir.npu_region
    if (llvm::isa<scheduleir::NpuRegionOp>(op)) {
      std::vector<weight_compression_parameters> parameters;

      mlir::ArchitectureConfig::ArchitectureConfigAttr arch_c =
          AccessArchitectureConfigAttribute_From_NpuRegion(&op);

      mlir::CompilerConfig::CompilerConfigAttr compiler_config =
          AccessCompilerConfigAttribute_From_NpuRegion(&op);

      bool failure;
      llvm::iplist<Operation> &operations_nr = get_operations(op, failure);
      if (failure)
        return 1;

      for (auto &op_nr : operations_nr) {
        Operation *op_const_weight;
        NpuBlockType block_type;
        uint32_t ofm_block_depth;
        uint32_t ofm_depth_step;
        SmallVector<int32_t, 2> dilation = {1, 1};

        bool is_depthwise_op = llvm::isa<scheduleir::DepthwiseConv2DOp>(&op_nr);

        if ((llvm::isa<scheduleir::Conv2DOp>(&op_nr)) ||
            (llvm::isa<scheduleir::DepthwiseConv2DOp>(&op_nr))) {
          op_const_weight =
              op_nr.getOperand(1).getDefiningOp(); /* Passes the type and the
                                                      Const Operation */
          if (llvm::isa<mlir::scheduleir::DmaOp>(op_const_weight))
            op_const_weight = op_const_weight->getOperand(0).getDefiningOp();
          ASSERT_COND(
              not(llvm::isa<scheduleir::ConstOp>(*op_const_weight)),
              "planner_weight_compression::op_const_weight not a const");

          // ArrayRef<NamedAttribute> attr = op_nr.getAttrs();
          // for (auto a: attr)
          //     std::cout << a.first.str() << std::endl;

          block_type = (mlir::utils::NpuBlockType)op_nr
                           .getAttrOfType<IntegerAttr>("block_type")
                           .getInt();
          if (!block_type)
            FATAL_ERROR(
                "planner_weight_compression: block type attribute not found");

          ofm_block_depth = 16;
          ofm_depth_step = 16;

          ArrayAttr d = op_nr.getAttrOfType<ArrayAttr>("dilation");
          if (!d)
            FATAL_ERROR(
                "planner_weight_compression: dilation attribute not found");
          dilation = ArrayAttr_to_SmallVector(d);
        } else if (llvm::isa<scheduleir::FullyConnectedOp>(&op_nr)) {
          op_const_weight =
              op_nr.getOperand(1).getDefiningOp(); /* Passes the type and the
                                                      Const Operation */
          if (llvm::isa<mlir::scheduleir::DmaOp>(op_const_weight))
            op_const_weight = op_const_weight->getOperand(0).getDefiningOp();
          block_type = (mlir::utils::NpuBlockType)op_nr
                           .getAttrOfType<IntegerAttr>("block_type")
                           .getInt();
          ofm_block_depth = 16;
          ofm_depth_step = 16;
        } else
          continue; /* Not a Weight Based Operation */

        /* Populate Parameters */
        weight_compression_parameters p;
        p.op_const_weight = op_const_weight;
        p.block_type = block_type;
        p.ofm_block_depth = ofm_block_depth;
        p.ofm_depth_step = ofm_depth_step;
        p.dilation = dilation;
        p.plan_type = ::mlir::cascadeir::PlanType::UNDEFINED;
        p.is_depthwise_op = is_depthwise_op;
        p.weight_buffer_count = weight_buffer_count;
        p.compression_or_packing = true;
        parameters.push_back(p);
        weight_buffer_count++;

        /* Weight Cache Parameters */
        op_const_weight->getResult(0)
            .getType()
            .dyn_cast<mlir::SchedTensorType>()
            .setWeightCompressionCache_BlockType(block_type);
        op_const_weight->getResult(0)
            .getType()
            .dyn_cast<mlir::SchedTensorType>()
            .setWeightCompressionCache_OFMBlockDepth(ofm_block_depth);
        op_const_weight->getResult(0)
            .getType()
            .dyn_cast<mlir::SchedTensorType>()
            .setWeightCompressionCache_OFMDepthStep(ofm_depth_step);
      }

#if MULTI_THREAD
      if (parameters.size() > 0) {
        std::vector<std::thread> threads;
        std::vector<int32_t> thread_start;
        std::vector<int32_t> thread_end;
        workload_distribution(parameters.size(), thread_start, thread_end,
                              MULTI_THREAD_MAX);
        for (int i = 0; i < thread_start.size(); i++)
          threads.push_back(std::thread(
              compress_weights_local, thread_start[i], thread_end[i],
              std::ref(arch_c), std::ref(builder), std::ref(parameters)));
        for (auto &th : threads) {
          th.join();
        }
      }
#else
      compress_weights_local(0, parameters.size(), std::ref(arch_c),
                             std::ref(builder), std::ref(parameters));
#endif
    }
  }
  return 0;
}

class PlannerWeightCompression
    : public PassWrapper<PlannerWeightCompression, FunctionPass> {
public:
  explicit PlannerWeightCompression() {}

  StringRef getArgument() const final {
    // This is the argument used to refer to the pass in
    // the textual format (on the commandline for example).
    return PASS_NAME;
  }
  StringRef getDescription() const final {
    // This is a brief description of the pass.
    return "Compress Passes that have weights";
  }

  void runOnFunction() override;
};

void PlannerWeightCompression::runOnFunction() {
  auto function = getFunction();
  auto *ctx = &getContext();

  if (function.getName() == "main") {
    if (mlir::planner::apply_pass(function, ctx) > 0)
      signalPassFailure();
  }
}

} // anonymous namespace

std::unique_ptr<OperationPass<FuncOp>> CreatePlannerWeightCompression() {
  return absl::make_unique<PlannerWeightCompression>();
}

static PassRegistration<PlannerWeightCompression> pass;

} // namespace planner
} // namespace mlir

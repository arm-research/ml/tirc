/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#include "absl/memory/memory.h"
#include "mlir/Dialect/Quant/QuantOps.h"
#include "mlir/Dialect/Quant/QuantTypes.h"
#include "mlir/Dialect/StandardOps/IR/Ops.h"      // from @llvm-project
#include "mlir/IR/AffineMap.h"                    // from @llvm-project
#include "mlir/IR/Attributes.h"                   // from @llvm-project
#include "mlir/IR/BlockAndValueMapping.h"         // from @llvm-project
#include "mlir/IR/Builders.h"                     // from @llvm-project
#include "mlir/IR/Location.h"                     // from @llvm-project
#include "mlir/IR/MLIRContext.h"                  // from @llvm-project
#include "mlir/IR/Operation.h"                    // from @llvm-project
#include "mlir/IR/PatternMatch.h"                 // from @llvm-project
#include "mlir/Interfaces/InferTypeOpInterface.h" // from @llvm-project
#include "mlir/Pass/Pass.h"                       // from @llvm-project
#include "mlir/Transforms/DialectConversion.h"    // from @llvm-project

#include "src/transforms/passes.h"

#include "src/ir/architecture_config/architecture_config.h"
#include "src/utils/checkIR.h"
#include "src/utils/ir_utils.h"
#include "src/utils/printIR.h"

#include "src/ir/cascade_ir.h"
#include "src/ir/kernel_ir.h"
#include "src/ir/schedule_ir.h"
#include "src/transforms/compression/encode_weights.h"
#include "src/utils/dag.h"

using namespace std;
#include <iostream>

#define PASS_NAME "planner-pack-weights"
#define DEBUG_TYPE PASS_NAME

namespace mlir {
namespace planner {
namespace {

int apply_pass(FuncOp function, MLIRContext *context) {
  OpBuilder builder(context);

  Region *region = function.getCallableRegion();
  llvm::iplist<Block> &blocks = region->getBlocks();
  if (blocks.size() > 1) {
    std::cout << "PlannerWeightPacking:::More than a single block in function"
              << std::endl;
    return 1;
  }
  llvm::iplist<Operation> &operations_ir = region->front().getOperations();

  int weight_buffer_count = 0;

  // Loop over IR & detect NPU Region
  for (auto &op_ir : operations_ir) {
    if (llvm::isa<scheduleir::NpuRegionOp>(op_ir)) {
      std::vector<weight_compression_parameters> parameters_weights;
      std::vector<bias_compression_parameters> parameters_biases;

      mlir::ArchitectureConfig::ArchitectureConfigAttr arch_c =
          AccessArchitectureConfigAttribute_From_NpuRegion(&op_ir);

      mlir::CompilerConfig::CompilerConfigAttr compiler_config =
          AccessCompilerConfigAttribute_From_NpuRegion(&op_ir);

      bool failure;
      llvm::iplist<Operation> &operations_npu_regon =
          get_operations(op_ir, failure);
      if (failure)
        return 1;

      // Loop over Cascades
      for (auto &op_cascade : operations_npu_regon) {
        if (llvm::isa<cascadeir::CascadeRegionOp>(op_cascade)) {
          llvm::iplist<Operation> &operations_cascade =
              get_operations(op_cascade, failure);
          if (failure)
            return 1;

          ::mlir::cascadeir::PlanType plan_type = (::mlir::cascadeir::PlanType)(
              op_cascade.getAttrOfType<::mlir::IntegerAttr>("plan_type")
                  .getInt());

          // Loop over Kernels
          for (auto &op_kernel : operations_cascade) {
            if (llvm::isa<kernelir::KernelRegionOp>(op_kernel)) {
              NpuBlockType block_type =
                  (mlir::utils::NpuBlockType)op_kernel
                      .getAttrOfType<IntegerAttr>("block_type")
                      .getInt();
              auto block_config = ArrayAttr_to_SmallVector(
                  op_kernel.getAttrOfType<::mlir::ArrayAttr>("block_config"));

              // 1. Update CompressedWeights given we now have details about
              // scheduling
              SmallVector<Value, 1> weights =
                  mlir::kernelir::ExtractKernelWeightValues(&op_kernel);
              if (weights.size() == 0)
                continue;
              ASSERT_COND(weights.size() != 1,
                          "planner_weight_packing:: More than 1 Weight Tensor "
                          "in a Kernel");

              Operation *op_const_weight = weights[0].getDefiningOp();
              uint32_t ofm_block_depth;

              Operation *op_kernel_primary_op =
                  mlir::kernelir::getKernelMajorOp(&op_kernel);
              bool is_depthwise_op = llvm::isa<scheduleir::DepthwiseConv2DOp>(
                  *op_kernel_primary_op);

              ArrayRef<int64_t> weight_shape =
                  weights[0]
                      .getType()
                      .dyn_cast<mlir::SchedTensorType>()
                      .getShape();

              uint32_t ofm_depth_step;
              bool weight_streaming_plan =
                  ((plan_type == ::mlir::cascadeir::PlanType::
                                     IFM_UNDEFINED_WEIGHT_STREAMING) or
                   (plan_type == ::mlir::cascadeir::PlanType::
                                     IFM_STREAMING_WEIGHT_STREAMING) or
                   (plan_type == ::mlir::cascadeir::PlanType::
                                     IFM_STATIONARY_WEIGHT_STREAMING));

              if (weight_streaming_plan and consumed_only_by_dma_Op(weights[0]))
                ofm_depth_step = block_config[block_config.size() - 1];
              else
                ofm_depth_step = weight_shape[weight_shape.size() - 1];

              SmallVector<int32_t, 2> dilation = {1, 1};
              ofm_block_depth = block_config[block_config.size() - 1];

              Operation *op_processing_weight =
                  mlir::kernelir::getKernelMajorOp(&op_kernel);
              if ((llvm::isa<scheduleir::Conv2DOp>(op_processing_weight)) ||
                  (llvm::isa<scheduleir::DepthwiseConv2DOp>(
                      op_processing_weight)))
                dilation = ArrayAttr_to_SmallVector(
                    op_processing_weight->getAttrOfType<ArrayAttr>("dilation"));
              else if (llvm::isa<scheduleir::FullyConnectedOp>(
                           op_processing_weight))
                continue;
              else
                ASSERT_COND(true, "planner_weight_packing:: Node Consuming "
                                  "Weight is Ambigous");

              int32_t block_type_cache =
                  op_const_weight->getResult(0)
                      .getType()
                      .dyn_cast<mlir::SchedTensorType>()
                      .getWeightCompressionCache_BlockType();
              uint32_t ofm_block_depth_cache =
                  op_const_weight->getResult(0)
                      .getType()
                      .dyn_cast<mlir::SchedTensorType>()
                      .getWeightCompressionCache_OFMBlockDepth();
              uint32_t ofm_depth_step_cache =
                  op_const_weight->getResult(0)
                      .getType()
                      .dyn_cast<mlir::SchedTensorType>()
                      .getWeightCompressionCache_OFMDepthStep();

              if (not((block_type_cache == block_type) and
                      (ofm_block_depth_cache == ofm_block_depth) and
                      (ofm_depth_step_cache == ofm_depth_step))) {
                weight_compression_parameters p;
                p.op_const_weight = op_const_weight;
                p.block_type = block_type;
                p.ofm_block_depth = ofm_block_depth;
                p.ofm_depth_step = ofm_depth_step;
                p.dilation = dilation;
                p.plan_type = plan_type;
                p.is_depthwise_op = is_depthwise_op;
                p.weight_buffer_count = weight_buffer_count;
                p.compression_or_packing = false;
                parameters_weights.push_back(p);
              }

              // 2. Create Packed Scales, Shifts and Bias
              SmallVector<Value, 1> biases =
                  mlir::kernelir::ExtractKernelBiasValues(&op_kernel);
              if (biases.size() == 0)
                continue;
              ASSERT_COND(biases.size() != 1, "planner_weight_packing:: More "
                                              "than 1 Bias Tensor in a Kernel");
              bias_compression_parameters p;
              p.op_const_biases = biases[0].getDefiningOp();
              p.kernel = &op_kernel;
              p.ofm_depth_step = ofm_depth_step;
              p.full_ofm_depth = weight_shape[weight_shape.size() - 1];
              p.rescale_for_faf = false;
              p.weight_buffer_count = weight_buffer_count;
              parameters_biases.push_back(p);

              weight_buffer_count++;
            }
          }
        }
      }

#if MULTI_THREAD
      {
        std::vector<std::thread> threads;
        std::vector<int32_t> thread_start;
        std::vector<int32_t> thread_end;
        workload_distribution(parameters_weights.size(), thread_start,
                              thread_end, MULTI_THREAD_MAX);
        for (int i = 0; i < thread_start.size(); i++)
          threads.push_back(std::thread(compress_weights_local, thread_start[i],
                                        thread_end[i], std::ref(arch_c),
                                        std::ref(builder),
                                        std::ref(parameters_weights)));
        for (auto &th : threads) {
          th.join();
        }
      }
      {
        std::vector<std::thread> threads;
        std::vector<int32_t> thread_start;
        std::vector<int32_t> thread_end;
        workload_distribution(parameters_biases.size(), thread_start,
                              thread_end, MULTI_THREAD_MAX);
        for (int i = 0; i < thread_start.size(); i++)
          threads.push_back(std::thread(compress_biases_local, thread_start[i],
                                        thread_end[i], std::ref(arch_c),
                                        std::ref(builder),
                                        std::ref(parameters_biases)));
        for (auto &th : threads) {
          th.join();
        }
      }
#else
      compress_weights_local(0, parameters_weights.size(), std::ref(arch_c),
                             std::ref(builder), std::ref(parameters_weights));
      compress_biases_local(0, parameters_biases.size(), std::ref(arch_c),
                            std::ref(builder), std::ref(parameters_biases));
#endif
    }
  }

  for (auto &op_ir : operations_ir) {
    if (llvm::isa<scheduleir::NpuRegionOp>(op_ir)) {
      bool failure;
      llvm::iplist<Operation> &operations_npu_regon =
          get_operations(op_ir, failure);
      if (failure)
        return 1;

      // Loop over Cascades
      for (auto &op_cascade : operations_npu_regon) {
        if (llvm::isa<cascadeir::CascadeRegionOp>(op_cascade)) {
          llvm::iplist<Operation> &operations_cascade =
              get_operations(op_cascade, failure);
          if (failure)
            return 1;

          ::mlir::cascadeir::PlanType plan_type = (::mlir::cascadeir::PlanType)(
              op_cascade.getAttrOfType<::mlir::IntegerAttr>("plan_type")
                  .getInt());

          // Loop over Kernels
          for (auto &op_kernel : operations_cascade) {
            if (llvm::isa<kernelir::KernelRegionOp>(op_kernel)) {
              llvm::iplist<Operation> &operations_kernel =
                  get_operations(op_kernel, failure);
              if (failure)
                return 1;

              /* Do not Update if as this is updated by the rewrites in the
               * scheduler */

              SmallVector<Value, 1> int_values =
                  kernelir::ExtractKernelIntermediateValues(&op_kernel);
              for (auto v : int_values) {
                auto op_dma = v.getDefiningOp();
                if (llvm::isa<scheduleir::DmaOp>(op_dma)) {
                  std::vector<connection> connections =
                      ExtractConsumers(op_dma);
                  ASSERT_COND(connections.size() != 1,
                              "planner_weight_packing::Dma Op Consumed by more "
                              "than 1 operation");
                  if (connections[0].external_slot_index == 1) {
                    bool copy_storage_shape =
                        ((plan_type != ::mlir::cascadeir::PlanType::
                                           IFM_UNDEFINED_WEIGHT_STREAMING) and
                         (plan_type != ::mlir::cascadeir::PlanType::
                                           IFM_STREAMING_WEIGHT_STREAMING) and
                         (plan_type != ::mlir::cascadeir::PlanType::
                                           IFM_STATIONARY_WEIGHT_STREAMING));
                    clone_scheduler_metadata(op_dma->getOperand(0),
                                             op_dma->getResult(0), plan_type,
                                             copy_storage_shape);
                  } else if (connections[0].external_slot_index == 2)
                    clone_scheduler_metadata(op_dma->getOperand(0),
                                             op_dma->getResult(0), plan_type,
                                             true);
                  else
                    FATAL_ERROR("planner_weight_packing::Dma Op Consumed by "
                                "illegal index");
                }
              }

              /*
              for (auto &op_dma: operations_kernel)
              {
                  if (llvm::isa<scheduleir::DmaOp>(op_dma))
                      clone_scheduler_metadata(op_dma.getOperand(0),
              op_dma.getResult(0), plan_type, copy_storage_shape);
              }
              */
            }
          }
        }
      }
    }
  }
  return 0;
}

class PlannerWeightPacking
    : public PassWrapper<PlannerWeightPacking, FunctionPass> {
public:
  explicit PlannerWeightPacking() {}

  StringRef getArgument() const final {
    // This is the argument used to refer to the pass in
    // the textual format (on the commandline for example).
    return PASS_NAME;
  }
  StringRef getDescription() const final {
    // This is a brief description of the pass.
    return "Compress Passes that have weights";
  }

  void runOnFunction() override;
};

void PlannerWeightPacking::runOnFunction() {
  auto function = getFunction();
  auto *ctx = &getContext();

  if (function.getName() == "main") {
    if (apply_pass(function, ctx) > 0)
      signalPassFailure();
  }
}

} // anonymous namespace

std::unique_ptr<OperationPass<FuncOp>> CreatePlannerWeightPacking() {
  return absl::make_unique<PlannerWeightPacking>();
}

static PassRegistration<PlannerWeightPacking> pass;

} // namespace planner
} // namespace mlir

/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#include "absl/memory/memory.h"
#include "mlir/Dialect/Quant/QuantOps.h"
#include "mlir/Dialect/Quant/QuantTypes.h"
#include "mlir/Dialect/StandardOps/IR/Ops.h"      // from @llvm-project
#include "mlir/IR/AffineMap.h"                    // from @llvm-project
#include "mlir/IR/Attributes.h"                   // from @llvm-project
#include "mlir/IR/BlockAndValueMapping.h"         // from @llvm-project
#include "mlir/IR/Builders.h"                     // from @llvm-project
#include "mlir/IR/Location.h"                     // from @llvm-project
#include "mlir/IR/MLIRContext.h"                  // from @llvm-project
#include "mlir/IR/Operation.h"                    // from @llvm-project
#include "mlir/IR/PatternMatch.h"                 // from @llvm-project
#include "mlir/Interfaces/InferTypeOpInterface.h" // from @llvm-project
#include "mlir/Pass/Pass.h"                       // from @llvm-project
#include "mlir/Support/FileUtilities.h"
#include "mlir/Support/ToolUtilities.h"
#include "mlir/Transforms/DialectConversion.h" // from @llvm-project
#include "llvm/Support/ToolOutputFile.h"
#include "llvm/Support/raw_ostream.h"

#include "src/ir/hlcs_ir.h"
#include "src/transforms/llcs_generator/hlcs_helper.h"
#include "src/transforms/llcs_generator/hlcs_mlir_utils.h"
#include "src/transforms/llcs_generator/hlcs_npu_api.h"
#include "src/transforms/llcs_generator/range_set.h"
#include "src/transforms/llcs_generator/tensor_info.h"
#include "src/utils/string_utils.h"

using namespace mlir;

#define PASS_NAME "hlcs-calc-dependancy"
#define DEBUG_TYPE PASS_NAME

#include <iostream>
#include <iterator>
using namespace std;

namespace mlir {

namespace hlcs {

namespace {

typedef std::vector<int32_t> ivec;
typedef std::pair<ivec, ivec> ivec_pair;

struct Watermark {
  int32_t npu = 0;
  int32_t dma = 0;
};

llvm::raw_ostream &operator<<(llvm::raw_ostream &os, const Watermark &w) {
  os << "<Watermark> npu:" << w.npu << ", dma:" << w.dma;
}

llvm::raw_ostream &operator<<(llvm::raw_ostream &os, const ivec &v) {
  os << "[";
  size_t l = v.size();
  for (size_t i = 0; i < l; ++i) {
    if (i)
      os << ", ";
    os << v[i];
  }
  os << "]";
  return os;
}

void show_memory_access_set(const std::string &name, MemoryAccessSet &mas) {
  std::string buffer;
  llvm::raw_string_ostream os(buffer);
  buffer.clear();
  mas.print(os);
  printf(">>> %s %s\n", name.c_str(), buffer.c_str());
}

Watermark getCmdWaitDependency(
    std::vector<mlir::Operation *> &cmd_stream,
    std::unordered_map<mlir::Operation *, MemoryAccessSet> &memory_accesses,
    int32_t cmd_index, Watermark &dep_watermark) {

  const int32_t arch_max_outstanding_kernels = 3;
  const int32_t arch_max_outstanding_dma = 1;

  mlir::Operation &cmd = *cmd_stream[cmd_index];
  MemoryAccessSet &cmd_access = memory_accesses[&cmd];
  int index = cmd_index - 1;

  // NPU dependency tracking
  int npu_outstanding = -1;
  int npu_ops = 0;
  int npu_index = dep_watermark.npu;

  // DMA dependency tracking
  int dma_outstanding = -1;
  int dma_ops = 0;
  int dma_index = dep_watermark.dma;

  // Seek back in the command stream looking for NPU or DMA dependencies
  // but only as far as the first dependency or the watermarks (dependencies
  // before this point have been satisfied already).
  // The watermark moves to after the latest element we must wait for, not
  // the command that issues the wait.
  // NPU->NPU dependency is handled via blockdep.
  while ((index >= npu_index) || (index >= dma_index)) {
    mlir::Operation &prev_cmd = *cmd_stream[index];
    MemoryAccessSet &prev_access = memory_accesses[&prev_cmd];

    // show_memory_access_set("prev", prev_access);
    // show_memory_access_set("curr", cmd_access);
    // Check NPU consuming DMA output
    if (llvm::isa<hlcs::DmaCmd>(prev_cmd)) {
      if (index >= dma_index) {
        if (!llvm::isa<hlcs::DmaCmd>(cmd) && (dma_outstanding == -1) &&
            prev_access.conflicts(cmd_access))
          dma_outstanding = dma_ops;
        dma_ops += 1; // Count DMA ops in the pipeline
        if (dma_ops >= arch_max_outstanding_dma)
          dma_index = std::max(index + 1, dma_index);
      }
    } else { // Check DMA consuming NPU output
      if (index >= npu_index) {
        if (llvm::isa<hlcs::DmaCmd>(cmd) && (npu_outstanding == -1) &&
            prev_access.conflicts(cmd_access))
          npu_outstanding = npu_ops;
        npu_ops += 1; // Count NPU ops in the pipeline
        if (npu_ops >= arch_max_outstanding_kernels)
          npu_index = std::max(index + 1, npu_index);
      }
    }
    --index;
  }

  // Update DMA watermark if we didn't see any and the NPU pipeline is full
  if ((dma_ops == 0) && (npu_ops >= arch_max_outstanding_kernels))
    dma_index = cmd_index;

  // Bring the search watermark forwards as we complete for those dependencies
  dep_watermark.npu = npu_index;
  dep_watermark.dma = dma_index;
  return Watermark{npu_outstanding, dma_outstanding};
}

void printout(ivec &v, const char *flag) {
  printf("%s  ", flag);
  for (int i : v)
    printf("%d ", i);
  printf("\n");
}

// Returns available number of SHRAM banks depending on activation lookup table
// being used or not
//
// TODO: use correct constant in arch instead of magic number.
const int32_t arch_shram_total_banks = 46;
const int32_t arch_shram_reserved_unused_banks = 2;
const int32_t arch_shram_bank_size = 1024;
int32_t arch_available_shram_banks(bool uses_activation_lut) {
  int32_t banks = arch_shram_total_banks;
  if (uses_activation_lut && arch_shram_reserved_unused_banks == 0)
    banks -= 2;
  return banks;
}

MemoryAccessSet getOpMemoryAccesses(mlir::Operation &cmd) {
  auto &kernel = *cmd.getOperand(0).getDefiningOp();
  auto ifms = get_operands_from_variadic(cmd, "ifm_range");
  auto ofms = get_operands_from_variadic(cmd, "ofm_range");
  auto &ofm = *ofms[0];

  std::vector<NpuAddressRange> read_ranges = get_address_ranges(*ifms[0]);
  if (ifms.size() > 1) {
    assert(ifms[1] != nullptr);
    // Scalar doesn't occupy Shared Buffer
    int32_t ifm2_rank = get_ivec_from_attr(*ifms[1], "shape").size();
    if (ifm2_rank > 0) {
      std::vector<NpuAddressRange> ifm2_ranges = get_address_ranges(*ifms[1]);
      read_ranges.insert(read_ranges.end(),
                         std::make_move_iterator(ifm2_ranges.begin()),
                         std::make_move_iterator(ifm2_ranges.end()));
    }
  }
  ivecvec weights = get_ivecvec_from_attr(cmd, "weights");
  for (auto &weight : weights) {
    read_ranges.push_back(NpuAddressRange{weight[0], weight[1], weight[2]});
  }
  ivecvec biases = get_ivecvec_from_attr(cmd, "biases");
  for (auto &bias : biases) {
    read_ranges.push_back(NpuAddressRange{bias[0], bias[1], bias[2]});
  }
  NpuActivationOp act_op_type = static_cast<NpuActivationOp>(
      get_i32_from_attr(cmd, "activation_op_type"));
  if (act_op_type == NpuActivationOp::TABLE_LOOKUP) {
    int32_t address = arch_available_shram_banks(true) * arch_shram_bank_size;
    read_ranges.push_back(NpuAddressRange{
        static_cast<int32_t>(BasePointerIndex::Mem2Mem), address, 2048});
  }

  std::vector<NpuAddressRange> write_ranges = get_address_ranges(ofm);
  // Add write access to SHRAM, needed when LUTs can overwrite accumulator banks
  bool uses_lut = (act_op_type == NpuActivationOp::TABLE_LOOKUP);
  int32_t written_shram_size =
      arch_available_shram_banks(uses_lut) * arch_shram_bank_size;
  write_ranges.push_back(NpuAddressRange{
      static_cast<int32_t>(BasePointerIndex::Mem2Mem), 0, written_shram_size});

  MemoryAccessSet mas;
  for (auto &read_range : read_ranges) {
    mas.add(MemoryRangeSet(get_region_name(read_range.region),
                           read_range.address,
                           read_range.address + read_range.length),
            AccessDirection::Read);
  }
  for (auto &write_range : write_ranges) {
    mas.add(MemoryRangeSet(get_region_name(write_range.region),
                           write_range.address,
                           write_range.address + write_range.length),
            AccessDirection::Write);
  }

  return std::move(mas);
}

MemoryAccessSet getDmaMemoryAccesses(mlir::Operation &cmd) {
  MemoryAccessSet mas;

  int32_t src_region = get_i32_from_attr(cmd, "src_region");
  int32_t src_address = get_i32_from_attr(cmd, "src_address");
  int32_t dest_region = get_i32_from_attr(cmd, "dest_region");
  int32_t dest_address = get_i32_from_attr(cmd, "dest_address");
  int32_t size = get_i32_from_attr(cmd, "size");

  auto src_mrs = MemoryRangeSet(get_region_name(src_region), src_address,
                                src_address + size);
  auto dest_mrs = MemoryRangeSet(get_region_name(dest_region), dest_address,
                                 dest_address + size);

  mas.add(src_mrs, AccessDirection::Read);
  mas.add(dest_mrs, AccessDirection::Write);

  return std::move(mas);
}

class HLCSCalcDependancy
    : public PassWrapper<HLCSCalcDependancy, FunctionPass> {
private:
  std::vector<mlir::Operation *>
  getNpuOpStream(mlir::Operation &npu_region_op) {
    std::vector<mlir::Operation *> cmd_stream;
    mlir::Block &block = npu_region_op.getRegion(0).getBlocks().front();
    assert(block.hasNoSuccessors());
    for (mlir::Operation &cmdlist : block.getOperations()) {
      if (!llvm::isa<hlcs::CommandListOp>(cmdlist))
        continue;
      mlir::Block &bb = cmdlist.getRegion(0).getBlocks().front();
      assert(bb.hasNoSuccessors());
      for (mlir::Operation &cmd : bb.getOperations()) {
        if (!llvm::isa<hlcs::DmaCmd>(cmd) &&
            !llvm::isa<hlcs::NpuStripeCmd>(cmd))
          continue;
        // if cmd.cmdtype == CommandType.NpuStripe and cmd.ps.npu_block_type ==
        // NpuBlockType.Default:
        //     print("Warning: Skipping register command stream generation for",
        //     cmd.ps)
        cmd_stream.push_back(&cmd);
      }
    }
    return std::move(cmd_stream);
  }

  void insertIssueAndWaitCmd(std::vector<mlir::Operation *> &cmd_stream,
                             OpBuilder &builder) {
    for (auto cmd : cmd_stream) {
      builder.setInsertionPointAfter(cmd);
      auto dummy = builder.getI32IntegerAttr(0);
      auto wait_cmd =
          builder.create<hlcs::WaitCmd>(cmd->getLoc(), dummy, dummy);
      auto issue_cmd = builder.create<hlcs::IssueCmd>(cmd->getLoc());
    }
  }

  std::unordered_map<mlir::Operation *, MemoryAccessSet>
  getMemoryAccessSet(std::vector<mlir::Operation *> &cmd_stream) {
    std::unordered_map<mlir::Operation *, MemoryAccessSet> memory_accesses;
    for (auto cmd : cmd_stream) {
      if (llvm::isa<hlcs::DmaCmd>(*cmd)) {
        memory_accesses[cmd] = getDmaMemoryAccesses(*cmd);
      } else if (llvm::isa<hlcs::NpuStripeCmd>(*cmd)) {
        memory_accesses[cmd] = getOpMemoryAccesses(*cmd);
      } else {
        assert(!"Unknown Command!");
      }
      /* std::string name = get_op_name(*cmd); */
      /* show_memory_access_set(name, memory_accesses[cmd]); */
    }
    return std::move(memory_accesses);
  }

  std::vector<Watermark> calcDependency(
      std::vector<mlir::Operation *> &cmd_stream,
      std::unordered_map<mlir::Operation *, MemoryAccessSet> &memory_accesses) {
    Watermark dep_watermark;
    std::vector<Watermark> cmd_waits;
    size_t cnt = cmd_stream.size();
    for (int cmd_index = 0; cmd_index < cnt; cmd_index++) {
      Watermark outstanding = getCmdWaitDependency(cmd_stream, memory_accesses,
                                                   cmd_index, dep_watermark);
      cmd_waits.push_back(outstanding);
    }
    return std::move(cmd_waits);
  }

  void updateWaitCmd(mlir::Operation &npu_region_op, OpBuilder &builder,
                     std::vector<Watermark> &cmd_waits) {
    size_t idx = 0;
    mlir::Block &block = npu_region_op.getRegion(0).getBlocks().front();
    assert(block.hasNoSuccessors());
    for (mlir::Operation &cmdlist : block.getOperations()) {
      if (!llvm::isa<hlcs::CommandListOp>(cmdlist))
        continue;
      mlir::Block &bb = cmdlist.getRegion(0).getBlocks().front();
      assert(bb.hasNoSuccessors());
      for (mlir::Operation &cmd : bb.getOperations()) {
        if (!llvm::isa<hlcs::WaitCmd>(cmd))
          continue;
        auto &watermark = cmd_waits[idx++];
        set_i32_to_attr(builder, cmd, "npu_outstanding_count", watermark.npu);
        set_i32_to_attr(builder, cmd, "dma_outstanding_count", watermark.dma);
      }
    }
  }

public:
  HLCSCalcDependancy() = default;

  StringRef getArgument() const final {
    // This is the argument used to refer to the pass in
    // the textual format (on the commandline for example).
    return PASS_NAME;
  }
  StringRef getDescription() const final {
    // This is a brief description of the pass.
    return "Calculate HLCS command dependancy";
  }

  void runOnFunction() override {
    auto function = getFunction();
    if (function.getName() != "main")
      return;
    // printf("subpass: HLCSCalcDependancy\n");

    MLIRContext *context = &getContext();
    OpBuilder builder(context);
    auto name = function.getName();

    mlir::Region *region = function.getCallableRegion();
    for (mlir::Block &bb : region->getBlocks()) {
      for (mlir::Operation &op : bb.getOperations()) {
        std::string name = get_op_name(op);
        if (!llvm::isa<hlcs::NpuRegionOp>(op))
          continue;
        std::vector<mlir::Operation *> cmd_stream =
            std::move(getNpuOpStream(op));
        insertIssueAndWaitCmd(cmd_stream, builder);
        std::unordered_map<mlir::Operation *, MemoryAccessSet> memory_accesses =
            std::move(getMemoryAccessSet(cmd_stream));
        std::vector<Watermark> cmd_waits =
            std::move(calcDependency(cmd_stream, memory_accesses));
        updateWaitCmd(op, builder, cmd_waits);
      }
    }
  }
};

} // anonymous namespace

std::unique_ptr<OperationPass<FuncOp>> CreateHLCSCalcDependencyPass() {
  return absl::make_unique<HLCSCalcDependancy>();
}

static PassRegistration<HLCSCalcDependancy> pass;
} // namespace hlcs
} // namespace mlir

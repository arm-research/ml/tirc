/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#include "absl/memory/memory.h"
#include "mlir/Dialect/Quant/QuantOps.h"
#include "mlir/Dialect/Quant/QuantTypes.h"
#include "mlir/Dialect/StandardOps/IR/Ops.h"      // from @llvm-project
#include "mlir/IR/AffineMap.h"                    // from @llvm-project
#include "mlir/IR/Attributes.h"                   // from @llvm-project
#include "mlir/IR/BlockAndValueMapping.h"         // from @llvm-project
#include "mlir/IR/Builders.h"                     // from @llvm-project
#include "mlir/IR/Location.h"                     // from @llvm-project
#include "mlir/IR/MLIRContext.h"                  // from @llvm-project
#include "mlir/IR/Operation.h"                    // from @llvm-project
#include "mlir/IR/PatternMatch.h"                 // from @llvm-project
#include "mlir/Interfaces/InferTypeOpInterface.h" // from @llvm-project
#include "mlir/Pass/Pass.h"                       // from @llvm-project
#include "mlir/Support/FileUtilities.h"
#include "mlir/Support/ToolUtilities.h"
#include "mlir/Transforms/DialectConversion.h" // from @llvm-project
#include "llvm/Support/ToolOutputFile.h"
#include "llvm/Support/raw_ostream.h"

#include "src/ir/hlcs_ir.h"
#include "src/transforms/llcs_generator/hlcs_helper.h"
#include "src/transforms/llcs_generator/hlcs_mlir_utils.h"
#include "src/transforms/llcs_generator/hlcs_npu_api.h"
#include "src/transforms/llcs_generator/range_set.h"
#include "src/transforms/llcs_generator/tensor_info.h"
#include "src/utils/string_utils.h"

using namespace mlir;
using namespace hlcs;

#define PASS_NAME "hlcs-conv-to-npu-op"
#define DEBUG_TYPE PASS_NAME

#include <iostream>
using namespace std;

namespace mlir {

namespace hlcs {

//// helper
bool arch_is_spilling_enabled = false;
int32_t get_region(mlir::Operation &tensor_info) {
  unordered_map<MemoryType, BasePointerIndex> base_ptr_idx_map{
      {MemoryType::PERMANENT_NPU, BasePointerIndex::WeightTensor},
      {MemoryType::PERMANENT_CPU, BasePointerIndex::WeightTensor},
      {MemoryType::SCRATCH, BasePointerIndex::ScratchTensor},
  };
  if (arch_is_spilling_enabled) {
    base_ptr_idx_map[MemoryType::SCRATCH_FAST] =
        BasePointerIndex::ScratchFastTensor;
  } else {
    base_ptr_idx_map[MemoryType::SCRATCH_FAST] =
        BasePointerIndex::ScratchTensor;
  }

  MemoryType mem_type =
      static_cast<MemoryType>(get_i32_from_attr(tensor_info, "mem_type"));
  return static_cast<int32_t>(base_ptr_idx_map[mem_type]);
}

int32_t get_ifm_depth(NpuBlockType npu_block_type, Box &ifm_box, Box &ofm_box) {
  int32_t depth = 0;
  if (npu_block_type == NpuBlockType::ConvolutionMxN ||
      npu_block_type == NpuBlockType::VectorProduct ||
      npu_block_type == NpuBlockType::ReduceSum) {
    depth = ifm_box.get_block().depth;
  } else {
    depth = ofm_box.get_block().depth;
  }
  return depth;
}

bool is_maxpool_op(string &type) { return type == "scheduleir.max_pool2d"; }

bool is_avgpool_op(string &type) { return type == "scheduleir.avg_pool2d"; }

bool ifm_ifm2_correct_order(ivec &ifm_shape, ivec &ifm2_shape) {
  if (ifm_shape.empty())
    return false; // scalar needs to be in IFM2
  if (ifm2_shape.empty())
    return true;
  uint32_t length = std::min(ifm_shape.size(), ifm2_shape.size());
  for (uint32_t i = 0; i < length; i++) {
    // Compare dimension from right to left
    uint32_t ifm_axis = ifm_shape.size() - i;
    uint32_t ifm2_axis = ifm2_shape.size() - i;
    if ((ifm_shape[ifm_axis] != ifm2_shape[ifm2_axis]) and
        (ifm_shape[ifm_axis] == 1))
      return false; // Broadcasted FM needs to be in IFM2
  }
  return true;
}

bool is_fused_quantize(mlir::Operation &kernel) {
  for (mlir::Operation &op : kernel.getRegion(0).front().getOperations()) {
    string type = get_str_from_attr(op, "operator_type");
    if (type == "Quantize")
      return true;
  }
  bool fused_quantize =
      get_bool_from_attr_with_default(kernel, "fused_quantize", false);
  return fused_quantize;
}

bool use_zero_point_0(mlir::Operation &kernel, mlir::Operation &tensor,
                      bool is_ifm_tensor) {
  // Checks if quantization should use 0 as zero point
  string dtype = get_str_from_attr(tensor, "dtype");
  if (dtype == "int32" && is_ifm_tensor)
    return true;
  mlir::Operation &primary_op = getPrimaryOp(kernel);

  // TODO: revisit here when we got test case.
  // string type = get_str_from_attr(primary_op, "type");
  // if (type != "AvgPool" && type != "ResizeBilinear" && type != "CLZ" && type
  // != "SHL") return false;
  string type = get_str_from_attr(primary_op, "operator_type");
  if (type != "scheduleir.avg_pool2d" && type != "scheduleir.resize" &&
      type != "scheduleir.clz" && type != "scheduleir.logical_left_shift")
    return false;

  bool fused_quantize = is_fused_quantize(kernel);
  if (fused_quantize)
    return false;

  string memory_function = get_str_from_attr(primary_op, "memory_function");
  if (memory_function == "ConcatSliceWrite")
    return false;

  int32_t act_op_idx = get_i32_from_attr(kernel, "kernel_act_op_index");

  // string forced_ofm_quantization = get_str_from_attr(primary_op,
  // "forced_output_quantization");
  bool use_0 =
      ((act_op_idx == -1) || (true /*forced_ofm_quantization is not None*/));
  use_0 =
      use_0 && true; //(ps.primary_op.memory_function != Op.ConcatSliceWrite)
  use_0 = use_0 && !fused_quantize;
  return use_0;
}

NpuElementWiseOp get_elementwise_op(string &op_type) {
  assert(op_type.substr(0, 10) == "scheduleir");
  string elementwise_op = op_type.substr(11);
  static unordered_map<string, NpuElementWiseOp> elementwise_op_lut{
      {"mul", NpuElementWiseOp::MUL}, {"add", NpuElementWiseOp::ADD},
      {"sub", NpuElementWiseOp::SUB}, {"min", NpuElementWiseOp::MIN},
      {"max", NpuElementWiseOp::MAX}, {"leakyrelu", NpuElementWiseOp::LRELU},
      {"abs", NpuElementWiseOp::ABS}, {"clz", NpuElementWiseOp::CLZ},
      {"shr", NpuElementWiseOp::SHR}, {"shl", NpuElementWiseOp::SHL},
  };
  return elementwise_op_lut[elementwise_op];
}

NpuRoundingMode get_rounding_mode(mlir::Operation &cmd, bool fused_quantize) {
  auto &kernel = *cmd.getOperand(0).getDefiningOp();
  mlir::Operation &primary_op = getPrimaryOp(kernel);
  string rounding_mode_str =
      get_str_from_attr(primary_op, "attrs_rounding_mode");
  if (!rounding_mode_str.empty()) {
    static unordered_map<string, NpuRoundingMode> rounding_mode_lut{
        {"TFL", NpuRoundingMode::TFL},
        {"TRUNCATE", NpuRoundingMode::TRUNCATE},
        {"NATURAL", NpuRoundingMode::NATURAL},
    };
    return rounding_mode_lut[rounding_mode_str];
  }

  string type = get_str_from_attr(primary_op, "type");
  if (type == "ResizeBilinear") {
    return NpuRoundingMode::TRUNCATE;
  }

  auto ifm_stripes = get_operands_from_variadic(cmd, "ifm_range");
  assert(ifm_stripes.size() > 0);
  auto &ifm_stripe_mem = *ifm_stripes[0];
  auto &ifm_tensor = *ifm_stripe_mem.getOperand(0).getDefiningOp();
  string ifm_dtype = get_str_from_attr(ifm_tensor, "dtype");
  NpuBlockType npu_block_type =
      static_cast<NpuBlockType>(get_i32_from_attr(kernel, "block_type"));
  if ((ifm_dtype == "int16") &&
      (npu_block_type == NpuBlockType::ConvolutionMxN ||
       npu_block_type == NpuBlockType::ConvolutionDepthWise)) {
    return NpuRoundingMode::NATURAL;
  }

  string memory_function = get_str_from_attr(primary_op, "memory_function");
  ivec kernel_hw = get_ivec_from_attr(primary_op, "kernel");
  assert(kernel_hw.size() == 2);
  int32_t kernel_h = kernel_hw[0];
  int32_t kernel_w = kernel_hw[1];
  if (!fused_quantize &&
      ((type == "QuantizedAvgPool") || (type == "AvgPool")) &&
      (memory_function == "ConcatSliceWrite") &&
      (kernel_w == 1 && kernel_h == 1)) {
    return NpuRoundingMode::NATURAL;
  }
  return NpuRoundingMode::TFL;
}

NpuResamplingMode get_upscale(mlir::Operation &primary_op) {
  string type = get_str_from_attr(primary_op, "type");
  NpuResamplingMode upscale = NpuResamplingMode::NONE;
  if (type == "ResizeBilinear")
    // perform nearest neighbor upscale
    upscale = NpuResamplingMode::NEAREST;
  else if (type == "Conv2DBackpropInputSwitchedBias")
    // perform insert zero upscale
    upscale = NpuResamplingMode::TRANSPOSE;
  return upscale;
}

namespace {

class HLCSConvToNpuOp : public PassWrapper<HLCSConvToNpuOp, FunctionPass> {
private:
  void setup_kernel(mlir::OpBuilder &bldr, mlir::Operation &cmd,
                    mlir::Operation &primary_op) {
    ivec kernel_hw = get_ivec_from_attr(primary_op, "kernel");
    set_i32_to_attr(bldr, cmd, "kernel_h", kernel_hw[0]);
    set_i32_to_attr(bldr, cmd, "kernel_w", kernel_hw[1]);
    ivec kernel_stride = get_ivec_from_attr(primary_op, "stride");
    set_ivec_to_attr(bldr, cmd, "kernel_stride", kernel_stride);
    ivec kernel_dilation = get_ivec_from_attr(primary_op, "dilation");
    set_ivec_to_attr(bldr, cmd, "kernel_dilation", kernel_dilation);
  }
  void setup_padding(mlir::OpBuilder &bldr, mlir::Operation &cmd, Box &ifm_box,
                     int32_t ifm_width) {
    auto &kernel = *cmd.getOperand(0).getDefiningOp();
    NpuBlockType npu_block_type =
        static_cast<NpuBlockType>(get_i32_from_attr(kernel, "block_type"));
    if (npu_block_type == NpuBlockType::VectorProduct) {
      ivec zero_padding{0, 0, 0, 0};
      set_ivec_to_attr(bldr, cmd, "padding", zero_padding);
      return;
    }
    mlir::Operation &primary_op = getPrimaryOp(kernel);
    // TODO: revisit this
    ivec padding = get_ivec_from_attr(primary_op, "padding");
    int32_t &top = padding[0];
    int32_t &left = padding[1];
    int32_t &bottom = padding[2];
    int32_t &right = padding[3];

    // Check if this is for horizontal ifm streaming
    bool is_first_h_stripe = get_bool_from_attr(cmd, "is_first_h_stripe");
    bool is_last_h_stripe = get_bool_from_attr(cmd, "is_last_h_stripe");
    if (!(is_first_h_stripe && is_last_h_stripe)) {
      top = get_i32_from_attr(cmd, "pad_top");
      bottom = get_i32_from_attr(cmd, "pad_bottom");
    }

    // Indexing from end since a 1x1 Avgpool might have been added with non
    // 4-dimensional input/output, because of activation function needed to be
    // fused.
    int32_t start_coord_size = ifm_box.start_coord.size();
    int32_t end_coord_size = ifm_box.end_coord.size();
    if (start_coord_size >= 2 && ifm_box.start_coord[start_coord_size - 2] > 0)
      left = 0;
    if (end_coord_size >= 2 &&
        ifm_box.end_coord[end_coord_size - 2] < ifm_width)
      right = 0;
    set_ivec_to_attr(bldr, cmd, "padding", padding);
  }

  void setup_feature_map(mlir::OpBuilder &bldr, mlir::Operation &op, Box &box) {
    auto &tensor = *op.getOperand(0).getDefiningOp();
    set_str_to_attr(bldr, op, "category", "feature_map");
    set_i32_to_attr(bldr, op, "region", get_region(tensor));

    string data_type = get_str_from_attr(tensor, "dtype");
    set_str_to_attr(bldr, op, "data_type", data_type);

    TensorFormat format =
        static_cast<TensorFormat>(get_i32_from_attr(tensor, "format"));
    assert(format == TensorFormat::NHWC || format == TensorFormat::NHCWB16);
    NpuLayout layout = NpuLayout::NHWC;
    if (format == TensorFormat::NHCWB16)
      layout = NpuLayout::NHCWB16;
    set_i32_to_attr(bldr, op, "layout", static_cast<int32_t>(layout));

    int32_t height_0 = 0, height_1 = 0, width_0 = 0;
    ivec addresses;
    tie(height_0, height_1, width_0, addresses) =
        getAddressesForRollingBuffer(tensor, box.start_coord, box.end_coord);
    for (int i = 0; i < addresses.size(); ++i) {
      if (addresses[i] == -1)
        addresses[i] = 0;
    }
    set_i32_to_attr(bldr, op, "tiles_height_0", height_0);
    set_i32_to_attr(bldr, op, "tiles_height_1", height_1);
    set_i32_to_attr(bldr, op, "tiles_width_0", width_0);
    set_ivec_to_attr(bldr, op, "tiles_addresses", addresses);

    ivec strides, tmp_coord;
    tie(strides, tmp_coord) = getStridesAndCoord(tensor);
    strides = ivec{strides[2], strides[3], strides[1]};
    set_ivec_to_attr(bldr, op, "strides", strides);
  }

  void setup_weights(mlir::OpBuilder &bldr, mlir::Operation &cmd,
                     mlir::Operation &wt_stripe_mem, Box &box) {
    ivecvec weights;
    auto &wt_tensor = *wt_stripe_mem.getOperand(0).getDefiningOp();
    int32_t stream_index =
        getCompressedStreamIndexFromCoord(wt_tensor, box.start_coord);
    ivec weight_substream_offsets = get_ivecvec_from_attr(
        wt_tensor, "compressed_values_substream_offsets")[stream_index];
    int32_t substreams = weight_substream_offsets.size() - 1;

    // Extract weight substream offsets and calculate their lengths
    assert(weight_substream_offsets.size() > 1 &&
           weight_substream_offsets[0] == 0);
    int32_t weight_addr = getAddressForCoordinate(wt_tensor, box.start_coord);
    int32_t region = get_region(wt_tensor);
    for (int core = 0; core < substreams; ++core) {
      int32_t address = weight_addr + weight_substream_offsets[core];
      int32_t length =
          weight_substream_offsets[core + 1] - weight_substream_offsets[core];
      weights.push_back({region, address, length});
    }
    set_ivecvec_to_attr(bldr, cmd, "weights", weights);
  }

  void setup_bias(mlir::OpBuilder &bldr, mlir::Operation &cmd,
                  mlir::Operation &weight_stripe, mlir::Operation &scale_stripe,
                  Box &box) {
    ivecvec biases;
    auto &weight_tensor = *weight_stripe.getOperand(0).getDefiningOp();
    int32_t stream_index =
        getCompressedStreamIndexFromCoord(weight_tensor, box.start_coord);
    auto &scale_tensor = *scale_stripe.getOperand(0).getDefiningOp();
    ivec scale_substream_offsets = get_ivecvec_from_attr(
        scale_tensor, "compressed_values_substream_offsets")[stream_index];
    int32_t substreams = scale_substream_offsets.size() - 1;

    // Extract scale substream offsets and calculate their lengths
    assert(scale_substream_offsets.size() > 1 &&
           scale_substream_offsets[0] == 0);
    ivec coord{box.start_coord.back()};
    int32_t scale_addr = getAddressForCoordinate(scale_tensor, coord);

    int32_t region = get_region(scale_tensor);
    for (int core = 0; core < substreams; ++core) {
      int32_t address = scale_addr + scale_substream_offsets[core];
      int32_t length =
          scale_substream_offsets[core + 1] - scale_substream_offsets[core];
      biases.push_back({region, address, length});
    }
    set_ivecvec_to_attr(bldr, cmd, "biases", biases);
  }

  void setup_npu_activation(mlir::OpBuilder &bldr, mlir::Operation &op,
                            mlir::Operation &primary_op) {
    NpuActivation act(NpuActivationOp::NONE_OR_RELU);
    std::string faf = get_str_from_attr(primary_op, "activation_op_type");
    if (!faf.empty()) {
      if (faf == "Tanh")
        act.op_type = NpuActivationOp::TANH;
      else if (faf == "Sigmoid")
        act.op_type = NpuActivationOp::SIGMOID;
      else if (faf == "LUT")
        act.op_type = NpuActivationOp::TABLE_LOOKUP;
      else if (faf != "Relu" && faf != "Relu6" && faf != "ReluN1To1" &&
               faf != "Clip")
        assert(!"Unsupported fused_activation_function");

      act.min = get_f32_from_attr(primary_op, "activation_min");
      act.max = get_f32_from_attr(primary_op, "activation_max");
      act.lut_index = get_i32_from_attr(primary_op, "activation_lut_index");
    }

    set_i32_to_attr(bldr, op, "activation_op_type",
                    static_cast<int32_t>(act.op_type));
    set_f32_to_attr(bldr, op, "activation_min", act.min);
    set_f32_to_attr(bldr, op, "activation_max", act.max);
    set_i32_to_attr(bldr, op, "activation_lut_index", act.lut_index);
  }

  void setup_ifm_or_ifm2_quantization(mlir::OpBuilder &bldr,
                                      mlir::Operation &stripe_mem,
                                      mlir::Operation &kernel) {
    auto &tensor = *stripe_mem.getOperand(0).getDefiningOp();
    /* string quantization = get_str_from_attr(tensor, "quantization"); */
    /* if (quantization.empty()) return; */
    int32_t zero_point = 0;
    if (!use_zero_point_0(kernel, tensor, true)) {
      ivec zero_points = get_ivec_from_attr(tensor, "zero_point");
      assert(zero_points.size() == 1);
      zero_point = zero_points[0];
    }
    fvec scales = get_fvec_from_attr(tensor, "scale");
    assert(scales.size() == 1);
    float scale_f32 = scales[0];
    set_i32_to_attr(bldr, stripe_mem, "zero_point", zero_point);
    set_f32_to_attr(bldr, stripe_mem, "scale_f32", scale_f32);
  }

  void setup_ofm_quantization(mlir::OpBuilder &bldr,
                              mlir::Operation &stripe_mem,
                              mlir::Operation &kernel) {
    auto &tensor = *stripe_mem.getOperand(0).getDefiningOp();
    mlir::Operation &primary_op = getPrimaryOp(kernel);
    if (static_cast<bool>(primary_op.getAttr("forced_output_quantization"))) {
      assert(!"TODO: check corner case LUT tensor");
    }

    int32_t zero_point = 0;
    if (!use_zero_point_0(kernel, tensor, false)) {
      ivec zero_points = get_ivec_from_attr(tensor, "zero_point");
      assert(zero_points.size() == 1);
      zero_point = zero_points[0];
    }
    fvec scales = get_fvec_from_attr(tensor, "scale");
    assert(scales.size() == 1);
    float scale_f32 = scales[0];
    set_i32_to_attr(bldr, stripe_mem, "zero_point", zero_point);
    set_f32_to_attr(bldr, stripe_mem, "scale_f32", scale_f32);
  }

  void setup_common_op_fields(mlir::OpBuilder &bldr, mlir::Operation &cmd) {
    auto &kernel = *cmd.getOperand(0).getDefiningOp();
    NpuBlockType npu_block_type =
        static_cast<NpuBlockType>(get_i32_from_attr(kernel, "block_type"));

    auto ifm_stripes = get_operands_from_variadic(cmd, "ifm_range");
    assert(ifm_stripes.size() > 0);
    auto &ifm_stripe_mem = *ifm_stripes[0];
    auto &ifm_tensor = *ifm_stripe_mem.getOperand(0).getDefiningOp();

    auto ofm_stripes = get_operands_from_variadic(cmd, "ofm_range");
    assert(ofm_stripes.size() > 0);
    auto &ofm_stripe_mem = *ofm_stripes[0];
    auto &ofm_tensor = *ofm_stripe_mem.getOperand(0).getDefiningOp();

    ivec ifm_start_coord = get_ivec_from_attr(ifm_stripe_mem, "start_coord");
    ivec ifm_end_coord = get_ivec_from_attr(ifm_stripe_mem, "end_coord");
    Box ifm_box(ifm_start_coord, ifm_end_coord);

    ivec ofm_start_coord = get_ivec_from_attr(ofm_stripe_mem, "start_coord");
    ivec ofm_end_coord = get_ivec_from_attr(ofm_stripe_mem, "end_coord");
    Box ofm_box(ofm_start_coord, ofm_end_coord);

    ivec ifm_tensor_shape = get_ivec_from_attr(ifm_tensor, "shape");
    int32_t ifm_height = ifm_box.get_block().height;
    int32_t ifm_width = NpuBlock(ifm_tensor_shape).width;
    int32_t ifm_depth = get_ifm_depth(npu_block_type, ifm_box, ofm_box);
    ivec ifm_shape{ifm_height, ifm_width, ifm_depth};

    setup_feature_map(bldr, ifm_stripe_mem, ifm_box);
    set_ivec_to_attr(bldr, ifm_stripe_mem, "shape", ifm_shape);
    setup_ifm_or_ifm2_quantization(bldr, ifm_stripe_mem, kernel);

    auto out_block = ofm_box.get_block();
    setup_feature_map(bldr, ofm_stripe_mem, ofm_box);

    ivec ofm_shape{out_block.height, out_block.width, out_block.depth};
    set_ivec_to_attr(bldr, ofm_stripe_mem, "shape", ofm_shape);
    setup_ofm_quantization(bldr, ofm_stripe_mem, kernel);

    auto wt_stripes = get_operands_from_variadic(cmd, "weight_range");
    if (wt_stripes.size()) {
      assert(wt_stripes.size() == 1);
      auto &wt_stripe_mem = *wt_stripes[0];
      ivec wt_start_coord = get_ivec_from_attr(wt_stripe_mem, "start_coord");
      ivec wt_end_coord = get_ivec_from_attr(wt_stripe_mem, "end_coord");
      Box wt_box(wt_start_coord, wt_end_coord);
      setup_weights(bldr, cmd, wt_stripe_mem, wt_box);
    }

    auto scale_stripes = get_operands_from_variadic(cmd, "scale_range");
    if (scale_stripes.size()) {
      assert(wt_stripes.size() == 1);
      assert(scale_stripes.size() == 1);
      auto &wt_stripe_mem = *wt_stripes[0];
      ivec wt_start_coord = get_ivec_from_attr(wt_stripe_mem, "start_coord");
      ivec wt_end_coord = get_ivec_from_attr(wt_stripe_mem, "end_coord");
      Box wt_box(wt_start_coord, wt_end_coord);
      auto &scale_stripe_mem = *scale_stripes[0];
      setup_bias(bldr, cmd, wt_stripe_mem, scale_stripe_mem, wt_box);
    }

    auto &primary_op = getPrimaryOp(kernel);
    setup_npu_activation(bldr, cmd, primary_op);

    bool fused_quantize = is_fused_quantize(kernel);
    set_bool_to_attr(bldr, cmd, "fused_quantize", fused_quantize);

    NpuRoundingMode rounding_mode = get_rounding_mode(cmd, fused_quantize);
    set_i32_to_attr(bldr, cmd, "rounding_mode",
                    static_cast<int32_t>(rounding_mode));

    ivec block_config = get_ivec_from_attr(kernel, "block_config");
    assert(block_config.size() == 4);
    block_config.erase(block_config.begin() + 2);
    set_ivec_to_attr(bldr, cmd, "block_config", block_config);

    if (npu_block_type != NpuBlockType::ElementWise) {
      setup_padding(bldr, cmd, ifm_box, ifm_width);
      //# npu_op.kernel = to_npu_kernel(op.kernel)
      setup_kernel(bldr, cmd, primary_op);
    } else {
      ivec zero_padding{0, 0, 0, 0};
      ivec one_array{1, 1};
      set_ivec_to_attr(bldr, cmd, "padding", zero_padding);
      set_i32_to_attr(bldr, cmd, "kernel_w", 1);
      set_i32_to_attr(bldr, cmd, "kernel_h", 1);
      set_ivec_to_attr(bldr, cmd, "kernel_stride", one_array);
      set_ivec_to_attr(bldr, cmd, "kernel_dilation", one_array);
    }

    NpuResamplingMode ifm_upscale = get_upscale(primary_op);
    set_i32_to_attr(bldr, cmd, "ifm_upscale",
                    static_cast<int32_t>(ifm_upscale));
  }

  void setup_dma_op(mlir::OpBuilder &bldr, mlir::Operation &cmd) {
    set_i32_to_attr(bldr, cmd, "op_type",
                    static_cast<int32_t>(NpuOperationType::Dma));
    set_i32_to_attr(bldr, cmd, "channel", 0);
    set_i32_to_attr(bldr, cmd, "mode", 0);

    auto &src_stripe_mem = *cmd.getOperand(0).getDefiningOp();
    auto &in_tensor = *src_stripe_mem.getOperand(0).getDefiningOp();
    auto &dest_stripe_mem = *cmd.getOperand(1).getDefiningOp();
    auto &out_tensor = *dest_stripe_mem.getOperand(0).getDefiningOp();

    int32_t src_region = get_region(in_tensor);
    TensorPurpose out_tensor_purpose =
        static_cast<TensorPurpose>(get_i32_from_attr(out_tensor, "purpose"));
    int32_t dest_region = get_region(out_tensor);
    if (out_tensor_purpose == TensorPurpose::LUT) {
      dest_region = static_cast<int32_t>(BasePointerIndex::Mem2Mem);
    }

    ivec src_start_coord = get_ivec_from_attr(src_stripe_mem, "start_coord");
    ivec dest_start_coord = get_ivec_from_attr(dest_stripe_mem, "start_coord");

    int src_addr = getAddressForCoordinate(in_tensor, src_start_coord);
    int dest_addr = getAddressForCoordinate(out_tensor, dest_start_coord);

    int size = 0;
    auto compressed =
        get_ivecvec_from_attr(in_tensor, "compressed_values_substream_offsets");
    if (compressed.size()) {
      if (out_tensor_purpose == TensorPurpose::FSBias) {
        size = getStorageSize(in_tensor);
      } else {
        int stream_index =
            getCompressedStreamIndexFromCoord(in_tensor, src_start_coord);
        size = getSizeOfCompressedStream(in_tensor, stream_index);
      }
    } else {
      ivec src_end_coord = get_ivec_from_attr(src_stripe_mem, "end_coord");
      size = getAddressForCoordinate(in_tensor, src_end_coord, true) - src_addr;
    }

    set_i32_to_attr(bldr, cmd, "src_region", static_cast<int>(src_region));
    set_i32_to_attr(bldr, cmd, "src_address", static_cast<int>(src_addr));

    set_i32_to_attr(bldr, cmd, "dest_region", static_cast<int>(dest_region));
    set_i32_to_attr(bldr, cmd, "dest_address", static_cast<int>(dest_addr));

    set_i32_to_attr(bldr, cmd, "size", size);
  }
  void setup_npu_conv2d_op(mlir::OpBuilder &bldr, mlir::Operation &cmd) {
    set_i32_to_attr(bldr, cmd, "op_type",
                    static_cast<int32_t>(NpuOperationType::Conv2D));
    set_i32_to_attr(bldr, cmd, "block_traversal",
                    static_cast<int32_t>(NpuBlockTraversal::PART_KERNEL_FIRST));
    setup_common_op_fields(bldr, cmd);
    mlir::Operation &kernel = *cmd.getOperand(0).getDefiningOp();
    NpuBlockType npu_block_type =
        static_cast<NpuBlockType>(get_i32_from_attr(kernel, "block_type"));
    if (npu_block_type == NpuBlockType::VectorProduct) {
      set_i32_to_attr(bldr, cmd, "block_traversal",
                      static_cast<int32_t>(NpuBlockTraversal::DEPTH_FIRST));
    } else {
      auto wt_stripes = get_operands_from_variadic(cmd, "weight_range");
      auto &wt_stripe_mem = *wt_stripes[0];
      auto &wt_tensor = *wt_stripe_mem.getOperand(0).getDefiningOp();
      NpuBlockTraversal block_traversal = static_cast<NpuBlockTraversal>(
          get_i32_from_attr(wt_tensor, "block_traversal"));
      assert(block_traversal == NpuBlockTraversal::DEPTH_FIRST ||
             block_traversal == NpuBlockTraversal::PART_KERNEL_FIRST);
      set_i32_to_attr(bldr, cmd, "block_traversal",
                      static_cast<int32_t>(block_traversal));
    }
  }
  void setup_npu_conv_depthwise_op(mlir::OpBuilder &bldr,
                                   mlir::Operation &cmd) {
    set_i32_to_attr(bldr, cmd, "op_type",
                    static_cast<int32_t>(NpuOperationType::ConvDepthWise));
    setup_common_op_fields(bldr, cmd);
  }
  void setup_npu_pool_op(mlir::OpBuilder &bldr, mlir::Operation &cmd) {
    set_i32_to_attr(bldr, cmd, "op_type",
                    static_cast<int32_t>(NpuOperationType::Pooling));
    mlir::Operation &kernel = *cmd.getOperand(0).getDefiningOp();
    mlir::Operation &primary_op = getPrimaryOp(kernel);
    std::string op_type = get_str_from_attr(primary_op, "operator_type");

    NpuPoolingOp pool_op = NpuPoolingOp::AVERAGE;
    if (is_maxpool_op(op_type))
      pool_op = NpuPoolingOp::MAX;
    else if (is_avgpool_op(op_type))
      pool_op = NpuPoolingOp::AVERAGE;
    else if (op_type == "scheduleir.reduce_sum")
      pool_op = NpuPoolingOp::REDUCE_SUM;
    else
      assert(!"Unknown pool type");
    set_i32_to_attr(bldr, cmd, "sub_op_type", static_cast<int32_t>(pool_op));
    setup_common_op_fields(bldr, cmd);
    // Pooling specific info
    // if op.type == Op.ResizeBilinear and "rescale" in op.attrs:
    //     npu_op.rescale = op.attrs["rescale"]
  }
  void setup_npu_elementwise_op(mlir::OpBuilder &bldr, mlir::Operation &cmd) {
    set_i32_to_attr(bldr, cmd, "op_type",
                    static_cast<int32_t>(NpuOperationType::ElementWise));
    mlir::Operation &kernel = *cmd.getOperand(0).getDefiningOp();
    mlir::Operation &primary_op = getPrimaryOp(kernel);

    auto ifm_stripes = get_operands_from_variadic(cmd, "ifm_range");
    assert((ifm_stripes.size() == 1) || (ifm_stripes.size() == 2));

    mlir::Operation *ifm_stripe_mem = ifm_stripes[0];
    mlir::Operation *ifm_tensor = ifm_stripe_mem->getOperand(0).getDefiningOp();
    mlir::Operation *ifm2_stripe_mem = nullptr;
    mlir::Operation *ifm2_tensor = nullptr;
    if (ifm_stripes.size() == 2) {
      ifm2_stripe_mem = ifm_stripes[1];
      ifm2_tensor = ifm2_stripe_mem->getOperand(0).getDefiningOp();
    }

    std::string op_type = get_str_from_attr(primary_op, "operator_type");
    NpuElementWiseOp elemwise_op = get_elementwise_op(op_type);
    set_i32_to_attr(bldr, cmd, "sub_op_type",
                    static_cast<int32_t>(elemwise_op));

    bool reversed_operands = false;
    if (!is_unary_elementwise_op(elemwise_op)) {
      assert(ifm_stripes.size() == 2); // two operands
      ivec ifm_tensor_shape = get_ivec_from_attr(*ifm_tensor, "shape");
      ivec ifm2_tensor_shape = get_ivec_from_attr(*ifm2_tensor, "shape");
      ivec ifm2_start_coord =
          get_ivec_from_attr(*ifm2_stripe_mem, "start_coord");
      ivec ifm2_end_coord = get_ivec_from_attr(*ifm2_stripe_mem, "end_coord");
      Box ifm2_box(ifm2_start_coord, ifm2_end_coord);
      setup_feature_map(bldr, *ifm2_stripe_mem, ifm2_box);
      setup_ifm_or_ifm2_quantization(bldr, *ifm2_stripe_mem, kernel);
      if (ifm2_tensor_shape.empty()) { // scalar
        // assert(false && "ifm2 is scalar!");
        ivec shape_for_scalar{};
        set_ivec_to_attr(bldr, *ifm2_stripe_mem, "shape", shape_for_scalar);
      } else {
        NpuBlock ifm2_blk = ifm2_box.get_block();
        NpuBlock ifm2_shape(ifm2_tensor_shape);
        ivec shape_for_ifm2{ifm2_blk.height, ifm2_shape.width, ifm2_blk.depth};
        set_ivec_to_attr(bldr, *ifm2_stripe_mem, "shape", shape_for_ifm2);
      }

      if (!ifm_ifm2_correct_order(ifm_tensor_shape, ifm2_tensor_shape)) {
        // The scalar/broadcasted feature map has to be the ifm2 tensor so
        // switch the ifms
        std::swap(ifm_stripe_mem, ifm2_stripe_mem);
        std::swap(ifm_tensor, ifm2_tensor);
        std::swap(ifm_tensor_shape, ifm2_tensor_shape);
        reversed_operands = true;
      }
    }
    set_bool_to_attr(bldr, cmd, "reversed_operands", reversed_operands);

    setup_common_op_fields(bldr, cmd);

    auto ofm_stripes = get_operands_from_variadic(cmd, "ofm_range");
    auto &ofm_stripe_mem = *ofm_stripes[0];
    auto &ofm_tensor = *ofm_stripe_mem.getOperand(0).getDefiningOp();

    // Check if output scale needs to be overridden
    fvec output_scales = get_fvec_from_attr(ofm_tensor, "scale");
    // assert(output_scales.size() == 1);
    float output_scale = output_scales[0];
    ivec output_zero_points = get_ivec_from_attr(ofm_tensor, "zero_point");
    // assert(output_zero_points.size() == 1);
    int32_t output_zero_point = output_zero_points[0];

    // rescale (scale, shift)
    if (elemwise_op == NpuElementWiseOp::ADD) {
      // Force output scale same as the input scale for
      // resizebilinear 1x1 that is converted to add
      string resizebilinear_str =
          get_str_from_attr(primary_op, "attrs_resizebilinear");
      if (!resizebilinear_str.empty()) {
        fvec ifm2_scales = get_fvec_from_attr(*ifm2_tensor, "scale");
        output_scale = ifm2_scales[0];
      }
    }
    if (elemwise_op == NpuElementWiseOp::LRELU) {
      float alpha = get_f32_from_attr(primary_op, "attrs_alpha");
      output_scale = alpha;
    }
    if ((elemwise_op == NpuElementWiseOp::ADD) ||
        (elemwise_op == NpuElementWiseOp::SUB)) {
      string rescale_str = get_str_from_attr(primary_op, "attrs_rescale");
      if (!rescale_str.empty()) {
        // parse rescale string, and set params to ofm_stripe_mem
        assert(false);
      }
    }
    if ((elemwise_op == NpuElementWiseOp::ADD) ||
        (elemwise_op == NpuElementWiseOp::SUB) ||
        (elemwise_op == NpuElementWiseOp::MUL)) {
      int32_t act_op_idx = get_i32_from_attr(kernel, "kernel_act_op_index");
      if (act_op_idx >= 0) {
        mlir::Operation &act_op =
            getOperationByIndex(kernel.getRegion(0).front(), act_op_idx);
        std::string act_op_type = get_str_from_attr(act_op, "operator_type");
        if ((act_op_type == "sigmoid") || (act_op_type == "tanh")) {
          output_scale = 1.0 / 0x3000;
        }
      }
    }
    set_i32_to_attr(bldr, ofm_stripe_mem, "zero_point", output_zero_point);
    set_f32_to_attr(bldr, ofm_stripe_mem, "scale_f32", output_scale);
  }

public:
  HLCSConvToNpuOp() = default;

  StringRef getArgument() const final {
    // This is the argument used to refer to the pass in
    // the textual format (on the commandline for example).
    return PASS_NAME;
  }
  StringRef getDescription() const final {
    // This is a brief description of the pass.
    return "Convert HLCS command to Npu Op stream";
  }

  void runOnFunction() override {
    auto function = getFunction();
    if (function.getName() != "main")
      return;

    // printf("subpass: HLCSConvToNpuOp\n");
    // function.dump();
    MLIRContext *context = &getContext();
    OpBuilder bldr(context);
    auto name = function.getName();

    mlir::Region *region = function.getCallableRegion();
    for (mlir::Operation &region_op :
         region->getBlocks().front().getOperations()) {
      if (!llvm::isa<hlcs::NpuRegionOp>(region_op))
        continue;
      mlir::Block &region_block = region_op.getRegion(0).front();
      for (mlir::Operation &op : region_block.getOperations()) {
        if (!llvm::isa<hlcs::CommandListOp>(op))
          continue;
        mlir::Block &cascaded_block = op.getRegion(0).front();
        for (mlir::Operation &cmd : cascaded_block.getOperations()) {
          // printf(">>>: %s\n", cmd.getName().getStringRef().str().c_str());
          if (llvm::isa<hlcs::DmaCmd>(cmd)) {
            setup_dma_op(bldr, cmd);
          } else if (llvm::isa<hlcs::NpuStripeCmd>(cmd)) {
            mlir::Operation &kernel = *cmd.getOperand(0).getDefiningOp();
            NpuBlockType npu_block_type = static_cast<NpuBlockType>(
                get_i32_from_attr(kernel, "block_type"));
            if ((npu_block_type == NpuBlockType::ConvolutionMxN) ||
                (npu_block_type == NpuBlockType::VectorProduct)) {
              setup_npu_conv2d_op(bldr, cmd);
            } else if (npu_block_type == NpuBlockType::ConvolutionDepthWise) {
              setup_npu_conv_depthwise_op(bldr, cmd);
            } else if ((npu_block_type == NpuBlockType::Pooling) ||
                       (npu_block_type == NpuBlockType::ReduceSum)) {
              setup_npu_pool_op(bldr, cmd);
            } else if (npu_block_type == NpuBlockType::ElementWise) {
              setup_npu_elementwise_op(bldr, cmd);
            } else {
              assert(!"Unknown command type!");
            }
            // cmd.dump();
          }
        }
      }
    }
  }
};

} // anonymous namespace

std::unique_ptr<OperationPass<FuncOp>> CreateHLCSConvToNpuOpPass() {
  return absl::make_unique<HLCSConvToNpuOp>();
}

static PassRegistration<HLCSConvToNpuOp> pass;
} // namespace hlcs
} // namespace mlir

/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#include "absl/memory/memory.h"
#include "mlir/Dialect/StandardOps/IR/Ops.h"      // from @llvm-project
#include "mlir/IR/AffineMap.h"                    // from @llvm-project
#include "mlir/IR/Attributes.h"                   // from @llvm-project
#include "mlir/IR/BlockAndValueMapping.h"         // from @llvm-project
#include "mlir/IR/Builders.h"                     // from @llvm-project
#include "mlir/IR/Location.h"                     // from @llvm-project
#include "mlir/IR/MLIRContext.h"                  // from @llvm-project
#include "mlir/IR/Operation.h"                    // from @llvm-project
#include "mlir/IR/PatternMatch.h"                 // from @llvm-project
#include "mlir/Interfaces/InferTypeOpInterface.h" // from @llvm-project
#include "mlir/Pass/Pass.h"                       // from @llvm-project
#include "mlir/Transforms/DialectConversion.h"    // from @llvm-project

//#include "mlir/lite/ir/tfl_ops.h"
#include "src/ir/hlcs_ir.h"
#include "src/ir/llcs_ir.h"
#include "src/transforms/llcs_generator/command_emitter.h"
#include "src/transforms/llcs_generator/hlcs_mlir_utils.h"

using namespace mlir;

#define PASS_NAME "hlcs-generate-llcs"
#define DEBUG_TYPE PASS_NAME

using namespace std;

namespace mlir {

namespace hlcs {

namespace {

/*
   This pass can be made redundant, and just keep the attribute but we run
   it at the moment so we can generate a LLCS Dialect IR for debug purposes
 */

void setup_llcs_registers(mlir::Operation &hlcs_call_npu_region_op) {
  /* Replace HLCSNPURegion */
  mlir::Operation *hlcs_npu_region_op =
      hlcs_call_npu_region_op.getOperand(0).getDefiningOp();

  auto *ctx = hlcs_npu_region_op->getContext();
  OpBuilder builder(ctx);
  mlir::Location loc = builder.getUnknownLoc();
  Type type = builder.getIndexType();

  auto label_attr = hlcs_npu_region_op->getAttrOfType<StringAttr>("label");
  auto scratch_tensor_size =
      hlcs_npu_region_op->getAttrOfType<IntegerAttr>("scratch_tensor_size");
  auto scratch_fast_tensor_size =
      hlcs_npu_region_op->getAttrOfType<IntegerAttr>(
          "scratch_fast_tensor_size");

  auto input_address =
      hlcs_npu_region_op->getAttrOfType<IntegerAttr>("Input_0_Address");
  auto output_address =
      hlcs_npu_region_op->getAttrOfType<IntegerAttr>("Output_0_Address");

  builder.setInsertionPoint(hlcs_npu_region_op);
  auto llcs_npu_region_op = builder.create<llcs::NpuRegionOp>(
      loc, type, label_attr, scratch_tensor_size, scratch_fast_tensor_size,
      input_address, output_address);

  mlir::Block *llcs_npu_region_block =
      builder.createBlock(&llcs_npu_region_op.commands());
  auto cmd_stream_attr =
      hlcs_npu_region_op->getAttrOfType<ArrayAttr>("reg_stream").getValue();

  uint32_t checksum = 0;
  int count = 0;
  for (auto &cmd : cmd_stream_attr) {
    uint64_t dword = cmd.dyn_cast<IntegerAttr>().getUInt();
    uint16_t code = dword & 0x0000FFFF;
    uint16_t param = (dword & 0xFFFF0000) >> 16;
    IntegerAttr c = builder.getI16IntegerAttr(code);
    IntegerAttr p = builder.getI16IntegerAttr(param);
    CmdMode payload_mode = static_cast<CmdMode>(code & e2i(CmdMode::Mask));
    mlir::Operation *op = nullptr;
    checksum <<= 1;
    checksum += dword & 0xFFFFFFFF;
    if (payload_mode == CmdMode::NoPayload) {
      op = builder.create<llcs::Cmd0>(loc, c, p);
      count += 1;
    } else { // CmdMode::Payload32
      uint32_t payload = dword >> 32;
      IntegerAttr d = builder.getI32IntegerAttr(payload);
      op = builder.create<llcs::Cmd1>(loc, c, p, d);
      count += 2;
      checksum += payload;
    }
  }
  builder.create<llcs::YieldOp>(loc);

  hlcs_npu_region_op->replaceAllUsesWith(llcs_npu_region_op);
  hlcs_npu_region_op->erase();

  /* Replace HLCSCallNPURegion */
  builder.setInsertionPoint(&hlcs_call_npu_region_op);
  auto llcs_call_npu_region_op = builder.create<llcs::CallNpuRegionCmd>(
      loc, hlcs_call_npu_region_op.getResult(0).getType(),
      llcs_npu_region_op.getResult(), hlcs_call_npu_region_op.getOperand(1),
      hlcs_call_npu_region_op.getOperand(2));
  hlcs_call_npu_region_op.replaceAllUsesWith(llcs_call_npu_region_op);
  hlcs_call_npu_region_op.erase();
}

class HLCSGenerateLLCS : public PassWrapper<HLCSGenerateLLCS, FunctionPass> {
public:
  HLCSGenerateLLCS() = default;

  StringRef getArgument() const final {
    // This is the argument used to refer to the pass in
    // the textual format (on the commandline for example).
    return PASS_NAME;
  }
  StringRef getDescription() const final {
    // This is a brief description of the pass.
    return "Generate LLCS, including NpuRegion and CpuRegion";
  }

  void runOnFunction() override {
    auto func = getFunction();
    if (func.getName() != "main")
      return;

    // printf("##############################################\n");
    mlir::Region &region = *func.getCallableRegion();
    assert(region.getBlocks().size() == 1);
    mlir::Block &block = region.getBlocks().front();

    std::vector<mlir::Operation *> npu_regions;

    mlir::Operation *npu_region_op = nullptr;
    for (mlir::Operation &op : block.getOperations()) {
      if (llvm::isa<hlcs::CallNpuRegionCmd>(op)) {
        npu_regions.push_back(&op);
      }
    }

    for (auto npu_region_op : npu_regions)
      setup_llcs_registers(*npu_region_op);
  }
};

} // anonymous namespace

std::unique_ptr<OperationPass<FuncOp>> CreateHLCSGenerateLLCSPass() {
  return absl::make_unique<HLCSGenerateLLCS>();
}

static PassRegistration<HLCSGenerateLLCS> pass;
} // namespace hlcs
} // namespace mlir

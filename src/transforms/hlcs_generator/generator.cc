/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#include "src/transforms/hlcs_generator/generator.h"
#include "src/utils/ir_utils.h"

namespace mlir {
namespace planner {

bool create_generators_dependancies(
    std::unordered_map<Operation *, unique_ptr<Generator>> &generators) {
  for (auto &g : generators) {
    Operation *op = g.first;
    Generator *gen = g.second.get();
    std::vector<connection> prod = ExtractProducers(op);

    for (auto p : prod) {
      if (p.is_producer_block_or_operation() == operation_op) {
        if (generators.find(p.external_operation) != generators.end())
          gen->add_producer(generators[p.external_operation].get());
      }
    }
  }
}

bool all_drained(
    std::unordered_map<Operation *, unique_ptr<Generator>> &generators) {
  bool drained = true;
  for (auto &g : generators)
    drained = (drained and g.second.get()->drained());
  return drained;
}

std::vector<Generator *> generation_order(
    Operation *starting_kernel,
    std::unordered_map<Operation *, unique_ptr<Generator>> &generators) {
  std::vector<Generator *> order;
  Generator *g = generators[starting_kernel].get();
  do {
    order.push_back(g);
    if (g->get_no_producers() > 0)
      g = g->get_producers()[0];
  } while (order.size() != generators.size());
  return order;
}

int32_t needed_total_padding(int32_t input_size, int32_t stride,
                             int32_t filter_size) {
  int32_t out_size = floor(float(input_size + stride - 1) / float(stride));
  int32_t needed_input = (out_size - 1) * stride + filter_size;
  int32_t total_padding = max(0, needed_input - input_size);
  return total_padding;
}

Generator::Generator(Operation *op_kernel) {
  this->op_kernel = op_kernel;

  ArrayAttr start_coord = op_kernel->getAttrOfType<ArrayAttr>("block_config");
  for (int i = 0; i < start_coord.size(); i++)
    this->block_config.push_back(
        start_coord[i].dyn_cast<IntegerAttr>().getInt());

  SmallVector<Value, 1> inputs = kernelir::ExtractKernelInputValues(op_kernel);
  ASSERT_COND(inputs.size() != 1,
              "Generator::Generator::ExtractKernelInputValues:: Expected 1 "
              "Inputs at most ");
  this->ifm_0_shape =
      inputs[0].getType().dyn_cast<SchedTensorType>().getShape();

  SmallVector<Value, 1> outputs =
      kernelir::ExtractKernelOutputValues(op_kernel);
  ASSERT_COND(outputs.size() != 1,
              "Generator::Generator::ExtractKernelOutputValues:: Expected 1 "
              "Inputs at most ");
  this->ofm_shape = outputs[0].getType().dyn_cast<SchedTensorType>().getShape();

  SmallVector<Value, 1> weights =
      kernelir::ExtractKernelWeightValues(op_kernel);
  if (weights.size() == 0)
    this->weight_shape = {0, 0, 0, 0};
  else
    this->weight_shape =
        weights[0].getType().dyn_cast<SchedTensorType>().getShape();

  SmallVector<Value, 1> bias = kernelir::ExtractKernelBiasValues(op_kernel);
  if (bias.size() == 0)
    this->bias_shape = {0};
  else
    this->bias_shape = bias[0].getType().dyn_cast<SchedTensorType>().getShape();

  this->npu_block_type =
      (NpuBlockType)op_kernel->getAttrOfType<::mlir::IntegerAttr>("block_type")
          .getInt();

  mlir::utils::FilterMetaData filter = kernelir::getKernelFilter(op_kernel);
  mlir::utils::SliceViewMetaData slice_view =
      kernelir::getKernelSliceView(op_kernel);
  this->strides = {filter.stride.y, filter.stride.x};

  // this->skirt =
  // {filter.padding.top,filter.padding.left,filter.padding.bottom,filter.padding.right};
  int32_t y_pad = needed_total_padding(
      this->ifm_0_shape[1], filter.stride.y,
      filter.height); /* (this->ifm_0_shape[1] - this->ofm_shape[1]); */
  int32_t x_pad = needed_total_padding(
      this->ifm_0_shape[2], filter.stride.x,
      filter.width); /* (this->ifm_0_shape[2] - this->ofm_shape[2]); */
  this->skirt = {filter.padding.top, filter.padding.left,
                 y_pad - filter.padding.top, x_pad - filter.padding.left};

  this->k_height = filter.height;
  this->split_offset = {};

  // In Cascade Order
  this->is_first_in_cascade = false;
  this->is_last_in_cascade = false;

  this->next_is_fist_h_stripe = true;
  this->next_is_last_h_stripe = false;

  this->pad_top = filter.padding.top;
  this->pad_bottom = filter.padding.bottom;
  this->concat_axis = slice_view.axis;
  this->concat_offset = slice_view.offset;

  this->upscaling_factor = 1;
  this->ofm_y_step = this->block_config[0]; // Height Dimension

  // Record the current (Consider Rolling Buffer) range of data we have in the
  // intermediate after execution of this next command
  this->ofm_y_available_start = 0;
  this->ofm_y_available_end = 0;

  // Next Stripe Command OFM Range
  this->ofm_y_next_start = 0;
  this->ofm_y_next_end = ofm_y_step;
  this->ofm_y_next_start =
      std::min(this->ofm_y_next_start, (int64_t)this->ofm_shape[1]);
  this->ofm_y_next_end =
      std::min(this->ofm_y_next_end, (int64_t)this->ofm_shape[1]);
  this->ofm = BoundingBox(
      {0, this->ofm_y_next_start, 0, 0},
      {ofm_shape[0], this->ofm_y_next_end, ofm_shape[2], ofm_shape[3]});

  // Next Stripe Command IFM Range
  this->ifm_0 = transform_with_strides_and_skirt(
      this->ofm, this->strides, this->skirt, this->ifm_0_shape,
      this->npu_block_type, this->concat_axis, this->concat_offset,
      this->k_height, this->upscaling_factor, this->split_offset, this->pad_top,
      this->pad_bottom);

  this->ifm_y_next_start = this->ifm_0.get_start_coord()[1];
  this->ifm_y_next_end = this->ifm_0.get_end_coord()[1];

  this->ofm.clamp(this->ofm_shape);
  this->ifm_0.clamp(this->ifm_0_shape);
}

bool Generator::next_command_available() {
  if (producers.size() == 0)
    return false;

  Generator *gen_prod = this->producers[0];
  int64_t prod_ofm_y_available_start = gen_prod->ofm_y_available_start;
  int64_t prod_ofm_y_available_end = gen_prod->ofm_y_available_end;

  if ((prod_ofm_y_available_end >= this->ifm_y_next_end))
    return true;
  return false;
}

bool Generator::drained() {
  if (this->ofm_y_next_start >= this->ofm_shape[1])
    return true;
  else
    return false;
}

stripe_command Generator::get_next_command() {
  stripe_command s;

  s.op = this->op_kernel;

  s.ofm = this->ofm;
  s.ifm.push_back(this->ifm_0);
  s.weight = BoundingBox({0, 0, 0, 0}, {weight_shape[0], weight_shape[1],
                                        weight_shape[2], weight_shape[3]});
  s.scale = BoundingBox({0}, {bias_shape[0]});

  s.block_config = this->block_config;

  s.is_first_in_cascade = this->is_first_in_cascade;
  s.is_last_in_cascade = this->is_last_in_cascade;

  s.concat_axis = this->concat_axis;
  s.concat_offset = this->concat_offset;
  s.pad_top = this->pad_top;
  s.pad_bottom = this->pad_bottom;

  // Configured the stripe command now lets prepare the next (We prepared the
  // first in the constructor)...

  // Record the current (Consider Rolling Buffer) range of data we have in the
  // intermediate after execution of this next command
  this->ofm_y_available_start = this->ofm_y_next_start;
  this->ofm_y_available_end = this->ofm_y_next_end;

  // Next Stripe Command OFM Range
  this->ofm_y_next_start += this->ofm_y_step;
  this->ofm_y_next_end += this->ofm_y_step;
  this->ofm_y_next_start =
      std::min(this->ofm_y_next_start, (int64_t)this->ofm_shape[1]);
  this->ofm_y_next_end =
      std::min(this->ofm_y_next_end, (int64_t)this->ofm_shape[1]);

  s.is_fist_h_stripe = this->next_is_fist_h_stripe;
  s.is_last_h_stripe = drained();

  this->ofm = BoundingBox(
      {0, this->ofm_y_next_start, 0, 0},
      {ofm_shape[0], this->ofm_y_next_end, ofm_shape[2], ofm_shape[3]});

  // Next Stripe Command IFM Range
  this->ifm_0 = transform_with_strides_and_skirt(
      this->ofm, this->strides, this->skirt, this->ifm_0_shape,
      this->npu_block_type, this->concat_axis, this->concat_offset,
      this->k_height, this->upscaling_factor, this->split_offset, this->pad_top,
      this->pad_bottom);

  this->ifm_y_next_start = this->ifm_0.get_start_coord()[1];
  this->ifm_y_next_end = this->ifm_0.get_end_coord()[1];

  this->next_is_fist_h_stripe = false;
  this->next_is_last_h_stripe = drained();

  /*
  std::cout << "drained: " << drained() << " " << this->ofm_y_next_start << "/"
  << this->ofm_shape[1] << " " <<  std::endl; std::cout << "gen_next_command" <<
  op_kernel
                                  << " ofm_y_step: "  << this->ofm_y_step
                                  << " ofm_y_start: " << this->ofm_y_next_start
                                  << " ofm_y_end: "   << this->ofm_y_next_end
                                  << " ifm_y_start: " << this->ifm_y_next_start
                                  << " ifm_y_end: "   << this->ifm_y_next_end
                                  << std::endl;
  */

  this->ofm.clamp(this->ofm_shape);
  this->ifm_0.clamp(this->ifm_0_shape);

  return s;
}

BoundingBox transform_with_strides_and_skirt(
    BoundingBox i_box, llvm::SmallVector<int64_t, 4> strides,
    llvm::SmallVector<int64_t, 4> skirt, ArrayRef<int64_t> ifm_shape,
    NpuBlockType npu_block_type, int64_t concat_axis, int64_t concat_offset,
    int64_t k_height, int64_t upscaling_factor,
    llvm::SmallVector<int64_t, 4> split_offset, int64_t &pad_top,
    int64_t &pad_bottom) {
  ASSERT_COND(strides.size() != 2,
              "transform_with_strides_and_skirt::Incorrect Stride Size");
  ASSERT_COND(skirt.size() != 4,
              "transform_with_strides_and_skirt::Incorrect Skirt Size");
  ASSERT_COND(ifm_shape.size() != 4,
              "transform_with_strides_and_skirt::Incorrect Ifm Shape Size");

  coordinate new_start_coord = i_box.get_start_coord();
  coordinate new_end_coord = i_box.get_end_coord();

  new_start_coord[concat_axis] -= concat_offset;
  new_end_coord[concat_axis] -= concat_offset;

  pad_top = 0;
  pad_bottom = 0;

  if (split_offset.size() > 0) {
    for (int idx = 0; idx < split_offset.size(); idx++) {
      new_start_coord[idx] += split_offset[idx];
      new_end_coord[idx] += split_offset[idx];
    }
  }

  if ((split_offset.size() != 0) and
      ((npu_block_type == ConvolutionMxN) or
       (npu_block_type == VectorProduct) or (npu_block_type == ReduceSum))) {
    // These types of operations do a "dot product" or sum over the entire IFM
    new_start_coord[new_start_coord.size() - 1] = 0;
    new_end_coord[new_end_coord.size() - 1] = ifm_shape[ifm_shape.size() - 1];
  }

  coordinate original_end_coord;
  if (npu_block_type == ConvolutionMxN) {
    // Copy over the Depth of the IFM
    new_end_coord[new_end_coord.size() - 1] = ifm_shape[ifm_shape.size() - 1];
  }
  if ((npu_block_type == ElementWise) and
      (min(new_end_coord.size(), ifm_shape.size()) >= 1)) {
    new_end_coord[new_end_coord.size() - 1] =
        min(new_end_coord[new_end_coord.size() - 1],
            ifm_shape[ifm_shape.size() - 1]);
  }
  if (min(new_end_coord.size(), ifm_shape.size()) >= 2) {
    new_end_coord[new_end_coord.size() - 2] =
        min(new_end_coord[new_end_coord.size() - 2],
            ifm_shape[ifm_shape.size() - 2] * upscaling_factor);
  }
  if (min(new_end_coord.size(), ifm_shape.size()) >= 3) {
    original_end_coord = new_end_coord;
    new_end_coord[new_end_coord.size() - 3] =
        min(new_end_coord[new_end_coord.size() - 3],
            ifm_shape[ifm_shape.size() - 3] * upscaling_factor);
  }

  if ((strides.size() != 0) and (skirt.size() != 0)) {
    if (new_start_coord.size() >= 2) {
      int64_t stride = strides[1];
      new_start_coord[new_start_coord.size() - 2] =
          max(new_start_coord[new_start_coord.size() - 2] * stride - skirt[1],
              int64_t(0));
      new_end_coord[new_end_coord.size() - 2] =
          min(new_end_coord[new_end_coord.size() - 2] * stride + skirt[3],
              ifm_shape[ifm_shape.size() - 2]);
    }

    if (new_start_coord.size() >= 3) {
      int64_t stride = strides[0];
      int64_t skirt_top_remainder = (skirt[0] % upscaling_factor);
      int64_t total_stride =
          stride * (new_end_coord[new_end_coord.size() - 3] -
                    new_start_coord[new_start_coord.size() - 3] - 1);

      new_start_coord[new_start_coord.size() - 3] =
          new_start_coord[new_end_coord.size() - 3] * stride - skirt[0] +
          skirt_top_remainder;

      pad_top =
          max(int64_t(0), 0 - new_start_coord[new_start_coord.size() - 3]) +
          skirt_top_remainder;
      new_start_coord[new_start_coord.size() - 3] =
          max(new_start_coord[new_start_coord.size() - 3], int64_t(0));

      int64_t count = 0;
      while (ifm_shape.size() < 3)
        count++;
      std::vector<int64_t> temp = create_vector<int64_t>(1, count);
      std::vector<int64_t> ifm_shape_normalized;
      for (int i = 0; i < temp.size(); i++)
        ifm_shape_normalized.push_back(temp[i]);
      for (int i = 0; i < ifm_shape.size(); i++)
        ifm_shape_normalized.push_back(ifm_shape[i]);

      if ((new_end_coord[new_end_coord.size() - 3] * stride + skirt[2]) >
          (ifm_shape_normalized[ifm_shape_normalized.size() - 3] *
           upscaling_factor)) {
        // pad_bottom is calculated based the diff between the end position of
        // the weight kernel, after last stride and the ifm height.
        if ((upscaling_factor != 1 and
             original_end_coord[original_end_coord.size() - 3]) >
            (ifm_shape_normalized[ifm_shape_normalized.size() - 3] *
             upscaling_factor)) {
          // Special case for Transpose Convolution with VALID padding.
          pad_bottom = original_end_coord[original_end_coord.size() - 3] -
                       (ifm_shape_normalized[ifm_shape_normalized.size() - 3] *
                        upscaling_factor);
        } else {
          int64_t k_start =
              new_start_coord[new_start_coord.size() - 3] - pad_top;
          pad_bottom =
              max(int64_t(0),
                  k_start + total_stride + k_height -
                      (ifm_shape_normalized[ifm_shape_normalized.size() - 3] *
                       upscaling_factor));
        }
      }
      // Adjust for upscaling
      new_start_coord[new_start_coord.size() - 3] =
          max(new_start_coord[new_start_coord.size() - 3] / upscaling_factor,
              int64_t(0));
      new_end_coord[new_end_coord.size() - 3] =
          new_end_coord[new_end_coord.size() - 3] * stride + skirt[2] +
          (skirt[2] % upscaling_factor);
      new_end_coord[new_end_coord.size() - 3] =
          min(new_end_coord[new_end_coord.size() - 3] / upscaling_factor,
              ifm_shape_normalized[ifm_shape_normalized.size() - 3]);
    }
  }

  /* Currently these do not need to Change so hardcoding until we move over to
   * the Affine Transform */
  new_end_coord[0] = ifm_shape[0]; // Batch
  new_end_coord[2] = ifm_shape[2]; // Width

  return BoundingBox(new_start_coord, new_end_coord);
}

int64_t get_ofm_y_range_for_pass(BoundingBox i_box) {
  return (i_box.get_start_coord()[1], i_box.get_end_coord()[1]);
}

std::vector<int64_t> get_size_shape(BoundingBox i_box) {
  std::vector<int64_t> shape;
  for (int idx = 0; idx < i_box.get_start_coord().size(); idx++)
    shape.push_back(i_box.get_end_coord()[idx] - i_box.get_start_coord()[idx]);
  return shape;
}

int64_t get_size(BoundingBox i_box) {
  std::vector<int64_t> shape = get_size_shape(i_box);
  int64_t size = 1;
  for (int idx = 0; idx < shape.size(); idx++)
    size = (size * shape[idx]);
  return size;
}

BoundingBox make_weight_box(ArrayRef<int64_t> weight_shape, bool is_depthwise,
                            int64_t oc_range_start, int64_t oc_range_end,
                            bool weights_transposed) {
  coordinate start;
  initialize_coordinate(start, 0, weight_shape.size());

  coordinate end;
  load_coordinate(end, weight_shape);

  if (is_depthwise) {
    // HWOI
    if (weights_transposed) {
      FATAL_ERROR(
          "make_weight_box::Running with Weight Transpossed not verified");
    } else {
      // input range is output range divided by channel multiplier
      start[start.size() - 1] =
          (oc_range_start / weight_shape[weight_shape.size() - 2]);
      end[end.size() - 1] =
          (oc_range_end / weight_shape[weight_shape.size() - 2]);
    }
  } else {
    // HWIO
    start[start.size() - 1] = oc_range_start;
    end[end.size() - 1] = oc_range_end;
  }
  return BoundingBox(start, end);
}

} // namespace planner
} // namespace mlir

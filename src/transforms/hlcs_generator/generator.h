/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#ifndef GENERATOR_H
#define GENERATOR_H

#include "mlir/Dialect/Quant/QuantOps.h"
#include "mlir/Dialect/Quant/QuantTypes.h"
#include "mlir/Dialect/StandardOps/IR/Ops.h"      // from @llvm-project
#include "mlir/IR/AffineMap.h"                    // from @llvm-project
#include "mlir/IR/Attributes.h"                   // from @llvm-project
#include "mlir/IR/BlockAndValueMapping.h"         // from @llvm-project
#include "mlir/IR/Builders.h"                     // from @llvm-project
#include "mlir/IR/Location.h"                     // from @llvm-project
#include "mlir/IR/MLIRContext.h"                  // from @llvm-project
#include "mlir/IR/Operation.h"                    // from @llvm-project
#include "mlir/IR/PatternMatch.h"                 // from @llvm-project
#include "mlir/Interfaces/InferTypeOpInterface.h" // from @llvm-project
#include "mlir/Pass/Pass.h"                       // from @llvm-project
#include "mlir/Transforms/DialectConversion.h"    // from @llvm-project

#include "src/transforms/hlcs_generator/hlcs_generator.h"
#include "src/transforms/hlcs_generator/stripe_command.h"

#include <unordered_map>

using namespace std;

namespace mlir {
namespace planner {

class Generator {
public:
  Generator(){};
  Generator(Operation *kernel);
  bool drained();
  bool next_command_available();
  stripe_command get_next_command();

  int64_t get_no_producers() { return producers.size(); };
  void add_producer(Generator *g) { producers.push_back(g); }
  std::vector<Generator *> get_producers() { return producers; };

  Operation *get_op_kernel() { return op_kernel; }

  int64_t get_ofm_y_available_start() { return ofm_y_available_start; };
  int64_t get_ofm_y_available_end() { return ofm_y_available_end; };

  void set_first() { is_first_in_cascade = true; }
  void set_last() { is_last_in_cascade = true; }

private:
  int64_t ofm_y_available_start;
  int64_t ofm_y_available_end;

  int64_t ifm_y_next_start;
  int64_t ifm_y_next_end;
  int64_t ofm_y_next_start;
  int64_t ofm_y_next_end;

  int64_t ofm_y_step;

  llvm::ArrayRef<int64_t> ifm_0_shape;
  llvm::ArrayRef<int64_t> ifm_1_shape;
  llvm::ArrayRef<int64_t> ofm_shape;
  llvm::ArrayRef<int64_t> weight_shape;
  llvm::ArrayRef<int64_t> bias_shape;

  NpuBlockConfig block_config;
  NpuBlockType npu_block_type;
  llvm::SmallVector<int64_t, 4> strides;
  llvm::SmallVector<int64_t, 4> skirt;
  llvm::SmallVector<int64_t, 4> split_offset;
  int64_t concat_axis;
  int64_t concat_offset;
  int64_t k_height;
  int64_t upscaling_factor;

  bool is_first_in_cascade;
  bool is_last_in_cascade;

  bool next_is_fist_h_stripe;
  bool next_is_last_h_stripe;

  int32_t next_concat_axis;
  int32_t next_concat_offset;

  Operation *op_kernel;

  BoundingBox ifm_0;
  BoundingBox ofm;

  int64_t pad_top;
  int64_t pad_bottom;

  std::vector<Generator *> producers;
};

bool all_drained(
    std::unordered_map<Operation *, unique_ptr<Generator>> &generators);
bool create_generators_dependancies(
    std::unordered_map<Operation *, unique_ptr<Generator>> &generators);
std::vector<Generator *> generation_order(
    Operation *starting_kernel,
    std::unordered_map<Operation *, unique_ptr<Generator>> &generators);

BoundingBox transform_with_strides_and_skirt(
    BoundingBox i_box, llvm::SmallVector<int64_t, 4> strides,
    llvm::SmallVector<int64_t, 4> skirt, ArrayRef<int64_t> ifm_shape,
    NpuBlockType npu_block_type, int64_t concat_axis, int64_t concat_offset,
    int64_t k_height, int64_t upscaling_factor,
    llvm::SmallVector<int64_t, 4> split_offset, int64_t &pad_top,
    int64_t &pad_bottom);
int64_t get_ofm_y_range_for_pass(BoundingBox i_box);
std::vector<int64_t> get_size_shape(BoundingBox i_box);
int64_t get_size(BoundingBox i_box);
BoundingBox make_weight_box(ArrayRef<int64_t> weight_shape, bool is_depthwise,
                            int64_t oc_range_start, int64_t oc_range_end,
                            bool weights_transposed = false);

} // namespace planner
} // namespace mlir

#endif // GENERATOR_H

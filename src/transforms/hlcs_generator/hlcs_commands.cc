/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#include "src/transforms/hlcs_generator/hlcs_generator.h"

namespace mlir {
namespace planner {

void hlcs_generator::createOperationInfo(
    Operation *op_Operation, Block *block_l,
    llvm::iplist<Operation>::iterator insertionPoint) {
  Type type = builder->getIndexType();

  mlir::Location loc = builder->getUnknownLoc();

  stringstream ss;
  ss << "Operator_" << operator_count++;
  ::mlir::StringAttr operator_name =
      builder->getStringAttr(StringRef(ss.str().c_str()));
  ::mlir::StringAttr operator_type = builder->getStringAttr(
      StringRef(op_Operation->getName().getStringRef().str().c_str()));

  FilterMetaData filter = getScheduleIRFilter(op_Operation);

  ::mlir::ArrayAttr dilation =
      builder->getI32ArrayAttr({filter.dilation.y, filter.dilation.x});
  ::mlir::ArrayAttr stride =
      builder->getI32ArrayAttr({filter.stride.y, filter.stride.x});
  ::mlir::ArrayAttr kernelSize =
      builder->getI32ArrayAttr({filter.height, filter.width});

  // TOSA: Top, Bottom, Left, Right
  // TOSA: 0  , 1 , 0,  1
  // HLCS: Top, Left, Bottom, Right (This should be aligned !!!!)
  // HLCS: 0, 0, 1, 1
  // std::cout << "TOSA: " << filter.padding.top << filter.padding.bottom <<
  // filter.padding.left << filter.padding.right << std::endl; std::cout <<
  // "HLCS: " << filter.padding.top << filter.padding.left <<
  // filter.padding.bottom << filter.padding.right << std::endl;
  ::mlir::ArrayAttr padding =
      builder->getI32ArrayAttr({filter.padding.top, filter.padding.left,
                                filter.padding.bottom, filter.padding.right});

  builder->setInsertionPoint(block_l, block_l->end());
  builder->create<hlcs::OperatorInfoOp>(loc, type, operator_name, operator_type,
                                        dilation, stride, kernelSize, padding);
}

void hlcs_generator::createHLCSYield(
    Block *block_l, llvm::iplist<Operation>::iterator insertionPoint) {
  mlir::Location loc = builder->getUnknownLoc();
  builder->setInsertionPoint(block_l, block_l->end());
  builder->create<hlcs::YieldOp>(loc);
}

mlir::Operation *hlcs_generator::createKernelInfo(
    Operation *op_kernel, Block *block_l,
    llvm::iplist<Operation>::iterator insertionPoint) {
  ASSERT_COND(
      not(llvm::isa<kernelir::KernelRegionOp>(op_kernel)),
      "hlcs_generator::createKernelInfo can only be called on Kernel Ops");

  mlir::Location loc = builder->getUnknownLoc();

  Region &kernel_region = op_kernel->getRegion(0);
  Block &kernel_block = kernel_region.front();

  // Add the Kernel
  Type type_kernel = builder->getIndexType();

  stringstream ss;
  ss << "Kernel_" << kernel_count++;

  ::mlir::StringAttr kernel_name =
      builder->getStringAttr(StringRef(ss.str().c_str()));
  ::mlir::IntegerAttr kernel_primary_op_index =
      builder->getI32IntegerAttr(-1); // Index in Basic Block of primary
                                      // Operation OperationInfo; Upated Later
  ::mlir::IntegerAttr kernel_act_op_index =
      builder->getI32IntegerAttr(-1); // Index in Basic Block of primary
                                      // Operation OperationInfo; Upated Later

  ::mlir::ArrayAttr shared_buffer_banks_locations =
      op_kernel->getAttrOfType<::mlir::ArrayAttr>(
          "shared_buffer_banks_locations");
  ::mlir::ArrayAttr shared_buffer_banks_required =
      op_kernel->getAttrOfType<::mlir::ArrayAttr>(
          "shared_buffer_banks_required");
  ::mlir::IntegerAttr shared_buffer_use_accumulator_element =
      op_kernel->getAttrOfType<::mlir::IntegerAttr>(
          "shared_buffer_use_accumulator_element");
  ::mlir::IntegerAttr shared_buffer_ifm_count =
      op_kernel->getAttrOfType<::mlir::IntegerAttr>("shared_buffer_ifm_count");

  ::mlir::ArrayAttr block_config =
      op_kernel->getAttrOfType<::mlir::ArrayAttr>("block_config");
  ::mlir::IntegerAttr block_type =
      op_kernel->getAttrOfType<::mlir::IntegerAttr>("block_type");

  ::mlir::StringAttr frontend_rescale_mode =
      op_kernel->getAttrOfType<::mlir::StringAttr>("frontend_rescale_mode");

  ::mlir::IntegerAttr input_zp =
      op_kernel->getAttrOfType<::mlir::IntegerAttr>("input_zp");
  ::mlir::IntegerAttr weight_zp =
      op_kernel->getAttrOfType<::mlir::IntegerAttr>("weight_zp");

  ::mlir::IntegerAttr input0_zp =
      op_kernel->getAttrOfType<::mlir::IntegerAttr>("input0_zp");
  ::mlir::IntegerAttr input0_multiplier =
      op_kernel->getAttrOfType<::mlir::IntegerAttr>("input0_multiplier");
  ::mlir::IntegerAttr input0_shift =
      op_kernel->getAttrOfType<::mlir::IntegerAttr>("input0_shift");
  ::mlir::IntegerAttr input1_zp =
      op_kernel->getAttrOfType<::mlir::IntegerAttr>("input1_zp");
  ::mlir::IntegerAttr input1_multiplier =
      op_kernel->getAttrOfType<::mlir::IntegerAttr>("input1_multiplier");
  ::mlir::IntegerAttr input1_shift =
      op_kernel->getAttrOfType<::mlir::IntegerAttr>("input1_shift");
  ::mlir::IntegerAttr output_zp =
      op_kernel->getAttrOfType<::mlir::IntegerAttr>("output_zp");
  ::mlir::ArrayAttr output_multiplier =
      op_kernel->getAttrOfType<::mlir::ArrayAttr>("output_multiplier");
  ::mlir::ArrayAttr output_shift =
      op_kernel->getAttrOfType<::mlir::ArrayAttr>("output_shift");

  ::mlir::BoolAttr scale32 =
      op_kernel->getAttrOfType<::mlir::BoolAttr>("scale32");
  ::mlir::BoolAttr double_round =
      op_kernel->getAttrOfType<::mlir::BoolAttr>("double_round");
  ::mlir::BoolAttr per_channel =
      op_kernel->getAttrOfType<::mlir::BoolAttr>("per_channel");

  ::mlir::IntegerAttr act_min =
      op_kernel->getAttrOfType<::mlir::IntegerAttr>("act_min");
  ::mlir::IntegerAttr act_max =
      op_kernel->getAttrOfType<::mlir::IntegerAttr>("act_max");

  builder->setInsertionPoint(block_l, block_l->end());
  auto op_kernel_info = builder->create<hlcs::KernelInfoOp>(
      loc, type_kernel, kernel_name, kernel_primary_op_index,
      kernel_act_op_index, shared_buffer_banks_locations,
      shared_buffer_banks_required, shared_buffer_use_accumulator_element,
      shared_buffer_ifm_count, block_config, block_type, frontend_rescale_mode,
      input_zp, weight_zp, input0_zp, input0_multiplier, input0_shift,
      input1_zp, input1_multiplier, input1_shift, output_zp, output_multiplier,
      output_shift, scale32, double_round, per_channel, act_min, act_max);

  Region &op_kernel_info_region = op_kernel_info.getRegion();
  Block *op_kernel_info_block = builder->createBlock(
      &op_kernel_info_region, op_kernel_info_region.begin());

  int32_t index = -1;

  // Add all the Ops
  Operation *primary_op = mlir::kernelir::getKernelMajorOp(op_kernel);
  int32_t primary_op_index = -1;

  SmallVector<Operation *, 1> act_ops =
      mlir::kernelir::getKernelActivationsOps(op_kernel);
  int32_t act_op_index = -1;

  llvm::iplist<Operation> &kernel_operations = kernel_block.getOperations();
  for (auto &op : kernel_operations) {
    index++;
    if (primary_op == &op)
      primary_op_index = index;
    if (act_ops.size() > 0) {
      if (act_ops[0] == &op)
        act_op_index = index;
    }
    createOperationInfo(&op, op_kernel_info_block, op_kernel_info_block->end());
  }
  createHLCSYield(op_kernel_info_block, op_kernel_info_block->end());

  Operation *op_kernel_info_base_op = op_kernel_info;
  op_kernel_info_base_op->setAttr("kernel_primary_op_index",
                                  builder->getI32IntegerAttr(primary_op_index));
  op_kernel_info_base_op->setAttr("kernel_act_op_index",
                                  builder->getI32IntegerAttr(act_op_index));

  /* ToDo: Not officially part of the dialect */
  auto fused_quantize =
      op_kernel->getAttrOfType<mlir::BoolAttr>("fused_quantize");
  auto quant_present =
      op_kernel->getAttrOfType<mlir::BoolAttr>("quant_present");
  // std::cout << "fused_quantize: " << fused_quantize.getValue() << "
  // quant_present: " << quant_present.getValue() << std::endl;
  op_kernel_info_base_op->setAttr("fused_quantize", fused_quantize);
  op_kernel_info_base_op->setAttr("quant_present", quant_present);

  return &(block_l->back());
}

SmallVector<int32_t, 4> convert_to_ivec(ArrayRef<int64_t> &&vec) {
  SmallVector<int32_t, 4> ret;
  for (auto v : vec)
    ret.push_back(v);
  return ret;
}

mlir::Operation *hlcs_generator::createTensorInfo(
    mlir::Value tensor, Block *block_l,
    llvm::iplist<Operation>::iterator insertionPoint, uint32_t id) {
  mlir::Location loc = builder->getUnknownLoc();
  Type type = builder->getIndexType();

  mlir::Type t0 = tensor.getType();
  mlir::SchedTensorType t = tensor.getType().dyn_cast<mlir::SchedTensorType>();
  ASSERT_COND(
      checkTensorType<mlir::SchedTensorType>(tensor.getType()) == false,
      "hlcs_generator::createTensorInfo can only be run on a SchedtensorType");

  stringstream ss_equivalence_id;
  ss_equivalence_id << id; // hash_value(tensor);

  mlir::StringAttr tensor_id =
      builder->getStringAttr(StringRef(ss_equivalence_id.str().c_str()));
  mlir::ArrayAttr tensor_shape =
      builder->getI32ArrayAttr(convert_to_ivec(t.getShape()));
  mlir::ArrayAttr tensor_storage_shape =
      builder->getI32ArrayAttr(convert_to_ivec(t.getStorageShape()));

  mlir::StringAttr tensor_dtype = builder->getStringAttr(
      StringRef(datatype_to_str(getDataType(t)).c_str()));

  std::vector<double> scalePoints = getScale(t0);
  SmallVector<mlir::Attribute, 1> scaleAttr;
  for (auto s : scalePoints) {
    auto zO = builder->getF64FloatAttr(s);
    scaleAttr.push_back(zO);
  }
  mlir::ArrayAttr scale = builder->getArrayAttr(scaleAttr);

  std::vector<int64_t> zeroPoints = getZeroPoint(t0);
  SmallVector<mlir::Attribute, 1> zeroPointsAttr;
  for (auto z : zeroPoints) {
    auto zO = builder->getI32IntegerAttr(z);
    zeroPointsAttr.push_back(zO);
  }
  mlir::ArrayAttr zero_point = builder->getArrayAttr(zeroPointsAttr);

  mlir::BoolAttr symmetric = builder->getBoolAttr(getIsSymmetric(t0));
  mlir::IntegerAttr tensor_element_size_bytes =
      builder->getI32IntegerAttr(getNoBytesType(t)); //

  mlir::IntegerAttr tensor_mem_area =
      builder->getI32IntegerAttr(t.get_mem_area());
  mlir::IntegerAttr tensor_mem_type =
      builder->getI32IntegerAttr(t.get_mem_type());
  mlir::IntegerAttr tensor_format = builder->getI32IntegerAttr(t.get_format());
  mlir::IntegerAttr tensor_purpose =
      builder->getI32IntegerAttr(t.get_purpose());
  mlir::IntegerAttr tensor_sub_purpose =
      builder->getI32IntegerAttr(t.get_sub_purpose());
  mlir::IntegerAttr tensor_alignment =
      builder->getI32IntegerAttr(t.get_alignment());
  mlir::IntegerAttr tensor_block_traversal =
      builder->getI32IntegerAttr(t.get_NpuBlockTraversal());
  mlir::IntegerAttr tensor_address =
      builder->getI32IntegerAttr(t.get_address());

  mlir::ArrayAttr tensor_brick_size =
      builder->getI32ArrayAttr(t.get_brick_size());

  mlir::IntegerAttr tensor_resampling_mode =
      builder->getI32IntegerAttr(t.get_resampling_mode());
  stringstream ss_unique_id;
  ss_unique_id << "TID_" << id;
  StringAttr tensor_label =
      builder->getStringAttr(StringRef(ss_unique_id.str().c_str()));

  Operation *op = tensor.getDefiningOp();
  mlir::BoolAttr tensor_dma_op_result;
  mlir::BoolAttr broadcast_scalar_value_valid = builder->getBoolAttr(false);
  mlir::IntegerAttr broadcast_scalar_value = builder->getI32IntegerAttr(0);
  if (op) {
    tensor_dma_op_result =
        builder->getBoolAttr(llvm::isa<scheduleir::DmaOp>(op));
    if (llvm::isa<scheduleir::ConstOp>(*op)) {
      RawDataAttr val_attr = op->getAttr("value").dyn_cast<RawDataAttr>();
      if (val_attr) {
        // only support 8-bit scalar broadcast now
        if (val_attr.getSize() == 1 && val_attr.getElementSize() == 1) {
          int8_t *val = (int8_t *)val_attr.getData();
          broadcast_scalar_value_valid = builder->getBoolAttr(true);
          broadcast_scalar_value = builder->getI32IntegerAttr(val[0]);
        }
      }
    }
  } else
    tensor_dma_op_result = builder->getBoolAttr(false);

  mlir::FloatAttr tensor_storage_compression_scale =
      builder->getF32FloatAttr(t.get_storage_compression_scale());

  mlir::ArrayAttr tensor_weight_compressed_offsets =
      builder->getI32ArrayAttr(t.get_weight_compressed_offsets());

  std::vector<std::vector<int32_t>> comp_vs =
      t.get_compressed_values_substream_offsets();
  std::vector<mlir::Attribute> arrays;
  for (auto &v : comp_vs) {
    ArrayAttr array = builder->getI32ArrayAttr(v);
    arrays.push_back(array);
  }
  ArrayAttr tensor_compressed_values_substream_offsets =
      builder->getArrayAttr(arrays);

  builder->setInsertionPoint(block_l, insertionPoint);
  auto op_tensor_info = builder->create<hlcs::TensorInfoOp>(
      loc, type, tensor_id, tensor_shape, tensor_storage_shape, tensor_dtype,
      scale, zero_point, symmetric, tensor_element_size_bytes, tensor_mem_area,
      tensor_mem_type, tensor_format, tensor_purpose, tensor_sub_purpose,
      tensor_alignment, tensor_block_traversal, tensor_address,
      tensor_brick_size, tensor_resampling_mode, tensor_label,
      tensor_dma_op_result, broadcast_scalar_value_valid,
      broadcast_scalar_value, tensor_storage_compression_scale,
      tensor_weight_compressed_offsets,
      tensor_compressed_values_substream_offsets);

  llvm::iplist<Operation> &operations = block_l->getOperations();
  for (auto &i : operations)
    if (i.getAttr("id"))
      if (i.getAttrOfType<mlir::StringAttr>("id").getValue().str() ==
          ss_equivalence_id.str())
        return &i;
  ASSERT_COND(
      true,
      " hlcs_generator::createTensorInfo():Something has gone terribly wrong");
}

Operation *hlcs_generator::createStripeMemCmd(Block *block_l,
                                              Operation *tensorInfo,
                                              coordinate startCoord,
                                              coordinate endCoord) {
  mlir::Location loc = builder->getUnknownLoc();
  Type type = builder->getIndexType();

  mlir::ArrayAttr start_coord = builder->getI64ArrayAttr(startCoord);
  mlir::ArrayAttr end_coord = builder->getI64ArrayAttr(endCoord);

  builder->setInsertionPoint(block_l, block_l->end());
  auto op_stripe_cmd = builder->create<hlcs::StripeMemCmd>(
      loc, type, tensorInfo->getResult(0), start_coord, end_coord);

  return &(block_l->back());
}

Operation *hlcs_generator::createDmaCmd(Block *block_l, Operation *srcMemRefCmd,
                                        Operation *dstMemRefCmd,
                                        Operation *kernelInfo) {
  mlir::Location loc = builder->getUnknownLoc();
  Type type = builder->getIndexType();

  mlir::Value srcMemRefResult = srcMemRefCmd->getResult(0);
  mlir::Value dstMemRefResult = dstMemRefCmd->getResult(0);
  mlir::Value kernelResult = kernelInfo->getResult(0);

  builder->setInsertionPoint(block_l, block_l->end());
  auto op_dma_cmd = builder->create<hlcs::DmaCmd>(
      loc, srcMemRefResult, dstMemRefResult, kernelResult);

  return &(block_l->back());
}

Operation *hlcs_generator::createStripeCmd(
    Block *block_l, std::vector<Operation *> ifmMemRefCmds,
    Operation *weightMemRefCmd, Operation *ofmMemRefCmd,
    Operation *scaleMemRefCmd, NpuBlockConfig block_config_i, bool is_first_i,
    bool is_last_i, bool is_fist_h_stripe_i, bool is_last_h_stripe_i,
    int32_t concat_axis_i, int32_t concat_offset_i, int32_t pad_top_i,
    int32_t pad_bottom_i, Operation *kernelInfo) {
  mlir::Location loc = builder->getUnknownLoc();
  Type type = builder->getIndexType();

  mlir::Value kernelResult = kernelInfo->getResult(0);

  SmallVector<mlir::Value, TYPICAL_NO_INPUTS> srcMemRef;
  for (Operation *op : ifmMemRefCmds)
    srcMemRef.push_back(op->getResult(0));
  srcMemRef.push_back(ofmMemRefCmd->getResult(0));

  int32_t start = 0;
  int32_t end = ifmMemRefCmds.size();
  auto sI = builder->getI32IntegerAttr(start);
  auto eI = builder->getI32IntegerAttr(end);
  mlir::ArrayAttr ifmMemRefRange = builder->getArrayAttr({sI, eI});

  start = end;
  end = start + 1;
  auto sO = builder->getI32IntegerAttr(start);
  auto eO = builder->getI32IntegerAttr(end);
  mlir::ArrayAttr ofmMemRefRange = builder->getArrayAttr({sO, eO});

  mlir::ArrayAttr block_config = builder->getI32ArrayAttr(block_config_i);

  mlir::BoolAttr is_first = builder->getBoolAttr(is_first_i);
  mlir::BoolAttr is_last = builder->getBoolAttr(is_last_i);
  mlir::BoolAttr is_first_h_stripe = builder->getBoolAttr(is_fist_h_stripe_i);
  mlir::BoolAttr is_last_h_stripe = builder->getBoolAttr(is_last_h_stripe_i);
  mlir::IntegerAttr concat_axis = builder->getI32IntegerAttr(concat_axis_i);
  mlir::IntegerAttr concat_offset = builder->getI32IntegerAttr(concat_offset_i);
  mlir::IntegerAttr pad_top = builder->getI32IntegerAttr(pad_top_i);
  mlir::IntegerAttr pad_bottom = builder->getI32IntegerAttr(pad_bottom_i);

  if ((weightMemRefCmd == nullptr) and (scaleMemRefCmd == nullptr)) {
    auto sW = builder->getI32IntegerAttr(0);
    auto eW = builder->getI32IntegerAttr(0);
    mlir::ArrayAttr weightMemRefRange = builder->getArrayAttr({sW, eW});

    start = end;
    end = start + 1;
    auto sS = builder->getI32IntegerAttr(0);
    auto eS = builder->getI32IntegerAttr(0);
    mlir::ArrayAttr scaleMemRefRange = builder->getArrayAttr({sS, eS});

    builder->setInsertionPoint(block_l, block_l->end());
    auto op_tensor_info = builder->create<hlcs::NpuStripeCmd>(
        loc, kernelResult, srcMemRef, ifmMemRefRange, ofmMemRefRange,
        weightMemRefRange, scaleMemRefRange, block_config, is_first, is_last,
        is_first_h_stripe, is_last_h_stripe, concat_axis, concat_offset,
        pad_top, pad_bottom);
  } else {
    srcMemRef.push_back(weightMemRefCmd->getResult(0));
    srcMemRef.push_back(scaleMemRefCmd->getResult(0));

    start = end;
    end = start + 1;
    auto sW = builder->getI32IntegerAttr(start);
    auto eW = builder->getI32IntegerAttr(end);
    mlir::ArrayAttr weightMemRefRange = builder->getArrayAttr({sW, eW});

    start = end;
    end = start + 1;
    auto sS = builder->getI32IntegerAttr(start);
    auto eS = builder->getI32IntegerAttr(end);
    mlir::ArrayAttr scaleMemRefRange = builder->getArrayAttr({sS, eS});

    builder->setInsertionPoint(block_l, block_l->end());
    auto op_tensor_info = builder->create<hlcs::NpuStripeCmd>(
        loc, kernelResult, srcMemRef, ifmMemRefRange, ofmMemRefRange,
        weightMemRefRange, scaleMemRefRange, block_config, is_first, is_last,
        is_first_h_stripe, is_last_h_stripe, concat_axis, concat_offset,
        pad_top, pad_bottom);
  }
  return &(block_l->back());
}

} // namespace planner
} // namespace mlir

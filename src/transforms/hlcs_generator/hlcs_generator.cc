/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#include "src/transforms/hlcs_generator/hlcs_generator.h"
#include "src/ir/geometric_transform/bounding_box.h"

namespace mlir {
namespace planner {

/*
   0: Reasoning behinf Info Structures is that they do not have tensor inputs
   and outputs i.e data flow is broken HLCS does not have a command stream data
   flow Also they have a lower footprint and can be re-used into strip commands
   efficiently without repeating

      ToDo: Evaluate a different sturcutre where we keep lowering the ScheduleIR
   and the IR transformation is quite aggressive at this point
 */

hlcs_generator::hlcs_generator() {
  operator_count = 0;
  kernel_count = 0;
  cascade_count = 0;
  tensor_count = 0;
}

Operation *hlcs_generator::search_in_tensor_library(
    mlir::Value v, llvm::iplist<Operation>::iterator insertionPoint) {
  mlir::Operation *opt;

  uint64_t unique_id =
      v.getType().dyn_cast<mlir::SchedTensorType>().getUniqueId();
  bool io_memory_op =
      v.getType().dyn_cast<mlir::SchedTensorType>().get_io_memory_op();

  // std::cout << "unique id: " << unique_id << std::endl;
  if (tensor_library.find(unique_id) == tensor_library.end() or io_memory_op) {
    opt = createTensorInfo(v, this->hlcs_npu_region_op_block, insertionPoint,
                           tensor_count++);
    if (not(io_memory_op))
      tensor_library[unique_id] = opt;
  } else {
    opt = tensor_library[unique_id];
  }
  return opt;
}

Operation *hlcs_generator::search_in_tensor_library(mlir::Value v) {
  mlir::Operation *opt;
  uint64_t unique_id =
      v.getType().dyn_cast<mlir::SchedTensorType>().getUniqueId();
  bool io_memory_op =
      v.getType().dyn_cast<mlir::SchedTensorType>().get_io_memory_op();

  if (io_memory_op)
    FATAL_ERROR("hlcs_generator::search_in_tensor_library::Searching for "
                "tensor that touches a io_memory_op")

  if (tensor_library.find(unique_id) == tensor_library.end())
    return nullptr;
  else
    return tensor_library[unique_id];
}

void hlcs_generator::generate_command_list(
    cascadeir::CascadeRegionOp *cascade) {
  LOG(ENABLE_LOGGING_HLCS_GENERATOR_VERBOSE_LEVEL_1,
      "generate_command_list()::Start");

  ::mlir::cascadeir::PlanType plan_type = cascade->plan_type().getValue();
  if (plan_type == ::mlir::cascadeir::PlanType::MEMORY)
    return;

  Region &cascade_list_region = cascade->getRegion();
  Block &cascade_list_block = cascade_list_region.front();

  std::map<Operation *, mlir::Operation *> kernel_library;

  // 0. Prepare Command List Attributes
  ::mlir::StringAttr HLCSPlan;
  if (plan_type ==
      ::mlir::cascadeir::PlanType::IFM_STATIONARY_WEIGHT_STREAMING) {
    HLCSPlan =
        builder->getStringAttr(StringRef("IFM_STATIONARY_WEIGHT_STREAMING"));
  } else if (plan_type ==
             ::mlir::cascadeir::PlanType::IFM_STREAMING_WEIGHT_STATIONARY) {
    HLCSPlan =
        builder->getStringAttr(StringRef("IFM_STREAMING_WEIGHT_STATIONARY"));
  } else if (plan_type ==
             ::mlir::cascadeir::PlanType::IFM_STATIONARY_WEIGHT_STATIONARY) {
    HLCSPlan =
        builder->getStringAttr(StringRef("IFM_STATIONARY_WEIGHT_STATIONARY"));
  } else {
    std::cout << "Plan Type: " << ConvertToString(plan_type).str()
              << "::" << (int)(plan_type) << std::endl;
    ASSERT_COND(true, "hlcs_generator::generate_command_list::Illegal Plan");
  }

  // 1.  Generate Command List and Cascade attributes to Command List
  builder->setInsertionPoint(this->hlcs_npu_region_op_block,
                             this->hlcs_npu_region_op_block->end());
  auto command_list =
      builder->create<hlcs::CommandListOp>(builder->getUnknownLoc(), HLCSPlan);
  Region &command_list_region = command_list.getRegion();
  Block *command_list_block =
      builder->createBlock(&command_list_region, command_list_region.begin());

  // 0. Every needed kernel & op gets packed into a KernelInfo and placed at the
  // insertionPoint
  llvm::iplist<Operation> &cascade_operations =
      cascade_list_block.getOperations();
  for (auto &op : cascade_operations) {
    if (llvm::isa<kernelir::KernelRegionOp>(&op)) {
      mlir::Operation *kernel =
          createKernelInfo(&op, command_list_block, command_list_block->end());
      kernel_library[&op] = kernel;
    }
  }

  if (plan_type ==
      ::mlir::cascadeir::PlanType::IFM_STREAMING_WEIGHT_STATIONARY) {
    generate_IFM_STREAMING(command_list_block, cascade, kernel_library);
  } else if (plan_type ==
             ::mlir::cascadeir::PlanType::IFM_STATIONARY_WEIGHT_STREAMING) {
    generate_WEIGHT_STREAMING(command_list_block, cascade, kernel_library);
  } else if (plan_type ==
             ::mlir::cascadeir::PlanType::IFM_STATIONARY_WEIGHT_STATIONARY) {
    generate_STATIONARY(command_list_block, cascade, kernel_library);
  }

  LOG(ENABLE_LOGGING_HLCS_GENERATOR_VERBOSE_LEVEL_1,
      "generate_command_list()::End");
}

unsigned hlcs_generator::generate(
    OpBuilder &builder, mlir::Operation *schedule_npu_region_op,
    mlir::Operation *hlcs_npu_region_op,
    mlir::ArchitectureConfig::ArchitectureConfigAttr &arch_c) {
  LOG(ENABLE_LOGGING_HLCS_GENERATOR_VERBOSE_LEVEL_1, "generate()::Start");
  Region &schedule_npu_region = schedule_npu_region_op->getRegion(0);
  Block &schedule_npu_region_block = schedule_npu_region.front();

  Region &hlcs_npu_region = hlcs_npu_region_op->getRegion(0);
  Block &hlcs_npu_region_block = hlcs_npu_region.front();

  this->hlcs_npu_region_op_block = &hlcs_npu_region_block;
  this->builder = &builder;
  this->arch_c = arch_c;

  llvm::iplist<Operation> &schedule_npu_region_operations =
      schedule_npu_region_block.getOperations();
  for (auto &op : schedule_npu_region_operations) {
    if (llvm::isa<cascadeir::CascadeRegionOp>(&op)) {
      /* Every Cascade gets lowered into a TensorInfo & {KernelInfo, Command
       * List} */
      cascadeir::CascadeRegionOp cascade_op =
          cast<cascadeir::CascadeRegionOp>(op);
      generate_command_list(&cascade_op);
    } else if (llvm::isa<scheduleir::YieldOp>(&op)) {
      this->builder->setInsertionPoint(this->hlcs_npu_region_op_block,
                                       this->hlcs_npu_region_op_block->end());
      auto command_list =
          builder.create<hlcs::YieldOp>(schedule_npu_region_op->getLoc());
    } else if (llvm::isa<scheduleir::CastInOp>(&op))
      continue;
    else if (llvm::isa<scheduleir::CastOutOp>(&op))
      continue;
    else
      ASSERT_COND(true, "A Schedule NPU Region should only hold "
                        "cascadeir::CascadeRegionOp and scheduleir");
  }
  LOG(ENABLE_LOGGING_HLCS_GENERATOR_VERBOSE_LEVEL_1, "generate()::End");
  return 0;
}

} // namespace planner
} // namespace mlir

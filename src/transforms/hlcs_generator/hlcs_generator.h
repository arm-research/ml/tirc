/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#ifndef HLCS_GENERATOR_H
#define HLCS_GENERATOR_H

#include <iostream>
#include <unordered_map>
#include <vector>

#include "src/utils/cluster_ir.h"
#include "src/utils/dag.h"

#include "src/ir/architecture_config/architecture_config.h"
#include "src/utils/checkIR.h"
#include "src/utils/printIR.h"

#include "mlir/Dialect/Quant/QuantOps.h"
#include "mlir/Dialect/Quant/QuantTypes.h"
#include "mlir/Dialect/StandardOps/IR/Ops.h"      // from @llvm-project
#include "mlir/IR/AffineMap.h"                    // from @llvm-project
#include "mlir/IR/Attributes.h"                   // from @llvm-project
#include "mlir/IR/BlockAndValueMapping.h"         // from @llvm-project
#include "mlir/IR/Builders.h"                     // from @llvm-project
#include "mlir/IR/Location.h"                     // from @llvm-project
#include "mlir/IR/MLIRContext.h"                  // from @llvm-project
#include "mlir/IR/Operation.h"                    // from @llvm-project
#include "mlir/IR/PatternMatch.h"                 // from @llvm-project
#include "mlir/Interfaces/InferTypeOpInterface.h" // from @llvm-project
#include "mlir/Pass/Pass.h"                       // from @llvm-project
#include "mlir/Transforms/DialectConversion.h"    // from @llvm-project

#include "src/ir/cascade_ir.h"
#include "src/ir/hlcs_ir.h"
#include "src/ir/kernel_ir.h"
#include "src/ir/raw_data_attribute/RawDataAttr.h"

#include "src/transforms/kernel_packing/kernelInfo.h"
#include "src/utils/printIR.h"

#include "src/ir/geometric_transform/bounding_box.h"

namespace mlir {
namespace planner {

class hlcs_generator {
public:
  hlcs_generator();

  unsigned generate(OpBuilder &builder, mlir::Operation *schedule_npu_region_op,
                    mlir::Operation *hlcs_npu_region_op,
                    mlir::ArchitectureConfig::ArchitectureConfigAttr &arch_c);

  Block *hlcs_npu_region_op_block;
  OpBuilder *builder;
  mlir::ArchitectureConfig::ArchitectureConfigAttr arch_c;

  Operation *search_in_tensor_library(mlir::Value v);

private:
  Operation *
  search_in_tensor_library(mlir::Value v,
                           llvm::iplist<Operation>::iterator insertionPoint);

  void generate_command_list(cascadeir::CascadeRegionOp *cascade);

  void createOperationInfo(Operation *op_Operation, Block *block,
                           llvm::iplist<Operation>::iterator insertionPoint);
  mlir::Operation *
  createKernelInfo(Operation *op_kernel, Block *block,
                   llvm::iplist<Operation>::iterator insertionPoint);
  mlir::Operation *
  createTensorInfo(mlir::Value tensor, Block *block,
                   llvm::iplist<Operation>::iterator insertionPoint,
                   uint32_t id);

  /* Always Inserted at the End of the Block by Default */
  Operation *createStripeMemCmd(Block *block, Operation *tensorInfo,
                                coordinate startCoord, coordinate endCoord);
  Operation *createDmaCmd(Block *block, Operation *srcMemRefCmd,
                          Operation *dstMemRefCmd, Operation *kernelInfo);
  Operation *createStripeCmd(Block *block,
                             std::vector<Operation *> ifmMemRefCmds,
                             Operation *weightMemRefCmd,
                             Operation *ofmMemRefCmd, Operation *scaleMemRefCmd,
                             NpuBlockConfig block_config_i, bool is_first_i,
                             bool is_last_i, bool is_fist_h_stripe_i,
                             bool is_last_h_stripe_i, int32_t concat_axis_i,
                             int32_t concat_offset_i, int32_t pad_top_i,
                             int32_t pad_bottom_i, Operation *kernelInfo);

  void generate_IFM_STREAMING(
      Block *command_list_block, cascadeir::CascadeRegionOp *cascade,
      std::map<Operation *, mlir::Operation *> kernel_library);
  void generate_WEIGHT_STREAMING(
      Block *command_list_block, cascadeir::CascadeRegionOp *cascade,
      std::map<Operation *, mlir::Operation *> kernel_library);
  void
  generate_STATIONARY(Block *command_list_block,
                      cascadeir::CascadeRegionOp *cascade,
                      std::map<Operation *, mlir::Operation *> kernel_library);

  void createHLCSYield(Block *block_l,
                       llvm::iplist<Operation>::iterator insertionPoint);

  std::map<uint64_t, mlir::Operation *> tensor_library;

  uint32_t operator_count;
  uint32_t kernel_count;
  uint32_t cascade_count;
  uint32_t tensor_count;
};

} // namespace planner
} // namespace mlir

#endif // HLCS_GENERATOR_H

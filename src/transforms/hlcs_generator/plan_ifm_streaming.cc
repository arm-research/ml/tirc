/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#include "src/ir/geometric_transform/bounding_box.h"
#include "src/transforms/hlcs_generator/generator.h"
#include "src/transforms/hlcs_generator/hlcs_generator.h"
#include "src/transforms/hlcs_generator/stripe_command.h"

namespace mlir {
namespace planner {

void printSequence(const std::vector<stripe_command> sequence) {
  std::cout << std::endl;
  std::cout << YELLOW << "Sequence: " << RESET << sequence.size() << std::endl;
  for (auto s : sequence)
    std::cout
        << std::left << std::setfill(' ') << s.op << " "
        << std::setw(15)
        //<< kernelir::getKernelMajorOp(s.op)->getName().getStringRef().str()
        << BLUE << " IFM: " << RESET << "[" << s.ifm[0].get_start_coord() << " "
        << s.ifm[0].get_end_coord() << "]" << BLUE << " OFM: " << RESET << "["
        << s.ofm.get_start_coord() << " " << s.ofm.get_end_coord() << "]"
        << BLUE << " WEIGHT: " << RESET << "[" << s.weight.get_start_coord()
        << " " << s.weight.get_end_coord() << "]" << BLUE << " SCALE: " << RESET
        << "[" << s.scale.get_start_coord() << " " << s.scale.get_end_coord()
        << "]" << BLUE << " is_first: " << RESET << s.is_first_in_cascade
        << BLUE << " is_last: " << RESET << s.is_last_in_cascade << BLUE
        << " is_fist_h_stripe: " << RESET << s.is_fist_h_stripe << BLUE
        << " is_last_h_stripe: " << RESET << s.is_last_h_stripe << std::endl;
  //<< " " << s.ofm << " " << s.weight << " " << s.scale << std::endl;
  std::cout << std::endl;
}

void printGenerators(
    std::unordered_map<Operation *, unique_ptr<Generator>> &generators) {
  for (auto &g : generators) {
    Operation *op = g.first;
    Generator *gen = g.second.get();
    std::cout << op << "->" << gen << std::endl;
    std::cout << gen->get_no_producers() << std::endl;
    if (gen->get_no_producers() > 0)
      std::cout << gen->get_producers()[0] << std::endl;
  }
  std::cout << std::endl;
}

void recurse_to_prev(std::vector<stripe_command> &sequence, Generator *g) {
  auto pred_generator = g->get_producers()[0];
  if (pred_generator->get_no_producers() == 0)
    sequence.push_back(pred_generator->get_next_command());
  else {
    if (pred_generator->next_command_available())
      sequence.push_back(pred_generator->get_next_command());
    else
      recurse_to_prev(sequence, pred_generator);
  }
}
void GenerateSequence(cascadeir::CascadeRegionOp *cascade,
                      std::vector<stripe_command> &sequence,
                      OpBuilder *builder) {
  SmallVector<Operation *> kernels = cascadeir::ExtractCascadeKernels(cascade);

  std::unordered_map<Operation *, unique_ptr<Generator>> generators;
  for (auto k : kernels)
    generators[k] = make_unique<Generator>(k);

  create_generators_dependancies(generators);

  /*
      std::cout << "Cascade Kernels: ";
      for (auto op: kernels)
          std::cout << op << ",";
      std::cout << std::endl;
      for (auto &g: generators)
      {
         auto pr = g.second->get_producers();
         for (auto p: pr)
         {
           std::cout << g.first << "->" << p->op_kernel << std::endl;
         }
      }
      std::cout << std::endl;
  */

  Generator *gen_starting_point = generators[kernels[kernels.size() - 1]].get();

  Generator *gen_first = generators[kernels[0]].get();
  gen_first->set_first();
  gen_starting_point->set_last();
  do {
    if (gen_starting_point->next_command_available()) {
      sequence.push_back(gen_starting_point->get_next_command());
    } else
      recurse_to_prev(sequence, gen_starting_point);
  } while (not(gen_starting_point->drained()));
}

void hlcs_generator::generate_IFM_STREAMING(
    Block *command_list_block, cascadeir::CascadeRegionOp *cascade,
    std::map<Operation *, mlir::Operation *> kernel_library) {
  std::vector<stripe_command> sequence;
  GenerateSequence(cascade, sequence, this->builder);

  // printSequence(sequence);

  llvm::iplist<Operation>::iterator insertionPoint(
      command_list_block->getParentOp());

  std::vector<Operation *> register_wb_op;
  for (auto s : sequence) {
    mlir::Operation *op_kernel_info = kernel_library[s.op];
    Operation *op_kernel_primary_op = mlir::kernelir::getKernelMajorOp(s.op);

    Operation *weightMemRefCmd = nullptr;
    Operation *scaleMemRefCmd = nullptr;
    if ((llvm::isa<scheduleir::Conv2DOp>(*op_kernel_primary_op)) ||
        (llvm::isa<scheduleir::DepthwiseConv2DOp>(*op_kernel_primary_op))) {
      if (std::find(register_wb_op.begin(), register_wb_op.end(), s.op) ==
          register_wb_op.end()) {
        mlir::Value weight_input_0 = op_kernel_primary_op->getOperand(1);
        mlir::Value scale_input_0 = op_kernel_primary_op->getOperand(2);

        mlir::SchedTensorType weight_input_0_type =
            weight_input_0.getType().dyn_cast<mlir::SchedTensorType>();
        mlir::SchedTensorType scale_input_0_type =
            scale_input_0.getType().dyn_cast<mlir::SchedTensorType>();

        // DMA_CMD Move all Weights into SRAM
        if (llvm::isa<mlir::scheduleir::DmaOp>(
                weight_input_0.getDefiningOp())) {
          Operation *dma_op = weight_input_0.getDefiningOp();

          mlir::Operation *opt_input =
              search_in_tensor_library(dma_op->getOperand(0), insertionPoint);
          coordinate opt_input_start = {0, 0, 0, 0};
          coordinate opt_input_end =
              arrayref_2_smallvector(weight_input_0_type.getShape());
          Operation *dma_op_input = createStripeMemCmd(
              command_list_block, opt_input, opt_input_start, opt_input_end);

          mlir::Operation *opt_output =
              search_in_tensor_library(dma_op->getResult(0), insertionPoint);
          coordinate opt_output_start = {0, 0, 0, 0};
          coordinate opt_output_end =
              arrayref_2_smallvector(weight_input_0_type.getShape());
          Operation *dma_op_output = createStripeMemCmd(
              command_list_block, opt_output, opt_output_start, opt_output_end);

          Operation *dma_op_command = createDmaCmd(
              command_list_block, dma_op_input, dma_op_output, op_kernel_info);
        }

        // DMA_CMD Move all Biases into SRAM
        if (llvm::isa<mlir::scheduleir::DmaOp>(scale_input_0.getDefiningOp())) {
          Operation *dma_op = scale_input_0.getDefiningOp();

          mlir::Operation *opt_input =
              search_in_tensor_library(dma_op->getOperand(0), insertionPoint);
          coordinate opt_input_start = {0};
          coordinate opt_input_end =
              arrayref_2_smallvector(scale_input_0_type.getShape());
          Operation *dma_op_input = createStripeMemCmd(
              command_list_block, opt_input, opt_input_start, opt_input_end);

          mlir::Operation *opt_output =
              search_in_tensor_library(dma_op->getResult(0), insertionPoint);
          coordinate opt_output_start = {0};
          coordinate opt_output_end =
              arrayref_2_smallvector(scale_input_0_type.getShape());
          Operation *dma_op_output = createStripeMemCmd(
              command_list_block, opt_output, opt_output_start, opt_output_end);

          Operation *dma_op_command = createDmaCmd(
              command_list_block, dma_op_input, dma_op_output, op_kernel_info);
        }
        register_wb_op.push_back(s.op);
      }

      mlir::Value weight_input_0 = op_kernel_primary_op->getOperand(1);
      coordinate weight_start = s.weight.get_start_coord();
      coordinate weight_end = s.weight.get_start_coord();
      mlir::Operation *weight_opt =
          search_in_tensor_library(weight_input_0, insertionPoint);
      weightMemRefCmd = createStripeMemCmd(command_list_block, weight_opt,
                                           weight_start, weight_end);

      mlir::Value scale_input_0 = op_kernel_primary_op->getOperand(2);
      coordinate scale_start = s.scale.get_start_coord();
      coordinate scale_end = s.scale.get_end_coord();
      mlir::Operation *scale_opt =
          search_in_tensor_library(scale_input_0, insertionPoint);
      scaleMemRefCmd = createStripeMemCmd(command_list_block, scale_opt,
                                          scale_start, scale_end);
    }

    mlir::Value ifm_input_0 = op_kernel_primary_op->getOperand(0);
    coordinate ifm_start = s.ifm[0].get_start_coord();
    coordinate ifm_end = s.ifm[0].get_end_coord();
    mlir::Operation *ifm_opt =
        search_in_tensor_library(ifm_input_0, insertionPoint);
    Operation *ifmMemRefCmds_0 =
        createStripeMemCmd(command_list_block, ifm_opt, ifm_start, ifm_end);
    std::vector<Operation *> ifmMemRefCmds;
    ifmMemRefCmds.push_back(ifmMemRefCmds_0);

    mlir::Value ofm_output_0 = s.op->getResult(0);
    coordinate ofm_start = s.ofm.get_start_coord();
    coordinate ofm_end = s.ofm.get_end_coord();
    mlir::Operation *ofm_opt =
        search_in_tensor_library(ofm_output_0, insertionPoint);
    Operation *ofmMemRefCmd =
        createStripeMemCmd(command_list_block, ofm_opt, ofm_start, ofm_end);

    Operation *npu_stripe_command = createStripeCmd(
        command_list_block, ifmMemRefCmds, weightMemRefCmd, ofmMemRefCmd,
        scaleMemRefCmd, s.block_config, s.is_first_in_cascade,
        s.is_last_in_cascade, s.is_fist_h_stripe, s.is_last_h_stripe,
        s.concat_axis, s.concat_offset, s.pad_top, s.pad_bottom,
        op_kernel_info);
  }
  createHLCSYield(command_list_block, command_list_block->end());
  cascade_count++;
}

} // namespace planner
} // namespace mlir

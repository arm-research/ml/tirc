/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#include "src/ir/geometric_transform/bounding_box.h"
#include "src/transforms/hlcs_generator/hlcs_generator.h"

namespace mlir {
namespace planner {

void hlcs_generator::generate_STATIONARY(
    Block *command_list_block, cascadeir::CascadeRegionOp *cascade,
    std::map<Operation *, mlir::Operation *> kernel_library) {
  // std::cout << "generate_STATIONARY::start" << std::endl;

  // 0. Tensor Insertion Point
  llvm::iplist<Operation>::iterator insertionPoint(
      command_list_block->getParentOp());

  // 1. Get Single Kernel in Cascade
  Operation *op_kernel = &(cascade->getRegion().front().front());
  mlir::Operation *op_kernel_info = kernel_library[op_kernel];

  Operation *op_kernel_primary_op = mlir::kernelir::getKernelMajorOp(op_kernel);

  bool is_first_i = true;
  bool is_last_i = true;
  bool is_fist_h_stripe_i = true;
  bool is_last_h_stripe_i = true;

  mlir::utils::FilterMetaData filter = kernelir::getKernelFilter(op_kernel);
  int32_t pad_top_i = filter.padding.top;
  int32_t pad_bottom_i = filter.padding.bottom;
  mlir::utils::SliceViewMetaData slice_view =
      kernelir::getKernelSliceView(op_kernel);
  int32_t concat_axis_i = slice_view.axis;
  int32_t concat_offset_i = slice_view.offset;

  Operation *weightMemRefCmd = nullptr;
  Operation *scaleMemRefCmd = nullptr;
  if ((llvm::isa<scheduleir::Conv2DOp>(*op_kernel_primary_op)) ||
      (llvm::isa<scheduleir::DepthwiseConv2DOp>(*op_kernel_primary_op))) {
    mlir::Value scale_input_0 = op_kernel_primary_op->getOperand(2);
    mlir::SchedTensorType scale_input_0_type =
        scale_input_0.getType().dyn_cast<mlir::SchedTensorType>();
    // 0. DMAOp Move Scales from one MemArea to another MemArea
    // std::cout << "Scales Start: " << std::endl;
    if (llvm::isa<mlir::scheduleir::DmaOp>(scale_input_0.getDefiningOp())) {
      Operation *dma_op = scale_input_0.getDefiningOp();

      mlir::Operation *opt_input =
          search_in_tensor_library(dma_op->getOperand(0), insertionPoint);
      coordinate opt_input_start = {0};
      coordinate opt_input_end =
          arrayref_2_smallvector(scale_input_0_type.getShape());
      Operation *dma_op_input = createStripeMemCmd(
          command_list_block, opt_input, opt_input_start, opt_input_end);

      mlir::Operation *opt_output =
          search_in_tensor_library(dma_op->getResult(0), insertionPoint);
      coordinate opt_output_start = {0};
      coordinate opt_output_end =
          arrayref_2_smallvector(scale_input_0_type.getShape());
      Operation *dma_op_output = createStripeMemCmd(
          command_list_block, opt_output, opt_output_start, opt_output_end);

      Operation *dma_op_command = createDmaCmd(command_list_block, dma_op_input,
                                               dma_op_output, op_kernel_info);
    }
    // std::cout << "Scales End: " << std::endl;
    coordinate scale_start = {0};
    coordinate scale_end =
        arrayref_2_smallvector(scale_input_0_type.getShape());
    mlir::Operation *scale_opt =
        search_in_tensor_library(scale_input_0, insertionPoint);
    scaleMemRefCmd = createStripeMemCmd(command_list_block, scale_opt,
                                        scale_start, scale_end);

    mlir::Value weight_input_0 = op_kernel_primary_op->getOperand(1);
    mlir::SchedTensorType weight_input_0_type =
        weight_input_0.getType().dyn_cast<mlir::SchedTensorType>();
    // 0. DMAOp Move Weights from one MemArea to another MemArea
    // std::cout << "Weights: " << std::endl;
    if (llvm::isa<mlir::scheduleir::DmaOp>(weight_input_0.getDefiningOp())) {
      Operation *dma_op = weight_input_0.getDefiningOp();

      mlir::Operation *opt_input =
          search_in_tensor_library(dma_op->getOperand(0), insertionPoint);
      coordinate opt_input_start = {0, 0, 0, 0};
      coordinate opt_input_end =
          arrayref_2_smallvector(weight_input_0_type.getShape());
      Operation *dma_op_input = createStripeMemCmd(
          command_list_block, opt_input, opt_input_start, opt_input_end);

      mlir::Operation *opt_output =
          search_in_tensor_library(dma_op->getResult(0), insertionPoint);
      coordinate opt_output_start = {0, 0, 0, 0};
      coordinate opt_output_end =
          arrayref_2_smallvector(weight_input_0_type.getShape());
      Operation *dma_op_output = createStripeMemCmd(
          command_list_block, opt_output, opt_output_start, opt_output_end);

      Operation *dma_op_command = createDmaCmd(command_list_block, dma_op_input,
                                               dma_op_output, op_kernel_info);
    }
    coordinate weight_start = {0, 0, 0, 0};
    coordinate weight_end =
        arrayref_2_smallvector(weight_input_0_type.getShape());
    mlir::Operation *weight_opt =
        search_in_tensor_library(weight_input_0, insertionPoint);
    weightMemRefCmd = createStripeMemCmd(command_list_block, weight_opt,
                                         weight_start, weight_end);
  }

  // 1. NPU Stripe
  // std::cout << "Inputs: " << std::endl;
  std::vector<Operation *> ifmMemRefCmds;
  assert((op_kernel->getNumOperands() > 0) &&
         "generate_STATIONARY(): op_kernel must have at least one operand.");
  mlir::Value ifm_input_0 = op_kernel_primary_op->getOperand(0);
  mlir::SchedTensorType ifm_input_0_type =
      ifm_input_0.getType().dyn_cast<mlir::SchedTensorType>();
  coordinate ifm_start;
  ifm_start.assign(ifm_input_0_type.getRank(), 0);
  coordinate ifm_end = arrayref_2_smallvector(ifm_input_0_type.getShape());
  mlir::Operation *ifm_opt =
      search_in_tensor_library(ifm_input_0, insertionPoint);
  Operation *ifmMemRefCmds_0 =
      createStripeMemCmd(command_list_block, ifm_opt, ifm_start, ifm_end);
  ifmMemRefCmds.push_back(ifmMemRefCmds_0);

  // if (op_kernel_primary_op->getNumOperands() > 1) {
  if (llvm::isa<scheduleir::AddOp>(op_kernel_primary_op) ||
      llvm::isa<scheduleir::MulOp>(op_kernel_primary_op)) {
    mlir::Value ifm_input_1 = op_kernel_primary_op->getOperand(1);
    mlir::SchedTensorType ifm_input_1_type =
        ifm_input_1.getType().dyn_cast<mlir::SchedTensorType>();
    coordinate ifm_start;
    ifm_start.assign(ifm_input_1_type.getRank(), 0);
    coordinate ifm_end = arrayref_2_smallvector(ifm_input_1_type.getShape());
    mlir::Operation *ifm_opt =
        search_in_tensor_library(ifm_input_1, insertionPoint);
    Operation *ifmMemRefCmds_1 =
        createStripeMemCmd(command_list_block, ifm_opt, ifm_start, ifm_end);
    ifmMemRefCmds.push_back(ifmMemRefCmds_1);
  }

  // std::cout << "Outputs: " << std::endl;
  mlir::Value ofm_output_0 = op_kernel->getResult(0);
  mlir::SchedTensorType ofm_output_0_type =
      ofm_output_0.getType().dyn_cast<mlir::SchedTensorType>();
  coordinate ofm_start;
  ofm_start.assign(ofm_output_0_type.getRank(), 0);
  coordinate ofm_end = arrayref_2_smallvector(ofm_output_0_type.getShape());
  mlir::Operation *ofm_opt =
      search_in_tensor_library(ofm_output_0, insertionPoint);
  Operation *ofmMemRefCmd =
      createStripeMemCmd(command_list_block, ofm_opt, ofm_start, ofm_end);

  NpuBlockConfig block_config_i;
  ArrayAttr start_coord = op_kernel->getAttrOfType<ArrayAttr>("block_config");
  for (int i = 0; i < start_coord.size(); i++)
    block_config_i.push_back(start_coord[i].dyn_cast<IntegerAttr>().getInt());

  Operation *npu_stripe_command = createStripeCmd(
      command_list_block, ifmMemRefCmds, weightMemRefCmd, ofmMemRefCmd,
      scaleMemRefCmd, block_config_i, is_first_i, is_last_i, is_fist_h_stripe_i,
      is_last_h_stripe_i, concat_axis_i, concat_offset_i, pad_top_i,
      pad_bottom_i, op_kernel_info);

  createHLCSYield(command_list_block, command_list_block->end());
  cascade_count++;

  // std::cout << "generate_STATIONARY::end" << std::endl;
}

} // namespace planner
} // namespace mlir

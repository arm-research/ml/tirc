/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#include "src/ir/geometric_transform/bounding_box.h"
#include "src/transforms/hlcs_generator/generator.h"
#include "src/transforms/hlcs_generator/hlcs_generator.h"

namespace mlir {
namespace planner {

void hlcs_generator::generate_WEIGHT_STREAMING(
    Block *command_list_block, cascadeir::CascadeRegionOp *cascade,
    std::map<Operation *, mlir::Operation *> kernel_library) {
  // 0. Tensor Insertion Point
  llvm::iplist<Operation>::iterator insertionPoint(
      command_list_block->getParentOp());

  // 1. Get Single Kernel in Cascade
  Operation *op_kernel = &(cascade->getRegion().front().front());
  mlir::Operation *op_kernel_info = kernel_library[op_kernel];

  Operation *op_kernel_primary_op = mlir::kernelir::getKernelMajorOp(op_kernel);
  bool is_depthwise =
      llvm::isa<scheduleir::DepthwiseConv2DOp>(*op_kernel_primary_op);

  bool is_first_i = true;
  bool is_last_i = true;
  bool is_fist_h_stripe_i = true;
  bool is_last_h_stripe_i = true;

  mlir::utils::FilterMetaData filter = kernelir::getKernelFilter(op_kernel);
  int32_t pad_top_i = filter.padding.top;
  int32_t pad_bottom_i = filter.padding.bottom;
  mlir::utils::SliceViewMetaData slice_view =
      kernelir::getKernelSliceView(op_kernel);
  int32_t concat_axis_i = slice_view.axis;
  int32_t concat_offset_i = slice_view.offset;

  if ((llvm::isa<scheduleir::Conv2DOp>(*op_kernel_primary_op)) ||
      (llvm::isa<scheduleir::DepthwiseConv2DOp>(*op_kernel_primary_op))) {
    mlir::Value weight_input_0 = op_kernel_primary_op->getOperand(1);
    mlir::SchedTensorType weight_input_0_type =
        weight_input_0.getType().dyn_cast<mlir::SchedTensorType>();
    ArrayRef<int64_t> weight_shape = weight_input_0_type.getShape();

    mlir::Value ofm_output_0 = op_kernel->getResult(0);
    mlir::SchedTensorType ofm_output_0_type =
        ofm_output_0.getType().dyn_cast<mlir::SchedTensorType>();
    ArrayRef<int64_t> ofm_shape = ofm_output_0_type.getShape();

    // Check Weights need DMAIng otherwise this plan makes no sense
    if (llvm::isa<mlir::scheduleir::DmaOp>(weight_input_0.getDefiningOp()) !=
        true)
      ASSERT_COND(true, "generate_WEIGHT_STREAMING:: Weights hve no DMA "
                        "associated to them so why wieght stream ?");

    NpuBlockConfig block_config_i;
    ArrayAttr start_coord = op_kernel->getAttrOfType<ArrayAttr>("block_config");
    uint32_t ofm_step = 0;
    for (int i = 0; i < start_coord.size(); i++) {
      auto v = start_coord[i].dyn_cast<IntegerAttr>().getInt();
      block_config_i.push_back(v);
      ofm_step = v;
    }

    // -1. ISSUE SCALE DMA COMMANDif
    // (llvm::isa<mlir::scheduleir::DmaOp>(scale_input_0.getDefiningOp()))
    mlir::Value scale_input_0 = op_kernel_primary_op->getOperand(2);
    mlir::SchedTensorType scale_input_0_type =
        scale_input_0.getType().dyn_cast<mlir::SchedTensorType>();
    if (llvm::isa<mlir::scheduleir::DmaOp>(scale_input_0.getDefiningOp())) {
      Operation *dma_op = scale_input_0.getDefiningOp();

      mlir::Operation *opt_input =
          search_in_tensor_library(dma_op->getOperand(0), insertionPoint);
      coordinate opt_input_start = {0};
      coordinate opt_input_end =
          arrayref_2_smallvector(scale_input_0_type.getShape());
      Operation *dma_op_input = createStripeMemCmd(
          command_list_block, opt_input, opt_input_start, opt_input_end);

      mlir::Operation *opt_output =
          search_in_tensor_library(dma_op->getResult(0), insertionPoint);
      coordinate opt_output_start = {0};
      coordinate opt_output_end =
          arrayref_2_smallvector(scale_input_0_type.getShape());
      Operation *dma_op_output = createStripeMemCmd(
          command_list_block, opt_output, opt_output_start, opt_output_end);

      Operation *dma_op_command = createDmaCmd(command_list_block, dma_op_input,
                                               dma_op_output, op_kernel_info);
    }

    uint32_t ofm_start = 0;
    uint32_t ofm_stop = ofm_shape[ofm_shape.size() - 1];
    for (uint32_t s = ofm_start; s < ofm_stop; s += ofm_step) {
      int64_t oc_range_start = s;
      int64_t oc_range_end = s + ofm_step;
      oc_range_end = min(oc_range_end, ofm_shape[ofm_shape.size() - 1]);

      // 0. ISSUE WEIGHT DMA COMMAND
      Operation *dma_op = weight_input_0.getDefiningOp();
      BoundingBox weight_box = make_weight_box(weight_shape, is_depthwise,
                                               oc_range_start, oc_range_end);
      mlir::Operation *opt_input =
          search_in_tensor_library(dma_op->getOperand(0), insertionPoint);
      Operation *dma_op_input = createStripeMemCmd(
          command_list_block, opt_input, weight_box.get_start_coord(),
          weight_box.get_end_coord());
      mlir::Operation *opt_output =
          search_in_tensor_library(dma_op->getResult(0), insertionPoint);
      Operation *dma_op_output = createStripeMemCmd(
          command_list_block, opt_output, weight_box.get_start_coord(),
          weight_box.get_end_coord());
      Operation *dma_op_command = createDmaCmd(command_list_block, dma_op_input,
                                               dma_op_output, op_kernel_info);

      // 1. ISSUE STRIPE COMMAND
      mlir::Value ifm_input_0 = op_kernel->getOperand(0);
      mlir::SchedTensorType ifm_input_0_type =
          ifm_input_0.getType().dyn_cast<mlir::SchedTensorType>();
      coordinate ifm_start, ifm_end;
      if (is_depthwise) {
        ifm_start = {0, 0, 0, oc_range_start};
        ifm_end = arrayref_2_smallvector(ifm_input_0_type.getShape());
        ifm_end[3] = oc_range_end;
      } else {
        ifm_start = {0, 0, 0, 0};
        ifm_end = arrayref_2_smallvector(ifm_input_0_type.getShape());
      }
      mlir::Operation *ifm_opt =
          search_in_tensor_library(ifm_input_0, insertionPoint);
      Operation *ifmMemRefCmds_0 =
          createStripeMemCmd(command_list_block, ifm_opt, ifm_start, ifm_end);
      std::vector<Operation *> ifmMemRefCmds;
      ifmMemRefCmds.push_back(ifmMemRefCmds_0);

      mlir::Operation *weight_opt =
          search_in_tensor_library(weight_input_0, insertionPoint);
      Operation *weightMemRefCmd = createStripeMemCmd(
          command_list_block, weight_opt, weight_box.get_start_coord(),
          weight_box.get_end_coord());

      mlir::Value scale_input_0 = op_kernel_primary_op->getOperand(2);
      mlir::SchedTensorType scale_input_0_type =
          scale_input_0.getType().dyn_cast<mlir::SchedTensorType>();
      coordinate scale_start = {oc_range_start};
      coordinate scale_end = {oc_range_end};
      mlir::Operation *scale_opt =
          search_in_tensor_library(scale_input_0, insertionPoint);
      Operation *scaleMemRefCmd = createStripeMemCmd(
          command_list_block, scale_opt, scale_start, scale_end);

      mlir::SchedTensorType ofm_output_0_type =
          ofm_output_0.getType().dyn_cast<mlir::SchedTensorType>();
      coordinate ofm_start = {0, 0, 0, oc_range_start};
      coordinate ofm_end = {ofm_shape[0], ofm_shape[1], ofm_shape[2],
                            oc_range_end};
      mlir::Operation *ofm_opt =
          search_in_tensor_library(ofm_output_0, insertionPoint);
      Operation *ofmMemRefCmd =
          createStripeMemCmd(command_list_block, ofm_opt, ofm_start, ofm_end);

      Operation *npu_stripe_command = createStripeCmd(
          command_list_block, ifmMemRefCmds, weightMemRefCmd, ofmMemRefCmd,
          scaleMemRefCmd, block_config_i, is_first_i, is_last_i,
          is_fist_h_stripe_i, is_last_h_stripe_i, concat_axis_i,
          concat_offset_i, pad_top_i, pad_bottom_i, op_kernel_info);
    }
  } else
    ASSERT_COND(true, "generate_WEIGHT_STREAMING:: Only works on Kernels with "
                      "a Major Op being a Conv2DOp.....One Step at a time !!!");
  createHLCSYield(command_list_block, command_list_block->end());
  cascade_count++;
}

} // namespace planner
} // namespace mlir

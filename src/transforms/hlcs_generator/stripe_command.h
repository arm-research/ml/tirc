/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#ifndef STRIPE_COMMAND_H
#define STRIPE_COMMAND_H

using namespace std;

#include "src/ir/geometric_transform/bounding_box.h"
#include "src/transforms/hlcs_generator/hlcs_generator.h"

namespace mlir {
namespace planner {

struct stripe_command {
  Operation *op;
  std::vector<BoundingBox> ifm;
  BoundingBox ofm;
  BoundingBox weight;
  BoundingBox scale;
  NpuBlockConfig block_config;
  bool is_first_in_cascade;
  bool is_last_in_cascade;
  bool is_fist_h_stripe;
  bool is_last_h_stripe;
  int32_t concat_axis;
  int32_t concat_offset;
  int32_t pad_top;
  int32_t pad_bottom;
};

} // namespace planner
} // namespace mlir

#endif // STRIPE_COMMAND_H

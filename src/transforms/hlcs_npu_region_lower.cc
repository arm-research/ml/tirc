/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#include "absl/memory/memory.h"
#include "mlir/Dialect/Quant/QuantOps.h"
#include "mlir/Dialect/Quant/QuantTypes.h"
#include "mlir/Dialect/StandardOps/IR/Ops.h"      // from @llvm-project
#include "mlir/IR/AffineMap.h"                    // from @llvm-project
#include "mlir/IR/Attributes.h"                   // from @llvm-project
#include "mlir/IR/BlockAndValueMapping.h"         // from @llvm-project
#include "mlir/IR/Builders.h"                     // from @llvm-project
#include "mlir/IR/Location.h"                     // from @llvm-project
#include "mlir/IR/MLIRContext.h"                  // from @llvm-project
#include "mlir/IR/Operation.h"                    // from @llvm-project
#include "mlir/IR/PatternMatch.h"                 // from @llvm-project
#include "mlir/Interfaces/InferTypeOpInterface.h" // from @llvm-project
#include "mlir/Pass/Pass.h"                       // from @llvm-project
#include "mlir/Support/FileUtilities.h"
#include "mlir/Support/ToolUtilities.h"
#include "mlir/Transforms/DialectConversion.h" // from @llvm-project
#include "llvm/Support/ToolOutputFile.h"
#include "llvm/Support/raw_ostream.h"

#include "src/compiler/compiler_cl.h"
#include "src/ir/hlcs_ir.h"
#include "src/transforms/llcs_generator/command_emitter.h"
#include "src/transforms/llcs_generator/hlcs_helper.h"
#include "src/transforms/llcs_generator/hlcs_mlir_utils.h"
#include "src/transforms/llcs_generator/hlcs_npu_api.h"
#include "src/transforms/llcs_generator/tensor_info.h"
#include "tensorflow/compiler/mlir/lite/ir/tfl_ops.h"

using namespace mlir;

#define PASS_NAME "hlcs-npu-region-lower"
#define DEBUG_TYPE PASS_NAME

#include <climits>
#include <cstdio>
#include <iostream>
#include <numeric>
using namespace std;

namespace mlir {

namespace hlcs {

namespace {

const int32_t arch_max_blockdep = 3;
static hlcs::Block arch_ofm_ublock{2, 2, 8};
static hlcs::Block arch_ifm_ublock{2, 1, 8};
hlcs::Block arch_ofm_block_max = hlcs::Block{64, 32, 128};
static uint32_t arch_shram_total_banks = 46; // 46 + 2 == 48
static uint32_t arch_shram_reserved_unused_banks = 2;

static uint32_t arch_available_shram_banks(bool uses_activation_lut) {
  uint32_t banks = arch_shram_total_banks;
  if (uses_activation_lut && (arch_shram_reserved_unused_banks == 0))
    banks -= 2;
  return banks;
}

// Maps an AccumulatorType enum to the corresponding acc_format value
static unordered_map<uint32_t, uint32_t> acc_format_map = {
    {/*"SHRAMElements.Acc16"*/ 5, e2i(acc_format::FP_S5_10)},
    {/*"SHRAMElements.Acc32"*/ 6, e2i(acc_format::INT_32BIT)},
    {/*"SHRAMElements.Acc40"*/ 7, e2i(acc_format::INT_40BIT)},
};

enum IFM2Broadcast {
  BroadcastHdim = 1 << 0,
  BroadcastWdim = 1 << 1,
  BroadcastCdim = 1 << 2,
  ReverseOperandOrder = 1 << 6,
  UseIFM2Scalar = 1 << 7,
};

enum OperandToScale {
  OPa = 1,
  OPb = 2,
};

static std::vector<int32_t> pooling_op_map = {e2i(pooling_mode::MAX),
                                              e2i(pooling_mode::AVERAGE),
                                              e2i(pooling_mode::REDUCE_SUM)};

// Maps an elementwise op type to an elementwise_mode enum value used by
// NPU_OP_ELEMENTWISE
static std::vector<int32_t> elementwise_mode_map = {
    e2i(elementwise_mode::ADD),   e2i(elementwise_mode::SUB),
    e2i(elementwise_mode::MUL),   e2i(elementwise_mode::ABS),
    e2i(elementwise_mode::MIN),   e2i(elementwise_mode::MAX),
    e2i(elementwise_mode::LRELU), e2i(elementwise_mode::CLZ),
    e2i(elementwise_mode::SHR),   e2i(elementwise_mode::SHL)};

static std::vector<int32_t> resampling_mode_map = {
    e2i(resampling_mode::NONE), e2i(resampling_mode::NEAREST),
    e2i(resampling_mode::TRANSPOSE)};

static std::vector<int32_t> rounding_mode_map = {
    e2i(rounding::TFL), e2i(rounding::TRUNCATE), e2i(rounding::NATURAL)};

static std::vector<int32_t> activation_op_map = {
    e2i(activation::NONE), e2i(activation::TANH), e2i(activation::SIGMOID)};

Kernel to_kernel(mlir::Operation &npu_op) {
  // Converts the given public API object to Kernel (used internally)
  int32_t kernel_w = get_i32_from_attr(npu_op, "kernel_w");
  int32_t kernel_h = get_i32_from_attr(npu_op, "kernel_h");
  ivec kernel_stride = get_ivec_from_attr(npu_op, "kernel_stride");
  ivec kernel_dilation = get_ivec_from_attr(npu_op, "kernel_dilation");
  return Kernel(kernel_w, kernel_h, kernel_stride[0], kernel_stride[1],
                kernel_dilation[0], kernel_dilation[1]);
}

/*
# Block job dependency:
# Does the VOLUME of IFMs for block job B(0) overlap with VOLUME of OFMs block
jobs A(8,9,10)
#
#  A                    | B
# ----------------------+------------------
# .... 3,4,5,6,7,8,9,10 | 0,1,2,3,4,5,6,8 10 < JOB NUMBER
#               |<------->| dependency offset
#
*/

PointXYZ get_offset_block_coords(Rect &area, hlcs::Block &block,
                                 int32_t offset) {
  // Get the coordinates of a block offset from either the end (negative)
  // or the start (zero or positive) of the given 3D area
  hlcs::Block size = area.size();
  // Dimensions of the region, in blocks
  int32_t width_blocks = round_up_divide(size.width, block.width);
  int32_t height_blocks = round_up_divide(size.height, block.height);
  int32_t depth_blocks = round_up_divide(size.depth, block.depth);
  int32_t total_blocks = width_blocks * height_blocks * depth_blocks;
  int32_t index = offset;
  if (offset < 0) {
    index = total_blocks + offset;
  }

  if (index >= total_blocks)
    return {-1, -1, -1}; // None

  // Coordinates of the indexed block
  int32_t coord_z = block.depth * (index % depth_blocks);
  int32_t coord_y = block.height * (index / (depth_blocks * width_blocks));
  int32_t coord_x = block.width * ((index / depth_blocks) % width_blocks);

  return {coord_x + area.x, coord_y + area.y, coord_z + area.z};
}

bool intersects(mlir::Operation &ifm, PointXYZ &ifm_start_coord,
                PointXYZ &ifm_end_coord, mlir::Operation &prev_ofm,
                PointXYZ &ofm_start_coord, PointXYZ &ofm_end_coord) {
  // Checks if the given IFM area overlaps with the given OFM area
  ivec ifm_shape = get_ivec_from_attr(ifm, "shape");
  ivec prev_ofm_shape = get_ivec_from_attr(prev_ofm, "shape");
  int32_t ifm_tiles_height_0 = get_i32_from_attr(ifm, "tiles_height_0");
  int32_t prev_ofm_tiles_height_0 =
      get_i32_from_attr(prev_ofm, "tiles_height_0");
  int32_t ifm_tiles_height_1 = get_i32_from_attr(ifm, "tiles_height_1");
  int32_t prev_ofm_tiles_height_1 =
      get_i32_from_attr(prev_ofm, "tiles_height_1");
  int32_t ifm_tiles_width_0 = get_i32_from_attr(ifm, "tiles_width_0");
  int32_t prev_ofm_tiles_width_0 = get_i32_from_attr(prev_ofm, "tiles_width_0");
  ivec ifm_tiles_addresses = get_ivec_from_attr(ifm, "tiles_addresses");
  ivec prev_ofm_tiles_addresses =
      get_ivec_from_attr(prev_ofm, "tiles_addresses");

  bool res = false;
  if ((ifm_shape == prev_ofm_shape) &&
      (ifm_tiles_height_0 == prev_ofm_tiles_height_0) &&
      (ifm_tiles_height_1 == prev_ofm_tiles_height_1) &&
      (ifm_tiles_width_0 == prev_ofm_tiles_width_0) &&
      (ifm_tiles_addresses == prev_ofm_tiles_addresses)) {
    // Common case: prev_op.ofm == op.ifm; in this case it suffices to check
    // if the xyz coordinates overlap, which is quick and easy
    res = coords_intersect(ifm_start_coord, ifm_end_coord, ofm_start_coord,
                           ofm_end_coord);
  } else {
    // The OFM produces a part of the IFM (e.g. a stripe), or the IFM consumes
    // part of the OFM. In this case, address comparison between the two areas
    // is needed
    auto ifm_ranges =
        get_address_ranges_for_area(ifm, ifm_start_coord, ifm_end_coord);
    auto prev_ofm_ranges =
        get_address_ranges_for_area(prev_ofm, ofm_start_coord, ofm_end_coord);
    res = range_lists_overlap(ifm_ranges, prev_ofm_ranges);
  }
  return res;
}

int32_t shape3d_size(NpuShape3D &shape) {
  return shape.width * shape.height * shape.depth;
}

Rect shape3d_to_rect(NpuShape3D &shape) {
  return Rect(0, 0, 0, shape.width - 1, shape.height - 1, shape.depth - 1);
}

// Simplified version of calculating elementwise Add/Sub scales
void simplified_elementwise_add_sub_scale(
    float input1_scale, float input2_scale, float output_scale,
    int32_t input_shift, float &input1_rescale, float &input2_rescale,
    uint32_t &out_scale, int32_t &out_shift) {
  float max_input_scale = std::max(input1_scale, input2_scale);

  input1_rescale = input1_scale * (1 << input_shift) / (2 * max_input_scale);
  input2_rescale = input2_scale * (1 << input_shift) / (2 * max_input_scale);
  float output_rescale =
      (2 * max_input_scale) / (output_scale * (1 << input_shift));

  quantise_scale(output_rescale, out_scale, out_shift);
}

// Advanced version of calculating elementwise Add/Sub scales
void advanced_elementwise_add_sub_scale(float input1_scale, float input2_scale,
                                        float output_scale, int32_t bitdepth,
                                        uint32_t &in_scale, int32_t &in_shift,
                                        uint32_t &out_scale, int32_t &out_shift,
                                        uint32_t &op_to_scale) {
  // Always scale the smaller of the input scales
  float max_input_scale = std::max(input1_scale, input2_scale);
  float min_input_scale = std::min(input1_scale, input2_scale);
  int32_t input_shift = (bitdepth == 8) ? 20 : 15;
  op_to_scale = static_cast<uint32_t>((input1_scale < input2_scale)
                                          ? OperandToScale::OPa
                                          : OperandToScale::OPb);

  float input1_rescale = 0.0f, input2_rescale = 0.0f;
  simplified_elementwise_add_sub_scale(
      min_input_scale, max_input_scale, output_scale, input_shift,
      input1_rescale, input2_rescale, out_scale, out_shift);

  quantise_scale(input1_rescale, in_scale, in_shift);
}

void elementwise_mul_scale(float input1_scale, float input2_scale,
                           float output_scale, uint32_t &out_scale,
                           int32_t &out_shift) {
  float output_rescale = (input1_scale * input2_scale) / output_scale;

  quantise_scale(output_rescale, out_scale, out_shift);
}

hlcs::Block arch_get_ifm_block_size(
    int32_t ifm_block_depth, hlcs::Block &ofm_block, Kernel &kernel,
    hlcs::Block subkernel = hlcs::Block{8, 8, 65536},
    resampling_mode ifm_resampling_mode = resampling_mode::NONE) {
  int32_t upscaling = (ifm_resampling_mode == resampling_mode::NONE) ? 1 : 2;
  // Height
  int32_t ifm_odd_2x_height_enable = 0;
  int32_t dilated_kernel_height = ((kernel.height - 1) * kernel.dilation.y) + 1;
  // upscaling
  int32_t ifm_block_height =
      ((ofm_block.height - 1) * kernel.stride.y +
       std::min(subkernel.height, dilated_kernel_height) +
       ifm_odd_2x_height_enable) /
      upscaling;

  ifm_block_height = round_up(ifm_block_height, arch_ofm_ublock.height);

  // Width
  int32_t ifm_odd_2x_width_enable = 0;
  int32_t dilated_kernel_width = ((kernel.width - 1) * kernel.dilation.x) + 1;
  int32_t ifm_block_width = ((ofm_block.width - 1) * kernel.stride.x +
                             std::min(subkernel.width, dilated_kernel_width) +
                             ifm_odd_2x_width_enable) /
                            upscaling;

  ifm_block_width = round_up(ifm_block_width, arch_ofm_ublock.width);

  return hlcs::Block{ifm_block_width, ifm_block_height, ifm_block_depth};
}

InputVolume get_first_job_input_volume(Rect &ifm, Rect &ofm,
                                       int32_t ifm_block_depth,
                                       hlcs::Block &ofm_block, Kernel &kernel,
                                       NpuPadding &padding,
                                       int32_t block_offset) {
  // Get ifm block size (jobs are invisibly decomposed into subkernels)
  hlcs::Block ifm_block = arch_get_ifm_block_size(ifm_block_depth, ofm_block,
                                                  kernel, arch_ofm_block_max);
  int32_t ifm_depth_blocks = round_up_divide(ifm.size().depth, ifm_block_depth);

  // Which OFM block are we calculating
  auto ofm_coord =
      get_offset_block_coords(ofm, ofm_block, block_offset / ifm_depth_blocks);
  if (ofm_coord.x == -1)               // None
    return {ofm_coord, ofm_coord, -1}; // None

  // Coordinate of the source IFM block
  int32_t ifm_coord_x = max(0, ofm_coord.x * kernel.stride.x - padding.left);
  int32_t ifm_coord_y = max(0, ofm_coord.y * kernel.stride.y - padding.right);
  int32_t ifm_coord_z =
      ifm.z + (block_offset % ifm_depth_blocks) * ifm_block.depth;

  // IFM block that will be sampled for the FIRST+block_offset job in the next
  // operator's OFM
  auto start_coord = PointXYZ{ifm_coord_x, ifm_coord_y, ifm_coord_z};
  auto end_coord = PointXYZ{start_coord.x + ifm_block.width,
                            start_coord.y + ifm_block.height,
                            start_coord.z + ifm_block.depth};

  return {start_coord, end_coord, 1}; // start, end, total jobs
}

InputVolume get_prev_job_output_volume(Rect &ofm, Block &ofm_block,
                                       int32_t block_offset) {
  assert(block_offset >= 0);

  // Get OFM block's volume coordinates
  auto start_coord = get_offset_block_coords(ofm, ofm_block, -1 - block_offset);
  if (start_coord.x == -1)                 // None
    return {start_coord, start_coord, -1}; // None
  auto end_coord = PointXYZ{start_coord.x + ofm_block.width,
                            start_coord.y + ofm_block.height,
                            start_coord.z + ofm_block.depth};
  return {start_coord, end_coord,
          1}; // start, end, total jobs for this OFM block
}

int32_t arch_calc_ifm_block_depth(int32_t ifm_depth, int32_t ifm_bits) {
  assert(ifm_depth > 0);
  ifm_depth = round_up(ifm_depth, arch_ifm_ublock.depth);
  int32_t max_block_depth = 8 * 32; // ifm_bits
  return std::min(max_block_depth, ifm_depth);
}

struct SharedBufferAllocation {
  Kernel kernel;
  bool uses_lut;
  NpuBlockType npu_block_type;
  bool all_fms_have_quant;
  resampling_mode ifm_resampling_mode;
  int32_t ifm_bits;
  int32_t ifm_depth;
  int32_t ifm_count;
  NpuShape3D ofm_shape;
};

SharedBufferAllocation create_shared_buffer(mlir::Operation &cmd) {

  // Creates shared buffer allocation for the given operation
  NpuOperationType op_type =
      static_cast<NpuOperationType>(get_i32_from_attr(cmd, "op_type"));
  NpuBlockType block_type = NpuBlockType::Default;
  switch (op_type) {
  case NpuOperationType::Conv2D:
    block_type = NpuBlockType::ConvolutionMxN;
    break;
  case NpuOperationType::ConvDepthWise:
    block_type = NpuBlockType::ConvolutionDepthWise;
    break;
  case NpuOperationType::Pooling: {
    string sub_op_type = get_str_from_attr(cmd, "sub_op_type");
    block_type = (sub_op_type == "ReduceSum") ? NpuBlockType::ReduceSum
                                              : NpuBlockType::Pooling;
  } break;
  case NpuOperationType::ElementWise:
    block_type = NpuBlockType::ElementWise;
    break;
  default:
    assert(!"unknown type.");
    break;
  }
  int32_t ifm_upscale = get_i32_from_attr(cmd, "ifm_upscale");
  resampling_mode ifm_resampling_mode =
      static_cast<resampling_mode>(resampling_mode_map[ifm_upscale]);

  // shared_buffer_allocation_for_npu_op
  NpuActivationOp act_op_type = static_cast<NpuActivationOp>(
      get_i32_from_attr(cmd, "activation_op_type"));
  bool uses_lut = (act_op_type == NpuActivationOp::TABLE_LOOKUP);

  auto ifms = get_operands_from_variadic(cmd, "ifm_range");
  assert(ifms.size() > 0);
  mlir::Operation &ifm = *ifms[0];

  auto ofms = get_operands_from_variadic(cmd, "ofm_range");
  assert(ofms.size() > 0);
  mlir::Operation &ofm = *ofms[0];

  /* std::vector<mlir::Operation&> fms; */
  /* fms.push_back(ifm); */
  /* fms.push_back(ofm); */
  /* if npu_op.ifm2 is not None: */
  /*     fms.append(npu_op.ifm2) */

  // all_fms_have_quant = not any(fm.quantization is None or
  // fm.quantization.scale_f32 is None for fm in fms)
  bool all_fms_have_quant = true;
  std::string data_type = get_str_from_attr(ifm, "data_type");
  int32_t ifm_bits = size_in_bytes(data_type) << 3;
  NpuShape3D shape(get_ivec_from_attr(ifm, "shape"));
  int32_t ifm_depth = shape.depth;
  // ifm_count = 2 if npu_op.ifm2 is not None and npu_op.ifm2_scalar is None
  // else 1
  int32_t ifm_count = 1;
  NpuShape3D ofm_shape(get_ivec_from_attr(ofm, "shape"));
  Kernel kernel = to_kernel(cmd);

  // shared_buffer_allocation
  return SharedBufferAllocation{
      kernel,   uses_lut,  block_type, all_fms_have_quant, ifm_resampling_mode,
      ifm_bits, ifm_depth, ifm_count,  ofm_shape};
}

int32_t get_ifm_ofm_block_depth(mlir::Operation &npu_op) {
  // Note: NOT equivalent to the normal ifm block depth calculation since
  // it takes into account 'depthless' block operations by returning full
  // depth
  NpuOperationType op_type =
      static_cast<NpuOperationType>(get_i32_from_attr(npu_op, "op_type"));
  if (op_type != NpuOperationType::Conv2D) {
    auto npu_op_ofms = get_operands_from_variadic(npu_op, "ofm_range");
    mlir::Operation &npu_op_ofm = *npu_op_ofms[0];
    ivec shape = get_ivec_from_attr(npu_op_ofm, "shape");
    return shape[2];
  }
  // Conv2D case
  auto npu_op_ifms = get_operands_from_variadic(npu_op, "ifm_range");
  mlir::Operation &npu_op_ifm = *npu_op_ifms[0];
  ivec shape = get_ivec_from_attr(npu_op_ifm, "shape");
  string data_type = get_str_from_attr(npu_op_ifm, "data_type");
  int32_t res =
      arch_calc_ifm_block_depth(shape[2], size_in_bytes(data_type) << 3);
  return res;
}

int32_t calc_blockdep(mlir::Operation *prev_op, mlir::Operation *npu_op) {
  // Calculates the value of the BLOCKDEP register
  if (prev_op == nullptr)
    return 0;
  // assert prev_op.ofm is not None
  auto npu_op_ifms = get_operands_from_variadic(*npu_op, "ifm_range");
  assert(npu_op_ifms.size());
  mlir::Operation &npu_op_ifm = *npu_op_ifms[0];

  auto npu_op_ofms = get_operands_from_variadic(*npu_op, "ofm_range");
  mlir::Operation &npu_op_ofm = *npu_op_ofms[0];

  auto prev_op_ofms = get_operands_from_variadic(*prev_op, "ofm_range");
  assert(prev_op_ofms.size());
  mlir::Operation &prev_op_ofm = *prev_op_ofms[0];

  NpuShape3D npu_op_ofm_shape(get_ivec_from_attr(npu_op_ofm, "shape"));
  NpuShape3D npu_op_ifm_shape(get_ivec_from_attr(npu_op_ifm, "shape"));
  NpuShape3D prev_op_ofm_shape(get_ivec_from_attr(prev_op_ofm, "shape"));

  // Check if IFM or IFM2 overlaps with prev op's OFM
  auto prev_ofm_ranges = get_address_ranges(prev_op_ofm);
  auto ifm_ranges = get_address_ranges(npu_op_ifm);
  bool ifm_overlaps = range_lists_overlap(prev_ofm_ranges, ifm_ranges);

  bool ifm2_overlaps = false;
  if (npu_op_ifms.size() > 1) {
    mlir::Operation &npu_op_ifm2 = *npu_op_ifms[1];
    auto ifm2_ranges = get_address_ranges(npu_op_ifm2);
    ifm2_overlaps = range_lists_overlap(prev_ofm_ranges, ifm2_ranges);
  }

  if (ifm_overlaps &&
      ifm2_overlaps) // Both IFM and IFM2 overlap (should be rare)
    return 0;
  if (!ifm_overlaps &&
      !ifm2_overlaps) // No overlap between prev OFM and IFM/IFM2
    return arch_max_blockdep;
  if (ifm2_overlaps) {
    mlir::Operation &npu_op_ifm2 = *npu_op_ifms[1];
    NpuShape3D npu_op_ifm2_shape(get_ivec_from_attr(npu_op_ifm2, "shape"));
    // Prev OFM produces IFM2 which is broadcasted (this should be rare)
    if (shape3d_size(npu_op_ifm2_shape) < shape3d_size(npu_op_ifm_shape))
      return 0;
  }

  // Prev OFM overlaps with IFM or IFM2; calculate the blockdep
  NpuShape3D prev_block_config(get_ivec_from_attr(*prev_op, "block_config"));
  NpuShape3D block_config(get_ivec_from_attr(*npu_op, "block_config"));

  uint32_t overlapping_fm_idx = ifm_overlaps ? 0 : 1;
  assert(npu_op_ifms[overlapping_fm_idx]);
  mlir::Operation &overlapping_fm = *npu_op_ifms[overlapping_fm_idx];

  int32_t cur_ifm_block_depth = get_ifm_ofm_block_depth(*npu_op);
  auto cur_ofm_block =
      hlcs::Block(block_config.width, block_config.height, block_config.depth);
  auto cur_ofm_rect = shape3d_to_rect(npu_op_ofm_shape);
  auto cur_ifm_rect = shape3d_to_rect(npu_op_ifm_shape);
  NpuPadding padding(get_ivec_from_attr(*npu_op, "padding")); // {0, 0, 0, 0};
  int32_t blockdep = arch_max_blockdep;
  Kernel kernel = to_kernel(*npu_op);

  auto prev_ofm_block =
      hlcs::Block(prev_block_config.width, prev_block_config.height,
                  prev_block_config.depth);
  auto prev_ofm_rect = shape3d_to_rect(prev_op_ofm_shape);
  // Iterate over the next BLOCKDEP inputs, checking to see if a sliding window
  // of IFM area overlaps with any previous OFM block generation.
  int32_t elapsed_jobs = 0;
  for (int32_t forward_offset = 0; forward_offset < arch_max_blockdep;
       ++forward_offset) {
    // This is the IFM block we want to sample from
    auto in_area = get_first_job_input_volume(
        cur_ifm_rect, cur_ofm_rect, cur_ifm_block_depth, cur_ofm_block, kernel,
        padding, forward_offset);
    if (in_area.total_jobs == -1)
      break;

    // Try several previous-OFM blocks in the past (they still might comprise
    // multiple IFM jobs)
    int32_t outstanding_jobs = 0;
    for (int32_t block_offset = 0; block_offset < arch_max_blockdep;
         ++block_offset) {
      // This is the OFM block being generated by the previous op
      auto out_area = get_prev_job_output_volume(prev_ofm_rect, prev_ofm_block,
                                                 block_offset);
      if (out_area.total_jobs == -1)
        break;

      // Block dependency is the max number of allowed outstanding jobs
      // in the pipeline. Selected by determining how many jobs occur
      // in between two operators' overlapping OFM->IFM block volumes
      if (intersects(overlapping_fm, in_area.start_coord, in_area.end_coord,
                     prev_op_ofm, out_area.start_coord, out_area.end_coord))
        break;
      // Early exit if no intersections and we've seen enough jobs in the
      // pipeline
      if (outstanding_jobs > arch_max_blockdep)
        break;

      // This OFM had this many jobs (accumulate over multiple OFM blocks)
      outstanding_jobs += out_area.total_jobs;
    }

    blockdep = min(blockdep, elapsed_jobs + outstanding_jobs);
    elapsed_jobs += in_area.total_jobs;
    // Early exit if no intersections and we've seen enough jobs in the pipeline
    if (elapsed_jobs > arch_max_blockdep)
      break;
  }

  return blockdep;
}

class HLCSNpuRegionLower
    : public PassWrapper<HLCSNpuRegionLower, FunctionPass> {
private:
  void lowerDmaCmd(mlir::Operation &cmd, CommandStreamEmitter &emit) {
    // Generates register commands for DMA operations
    int32_t src_region = get_i32_from_attr(cmd, "src_region");
    int32_t dest_region = get_i32_from_attr(cmd, "dest_region");
    int32_t src_address = get_i32_from_attr(cmd, "src_address");
    int32_t dest_address = get_i32_from_attr(cmd, "dest_address");
    int32_t size = get_i32_from_attr(cmd, "size");

    emit.cmd0_with_param(cmd0::NPU_SET_DMA0_SRC_REGION, src_region);
    emit.cmd1_with_offset(cmd1::NPU_SET_DMA0_SRC, src_address);
    emit.cmd0_with_param(cmd0::NPU_SET_DMA0_DST_REGION, dest_region);
    emit.cmd1_with_offset(cmd1::NPU_SET_DMA0_DST, dest_address);
    emit.cmd1_with_offset(cmd1::NPU_SET_DMA0_LEN, size);
  }

  void lowerWaitCmd(mlir::Operation &cmd, CommandStreamEmitter &emit) {
    // Generates KERNEL_WAIT/DMA_WAIT
    int32_t npu_outstanding_count =
        get_i32_from_attr(cmd, "npu_outstanding_count");
    int32_t dma_outstanding_count =
        get_i32_from_attr(cmd, "dma_outstanding_count");
    if (npu_outstanding_count >= 0) {
      emit.cmd_wait(e2i(cmd0::NPU_OP_KERNEL_WAIT), 0, npu_outstanding_count);
    }

    if (dma_outstanding_count >= 0) {
      emit.cmd_wait(e2i(cmd0::NPU_OP_DMA_WAIT), 0, dma_outstanding_count);
    }
  }

  void lowerYieldOp(mlir::Operation &cmd, CommandStreamEmitter &emit) {
    // only generate NPU_OP_STOP for the last YieldOp in NpuRegionOp
    // Fill in final part of command stream:
    emit.cmd_do_operation(cmd0::NPU_OP_STOP, 0xFFFF);
  }

  void lowerIssueCmd(mlir::Operation &cmd, CommandStreamEmitter &emit) {
    // generate_operation_code
    // Generates NPU_OP_* command

    NpuOperationType op_type =
        static_cast<NpuOperationType>(get_i32_from_attr(cmd, "op_type"));
    switch (op_type) {
    case NpuOperationType::Dma: {
      int32_t channel = get_i32_from_attr(cmd, "channel");
      uint32_t mode = get_i32_from_attr(cmd, "mode");
      emit.cmd_do_operation(cmd0::NPU_OP_DMA_START, channel * 16 + mode);
    } break;
    case NpuOperationType::Conv2D:
      emit.cmd_do_operation(cmd0::NPU_OP_CONV);
      break;
    case NpuOperationType::ConvDepthWise:
      emit.cmd_do_operation(cmd0::NPU_OP_DEPTHWISE);
      break;
    case NpuOperationType::Pooling: {
      int32_t sub_op_type = get_i32_from_attr(cmd, "sub_op_type");
      emit.cmd_do_operation(cmd0::NPU_OP_POOL, pooling_op_map[sub_op_type]);
    } break;
    case NpuOperationType::ElementWise: {
      int32_t sub_op_type = get_i32_from_attr(cmd, "sub_op_type");
      emit.cmd_do_operation(cmd0::NPU_OP_ELEMENTWISE,
                            elementwise_mode_map[sub_op_type]);
    } break;
    default:
      assert(!"unknown commands");
    }
  }

  void lowerRelocateCmd(mlir::Operation &cmd, mlir::OpBuilder &bldr,
                        mlir::Operation &npu_region_op) {}

  void lowerAddresses(CommandStreamEmitter &emit, std::vector<cmd1> &&ptr_cmds,
                      ivec &addresses, NpuLayout layout) {
    // Generates xFM_BASE registers
    if (layout == NpuLayout::NHCWB16) {
      // Check that all BasePointer addresses are aligned to 16 bytes
      for (int32_t addr : addresses)
        assert((addr & 0xF) == 0);
    }
    for (int i = 0; i < 4; i++) {
      emit.cmd1_with_offset(ptr_cmds[i], addresses[i]);
    }
  }

  void lowerTiles(CommandStreamEmitter &emit, std::vector<cmd0> &&tile_cmds,
                  ivec &&tiles) {
    // Generates xFM_HEIGHT0/HEIGHT1/WIDTH0 registers
    for (int i = 0; i < 3; i++) {
      // order: height_0, height_1, width_0
      emit.cmd0_with_param(tile_cmds[i], tiles[i] - 1);
    }
  }

  void lowerStrides(CommandStreamEmitter &emit, std::vector<cmd1> &&stride_cmds,
                    NpuShape3D &strides) {
    // Generates STRIDE_C/Y/X registers
    emit.cmd1_with_offset(
        stride_cmds[0],
        strides.depth); // stride between 16-byte channel blocks (C)
    emit.cmd1_with_offset(stride_cmds[1],
                          strides.height); // stride between vertical values (H)
    emit.cmd1_with_offset(
        stride_cmds[2], strides.width); // stride between horisontal values (W)
  }

  void lowerIFM2(CommandStreamEmitter &emit, mlir::Operation &ifm2,
                 bool has_scalar) {
    // Generates general IFM2 registers
    if (!has_scalar) {
      int32_t region = get_i32_from_attr(ifm2, "region");
      emit.cmd0_with_param(cmd0::NPU_SET_IFM2_REGION, region);

      ivec tiles_addresses = get_ivec_from_attr(ifm2, "tiles_addresses");
      NpuLayout layout =
          static_cast<NpuLayout>(get_i32_from_attr(ifm2, "layout"));
      lowerAddresses(emit,
                     {cmd1::NPU_SET_IFM2_BASE0, cmd1::NPU_SET_IFM2_BASE1,
                      cmd1::NPU_SET_IFM2_BASE2, cmd1::NPU_SET_IFM2_BASE3},
                     tiles_addresses, layout);

      int32_t height_0 = get_i32_from_attr(ifm2, "tiles_height_0");
      int32_t height_1 = get_i32_from_attr(ifm2, "tiles_height_1");
      int32_t width_0 = get_i32_from_attr(ifm2, "tiles_width_0");
      lowerTiles(emit,
                 {cmd0::NPU_SET_IFM2_HEIGHT0_M1, cmd0::NPU_SET_IFM2_HEIGHT1_M1,
                  cmd0::NPU_SET_IFM2_WIDTH0_M1},
                 {height_0, height_1, width_0});

      NpuShape3D strides(get_ivec_from_attr(ifm2, "strides"));
      lowerStrides(emit,
                   {cmd1::NPU_SET_IFM2_STRIDE_C, cmd1::NPU_SET_IFM2_STRIDE_Y,
                    cmd1::NPU_SET_IFM2_STRIDE_X},
                   strides);
    }
    int32_t ifm2_zero_point = get_i32_from_attr(ifm2, "zero_point");
    emit.cmd0_with_param(cmd0::NPU_SET_IFM2_ZERO_POINT, ifm2_zero_point);
  }

  void lowerIFM2Broadcast(CommandStreamEmitter &emit, mlir::Operation &cmd,
                          bool has_scalar) {
    // Generates IFM2_BROADCAST register for binary elementwise operations
    auto ifm_stripes = get_operands_from_variadic(cmd, "ifm_range");
    uint32_t ifm2_broadcast = 0;
    auto &ifm = *ifm_stripes[0];
    auto &ifm2 = *ifm_stripes[1];

    bool reversed_operands = get_bool_from_attr(cmd, "reversed_operands");
    if (reversed_operands) {
      ifm2_broadcast |= IFM2Broadcast::ReverseOperandOrder;
    }
    if (has_scalar) {
      // IFM2 is a constant, set UseIFM2Scalar bit to IFM2_BROADCAST
      ifm2_broadcast |= IFM2Broadcast::UseIFM2Scalar;
    } else {
      ivec ifm_shape = get_ivec_from_attr(ifm, "shape");
      ivec ifm2_shape = get_ivec_from_attr(ifm2, "shape");
      for (int d = 0; d < 3; d++) { // detect broadcast in 'HWC' dimensions
        if (ifm_shape[d] != ifm2_shape[d]) {
          assert(ifm2_shape[d] == 1);
          ifm2_broadcast |= (1 << d);
        }
      }
    }
    emit.cmd0_with_param(cmd0::NPU_SET_IFM2_BROADCAST, ifm2_broadcast);
  }

  void lowerIFM(CommandStreamEmitter &emit, mlir::Operation &ifm) {
    // Generates general IFM registers
    int32_t region = get_i32_from_attr(ifm, "region");
    emit.cmd0_with_param(cmd0::NPU_SET_IFM_REGION, region);

    ivec tiles_addresses = get_ivec_from_attr(ifm, "tiles_addresses");
    NpuLayout layout = static_cast<NpuLayout>(get_i32_from_attr(ifm, "layout"));
    lowerAddresses(emit,
                   {cmd1::NPU_SET_IFM_BASE0, cmd1::NPU_SET_IFM_BASE1,
                    cmd1::NPU_SET_IFM_BASE2, cmd1::NPU_SET_IFM_BASE3},
                   tiles_addresses, layout);

    int32_t height_0 = get_i32_from_attr(ifm, "tiles_height_0");
    int32_t height_1 = get_i32_from_attr(ifm, "tiles_height_1");
    int32_t width_0 = get_i32_from_attr(ifm, "tiles_width_0");
    lowerTiles(emit,
               {cmd0::NPU_SET_IFM_HEIGHT0_M1, cmd0::NPU_SET_IFM_HEIGHT1_M1,
                cmd0::NPU_SET_IFM_WIDTH0_M1},
               {height_0, height_1, width_0});

    ivec shape = get_ivec_from_attr(ifm, "shape");
    emit.cmd0_with_param(cmd0::NPU_SET_IFM_DEPTH_M1, shape[2] - 1);

    NpuShape3D strides(get_ivec_from_attr(ifm, "strides"));
    lowerStrides(emit,
                 {cmd1::NPU_SET_IFM_STRIDE_C, cmd1::NPU_SET_IFM_STRIDE_Y,
                  cmd1::NPU_SET_IFM_STRIDE_X},
                 strides);

    int32_t zero_point = get_i32_from_attr(ifm, "zero_point");
    emit.cmd0_with_param(cmd0::NPU_SET_IFM_ZERO_POINT, zero_point);
  }

  void lowerIFMPrecision(CommandStreamEmitter &emit, mlir::Operation &ifm,
                         int32_t op_to_scale, cmd0 precision_cmd) {
    // Generates IFM/IFM2_PRECISION register
    int32_t prec = 0;
    std::string dtype = get_str_from_attr(ifm, "data_type");
    if (dtype == "int8" || dtype == "int16" || dtype == "int32")
      prec = 1;
    int32_t bytes = size_in_bytes(dtype);
    int32_t activation_precision = 0;
    while (bytes & 0x1 == 0) {
      bytes >>= 1;
      activation_precision++;
    }
    prec += (activation_precision << 2);

    NpuLayout layout = static_cast<NpuLayout>(get_i32_from_attr(ifm, "layout"));
    if (layout == NpuLayout::NHCWB16)
      prec |= (1 << 6);
    prec |= op_to_scale << 8;
    emit.cmd0_with_param(precision_cmd, prec);
  }

  void lowerPadding(CommandStreamEmitter &emit, ivec &padding) {
    // Generates IFM_PAD registers
    emit.cmd0_with_param(cmd0::NPU_SET_IFM_PAD_TOP, padding[0]);
    emit.cmd0_with_param(cmd0::NPU_SET_IFM_PAD_LEFT, padding[1]);
    emit.cmd0_with_param(cmd0::NPU_SET_IFM_PAD_BOTTOM, padding[2]);
    emit.cmd0_with_param(cmd0::NPU_SET_IFM_PAD_RIGHT, padding[3]);
  }

  void lowerOFM(CommandStreamEmitter &emit, mlir::Operation &ofm) {
    // Generates general OFM registers
    int32_t region = get_i32_from_attr(ofm, "region");
    emit.cmd0_with_param(cmd0::NPU_SET_OFM_REGION, region);

    ivec tiles_addresses = get_ivec_from_attr(ofm, "tiles_addresses");
    NpuLayout layout = static_cast<NpuLayout>(get_i32_from_attr(ofm, "layout"));
    lowerAddresses(emit,
                   {cmd1::NPU_SET_OFM_BASE0, cmd1::NPU_SET_OFM_BASE1,
                    cmd1::NPU_SET_OFM_BASE2, cmd1::NPU_SET_OFM_BASE3},
                   tiles_addresses, layout);

    int32_t height_0 = get_i32_from_attr(ofm, "tiles_height_0");
    int32_t height_1 = get_i32_from_attr(ofm, "tiles_height_1");
    int32_t width_0 = get_i32_from_attr(ofm, "tiles_width_0");
    lowerTiles(emit,
               {cmd0::NPU_SET_OFM_HEIGHT0_M1, cmd0::NPU_SET_OFM_HEIGHT1_M1,
                cmd0::NPU_SET_OFM_WIDTH0_M1},
               {height_0, height_1, width_0});

    ivec shape = get_ivec_from_attr(ofm, "shape");
    emit.cmd0_with_param(cmd0::NPU_SET_OFM_HEIGHT_M1, shape[0] - 1);
    emit.cmd0_with_param(cmd0::NPU_SET_OFM_WIDTH_M1, shape[1] - 1);
    emit.cmd0_with_param(cmd0::NPU_SET_OFM_DEPTH_M1, shape[2] - 1);

    NpuShape3D strides(get_ivec_from_attr(ofm, "strides"));
    lowerStrides(emit,
                 {cmd1::NPU_SET_OFM_STRIDE_C, cmd1::NPU_SET_OFM_STRIDE_Y,
                  cmd1::NPU_SET_OFM_STRIDE_X},
                 strides);

    int32_t zero_point = get_i32_from_attr(ofm, "zero_point");
    emit.cmd0_with_param(cmd0::NPU_SET_OFM_ZERO_POINT, zero_point);
  }

  void lowerOFMPrecision(CommandStreamEmitter &emit, mlir::Operation &ofm,
                         bool use_global_scale, NpuRoundingMode rounding_mode) {
    // Generates OFM_PRECISION register
    int32_t prec = 0;
    std::string dtype = get_str_from_attr(ofm, "data_type");
    if (dtype == "int8" || dtype == "int16" || dtype == "int32")
      prec = 1;
    int32_t bytes = size_in_bytes(dtype);
    int32_t activation_precision = 0;
    while (bytes & 0x1 == 0) {
      bytes >>= 1;
      activation_precision++;
    }
    prec += (activation_precision << 1);

    // Set global scale bit, as opposed to using per channel scale
    if (use_global_scale)
      prec |= 1 << 8;

    NpuLayout layout = static_cast<NpuLayout>(get_i32_from_attr(ofm, "layout"));
    if (layout == NpuLayout::NHCWB16)
      prec |= (1 << 6);

    prec |= rounding_mode_map[e2i(rounding_mode)] << 14;
    emit.cmd0_with_param(cmd0::NPU_SET_OFM_PRECISION, prec);
  }

  void lowerKernel(CommandStreamEmitter &emit, mlir::Operation &cmd,
                   NpuBlockTraversal block_traversal) {
    // Generates KERNEL related registers
    Kernel kernel = to_kernel(cmd);

    emit.cmd0_with_param(cmd0::NPU_SET_KERNEL_HEIGHT_M1,
                         kernel.dilation.y * (kernel.height - 1));
    emit.cmd0_with_param(cmd0::NPU_SET_KERNEL_WIDTH_M1,
                         kernel.dilation.x * (kernel.width - 1));
    // set kernel x stride low bit
    int32_t stride = (kernel.stride.x - 1) & 1;
    // set kernel y stride low bit
    stride |= ((kernel.stride.y - 1) & 1) << 1;
    //  set kernel x stride extension bits
    stride |= ((kernel.stride.x - 1) >> 1) << 6;
    // set kernel y stride extension bits
    stride |= ((kernel.stride.y - 1) >> 1) << 9;
    stride |= (kernel.dilation.x - 1) << 3;
    stride |= (kernel.dilation.y - 1) << 4;
    if (block_traversal == NpuBlockTraversal::PART_KERNEL_FIRST)
      stride |= (1 << 2);
    emit.cmd0_with_param(cmd0::NPU_SET_KERNEL_STRIDE, stride);
  }

  const uint32_t arch_ncores = 1;
  void lowerWeights(CommandStreamEmitter &emit, ivecvec &weights) {
    // Generates WEIGHT registers"""
    if (weights.empty())
      return;
    emit.cmd0_with_param(cmd0::NPU_SET_WEIGHT_REGION, weights[0][0]);
    // Set weights sources for active and present cores
    static std::vector<pair<cmd1, cmd1>> regs = {
        {cmd1::NPU_SET_WEIGHT_BASE, cmd1::NPU_SET_WEIGHT_LENGTH},
        {cmd1::NPU_SET_WEIGHT1_BASE, cmd1::NPU_SET_WEIGHT1_LENGTH},
    };
    for (int core = 0; core < regs.size(); ++core) {
      if (core < weights.size()) {
        emit.cmd1_with_offset(regs[core].first, weights[core][1]);
        emit.cmd1_with_offset(regs[core].second, weights[core][2]);
      } else if (core < arch_ncores) {
        emit.cmd1_with_offset(regs[core].first, weights[0][1]);
        emit.cmd1_with_offset(regs[core].second, 0);
      }
    }
  }

  void lowerBiases(CommandStreamEmitter &emit, ivecvec &biases) {
    // Generates SCALE registers
    if (biases.empty())
      return;
    emit.cmd0_with_param(cmd0::NPU_SET_SCALE_REGION, biases[0][0]);
    // Set weights sources for active and present cores
    static std::vector<pair<cmd1, cmd1>> regs = {
        {cmd1::NPU_SET_SCALE_BASE, cmd1::NPU_SET_SCALE_LENGTH},
        {cmd1::NPU_SET_SCALE1_BASE, cmd1::NPU_SET_SCALE1_LENGTH},
    };
    for (int core = 0; core < regs.size(); ++core) {
      if (core < biases.size()) {
        emit.cmd1_with_offset(regs[core].first, biases[core][1]);
        emit.cmd1_with_offset(regs[core].second, biases[core][2]);
      } else if (core < arch_ncores) {
        emit.cmd1_with_offset(regs[core].first, biases[0][1]);
        emit.cmd1_with_offset(regs[core].second, 0);
      }
    }
  }

  void lowerActivation(CommandStreamEmitter &emit, mlir::Operation &cmd,
                       mlir::Operation &ofm) {
    // Generates ACTIVATION registers
    NpuActivation act(NpuActivationOp::NONE_OR_RELU);
    std::string data_type = get_str_from_attr(ofm, "data_type");
    float scale_f32 = get_f32_from_attr(ofm, "scale_f32");
    int32_t zero_point = get_i32_from_attr(ofm, "zero_point");

    std::string faf = get_str_from_attr(cmd, "activation_op_type");
    if (!faf.empty()) {
      if (faf == "Tanh")
        act.op_type = NpuActivationOp::TANH;
      else if (faf == "Sigmoid")
        act.op_type = NpuActivationOp::SIGMOID;
      else if (faf == "LUT")
        act.op_type = NpuActivationOp::TABLE_LOOKUP;
      else if (faf != "Relu" && faf != "Relu6" && faf != "ReluN1To1" &&
               faf != "Clip")
        assert(!"Unsupported fused_activation_function");

      act.min = get_f32_from_attr(cmd, "activation_min");
      act.max = get_f32_from_attr(cmd, "activation_max");
      act.lut_index = get_i32_from_attr(cmd, "activation_lut_index");
    }

    int32_t activation_value = 0;
    int32_t quantized_min = 0;
    int32_t quantized_max = 0;

    if (std::isnan(act.min))
      quantized_min = min_value(data_type);
    else
      quantized_min = quantise_float32(act.min, scale_f32, zero_point);

    if (std::isnan(act.max))
      quantized_max = max_value(data_type);
    else
      quantized_max = quantise_float32(act.max, scale_f32, zero_point);

    quantized_min = max(quantized_min, min_value(data_type));
    quantized_min = max(quantized_min, min_value("int16"));
    quantized_max = min(quantized_max, max_value(data_type));
    quantized_max = min(quantized_max, max_value("int16"));

    if (act.op_type == NpuActivationOp::TABLE_LOOKUP) {
      assert(0 <= act.lut_index && act.lut_index < 8);
      activation_value = 16 + act.lut_index;
      if (data_type == "int32") {
        activation_value |= 3 << 12; // force I8 range
        quantized_min = max(-128, quantized_min);
        quantized_max = min(127, quantized_max);
      }
    } else {
      activation_value = activation_op_map[e2i(act.op_type)];
    }
    emit.cmd0_with_param(cmd0::NPU_SET_ACTIVATION, activation_value);
    emit.cmd0_with_param(cmd0::NPU_SET_ACTIVATION_MIN, quantized_min);
    emit.cmd0_with_param(cmd0::NPU_SET_ACTIVATION_MAX, quantized_max);
  }

  void
  lowerBlockConfig(CommandStreamEmitter &emit,
                   mlir::Operation &cmd /*, SharedBufferAllocation& sba*/) {
    NpuShape3D block_config(get_ivec_from_attr(cmd, "block_config"));
    emit.cmd0_with_param(cmd0::NPU_SET_OFM_BLK_HEIGHT_M1,
                         block_config.height - 1);
    emit.cmd0_with_param(cmd0::NPU_SET_OFM_BLK_WIDTH_M1,
                         block_config.width - 1);
    emit.cmd0_with_param(cmd0::NPU_SET_OFM_BLK_DEPTH_M1,
                         block_config.depth - 1);
  }

  void lowerShramRegistersElementwise(CommandStreamEmitter &emit,
                                      mlir::Operation &cmd) {
    // Generates IB_END/IB_START/AB_START registers for elementwise operations
    // For elementwise set the required SHRAM to be equal to the total size of
    // available SHRAM
    NpuActivationOp act_op_type = static_cast<NpuActivationOp>(
        get_i32_from_attr(cmd, "activation_op_type"));
    bool uses_lut = (act_op_type == NpuActivationOp::TABLE_LOOKUP);
    // TODO: support lookup table
    uint32_t shram_required = arch_available_shram_banks(uses_lut);
    auto &kernel = *cmd.getOperand(0).getDefiningOp();

    // Acc buffers not needed so set AB_START to size of SHRAM
    emit.cmd0_with_param(cmd0::NPU_SET_IFM_IB_END, shram_required);
    emit.cmd0_with_param(cmd0::NPU_SET_AB_START, shram_required);

    auto ifm_stripes = get_operands_from_variadic(cmd, "ifm_range");
    if (ifm_stripes.size() == 2) {
      // Set IFM2_IB_START to the latter half of the IB space
      ivec shared_buffer_bank_locations =
          get_ivec_from_attr(kernel, "shared_buffer_banks_locations");
      int32_t shared_buffer_ifm_count =
          get_i32_from_attr(kernel, "shared_buffer_ifm_count");
      int32_t ifm_ib_start =
          shared_buffer_bank_locations[e2i(SharedBufferArea::IFM)];
      emit.cmd0_with_param(cmd0::NPU_SET_IFM2_IB_START,
                           (shram_required - ifm_ib_start) /
                                   shared_buffer_ifm_count +
                               ifm_ib_start);
    }

    int32_t shared_buffer_use_accumulator_element =
        get_i32_from_attr(kernel, "shared_buffer_use_accumulator_element");
    emit.cmd0_with_param(cmd0::NPU_SET_ACC_FORMAT,
                         acc_format_map[shared_buffer_use_accumulator_element]);
  }

  void lowerShramRegistersNonElementwise(CommandStreamEmitter &emit,
                                         mlir::Operation &cmd) {
    auto &kernel = *cmd.getOperand(0).getDefiningOp();
    ivec shared_buffer_bank_locations =
        get_ivec_from_attr(kernel, "shared_buffer_banks_locations");
    ivec shared_buffer_banks_required =
        get_ivec_from_attr(kernel, "shared_buffer_banks_required");
    int32_t shared_buffer_use_accumulator_element =
        get_i32_from_attr(kernel, "shared_buffer_use_accumulator_element");
    // Generates IB_END/IB_START/AB_START registers for non-elementwise
    // operations
    emit.cmd0_with_param(
        cmd0::NPU_SET_IFM_IB_END,
        shared_buffer_bank_locations[e2i(SharedBufferArea::IFM)] +
            shared_buffer_banks_required[e2i(SharedBufferArea::IFM)]);
    emit.cmd0_with_param(
        cmd0::NPU_SET_AB_START,
        shared_buffer_bank_locations[e2i(SharedBufferArea::Accumulators)]);
    emit.cmd0_with_param(cmd0::NPU_SET_ACC_FORMAT,
                         acc_format_map[shared_buffer_use_accumulator_element]);
  }

  uint32_t lowerScalingForElementwise(CommandStreamEmitter &emit,
                                      mlir::Operation &cmd) {
    mlir::hlcs::KernelInfoOp hlcs_kernel_op =
        cast<mlir::hlcs::KernelInfoOp>(cmd.getOperand(0).getDefiningOp());

    StringAttr frontend_rescale_mode =
        hlcs_kernel_op.frontend_rescale_modeAttr();

    // Generates OFM/OPA/OPB_SCALE registers for elementwise operators.
    // Returns the operator to scale
    uint32_t op_to_scale = 0;

    int32_t input0_zp = hlcs_kernel_op.input0_zpAttr().getInt();
    int32_t input0_multiplier = hlcs_kernel_op.input0_multiplierAttr().getInt();
    int32_t input0_shift = hlcs_kernel_op.input0_shiftAttr().getInt();
    int32_t input1_zp = hlcs_kernel_op.input1_zpAttr().getInt();
    int32_t input1_multiplier = hlcs_kernel_op.input1_multiplierAttr().getInt();
    int32_t input1_shift = hlcs_kernel_op.input1_shiftAttr().getInt();
    int32_t output_zp = hlcs_kernel_op.output_zpAttr().getInt();
    ArrayAttr output_multiplier_attr = hlcs_kernel_op.output_multiplierAttr();
    ArrayAttr output_shift_attr = hlcs_kernel_op.output_shiftAttr();
    int32_t output_multiplier =
        output_multiplier_attr.getValue().front().cast<IntegerAttr>().getInt();
    int32_t output_shift =
        output_shift_attr.getValue().front().cast<IntegerAttr>().getInt();

    NpuElementWiseOp sub_op_type =
        static_cast<NpuElementWiseOp>(get_i32_from_attr(cmd, "sub_op_type"));
    if ((sub_op_type == NpuElementWiseOp::ADD) ||
        (sub_op_type == NpuElementWiseOp::SUB)) {

      ASSERT_COND(frontend_rescale_mode.getValue().str() !=
                      std::string("ELEMENTWISE_ADDSUB"),
                  "lowerScalingForElementwise(): rescale mode must be "
                  "ELEMENTWISE_ADDSUB.");

      uint32_t opa_scale = 0, opb_scale = 0;
      int32_t opa_shift = 0;

      // Add / Sub
      if (input0_multiplier == input1_multiplier &&
          input0_shift == input1_shift) {
        opa_scale = input0_multiplier;
        opa_shift = input0_shift;
        opb_scale = input1_multiplier;
      } else {
        bool input0_no_rescale =
            (input0_multiplier == (1 << 30) && input0_shift == 11) ? true
                                                                   : false;
        bool input1_no_rescale =
            (input1_multiplier == (1 << 30) && input1_shift == 11) ? true
                                                                   : false;

        ASSERT_COND(not(input0_no_rescale ^ input1_no_rescale),
                    "lowerScalingForElementwise(): Can only scale one "
                    "input when binary op input scales are different");

        if (input0_no_rescale) {
          // input1 needs to be rescaled
          op_to_scale = static_cast<uint32_t>(OperandToScale::OPb);
          opa_scale = input1_multiplier;
          opa_shift = input1_shift;
        } else {
          // input0 needs to be rescaled
          op_to_scale = static_cast<uint32_t>(OperandToScale::OPa);
          opa_scale = input0_multiplier;
          opa_shift = input0_shift;
        }

        bool reversed_operands = get_bool_from_attr(cmd, "reversed_operands");
        if (reversed_operands) {
          // If the operand order is reversed we also have to swap which
          // operand is scaled
          if (op_to_scale == OperandToScale::OPa)
            op_to_scale = OperandToScale::OPb;
          else
            op_to_scale = OperandToScale::OPa;
        }
      }
      emit.cmd1_with_offset(cmd1::NPU_SET_OPA_SCALE, opa_scale, opa_shift);
      emit.cmd1_with_offset(cmd1::NPU_SET_OPB_SCALE, opb_scale);
      emit.cmd1_with_offset(cmd1::NPU_SET_OFM_SCALE, output_multiplier,
                            output_shift);
    } else if (sub_op_type == NpuElementWiseOp::MUL) {
      emit.cmd1_with_offset(cmd1::NPU_SET_OFM_SCALE, output_multiplier,
                            output_shift);
    } else if ((sub_op_type == NpuElementWiseOp::LRELU) ||
               (sub_op_type == NpuElementWiseOp::ABS)) {
      emit.cmd1_with_offset(cmd1::NPU_SET_OFM_SCALE, output_multiplier,
                            output_shift);
    } else {
      emit.cmd1_with_offset(cmd1::NPU_SET_OFM_SCALE, 1, 0);
    }
    return op_to_scale;
  }

  void lowerOFMScalingForPooling(CommandStreamEmitter &emit,
                                 mlir::Operation &cmd) {
    // Generates OFM_SCALE register for pooling operations
    // For valid padding other compilers have to output scaling values
    // auto& kernel = *cmd.getOperand(0).getDefiningOp();
    Kernel kernel = to_kernel(cmd);

    auto ifm_stripes = get_operands_from_variadic(cmd, "ifm_range");
    assert(ifm_stripes.size() == 1);
    auto &ifm_stripe_mem = *ifm_stripes[0];
    int32_t ifm_zero_point = get_i32_from_attr(ifm_stripe_mem, "zero_point");
    float ifm_scale_f32 = get_f32_from_attr(ifm_stripe_mem, "scale_f32");

    auto ofm_stripes = get_operands_from_variadic(cmd, "ofm_range");
    assert(ofm_stripes.size() == 1);
    auto &ofm_stripe_mem = *ofm_stripes[0];
    int32_t ofm_zero_point = get_i32_from_attr(ofm_stripe_mem, "zero_point");
    float ofm_scale_f32 = get_f32_from_attr(ofm_stripe_mem, "scale_f32");

    NpuActivationOp act_op_type = static_cast<NpuActivationOp>(
        get_i32_from_attr_with_default(cmd, "activation_op_type", 0));
    bool fused_quantize =
        get_bool_from_attr_with_default(cmd, "fused_quantize", false);

    uint32_t scale = 1;
    int32_t shift = 0;
    if ((act_op_type == NpuActivationOp::SIGMOID) ||
        (act_op_type == NpuActivationOp::TANH)) {
      float rescale = 0x3000 * ifm_scale_f32;
      string ifm_data_type = get_str_from_attr(ifm_stripe_mem, "data_type");
      if (ifm_data_type == "int16") {
        // Calculate scale and shift for the output scale of 1/(3*4096)
        uint32_t ushift = 0;
        static const uint32_t max_rescale = SHRT_MAX / 2;
        while ((rescale <= max_rescale) && (ushift <= 30)) {
          ++ushift;
          rescale *= 2;
        }
        scale = static_cast<uint32_t>(rescale);
        shift = static_cast<int32_t>(ushift);
      } else {
        int32_t tmp = static_cast<int32_t>(std::ceil(rescale));
        int rescale_bits = (32 - __builtin_clz(tmp)) + 1;
        int64_t scale_64 = 0;
        quantise_pooling_scale(kernel.height * kernel.width, rescale_bits,
                               scale_64, shift);
        scale = static_cast<uint32_t>(std::round(scale_64 * 1.0 * rescale));
      }
    } else if (fused_quantize) {
      // Quantize op requires different scaling
      quantise_scale(ifm_scale_f32 * 1.0 / ofm_scale_f32, scale, shift);
      /*
      } else if (rescale) {
          // for ResizeBilinear operations with "rescale" in primary_op.attrs
          rescale = pool_op.rescale
          rescale_bits = len(bin(round_up_to_int(rescale))) - 2 + 1
          scale, shift = scaling.quantise_pooling_scale(kernel.height *
      kernel.width, rescale_bits) scale = int(round_away_zero(scale * rescale))
      */
    } else {
      // In case avg pool fused with concat or other memory operation, rescaling
      // might be needed. kernel height == kernel width == 1 is always true in
      // this case Normally the scale is maximised, to get maximum precision,
      // which means that if rescale != 1, scale need to consider the number of
      // bits needed for rescaling
      float rescale = ifm_scale_f32 / ofm_scale_f32;
      int rescale_bits = 0;
      if ((kernel.width == 1) && (kernel.height == 1)) {
        if (rescale > 1.0) {
          int32_t tmp = static_cast<int32_t>(std::ceil(rescale));
          rescale_bits = (32 - __builtin_clz(tmp)) + 1;
        } else if (rescale < 1.0) {
          int32_t tmp = static_cast<int32_t>(std::ceil(1.0 / rescale));
          rescale_bits = -(32 - __builtin_clz(tmp)) + 1;
        }
      }
      int64_t scale_64 = 0;
      quantise_pooling_scale(kernel.height * kernel.width, rescale_bits,
                             scale_64, shift);
      scale = static_cast<uint32_t>(std::round(scale_64 * 1.0 * rescale));
    }
    emit.cmd1_with_offset(cmd1::NPU_SET_OFM_SCALE, scale, shift);
  }

  void lowerCommon(mlir::Operation &cmd, CommandStreamEmitter &emit,
                   NpuBlockTraversal traversal, bool use_global_scale = false,
                   int32_t op_to_scale = 0) {
    // Generate registers that are common to most operations
    auto ifm_stripes = get_operands_from_variadic(cmd, "ifm_range");
    auto &ifm_stripe_mem = *ifm_stripes[0];
    lowerIFM(emit, ifm_stripe_mem);
    lowerIFMPrecision(emit, ifm_stripe_mem, op_to_scale,
                      cmd0::NPU_SET_IFM_PRECISION);

    int32_t ifm_upscale = get_i32_from_attr(cmd, "ifm_upscale");
    emit.cmd0_with_param(cmd0::NPU_SET_IFM_UPSCALE,
                         resampling_mode_map[ifm_upscale]);

    auto attr = cmd.getAttrOfType<ArrayAttr>("padding");
    if (attr) {
      ivec padding = get_ivec_from_attr(cmd, "padding");
      lowerPadding(emit, padding);
    }

    auto ofm_stripes = get_operands_from_variadic(cmd, "ofm_range");
    assert(ofm_stripes.size() > 0);
    auto &ofm_stripe_mem = *ofm_stripes[0];
    lowerOFM(emit, ofm_stripe_mem);

    NpuRoundingMode rounding_mode =
        static_cast<NpuRoundingMode>(get_i32_from_attr(cmd, "rounding_mode"));
    lowerOFMPrecision(emit, ofm_stripe_mem, use_global_scale, rounding_mode);

    NpuOperationType op_type =
        static_cast<NpuOperationType>(get_i32_from_attr(cmd, "op_type"));
    if (op_type != NpuOperationType::ElementWise) {
      lowerKernel(emit, cmd, traversal);
    }

    ivecvec weights = get_ivecvec_from_attr(cmd, "weights");
    lowerWeights(emit, weights);

    ivecvec biases = get_ivecvec_from_attr(cmd, "biases");
    lowerBiases(emit, biases);

    lowerActivation(emit, cmd, ofm_stripe_mem);

    // SharedBufferAllocation shared_buffer = create_shared_buffer(cmd);
    lowerBlockConfig(emit, cmd /*, shared_buffer */);

    if (op_type == NpuOperationType::ElementWise)
      lowerShramRegistersElementwise(emit, cmd /*, shared_buffer*/);
    else
      lowerShramRegistersNonElementwise(emit, cmd /*, shared_buffer*/);
  }
  void lowerConv2DCmd(mlir::Operation &cmd, CommandStreamEmitter &emit) {
    // Generates register commands for Conv2D operations
    NpuBlockTraversal traversal = static_cast<NpuBlockTraversal>(
        get_i32_from_attr(cmd, "block_traversal"));
    lowerCommon(cmd, emit, traversal);
  }
  void lowerConvDepthwiseCmd(mlir::Operation &cmd, CommandStreamEmitter &emit) {
    // Generates register commands for depthwise convolution operations
    lowerCommon(cmd, emit, NpuBlockTraversal::DEPTH_FIRST);
  }
  void lowerPoolingCmd(mlir::Operation &cmd, CommandStreamEmitter &emit) {
    // Generates register commands for pooling operations
    NpuPoolingOp sub_op_type =
        static_cast<NpuPoolingOp>(get_i32_from_attr(cmd, "sub_op_type"));
    ivec padding = get_ivec_from_attr(cmd, "padding");
    int32_t sum = std::accumulate(padding.begin(), padding.end(), 0);

    bool use_global_scale =
        (sum == 0 && (sub_op_type == NpuPoolingOp::AVERAGE ||
                      sub_op_type == NpuPoolingOp::REDUCE_SUM));
    lowerCommon(cmd, emit, NpuBlockTraversal::DEPTH_FIRST, use_global_scale);
    if (use_global_scale) {
      lowerOFMScalingForPooling(emit, cmd);
    }
  }
  void lowerElementwiseCmd(mlir::Operation &cmd, CommandStreamEmitter &emit) {
    // Generates register commands for elementwise operations
    NpuElementWiseOp sub_op_type =
        static_cast<NpuElementWiseOp>(get_i32_from_attr(cmd, "sub_op_type"));
    bool use_global_scale = ((sub_op_type == NpuElementWiseOp::ADD) ||
                             (sub_op_type == NpuElementWiseOp::SUB) ||
                             (sub_op_type == NpuElementWiseOp::MUL) ||
                             (sub_op_type == NpuElementWiseOp::LRELU) ||
                             (sub_op_type == NpuElementWiseOp::ABS));
    int32_t op_to_scale = lowerScalingForElementwise(emit, cmd);
    lowerCommon(cmd, emit, NpuBlockTraversal::DEPTH_FIRST, use_global_scale,
                op_to_scale);

    // Elementwise op specific
    if (!is_unary_elementwise_op(sub_op_type)) {
      // Binary operation; generate IFM2 registers
      auto ifm_stripes = get_operands_from_variadic(cmd, "ifm_range");
      assert(ifm_stripes.size() == 2);
      auto &ifm2_stripe_mem = *ifm_stripes[1];
      Operation *op = ifm2_stripe_mem.getOperand(0).getDefiningOp();
      assert(llvm::isa<hlcs::TensorInfoOp>(*op));
      hlcs::TensorInfoOp tensor_info_op = cast<hlcs::TensorInfoOp>(*op);
      Optional<bool> broadcast_scalar_value_valid =
          tensor_info_op.broadcast_scalar_value_valid();
      Optional<unsigned int> broadcast_scalar_value =
          tensor_info_op.broadcast_scalar_value();
      bool has_scalar = false;
      // Check against optional attribute exists
      if (broadcast_scalar_value_valid && broadcast_scalar_value)
        has_scalar = broadcast_scalar_value_valid.getValue();
      lowerIFM2(emit, ifm2_stripe_mem, has_scalar);
      lowerIFMPrecision(emit, ifm2_stripe_mem, 0, cmd0::NPU_SET_IFM2_PRECISION);
      lowerIFM2Broadcast(emit, cmd, has_scalar);
      if (has_scalar) {
        int32_t quantized_scalar = broadcast_scalar_value.getValue();
        emit.cmd0_with_param(cmd0::NPU_SET_IFM2_SCALAR, quantized_scalar);
      }
    }
  }

  void lowerNpuRegionOp(mlir::Operation &npu_region_op) {
    CommandStreamEmitter emit;
    MLIRContext &context = getContext();
    OpBuilder builder(&context);
    // TODO:
    // if arch.is_u65_system:
    //    emit.cmd0_with_param(cmd0.NPU_SET_PARALLEL_MODE, arch.ncores - 1)

    mlir::Block &block = npu_region_op.getRegion(0).getBlocks().front();
    assert(block.hasNoSuccessors());
    for (mlir::Operation &cmdlist : block.getOperations()) {
      if (llvm::isa<hlcs::TensorInfoOp>(cmdlist))
        continue;
      if (llvm::isa<hlcs::YieldOp>(cmdlist)) {
        lowerYieldOp(cmdlist, emit);
        continue;
      }
      if (llvm::isa<hlcs::RelocateCmd>(cmdlist)) {
        lowerRelocateCmd(cmdlist, builder, npu_region_op);
        continue;
      }
      assert(llvm::isa<hlcs::CommandListOp>(cmdlist));
      mlir::Block &bb = cmdlist.getRegion(0).getBlocks().front();
      assert(bb.hasNoSuccessors());
      for (mlir::Operation &cmd : bb.getOperations()) {
        if (llvm::isa<hlcs::DmaCmd>(cmd)) {
          issuing_cmd = &cmd;
          lowerDmaCmd(cmd, emit);
        } else if (llvm::isa<hlcs::WaitCmd>(cmd)) {
          lowerWaitCmd(cmd, emit);
        } else if (llvm::isa<hlcs::IssueCmd>(cmd)) {
          assert(issuing_cmd);
          lowerIssueCmd(*issuing_cmd, emit);
        } else if (llvm::isa<hlcs::NpuStripeCmd>(cmd)) {
          issuing_cmd = &cmd;
          NpuOperationType op_type =
              static_cast<NpuOperationType>(get_i32_from_attr(cmd, "op_type"));
          switch (op_type) {
          case NpuOperationType::Conv2D:
            lowerConv2DCmd(cmd, emit);
            break;
          case NpuOperationType::ConvDepthWise:
            lowerConvDepthwiseCmd(cmd, emit);
            break;
          case NpuOperationType::Pooling:
            lowerPoolingCmd(cmd, emit);
            break;
          case NpuOperationType::ElementWise:
            lowerElementwiseCmd(cmd, emit);
            break;
          default:
            assert(!"unknown op_type");
          }

          // Generate BLOCKDEP
          int32_t blockdep = calc_blockdep(prev_cmd, &cmd);
          // printf("blockdep: %d %d\n", blockdep,
          // emit._get_cmd_stream().size());
          blockdep = std::min(blockdep, arch_max_blockdep);
          emit.cmd0_with_param(cmd0::NPU_SET_BLOCKDEP, blockdep);
          prev_cmd = &cmd;
        }
      }
    }

    std::vector<int64_t> &cmd_stream = emit._get_cmd_stream();
    if (generate_register_stream) {
      size_t len = input_filename.size();
      std::string register_stream_filename =
          input_filename.substr(0, len - 5) + "_registers_tirc.txt";
      static bool first_npu_op = true;
      if (first_npu_op) {
        first_npu_op = false;
        std::remove(register_stream_filename.c_str());
      }
      emit.print_cmds(register_stream_filename);
    }

    ArrayAttr cmd_stream_attr =
        builder.getI64ArrayAttr(ArrayRef<int64_t>(cmd_stream));
    npu_region_op.setAttr(Identifier::get("reg_stream", &context),
                          cmd_stream_attr);
  }

private:
  mlir::Operation *prev_cmd = nullptr;
  mlir::Operation *issuing_cmd = nullptr;

public:
  HLCSNpuRegionLower() = default;

  StringRef getArgument() const final {
    // This is the argument used to refer to the pass in
    // the textual format (on the commandline for example).
    return PASS_NAME;
  }
  StringRef getDescription() const final {
    // This is a brief description of the pass.
    return "Lower HLCS NpuRegionOp to LLCS";
  }

  void runOnFunction() override {
    auto func = getFunction();
    auto *ctx = &getContext();
    if (func.getName() != "main")
      return;
    mlir::Region *region = func.getCallableRegion();
    for (mlir::Block &bb : region->getBlocks()) {

      for (mlir::Operation &op : bb.getOperations()) {
        if (!llvm::isa<hlcs::NpuRegionOp>(op))
          continue;
        lowerNpuRegionOp(op);
      }
    }
  }
};

} // anonymous namespace

std::unique_ptr<OperationPass<FuncOp>> CreateHLCSNpuRegionLowerPass() {
  return absl::make_unique<HLCSNpuRegionLower>();
}

static PassRegistration<HLCSNpuRegionLower> pass;
} // namespace hlcs
} // namespace mlir

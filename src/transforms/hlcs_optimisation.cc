/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#include "absl/memory/memory.h"
#include "mlir/Dialect/StandardOps/IR/Ops.h"      // from @llvm-project
#include "mlir/IR/AffineMap.h"                    // from @llvm-project
#include "mlir/IR/Attributes.h"                   // from @llvm-project
#include "mlir/IR/BlockAndValueMapping.h"         // from @llvm-project
#include "mlir/IR/Builders.h"                     // from @llvm-project
#include "mlir/IR/Location.h"                     // from @llvm-project
#include "mlir/IR/MLIRContext.h"                  // from @llvm-project
#include "mlir/IR/Operation.h"                    // from @llvm-project
#include "mlir/IR/PatternMatch.h"                 // from @llvm-project
#include "mlir/Interfaces/InferTypeOpInterface.h" // from @llvm-project
#include "mlir/Pass/Pass.h"                       // from @llvm-project
#include "mlir/Transforms/DialectConversion.h"    // from @llvm-project

#include "src/ir/hlcs_ir.h"

#include "src/transforms/llcs_generator/hlcs_mlir_utils.h"
#include "src/transforms/llcs_generator/hlcs_npu_api.h"

using namespace mlir;

#define PASS_NAME "hlcs-optimisation"
#define DEBUG_TYPE PASS_NAME

using namespace std;

static std::vector<int64_t>
conv_i32vec_to_i64vec(std::vector<int32_t> &i32vec) {
  std::vector<int64_t> i64vec;
  for (const int &i : i32vec)
    i64vec.push_back(i);
  return std::move(i64vec);
}

namespace mlir {

namespace hlcs {

namespace {

/* Alot of the pass functionality can be reomved */

void correct_shape_and_coords_for_concat(OpBuilder &bldr,
                                         mlir::Operation &func_op) {
  func_op.walk([&](mlir::Operation *op) {
    mlir::Operation &cmd = *op;
    // match
    if (!llvm::isa<hlcs::NpuStripeCmd>(cmd))
      return;
    int32_t concat_axis = get_i32_from_attr(cmd, "concat_axis");
    if (3 != concat_axis)
      return;

    // update start_coord and end_coord
    int32_t concat_offset = get_i32_from_attr(cmd, "concat_offset");
    auto ofms = get_operands_from_variadic(cmd, "ofm_range");
    assert(ofms.size() == 1);
    mlir::Operation &ofm = *ofms[0];
    ivec start_coord = get_ivec_from_attr(ofm, "start_coord");
    ivec end_coord = get_ivec_from_attr(ofm, "end_coord");
    start_coord[concat_axis] += concat_offset;
    end_coord[concat_axis] += concat_offset;
    auto s_vec = conv_i32vec_to_i64vec(start_coord);
    auto e_vec = conv_i32vec_to_i64vec(end_coord);
    auto attr = bldr.getI64ArrayAttr(ArrayRef<int64_t>(s_vec));
    ofm.setAttr(Identifier::get("start_coord", bldr.getContext()), attr);
    attr = bldr.getI64ArrayAttr(ArrayRef<int64_t>(e_vec));
    ofm.setAttr(Identifier::get("end_coord", bldr.getContext()), attr);

    // update ofm's shape in tensor_info, align shape with storage_shape
    mlir::Operation &tensor_info = *ofm.getOperand(0).getDefiningOp();
    auto storage_shape = get_ivec_from_attr(tensor_info, "storage_shape");
    set_ivec_to_attr(bldr, tensor_info, "shape", storage_shape);
  });
}

void correct_storage_shape_for_scale_tensor(OpBuilder &bldr,
                                            mlir::Operation &func_op) {
  mlir::Region &region = func_op.getRegions().front();
  assert(region.getBlocks().size() == 1);
  mlir::Block &block = region.getBlocks().front();
  for (mlir::Operation &region_op : block.getOperations()) {
    if (!llvm::isa<hlcs::NpuRegionOp>(region_op))
      continue;
    mlir::Block &region_block =
        region_op.getRegions().front().getBlocks().front();
    for (mlir::Operation &op : region_block.getOperations()) {
      if (!llvm::isa<hlcs::TensorInfoOp>(op))
        continue;
      TensorPurpose purpose =
          static_cast<TensorPurpose>(get_i32_from_attr(op, "purpose"));
      if (purpose != TensorPurpose::FSBias)
        continue;
      int32_t bytes = get_i32_from_attr(op, "element_size_bytes");
      const int32_t FSB_STRIDE =
          10; // 5 bytes bias, 4 bytes scale, and 1 bytes shift
      if (bytes == FSB_STRIDE)
        continue;
      assert(bytes == 4);
      set_i32_to_attr(bldr, op, "element_size_bytes", FSB_STRIDE);
      ivec storage_shape = get_ivec_from_attr(op, "storage_shape");
      assert((storage_shape.size() == 1) &&
             ((storage_shape[0] % FSB_STRIDE) == 0));
      storage_shape[0] /= FSB_STRIDE;
      set_ivec_to_attr(bldr, op, "storage_shape", storage_shape);
    }
  }
}

void set_label_for_region_op(OpBuilder &bldr, mlir::Operation &func_op) {
  mlir::Region &region = func_op.getRegions().front();
  assert(region.getBlocks().size() == 1);
  mlir::Block &block = region.getBlocks().front();
  int32_t split_index = 0;
  for (mlir::Operation &op : block.getOperations()) {
    if (llvm::isa<hlcs::NpuRegionOp>(op)) {
      std::string label = "_split_" + std::to_string(++split_index) + "_";
      set_str_to_attr(bldr, op, "label", label);
    } else if (llvm::isa<hlcs::CallNpuRegionCmd>(op)) {
      mlir::Operation &region_op = *op.getOperand(0).getDefiningOp();
      std::string label = get_str_from_attr(region_op, "label");
      assert(!label.empty());
      set_str_to_attr(bldr, op, "label", label);
    }
  }
}

class HLCSOptimisation : public PassWrapper<HLCSOptimisation, FunctionPass> {
public:
  HLCSOptimisation() = default;

  StringRef getArgument() const final {
    // This is the argument used to refer to the pass in
    // the textual format (on the commandline for example).
    return PASS_NAME;
  }
  StringRef getDescription() const final {
    // This is a brief description of the pass.
    return "HLCS Optimisation";
  }

  void runOnFunction() override {
    auto func = getFunction();
    if (func.getName() != "main")
      return;
    // printf("subpass: HLCSOptimisation\n");
    MLIRContext *context = func.getContext();
    OpBuilder bldr(context);

    Operation &func_op = *func.getOperation();

    correct_shape_and_coords_for_concat(bldr, func_op);
    correct_storage_shape_for_scale_tensor(bldr, func_op);
    set_label_for_region_op(bldr, func_op);
    // func.dump();
  }
};

} // anonymous namespace

std::unique_ptr<OperationPass<FuncOp>> CreateHLCSOptimisationPass() {
  return absl::make_unique<HLCSOptimisation>();
}

static PassRegistration<HLCSOptimisation> pass;
} // namespace hlcs
} // namespace mlir

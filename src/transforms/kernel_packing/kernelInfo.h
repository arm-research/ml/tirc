/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#ifndef KERNEL_INFO_H
#define KERNEL_INFO_H

#include "src/ir/kernel_ir.h"
#include "src/utils/dag.h"

namespace mlir {
namespace planner {

class kernelInfo {
public:
  std::vector<OP_INDEX> ops;
  std::vector<OP_INDEX> ops_const;
  std::string name;

  SmallVector<Value, 1> weights;
  SmallVector<Value, 1> bias;
  SmallVector<Value, 1> inputs;
  SmallVector<Value, 1> outputs;

  std::vector<SchedTensorType> input_tensors;
  std::vector<SchedTensorType> output_tensors;
  std::vector<SchedTensorType> weight_tensors;
  std::vector<SchedTensorType> bias_tensors;

  std::vector<connection> consumers;
  std::vector<connection> producers;

  std::vector<NpuBlockConfig> all_block_configs;

  FilterMetaData KernelFilter;
  NpuBlockType BlockType;
  Operation *major_op;

  SmallVector<Operation *, 1> act_ops;
};

} // namespace planner
} // namespace mlir

#endif // KERNEL_INFO_H

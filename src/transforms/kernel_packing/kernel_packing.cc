/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#include "src/transforms/kernel_packing/kernel_packing.h"

namespace mlir {
namespace planner {

void delete_op(list<OP_INDEX> &order_ops_filtered, OP_INDEX index) {
  for (auto it = order_ops_filtered.begin(); it != order_ops_filtered.end();
       it++) {
    if (*it == index) {
      order_ops_filtered.erase(it);
      return;
    }
  }
}

unsigned KernelPacker(
    MLIRContext *ctx, dag_graph &dag, Block &block,
    mlir::ArchitectureConfig::ArchitectureConfigAttr &architecture_config,
    std::vector<kernelInfo> &kernels,
    std::unordered_map<std::string, MappingStructure> &hw_maplist) {
  list<OP_INDEX> order_ops; /* Whole Basic Block Topologically Sorted     */
  if (dag.TopologicalSort(order_ops) != 0) {
    std::cout << "TopologicalSort failed" << std::endl;
    return 1;
  }

  /* Remove scheduleir.yield */
  list<OP_INDEX> order_ops_filtered;
  for (auto op_index : order_ops) {
    Operation *op = dag.get_index_2_operation(op_index);
    std::string op_dialect = op->getDialect()->getNamespace().str();

    if (llvm::isa<scheduleir::YieldOp>(op))
      continue;

    /* Assert all Ops are ScheduleIR. If CPU they would still be TFLITE
     * andshould not be in the NPU Region */
    bool illegal_op_condition = (not(llvm::isa<scheduleir::SliceViewOp>(op)) and
                                 not(llvm::isa<scheduleir::MemoryOp>(op)) and
                                 not(llvm::isa<scheduleir::DmaOp>(op)) and
                                 (op_dialect != "scheduleir"));
    ASSERT_COND(illegal_op_condition,
                "KernelPacker::Encountered non ScheduleIR Op in NPU Region");
    order_ops_filtered.push_back(op_index);
  }

  unsigned kernel_count = 0;

  bool matched = false;
  do {
    matched = false;
    for (auto it = order_ops_filtered.begin(); it != order_ops_filtered.end();
         it++) {
      OP_INDEX op_index = *it;
      Operation *op = dag.get_index_2_operation(op_index);
      kernelInfo p;
      /* DPU Pipeline */
      if (llvm::isa<scheduleir::Conv2DOp>(op) ||
          llvm::isa<scheduleir::DepthwiseConv2DOp>(op) ||
          llvm::isa<scheduleir::MaxPool2dOp>(op) ||
          llvm::isa<scheduleir::AvgPool2dOp>(op) ||
          llvm::isa<scheduleir::AddOp>(op) ||
          llvm::isa<scheduleir::MulOp>(op)) {
        p.ops.push_back(op_index);
        order_ops_filtered.erase(it);

        OP_INDEX consumer_to_add = op_index;
        do {
          std::vector<OP_INDEX> consumers_level_n =
              dag.GetConsumers(consumer_to_add);
          consumer_to_add = consumers_level_n[0];

          std::vector<OP_INDEX> producers_level_n =
              dag.GetProducers(consumer_to_add);

          Operation *op_consumer = dag.get_index_2_operation(consumer_to_add);

          if (producers_level_n.size() != 1)
            break;
          else if (consumers_level_n.size() != 1)
            break;
          else if (not((hw_maplist[getOpName(*op_consumer)].compute_unit ==
                        Dpu) &&
                       (hw_maplist[getOpName(*op_consumer)].compute_sub_unit ==
                        OutputUnit)))
            break;

          /* Add the Consumer */
          p.ops.push_back(consumers_level_n[0]);
          delete_op(order_ops_filtered, consumers_level_n[0]);
          /* Move one one more level down the consumer list */

        } while (true);

        /* Check for producers */
        std::vector<OP_INDEX> producers_level_n = dag.GetProducers(op_index);
        for (auto pr : producers_level_n) {
          Operation *op_prod = dag.get_index_2_operation(pr);
          if (llvm::isa<scheduleir::DmaOp>(op_prod)) {
            Value v_const = op_prod->getOperand(0);
            Operation *v_const_defining_op = v_const.getDefiningOp();
            if (llvm::isa<scheduleir::ConstOp>(v_const_defining_op)) {
              unsigned pr_constant =
                  dag.get_operation_2_index(v_const_defining_op);
              p.ops_const.push_back(pr_constant);
              delete_op(order_ops_filtered, pr_constant);
            }
            p.ops_const.push_back(pr);
            delete_op(order_ops_filtered, pr);
          } else if (llvm::isa<scheduleir::ConstOp>(op_prod)) {
            p.ops_const.push_back(pr);
            delete_op(order_ops_filtered, pr);
          }
        }

        OpBuilder builder(ctx);

        p.name = ("Kernel_" + std::to_string(kernel_count));

        kernels.push_back(p);
        matched = true;
        kernel_count++;
        break;
      }
    }
  } while (matched);

  /* Default where an Op is placed in a Kernel on its own */
  for (auto it = order_ops_filtered.begin(); it != order_ops_filtered.end();
       it++) {
    OP_INDEX op_index = *it;
    Operation *op = dag.get_index_2_operation(op_index);

    if (llvm::isa<scheduleir::CastOutOp>(op))
      continue;
    if (llvm::isa<scheduleir::CastInOp>(op))
      continue;

    kernelInfo p;
    p.ops.push_back(op_index);
    // Configure Kernel Attribute
    OpBuilder builder(ctx);
    p.name = ("Kernel_" + std::to_string(kernel_count));
    kernels.push_back(p);
    /* No need to erase we are moving linearily here */
    kernel_count++;
  }
}

} // namespace planner
} // namespace mlir

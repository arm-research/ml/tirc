/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#ifndef KERNEL_PACKING_H
#define KERNEL_PACKING_H

#include <iostream>
#include <unordered_map>
#include <vector>

#include "src/ir/architecture_config/architecture_config.h"
#include "src/utils/checkIR.h"
#include "src/utils/printIR.h"

#include "mlir/Dialect/Quant/QuantOps.h"
#include "mlir/Dialect/Quant/QuantTypes.h"
#include "mlir/Dialect/StandardOps/IR/Ops.h"      // from @llvm-project
#include "mlir/IR/AffineMap.h"                    // from @llvm-project
#include "mlir/IR/Attributes.h"                   // from @llvm-project
#include "mlir/IR/BlockAndValueMapping.h"         // from @llvm-project
#include "mlir/IR/Builders.h"                     // from @llvm-project
#include "mlir/IR/Location.h"                     // from @llvm-project
#include "mlir/IR/MLIRContext.h"                  // from @llvm-project
#include "mlir/IR/Operation.h"                    // from @llvm-project
#include "mlir/IR/PatternMatch.h"                 // from @llvm-project
#include "mlir/Interfaces/InferTypeOpInterface.h" // from @llvm-project
#include "mlir/Pass/Pass.h"                       // from @llvm-project
#include "mlir/Transforms/DialectConversion.h"    // from @llvm-project

#include "src/ir/kernel_ir.h"
#include "src/transforms/kernel_packing/kernelInfo.h"
#include "src/utils/cluster_ir.h"
#include "src/utils/dag.h"

namespace mlir {
namespace planner {

unsigned KernelPacker(
    MLIRContext *ctx, dag_graph &dag, Block &block,
    mlir::ArchitectureConfig::ArchitectureConfigAttr &architecture_config,
    std::vector<kernelInfo> &kernels,
    std::unordered_map<std::string, MappingStructure> &maplist);

} // namespace planner
} // namespace mlir

#endif // KERNEL_PACKING_H

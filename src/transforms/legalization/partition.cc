/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#include "src/transforms/legalization/partition.h"

namespace mlir {
namespace planner {

enum subgraph_type { cpu, npu, unknown };
subgraph_type get_op_iploc(Operation *op) {
  std::string op_dialect = op->getDialect()->getNamespace().str();
  if (op_dialect == "scheduleir")
    return npu;
  else
    return cpu;
  return unknown;
}

class subgraph {
public:
  subgraph_type type;
  std::vector<mlir::Operation *> ops;
};
using subgraphs = std::list<subgraph>;

/* Implements a Bottom Up Search for Subgraphs */
int extract_subgraphs(Block &block, subgraphs &sgs) {
  dag_graph dag;
  if (dag.LoadBlock2DAGRepresentation(block, true) != 0) {
    std::cout << "PlannerExtractNpuRegions::"
                 "ConvertBlock2DAGRepresentation failed"
              << std::endl;
    return 1;
  }

  // dag.print_topological_sort();

  std::vector<OP_INDEX> order_ops;
  dag.TopologicalSort(order_ops);

  std::unordered_map<OP_INDEX, subgraph *> sg_map;
  for (auto i : order_ops)
    sg_map[i] = nullptr;

  bool iterate = false;
  do {
    for (auto i : order_ops) {
      Operation *op = dag.get_index_2_operation(i);

      std::vector<OP_INDEX> producers = dag.GetProducers(i);
      std::vector<OP_INDEX> consumers = dag.GetConsumers(i);

      if (producers.size() == 0) {
        /* If we have no producers than we start a new subgraph */
        subgraphs::iterator sg = sgs.emplace(sgs.end());
        sg->ops.push_back(op);
        sg->type = get_op_iploc(op);
        sg_map[i] = &(*sg);
      } else {
        /* Check 0: All producers are part of a Subgraph */
        bool all_producers_sg = true;
        for (auto p : producers) {
          auto producer_sg = sg_map[p];
          if (producer_sg == nullptr)
            goto REITERATE;
        }

        bool producers_all_cpu = true;
        bool producers_all_npu = true;
        for (auto p : producers) {
          producers_all_cpu &= (sg_map[p]->type == cpu);
          producers_all_npu &= (sg_map[p]->type == npu);
        }

        auto sg = sg_map[producers[0]];

        /* Check 1: All producers are NPU Subgraphs and the Op is a NPU Node so
         * we can merge */
        if (producers_all_npu and (get_op_iploc(op) == npu)) {
          sg->ops.push_back(op);
          sg_map[i] = &(*sg);
        }
        /* Check 2: All producers are CPU Subgraphs and Op is a CPU Node so we
           can merge */
        else if (producers_all_cpu and (get_op_iploc(op) == cpu)) {
          sg->ops.push_back(op);
          sg_map[i] = &(*sg);
        }
        /* Check 3: Producers are different types of subgraphs so we need to
           create a new subgraph starting from this Op */
        else {
          subgraphs::iterator sg = sgs.emplace(sgs.end());
          sg->ops.push_back(op);
          sg->type = get_op_iploc(op);
          sg_map[i] = &(*sg);
        }
      }

      /*
      for (auto const &pair : sg_map) {
        if (pair.second != nullptr)
            std::cout << pair.first << " " << pair.second << " -> " <<
      pair.second->type << std::endl; else std::cout << pair.first << ": " <<
      pair.second << std::endl;
      }
      */
    }
  REITERATE:
    iterate = false;
    for (auto const &pair : sg_map) {
      if (pair.second == nullptr)
        iterate = true;
    }
  } while (iterate);
}

int create_npu_regions(subgraphs &sgs, Block &block, MLIRContext *context) {
  for (auto &sg : sgs) {
    if (sg.type == npu) {
      Operation *clusterOp;

      std::vector<mlir::Operation *>
          order_ops_filtered; /* Ops including CastIN and CastOut */
      std::vector<mlir::Operation *>
          order_ops_filtered_constant_ops; /* Const Ops */

      for (auto op : sg.ops) {
        /* Check ops Operands and add the consts */
        for (mlir::Value v : op->getOperands()) {
          Operation *op_con = v.getDefiningOp();
          if (op_con != nullptr) {
            if (llvm::isa<scheduleir::ConstOp>(op_con)) {
              order_ops_filtered_constant_ops.push_back(op_con);
            }
          }
        }

        /* Else Add Node and Progress onwards through the topological sort*/
        order_ops_filtered.push_back(op);
      }

      if (Cluster(order_ops_filtered, order_ops_filtered_constant_ops, block,
                  mlir::utils::cluster_op::npu_region_op, context,
                  clusterOp) != 0) {
        std::cout << "PlannerExtractNpuRegions::Cluster failed" << std::endl;
        return 1;
      }
    }
  }
}

int Partition(MLIRContext *context, FuncOp &func) {
  OpBuilder builder(context);
  Region *region = func.getCallableRegion();
  llvm::iplist<Block> &blocks = region->getBlocks();
  if (blocks.size() > 1) {
    std::cout << "PlannerExtractNpuRegions:::More than a single block in main "
                 "function callable region"
              << std::endl;
    return 1;
  }
  Block &block = region->front();

  /* 0. Extract Subgraphs Build a DAG from the Block for ease of traversal */
  subgraphs sgs;
  extract_subgraphs(block, sgs);

  /* 1. Create NPURegions */
  create_npu_regions(sgs, block, context);
}

} // namespace planner
} // namespace mlir

/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#include "src/transforms/legalization/scheduleir/legalization_scheduleir.h"

namespace mlir {
namespace planner {

using namespace mlir::utils;
#define FORCE_BATCH_TO_CONSTANT 1
#define BATCH_CONSTANT 1

void LoadDenseElementsAttr_8(mlir::DenseElementsAttr &value,
                             unsigned char *raw_data) {
  // raw_data = reinterpret_cast<unsigned const char
  // *>(value.getRawData().data());
  memcpy(raw_data, reinterpret_cast<const char *>(value.getRawData().data()),
         value.getNumElements());
}

void LoadDenseElementsAttr_32(mlir::DenseElementsAttr &value,
                              unsigned char *raw_data) {
  // raw_data = reinterpret_cast<unsigned const char
  // *>(value.getRawData().data());
  memcpy(raw_data, reinterpret_cast<const char *>(value.getRawData().data()),
         value.getNumElements() * 4);
}

LogicalResult
ConvertTosaConstOp::matchAndRewrite(Operation *op,
                                    PatternRewriter &rewriter) const {
  ASSERT_COND(op->getNumResults() != 1,
              "Legalization::ScheduleIR::Op with more than 1 Output");
  RankedTensorType resultType_original =
      op->getResult(0).getType().dyn_cast<RankedTensorType>();
  if (!resultType_original)
    FATAL_ERROR("Legalization::ScheduleIR::Output not RankedTensorType");
  SmallVector<int64_t> shape =
      arrayref_2_smallvector(resultType_original.getShape());
  if (FORCE_BATCH_TO_CONSTANT) {
    if ((shape.size() > 1) and (shape[0] == -1))
      shape[0] = BATCH_CONSTANT;
  }

  auto resultType = SchedTensorType::get(
      shape, resultType_original.getElementType(), getUniqueId());
  int width = 0;
  value_datatype v = getDataType(resultType);

  auto odsLoc = rewriter.getFusedLoc({op->getLoc()});
  (void)odsLoc;

  Builder builder(op->getContext());

  auto value = op->getAttrOfType<mlir::DenseElementsAttr>("value");

  auto op_new = rewriter.create<mlir::scheduleir::ConstOp>(odsLoc, resultType);

  int n = value.getNumElements();
  if ((v == int8) || (v == uint8)) {
    auto raw_data = (std::make_unique<unsigned char[]>(n));
    LoadDenseElementsAttr_8(value, raw_data.get());
    auto raw_data_attr = RawDataAttr::get(op->getContext(), std::move(raw_data),
                                          n, 1, getUniqueId());
    op_new->setAttr("value", raw_data_attr);
  } else if (v == int32) {
    auto raw_data = (std::make_unique<unsigned char[]>(n * 4));
    LoadDenseElementsAttr_32(value, raw_data.get());

    if ((value.getRawData().size() / 4) != value.getNumElements())
      memset(raw_data.get(), 0, n * 4);
    else
      LoadDenseElementsAttr_32(value, raw_data.get());

    /*
    std::cout << std::endl;
    value.dump();
    std::cout << value.getNumElements() << std::endl;
    std::cout << value.size() << std::endl;
    std::cout << value.empty() << std::endl;
    std::cout << value.getRawData().size() << std::endl;
    std::cout << std::endl;
    */

    auto raw_data_attr = RawDataAttr::get(op->getContext(), std::move(raw_data),
                                          n, 4, getUniqueId());
    op_new->setAttr("value", raw_data_attr);
  } else
    FATAL_ERROR("legalization_scheduleir::ConvertTosaConstOp::matchAndRewrite::"
                "FatalError Unknown Data Type of Dense Element Const");

  rewriter.replaceOp(op, op_new->getResult(0));
  op_new->setAttr("quant_present", builder.getBoolAttr(false));

  return ::mlir::success();
}

#define MATCH_AND_REWRITE(op)                                                  \
  LogicalResult ConvertTosa##op##Op::matchAndRewrite(                          \
      Operation *op, PatternRewriter &rewriter) const {                        \
    ASSERT_COND(op->getNumResults() != 1,                                      \
                "Legalization::ScheduleIR::Op with more than 1 Output");       \
    RankedTensorType resultType_original =                                     \
        op->getResult(0).getType().dyn_cast<RankedTensorType>();               \
    if (!resultType_original)                                                  \
      FATAL_ERROR("Legalization::ScheduleIR::Output not RankedTensorType");    \
    SmallVector<int64_t> shape =                                               \
        arrayref_2_smallvector(resultType_original.getShape());                \
    if (FORCE_BATCH_TO_CONSTANT) {                                             \
      if ((shape.size() > 1) and (shape[0] == -1))                             \
        shape[0] = BATCH_CONSTANT;                                             \
    }                                                                          \
    auto resultType = SchedTensorType::get(                                    \
        shape, resultType_original.getElementType(), getUniqueId());           \
    SmallVector<mlir::Value> inputs;                                           \
    for (int i = 0; i < op->getNumOperands(); i++)                             \
      inputs.push_back(op->getOperand(i));                                     \
    auto odsLoc = rewriter.getFusedLoc({op->getLoc()});                        \
    (void)odsLoc;                                                              \
    auto op_new = rewriter.create<mlir::scheduleir::op##Op>(                   \
        odsLoc, resultType, inputs, op->getAttrs());                           \
    rewriter.replaceOp(op, op_new->getResult(0));                              \
    Builder builder(op_new->getContext());                                     \
    op_new->setAttr("quant_present", builder.getBoolAttr(false));              \
    return ::mlir::success();                                                  \
  }
MATCH_AND_REWRITE(Clamp);
MATCH_AND_REWRITE(Conv2D);
MATCH_AND_REWRITE(FullyConnected);
MATCH_AND_REWRITE(DepthwiseConv2D);
MATCH_AND_REWRITE(Add);
MATCH_AND_REWRITE(Mul);
MATCH_AND_REWRITE(MaxPool2d);
MATCH_AND_REWRITE(AvgPool2d);
MATCH_AND_REWRITE(Concat);
MATCH_AND_REWRITE(Rescale);
MATCH_AND_REWRITE(Reshape);

#define FOLD_RESCALES(op)                                                      \
  LogicalResult ConvertScheduleIR##op##Op::matchAndRewrite(                    \
      Operation *op, PatternRewriter &rewriter) const {                        \
    bool quant_present =                                                       \
        op->getAttrOfType<::mlir::BoolAttr>(StringRef("quant_present"))        \
            .getValue();                                                       \
    if (quant_present)                                                         \
      return ::mlir::success();                                                \
    mlir::Value r = op->getResult(0);                                          \
    ASSERT_COND(not(r.hasOneUse()),                                            \
                "planner_legalize_scheduleir::ConvertScheduleIRop##Op::"       \
                "Output Value more than one use");                             \
    for (Operation * op_rescale : r.getUsers()) {                              \
      if (llvm::isa<scheduleir::RescaleOp>(op_rescale)) {                      \
        SchedTensorType resultType =                                           \
            op_rescale->getResult(0).getType().dyn_cast<SchedTensorType>();    \
        SmallVector<mlir::Value> inputs;                                       \
        for (int i = 0; i < op->getNumOperands(); i++)                         \
          inputs.push_back(op->getOperand(i));                                 \
        auto odsLoc = rewriter.getFusedLoc({op->getLoc()});                    \
        (void)odsLoc;                                                          \
        auto op_conv_new = rewriter.create<mlir::scheduleir::op##Op>(          \
            odsLoc, resultType, inputs, op->getAttrs());                       \
        op_conv_new->setAttr("input_zp",                                       \
                             op_rescale->getAttrOfType<::mlir::IntegerAttr>(   \
                                 StringRef("input_zp")));                      \
        op_conv_new->setAttr("output_zp",                                      \
                             op_rescale->getAttrOfType<::mlir::IntegerAttr>(   \
                                 StringRef("output_zp")));                     \
        op_conv_new->setAttr("multiplier",                                     \
                             op_rescale->getAttrOfType<::mlir::ArrayAttr>(     \
                                 StringRef("multiplier")));                    \
        op_conv_new->setAttr(                                                  \
            "shift",                                                           \
            op_rescale->getAttrOfType<::mlir::ArrayAttr>(StringRef("shift"))); \
        op_conv_new->setAttr("scale32",                                        \
                             op_rescale->getAttrOfType<::mlir::BoolAttr>(      \
                                 StringRef("scale32")));                       \
        op_conv_new->setAttr("double_round",                                   \
                             op_rescale->getAttrOfType<::mlir::BoolAttr>(      \
                                 StringRef("double_round")));                  \
        op_conv_new->setAttr("per_channel",                                    \
                             op_rescale->getAttrOfType<::mlir::BoolAttr>(      \
                                 StringRef("per_channel")));                   \
        Builder builder(op->getContext());                                     \
        op_conv_new->setAttr("quant_present", builder.getBoolAttr(true));      \
        op_rescale->getResult(0).replaceAllUsesWith(                           \
            op_conv_new->getResult(0));                                        \
        rewriter.replaceOp(op, op_conv_new->getResult(0));                     \
        rewriter.eraseOp(op_rescale);                                          \
        return ::mlir::success();                                              \
      }                                                                        \
    }                                                                          \
    FATAL_ERROR("planner_legalize_scheduleir::ConvertScheduleIRConv2DOp::"     \
                "Rescale at output of Convolution not Found");                 \
  }

FOLD_RESCALES(Conv2D)
FOLD_RESCALES(DepthwiseConv2D)
FOLD_RESCALES(FullyConnected)

#define CREATE_SCHEDULEIR_Add                                                  \
  rewriter.create<mlir::scheduleir::AddOp>(                                    \
      odsLoc, resultType, op_input_rescale_0->getOperand(0),                   \
      op_input_rescale_1->getOperand(0), input_rescale_0_input_zp,             \
      input_rescale_0_multiplier, input_rescale_0_shift,                       \
      input_rescale_1_input_zp, input_rescale_1_multiplier,                    \
      input_rescale_1_shift, output_rescale_output_zp,                         \
      output_rescale_multiplier, output_rescale_shift)

#define CREATE_SCHEDULEIR_Mul                                                  \
  rewriter.create<mlir::scheduleir::MulOp>(                                    \
      odsLoc, resultType, op_input_rescale_0->getOperand(0),                   \
      op_input_rescale_1->getOperand(0), input_rescale_0_input_zp,             \
      input_rescale_1_input_zp, output_rescale_output_zp,                      \
      output_rescale_multiplier, output_rescale_shift)

#define FOLD_RESCALE_BINARY_OP(op)                                             \
  LogicalResult ConvertScheduleIR##op##Op::matchAndRewrite(                    \
      Operation *op, PatternRewriter &rewriter) const {                        \
    /* fuse the rescales back */                                               \
    bool quant_present =                                                       \
        op->getAttrOfType<::mlir::BoolAttr>(StringRef("quant_present"))        \
            .getValue();                                                       \
    if (quant_present)                                                         \
      return ::mlir::success();                                                \
                                                                               \
    mlir::Value r = op->getResult(0);                                          \
    ASSERT_COND(                                                               \
        not(r.hasOneUse()),                                                    \
        "planner_legalize_scheduleir::ConvertScheduleIR##op##Op::Output "      \
        "Value more than one use");                                            \
                                                                               \
    for (Operation * op_output_rescale : r.getUsers()) {                       \
      if (llvm::isa<scheduleir::RescaleOp>(op_output_rescale)) {               \
        SchedTensorType resultType = op_output_rescale->getResult(0)           \
                                         .getType()                            \
                                         .dyn_cast<SchedTensorType>();         \
                                                                               \
        Operation *op_input_rescale_0 = op->getOperand(0).getDefiningOp();     \
        Operation *op_input_rescale_1 = op->getOperand(1).getDefiningOp();     \
                                                                               \
        /* Skip and erase reshape in rescale-reshape-binary_op pattern. */     \
        if (llvm::isa<tosa::ReshapeOp>(op_input_rescale_0)) {                  \
          Operation *dangling_op = op_input_rescale_0;                         \
          op_input_rescale_0 =                                                 \
              op_input_rescale_0->getOperand(0).getDefiningOp();               \
          rewriter.eraseOp(dangling_op);                                       \
        }                                                                      \
        if (llvm::isa<tosa::ReshapeOp>(op_input_rescale_1)) {                  \
          Operation *dangling_op = op_input_rescale_1;                         \
          op_input_rescale_1 =                                                 \
              op_input_rescale_1->getOperand(0).getDefiningOp();               \
          rewriter.eraseOp(dangling_op);                                       \
        }                                                                      \
                                                                               \
        /* Check Both Operands are Rescales. In the future this may not always \
         * be the case and we may need to think about it. We have already      \
         * prioritised Conv but Conv always have a rescale (assuming).         \
         */                                                                    \
        ASSERT_COND(                                                           \
            not(llvm::isa<scheduleir::RescaleOp>(op_input_rescale_0)),         \
            "planner_legalize_scheduleir::ConvertScheduleIR##op##Op::Input "   \
            "0 not Rescale");                                                  \
        ASSERT_COND(                                                           \
            not(llvm::isa<scheduleir::RescaleOp>(op_input_rescale_1)),         \
            "planner_legalize_scheduleir::ConvertScheduleIR##op##Op::Input "   \
            "1 not Rescale");                                                  \
                                                                               \
        auto input_rescale_0 =                                                 \
            llvm::cast<scheduleir::RescaleOp>(op_input_rescale_0);             \
        auto input_rescale_1 =                                                 \
            llvm::cast<scheduleir::RescaleOp>(op_input_rescale_1);             \
        auto output_rescale =                                                  \
            llvm::cast<scheduleir::RescaleOp>(op_output_rescale);              \
                                                                               \
        IntegerAttr input_rescale_0_input_zp = input_rescale_0.input_zpAttr(); \
        IntegerAttr input_rescale_0_output_zp =                                \
            input_rescale_0.output_zpAttr();                                   \
        IntegerAttr input_rescale_0_multiplier =                               \
            input_rescale_0.multiplierAttr()[0].dyn_cast<IntegerAttr>();       \
        IntegerAttr input_rescale_0_shift =                                    \
            input_rescale_0.shift()[0].dyn_cast<IntegerAttr>();                \
                                                                               \
        IntegerAttr input_rescale_1_input_zp = input_rescale_1.input_zpAttr(); \
        IntegerAttr input_rescale_1_output_zp =                                \
            input_rescale_1.output_zpAttr();                                   \
        IntegerAttr input_rescale_1_multiplier =                               \
            input_rescale_1.multiplierAttr()[0].dyn_cast<IntegerAttr>();       \
        IntegerAttr input_rescale_1_shift =                                    \
            input_rescale_1.shift()[0].dyn_cast<IntegerAttr>();                \
                                                                               \
        IntegerAttr output_rescale_input_zp = output_rescale.input_zpAttr();   \
        IntegerAttr output_rescale_output_zp = output_rescale.output_zpAttr(); \
        IntegerAttr output_rescale_multiplier =                                \
            output_rescale.multiplierAttr()[0].dyn_cast<IntegerAttr>();        \
        IntegerAttr output_rescale_shift =                                     \
            output_rescale.shift()[0].dyn_cast<IntegerAttr>();                 \
                                                                               \
        ASSERT_COND(not(input_rescale_0_output_zp.getInt() == 0 &&             \
                        input_rescale_1_output_zp.getInt() == 0 &&             \
                        output_rescale_input_zp.getInt() == 0),                \
                    "planner_legalize_scheduleir::ConvertScheduleIR##op##Op "  \
                    "zeropoints on accumulator space needs to be 0.");         \
                                                                               \
        auto odsLoc = rewriter.getFusedLoc({op->getLoc()});                    \
        auto op_add_new = CREATE_SCHEDULEIR_##op;                              \
                                                                               \
        op_add_new->setAttr("quant_present", rewriter.getBoolAttr(true));      \
                                                                               \
        op_output_rescale->getResult(0).replaceAllUsesWith(                    \
            op_add_new->getResult(0));                                         \
                                                                               \
        rewriter.replaceOp(op, op_add_new->getResult(0));                      \
                                                                               \
        rewriter.eraseOp(op_input_rescale_0);                                  \
        rewriter.eraseOp(op_input_rescale_1);                                  \
        rewriter.eraseOp(op_output_rescale);                                   \
                                                                               \
        return ::mlir::success();                                              \
      }                                                                        \
    }                                                                          \
    FATAL_ERROR(                                                               \
        "planner_legalize_scheduleir::ConvertScheduleIR##op##Op::Rescale at "  \
        "output of ##op## not Found");                                         \
  }

FOLD_RESCALE_BINARY_OP(Add)
FOLD_RESCALE_BINARY_OP(Mul)

LogicalResult
RemoveScheduleIRRescaleOp::matchAndRewrite(Operation *op,
                                           PatternRewriter &rewriter) const {
  ASSERT_COND(op->getNumResults() != 1,
              "Legalization::ScheduleIR::Op with more than 1 Output");

  auto resultType = op->getResult(0).getType().dyn_cast<SchedTensorType>();

  Builder builder(op->getContext());
  OpBuilder op_builder(op->getContext());
  mlir::Location loc = op_builder.getUnknownLoc();

  ::mlir::ArrayAttr kernel = builder.getI64ArrayAttr({1, 1});
  ::mlir::ArrayAttr stride = builder.getI64ArrayAttr({1, 1});
  ::mlir::ArrayAttr pad = builder.getI64ArrayAttr({0, 0, 0, 0});

  ::mlir::IntegerAttr input_zp =
      op->getAttrOfType<mlir::IntegerAttr>("input_zp");
  ::mlir::IntegerAttr output_zp =
      op->getAttrOfType<mlir::IntegerAttr>("output_zp");
  ::mlir::ArrayAttr multiplier =
      op->getAttrOfType<mlir::ArrayAttr>("multiplier");
  ::mlir::ArrayAttr shift = op->getAttrOfType<mlir::ArrayAttr>("shift");
  ::mlir::BoolAttr scale32 = op->getAttrOfType<mlir::BoolAttr>("scale32");
  ::mlir::BoolAttr double_round =
      op->getAttrOfType<mlir::BoolAttr>("double_round");
  ::mlir::BoolAttr per_channel =
      op->getAttrOfType<mlir::BoolAttr>("per_channel");

  auto odsLoc = rewriter.getFusedLoc({op->getLoc()});
  (void)odsLoc;
  auto op_new = rewriter.create<mlir::scheduleir::AvgPool2dOp>(
      odsLoc, resultType, op->getOperand(0), kernel, stride, pad, input_zp,
      output_zp, multiplier, shift, scale32, double_round, per_channel);
  rewriter.replaceOp(op, op_new->getResult(0));

  op_new->setAttr("quant_present", builder.getBoolAttr(true));
  return ::mlir::success();
}

} // namespace planner
} // namespace mlir

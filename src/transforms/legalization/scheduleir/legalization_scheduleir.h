/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#ifndef LEGALIZATION_SCHEDULEIR_H
#define LEGALIZATION_SCHEDULEIR_H

#include <climits>
#include <cstddef>
#include <cstdint>
#include <fstream>
#include <iterator>
#include <numeric>
#include <unordered_set>

#include "mlir/Dialect/Tosa/IR/TosaOps.h"               // from @llvm-project
#include "mlir/Support/LLVM.h"                          // from @llvm-project
#include "mlir/Transforms/GreedyPatternRewriteDriver.h" // from @llvm-project
#include "src/transforms/passes.h"
#include "tensorflow/compiler/mlir/lite/ir/tfl_ops.h"
#include "tensorflow/compiler/mlir/tosa/transforms/legalize_common.h"
#include "tensorflow/compiler/mlir/tosa/transforms/legalize_utils.h"

#include "src/ir/raw_data_attribute/RawDataAttr.h"
#include "src/ir/schedule_ir.h"

#include "src/utils/printIR.h"
#include "src/utils/unique_id.h"

#include "llvm/ADT/SmallPtrSet.h"

namespace mlir {
namespace planner {

#define DECL_CONVERT_OP(tfl_op)                                                \
  struct ConvertTosa##tfl_op##Op : public RewritePattern {                     \
    explicit ConvertTosa##tfl_op##Op(MLIRContext *context)                     \
        : RewritePattern(tosa::tfl_op##Op::getOperationName(), 1, context) {}  \
    LogicalResult matchAndRewrite(Operation *op,                               \
                                  PatternRewriter &rewriter) const override;   \
  }

DECL_CONVERT_OP(Const);
DECL_CONVERT_OP(Clamp);
DECL_CONVERT_OP(Conv2D);
DECL_CONVERT_OP(FullyConnected);
DECL_CONVERT_OP(DepthwiseConv2D);
DECL_CONVERT_OP(Add);
DECL_CONVERT_OP(Mul);
DECL_CONVERT_OP(MaxPool2d);
DECL_CONVERT_OP(AvgPool2d);
DECL_CONVERT_OP(Concat);
DECL_CONVERT_OP(Rescale);
DECL_CONVERT_OP(Reshape);

#define DECL_CONVERT_OP_S(tfl_op)                                              \
  struct ConvertScheduleIR##tfl_op##Op : public RewritePattern {               \
    explicit ConvertScheduleIR##tfl_op##Op(MLIRContext *context)               \
        : RewritePattern(scheduleir::tfl_op##Op::getOperationName(), 1,        \
                         context) {}                                           \
    LogicalResult matchAndRewrite(Operation *op,                               \
                                  PatternRewriter &rewriter) const override;   \
  }

DECL_CONVERT_OP_S(Conv2D);
DECL_CONVERT_OP_S(Add);
DECL_CONVERT_OP_S(Mul);
DECL_CONVERT_OP_S(DepthwiseConv2D);
DECL_CONVERT_OP_S(FullyConnected);

struct RemoveScheduleIRRescaleOp : public RewritePattern {
  explicit RemoveScheduleIRRescaleOp(MLIRContext *context)
      : RewritePattern(scheduleir::RescaleOp::getOperationName(), 1, context) {}
  LogicalResult matchAndRewrite(Operation *op,
                                PatternRewriter &rewriter) const override;
};

} // namespace planner
} // namespace mlir

#endif // LEGALIZATION_SCHEDULEIR_H

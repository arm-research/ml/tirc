/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#include <unordered_map>
#include <vector>

#include "src/transforms/llcs_generator/command_emitter.h"

namespace mlir {
namespace hlcs {

#define MAP_ITEM_FUNC(SET, CMD)                                                \
  { e2i(SET::CMD), #CMD }
#define SEP ,
static std::unordered_map<uint32_t, const char *> cmd0_namelist{
    EXPAND_CMD0(MAP_ITEM_FUNC, SEP)};

static std::unordered_map<uint32_t, const char *> cmd1_namelist{
    EXPAND_CMD1(MAP_ITEM_FUNC, SEP)};
#undef SEP
#undef MAP_ITEM_FUNC

/*
llvm::raw_ostream& operator<< (llvm::raw_ostream& os, const MemoryRangeSet& mrs)
{ os << "<MemoryRangeSet> "; for (auto elem : mrs.regions) { os << elem.first <<
": " << elem.second << "\t";
    }
    return os;
}
*/

// RegisterMachine
bool RegisterMachine::setRegister(uint32_t reg, uint64_t value) {
  auto iter = registers[bank_idx].find(reg);
  bool is_changed =
      (iter == registers[bank_idx].end()) || (iter->second != value);
  registers[bank_idx][reg] = value;
  return is_changed;
}

void RegisterMachine::switchBank() { bank_idx = (bank_idx + 1) % n_banks; }

// CommandStreamEmitter
bool isDmaCmd(uint32_t cmd) {
  switch (cmd) {
  case e2i(cmd0::NPU_OP_DMA_START):
  case e2i(cmd0::NPU_OP_DMA_WAIT):
  case e2i(cmd0::NPU_SET_DMA0_SRC_REGION):
  case e2i(cmd0::NPU_SET_DMA0_DST_REGION):
  case e2i(cmd0::NPU_SET_DMA0_SIZE0):
  case e2i(cmd0::NPU_SET_DMA0_SIZE1):
  case e2i(cmd1::NPU_SET_DMA0_SRC):
  case e2i(cmd1::NPU_SET_DMA0_DST):
  case e2i(cmd1::NPU_SET_DMA0_LEN):
  case e2i(cmd1::NPU_SET_DMA0_SKIP0):
  case e2i(cmd1::NPU_SET_DMA0_SKIP1):
    return true;
  }
  return false;
}

bool isPayload32(uint64_t cmd) {
  uint32_t code = cmd & 0x0000FFFF; // lower 16 bits
  CmdMode payload_mode = static_cast<CmdMode>(code & e2i(CmdMode::Mask));
  return payload_mode == CmdMode::Payload32;
}

RegisterMachine &CommandStreamEmitter::getRegMachine(uint32_t cmd) {
  uint32_t idx = isDmaCmd(cmd) ? 1 : 0;
  return reg_machine[idx];
}

size_t CommandStreamEmitter::sizeInBytes() {
  size_t size = 0;
  for (auto &dword : cmd_stream) {
    ++size;
    if (isPayload32(dword))
      ++size;
  }
  return size * 4;
}

std::vector<uint32_t> CommandStreamEmitter::to_list() {
  std::vector<uint32_t> list;
  for (auto &dword : cmd_stream) {
    list.push_back(dword & 0xFFFFFFFF);
    if (isPayload32(dword))
      list.push_back(dword >> 32);
  }
  return std::move(list);
}

void CommandStreamEmitter::print_cmds(std::string &cmd_file_name) {
  FILE *output = fopen(cmd_file_name.c_str(), "a");
  fprintf(output, "Code:    Command:                       Param: Payload:\n");
  for (auto &dword : cmd_stream) {
    uint32_t code = dword & 0x0000FFFF;
    uint32_t param = (dword & 0xFFFF0000) >> 16;
    CmdMode payload_mode = static_cast<CmdMode>(code & e2i(CmdMode::Mask));
    std::string codename;
    if (payload_mode == CmdMode::NoPayload) {
      uint32_t cmd = code & e2i(CmdMode::CmdOpMask);
      codename = "cmd0.";
      codename += cmd0_namelist[cmd];
    } else {
      uint32_t cmd = code & e2i(CmdMode::CmdOpMask);
      codename = "cmd1.";
      codename += cmd1_namelist[cmd];
    }
    // code and command
    fprintf(output, "  0x%04x %-30s%5d   ", code, codename.c_str(), param);

    if (payload_mode == CmdMode::Payload32) {
      uint32_t payload = dword >> 32;
      fprintf(output, "0x%08x (%u)\n", payload, payload);
    } else {
      fprintf(output, "-\n");
    }
  }
  fclose(output);
}

void CommandStreamEmitter::cmd0_with_param(cmd0 cmd, uint32_t param) {
  param &= 0xFFFF;
  uint32_t command = e2i(cmd) | (param << 16);
  if (getRegMachine(e2i(cmd)).setRegister(e2i(cmd), command)) {
    cmd_stream.push_back(command);
  }
}

void CommandStreamEmitter::cmd1_with_offset(cmd1 cmd, uint32_t offset,
                                            uint32_t param) {
  uint32_t command = e2i(cmd) | e2i(CmdMode::Payload32) | (param << 16);
  uint64_t dword = offset;
  dword <<= 32;
  dword |= command;
  if (getRegMachine(e2i(cmd)).setRegister(e2i(cmd), dword)) {
    cmd_stream.push_back(dword);
  }
}

void CommandStreamEmitter::cmd_wait(uint32_t cmd, uint32_t channel,
                                    uint32_t outstanding_count) {
  uint32_t param = (16 * channel) + outstanding_count;
  uint32_t command = cmd | ((param & 0xFFFF) << 16);
  cmd_stream.push_back(command);
}

void CommandStreamEmitter::cmd_do_operation(cmd0 cmd, uint32_t param) {
  uint32_t command = e2i(cmd) | ((param & 0xFFFF) << 16);
  cmd_stream.push_back(command);
  getRegMachine(e2i(cmd)).switchBank();
}

} // namespace hlcs
} // namespace mlir

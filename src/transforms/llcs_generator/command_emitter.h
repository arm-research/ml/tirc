/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#ifndef COMMAND_EMITTER_H
#define COMMAND_EMITTER_H

#include <assert.h>

#include <string>
#include <type_traits>
#include <typeinfo>
#include <unordered_map>
#include <vector>

#include "src/arch/reg_interface.h"
#include "src/transforms/llcs_generator/hlcs_npu_api.h"

namespace mlir {
namespace hlcs {

#ifndef ENUM_TO_INT
#define ENUM_TO_INT
template <typename Enum> constexpr auto e2i(Enum e) noexcept {
  return static_cast<std::underlying_type_t<Enum>>(e);
}
#endif // ENUM_TO_INT

enum class CmdMode : uint32_t {
  NoPayload = 0x0000,
  Payload32 = 0x4000,
  Mask = 0xC000,
  CmdOpMask = 0x03FF,
};

enum class IFM2Broadcast : uint32_t {
  BroadcastHdim = 1 << 0,
  BroadcastWdim = 1 << 1,
  BroadcastCdim = 1 << 2,
  ReverseOperandOrder = 1 << 6,
  UseIFM2Scalar = 1 << 7,
};

typedef std::unordered_map<uint32_t, uint64_t> reg_map_t;
typedef std::vector<reg_map_t> regs_t;

class RegisterMachine {
private:
  const size_t n_banks = 1;
  size_t bank_idx = 0;
  regs_t registers{n_banks};

public:
  RegisterMachine() = default;
  bool setRegister(uint32_t reg, uint64_t value);
  void switchBank();
  regs_t &_get_regs() { return registers; } // debug only
};

class CommandStreamEmitter {
private:
  std::vector<int64_t> cmd_stream;
  std::vector<RegisterMachine> reg_machine{2};
  // last_absolute_wait ???
public:
  RegisterMachine &getRegMachine(uint32_t cmd);
  size_t sizeInBytes();
  std::vector<uint32_t> to_list();
  void print_cmds(std::string &cmd_file_name);

  void cmd0_with_param(cmd0 cmd, uint32_t param);
  void cmd1_with_offset(cmd1 cmd, uint32_t offset, uint32_t param = 0x0);
  void cmd_wait(uint32_t cmd, uint32_t channel, uint32_t outstanding_count);
  void cmd_do_operation(cmd0 cmd, uint32_t param = 0x0);
  std::vector<int64_t> &_get_cmd_stream() { return cmd_stream; } // debug only
};

} // namespace hlcs
} // namespace mlir
#endif // COMMAND_EMITTER_H

/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#include <cmath>
#include <iostream>

#include "mlir/IR/Attributes.h"           // from @llvm-project
#include "mlir/IR/BlockAndValueMapping.h" // from @llvm-project
#include "mlir/IR/Builders.h"             // from @llvm-project
#include "mlir/IR/Location.h"             // from @llvm-project
#include "mlir/IR/MLIRContext.h"          // from @llvm-project
#include "mlir/IR/Operation.h"            // from @llvm-project

#include "src/transforms/llcs_generator/hlcs_helper.h"
#include "src/transforms/llcs_generator/hlcs_mlir_utils.h"
#include "src/transforms/llcs_generator/hlcs_npu_api.h"

using namespace mlir;
using namespace hlcs;

using namespace std;

namespace mlir {

namespace hlcs {
bool is_unary_elementwise_op(NpuElementWiseOp op) {
  return (op == NpuElementWiseOp::ABS) || (op == NpuElementWiseOp::CLZ) ||
         (op == NpuElementWiseOp::LRELU);
}

int32_t size_in_bytes(std::string &data_type) {
  if (data_type == "uint8" || data_type == "int8")
    return 1;
  else if (data_type == "uint16" || data_type == "int16")
    return 2;
  else if (data_type == "int32")
    return 4;
  else
    assert(!"unsupported format!");
}

bool is_signed(std::string &data_type) {
  return data_type != "uint8" && data_type != "uint16";
}

int32_t min_value(std::string data_type) {
  int32_t size_in_bits = size_in_bytes(data_type) << 3;
  if (is_signed(data_type))
    return -(1 << (size_in_bits - 1));
  else
    return 0;
}

int32_t max_value(std::string data_type) {
  int32_t size_in_bits = size_in_bytes(data_type) << 3;
  if (is_signed(data_type))
    return (1 << (size_in_bits - 1)) - 1;
  else
    return (1 << size_in_bits) - 1;
}

int32_t quantise_float32(float f, float scale, int32_t zero_point) {
  return zero_point + int(std::round(f / scale));
}

void quantise_scale(double scale, uint32_t &u32_scale, int32_t &i6_shift) {
  int32_t exponent = 0;
  double significand = frexp(scale, &exponent);
  uint32_t significand_q31 =
      static_cast<int32_t>(std::round(significand * (1ull << 31)));
  int32_t exponent_q31 = exponent - 31;
  int32_t shift = exponent_q31 * -1;

  if (0 <= shift && shift < (1 << 6)) {
    u32_scale = significand_q31;
    i6_shift = shift;
  } else {
    // Shift outside of valid range, set scale to 0
    u32_scale = 0;
    i6_shift = 16;
  }
}

void quantise_pooling_scale(int32_t nr_kernel_elements, int32_t rescale_bits,
                            int64_t &scale, int32_t &shift) {
  float elems = (nr_kernel_elements - 1) * 1.0f;
  int32_t k = 0;
  float dummy = frexp(elems, &k);
  int32_t N = 31 - rescale_bits;
  scale = static_cast<int64_t>(((1ll << (N + k)) + (1ll << k)) /
                               nr_kernel_elements);
  shift = N + k;
  assert(shift < (1 << 6));
}

int32_t round_up(int32_t a, int32_t b) { return ((a + b - 1) / b) * b; }

int32_t round_up_divide(int32_t a, int32_t b) { return (a + b - 1) / b; }

double clamp_tanh(double x) {
  double y = 0.0;
  if (x <= -4.0)
    y = -1.0;
  else if (x >= 4.0)
    y = 1.0;
  else
    y = tanh(x);
  return y;
}

double clamp_sigmoid(double x) {
  double y = 0.0;
  if (x <= -8.0)
    y = 0.0;
  else if (x >= 8.0)
    y = 1.0;
  else
    y = 1.0 / (1.0 + exp(-x));
  return y;
}

ivec full_shape(int32_t dim, ivec &shape, int32_t fill) {
  ivec ret(dim - shape.size(), fill);
  ret.insert(ret.end(), shape.begin(), shape.end());
  return std::move(ret);
}

bool overlaps(int32_t start1, int32_t end1, int32_t start2, int32_t end2) {
  return start1 < end2 && start2 < end1;
}

// address range

bool coords_intersect(PointXYZ &start_a, PointXYZ &start_b, PointXYZ &end_a,
                      PointXYZ &end_b) {
  // Checks if the two areas overlap
  int32_t start_x = max(start_a.x, start_b.x);
  int32_t end_x = min(end_a.x, end_b.x);
  int32_t start_y = max(start_a.y, start_b.y);
  int32_t end_y = min(end_a.y, end_b.y);
  int32_t start_z = max(start_a.z, start_b.z);
  int32_t end_z = min(end_a.z, end_b.z);
  return ((end_x - start_x) > 0) && ((end_y - start_y) > 0) &&
         ((end_z - start_z) > 0);
}

bool ranges_overlap(NpuAddressRange &range1, NpuAddressRange &range2) {
  // Checks if the ranges overlap
  return range1.region == range2.region &&
         overlaps(range1.address, range1.address + range1.length,
                  range2.address, range2.address + range2.length);
}

bool range_lists_overlap(std::vector<NpuAddressRange> &list1,
                         std::vector<NpuAddressRange> &list2) {
  for (auto &range1 : list1) {
    for (auto &range2 : list2) {
      if (ranges_overlap(range1, range2))
        return true;
    }
  }
  return false;
}

static void add_h_ranges(std::vector<NpuAddressRange> &ranges,
                         mlir::Operation &fm, ivec &strides, int32_t y0,
                         int32_t x0, int32_t c0, int32_t y1, int32_t x1,
                         int32_t c1) {
  /*
  Gets address ranges for (y0, x0, c0) - (y1, x1, c1) (inclusive, so the second
  coordinate is within the fm); the begin and end coordinates must be within the
  same tile. Divides the area in horizontal "stripes" of height 1, and returns
  the address ranges for these "stripes".
  */
  for (int y = y0; y <= y1; ++y) {
    NpuAddressRange range =
        get_address_range(fm, strides, y, x0, c0, y, x1, c1);
    ranges.push_back(range);
  }
}

static int32_t get_address(mlir::Operation &fm, ivec &strides, int32_t y,
                           int32_t x, int32_t c) {
  const int32_t BRICK = 16;
  NpuLayout layout = static_cast<NpuLayout>(get_i32_from_attr(fm, "layout"));
  std::string data_type = get_str_from_attr(fm, "data_type");
  int32_t elem_size = size_in_bytes(data_type);

  int32_t stride_c = strides[2];
  if (layout == NpuLayout::NHWC) {
    stride_c = BRICK * elem_size;
  }
  int32_t stride_x = strides[1];
  if (layout == NpuLayout::NHCWB16) {
    stride_x = BRICK * elem_size;
  }

  int32_t height_0 = get_i32_from_attr(fm, "tiles_height_0");
  int32_t height_1 = get_i32_from_attr(fm, "tiles_height_1");
  int32_t width_0 = get_i32_from_attr(fm, "tiles_width_0");

  int32_t t = 0;
  if (x >= width_0) {
    x -= width_0;
    t = 1;
    if (y >= height_1) {
      y -= height_1;
      t += 2;
    }
  } else if (y >= height_0) {
    y -= height_0;
    t += 2;
  }
  ivec tiles_addresses = get_ivec_from_attr(fm, "tiles_addresses");

  return tiles_addresses[t] + y * strides[0] + x * stride_x +
         (c / BRICK) * stride_c + (c % BRICK) * elem_size;
}

// NpuShape3D get_strides(mlir::Operation& fm) {
static ivec get_strides(mlir::Operation &fm) {
  // Calculates STRIDE_C/Y/X
  ivec strides = get_ivec_from_attr(fm, "strides");
  return strides;
  /*
  if fm.strides is not None:
      return fm.strides
  elem_size = fm.data_type.size_in_bytes()
  if fm.layout == NpuLayout.NHWC:
      stride_c = elem_size
      stride_x = fm.shape.depth * stride_c
      stride_y = fm.shape.width * stride_x
  else:
      stride_x = 16 * elem_size
      stride_c = stride_x * fm.shape.width
      stride_y = elem_size * fm.shape.width *
  numeric_util.round_up(fm.shape.depth, 16) return NpuShape3D(depth=stride_c,
  height=stride_y, width=stride_x)
  */
}

NpuAddressRange get_address_range(mlir::Operation &fm, ivec &strides,
                                  int32_t y0, int32_t x0, int32_t c0,
                                  int32_t y1, int32_t x1, int32_t c1) {
  // Gets address range for (y0, x0, c0) - (y1, x1, c1) (inclusive, so the
  // second coordinate is within the fm). The begin and end coordinates must be
  // within the same tile.
  int32_t addr0 = get_address(fm, strides, y0, x0, c0);
  int32_t addr1 = get_address(fm, strides, y1, x1, c1);
  int32_t region = get_i32_from_attr(fm, "region");
  std::string data_type = get_str_from_attr(fm, "data_type");
  int32_t elem_size = size_in_bytes(data_type);
  return NpuAddressRange{region, addr0, addr1 - addr0 + elem_size};
}

std::vector<NpuAddressRange> get_address_ranges(mlir::Operation &fm) {
  std::vector<NpuAddressRange> ranges;
  ivec strides = get_strides(fm);
  ivec shape = get_ivec_from_attr(fm, "shape");
  int32_t height = shape[0];
  int32_t width = shape[1];
  int32_t depth = shape[2];
  int32_t height_0 = get_i32_from_attr(fm, "tiles_height_0");
  int32_t height_1 = get_i32_from_attr(fm, "tiles_height_1");
  int32_t width_0 = get_i32_from_attr(fm, "tiles_width_0");
  NpuAddressRange t0 =
      get_address_range(fm, strides, 0, 0, 0, min(height, height_0) - 1,
                        min(width, width_0) - 1, depth - 1);
  ranges.push_back(t0);
  if (width > width_0) {
    NpuAddressRange t1 =
        get_address_range(fm, strides, 0, width_0, 0, min(height, height_1) - 1,
                          width - 1, depth - 1);
    ranges.push_back(t1);
  }
  if (height > height_0) {
    NpuAddressRange t2 =
        get_address_range(fm, strides, height_0, 0, 0, height - 1,
                          min(width, width_0) - 1, depth - 1);
    ranges.push_back(t2);
  }
  if (width > width_0 && height > height_0) {
    NpuAddressRange t3 = get_address_range(fm, strides, height_1, width_0, 0,
                                           height - 1, width - 1, depth - 1);
    ranges.push_back(t3);
  }
  return ranges;
}

std::vector<NpuAddressRange> get_address_ranges_for_area(mlir::Operation &fm,
                                                         PointXYZ &start,
                                                         PointXYZ &end) {
  /*
  Returns a list of adddress ranges that covers the area start - end
  (inclusive). Divides the area in horizontal "stripes" of height 1, and returns
  the address ranges for these "stripes".

  For example, for the area marked with X (in a feature map with 4 tiles) as
  input, this function would return 6 address ranges: the address ranges for
  1-height areas [AAA, BBB, CC, DD, EEE, FF]

      .....|....           .....|....
   t0 ..XXX|XX.. t1     t0 ..AAA|CC.. t1
      ..XXX|XX..           ..BBB|DD..
      -----+----    -->    -----+----
   t2 ..XXX|XX.. t3     t2 ..EEE|FF.. t3
      .....|....           .....|....
  */
  // NpuShape3D strides = get_strides(fm);
  ivec strides = get_strides(fm);

  int32_t height_0 = get_i32_from_attr(fm, "tiles_height_0");
  int32_t height_1 = get_i32_from_attr(fm, "tiles_height_1");
  int32_t width_0 = get_i32_from_attr(fm, "tiles_width_0");
  ivec shape = get_ivec_from_attr(fm, "shape");
  int y0 = start.y, x0 = start.x, c0 = start.z;
  int y1 = min(end.y, shape[0] - 1);
  int x1 = min(end.x, shape[1] - 1);
  int c1 = min(end.z, shape[2] - 1);
  std::vector<NpuAddressRange> ranges;
  if (x0 < width_0 && y0 < height_0) {
    // Horizontal ranges for tile 0
    add_h_ranges(ranges, fm, strides, y0, x0, c0, min(y1, height_0 - 1),
                 min(x1, width_0 - 1), c1);
  }
  if (x1 >= width_0 && y0 < height_1) {
    // Horizontal ranges for tile 1
    add_h_ranges(ranges, fm, strides, y0, max(x0, width_0), c0,
                 min(y1, height_1 - 1), x1, c1);
  }
  if (x0 < width_0 && y1 >= height_0) {
    // Horizontal ranges for tile 2
    add_h_ranges(ranges, fm, strides, max(y0, height_0), x0, c0, y1,
                 min(x1, width_0 - 1), c1);
  }
  if (x1 >= width_0 && y1 >= height_1) {
    // Horizontal ranges for tile 3
    add_h_ranges(ranges, fm, strides, max(y0, height_1), max(x0, width_0), c0,
                 y1, x1, c1);
  }
  return ranges;
}

// stream
llvm::raw_ostream &operator<<(llvm::raw_ostream &os, const PointXY &p) {
  os << "<PointXY: " << p.x << ", " << p.y << " >";
}

llvm::raw_ostream &operator<<(llvm::raw_ostream &os, const PointXYZ &p) {
  os << "<PointXYZ: " << p.x << ", " << p.y << ", " << p.z << " >";
}

llvm::raw_ostream &operator<<(llvm::raw_ostream &os, const Block &b) {
  os << "<Block: " << b.width << ", " << b.height << ", " << b.depth << " >";
}

llvm::raw_ostream &operator<<(llvm::raw_ostream &os, const Rect &r) {
  os << "<Rect: (" << r.x << ", " << r.y << ", " << r.z << ") ";
  os << "(" << r.x2 << ", " << r.y2 << ", " << r.z2 << ")>";
}

} // namespace hlcs
} // namespace mlir

/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#ifndef HLCS_HELPER_H
#define HLCS_HELPER_H

#include <string>

#include "src/transforms/llcs_generator/hlcs_npu_api.h"

namespace mlir {
namespace hlcs {

bool is_unary_elementwise_op(NpuElementWiseOp op);

// byte format utils
bool is_signed(std::string &data_type);

int32_t size_in_bytes(std::string &data_type);

int32_t min_value(std::string data_type);

int32_t max_value(std::string data_type);

// numeric utils
int32_t quantise_float32(float f, float scale = 1.0, int32_t zero_point = 0);

void quantise_scale(double scale, uint32_t &u32_scale, int32_t &i6_shift);

void quantise_pooling_scale(int32_t nr_kernel_elements, int32_t rescale_bits,
                            int64_t &scale, int32_t &shift);

int32_t round_up(int32_t a, int32_t b);

int32_t round_up_divide(int32_t a, int32_t b);

double clamp_tanh(double x);

double clamp_sigmoid(double x);

ivec full_shape(int32_t dim, ivec &shape, int32_t fill);

bool overlaps(int32_t start1, int32_t end1, int32_t start2, int32_t end2);

// address range
bool coords_intersect(PointXYZ &start_a, PointXYZ &start_b, PointXYZ &end_a,
                      PointXYZ &end_b);

bool range_lists_overlap(std::vector<NpuAddressRange> &list1,
                         std::vector<NpuAddressRange> &list2);

NpuAddressRange get_address_range(mlir::Operation &fm, ivec &strides,
                                  int32_t y0, int32_t x0, int32_t c0,
                                  int32_t y1, int32_t x1, int32_t c1);

std::vector<NpuAddressRange> get_address_ranges(mlir::Operation &fm);

std::vector<NpuAddressRange> get_address_ranges_for_area(mlir::Operation &fm,
                                                         PointXYZ &start,
                                                         PointXYZ &end);

} // namespace hlcs
} // namespace mlir

#endif // HLCS_HELPER_H

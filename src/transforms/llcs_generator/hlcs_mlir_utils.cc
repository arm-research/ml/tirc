/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#include "mlir/Dialect/StandardOps/IR/Ops.h" // from @llvm-project
#include "mlir/IR/AffineMap.h"               // from @llvm-project
#include "mlir/IR/Attributes.h"              // from @llvm-project
#include "mlir/IR/Location.h"                // from @llvm-project
#include "mlir/IR/MLIRContext.h"             // from @llvm-project

#include "src/transforms/llcs_generator/hlcs_mlir_utils.h"
#include "src/transforms/llcs_generator/hlcs_npu_api.h"

using namespace std;
using namespace mlir;
using namespace hlcs;

namespace mlir {

namespace hlcs {

std::string get_op_name(mlir::Operation &op) {
  return op.getName().getStringRef().str();
}

std::string get_region_name(int32_t region) {
  std::string name;
  switch (region) {
  case static_cast<int32_t>(BasePointerIndex::WeightTensor):
    name = "WeightTensor";
    break;
  case static_cast<int32_t>(BasePointerIndex::ScratchTensor):
    name = "ScratchTensor";
    break;
  case static_cast<int32_t>(BasePointerIndex::ScratchFastTensor):
    name = "ScratchFastTensor";
    break;
  case static_cast<int32_t>(BasePointerIndex::Mem2Mem):
    name = "Mem2Mem";
    break;
  default:
    assert(!"unknown region.");
  }
  return name;
}

std::string get_str_from_attr(mlir::Operation &op, const char *name) {
  auto attr = op.getAttrOfType<StringAttr>(name);
  if (!attr)
    return "";
  else
    return attr.getValue().str();
}

void set_str_to_attr(mlir::OpBuilder &bldr, mlir::Operation &op,
                     const char *name, const std::string &value) {
  StringAttr attr = bldr.getStringAttr(value);
  op.setAttr(Identifier::get(name, bldr.getContext()), attr);
}

ivec get_ivec_from_attr(mlir::Operation &op, const char *name) {
  ivec value;
  auto attr = op.getAttrOfType<ArrayAttr>(name).getValue();
  for (auto &elem : attr) {
    value.emplace_back(elem.dyn_cast<IntegerAttr>().getInt());
  }
  return value;
}

void set_ivec_to_attr(mlir::OpBuilder &bldr, mlir::Operation &op,
                      const char *name, ivec &value) {
  auto attr = bldr.getI32ArrayAttr(ArrayRef<int32_t>(value));
  op.setAttr(Identifier::get(name, bldr.getContext()), attr);
}

fvec get_fvec_from_attr(mlir::Operation &op, const char *name) {
  fvec value;
  auto attr = op.getAttrOfType<ArrayAttr>(name).getValue();
  for (auto &elem : attr) {
    value.emplace_back(elem.dyn_cast<FloatAttr>().getValueAsDouble());
  }
  return value;
}

void set_fvec_to_attr(mlir::OpBuilder &bldr, mlir::Operation &op,
                      const char *name, fvec &value) {
  auto attr = bldr.getF32ArrayAttr(ArrayRef<float>(value));
  op.setAttr(Identifier::get(name, bldr.getContext()), attr);
}

int32_t get_i32_from_attr(mlir::Operation &op, const char *name) {
  return op.getAttrOfType<IntegerAttr>(name).getInt();
}

void set_i32_to_attr(mlir::OpBuilder &bldr, mlir::Operation &op,
                     const char *name, int32_t value) {
  IntegerAttr attr = bldr.getIntegerAttr(bldr.getI32Type(), value);
  op.setAttr(Identifier::get(name, bldr.getContext()), attr);
}

int32_t get_i64_from_attr(mlir::Operation &op, const char *name) {
  return op.getAttrOfType<IntegerAttr>(name).getInt();
}

void set_i64_to_attr(mlir::OpBuilder &bldr, mlir::Operation &op,
                     const char *name, int64_t value) {
  IntegerAttr attr = bldr.getIntegerAttr(bldr.getI64Type(), value);
  op.setAttr(Identifier::get(name, bldr.getContext()), attr);
}

float get_f32_from_attr(mlir::Operation &op, const char *name) {
  return op.getAttrOfType<FloatAttr>(name).getValueAsDouble();
}

void set_f32_to_attr(mlir::OpBuilder &bldr, mlir::Operation &op,
                     const char *name, float value) {
  FloatAttr attr = bldr.getF32FloatAttr(value);
  op.setAttr(Identifier::get(name, bldr.getContext()), attr);
}

bool get_bool_from_attr(mlir::Operation &op, const char *name) {
  return op.getAttrOfType<BoolAttr>(name).getValue();
}

void set_bool_to_attr(mlir::OpBuilder &bldr, mlir::Operation &op,
                      const char *name, bool value) {
  BoolAttr attr = bldr.getBoolAttr(value);
  op.setAttr(Identifier::get(name, bldr.getContext()), attr);
}

bool get_bool_from_attr_with_default(mlir::Operation &op, const char *name,
                                     bool by_default) {
  auto attr = op.getAttrOfType<BoolAttr>(name);
  if (!attr)
    return by_default;
  else
    return attr.getValue();
}

ivecvec get_ivecvec_from_attr(mlir::Operation &op, const char *name) {
  ivecvec data;
  auto vv = op.getAttrOfType<ArrayAttr>(name);
  if (!static_cast<bool>(vv))
    return data;
  for (auto &v : vv.getValue()) {
    auto iv = v.dyn_cast<ArrayAttr>();
    ivec sub_arr;
    for (auto &i : iv) {
      sub_arr.push_back(i.dyn_cast<IntegerAttr>().getInt());
    }
    data.push_back(sub_arr);
  }
  return data;
}

void set_ivecvec_to_attr(mlir::OpBuilder &bldr, mlir::Operation &op,
                         const char *name, ivecvec &value) {
  std::vector<mlir::Attribute> arrays;
  for (auto &v : value) {
    ArrayAttr array = bldr.getI32ArrayAttr(v);
    arrays.push_back(array);
  }
  ArrayAttr attr = bldr.getArrayAttr(arrays);
  op.setAttr(Identifier::get(name, bldr.getContext()), attr);
}

int32_t get_i32_from_attr_with_default(mlir::Operation &op, const char *name,
                                       int32_t by_default) {
  auto attr = op.getAttrOfType<IntegerAttr>(name);
  if (!attr)
    return by_default;
  else
    return attr.getInt();
}

float get_f32_from_attr_with_default(mlir::Operation &op, const char *name,
                                     float by_default) {
  auto attr = op.getAttrOfType<FloatAttr>(name);
  if (!attr)
    return by_default;
  else
    return attr.getValueAsDouble();
}

std::string get_str_from_attr_with_default(mlir::Operation &op,
                                           const char *name,
                                           std::string &by_default) {
  auto attr = op.getAttrOfType<StringAttr>(name);
  if (!attr)
    return by_default;
  else
    return attr.getValue().str();
}

mlir::Operation &getOperationByIndex(mlir::Block &block, int index) {
  int idx = 0;
  for (mlir::Operation &op : block.getOperations()) {
    if (idx != index) {
      idx++;
      continue;
    }
    return op;
  }
}

mlir::Operation &getPrimaryOp(mlir::Operation &kernel) {
  int primary_op_idx = get_i32_from_attr(kernel, "kernel_primary_op_index");
  assert(primary_op_idx >= 0);
  mlir::Operation &primary_op =
      getOperationByIndex(kernel.getRegion(0).front(), primary_op_idx);
  return primary_op;
}

std::vector<mlir::Operation *>
get_operands_from_variadic(mlir::Operation &cmd, const char *range_name) {
  assert(cmd.getName().getStringRef() == "hlcs.npu_stripe_cmd");
  ivec range = get_ivec_from_attr(cmd, range_name);
  std::vector<mlir::Operation *> rst;
  for (int32_t i = range[0]; i < range[1]; i++) {
    // +1 because kernel_info always take position zero.
    mlir::Operation *stripe_mem = cmd.getOperand(i + 1).getDefiningOp();
    rst.push_back(stripe_mem);
  }
  return rst;
}

} // namespace hlcs
} // namespace mlir

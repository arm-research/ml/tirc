/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#ifndef HLCS_MLIR_UTILS_H
#define HLCS_MLIR_UTILS_H

#include <iostream>
#include <string>

#include "mlir/IR/BlockAndValueMapping.h" // from @llvm-project
#include "mlir/IR/Builders.h"             // from @llvm-project
#include "mlir/IR/Operation.h"            // from @llvm-project

#include "src/transforms/llcs_generator/hlcs_npu_api.h" // for ivec & ivecvec

namespace mlir {

namespace hlcs {

//// set and get attributes
std::string get_str_from_attr(mlir::Operation &op, const char *name);
void set_str_to_attr(mlir::OpBuilder &bldr, mlir::Operation &op,
                     const char *name, const std::string &value);

ivec get_ivec_from_attr(mlir::Operation &op, const char *name);
void set_ivec_to_attr(mlir::OpBuilder &bldr, mlir::Operation &op,
                      const char *name, ivec &value);

fvec get_fvec_from_attr(mlir::Operation &op, const char *name);
void set_fvec_to_attr(mlir::OpBuilder &bldr, mlir::Operation &op,
                      const char *name, fvec &value);

int32_t get_i32_from_attr(mlir::Operation &op, const char *name);
void set_i32_to_attr(mlir::OpBuilder &bldr, mlir::Operation &op,
                     const char *name, int32_t value);

int32_t get_i64_from_attr(mlir::Operation &op, const char *name);
void set_i64_to_attr(mlir::OpBuilder &bldr, mlir::Operation &op,
                     const char *name, int64_t value);

float get_f32_from_attr(mlir::Operation &op, const char *name);
void set_f32_to_attr(mlir::OpBuilder &bldr, mlir::Operation &op,
                     const char *name, float value);

bool get_bool_from_attr(mlir::Operation &op, const char *name);
void set_bool_to_attr(mlir::OpBuilder &bldr, mlir::Operation &op,
                      const char *name, bool value);

ivecvec get_ivecvec_from_attr(mlir::Operation &op, const char *name);
void set_ivecvec_to_attr(mlir::OpBuilder &bldr, mlir::Operation &op,
                         const char *name, ivecvec &value);

int32_t get_i32_from_attr_with_default(mlir::Operation &op, const char *name,
                                       int32_t by_default);
float get_f32_from_attr_with_default(mlir::Operation &op, const char *name,
                                     float by_default);
bool get_bool_from_attr_with_default(mlir::Operation &op, const char *name,
                                     bool by_default);
std::string get_str_from_attr_with_default(mlir::Operation &op,
                                           const char *name,
                                           std::string &by_default);

//// helper
std::string get_op_name(mlir::Operation &op);
std::string get_region_name(int32_t region);
mlir::Operation &getOperationByIndex(mlir::Block &block, int index);
mlir::Operation &getPrimaryOp(mlir::Operation &kernel);
std::vector<mlir::Operation *>
get_operands_from_variadic(mlir::Operation &cmd, const char *range_name);

} // namespace hlcs
} // namespace mlir

#endif // HLCS_MLIR_UTILS_H

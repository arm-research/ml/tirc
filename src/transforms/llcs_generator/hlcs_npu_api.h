/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#ifndef HLCS_NPU_API_H
#define HLCS_NPU_API_H

#include <cmath>
#include <vector>

#include "src/utils/data_structure_utils.h"

using namespace mlir::utils;

namespace mlir {
namespace hlcs {

typedef std::vector<int32_t> ivec;
typedef std::vector<std::vector<int32_t>> ivecvec;
typedef std::vector<float> fvec;

// NPU OP API
enum class NpuOperationType : uint32_t {
  Dma = 0,
  Conv2D = 1,
  ConvDepthWise = 2,
  Pooling = 3,
  ElementWise = 4,
};

enum class NpuElementWiseOp : uint32_t {
  ADD = 0,
  SUB = 1,
  MUL = 2,
  ABS = 3,
  MIN = 4,
  MAX = 5,
  LRELU = 6,
  CLZ = 7,
  SHR = 8,
  SHL = 9,
};

enum class NpuPoolingOp : uint32_t {
  MAX = 0,
  AVERAGE = 1,
  REDUCE_SUM = 2,
};

enum class NpuActivationOp : uint32_t {
  NONE_OR_RELU = 0,
  TANH = 1,
  SIGMOID = 2,
  TABLE_LOOKUP = 3,
};

enum class NpuRoundingMode : uint32_t {
  TFL = 0,      // TensorFlow Lite rounding
  TRUNCATE = 1, // Truncate towards zero
  NATURAL = 2,  // Round to nearest with x.5 rounded up, towards +infinity
};

enum class NpuLayout : uint32_t {
  NHWC = 0,
  NHCWB16 = 1,
};

enum class NpuResamplingMode : uint32_t {
  NONE = 0,      // No resampling is performed
  NEAREST = 1,   // 2x2 insert nearest
  TRANSPOSE = 2, // 2x2 transpose
};

enum class NpuBlockTraversal : uint32_t {
  DEFAULT = 0,
  DEPTHWISE = 1,
  DEPTH_FIRST = 2,
  PART_KERNEL_FIRST = 3,
};

enum class NpuBlockType : uint32_t {
  Default = 0,
  ConvolutionMxN = 1,
  VectorProduct = 2,
  Pooling = 3,
  ConvolutionDepthWise = 4,
  ElementWise = 5,
  ReduceSum = 6,
};

struct NpuKernel {
  int32_t width = 0;
  int32_t height = 0;
  int32_t stride_x = 0;
  int32_t stride_y = 0;
  int32_t dilation_x = 0;
  int32_t dilation_y = 0;
};

struct NpuShape3D {
  int32_t height = 0;
  int32_t width = 0;
  int32_t depth = 0;
  NpuShape3D() {}
  NpuShape3D(ivec &&value) {
    assert(value.size() == 3);
    height = value[0];
    width = value[1];
    depth = value[2];
  }
};

struct NpuPadding {
  int32_t top = 0;
  int32_t left = 0;
  int32_t bottom = 0;
  int32_t right = 0;
  NpuPadding() {}
  NpuPadding(ivec &&v) {
    assert(v.size() == 4);
    top = v[0];
    left = v[1];
    bottom = v[2];
    right = v[3];
  }
};

struct NpuAddressRange {
  int32_t region = 0;
  int32_t address = 0;
  int32_t length = 0;
};

struct NpuTileBox {
  int32_t height_0 = 0;
  int32_t height_1 = 0;
  int32_t width_0 = 0;
  ivec addresses = {0, 0, 0, 0};
};

struct NpuActivation {
  NpuActivationOp op_type;
  float min = NAN;
  float max = NAN;
  int32_t lut_index = 0;
  NpuActivation(NpuActivationOp type) : op_type(type) {}
};

struct NpuBlock {
  int32_t width = 0;
  int32_t height = 0;
  int32_t depth = 0;
  NpuBlock(int32_t w, int32_t h, int32_t d) : width(w), height(h), depth(d) {}
  NpuBlock(ivec &shape) {
    ivec shp;
    if (3 > shape.size())
      shp.resize(3 - shape.size(), 1);
    shp.insert(shp.end(), shape.begin(), shape.end());
    int size = shp.size();
    width = shp[size - 2];
    height = shp[size - 3];
    depth = shp[size - 1];
  }
};

// common

enum class MemoryType : uint32_t {
  UNKNOWN = 0,
  PERMANENT_NPU = 1,
  PERMANENT_CPU = 2,
  SCRATCH = 3,
  SCRATCH_FAST = 4,
};

enum class TensorPurpose : uint32_t {
  Unknown = 0,
  Weights = 1,
  FeatureMap = 2,
  Scratch = 3,
  LUT = 4,
  FSBias = 5,
};

enum class TensorSubPurpose : uint32_t {
  Standard = 0,
  DoubleBuffer = 1,
  RollingBufferX = 2,
  RollingBufferY = 3,
  RollingBufferXY = 4,
};

enum class TensorFormat : uint32_t {
  Unknown = 0,
  WeightsCompressed = 1,
  NHWC = 2,
  NHCWB16 = 3,
};

// Area indices must match U55 SHRAM layout spec
enum class SharedBufferArea : uint32_t {
  OFM = 0,
  Weights = 1,
  IFM = 2,
  Accumulators = 3,
  Size = Accumulators + 1
};

enum class BasePointerIndex : uint32_t {
  WeightTensor = 0, // base address index for the Weight tensor
  ScratchTensor =
      1, // base address index for the Scratch_tensor in the TensorArena
  ScratchFastTensor = 2, // base address for the Scratch_fast_tensor
  Mem2Mem =
      (1 << 8) | (3 << 0), // base address slot for memory 2 memory transfer
};

struct Box {
  ivec start_coord;
  ivec end_coord;
  Box(ivec &s_coord, ivec &e_coord) : start_coord(s_coord), end_coord(e_coord) {
    assert(start_coord.size() == end_coord.size());
    int size = start_coord.size();
    for (int i = 0; i < size; ++i) {
      assert(start_coord[i] <= end_coord[i]);
    }
  }

  ivec get_size_shape() {
    ivec size_shape;
    int size = start_coord.size();
    for (int i = 0; i < size; ++i) {
      size_shape.push_back(end_coord[i] - start_coord[i]);
    }
    return size_shape;
  }

  int32_t get_size() {
    ivec size_shape = get_size_shape();
    if (size_shape.empty())
      return 0;
    int size = 1;
    for (int i = 0; size_shape.size(); ++i) {
      size *= size_shape[i];
    }
    return size;
  }

  NpuBlock get_block() {
    ivec size_shape = get_size_shape();
    return NpuBlock(size_shape);
  }
};

struct Block { // from arch
  int32_t width = 0;
  int32_t height = 0;
  int32_t depth = 0;
  Block(int32_t w, int32_t h, int32_t d) : width(w), height(h), depth(d) {}
};

struct Rect {
  int32_t x = 0;
  int32_t y = 0;
  int32_t z = 0;
  int32_t x2 = 0;
  int32_t y2 = 0;
  int32_t z2 = 0;
  PointXYZ start() { return PointXYZ{x, y, z}; }
  PointXYZ end() { return PointXYZ{x2, y2, z2}; }
  Block size() { return Block{x2 - x + 1, y2 - y + 1, z2 - z + 1}; }
  Rect(int32_t _x, int32_t _y, int32_t _z, int32_t _x2, int32_t _y2,
       int32_t _z2)
      : x(_x), y(_y), z(_z), x2(_x2), y2(_y2), z2(_z2) {}
};

struct Kernel {
  int32_t width = 0;
  int32_t height = 0;
  PointXY stride;
  PointXY dilation;
  Kernel(int32_t w, int32_t h, int32_t sx = 1, int32_t sy = 1, int32_t dx = 1,
         int32_t dy = 1)
      : width(w), height(h), stride{sx, sy}, dilation{dx, dy} {}
};

struct InputVolume {
  PointXYZ start_coord;
  PointXYZ end_coord;
  int32_t total_jobs;
};

} // namespace hlcs
} // namespace mlir

#endif // HLCS_NPU_API_H

/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#include <unordered_map>
#include <vector>

#include "src/transforms/llcs_generator/range_set.h"

namespace mlir {
namespace hlcs {

RangeSet &RangeSet::operator|=(const RangeSet &rhs) {
  using std::begin;
  using std::end;
  ranges.insert(end(ranges), begin(rhs.ranges), end(rhs.ranges));
  std::sort(begin(ranges), end(ranges));
  return *this;
}

bool RangeSet::intersects(const RangeSet &b) {
#define MIN(a, b) (((a) < (b)) ? (a) : (b))
#define MAX(a, b) (((a) > (b)) ? (a) : (b))
  const RangeSet &a = *this;
  int a_idx = 0;
  int b_idx = 0;
  int a_len = a.ranges.size();
  int b_len = b.ranges.size();
  while ((a_idx < a_len) && (b_idx < b_len)) {
    const Range &ar = a.ranges[a_idx];
    const Range &br = b.ranges[b_idx];
    if (MAX(ar.start, br.start) < MIN(ar.end, br.end))
      return true;
    // advance one of the two upwards
    if (ar.start < br.start)
      ++a_idx;
    else {
      assert(ar.start != br.start); // never happen
      ++b_idx;
    }
  }
  return false;
#undef MIN
#undef MAX
}

MemoryRangeSet &MemoryRangeSet::operator|=(const MemoryRangeSet &other) {
  for (auto elem : other.regions) {
    regions[elem.first] |= elem.second;
  }
  return *this;
}

bool MemoryRangeSet::intersects(const MemoryRangeSet &other) {
  for (auto elem : regions) {
    auto it = other.regions.find(elem.first);
    if (it == other.regions.end())
      continue;
    if (elem.second.intersects(it->second))
      return true;
  }
  return false;
}

bool MemoryAccessSet::conflicts(const MemoryAccessSet &other) {
  // True dependencies, or write -> read
  if (accesses[e2i(AccessDirection::Write)].intersects(
          other.accesses[e2i(AccessDirection::Read)]))
    return true;

  // Anti-dependencies, or read -> write
  if (accesses[e2i(AccessDirection::Read)].intersects(
          other.accesses[e2i(AccessDirection::Write)]))
    return true;

  // Output dependencies, or write -> write
  if (accesses[e2i(AccessDirection::Write)].intersects(
          other.accesses[e2i(AccessDirection::Write)]))
    return true;

  // read -> read does not cause a conflict
  return false;
}

// binary operator
bool operator<(const Range &lhs, const Range &rhs) {
  if (lhs.start < rhs.start)
    return true;
  if (lhs.start > rhs.start)
    return false;
  if (lhs.end < rhs.end)
    return true;
  return false;
}

RangeSet operator|(RangeSet lhs, const RangeSet &rhs) {
  lhs |= rhs;
  return lhs;
}

MemoryRangeSet operator|(MemoryRangeSet lhs, const MemoryRangeSet &rhs) {
  lhs |= rhs;
  return lhs;
}

// dump format
llvm::raw_ostream &operator<<(llvm::raw_ostream &os, const Range &r) {
  os << (const void *)(r.start) << ":" << (const void *)(r.end);
  return os;
}

llvm::raw_ostream &operator<<(llvm::raw_ostream &os, const RangeSet &r) {
  os << "<RangeSet [";
  int s = r.ranges.size();
  for (int i = 0; i < s; i++) {
    if (i > 0)
      os << ", ";
    os << r.ranges[i];
  }
  os << "]>";
  return os;
}

llvm::raw_ostream &operator<<(llvm::raw_ostream &os,
                              const MemoryRangeSet &mrs) {
  os << "<MemoryRangeSet> ";
  for (auto elem : mrs.regions) {
    os << elem.first << ": " << elem.second << "\t";
  }
  return os;
}

llvm::raw_ostream &operator<<(llvm::raw_ostream &os, MemoryAccessSet &mas) {
  os << "<MemoryAccessSet>\n";
  os << "  Read: " << mas.accesses[e2i(AccessDirection::Read)] << "\n";
  os << "  Write: " << mas.accesses[e2i(AccessDirection::Write)] << "\n\n";
  return os;
}

} // namespace hlcs
} // namespace mlir

/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#ifndef RANGE_SET_H
#define RANGE_SET_H

#include <assert.h>

#include <string>
#include <type_traits>
#include <typeinfo>
#include <unordered_map>

#include "llvm/Support/raw_ostream.h"

namespace mlir {
namespace hlcs {

#ifndef ENUM_TO_INT
#define ENUM_TO_INT
template <typename Enum> constexpr auto e2i(Enum e) noexcept {
  return static_cast<std::underlying_type_t<Enum>>(e);
}
#endif // ENUM_TO_INT

enum class AccessDirection { Read, Write, Size };
enum class MemArea {
  Unknown,
  Sram,
  Dram,
  OnChipFlash,
  OffChipFlash,
  Shram,
  Size
}; // Shram for LUT

typedef unsigned long long addr_t;
struct Range {
private:
  addr_t start;
  addr_t end;

public:
  Range() : start(0), end(0) {}
  Range(addr_t s, addr_t e) : start(s), end(e) { assert(start <= end); }
  void print(llvm::raw_ostream &os) { os << *this; }
  friend bool operator<(const Range &lhs, const Range &rhs);
  friend llvm::raw_ostream &operator<<(llvm::raw_ostream &os, const Range &r);
  friend class RangeSet;

  // debug only
  addr_t _get_start() { return start; }
  addr_t _get_end() { return end; }
};

// A Range set class to track ranges and whether they intersect.
// Intended for e.g. tracking sets of memory ranges and whether two commands use
// the same memory areas.
class RangeSet {
private:
  std::vector<Range> ranges;

public:
  RangeSet() {}
  RangeSet(addr_t s, addr_t e) { ranges.emplace_back(s, e); }
  RangeSet(std::vector<Range> &&r) { std::swap(ranges, r); }

  // union
  RangeSet &operator|=(const RangeSet &rhs);
  friend RangeSet operator|(RangeSet lhs, const RangeSet &rhs);

  // detect overlap
  bool intersects(const RangeSet &other);

  // output
  void print(llvm::raw_ostream &os) { os << *this; }
  friend llvm::raw_ostream &operator<<(llvm::raw_ostream &os,
                                       const RangeSet &r);

  // debug only
  std::vector<Range> &_get_ranges() { return ranges; }
};

// Extended version of the RangeSet class that handles having different memory
// areas
class MemoryRangeSet {
private:
  std::unordered_map<std::string, RangeSet> regions;

public:
  MemoryRangeSet() {}
  MemoryRangeSet(std::unordered_map<std::string, RangeSet> &&r) {
    std::swap(regions, r);
  }
  MemoryRangeSet(const std::string &area, addr_t start, addr_t end) {
    regions[area] = std::move(RangeSet(start, end));
  }

  // union
  MemoryRangeSet &operator|=(const MemoryRangeSet &rhs);
  friend MemoryRangeSet operator|(MemoryRangeSet lhs,
                                  const MemoryRangeSet &rhs);

  // detect overlap
  bool intersects(const MemoryRangeSet &other);

  // output
  void print(llvm::raw_ostream &os) { os << *this; }
  friend llvm::raw_ostream &operator<<(llvm::raw_ostream &os,
                                       const MemoryRangeSet &mrs);

  // debug only
  std::unordered_map<std::string, RangeSet> &_get_regions() { return regions; }
};

// Tracks memory ranges, but also access patterns to know which accesses
// actually are in conflict
class MemoryAccessSet {
private:
  std::vector<MemoryRangeSet> accesses =
      std::vector<MemoryRangeSet>(e2i(AccessDirection::Size));

public:
  MemoryAccessSet() {}

  void add(const MemoryRangeSet &mrs, AccessDirection access) {
    accesses[e2i(access)] |= mrs;
  }

  // detect conflicts without lru_cache
  bool conflicts(const MemoryAccessSet &other);

  // output
  void print(llvm::raw_ostream &os) { os << *this; }
  friend llvm::raw_ostream &operator<<(llvm::raw_ostream &os,
                                       MemoryAccessSet &mas);

  // debug only
  std::vector<MemoryRangeSet> &_get_accesses() { return accesses; }
};

} // namespace hlcs
} // namespace mlir
#endif // RANGE_SET_H

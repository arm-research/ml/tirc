/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#include <vector>

#include "mlir/IR/Operation.h" // from @llvm-project

#include "src/transforms/llcs_generator/hlcs_mlir_utils.h"
#include "src/transforms/llcs_generator/hlcs_npu_api.h"
#include "src/transforms/llcs_generator/tensor_info.h"

namespace mlir {
namespace hlcs {

ivec convToI32Vector(ArrayRef<Attribute> &attr) {
  ivec result;
  for (auto &elem : attr) {
    result.emplace_back(elem.dyn_cast<IntegerAttr>().getInt());
  }
  return std::move(result);
}

int32_t getElementSize(mlir::Operation &tensor_info) {
  int32_t element_size_bytes =
      tensor_info.getAttrOfType<IntegerAttr>("element_size_bytes").getInt();
  if (element_size_bytes == 0) {
    std::string dtype =
        tensor_info.getAttrOfType<StringAttr>("dtype").getValue().str();
    int32_t size_in_bits = 8;
    if ((dtype == "int8") || (dtype == "uint8"))
      size_in_bits = 8;
    else if ((dtype == "int16") || (dtype == "uint16"))
      size_in_bits = 16;
    else if ((dtype == "int32") || (dtype == "int32") || (dtype == "float"))
      size_in_bits = 32;
    else if (dtype == "float64")
      size_in_bits = 64;
    else
      assert(!"unknown dtype");
    element_size_bytes = size_in_bits / 8;
  }
  return element_size_bytes;
}

int32_t getShapeNumElements(ivec &shape) {
  int32_t elems = 1;
  size_t len = shape.size();
  for (int i = 0; i < len; ++i) {
    elems *= shape[i];
  }
  return elems;
}

ivec_pair getStridesAndCoord(mlir::Operation &tensor_info, ivec &coord) {
  ivec storage_shape = get_ivec_from_attr(tensor_info, "storage_shape");
  if (coord.empty()) {
    coord.resize(storage_shape.size());
  }

  ivec augmented_coord(coord);
  ivec augmented_shape(storage_shape);
  if (augmented_shape.size() < 4)
    augmented_shape.insert(augmented_shape.begin(), 4 - augmented_shape.size(),
                           1);
  if (augmented_coord.size() < 4)
    augmented_coord.insert(augmented_coord.begin(), 4 - augmented_coord.size(),
                           0);

  assert(augmented_shape.size() == augmented_coord.size());

  TensorFormat format =
      static_cast<TensorFormat>(get_i32_from_attr(tensor_info, "format"));
  if (format == TensorFormat::NHWC) {
    auto &s = augmented_shape;
    auto &c = augmented_coord;
    augmented_shape = ivec({s[0], s[3], s[1], s[2], 1});
    augmented_coord = ivec({c[0], c[3], c[1], c[2], 0});

  } else if (format == TensorFormat::NHCWB16) {
    if (augmented_shape[1] == 0)
      augmented_shape[1] = 1;
    augmented_shape.push_back(1);

    auto &c = augmented_coord;
    const int32_t channel_divisor = 16;
    augmented_coord = ivec(
        {c[0], c[3] / channel_divisor, c[1], c[2], c[3] % channel_divisor});
  } else {
    assert((format == TensorFormat::Unknown) ||
           (format == TensorFormat::WeightsCompressed));
    // return None, None
    assert(!"Doesn't support!");
  }

  float storage_compression_scale =
      get_f32_from_attr(tensor_info, "storage_compression_scale");
  ivec strides(augmented_shape.size());
  assert(strides.size() == 5);
  int32_t stride = static_cast<int32_t>(
      getElementSize(tensor_info) * storage_compression_scale + 0.5);

  if (format != TensorFormat::NHCWB16) {
    assert(format == TensorFormat::NHWC);
    ivec stride_order{4, 1, 3, 2, 0};
    for (int i : stride_order) {
      strides[i] = stride;
      stride *= augmented_shape[i];
    }
  } else {
    strides[4] = stride;
    strides[3] = 16 * stride;                                      // STRIDE_X
    strides[1] = strides[3] * augmented_shape[2];                  // STRIDE_C
    strides[2] = augmented_shape[2] * augmented_shape[3] * stride; // STRIDE_Y
    strides[0] = strides[2] * augmented_shape[1];                  // STRIDE_N
  }
  return {strides, augmented_coord};
}

ivec_pair getStridesAndCoord(mlir::Operation &tensor_info) {
  ivec coord;
  return std::move(getStridesAndCoord(tensor_info, coord));
}

int32_t getStorageSize(mlir::Operation &tensor_info, double scale) {
  ivec storage_shape = get_ivec_from_attr(tensor_info, "storage_shape");
  int32_t alignment =
      tensor_info.getAttrOfType<IntegerAttr>("alignment").getInt();

  double raw_size =
      getShapeNumElements(storage_shape) * getElementSize(tensor_info) * scale;
  raw_size = std::max(raw_size, 1.0);
  int32_t rounded_size =
      (static_cast<int32_t>(raw_size + 0.5) + alignment - 1) / alignment *
      alignment;
  return rounded_size;
}

int32_t getCompressedStreamIndexFromCoord(mlir::Operation &tensor_info,
                                          ivec &coord) {
  /* auto brick_size_attr =
   * tensor_info.getAttrOfType<ArrayAttr>("brick_size").getValue(); */
  /* ivec brick_size = convToI32Vector(brick_size_attr); */
  ivec brick_size = get_ivec_from_attr(tensor_info, "brick_size");
  ivec shape = get_ivec_from_attr(tensor_info, "shape");
  auto weight_compressed_offsets_attr =
      tensor_info.getAttrOfType<ArrayAttr>("weight_compressed_offsets")
          .getValue();
  ivec weight_compressed_offsets =
      convToI32Vector(weight_compressed_offsets_attr);

  size_t len = coord.size();
  int32_t depth = coord[len - 1];
  int32_t brick_depth = brick_size[len - 1];
  depth = std::min(depth, shape[len - 1]);
  int32_t index = (depth + brick_depth - 1) / brick_depth;

  if (index < (weight_compressed_offsets.size() - 1)) {
    assert(depth % brick_depth == 0);
  }

  return index;
}

int32_t getSizeOfCompressedStream(mlir::Operation &tensor_info,
                                  int32_t stream_index) {
  auto substream_offsets_attr = tensor_info.getAttrOfType<ArrayAttr>(
      "compressed_values_substream_offsets");
  assert(substream_offsets_attr);
  auto substream_offsets = substream_offsets_attr.getValue();
  assert(0 <= stream_index && stream_index < substream_offsets.size());
  auto offsets_attr = substream_offsets[stream_index].dyn_cast<ArrayAttr>();
  int32_t index = offsets_attr.size() - 1;
  int32_t size = offsets_attr[index].dyn_cast<IntegerAttr>().getInt();
  return size;
}

bool needs_dma(mlir::Operation &tensor_info) {
  // def needs_dma(self):
  //   return len(self.ops) == 1 and self.ops[0].type == "DMA"
  return get_bool_from_attr(tensor_info, "dma_op_result");
}

int32_t getAddressOffsetForCoordinate(mlir::Operation &tensor_info,
                                      ivec &orig_coord, bool is_top_box) {
  int32_t address_offset = 0;
  TensorSubPurpose sub_purpose = static_cast<TensorSubPurpose>(
      get_i32_from_attr(tensor_info, "sub_purpose"));
  TensorFormat format =
      static_cast<TensorFormat>(get_i32_from_attr(tensor_info, "format"));

  ivec storage_shape = get_ivec_from_attr(tensor_info, "storage_shape");
  ivec shape = get_ivec_from_attr(tensor_info, "shape");
  ivec brick_size = get_ivec_from_attr(tensor_info, "brick_size");

  // coord = coord[-len(self.storage_shape) :]
  assert(orig_coord.size() == storage_shape.size());
  ivec coord = orig_coord;

  size_t len = coord.size();
  if (sub_purpose == TensorSubPurpose::Standard) {
    for (size_t i = 0; i < len; ++i) {
      if (is_top_box)
        assert((coord[i] > 0) && (coord[i] <= shape[i]));
      else
        assert((coord[i] >= 0) && (coord[i] < shape[i]));
    }
  }

  if (format == TensorFormat::WeightsCompressed) {
    ivec weight_compressed_offsets =
        get_ivec_from_attr(tensor_info, "weight_compressed_offsets");
    if (weight_compressed_offsets.empty())
      return 0;

    if ((sub_purpose == TensorSubPurpose::DoubleBuffer) &&
        needs_dma(tensor_info)) {
      int32_t depth = orig_coord[len - 1];
      int32_t brick_depth = brick_size[len - 1];
      // Clamp position at final element index
      depth = std::min(depth, shape[len - 1]);
      // Always round up to next boundary
      int32_t index = ((depth + brick_depth - 1) / brick_depth) % 2;

      auto compressed = get_ivecvec_from_attr(
          tensor_info, "compressed_values_substream_offsets");
      if (compressed.size() <= 2) {
        if (is_top_box && (index == 0))
          for (auto &cv : compressed)
            address_offset += cv[1];
        else
          address_offset = index * compressed[0][1];
      } else {
        if (is_top_box && (index == 0))
          address_offset = storage_shape[len - 1];
        else
          address_offset = index * (storage_shape[len - 1] / 2);
      }
    } else {
      int32_t index =
          getCompressedStreamIndexFromCoord(tensor_info, orig_coord);
      assert(index < weight_compressed_offsets.size());
      address_offset = weight_compressed_offsets[index];
    }
  } else {
    // handle wraparound for partial buffers. make sure to do this after
    // subtracting top box:
    for (int i = 0; i < len; ++i) {
      if (is_top_box)
        coord[i] -= 1;
      coord[i] %= storage_shape[i];
    }

    auto sc = getStridesAndCoord(tensor_info, coord);
    auto strides = sc.first;
    auto augmented_coord = sc.second;

    // if strides is None: return None
    if (is_top_box) {
      address_offset += 1 * strides[len - 1]; // one element
    }

    for (int i = 0; i < augmented_coord.size(); ++i) {
      address_offset += augmented_coord[i] * strides[i];
    }
  }
  assert(address_offset >= 0);
  assert(address_offset <= getStorageSize(tensor_info));
  return address_offset;
}

int32_t getAddressForCoordinate(mlir::Operation &tensor_info, ivec &orig_coord,
                                bool is_top_box) {
  int32_t address = get_i32_from_attr(tensor_info, "address");
  int32_t offset =
      getAddressOffsetForCoordinate(tensor_info, orig_coord, is_top_box);
  return address + offset;
}

ivec get_size_shape(ivec &start_coord, ivec &end_coord) {
  assert(start_coord.size() == end_coord.size());
  ivec result(end_coord);
  for (size_t i = 0; i < result.size(); i++) {
    result[i] -= start_coord[i];
  }
  return std::move(result);
}

#define ENABLE_LOGGING 0

std::tuple<int32_t, int32_t, int32_t, std::vector<int32_t>>
getAddressesForRollingBuffer(mlir::Operation &tensor_info, ivec &start_coord,
                             ivec &end_coord) {
#if ENABLE_LOGGING
  std::cout << std::endl;
  std::cout << "getAddressesForRollingBuffer::Start" << std::endl;
#endif

  // returns ( box_height0, box_height1, box_width, [address_tl, address_tr,
  // address_bl, address_br] )
  int32_t start_addr = getAddressForCoordinate(tensor_info, start_coord, false);
  int32_t box_height0 = 0;
  int32_t box_width = 0;
  size_t len = start_coord.size();
  ivec addresses{start_addr, 0, 0, 0};

  if (len < 4) {
    box_height0 = 1;
    box_width = 1;
    if (len >= 2) {
      box_width = end_coord[len - 2] - start_coord[len - 2];
    }
    return {box_height0, box_height0, box_width, addresses};
  }

  auto storage_shape_attr =
      tensor_info.getAttrOfType<ArrayAttr>("storage_shape").getValue();
  ivec storage_shape = convToI32Vector(storage_shape_attr);

#if ENABLE_LOGGING
  std::cout << "Storage Shape: " << storage_shape[0] << " " << storage_shape[1]
            << " " << storage_shape[2] << " " << storage_shape[3] << std::endl;
  std::cout << "Start Coord: " << start_coord[0] << " " << start_coord[1] << " "
            << start_coord[2] << " " << start_coord[3] << std::endl;
  std::cout << "End Coord: " << end_coord[0] << " " << end_coord[1] << " "
            << end_coord[2] << " " << end_coord[3] << std::endl;
  std::cout << "(0) Address: 0x" << std::hex << start_addr << std::dec
            << std::endl;
#endif

  int32_t crossing_y =
      (start_coord[1] + storage_shape[1]) / storage_shape[1] * storage_shape[1];
  int32_t crossing_x =
      (start_coord[2] + storage_shape[2]) / storage_shape[2] * storage_shape[2];

  crossing_y = std::min(crossing_y, end_coord[1]);
  crossing_x = std::min(crossing_x, end_coord[2]);

  box_height0 = crossing_y - start_coord[1];
  box_width = crossing_x - start_coord[2];

  if (end_coord[2] > crossing_x) {
    // addresses[1] = getAddressForCoordinate([start_coord[0], start_coord[1],
    // crossing_x, start_coord[3]])
    assert(!"Striping in vertical direction is not supported!");
    exit(1);
  }
  if (end_coord[1] > crossing_y) {
    ivec tmp_coord = {start_coord[0], crossing_y, start_coord[2],
                      start_coord[3]};
    addresses[2] = getAddressForCoordinate(tensor_info, tmp_coord, false);
#if ENABLE_LOGGING
    std::cout << "(2) Address: 0x" << std::hex << addresses[2] << std::dec
              << std::endl;
#endif
  }
  if (end_coord[1] > crossing_y && end_coord[2] > crossing_x) {
    ivec tmp_coord = {start_coord[0], crossing_y, crossing_x, start_coord[3]};
    addresses[3] = getAddressForCoordinate(tensor_info, tmp_coord, false);
#if ENABLE_LOGGING
    std::cout << "(3) Address: 0x" << std::hex << addresses[3] << std::dec
              << std::endl;
#endif
  }

#if ENABLE_LOGGING

#endif

#if ENABLE_LOGGING
  std::cout << "getAddressesForRollingBuffer::End" << std::endl;
  std::cout << std::endl;
#endif

  return {box_height0, box_height0, box_width, addresses};
}

} // namespace hlcs
} // namespace mlir

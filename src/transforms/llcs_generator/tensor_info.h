/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#ifndef TENSOR_INFO_H
#define TENSOR_INFO_H

#include <assert.h>

#include <string>
#include <type_traits>
#include <typeinfo>
#include <unordered_map>

#include "llvm/Support/raw_ostream.h"

#include "src/transforms/llcs_generator/range_set.h"

namespace mlir {
namespace hlcs {

typedef std::vector<int32_t> ivec;
typedef std::vector<std::vector<int32_t>> ivecvec;
typedef std::pair<ivec, ivec> ivec_pair;

ivec convToI32Vector(ArrayRef<Attribute> &attr);

int32_t getElementSize(mlir::Operation &tensor_info);

int32_t getShapeNumElements(ivec &shape);

ivec_pair getStridesAndCoord(mlir::Operation &tensor_info, ivec &coord);

ivec_pair getStridesAndCoord(mlir::Operation &tensor_info);

int32_t getStorageSize(mlir::Operation &tensor_info, double scale = 1.0);

int32_t getCompressedStreamIndexFromCoord(mlir::Operation &tensor_info,
                                          ivec &coord);

int32_t getSizeOfCompressedStream(mlir::Operation &tensor_info,
                                  int32_t stream_index);

int32_t getAddressOffsetForCoordinate(mlir::Operation &tensor_info,
                                      ivec &orig_coord,
                                      bool is_top_box = false);

int32_t getAddressForCoordinate(mlir::Operation &tensor_info, ivec &orig_coord,
                                bool is_top_box = false);

MemoryRangeSet getAddressRangesForCoordinates(mlir::Operation &tensor_info,
                                              ivec &start_coord,
                                              ivec &end_coord);

ivec get_size_shape(ivec &start_coord, ivec &end_coord);

std::tuple<int32_t, int32_t, int32_t, std::vector<int32_t>>
getAddressesForRollingBuffer(mlir::Operation &tensor_info, ivec &start_coord,
                             ivec &end_coord);
} // namespace hlcs
} // namespace mlir
#endif // TENSOR_INFO_H

/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#include "src/transforms/memory_organizer/GreedyAllocator.h"

using namespace mlir;

namespace mlir {
namespace planner {

// This is broken and needs updating

void alloc(unsigned &total_sz, LiveRange new_LiveRange,
           std::list<std::pair<unsigned, LiveRange>> &current_allocations) {
  /*
      unsigned size         = new_LiveRange.get_storage_size();
      unsigned aligned_size = round_up(size, new_LiveRange.get_alignment());

      unsigned address      = 0;
      unsigned address_fit  = ((1 << 64) - 1);

      // 0. Calculate the new address by finding the end of the last block
      unsigned current_top  = 0;
      for (auto p: current_allocations)
      {
          unsigned  alloc_start_address = p.first;
          LiveRange alloc_lr            = p.second;
          current_top                   = std::max(current_top,
  alloc_start_address+alloc_lr.get_storage_size());
      }
      address = round_up(current_top, new_LiveRange.get_alignment());

  #if TODO
      // 1. Calculate the new address by trying to slot it in
      unsigned current_offset = 0;
      for start_addr, lr in self.current_allocs:
          aligned_current_offset = numeric_util.round_up(current_offset,
  new_lr.get_alignment()) if ((aligned_current_offset + aligned_size <=
  start_addr) and (start_addr - current_offset < address_fit)): address     =
  current_offset address_fit = start_addr - current_offset current_offset =
  start_addr + lr.size #endif

      // 2. Update Value Addresses
      new_LiveRange.set_address(address);
      new_LiveRange.set_live_range();

      // 3. Register the memory required
      total_sz = max(total_sz, address + aligned_size);

      // 4. Store the new allocation
      current_allocations.emplace_back(address, new_LiveRange);
      sort_allocations(current_allocations);
  */
}

void dealloc(
    std::list<std::pair<unsigned, LiveRange>>::iterator alloc_LiveRange_it,
    std::list<std::pair<unsigned, LiveRange>> &current_allocations) {
  /*
      current_allocations.erase(alloc_LiveRange_it);
  */
}

unsigned
greedy_allocation(mlir::planner::LiveRangeGraph &lrg,
                  value_mem_area target_value_mem_area,
                  mlir::ArchitectureConfig::ArchitectureConfigAttr &arch_c) {
  unsigned total_sz = 0;
  /*
      std::set<::llvm::hash_code> allocated_values;

      std::list<std::tuple<signed, signed, LiveRange>> ranges_sorted =
     sort_ranges(lrg.ranges);

      std::list<std::pair<unsigned, LiveRange>> current_allocations;

      // 0. Iterate over all the Live Ranges we need to Alloc after being sorted
      for (auto new_LiveRange_tuple: ranges_sorted)
      {
          signed    new_LiveRange_start_time = std::get<0>(new_LiveRange_tuple);
          signed    new_LiveRange_end_time   = std::get<1>(new_LiveRange_tuple);
          LiveRange new_LiveRange            = std::get<2>(new_LiveRange_tuple);

          // 1. Iterate over all the current Allocations and try to clear any
     allocations which have now expired
          //    If the end time of any alloc is smaller than the start time of
     the next alloc than clearly this is not needed any more so deallocate bool
     deleted = false; do { deleted = false; std::list<std::pair<unsigned,
     LiveRange>>::iterator alloc_LiveRange_it; for (alloc_LiveRange_it =
     current_allocations.begin(); alloc_LiveRange_it !=
     current_allocations.end(); alloc_LiveRange_it++)
              {
                  signed  alloc_LiveRange_it_end_time =
     alloc_LiveRange_it->second.get_end_time(); if (alloc_LiveRange_it_end_time
     < new_LiveRange_start_time)
                  {
                      dealloc(alloc_LiveRange_it, current_allocations);
                      deleted = true;
                      break;
                  }
              }
          } while(deleted);

          // 2. Allocate the new Live Range
          alloc(total_sz, new_LiveRange, current_allocations);
      }
  */
  return total_sz;
}

} // namespace planner
} // namespace mlir

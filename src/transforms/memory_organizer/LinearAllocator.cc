/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#include "src/transforms/memory_organizer/LinearAllocator.h"

using namespace mlir;

namespace mlir {
namespace planner {

unsigned
linear_allocation(mlir::planner::LiveRangeGraph &lrg,
                  mlir::ArchitectureConfig::ArchitectureConfigAttr &arch_c) {
  LOG(ENABLE_LOGGING_MEMORY_ORGANIZER_VERBOSE_LEVEL_1,
      "Linear Allocator::Start");

  int32_t sram_size = arch_c.sram_size().getInt();
  unsigned alloc_granularity = arch_c.allocation_alignment().getInt();

  /*
     Note LiveRanges are not stored sorted in terms of start/end time so
     allocation does not represent the graph toplogy which is okay in this
     simpe memory allocator
   */

  unsigned total_sz = 0;

  std::vector<LiveRange *> sorted_ranges = lrg.get_sorted_ranges();
  for (auto lr : sorted_ranges) {
    unsigned address = total_sz;
    // std::cout << "Allocated Address: " << address << std::endl;

    total_sz += round_up<uint64_t>(lr->storage_size, alloc_granularity);
    if (total_sz > sram_size)
      FATAL_ERROR("linear_allocation::The Total Size required is bigger than "
                  "the sram_size");

    // Configure Address for all Values
    lr->set_values_address(address);
    lr->set_values_live_range();
  }

  if (not(lrg.verify_allocation(arch_c)))
    FATAL_ERROR(
        "linear_allocation::verification::Live Range Graph Allocation failed");

  LOG(ENABLE_LOGGING_MEMORY_ORGANIZER_VERBOSE_LEVEL_1, "Linear Allocator::End");
  return total_sz;
}

} // namespace planner
} // namespace mlir

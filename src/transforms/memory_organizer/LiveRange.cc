/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#include "src/transforms/memory_organizer/LiveRange.h"

using namespace mlir;

namespace mlir {
namespace planner {

ostream &operator<<(ostream &os, const LiveRange &lr) {
  os << "Live Range -> No Of Values " << lr.values.size() << " UniqueId: "
     << lr.values[0].getType().dyn_cast<mlir::SchedTensorType>().getUniqueId()
     << " Start Time: " << lr.start_time << " End Time: " << lr.end_time
     << " Tensor Size: " << lr.storage_size;
  return os;
}

LiveRange::LiveRange() {
  start_time = 0x7FFFFFFF;
  end_time = -1;
  this->storage_size = 0;
  this->storage_shape = {};
  alignment = 16;
}

LiveRange::LiveRange(unsigned alignment) {
  start_time = 0x7FFFFFFF;
  end_time = -1;
  this->storage_size = 0;
  this->storage_shape = {};
  alignment = alignment;
}

void LiveRange::add_value(Value v) {
  ASSERT_COND((not(checkTensorType<SchedTensorType>(v.getType()))),
              "LiveRange::add_tensor v is not SchedType");
  SchedTensorType t = v.getType().dyn_cast<SchedTensorType>();
  ArrayRef<int64_t> v_storage_shape = t.getStorageShape();
  if (array_prod(this->storage_size) == 0) {
    this->storage_shape = v_storage_shape;
    this->storage_size = array_prod(v_storage_shape);
    // Allocate a size that is alligned
    while ((this->storage_size % 16) != 0) {
      storage_size += 1;
    }
  } else {
    ASSERT_COND(storage_size != array_prod(v_storage_shape),
                "Values assigned to the same LiveRange need to fit the size of "
                "the LiveRange.");
  }
  values.push_back(v);
}

void LiveRange::mark_usage(unsigned op_time) {
  if (op_time == -1)
    return;
  int32_t op_time_start = op_time;
  int32_t op_time_end = op_time + 1;
  start_time = std::min(start_time, op_time_start);
  end_time = std::max(end_time, op_time_end);
}

void LiveRange::set_values_address(unsigned address) {
  for (auto v : values)
    v.getType().dyn_cast<SchedTensorType>().set_address(address);
}

void LiveRange::set_values_live_range() {
  for (auto v : values) {
    v.getType().dyn_cast<SchedTensorType>().set_start_time(start_time);
    v.getType().dyn_cast<SchedTensorType>().set_end_time(end_time);
  }
}

void LiveRange::set_values_pin_alloc_size(int64_t pinned_size) {
  for (auto v : values)
    v.getType().dyn_cast<SchedTensorType>().set_pinned_size(pinned_size);
}

void LiveRange::set_values_pin_location(value_mem_area pinned_mem_area) {
  for (auto v : values)
    v.getType().dyn_cast<SchedTensorType>().set_pinned_mem_area(
        pinned_mem_area);
}

bool LiveRange::overlaps_ranges(const LiveRange lr) {
  return (std::max(start_time, lr.start_time) <
          std::min(end_time, lr.end_time));
}

// Returns the first pair of values in this LiveRange and 'other' which have
// overlapping addresses
bool LiveRange::overlaps_address(const LiveRange lr,
                                 SmallVector<Value, 2> &overlap_values) {
  for (auto v1 : values) {
    for (auto v2 : lr.values) {
      unsigned v1_address =
          v1.getType().dyn_cast<SchedTensorType>().get_address();
      unsigned v2_address =
          v2.getType().dyn_cast<SchedTensorType>().get_address();
      if (std::max(v1_address, v2_address) <
          std::min(v1_address + storage_size, v2_address + lr.storage_size)) {
        overlap_values.push_back(v1);
        overlap_values.push_back(v2);
        return true;
      }
    }
  }
  overlap_values.clear();
  return false;
}

bool LiveRange::operator<(const LiveRange &lr) {
  if (start_time != lr.start_time)
    return start_time < lr.start_time;
  if (end_time != lr.end_time)
    return end_time < lr.end_time;
  if (storage_size != lr.storage_size)
    return storage_size < lr.storage_size;
  return false;
}

} // namespace planner
} // namespace mlir

/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#ifndef LIVE_RANGE_H
#define LIVE_RANGE_H

#include "mlir/Dialect/Quant/QuantOps.h"
#include "mlir/Dialect/Quant/QuantTypes.h"
#include "mlir/Dialect/StandardOps/IR/Ops.h"      // from @llvm-project
#include "mlir/IR/AffineMap.h"                    // from @llvm-project
#include "mlir/IR/Attributes.h"                   // from @llvm-project
#include "mlir/IR/BlockAndValueMapping.h"         // from @llvm-project
#include "mlir/IR/Builders.h"                     // from @llvm-project
#include "mlir/IR/Location.h"                     // from @llvm-project
#include "mlir/IR/MLIRContext.h"                  // from @llvm-project
#include "mlir/IR/Operation.h"                    // from @llvm-project
#include "mlir/IR/PatternMatch.h"                 // from @llvm-project
#include "mlir/Interfaces/InferTypeOpInterface.h" // from @llvm-project
#include "mlir/Pass/Pass.h"                       // from @llvm-project
#include "mlir/Transforms/DialectConversion.h"    // from @llvm-project

#include <iostream>

using namespace std;

#include "src/ir/architecture_config/architecture_config.h"
#include "src/utils/checkIR.h"

#include "src/ir/schedule_ir.h"

namespace mlir {
namespace planner {

class LiveRange {
public:
  LiveRange();
  LiveRange(unsigned alignment);

  void add_value(Value v);
  void mark_usage(unsigned op_time);

  void set_values_address(unsigned address);
  void set_values_live_range();

  void set_values_pin_alloc_size(int64_t pinned_size);
  void set_values_pin_location(value_mem_area pinned_mem_area);

  bool overlaps_ranges(const LiveRange lr);

  // Returns the first pair of values in this LiveRange and 'other' which have
  // overlapping addresses
  bool overlaps_address(const LiveRange lr,
                        SmallVector<Value, 2> &overlap_values);

  bool overlaps(uint32_t addr2, uint32_t size2) const {
    return address < addr2 + size2 && addr2 < end_address;
  }
  bool is_neighbour(const LiveRange &lr) const {
    return start_time <= lr.end_time && lr.start_time <= end_time;
  }

  bool operator<(const LiveRange &lr);

  friend ostream &operator<<(ostream &os, const LiveRange &lr);

  /** Storage Size in bytes (input to the allocator algorithm) */
  uint32_t storage_size;
  /** Start time (input to the allocator algorithm) */
  int32_t start_time;
  /** End time, inclusive (input to the allocator algorithm) */
  int32_t end_time;

  /** Values that are assigned to the same LiveRange will be allocated to the
   * same address */
  SmallVector<Value, 1> values;
  /** The Sched Tensor Type Storage Shape */
  ArrayRef<int64_t> storage_shape;
  /** Alignment needed for this Live Range Block */
  unsigned alignment;

  /** (Hill Climb Working Variable: Not Valid outside search) Index of this live
   * range */
  int id;
  /** (Hill Climb Working Variable: Not Valid outside search) Allocated address
   * (the main output from the allocator algorithm) */
  uint32_t address;
  /** (Hill Climb Working Variable: Not Valid outside search) End address,
   * exclusive */
  uint32_t end_address;
  /** (Hill Climb Working Variable: Not Valid outside search) id of predecessor
   * live range (predecessor's end address == this lr's address) */
  int predecessor;
  /** (Hill Climb Working Variable: Not Valid outside search) Turn at which the
   * live range was allocated */
  size_t turn;
};

} // namespace planner
} // namespace mlir

#endif // LIVE_RANGE_H

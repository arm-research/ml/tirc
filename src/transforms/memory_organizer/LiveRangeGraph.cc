/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#include "src/transforms/memory_organizer/LiveRangeGraph.h"

namespace mlir {
namespace planner {

LiveRangeGraph::LiveRangeGraph() { current_time = 0; }

LiveRange *LiveRangeGraph::get_or_create_range(Value v, unsigned alignment) {
  SchedTensorType type = v.getType().dyn_cast<mlir::SchedTensorType>();

  // Return the live range of the tensor
  auto unique_id = type.getUniqueId();

  if (ranges.find(unique_id) == ranges.end()) {
    auto rng = make_unique<LiveRange>(alignment);
    rng.get()->add_value(v);
    rng.get()->alignment = alignment;
    ranges[unique_id] = std::move(rng);
    return ranges[unique_id].get();
  } else {
    /*
       You need to add it as altugh they share the same Unique Id
       the Types could be fdifferent as they are keyed on the shape
       and in the case of memory ops the shapes of the common Types could be
       different
     */
    ranges[unique_id].get()->alignment = alignment;
    ranges[unique_id].get()->add_value(v);
    return ranges[unique_id].get();
  }
}

LiveRange *LiveRangeGraph::fuse_ranges(Value in, Value out,
                                       unsigned alignment) {
  LiveRange *live_range = get_or_create_range(in, alignment);
  live_range->add_value(out);
  return live_range;
}

std::vector<LiveRange *> LiveRangeGraph::get_sorted_ranges() {
  std::vector<LiveRange *> sorted_ranges;
  for (auto &lr : ranges)
    sorted_ranges.push_back(lr.second.get());
  if (sorted_ranges.size() <= 1)
    return sorted_ranges;
  bool swap = false;
  do {
    swap = false;
    for (auto lr = 0; lr < sorted_ranges.size() - 1; lr++) {
      if (*(sorted_ranges[lr + 1]) < *(sorted_ranges[lr])) {
        LiveRange *temp = sorted_ranges[lr];
        sorted_ranges[lr] = sorted_ranges[lr + 1];
        sorted_ranges[lr + 1] = temp;
        swap = true;
      }
    }
  } while (swap);
  return sorted_ranges;
}

std::vector<LiveRange *> LiveRangeGraph::get_ranges() {
  std::vector<LiveRange *> range;
  for (auto &lr : ranges)
    range.push_back(lr.second.get());
  return range;
}

bool LiveRangeGraph::verify_allocation(
    mlir::ArchitectureConfig::ArchitectureConfigAttr &arch_c) {
  // Check that tensors within the same Time do not overlap
  //  std::cout << "verify_allocation::start" << std::endl;
  for (auto &lr_0 : ranges) {
    LiveRange *n = lr_0.second.get();
    for (auto &lr_1 : ranges) {
      LiveRange *m = lr_1.second.get();

      if ((n != m) and (n->overlaps_ranges(*m))) {
        SmallVector<Value, 2> overlap_values;
        if (n->overlaps_address(*m, overlap_values))
          FATAL_ERROR("LiveRangeGraph::verify_allocation::Overlapping Ranges & "
                      "Address");
      }
    }
  }
  // Check that Tensors their allocated address does not overflow
  int32_t size_limit = arch_c.sram_size().getInt();
  for (auto &lr : ranges) {
    LiveRange *n = lr.second.get();
    for (auto t : n->values) {
      auto start_address =
          t.getType().dyn_cast<mlir::SchedTensorType>().get_address();
      auto storage_size = n->storage_size;
      // std::cout << "start_address: " << start_address << " Storage Size: " <<
      // n->storage_size << std::endl;
      ASSERT_COND(
          (start_address + storage_size) > size_limit,
          "LiveRangeGraph::verify_allocation::Allocation overflows sram size");
    }
  }
  // Check Address is aligned to 16
  unsigned alloc_granularity = arch_c.allocation_alignment().getInt();
  ASSERT_COND(
      alloc_granularity != 16,
      "LiveRangeGraph::verify_allocation::alloc_granularity expected to be 16");
  for (auto &lr : ranges) {
    LiveRange *n = lr.second.get();
    for (auto t : n->values) {
      auto start_address =
          t.getType().dyn_cast<mlir::SchedTensorType>().get_address();
      /* std::cout << std::hex << "0x" << start_address << std::dec << " Aligned
       * to 16: " << start_address%16 << std::endl; */
      if (start_address % 16 != 0) {
        FATAL_ERROR("LiveRangeGraph::verify_allocation::address not alligned "
                    "to alloc granularity");
      }
    }
  }

  // std::cout << "verify_allocation::end" << std::endl;
  return true;
}

ostream &operator<<(ostream &os, LiveRangeGraph &dt) {
  std::vector<LiveRange *> sorted_ranges = dt.get_sorted_ranges();
  for (auto lr : sorted_ranges)
    os << *lr << std::endl;
  return os;
}

} // namespace planner
} // namespace mlir

/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#ifndef LIVE_RANGE_GRAPH_H
#define LIVE_RANGE_GRAPH_H

#include "mlir/Dialect/Quant/QuantOps.h"
#include "mlir/Dialect/Quant/QuantTypes.h"
#include "mlir/Dialect/StandardOps/IR/Ops.h"      // from @llvm-project
#include "mlir/IR/AffineMap.h"                    // from @llvm-project
#include "mlir/IR/Attributes.h"                   // from @llvm-project
#include "mlir/IR/BlockAndValueMapping.h"         // from @llvm-project
#include "mlir/IR/Builders.h"                     // from @llvm-project
#include "mlir/IR/Location.h"                     // from @llvm-project
#include "mlir/IR/MLIRContext.h"                  // from @llvm-project
#include "mlir/IR/Operation.h"                    // from @llvm-project
#include "mlir/IR/PatternMatch.h"                 // from @llvm-project
#include "mlir/Interfaces/InferTypeOpInterface.h" // from @llvm-project
#include "mlir/Pass/Pass.h"                       // from @llvm-project
#include "mlir/Transforms/DialectConversion.h"    // from @llvm-project

#include <iostream>
#include <map>
#include <set>

using namespace std;

#include "src/ir/architecture_config/architecture_config.h"

#include "src/transforms/memory_organizer/LiveRange.h"

namespace mlir {
namespace planner {

class LiveRangeGraph {
public:
  LiveRangeGraph();

  LiveRange *get_or_create_range(Value v, unsigned alignment);
  LiveRange *fuse_ranges(Value in, Value out, unsigned alignment);

  std::vector<LiveRange *> get_sorted_ranges();
  std::vector<LiveRange *> get_ranges();

  bool
  verify_allocation(mlir::ArchitectureConfig::ArchitectureConfigAttr &arch_c);

  map<value_unique_id, unique_ptr<LiveRange>> ranges;
  unsigned current_time;

  friend ostream &operator<<(ostream &os, LiveRangeGraph &dt);
};

ostream &operator<<(ostream &os, LiveRangeGraph &dt);

} // namespace planner
} // namespace mlir

#endif // LIVE_RANGE_GRAPH_H

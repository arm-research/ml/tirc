/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#include "src/transforms/memory_organizer/LivenessAnalysis.h"

namespace mlir {
namespace planner {

bool tensor_should_be_ignored(
    Value v, value_mem_area target_value_mem_area,
    const std::set<value_mem_type> &target_value_mem_type_set) {
  mlir::SchedTensorType type = v.getType().dyn_cast<mlir::SchedTensorType>();
  mlir::utils::value_mem_type v_mem_type = type.get_mem_type();
  mlir::utils::value_mem_area v_mem_area = type.get_mem_area();
  if ((v_mem_area != target_value_mem_area) or
      (target_value_mem_type_set.find(v_mem_type) ==
       target_value_mem_type_set.end()))
    return true;
  return false;
}

void LivenessAnalysis_Kernels(
    LiveRangeGraph &lrg, Block *block, value_mem_area target_value_mem_area,
    const std::set<value_mem_type> target_value_mem_type_set,
    mlir::MLIRContext *context,
    mlir::ArchitectureConfig::ArchitectureConfigAttr &arch_c) {
  LOG(ENABLE_LOGGING_MEMORY_ORGANIZER_VERBOSE_LEVEL_1,
      "LivenessAnalysis_Kernels::Start");
  for (llvm::iplist<Operation>::iterator i = block->begin(); i != block->end();
       i++) {
    Operation *kernel_op = &(*i);
    if (not(llvm::isa<kernelir::KernelRegionOp>(kernel_op)))
      continue;
    unsigned time_for_pass = lrg.current_time;
    Builder builder(context);
    kernel_op->setAttr("current_time",
                       builder.getI32IntegerAttr(lrg.current_time));
    SmallVector<Value, 1> kernel_values =
        mlir::kernelir::ExtractAllKernelValues(kernel_op);
    for (auto v : kernel_values) {
      if (tensor_should_be_ignored(v, target_value_mem_area,
                                   target_value_mem_type_set))
        continue;

      LiveRange *lr =
          lrg.get_or_create_range(v, arch_c.allocation_alignment().getInt());

      // Expand, Create or Shrink the Live Range for this Value/s
      lr->mark_usage(time_for_pass);
    }
    lrg.current_time += 2;
  }
  LOG(ENABLE_LOGGING_MEMORY_ORGANIZER_VERBOSE_LEVEL_1,
      "LivenessAnalysis_Kernels::End");
}

void LivenessAnalysis_Cascades(
    LiveRangeGraph &lrg, Block *block, value_mem_area target_value_mem_area,
    const std::set<value_mem_type> target_value_mem_type_set,
    mlir::MLIRContext *context,
    mlir::ArchitectureConfig::ArchitectureConfigAttr &arch_c,
    bool mark_output_tensors_overlapping_with_input_tensors,
    bool use_ifm_ofm_overlap, bool use_basic_block_input_output_tensors) {
  LOG(ENABLE_LOGGING_MEMORY_ORGANIZER_VERBOSE_LEVEL_1,
      "LivenessAnalysis_Cascades::Start");
  for (llvm::iplist<Operation>::iterator i = block->begin(); i != block->end();
       i++) {
    Operation *cascade_op = &(*i);
    if (not(llvm::isa<cascadeir::CascadeRegionOp>(cascade_op)))
      continue;
    unsigned time_for_pass = lrg.current_time;
    SmallVector<Value, 1> cascade_values =
        mlir::cascadeir::ExtractAllCascadeValues(cascade_op);
    Builder builder(context);
    cascade_op->setAttr("current_time",
                        builder.getI32IntegerAttr(lrg.current_time));
    for (auto v : cascade_values) {
      if (tensor_should_be_ignored(v, target_value_mem_area,
                                   target_value_mem_type_set))
        continue;
      LiveRange *lr =
          lrg.get_or_create_range(v, arch_c.allocation_alignment().getInt());
      // Expand, Create or Shrink the Live Range for this Value/s
      lr->mark_usage(time_for_pass);
    }
    lrg.current_time += 2;
  }
  LOG(ENABLE_LOGGING_MEMORY_ORGANIZER_VERBOSE_LEVEL_1,
      "LivenessAnalysis_Cascades::End");
}

} // namespace planner
} // namespace mlir

/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#ifndef LIVENESS_ANALYSIS_H
#define LIVENESS_ANALYSIS_H

#include "mlir/Dialect/Quant/QuantOps.h"
#include "mlir/Dialect/Quant/QuantTypes.h"
#include "mlir/Dialect/StandardOps/IR/Ops.h"      // from @llvm-project
#include "mlir/IR/AffineMap.h"                    // from @llvm-project
#include "mlir/IR/Attributes.h"                   // from @llvm-project
#include "mlir/IR/BlockAndValueMapping.h"         // from @llvm-project
#include "mlir/IR/Builders.h"                     // from @llvm-project
#include "mlir/IR/Location.h"                     // from @llvm-project
#include "mlir/IR/MLIRContext.h"                  // from @llvm-project
#include "mlir/IR/Operation.h"                    // from @llvm-project
#include "mlir/IR/PatternMatch.h"                 // from @llvm-project
#include "mlir/Interfaces/InferTypeOpInterface.h" // from @llvm-project
#include "mlir/Pass/Pass.h"                       // from @llvm-project
#include "mlir/Transforms/DialectConversion.h"    // from @llvm-project

#include "src/ir/architecture_config/architecture_config.h"
#include "src/transforms/memory_organizer/LiveRange.h"
#include "src/transforms/memory_organizer/LiveRangeGraph.h"

#include "src/ir/kernel_ir.h"
#include "src/ir/schedule_ir.h"

namespace mlir {
namespace planner {

void LivenessAnalysis_Kernels(
    LiveRangeGraph &lrg, Block *block, value_mem_area target_value_mem_area,
    const std::set<value_mem_type> target_value_mem_type_set,
    mlir::MLIRContext *context,
    mlir::ArchitectureConfig::ArchitectureConfigAttr &arch_c);

void LivenessAnalysis_Cascades(
    LiveRangeGraph &lrg, Block *block, value_mem_area target_mem_area,
    const std::set<value_mem_type> target_value_mem_type_set,
    mlir::MLIRContext *context,
    mlir::ArchitectureConfig::ArchitectureConfigAttr &arch_c,
    bool mark_output_tensors_overlapping_with_input_tensors = false,
    bool use_ifm_ofm_overlap = false,
    bool use_basic_block_input_output_tensors = false);

} // namespace planner
} // namespace mlir

#endif // LIVENESS_ANALYSIS_H

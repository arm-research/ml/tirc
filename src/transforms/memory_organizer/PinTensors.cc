/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#include "src/transforms/memory_organizer/PinTensors.h"

using namespace mlir;

namespace mlir {
namespace planner {

void pin_tensors(mlir::planner::LiveRangeGraph &lrg,
                 mlir::ArchitectureConfig::ArchitectureConfigAttr &arch_c) {
  LOG(ENABLE_LOGGING_MEMORY_ORGANIZER_VERBOSE_LEVEL_1,
      "Linear Allocator::Start");

  int32_t sram_size = arch_c.sram_size().getInt();
  unsigned alloc_granularity = arch_c.allocation_alignment().getInt();

  int32_t size_limit = arch_c.sram_size().getInt();

  std::vector<LiveRange *> sorted_ranges = lrg.get_sorted_ranges();
  for (auto &lr : sorted_ranges) {
    auto storage_size = round_up<uint64_t>(lr->storage_size, alloc_granularity);

    value_mem_area mem_area = ma_OffChipFlash;
    if (storage_size < size_limit)
      mem_area = ma_Sram;

    lr->set_values_live_range();
    lr->set_values_pin_alloc_size(storage_size);
    lr->set_values_pin_location(mem_area);
  }
}

} // namespace planner
} // namespace mlir

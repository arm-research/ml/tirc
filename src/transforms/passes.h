/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#ifndef PASSES_H
#define PASSES_H

#include "mlir/IR/MLIRContext.h"
#include "mlir/Pass/Pass.h"
#include "mlir/Support/LogicalResult.h"
#include "llvm/ADT/ArrayRef.h"
#include <memory>

#include <unordered_map>
#include <unordered_set>

#include "src/ir/compiler_config/compiler_config.h"

namespace mlir {

class FuncOp;
class ModuleOp;
class Pass;
template <typename T> class OperationPass;

namespace tosa {
std::unique_ptr<OperationPass<FuncOp>> CreateLegalizeQuantPass();
std::unique_ptr<OperationPass<ModuleOp>> CreateTosaBufferAllocPass();

std::unique_ptr<OperationPass<FuncOp>> createConvertTFLUint8Pass(
    std::unordered_map<std::string, mlir::utils::Legalization>
        legalization_maplist);
std::unique_ptr<OperationPass<FuncOp>> createLegalizeTosaNPUTFLPass(
    std::unordered_map<std::string, mlir::utils::Legalization>
        legalization_maplist);

// CodeGen Abstraction Passes
std::unique_ptr<OperationPass<FuncOp>> CreateTosaFlatbufferGenPass();
std::unique_ptr<OperationPass<FuncOp>> CreateOptimizeAddOperation();
std::unique_ptr<OperationPass<FuncOp>> CreateOptimizeReluOperation();
std::unique_ptr<OperationPass<FuncOp>> CreateConcatRescaleFoldingTosaPass();

std::unique_ptr<OperationPass<FuncOp>> CreateConstantTransposeFoldingTosaPass();
std::unique_ptr<OperationPass<FuncOp>> CreateConcatFoldingTosaPass();

} // namespace tosa

namespace planner {
std::unique_ptr<OperationPass<FuncOp>> CreateLegalizeScheduleIR(
    mlir::ArchitectureConfig::ArchitectureConfigAttr &arch_config,
    mlir::CompilerConfig::CompilerConfigAttr comp_config);
std::unique_ptr<OperationPass<FuncOp>> CreatePlannerIRFormat();
std::unique_ptr<OperationPass<FuncOp>> CreatePlannerWeightCompression();
std::unique_ptr<OperationPass<FuncOp>> CreatePlannerWeightPacking();
std::unique_ptr<OperationPass<FuncOp>> CreatePlannerKernelPacking(
    std::unordered_map<std::string, mlir::utils::MappingStructure> hw_maplist);
std::unique_ptr<OperationPass<FuncOp>> CreatePlannerKernelRescaling();
std::unique_ptr<OperationPass<FuncOp>> CreatePlannerKernelTransformation();
std::unique_ptr<OperationPass<FuncOp>> CreatePlannerIRSort();
std::unique_ptr<OperationPass<FuncOp>> CreatePlannerScheduler();
std::unique_ptr<OperationPass<FuncOp>> CreatePlannerMemoryOrganizer();
std::unique_ptr<OperationPass<FuncOp>> CreatePlannerHLCSGenerator();
} // namespace planner

namespace hlcs {
std::unique_ptr<OperationPass<FuncOp>> CreateHLCSOptimisationPass();
std::unique_ptr<OperationPass<FuncOp>> CreateHLCSConvToNpuOpPass();
std::unique_ptr<OperationPass<FuncOp>> CreateHLCSCalcDependencyPass();
std::unique_ptr<OperationPass<FuncOp>> CreateHLCSNpuRegionLowerPass();
std::unique_ptr<OperationPass<FuncOp>> CreateHLCSGenerateLLCSPass();
} // namespace hlcs

namespace binary_writer {
std::unique_ptr<OperationPass<FuncOp>> CreateRuntimeBinaryGenerator();
}

} // namespace mlir

#endif // PASSES_H

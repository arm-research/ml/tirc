/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#include "mlir/Dialect/Quant/FakeQuantSupport.h" // TF:local_config_mlir
#include "mlir/Dialect/Quant/UniformSupport.h"   // TF:local_config_mlir
#include "mlir/Dialect/StandardOps/IR/Ops.h"     // TF:local_config_mlir
#include "mlir/IR/Attributes.h"                  // TF:local_config_mlir
#include "mlir/IR/Diagnostics.h"                 // TF:local_config_mlir
#include "mlir/IR/MLIRContext.h"                 // TF:local_config_mlir
#include "mlir/IR/Matchers.h"                    // TF:local_config_mlir
#include "mlir/IR/Operation.h"                   // TF:local_config_mlir
#include "mlir/IR/PatternMatch.h"                // TF:local_config_mlir
#include "mlir/IR/TypeUtilities.h"               // TF:local_config_mlir
#include "mlir/IR/Types.h"                       // TF:local_config_mlir
#include "mlir/Pass/Pass.h"                      // TF:local_config_mlir
#include "mlir/Support/LLVM.h"                   // TF:local_config_mlir
#include "mlir/Transforms/DialectConversion.h"   // TF:local_config_mlir
#include "mlir/Transforms/GreedyPatternRewriteDriver.h"
#include "llvm/ADT/APInt.h"
#include "llvm/ADT/ArrayRef.h"
#include "llvm/ADT/Optional.h"
#include "llvm/ADT/STLExtras.h"
#include "llvm/ADT/StringSwitch.h"

#include "src/ir/architecture_config/architecture_config.h"
#include "src/ir/schedule_ir.h"
#include "src/transforms/passes.h"
#include "src/utils/clone_ir.h"
#include "src/utils/dag.h"
#include "src/utils/ir_utils.h"
#include "src/utils/printIR.h"
#include "src/utils/unique_id.h"

using namespace std;
using namespace mlir::planner;
using namespace mlir::scheduleir;

#include <iostream>

#define PASS_NAME "planner-ir-format"
#define DEBUG_TYPE PASS_NAME

namespace mlir {
namespace planner {
namespace {

#define FORCE_INPUTS_TO_SECONDARY_STORAGE 0
#define FORCE_OUTPUTS_TO_SECONDARY_STORAGE 0

#define RESHAPE_MAPPING_ENABLED 1
#define CONCAT_MAPPING_ENABLED 1
#define NHCWB16_ENABLED 1

/*
   Go Through Each Op and Change the Operand and ResultOperands type from
   Ranked Tensor Type to ScheduleTensorType and do any TensorMarkup Required
 */
int apply_pass(FuncOp function, MLIRContext *context) {

  Builder builder(context);

  Region *region = function.getCallableRegion();
  llvm::iplist<Block> &blocks = region->getBlocks();
  if (blocks.size() > 1) {
    std::cout << "PlannerIRFormat:::More than a single block "
                 "in function"
              << std::endl;
    return 1;
  }
  llvm::iplist<Operation> &operations = region->front().getOperations();

  for (auto &op : operations) {
    if (llvm::isa<scheduleir::NpuRegionOp>(op)) {
      mlir::ArchitectureConfig::ArchitectureConfigAttr arch_c =
          AccessArchitectureConfigAttribute_From_NpuRegion(&op);

      mlir::CompilerConfig::CompilerConfigAttr compiler_config =
          AccessCompilerConfigAttribute_From_NpuRegion(&op);

      Region &region_instruction = op.getRegion(0);
      Block &block_instruction = region_instruction.front();
      llvm::iplist<Operation> &operations_npu_region =
          block_instruction.getOperations();

      unsigned int operation_id = 0;
      for (auto &op_npu : operations_npu_region) {
        // Check in IRFormat to make sure all tensors are symmetric if
        // per-channel (per-axis)
        // https://www.tensorflow.org/lite/performance/quantization_spec
        //    https://mlir.llvm.org/doxygen/classmlir_1_1quant_1_1UniformQuantizedPerAxisType.html
        //    https://mlir.llvm.org/doxygen/classmlir_1_1quant_1_1UniformQuantizedType.html
        // * Activation tensors are only zero point per tensor.
        // * Weight tensors can be zero point per channel as the zero point is
        // removed before weight compression (so hardware does not see the zero
        // point).
        // * Scaling can vary per weight output channel (but is per tensor for
        // activation tensors)
        // printOp(op_npu);
        for (int i = 0; i < op_npu.getNumOperands(); i++) {
          // std::cout << i << std::endl;
          auto t = op_npu.getOperand(i).getType();
          std::vector<double> scalePoints = getScale(t);
          std::vector<int64_t> zeroPoints = getZeroPoint(t);
          if (not(getIsSymmetric(t)) and getIsPerChannel(t)) {
            FATAL_ERROR("planner_ir_format:: Tensor Check detected a non "
                        "symmetric Tensor");
          }
        }

        ////////////////////////////////////////////////////////////////////////////////////
        // 1. Add metadata to each tensor
        ////////////////////////////////////////////////////////////////////////////////////
        NpuBlockType block_type;
        bool is_unary = false;
        if (llvm::isa<scheduleir::ConstOp>(op_npu)) {
          // No Inputs
        } else if (llvm::isa<scheduleir::Conv2DOp>(op_npu)) {
          SchedTensorType output_t =
              op_npu.getOperand(1).getType().dyn_cast<mlir::SchedTensorType>();
          ArrayRef<int64_t> shape = output_t.getShape();
          /*
            TOSA:          OHWI   0,1,2,3
            CompilerShape: HWIO   1,2,3,0
            Note that while the Tensor is marked as HWIO the underlying data is
            left as OHWI
           */
          std::vector<int64_t> shape_hwio = {shape[1], shape[2], shape[3],
                                             shape[0]}; // OHWI -> HWIO
          SchedTensorType t = SchedTensorType::get(
              shape_hwio, output_t.getElementType(), getUniqueId());
          op_npu.getOperand(1).setType(t);

          ASSERT_COND(getDataType(op_npu.getOperand(1).getType()) != int8,
                      "planner_ir_format::weights need to be int8");

          block_type = ConvolutionMxN;
          SetParameters(op_npu.getOperand(0), vp_FeatureMap, vsp_Standard,
                        vf_NHWC, tensor_storage_mem_area(vp_FeatureMap, arch_c),
                        tensor_storage_mem_type(vp_FeatureMap, arch_c)); // IFM
          SetParameters(op_npu.getOperand(1), vp_Weights, vsp_Standard, vf_HWIO,
                        tensor_storage_mem_area(vp_Weights, arch_c),
                        tensor_storage_mem_type(vp_Weights, arch_c)); // WEIGHTS
          SetParameters(op_npu.getOperand(2), vp_FSBias, vsp_Standard, vf_NHWC,
                        tensor_storage_mem_area(vp_Weights, arch_c),
                        tensor_storage_mem_type(vp_Weights, arch_c)); // BIAS
        } else if (llvm::isa<scheduleir::DepthwiseConv2DOp>(op_npu)) {
          SchedTensorType output_t =
              op_npu.getOperand(1).getType().dyn_cast<mlir::SchedTensorType>();

          ArrayRef<int64_t> shape = output_t.getShape();
          /*
            TOSA: OHWI             0,1,2,3
            Compiler Shape: HWOI   1,2,3,0
            Note that while the Tensor is marked as HWIO the underlying data is
            left as OHWI
           */
          std::vector<int64_t> shape_hwio = {shape[1], shape[2], shape[0],
                                             shape[3]}; // OHWI -> HWOI
          SchedTensorType t = SchedTensorType::get(
              shape_hwio, output_t.getElementType(), getUniqueId());
          op_npu.getOperand(1).setType(t);

          ASSERT_COND(getDataType(op_npu.getOperand(1).getType()) != int8,
                      "planner_ir_format::weights need to be int8");

          block_type = ConvolutionDepthWise;
          SetParameters(op_npu.getOperand(0), vp_FeatureMap, vsp_Standard,
                        vf_NHWC, tensor_storage_mem_area(vp_FeatureMap, arch_c),
                        tensor_storage_mem_type(vp_FeatureMap, arch_c)); // IFM
          SetParameters(op_npu.getOperand(1), vp_Weights, vsp_Standard, vf_HWOI,
                        tensor_storage_mem_area(vp_Weights, arch_c),
                        tensor_storage_mem_type(vp_Weights, arch_c)); // WEIGHTS
          SetParameters(op_npu.getOperand(2), vp_FSBias, vsp_Standard, vf_NHWC,
                        tensor_storage_mem_area(vp_Weights, arch_c),
                        tensor_storage_mem_type(vp_Weights, arch_c)); // BIAS
        } else if (llvm::isa<scheduleir::FullyConnectedOp>(op_npu)) {
          ASSERT_COND(true, "planner_ir_format::Fully Connected needs weight "
                            "remapping Check");
          block_type = VectorProduct;
          SetParameters(op_npu.getOperand(0), vp_FeatureMap, vsp_Standard,
                        vf_NHWC, tensor_storage_mem_area(vp_FeatureMap, arch_c),
                        tensor_storage_mem_type(vp_FeatureMap, arch_c)); // IFM
          SetParameters(op_npu.getOperand(1), vp_Weights, vsp_Standard, vf_OHWI,
                        tensor_storage_mem_area(vp_Weights, arch_c),
                        tensor_storage_mem_type(vp_Weights, arch_c)); // WEIGHTS
          SetParameters(op_npu.getOperand(2), vp_FSBias, vsp_Standard, vf_NHWC,
                        tensor_storage_mem_area(vp_Weights, arch_c),
                        tensor_storage_mem_type(vp_Weights, arch_c)); // BIAS
        } else if (llvm::isa<scheduleir::MaxPool2dOp>(op_npu)) {
          block_type = Pooling;
          SetParameters(op_npu.getOperand(0), vp_FeatureMap, vsp_Standard,
                        vf_NHWC, tensor_storage_mem_area(vp_FeatureMap, arch_c),
                        tensor_storage_mem_type(vp_FeatureMap, arch_c)); // IFM
        } else if (llvm::isa<scheduleir::AvgPool2dOp>(op_npu)) {
          block_type = Pooling;
          SetParameters(op_npu.getOperand(0), vp_FeatureMap, vsp_Standard,
                        vf_NHWC, tensor_storage_mem_area(vp_FeatureMap, arch_c),
                        tensor_storage_mem_type(vp_FeatureMap, arch_c)); // IFM
        } else if (llvm::isa<scheduleir::AddOp>(op_npu)) {
          block_type = ElementWise; /* Output Unit in Standalone Mode */
          mlir::Operation *op0_def_op = op_npu.getOperand(0).getDefiningOp();
          mlir::Operation *op1_def_op = op_npu.getOperand(1).getDefiningOp();
          bool is_op0_const = false, is_op1_const = false;
          if (op0_def_op)
            if (llvm::isa<scheduleir::ConstOp>(*op0_def_op))
              is_op0_const = true;
          if (op1_def_op)
            if (llvm::isa<scheduleir::ConstOp>(*op1_def_op))
              is_op1_const = true;
          SetParameters(
              op_npu.getOperand(0), vp_FeatureMap, vsp_Standard, vf_NHWC,
              tensor_storage_mem_area(vp_FeatureMap, arch_c, is_op0_const),
              tensor_storage_mem_type(vp_FeatureMap, arch_c,
                                      is_op0_const)); // IFM
          SetParameters(
              op_npu.getOperand(1), vp_FeatureMap, vsp_Standard, vf_NHWC,
              tensor_storage_mem_area(vp_FeatureMap, arch_c, is_op1_const),
              tensor_storage_mem_type(vp_FeatureMap, arch_c,
                                      is_op1_const)); // IFM
        } else if (llvm::isa<scheduleir::MulOp>(op_npu)) {
          block_type = ElementWise; /* Output Unit in Standalone Mode */
          mlir::Operation *op0_def_op = op_npu.getOperand(0).getDefiningOp();
          mlir::Operation *op1_def_op = op_npu.getOperand(1).getDefiningOp();
          bool is_op0_const = false, is_op1_const = false;
          if (op0_def_op)
            if (llvm::isa<scheduleir::ConstOp>(*op0_def_op))
              is_op0_const = true;
          if (op1_def_op)
            if (llvm::isa<scheduleir::ConstOp>(*op1_def_op))
              is_op1_const = true;
          SetParameters(
              op_npu.getOperand(0), vp_FeatureMap, vsp_Standard, vf_NHWC,
              tensor_storage_mem_area(vp_FeatureMap, arch_c, is_op0_const),
              tensor_storage_mem_type(vp_FeatureMap, arch_c,
                                      is_op0_const)); // IFM
          SetParameters(
              op_npu.getOperand(1), vp_FeatureMap, vsp_Standard, vf_NHWC,
              tensor_storage_mem_area(vp_FeatureMap, arch_c, is_op1_const),
              tensor_storage_mem_type(vp_FeatureMap, arch_c,
                                      is_op1_const)); // IFM
        } else if (llvm::isa<scheduleir::ClampOp>(op_npu)) {
          block_type = Default;
          SetParameters(op_npu.getOperand(0), vp_FeatureMap, vsp_Standard,
                        vf_NHWC, tensor_storage_mem_area(vp_FeatureMap, arch_c),
                        tensor_storage_mem_type(vp_FeatureMap, arch_c)); // IFM
        } else if (llvm::isa<scheduleir::ConcatOp>(op_npu)) {
          block_type = Memory;
          for (int i = 0; i < op_npu.getNumOperands(); i++) {
            SetParameters(
                op_npu.getOperand(i), vp_FeatureMap, vsp_Standard, vf_NHWC,
                tensor_storage_mem_area(vp_FeatureMap, arch_c),
                tensor_storage_mem_type(vp_FeatureMap, arch_c)); // IFM
            op_npu.getOperand(i)
                .getType()
                .dyn_cast<mlir::SchedTensorType>()
                .set_io_memory_op(true);
          }
          for (int i = 0; i < op_npu.getNumResults(); i++)
            op_npu.getResult(i)
                .getType()
                .dyn_cast<mlir::SchedTensorType>()
                .set_io_memory_op(true);
        } else if (llvm::isa<scheduleir::CastOutOp>(op_npu)) {
          block_type = Default;
#if FORCE_OUTPUTS_TO_SECONDARY_STORAGE
          for (unsigned i = 0; i < op_npu.getNumOperands(); i++)
            SetParameters(op_npu.getOperand(i), vp_FeatureMap, vsp_Standard,
                          vf_NHWC, ma_Unknown, mt_Permanent_CPU);
#else
          for (unsigned i = 0; i < op_npu.getNumOperands(); i++)
            SetParameters(op_npu.getOperand(i), vp_FeatureMap, vsp_Standard,
                          vf_NHWC,
                          tensor_storage_mem_area(vp_FeatureMap, arch_c),
                          tensor_storage_mem_type(vp_FeatureMap, arch_c));
#endif
        } else if (llvm::isa<scheduleir::ReshapeOp>(op_npu)) {
          block_type = Memory;
          SetParameters(op_npu.getOperand(0), vp_FeatureMap, vsp_Standard,
                        vf_NHWC, tensor_storage_mem_area(vp_FeatureMap, arch_c),
                        tensor_storage_mem_type(vp_FeatureMap, arch_c)); // IFM
        } else if (llvm::isa<scheduleir::YieldOp>(op_npu)) {
          // No SchedTensor Type
        } else if (llvm::isa<scheduleir::CastInOp>(op_npu)) {
          // No Inputs to Update as all RankedTensorType
        } else
          ASSERT_COND(true,
                      "planner_ir_format::FatalError::Node Not Supported: " +
                          std::string(getOpName(op_npu)));

        ////////////////////////////////////////////////////////////////////////////////////
        // 2. Add metadata to each operation
        ////////////////////////////////////////////////////////////////////////////////////
        op_npu.setAttr("block_type",
                       builder.getI64IntegerAttr(int(block_type)));
        op_npu.setAttr("is_unary", builder.getBoolAttr(is_unary));
      }

      // As we are changing the outputs needs to be done last
      for (auto &op_npu : operations_npu_region) {
        if (llvm::isa<scheduleir::CastInOp>(op_npu)) {
#if FORCE_INPUTS_TO_SECONDARY_STORAGE
          for (unsigned r = 0; r < op_npu.getNumResults(); r++)
            SetParameters(op_npu.getResult(r), vp_FeatureMap, vsp_Standard,
                          vf_NHWC, ma_Unknown, mt_Permanent_CPU);
#endif
        }
      }
    }
  }

  OpBuilder op_builder(context);

#if NHCWB16_ENABLED
  for (auto &op : operations) {
    if (llvm::isa<scheduleir::NpuRegionOp>(op)) {
      mlir::ArchitectureConfig::ArchitectureConfigAttr arch_c =
          AccessArchitectureConfigAttribute_From_NpuRegion(&op);

      mlir::CompilerConfig::CompilerConfigAttr compiler_config =
          AccessCompilerConfigAttribute_From_NpuRegion(&op);

      Region &region_instruction = op.getRegion(0);
      Block &block_instruction = region_instruction.front();
      llvm::iplist<Operation> &operations_npu_region =
          block_instruction.getOperations();

      unsigned int operation_id = 0;
      for (auto &op_npu : operations_npu_region) {
        ////////////////////////////////////////////////////////////////////////////////////
        // Configure to use NHCWB16 for tensors in between operations
        ////////////////////////////////////////////////////////////////////////////////////
        if (llvm::isa<scheduleir::Conv2DOp>(op_npu) or
            llvm::isa<scheduleir::DepthwiseConv2DOp>(op_npu) or
            llvm::isa<scheduleir::FullyConnectedOp>(op_npu) or
            llvm::isa<scheduleir::MaxPool2dOp>(op_npu) or
            llvm::isa<scheduleir::AvgPool2dOp>(op_npu) or
            llvm::isa<scheduleir::AddOp>(op_npu) or
            llvm::isa<scheduleir::MulOp>(op_npu) or
            llvm::isa<scheduleir::ClampOp>(op_npu)) {
          // If Consumers ConcatSliceWrite or Reshape leave as NHWC
          if (op_npu.getResult(0).hasOneUse()) {
            for (Operation *consumer : op_npu.getResult(0).getUsers()) {
              if (llvm::isa<scheduleir::SliceViewOp>(consumer) or
                  llvm::isa<scheduleir::ReshapeOp>(consumer)) {
                continue;
              }
            }
          }

          // Update Format, and Storage Shape for outputs
          for (auto out_c : op_npu.getResults()) {
            bool re_format = true;

            for (Operation *consumer : out_c.getUsers()) {
              if (llvm::isa<scheduleir::CastOutOp>(consumer))
                re_format = false;
              else if (llvm::isa<scheduleir::ConcatOp>(consumer))
                re_format = false;
            }

            auto type = out_c.getType().dyn_cast<mlir::SchedTensorType>();
            SmallVector<int64_t, 4> shape =
                arrayref_2_smallvector(type.getShape());
            if (shape.size() != 4)
              re_format = false;

            if (re_format) {
              while ((shape[3] % 16) != 0) {
                shape[3]++;
              }
              // These will all use the NHCWB16 format, and need to be aligned
              // to 16 in the C-dimension
              type.set_format(vf_NHCWB16);
              type.setStorageShape(shape);
            }
          }
        }
      }
    }
  }
#endif

#if CONCAT_MAPPING_ENABLED
  {
    std::vector<Operation *> to_delete;
    mlir::Location loc = builder.getUnknownLoc();
    for (auto &op_top : operations) {
      if (llvm::isa<scheduleir::NpuRegionOp>(op_top)) {
        mlir::ArchitectureConfig::ArchitectureConfigAttr arch_c =
            AccessArchitectureConfigAttribute_From_NpuRegion(&op_top);

        mlir::CompilerConfig::CompilerConfigAttr compiler_config =
            AccessCompilerConfigAttribute_From_NpuRegion(&op_top);

        Region &region_instruction = op_top.getRegion(0);
        Block &block_instruction = region_instruction.front();
        llvm::iplist<Operation> &operations_npu_region =
            block_instruction.getOperations();
        for (auto &op_concat : operations_npu_region) {
          if (llvm::isa<scheduleir::ConcatOp>(op_concat)) {
            auto storage_shape = op_concat.getResult(0)
                                     .getType()
                                     .dyn_cast<mlir::SchedTensorType>()
                                     .getStorageShape();
            int axis =
                op_concat.getAttrOfType<::mlir::IntegerAttr>("axis").getInt();
            int start = 0;
            int end = 0;

            auto concat_id = getUniqueId();

            // 0. Insert a SliceView between the Op and the concat
            int concat_index = 0;
            for (auto operand : op_concat.getOperands()) {
              Operation *input_op = operand.getDefiningOp();
              auto output_t = input_op->getResult(0)
                                  .getType()
                                  .dyn_cast<mlir::SchedTensorType>();

              end = end + output_t.getStorageShape()[axis];

              // std::cout << "storage_shape: " <<
              // array_ref_to_string(storage_shape) << std::endl; std::cout <<
              // "branch_shape: "  <<
              // array_ref_to_string(output_t.getStorageShape()) << std::endl;
              // std::cout << "axis: "          << axis  << std::endl;
              // std::cout << "start: "         << start << std::endl;
              // std::cout << "end: "           << end   << std::endl;

              auto sliceview_input = operand;
              auto sliceview_result = SchedTensorType::get(
                  output_t.getShape(), output_t.getElementType(), concat_id);

              llvm::iplist<Operation>::iterator insertionPoint(op_concat);
              op_builder.setInsertionPoint(op_concat.getBlock(),
                                           insertionPoint);
              auto sliceview_op = op_builder.create<scheduleir::SliceViewOp>(
                  loc, sliceview_result, sliceview_input);

              Operation *sliceview_op_base = sliceview_op;
              sliceview_op_base->setAttr("axis",
                                         builder.getI64IntegerAttr(int(axis)));
              sliceview_op_base->setAttr("start",
                                         builder.getI64IntegerAttr(int(start)));
              sliceview_op_base->setAttr("end",
                                         builder.getI64IntegerAttr(int(end)));

              SetParameters(
                  sliceview_op->getOpResult(0), vp_FeatureMap, vsp_Standard,
                  vf_NHWC, tensor_storage_mem_area(vp_FeatureMap, arch_c),
                  tensor_storage_mem_type(vp_FeatureMap, arch_c)); // IFM
              sliceview_result.setStorageShape(
                  arrayref_2_smallvector(storage_shape));

              SmallVector<Value, 1> replacment_operand;
              replacment_operand.push_back(sliceview_op->getOpResult(0));

              op_concat.setOperands(concat_index, 1, replacment_operand);
              concat_index++;

              start = start + output_t.getStorageShape()[axis];
            }

            // 1. Wrap the ConcatOp into a MemoryOp
            {
              SmallVector<mlir::Value> memory_op_inputs;
              SmallVector<mlir::Type> memory_op_inputs_types;
              for (auto operand : op_concat.getOperands()) {
                memory_op_inputs.push_back(operand);
                memory_op_inputs_types.push_back(operand.getType());
                operand.getType()
                    .dyn_cast<mlir::SchedTensorType>()
                    .set_io_memory_op(true);
              }

              SmallVector<mlir::Type> memory_op_outputs;
              auto output_t = op_concat.getOpResult(0)
                                  .getType()
                                  .dyn_cast<mlir::SchedTensorType>();
              auto t = SchedTensorType::get(
                  output_t.getShape(), output_t.getElementType(), concat_id);
              t.set_io_memory_op(true);
              memory_op_outputs.push_back(t);

              // 0. Create a Memory Op
              llvm::iplist<Operation>::iterator insertionPoint(op_concat);
              op_builder.setInsertionPoint(op_concat.getBlock(),
                                           insertionPoint);
              auto memory_op = op_builder.create<scheduleir::MemoryOp>(
                  loc, memory_op_outputs, memory_op_inputs);

              SetParameters(
                  memory_op->getOpResult(0), vp_FeatureMap, vsp_Standard,
                  vf_NHWC, tensor_storage_mem_area(vp_FeatureMap, arch_c),
                  tensor_storage_mem_type(vp_FeatureMap, arch_c)); // IFM
              t.setStorageShape(arrayref_2_smallvector(storage_shape));

              Operation *memory_op_base = memory_op;
              memory_op_base->setAttr("block_type",
                                      builder.getI64IntegerAttr(int(Memory)));
              memory_op_base->setAttr("is_unary", builder.getBoolAttr(false));

              Region &region_instruction = memory_op->getRegion(0);
              Block *block = op_builder.createBlock(&region_instruction,
                                                    region_instruction.begin(),
                                                    memory_op_inputs_types);

              // 1. Modify Consumers
              SmallVector<Value, 1> replacment_operand;
              replacment_operand.push_back(memory_op->getOpResult(0));
              auto consumers = ExtractConsumers(&op_concat);
              for (auto c : consumers)
                c.external_operation->setOperands(c.external_slot_index, 1,
                                                  replacment_operand);

              // 2. Clone Concat Into it
              SmallVector<value_slot, TYPICAL_NO_INPUTS> input_list;
              int index = 0;
              for (auto v : block->getArguments()) {
                value_slot vs(index++, v);
                input_list.push_back(vs);
              }
              Block::iterator insertPoint = block->end();
              auto new_concat = clone_op(op_builder, block, insertPoint,
                                         &op_concat, input_list);

              Operation *op_terminate = op_builder.create<scheduleir::YieldOp>(
                  loc, new_concat->getResult(0));

              // 3. Delete Original Concat
              to_delete.push_back(&op_concat);
            }
          }
        }
      }
    }
    for (auto op : to_delete) {
      op->dropAllReferences();
      op->dropAllDefinedValueUses();
      op->erase();
    }
#endif
  }

#if RESHAPE_MAPPING_ENABLED
  {
    std::vector<Operation *> to_delete;
    mlir::Location loc = builder.getUnknownLoc();
    for (auto &op_top : operations) {
      if (llvm::isa<scheduleir::NpuRegionOp>(op_top)) {
        mlir::ArchitectureConfig::ArchitectureConfigAttr arch_c =
            AccessArchitectureConfigAttribute_From_NpuRegion(&op_top);

        mlir::CompilerConfig::CompilerConfigAttr compiler_config =
            AccessCompilerConfigAttribute_From_NpuRegion(&op_top);

        Region &region_instruction = op_top.getRegion(0);
        Block &block_instruction = region_instruction.front();
        llvm::iplist<Operation> &operations_npu_region =
            block_instruction.getOperations();
        for (auto &op_concat : operations_npu_region) {
          if (llvm::isa<scheduleir::ReshapeOp>(op_concat)) {
            // 1. Wrap the Reshape into a MemoryOp
            {
              SmallVector<mlir::Value> memory_op_inputs;
              SmallVector<mlir::Type> memory_op_inputs_types;
              for (auto operand : op_concat.getOperands()) {
                memory_op_inputs.push_back(operand);
                memory_op_inputs_types.push_back(operand.getType());
                operand.getType()
                    .dyn_cast<mlir::SchedTensorType>()
                    .set_io_memory_op(true);
              }

              SmallVector<mlir::Type> memory_op_outputs;
              auto output_t = op_concat.getOpResult(0)
                                  .getType()
                                  .dyn_cast<mlir::SchedTensorType>();
              output_t.set_io_memory_op(true);
              memory_op_outputs.push_back(output_t);

              // 0. Create a Memory Op
              llvm::iplist<Operation>::iterator insertionPoint(op_concat);
              op_builder.setInsertionPoint(op_concat.getBlock(),
                                           insertionPoint);
              auto memory_op = op_builder.create<scheduleir::MemoryOp>(
                  loc, memory_op_outputs, memory_op_inputs);

              SetParameters(
                  memory_op->getOpResult(0), vp_FeatureMap, vsp_Standard,
                  vf_NHWC, tensor_storage_mem_area(vp_FeatureMap, arch_c),
                  tensor_storage_mem_type(vp_FeatureMap, arch_c)); // IFM

              Operation *memory_op_base = memory_op;
              memory_op_base->setAttr("block_type",
                                      builder.getI64IntegerAttr(int(Memory)));
              memory_op_base->setAttr("is_unary", builder.getBoolAttr(false));

              Region &region_instruction = memory_op->getRegion(0);
              Block *block = op_builder.createBlock(&region_instruction,
                                                    region_instruction.begin(),
                                                    memory_op_inputs_types);

              // 1. Modify Consumers
              SmallVector<Value, 1> replacment_operand;
              replacment_operand.push_back(memory_op->getOpResult(0));
              auto consumers = ExtractConsumers(&op_concat);
              for (auto c : consumers)
                c.external_operation->setOperands(c.external_slot_index, 1,
                                                  replacment_operand);

              // 2. Clone Reshape Into it
              SmallVector<value_slot, TYPICAL_NO_INPUTS> input_list;
              int index = 0;
              for (auto v : block->getArguments()) {
                value_slot vs(index++, v);
                input_list.push_back(vs);
              }
              Block::iterator insertPoint = block->end();
              auto new_concat = clone_op(op_builder, block, insertPoint,
                                         &op_concat, input_list);

              Operation *op_terminate = op_builder.create<scheduleir::YieldOp>(
                  loc, new_concat->getResult(0));

              // 3. Delete Original Concat
              to_delete.push_back(&op_concat);
            }
          }
        }
      }
    }
    for (auto op : to_delete) {
      op->dropAllReferences();
      op->dropAllDefinedValueUses();
      op->erase();
    }
  }
#endif

  return 0;
} // namespace

class PlannerIRFormat : public PassWrapper<PlannerIRFormat, FunctionPass> {
public:
  explicit PlannerIRFormat() {}

  StringRef getArgument() const final {
    // This is the argument used to refer to the pass in
    // the textual format (on the commandline for example).
    return PASS_NAME;
  }
  StringRef getDescription() const final {
    // This is a brief description of the pass.
    return "Apply Modification to tensors in IR ready for compression, "
           "scheduling & tensor allocation";
  }

  void runOnFunction() override;
};

void PlannerIRFormat::runOnFunction() {
  auto function = getFunction();
  auto *ctx = &getContext();

  if (function.getName() == "main") {
    if (apply_pass(function, ctx) > 0)
      signalPassFailure();
  }
}

} // anonymous namespace

std::unique_ptr<OperationPass<FuncOp>> CreatePlannerIRFormat() {
  return absl::make_unique<PlannerIRFormat>();
}

static PassRegistration<PlannerIRFormat> pass;
} // namespace planner
} // namespace mlir

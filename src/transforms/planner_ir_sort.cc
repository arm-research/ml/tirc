/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#include "mlir/Dialect/Quant/FakeQuantSupport.h" // TF:local_config_mlir
#include "mlir/Dialect/Quant/UniformSupport.h"   // TF:local_config_mlir
#include "mlir/Dialect/StandardOps/IR/Ops.h"     // TF:local_config_mlir
#include "mlir/IR/Attributes.h"                  // TF:local_config_mlir
#include "mlir/IR/Diagnostics.h"                 // TF:local_config_mlir
#include "mlir/IR/MLIRContext.h"                 // TF:local_config_mlir
#include "mlir/IR/Matchers.h"                    // TF:local_config_mlir
#include "mlir/IR/Operation.h"                   // TF:local_config_mlir
#include "mlir/IR/PatternMatch.h"                // TF:local_config_mlir
#include "mlir/IR/TypeUtilities.h"               // TF:local_config_mlir
#include "mlir/IR/Types.h"                       // TF:local_config_mlir
#include "mlir/Pass/Pass.h"                      // TF:local_config_mlir
#include "mlir/Support/LLVM.h"                   // TF:local_config_mlir
#include "mlir/Transforms/DialectConversion.h"   // TF:local_config_mlir
#include "mlir/Transforms/GreedyPatternRewriteDriver.h"
#include "llvm/ADT/APInt.h"
#include "llvm/ADT/ArrayRef.h"
#include "llvm/ADT/Optional.h"
#include "llvm/ADT/STLExtras.h"
#include "llvm/ADT/StringSwitch.h"

#include "src/transforms/passes.h"

#include "src/ir/architecture_config/architecture_config.h"
#include "src/utils/ir_sort.h"
#include "src/utils/ir_utils.h"
#include "src/utils/printIR.h"
#include "src/utils/unique_id.h"

#include "src/ir/schedule_ir.h"
#include "src/utils/dag.h"
#include "tensorflow/compiler/mlir/lite/ir/tfl_ops.h"

#include "src/utils/clone_ir.h"

using namespace std;
#include <iostream>

#define PASS_NAME "planner-ir-sort"
#define DEBUG_TYPE PASS_NAME

namespace mlir {
namespace planner {
namespace {

int apply_pass(FuncOp function, MLIRContext *context) {
  Region *region = function.getCallableRegion();
  llvm::iplist<Block> &blocks = region->getBlocks();
  if (blocks.size() > 1) {
    std::cout << "PlannerIRSort:::More than a single block "
                 "in function"
              << std::endl;
    return 1;
  }
  llvm::iplist<Operation> &operations = region->front().getOperations();

  for (auto &op : operations) {
    if (llvm::isa<scheduleir::NpuRegionOp>(op)) {
      ir_sort(op);
    }
  }

  return 0;
}

class PlannerIRSort : public PassWrapper<PlannerIRSort, FunctionPass> {
public:
  explicit PlannerIRSort() {}

  StringRef getArgument() const final {
    // This is the argument used to refer to the pass in
    // the textual format (on the commandline for example).
    return PASS_NAME;
  }
  StringRef getDescription() const final {
    // This is a brief description of the pass.
    return "Apply Modification to tensors in IR ready for compression, "
           "scheduling & tensor allocation";
  }

  void runOnFunction() override;
};

void PlannerIRSort::runOnFunction() {
  auto function = getFunction();
  auto *ctx = &getContext();

  if (function.getName() == "main") {
    if (apply_pass(function, ctx) > 0)
      signalPassFailure();
  }
}

} // anonymous namespace

std::unique_ptr<OperationPass<FuncOp>> CreatePlannerIRSort() {
  return absl::make_unique<PlannerIRSort>();
}

static PassRegistration<PlannerIRSort> pass;
} // namespace planner
} // namespace mlir

/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#include "absl/memory/memory.h"
#include "mlir/Dialect/Quant/QuantOps.h"
#include "mlir/Dialect/Quant/QuantTypes.h"
#include "mlir/Dialect/StandardOps/IR/Ops.h"      // from @llvm-project
#include "mlir/IR/AffineMap.h"                    // from @llvm-project
#include "mlir/IR/Attributes.h"                   // from @llvm-project
#include "mlir/IR/BlockAndValueMapping.h"         // from @llvm-project
#include "mlir/IR/Builders.h"                     // from @llvm-project
#include "mlir/IR/Location.h"                     // from @llvm-project
#include "mlir/IR/MLIRContext.h"                  // from @llvm-project
#include "mlir/IR/Operation.h"                    // from @llvm-project
#include "mlir/IR/PatternMatch.h"                 // from @llvm-project
#include "mlir/Interfaces/InferTypeOpInterface.h" // from @llvm-project
#include "mlir/Pass/Pass.h"                       // from @llvm-project
#include "mlir/Transforms/DialectConversion.h"    // from @llvm-project

#include "src/transforms/passes.h"

#include "src/ir/kernel_ir.h"
#include "src/ir/schedule_ir.h"

#include "src/ir/architecture_config/architecture_config.h"
#include "src/utils/checkIR.h"
#include "src/utils/cluster_ir.h"
#include "src/utils/dag.h"
#include "src/utils/printIR.h"

#include "src/transforms/kernel_packing/kernel_packing.h"

using namespace std;
#include <iostream>

#define PASS_NAME "planner-kernel_packing"
#define DEBUG_TYPE PASS_NAME

namespace mlir {
namespace planner {
namespace {

class PlannerKernelPacking
    : public PassWrapper<PlannerKernelPacking, FunctionPass> {
public:
  explicit PlannerKernelPacking() {}

  StringRef getArgument() const final {
    // This is the argument used to refer to the pass in
    // the textual format (on the commandline for example).
    return PASS_NAME;
  }
  StringRef getDescription() const final {
    // This is a brief description of the pass.
    return "Apply Kernel Packing to IR";
  }

  void set_hw_maplist(
      std::unordered_map<std::string, mlir::utils::MappingStructure> map_list) {
    hw_maplist = map_list;
  }
  void runOnFunction() override;

private:
  std::unordered_map<std::string, mlir::utils::MappingStructure> hw_maplist;
};

void PlannerKernelPacking::runOnFunction() {
  auto function = getFunction();
  auto *context = &getContext();

  Builder builder(context);
  OpBuilder op_builder(context);

  if (function.getName() == "main") {
    Region *region = function.getCallableRegion();
    Block &block = region->front();
    llvm::iplist<Operation>::iterator i;

    for (i = block.begin(); i != block.end(); i++) {
      if (llvm::isa<scheduleir::NpuRegionOp>(i)) {
        Operation *op = &(*i);

        mlir::ArchitectureConfig::ArchitectureConfigAttr architecture_config =
            AccessArchitectureConfigAttribute_From_NpuRegion(op);

        mlir::CompilerConfig::CompilerConfigAttr compiler_config =
            AccessCompilerConfigAttribute_From_NpuRegion(op);

        /*
           If an Operation is going to end up in a single kernel and cause
           problems to map onto the NPU Pipeline we consider inserting a
           Identity Convolution. Doing it on a case by case bases at the moment
        */
        Region &region_instruction = i->getRegion(0);
        Block &block_npu_region = region_instruction.front();

        dag_graph dag;
        if (dag.LoadBlock2DAGRepresentation(block_npu_region) != 0) {
          std::cout << "PlannerKernelPacking::PackSingleOps::"
                       "ConvertBlock2DAGRepresentation failed"
                    << std::endl;
          signalPassFailure();
        }

        /* Combine the Ops into Kernels */
        std::vector<kernelInfo> kernels;
        KernelPacker(context, dag, block_npu_region, architecture_config,
                     kernels, hw_maplist);

        for (kernelInfo &k : kernels) {
          Operation *clusterOp = nullptr;
          if (Cluster(dag, k.ops, k.ops_const, block_npu_region,
                      cluster_op::kernel_region_op, context, clusterOp) != 0) {
            std::cout << "PlannerKernelPacking::Clustering failed" << std::endl;
            signalPassFailure();
          }

          Operation *major_op = mlir::kernelir::getKernelMajorOp(clusterOp);
          NpuBlockType block_type =
              (NpuBlockType)major_op
                  ->getAttrOfType<::mlir::IntegerAttr>("block_type")
                  .getInt();

          clusterOp->setAttr("name", builder.getStringAttr(k.name));
          clusterOp->setAttr("block_type",
                             builder.getI32IntegerAttr(int(block_type)));
          clusterOp->setAttr("block_config", builder.getI32ArrayAttr(0));

          clusterOp->setAttr("shared_buffer_banks_locations",
                             builder.getI32ArrayAttr(0));
          clusterOp->setAttr("shared_buffer_banks_required",
                             builder.getI32ArrayAttr(0));
          clusterOp->setAttr("shared_buffer_use_accumulator_element",
                             builder.getI32IntegerAttr(0));
          clusterOp->setAttr("shared_buffer_ifm_count",
                             builder.getI32IntegerAttr(0));

          clusterOp->setAttr("frontend_rescale_mode",
                             builder.getStringAttr("NONE"));
          clusterOp->setAttr("input_zp", builder.getI32IntegerAttr(0));
          clusterOp->setAttr("weight_zp", builder.getI32IntegerAttr(0));
          clusterOp->setAttr("input0_zp", builder.getI32IntegerAttr(0));
          clusterOp->setAttr("input0_multiplier", builder.getI32IntegerAttr(1));
          clusterOp->setAttr("input0_shift", builder.getI32IntegerAttr(0));
          clusterOp->setAttr("input1_zp", builder.getI32IntegerAttr(0));
          clusterOp->setAttr("input1_multiplier", builder.getI32IntegerAttr(1));
          clusterOp->setAttr("input1_shift", builder.getI32IntegerAttr(0));
          clusterOp->setAttr("output_zp", builder.getI32IntegerAttr(0));
          clusterOp->setAttr("output_multiplier", builder.getI32ArrayAttr({1}));
          clusterOp->setAttr("output_shift", builder.getI32ArrayAttr({0}));
          clusterOp->setAttr("scale32", builder.getBoolAttr(true));
          clusterOp->setAttr("double_round", builder.getBoolAttr(false));
          clusterOp->setAttr("per_channel", builder.getBoolAttr(false));
        }
      }
    }
  }
}

} // anonymous namespace

std::unique_ptr<OperationPass<FuncOp>> CreatePlannerKernelPacking(
    std::unordered_map<std::string, mlir::utils::MappingStructure> hw_maplist) {
  auto ptr = absl::make_unique<PlannerKernelPacking>();
  ptr->set_hw_maplist(hw_maplist);
  return ptr;
}

static PassRegistration<PlannerKernelPacking> kernel;

} // namespace planner
} // namespace mlir

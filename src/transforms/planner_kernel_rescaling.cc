/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#include "absl/memory/memory.h"
#include "mlir/Dialect/Quant/QuantOps.h"
#include "mlir/Dialect/Quant/QuantTypes.h"
#include "mlir/Dialect/StandardOps/IR/Ops.h"      // from @llvm-project
#include "mlir/IR/AffineMap.h"                    // from @llvm-project
#include "mlir/IR/Attributes.h"                   // from @llvm-project
#include "mlir/IR/BlockAndValueMapping.h"         // from @llvm-project
#include "mlir/IR/Builders.h"                     // from @llvm-project
#include "mlir/IR/Location.h"                     // from @llvm-project
#include "mlir/IR/MLIRContext.h"                  // from @llvm-project
#include "mlir/IR/Operation.h"                    // from @llvm-project
#include "mlir/IR/PatternMatch.h"                 // from @llvm-project
#include "mlir/Interfaces/InferTypeOpInterface.h" // from @llvm-project
#include "mlir/Pass/Pass.h"                       // from @llvm-project
#include "mlir/Transforms/DialectConversion.h"    // from @llvm-project

#include "src/transforms/passes.h"

#include "src/ir/kernel_ir.h"
#include "src/ir/schedule_ir.h"

#include "src/ir/architecture_config/architecture_config.h"
#include "src/utils/checkIR.h"
#include "src/utils/cluster_ir.h"
#include "src/utils/dag.h"
#include "src/utils/printIR.h"

#include "src/transforms/kernel_packing/kernel_packing.h"

using namespace std;
#include <iostream>

#define PASS_NAME "planner-kernel_rescaling"
#define DEBUG_TYPE PASS_NAME

namespace mlir {
namespace planner {
namespace {

class PlannerKernelRescaling
    : public PassWrapper<PlannerKernelRescaling, FunctionPass> {
public:
  explicit PlannerKernelRescaling() {}

  StringRef getArgument() const final {
    // This is the argument used to refer to the pass in
    // the textual format (on the commandline for example).
    return PASS_NAME;
  }
  StringRef getDescription() const final {
    // This is a brief description of the pass.
    return "Apply Kernel Rescaling to IR";
  }

  void runOnFunction() override;

private:
};

void PlannerKernelRescaling::runOnFunction() {
  auto function = getFunction();
  auto *context = &getContext();

  Builder builder(context);

  if (function.getName() == "main") {
    Region *region = function.getCallableRegion();
    Block &block = region->front();
    llvm::iplist<Operation>::iterator i;

    for (i = block.begin(); i != block.end(); i++) {
      if (llvm::isa<scheduleir::NpuRegionOp>(i)) {
        Operation *op = &(*i);

        mlir::ArchitectureConfig::ArchitectureConfigAttr architecture_config =
            AccessArchitectureConfigAttribute_From_NpuRegion(op);

        mlir::CompilerConfig::CompilerConfigAttr compiler_config =
            AccessCompilerConfigAttribute_From_NpuRegion(op);

        Region &region_instruction = i->getRegion(0);
        Block &block_instruction = region_instruction.front();
        llvm::iplist<Operation> &operations_npu_region =
            block_instruction.getOperations();
        for (auto &op : operations_npu_region) {
          if (llvm::isa<kernelir::KernelRegionOp>(op)) {
            Operation *op_rescale_attr =
                mlir::kernelir::getKernelOperationwithRescaleAttributes(&op);
            NpuBlockType block_type = (NpuBlockType)(
                op.getAttrOfType<::mlir::IntegerAttr>("block_type").getInt());

            bool quant_present = false;
            if ((op_rescale_attr != nullptr) and
                ((block_type == VectorProduct) or
                 (block_type == ConvolutionDepthWise) or
                 (block_type == Pooling) or (block_type == ConvolutionMxN) or
                 (block_type == ElementWise))) {
              quant_present = true;
              if (llvm::isa<scheduleir::MaxPool2dOp>(op_rescale_attr) or
                  llvm::isa<scheduleir::AvgPool2dOp>(op_rescale_attr))
                quant_present = op_rescale_attr
                                    ->getAttrOfType<::mlir::BoolAttr>(
                                        StringRef("quant_present"))
                                    .getValue();
            }

            if (quant_present) {
              // 1. Move Rescale Attributes to the Kernel Level
              if (llvm::isa<scheduleir::AddOp>(op_rescale_attr)) {
                op.setAttr("frontend_rescale_mode",
                           builder.getStringAttr("ELEMENTWISE_ADDSUB"));
                op.setAttr("input0_zp",
                           op_rescale_attr->getAttrOfType<mlir::IntegerAttr>(
                               "input0_zp"));
                op.setAttr("input0_multiplier",
                           op_rescale_attr->getAttrOfType<mlir::IntegerAttr>(
                               "input0_multiplier"));
                op.setAttr("input0_shift",
                           op_rescale_attr->getAttrOfType<mlir::IntegerAttr>(
                               "input0_shift"));
                op.setAttr("input1_zp",
                           op_rescale_attr->getAttrOfType<mlir::IntegerAttr>(
                               "input1_zp"));
                op.setAttr("input1_multiplier",
                           op_rescale_attr->getAttrOfType<mlir::IntegerAttr>(
                               "input1_multiplier"));
                op.setAttr("input1_shift",
                           op_rescale_attr->getAttrOfType<mlir::IntegerAttr>(
                               "input1_shift"));
                op.setAttr("output_zp",
                           op_rescale_attr->getAttrOfType<mlir::IntegerAttr>(
                               "output_zp"));
                int32_t output_multiplier =
                    op_rescale_attr
                        ->getAttrOfType<mlir::IntegerAttr>("output_multiplier")
                        .getInt();
                int32_t output_shift =
                    op_rescale_attr
                        ->getAttrOfType<mlir::IntegerAttr>("output_shift")
                        .getInt();
                op.setAttr("output_multiplier",
                           builder.getI32ArrayAttr({output_multiplier}));
                op.setAttr("output_shift",
                           builder.getI32ArrayAttr({output_shift}));
                op.setAttr("scale32", builder.getBoolAttr(true));
                op.setAttr("double_round", builder.getBoolAttr(false));
                op.setAttr("per_channel", builder.getBoolAttr(false));
              } else if (llvm::isa<scheduleir::MulOp>(op_rescale_attr)) {
                op.setAttr("frontend_rescale_mode",
                           builder.getStringAttr("NONE"));
                op.setAttr("output_zp",
                           op_rescale_attr->getAttrOfType<mlir::IntegerAttr>(
                               "output_zp"));
                int32_t output_multiplier =
                    op_rescale_attr
                        ->getAttrOfType<mlir::IntegerAttr>("output_multiplier")
                        .getInt();
                int32_t output_shift =
                    op_rescale_attr
                        ->getAttrOfType<mlir::IntegerAttr>("output_shift")
                        .getInt();
                op.setAttr("output_multiplier",
                           builder.getI32ArrayAttr({output_multiplier}));
                op.setAttr("output_shift",
                           builder.getI32ArrayAttr({output_shift}));
                op.setAttr("scale32", builder.getBoolAttr(true));
                op.setAttr("double_round", builder.getBoolAttr(false));
                op.setAttr("per_channel", builder.getBoolAttr(false));
              } else {
                op.setAttr("frontend_rescale_mode",
                           builder.getStringAttr("CONV"));
                op.setAttr("input_zp",
                           op_rescale_attr->getAttrOfType<mlir::IntegerAttr>(
                               "input_zp"));
                op.setAttr("output_zp",
                           op_rescale_attr->getAttrOfType<mlir::IntegerAttr>(
                               "output_zp"));
                op.setAttr("output_multiplier",
                           op_rescale_attr->getAttrOfType<mlir::ArrayAttr>(
                               "multiplier"));
                op.setAttr(
                    "output_shift",
                    op_rescale_attr->getAttrOfType<mlir::ArrayAttr>("shift"));
                op.setAttr(
                    "scale32",
                    op_rescale_attr->getAttrOfType<mlir::BoolAttr>("scale32"));
                op.setAttr("double_round",
                           op_rescale_attr->getAttrOfType<mlir::BoolAttr>(
                               "double_round"));
                op.setAttr("per_channel",
                           op_rescale_attr->getAttrOfType<mlir::BoolAttr>(
                               "per_channel"));
              }
              if (block_type == Pooling)
                op.setAttr("fused_quantize", builder.getBoolAttr(true));
              else
                op.setAttr("fused_quantize", builder.getBoolAttr(false));
              op.setAttr("quant_present", builder.getBoolAttr(true));
            } else {
              op.setAttr("fused_quantize", builder.getBoolAttr(false));
              op.setAttr("quant_present", builder.getBoolAttr(false));
            }

            int32_t min = 0;
            int32_t max = 0;
            SmallVector<Operation *, 1> act =
                mlir::kernelir::getKernelActivationsOps(&op);
            ASSERT_COND(act.size() > 1, "planner_kernel_rescaling::A Kernel "
                                        "should have zero or one Act Node");
            if (act.size() == 1) {
              Operation *op_a = act[0];
              if (llvm::isa<scheduleir::ClampOp>(op_a)) {
                min =
                    op_a->getAttrOfType<mlir::IntegerAttr>("min_int").getInt();
                max =
                    op_a->getAttrOfType<mlir::IntegerAttr>("max_int").getInt();
              } else
                FATAL_ERROR("planner_kernel_rescaling::Illegal Activation Op")
            }
            op.setAttr("act_min", builder.getI64IntegerAttr(min));
            op.setAttr("act_max", builder.getI64IntegerAttr(max));
          }
        }
      }
    }
  }
}

} // anonymous namespace

std::unique_ptr<OperationPass<FuncOp>> CreatePlannerKernelRescaling() {
  auto ptr = absl::make_unique<PlannerKernelRescaling>();
  return ptr;
}

static PassRegistration<PlannerKernelRescaling> kernel;

} // namespace planner
} // namespace mlir

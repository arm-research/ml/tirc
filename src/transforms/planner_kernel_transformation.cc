/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#include "absl/memory/memory.h"
#include "mlir/Dialect/Quant/QuantOps.h"
#include "mlir/Dialect/Quant/QuantTypes.h"
#include "mlir/Dialect/StandardOps/IR/Ops.h"      // from @llvm-project
#include "mlir/IR/AffineMap.h"                    // from @llvm-project
#include "mlir/IR/Attributes.h"                   // from @llvm-project
#include "mlir/IR/BlockAndValueMapping.h"         // from @llvm-project
#include "mlir/IR/Builders.h"                     // from @llvm-project
#include "mlir/IR/Location.h"                     // from @llvm-project
#include "mlir/IR/MLIRContext.h"                  // from @llvm-project
#include "mlir/IR/Operation.h"                    // from @llvm-project
#include "mlir/IR/PatternMatch.h"                 // from @llvm-project
#include "mlir/Interfaces/InferTypeOpInterface.h" // from @llvm-project
#include "mlir/Pass/Pass.h"                       // from @llvm-project
#include "mlir/Transforms/DialectConversion.h"    // from @llvm-project

#include "src/transforms/passes.h"

#include "src/ir/kernel_ir.h"
#include "src/ir/schedule_ir.h"

#include "src/ir/architecture_config/architecture_config.h"
#include "src/utils/checkIR.h"
#include "src/utils/cluster_ir.h"
#include "src/utils/dag.h"
#include "src/utils/printIR.h"

#include "src/transforms/kernel_packing/kernel_packing.h"

using namespace std;
#include <iostream>

#define PASS_NAME "planner-kernel_transformation"
#define DEBUG_TYPE PASS_NAME

namespace mlir {
namespace planner {
namespace {

class PlannerKernelTransformation
    : public PassWrapper<PlannerKernelTransformation, FunctionPass> {
public:
  explicit PlannerKernelTransformation() {}

  StringRef getArgument() const final {
    // This is the argument used to refer to the pass in
    // the textual format (on the commandline for example).
    return PASS_NAME;
  }
  StringRef getDescription() const final {
    // This is a brief description of the pass.
    return "Calculate the IO Geometric Transformation for the kernel";
  }

  void runOnFunction() override;

private:
};

void PlannerKernelTransformation::runOnFunction() {
  auto function = getFunction();
  auto *context = &getContext();

  Builder builder(context);

  if (function.getName() == "main") {
    Region *region = function.getCallableRegion();
    Block &block = region->front();
    llvm::iplist<Operation>::iterator i;

    for (i = block.begin(); i != block.end(); i++) {
      if (llvm::isa<scheduleir::NpuRegionOp>(i)) {
        Operation *op = &(*i);

        mlir::ArchitectureConfig::ArchitectureConfigAttr architecture_config =
            AccessArchitectureConfigAttribute_From_NpuRegion(op);

        mlir::CompilerConfig::CompilerConfigAttr compiler_config =
            AccessCompilerConfigAttribute_From_NpuRegion(op);

        Region &region_instruction = i->getRegion(0);
        Block &block_instruction = region_instruction.front();
        llvm::iplist<Operation> &operations_npu_region =
            block_instruction.getOperations();
        for (auto &op : operations_npu_region) {
          if (llvm::isa<kernelir::KernelRegionOp>(op)) {
            // For every Kernel we calculate a Affine transform from each input
            // to each output and store it in the Attribute
            attr_unique_id unique_id = 0;
            auto attr = mlir::AffineTransformAttr::get(&op, unique_id);
            op.setAttr("geometric_transform", attr);
          }
        }
      }
    }
  }
}

} // anonymous namespace

std::unique_ptr<OperationPass<FuncOp>> CreatePlannerKernelTransformation() {
  auto ptr = absl::make_unique<PlannerKernelTransformation>();
  return ptr;
}

static PassRegistration<PlannerKernelTransformation> kernel;

} // namespace planner
} // namespace mlir

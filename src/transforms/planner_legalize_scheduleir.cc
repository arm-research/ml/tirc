/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#include <climits>
#include <cstddef>
#include <cstdint>
#include <fstream>
#include <iterator>
#include <numeric>
#include <unordered_set>

#include "mlir/Dialect/Tosa/IR/TosaOps.h"               // from @llvm-project
#include "mlir/Support/LLVM.h"                          // from @llvm-project
#include "mlir/Transforms/GreedyPatternRewriteDriver.h" // from @llvm-project
#include "src/transforms/passes.h"
#include "tensorflow/compiler/mlir/lite/ir/tfl_ops.h"
#include "tensorflow/compiler/mlir/tosa/transforms/legalize_common.h"
#include "tensorflow/compiler/mlir/tosa/transforms/legalize_utils.h"

#include "src/ir/schedule_ir.h"

#include "src/transforms/legalization/partition.h"
#include "src/transforms/legalization/scheduleir/legalization_scheduleir.h"

#include "src/utils/assert.h"
#include "src/utils/ir_sort.h"
#include "src/utils/ir_utils.h"
#include "src/utils/printIR.h"

#include "src/ir/architecture_config/architecture_config.h"
#include "src/ir/architecture_config/architecture_config_utils.h"
#include "src/ir/compiler_config/compiler_config.h"

#define PASS_NAME "legalize-scheduleir"
#define DEBUG_TYPE PASS_NAME

namespace mlir {
namespace planner {
namespace {

void Legalize(MLIRContext *context, FuncOp &func) {
  Region *region = func.getCallableRegion();
  llvm::iplist<Block> &blocks = region->getBlocks();
  if (blocks.size() > 1) {
    FATAL_ERROR("PlannerLegalizeScheduleIR:::More than a single block in main "
                "function callable region");
  }
  Block &block = region->front();

  /*
      If the Batch size is set to -1 the schedulir dialect forces it to 1. The
     TFlite component is left to -1 which is a debatable approach as then the
     runtime needs to execute it with a batch size of 1 and nothing else or the
     npu commands will not functionally work.
      ** To rethink if we should also be updating the main func block and
     interface to a batch size of 1 especially if we want to allow batch size
     custom compilation
      ** Eventually models with a batch set to -1 will return a failure at this
     stage
   */

  // Convert TOSA -> ScheduleIR forcing the batch size to a constant
  {
    OwningRewritePatternList patterns(context);
#define DEF_PATTERN_INSERT(PAT) patterns.insert<ConvertTosa##PAT##Op>(context);
    DEF_PATTERN_INSERT(Const);
    DEF_PATTERN_INSERT(Clamp);
    DEF_PATTERN_INSERT(Conv2D);
    DEF_PATTERN_INSERT(DepthwiseConv2D);
    DEF_PATTERN_INSERT(Add);
    DEF_PATTERN_INSERT(Mul);
    DEF_PATTERN_INSERT(MaxPool2d);
    DEF_PATTERN_INSERT(AvgPool2d);
    DEF_PATTERN_INSERT(Concat);
    DEF_PATTERN_INSERT(Rescale);
    applyPatternsAndFoldGreedily(func, std::move(patterns));
  }

  // Fold Rescales
  {
    OwningRewritePatternList patterns(context);
#define DEF_PATTERN_INSERT_0(PAT)                                              \
  patterns.insert<ConvertScheduleIR##PAT##Op>(context);
    DEF_PATTERN_INSERT_0(Conv2D);
    DEF_PATTERN_INSERT_0(DepthwiseConv2D);
    applyPatternsAndFoldGreedily(func, std::move(patterns));
  }
  // Isolate the Elementiwise Ops as there is a chance that a Add does not have
  // a Rescale on the input so first we prioritise DPU operations
  {
    OwningRewritePatternList patterns(context);
#define DEF_PATTERN_INSERT_1(PAT)                                              \
  patterns.insert<ConvertScheduleIR##PAT##Op>(context);
    DEF_PATTERN_INSERT_1(Add);
    DEF_PATTERN_INSERT_1(Mul);
    applyPatternsAndFoldGreedily(func, std::move(patterns));
  }
  // Convert the left Standalone Rescales into Depthwsie Convolutions to map
  // onto the DPU Pipeline
  {
    OwningRewritePatternList patterns(context);
    patterns.insert<RemoveScheduleIRRescaleOp>(context);
    applyPatternsAndFoldGreedily(func, std::move(patterns));
  }

  uint64_t no_operators = 0;
  uint64_t no_cpu_operators = 0;
  uint64_t no_npu_operators = 0;

  for (llvm::iplist<Operation>::iterator i = block.begin(); i != block.end();
       i++) {
    if (i->getName().getStringRef().str() == "std.return")
      continue;
    else if (i->getName().getStringRef().str() == "std.constant")
      continue;
    else if (i->getName().getStringRef().str() == "arith.constant")
      continue;
    else if (i->getName().getStringRef().str() == "scheduleir.const")
      continue;
    else if (i->getName().getStringRef().str() == "scheduleir.yield")
      continue;
    else if (i->getName().getStringRef().str() == "tfl.pseudo_qconst")
      continue;
    else if (i->getName().getStringRef().str() == "tfl.pseudo_const")
      continue;

    std::string op_dialect = i->getDialect()->getNamespace().str();
    if (op_dialect == "scheduleir")
      no_npu_operators++;
    else
      no_cpu_operators++;
  }
  no_operators = no_npu_operators + no_cpu_operators;

  std::cout << std::endl;
  std::cout << "No Operators: " << no_operators
            << " No NPU Operators: " << no_npu_operators
            << " No CPU Operators: " << no_cpu_operators << std::endl;

  /* printIR(block, mlir::utils::info_option::off); */
}

mlir::Operation *CloneConstOp(OpBuilder &builder, Operation *op) {
  mlir::Location loc = builder.getUnknownLoc();

  auto resultType = op->getResult(0).getType().dyn_cast<SchedTensorType>();
  /* Not doing a deep copy intentionally as nothing should have been called
   * before this pass */
  Type resultType_new = SchedTensorType::get(
      resultType.getShape(), resultType.getElementType(), getUniqueId());

  llvm::iplist<Operation>::iterator insertion_Point(op);
  builder.setInsertionPoint(op->getBlock(), insertion_Point);

  Operation *op_new = builder.create<scheduleir::ConstOp>(loc, resultType_new);

  /* Copy over attributes */
  auto attrs_old = op->getAttrDictionary();
  op_new->setAttrs(op->getAttrs());

  return op_new;
}

int ConstantReplication(MLIRContext *context, FuncOp &func) {
  OpBuilder builder(context);
  std::vector<Operation *> to_delete;

  Region *region = func.getCallableRegion();
  llvm::iplist<Block> &blocks = region->getBlocks();
  if (blocks.size() > 1) {
    std::cout << "PlannerIRFormat:::More than a single block "
                 "in function"
              << std::endl;
    return 1;
  }
  llvm::iplist<Operation> &operations = region->front().getOperations();
  for (auto &op : operations) {
    if (llvm::isa<scheduleir::ConstOp>(op)) {
      std::vector<connection> consumers = ExtractConsumers(&op);
      if (consumers.size() > 1) {
        for (auto c = consumers.begin(); c != consumers.end(); c++) {
          auto op_new = CloneConstOp(builder, &op);
          SmallVector<Value, 1> replacment_operand;
          replacment_operand.push_back(op_new->getOpResult(0));
          c->external_operation->setOperands(c->external_slot_index, 1,
                                             replacment_operand);
          // std::cout << &op << " " << c->external_operation << " " <<
          // c->external_slot_index << " " << op_new << std::endl;
        }
        to_delete.push_back(&op);
      }
    }
  }
  for (auto op : to_delete) {
    op->dropAllReferences();
    op->dropAllDefinedValueUses();
    op->erase();
  }
  return 0;
}

void CheckIR(MLIRContext *context, FuncOp &func) {
  Region *region = func.getCallableRegion();
  llvm::iplist<Block> &blocks = region->getBlocks();
  if (blocks.size() > 1) {
    FATAL_ERROR("PlannerLegalizeScheduleIR:::More than a single block in main "
                "function callable region");
  }
  Block &block = region->front();

  // 1. Check no scheduleir or tosa ops are left other than NpuRegionOp
  for (llvm::iplist<Operation>::iterator i = block.begin(); i != block.end();
       i++) {
    if (llvm::isa<TFL::SoftmaxOp>(i))
      continue;
    else if (llvm::isa<TFL::ReshapeOp>(i))
      continue;
    else if (llvm::isa<TFL::SqueezeOp>(i))
      continue;
    else if (llvm::isa<TFL::FullyConnectedOp>(i))
      continue;
    else if (llvm::isa<TFL::QConstOp>(i))
      continue;
    else if (i->getName().getStringRef().str() == "std.return")
      continue;
    else if (i->getName().getStringRef().str() == "std.constant")
      continue;
    else if (i->getName().getStringRef().str() == "arith.constant")
      continue;
    else if (llvm::isa<scheduleir::NpuRegionOp>(i))
      continue;
    else {
      printOp(*i);
      FATAL_ERROR("PlannerExtractNPURegion::Illegal Ops still present in IR");
    }
  }

  // Check no Rescales left inside each NPU Region
  for (llvm::iplist<Operation>::iterator i = block.begin(); i != block.end();
       i++) {
    if (llvm::isa<scheduleir::NpuRegionOp>(i)) {
      Region &region_npu = i->getRegion(0);
      Block &block_npu_region = region_npu.front();
      for (llvm::iplist<Operation>::iterator j = block_npu_region.begin();
           j != block_npu_region.end(); j++)
        if (llvm::isa<scheduleir::RescaleOp>(j))
          FATAL_ERROR("PlannerLegalizeScheduleIR::scheuleir::Rescale still "
                      "present in IR");
    }
  }
}

int SortIR(MLIRContext *context, FuncOp &func,
           mlir::ArchitectureConfig::ArchitectureConfigAttr &arch_c,
           mlir::CompilerConfig::CompilerConfigAttr &compiler_config) {
  ir_sort(func, context, arch_c, compiler_config);

  Region *region = func.getCallableRegion();
  llvm::iplist<Block> &blocks = region->getBlocks();
  if (blocks.size() > 1) {
    std::cout << "PlannerIRSort:::More than a single block "
                 "in function"
              << std::endl;
    return 1;
  }
  llvm::iplist<Operation> &operations = region->front().getOperations();
  for (auto &op : operations) {
    if (llvm::isa<scheduleir::NpuRegionOp>(op)) {
      ir_sort(op);
    }
  }
}

// Performs lowering to TOSA dialect.
class LegalizeScheduleIR
    : public PassWrapper<LegalizeScheduleIR, FunctionPass> {
public:
  explicit LegalizeScheduleIR() {}

  StringRef getArgument() const final {
    // This is the argument used to refer to the pass in
    // the textual format (on the commandline for example).
    return PASS_NAME;
  }
  StringRef getDescription() const final {
    // This is a brief description of the pass.
    return "Legalize from Tosa Lite to ScheduleIR";
  }

  void runOnFunction() override;

  mlir::CompilerConfig::CompilerConfigAttr comp_config;
  mlir::ArchitectureConfig::ArchitectureConfigAttr arch_config;
};

void LegalizeScheduleIR::runOnFunction() {
  auto *context = &getContext();
  auto func = getFunction();

  /* Legalize Tosa Dialect to ScehduleIR using a deny list */
  Legalize(context, func);

  ConstantReplication(context, func);

  // * 0: Extract Subgraphs 1: Create NpuRegions 2: Insert Casts between
  // * SchedTensorType and RankedTensorType
  Partition(context, func);

  // Add Attribute to NPU Region
  AddArchitectureConfigAttribute_to_NpuRegions(arch_config, func, context);
  AddCompilerConfigAttribute_to_NpuRegions(comp_config, func, context);

  SortIR(context, func, arch_config, comp_config);

  CheckIR(context, func);
}
} // namespace

std::unique_ptr<OperationPass<FuncOp>> CreateLegalizeScheduleIR(
    mlir::ArchitectureConfig::ArchitectureConfigAttr &arch_config,
    mlir::CompilerConfig::CompilerConfigAttr comp_config) {
  auto p = std::make_unique<LegalizeScheduleIR>();
  p->comp_config = comp_config;
  p->arch_config = arch_config;
  return p;
}

static PassRegistration<LegalizeScheduleIR> pass;

} // namespace planner
} // namespace mlir

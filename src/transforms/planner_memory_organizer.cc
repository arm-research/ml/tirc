/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#include "absl/memory/memory.h"
#include "mlir/Dialect/Quant/QuantOps.h"
#include "mlir/Dialect/Quant/QuantTypes.h"
#include "mlir/Dialect/StandardOps/IR/Ops.h"      // from @llvm-project
#include "mlir/IR/AffineMap.h"                    // from @llvm-project
#include "mlir/IR/Attributes.h"                   // from @llvm-project
#include "mlir/IR/BlockAndValueMapping.h"         // from @llvm-project
#include "mlir/IR/Builders.h"                     // from @llvm-project
#include "mlir/IR/Location.h"                     // from @llvm-project
#include "mlir/IR/MLIRContext.h"                  // from @llvm-project
#include "mlir/IR/Operation.h"                    // from @llvm-project
#include "mlir/IR/PatternMatch.h"                 // from @llvm-project
#include "mlir/Interfaces/InferTypeOpInterface.h" // from @llvm-project
#include "mlir/Pass/Pass.h"                       // from @llvm-project
#include "mlir/Transforms/DialectConversion.h"    // from @llvm-project

#include "src/ir/architecture_config/architecture_config.h"
#include "src/transforms/passes.h"
#include "src/utils/ir_utils.h"
#include "src/utils/printIR.h"

#include "src/ir/schedule_ir.h"
#include "src/utils/dag.h"
#include "tensorflow/compiler/mlir/lite/ir/tfl_ops.h"

#include "src/transforms/memory_organizer/LiveRange.h"
#include "src/transforms/memory_organizer/LiveRangeGraph.h"
#include "src/transforms/memory_organizer/LivenessAnalysis.h"

#include "src/transforms/memory_organizer/GreedyAllocator.h"
#include "src/transforms/memory_organizer/HillClimbSearchAllocator.h"
#include "src/transforms/memory_organizer/LinearAllocator.h"

using namespace std;
#include <iostream>

#define PASS_NAME "planner-memory-organizer"
#define DEBUG_TYPE PASS_NAME

namespace mlir {

namespace planner {

namespace {

// Go Through Each Op and Change the Operand and ResultOperands type from
// Ranked Tensor Type to ScheduleTensorType and do any TensorMarkup Required
int apply_pass(FuncOp function, MLIRContext *context) {

  OpBuilder builder(context);

  Region *region = function.getCallableRegion();
  llvm::iplist<Block> &blocks = region->getBlocks();
  if (blocks.size() > 1) {
    std::cout << "PlannerMemoryOrganizer:::More than a single block "
                 "in function"
              << std::endl;
    return 1;
  }

  /* Calculate live ranges & allocation for tensors marked to be placed into
   * System Flash */
  llvm::iplist<Operation> &operations = region->front().getOperations();
  for (auto &op : operations) {
    if (llvm::isa<scheduleir::NpuRegionOp>(op)) {
      mlir::ArchitectureConfig::ArchitectureConfigAttr arch_c =
          AccessArchitectureConfigAttribute_From_NpuRegion(&op);

      mlir::CompilerConfig::CompilerConfigAttr compiler_config =
          AccessCompilerConfigAttribute_From_NpuRegion(&op);

      Region &region_npureg = op.getRegion(0);
      Block &block_npureg = region_npureg.front();

      std::set<value_mem_type> target_value_mem_type_set_intermettent = {
          mt_Scratch, mt_Scratch_fast};
      LiveRangeGraph lrg;

      LivenessAnalysis_Cascades(lrg, &block_npureg, value_mem_area::ma_Sram,
                                target_value_mem_type_set_intermettent, context,
                                arch_c);
      // std::cout << lrg << std::endl;

      // std::cout << BLUE << "Applying Linear Allocator" << RESET << std::endl;
      // linear_allocation(lrg, arch_c);

      // std::cout << BLUE << "Applying Greedy Allocator" << RESET << std::endl;
      // greedy_allocation(lrg, arch_c);

      // std::cout << BLUE << "Applying Hill Climb Allocator" << RESET <<
      // std::endl;
      hill_climb_search_allocation(lrg, arch_c);
    }
  }

  return 0;
}

class PlannerMemoryOrganizer
    : public PassWrapper<PlannerMemoryOrganizer, FunctionPass> {
public:
  explicit PlannerMemoryOrganizer() {}

  StringRef getArgument() const final {
    // This is the argument used to refer to the pass in
    // the textual format (on the commandline for example).
    return PASS_NAME;
  }
  StringRef getDescription() const final {
    // This is a brief description of the pass.
    return "Apply Modification to tensors in IR ready for compression, "
           "scheduling & tensor allocation";
  }

  void runOnFunction() override;
};

void PlannerMemoryOrganizer::runOnFunction() {
  auto function = getFunction();
  auto *ctx = &getContext();

  if (function.getName() == "main") {
    if (apply_pass(function, ctx) > 0)
      signalPassFailure();
  }
}

} // anonymous namespace

std::unique_ptr<OperationPass<FuncOp>> CreatePlannerMemoryOrganizer() {
  return absl::make_unique<PlannerMemoryOrganizer>();
}

static PassRegistration<PlannerMemoryOrganizer> pass;
} // namespace planner
} // namespace mlir

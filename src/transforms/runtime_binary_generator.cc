/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#include "absl/memory/memory.h"
#include "mlir/Dialect/Quant/QuantOps.h"
#include "mlir/Dialect/Quant/QuantTypes.h"
#include "mlir/Dialect/StandardOps/IR/Ops.h"      // from @llvm-project
#include "mlir/IR/AffineMap.h"                    // from @llvm-project
#include "mlir/IR/Attributes.h"                   // from @llvm-project
#include "mlir/IR/BlockAndValueMapping.h"         // from @llvm-project
#include "mlir/IR/Builders.h"                     // from @llvm-project
#include "mlir/IR/Location.h"                     // from @llvm-project
#include "mlir/IR/MLIRContext.h"                  // from @llvm-project
#include "mlir/IR/Operation.h"                    // from @llvm-project
#include "mlir/IR/PatternMatch.h"                 // from @llvm-project
#include "mlir/Interfaces/InferTypeOpInterface.h" // from @llvm-project
#include "mlir/Pass/Pass.h"                       // from @llvm-project
#include "mlir/Transforms/DialectConversion.h"    // from @llvm-project

#include "src/ir/hlcs_ir.h"
#include "src/ir/llcs_ir.h"

#include "src/transforms/binary_writer/driver_actions.h"
#include "src/transforms/llcs_generator/hlcs_mlir_utils.h"
#include "src/transforms/llcs_generator/range_set.h"
#include "src/transforms/passes.h"

#include "src/utils/assert.h"
#include "src/utils/data_structure_utils.h"
#include "src/utils/printIR.h"
#include "src/utils/string_utils.h"

#include "src/compiler/compiler_cl.h"

#include <set>
#include <unordered_map>
#include <vector>

#include "tensorflow/compiler/mlir/lite/ir/tfl_ops.h"

#include "tensorflow/lite/schema/schema_generated.h"

#include "flatbuffers/flatbuffers.h"

#include "src/transforms/binary_writer/generate_model.h"
#include "src/transforms/binary_writer/generate_tensors.h"
#include "src/transforms/binary_writer/serialize_model.h"
#include "src/transforms/binary_writer/utils.h"

using namespace std;
using namespace flatbuffers;
using namespace mlir;
using namespace hlcs;
using namespace llcs;

#define PASS_NAME "runtime-binary-generator"
#define DEBUG_TYPE PASS_NAME

using namespace std;

namespace mlir {
namespace binary_writer {
namespace {

int generate(mlir::MLIRContext &context, mlir::FuncOp &function) {
  // 0. Generate Custom Ops & Memory Organization CPU/NPU
  generate_tflite_custom_op(context, function);

  // 1. Serialize the IR to Tflite file
  FlatBufferBuilder builder;
  auto model = serialise_model(builder, context, function);
  tflite::FinishModelBuffer(builder, model);

  // 2. Write the Binary File
  size_t len = input_filename.size();
  std::string tflite_filename =
      input_filename.substr(0, len - 5) + "_tirc.tflite";
  ofstream outfile(tflite_filename.c_str(), ios::binary);
  outfile.write((const char *)(builder.GetBufferPointer()), builder.GetSize());
  outfile.close();

  return 0;
}

class RuntimeBinaryGenerator
    : public PassWrapper<RuntimeBinaryGenerator, FunctionPass> {
public:
  RuntimeBinaryGenerator() = default;

  StringRef getArgument() const final {
    // This is the argument used to refer to the pass in
    // the textual format (on the commandline for example).
    return PASS_NAME;
  }
  StringRef getDescription() const final {
    // This is a brief description of the pass.
    return "Generate Runtime Binary from LLCS";
  }

  void runOnFunction() override {
    auto func = getFunction();
    if (func.getName() != "main")
      return;
    auto &ctx = getContext();
    if (generate(ctx, func) != 0) {
      signalPassFailure();
    }
  }
};

} // anonymous namespace

std::unique_ptr<OperationPass<FuncOp>> CreateRuntimeBinaryGenerator() {
  return absl::make_unique<RuntimeBinaryGenerator>();
}

static PassRegistration<RuntimeBinaryGenerator> pass;
} // namespace binary_writer
} // namespace mlir

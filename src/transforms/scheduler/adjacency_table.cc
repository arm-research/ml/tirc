/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#include "src/transforms/scheduler/search_engine.h"

#include "src/utils/threading.h"

using namespace mlir::planner;

namespace mlir {
namespace planner {

void generate_adjacency_ops(SearchEngine *engine, std::vector<op_pair> &ops,
                            int32_t start, int32_t end,
                            uint32_t block_config_limit) {
  for (auto i = start; i < end; i++) {
    Operation *op = ops[i].op;
    if (llvm::isa<kernelir::KernelRegionOp>(op)) {
      kernelInfo &k = ops[i].k;

      k.weights = kernelir::ExtractKernelWeightValues(op);
      k.bias = kernelir::ExtractKernelBiasValues(op);
      k.inputs = kernelir::ExtractKernelInputValues(op);
      k.outputs = kernelir::ExtractKernelOutputValues(op);

      for (auto w : k.weights)
        k.weight_tensors.push_back(
            w.getType().dyn_cast<mlir::SchedTensorType>());
      for (auto i : k.inputs)
        k.input_tensors.push_back(
            i.getType().dyn_cast<mlir::SchedTensorType>());
      for (auto o : k.outputs)
        k.output_tensors.push_back(
            o.getType().dyn_cast<mlir::SchedTensorType>());
      for (auto b : k.bias)
        k.bias_tensors.push_back(b.getType().dyn_cast<mlir::SchedTensorType>());

      k.producers = ExtractProducers(op);
      k.consumers = ExtractConsumers(op);

      // Take a limited number of the largest blocks
      if (block_config_limit > 0) {
        engine->find_block_configs_suitable_for_kernel(op, k.all_block_configs,
                                                       block_config_limit);
      }

      k.KernelFilter = mlir::kernelir::getKernelFilter(op);
      k.BlockType = (NpuBlockType)(
          op->getAttrOfType<::mlir::IntegerAttr>("block_type").getInt());
      k.major_op = mlir::kernelir::getKernelMajorOp(op);

      k.act_ops = mlir::kernelir::getKernelActivationsOps(op);
    }
  }
}

void SearchEngine::generate_adjacency_table(uint32_t block_config_limit) {
  LOG(ENABLE_LOGGING_SEARCH_ENGINE_VERBOSE_LEVEL_1,
      "SearchEngine::generate_adjacency_table()::Start");

  int no_threads =
      std::min(thread_pool.getThreadCount(), uint32_t(MULTI_THREAD_MAX));

  llvm::iplist<Operation> &operations = block->getOperations();
  std::vector<op_pair> operation_data;
  for (auto &op : operations) {
    op_pair s;
    s.op = &op;
    operation_data.push_back(s);
  }

#if MULTI_THREAD
  std::vector<std::thread> threads;
  std::vector<int32_t> thread_start;
  std::vector<int32_t> thread_end;
  workload_distribution(operation_data.size(), thread_start, thread_end,
                        no_threads);
  /*
  for (int i = 0; i < thread_start.size(); i++)
      threads.push_back(std::thread(generate_adjacency_ops, this,
  std::ref(operation_data), thread_start[i], thread_end[i],
  block_config_limit)); for (auto &th : threads) { th.join();
  }
  */
  for (int i = 0; i < thread_start.size(); i++)
    thread_pool.async(generate_adjacency_ops, this, std::ref(operation_data),
                      thread_start[i], thread_end[i], block_config_limit);
  thread_pool.wait();
#else
  generate_adjacency_ops(this, std::ref(operation_data), 0,
                         operation_data.size(), block_config_limit);
#endif

  for (auto &op_data : operation_data) {
    adjacency_table[op_data.op] = op_data.k;
  }

  LOG(ENABLE_LOGGING_SEARCH_ENGINE_VERBOSE_LEVEL_1,
      "SearchEngine::generate_adjacency_table()::End");
}

void SearchEngine::print_adjacency_table(element e) {
  for (Operation *op : e) {
    kernelInfo k = adjacency_table[op];
    std::cout << op << " Producers: " << k.producers.size() << " " << std::endl;
  }
}

} // namespace planner
} // namespace mlir

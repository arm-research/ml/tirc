/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#include "src/transforms/scheduler/search_engine.h"

namespace mlir {
namespace planner {

#define ENABLE_LOGGING 0

using namespace mlir::tosa;

void printKernelTime(Block *block) {
  llvm::iplist<Operation>::iterator i;
  for (i = block->begin(); i != block->end(); i++) {
    if (not(llvm::isa<kernelir::KernelRegionOp>(i)))
      continue;

    unsigned kernel_time =
        i->getAttrOfType<mlir::IntegerAttr>("current_time").getInt();
    std::cout << "Kernel Time: " << kernel_time << std::endl;
  }
}

void SearchEngine::calc_non_local_mem_usage() {
  LOG(ENABLE_LOGGING_SEARCH_ENGINE_VERBOSE_LEVEL_1,
      "SearchEngine::calc_non_local_mem_usage::Start");

  llvm::iplist<Operation>::iterator i;

  std::set<value_mem_type> target_value_mem_type_set_intermettent = {
      mt_Scratch, mt_Scratch_fast};
  LiveRangeGraph lrg;
  LivenessAnalysis_Kernels(lrg, block, value_mem_area::ma_Sram,
                           target_value_mem_type_set_intermettent, context,
                           arch_c);

  unsigned end_pos = 0;
  for (i = block->begin(); i != block->end(); i++) {
    if (not(llvm::isa<kernelir::KernelRegionOp>(i)))
      continue;

    unsigned kernel_time =
        i->getAttrOfType<mlir::IntegerAttr>("current_time").getInt();
    end_pos = std::max(end_pos, kernel_time);
  }
  end_pos += 2;

  /*
     Find which ranges overlap passes but aren't input/outputs of the particulat
     pass. these won't be counted by the dynamic programming search and must be
     counted in manually
   */

  for (unsigned count = 0; count < end_pos; count++)
    mem_usage.push_back(0);

  /* Storage used by Kernels that spatially overlap, think branches */
  for (unsigned count = 0; count < end_pos; count++)
    non_local_mem_usage.push_back(0);

  for (unsigned count = 0; count < end_pos; count++)
    non_local_mem_usage_values.push_back({});

  /*
     Non Local Memory is calculated at the Kernel Level. When building cascades
     we may over-allocate due to this and so we keep a track what the tensors
     constitute the other tensor in case it is clear and we can avoid
     over-allocation of the MemArea.
   */

  std::vector<LiveRange *> ranges_sorted = lrg.get_sorted_ranges();
  for (auto lr : ranges_sorted) {
    bool all_correct_mem_area = true;

    for (auto v : lr->values)
      all_correct_mem_area &=
          (v.getType().dyn_cast<SchedTensorType>().get_mem_area() ==
           search_mem_area);
    ASSERT_COND(
        not(all_correct_mem_area),
        "SearchEngine::calc_non_local_mem_usage():Tensors in Live Range are "
        "not all SRAM when calculating non_local sram requirments");

#if ENABLE_LOGGING
    std::cout << "Start time: " << lr->start_time
              << " End time: " << lr->end_time
              << " Storage Size: " << lr->storage_size << std::endl;
    Operation *op = lr->values[0].getDefiningOp();
    if (op)
      std::cout << getOpName(*op) << std::endl;
#endif

    unsigned storage_size = lr->storage_size;
    // If a Value contribute at this time add its contribution
    for (unsigned j = lr->start_time; j < lr->end_time; j++)
      mem_usage[j] += storage_size;

    // If a Value contibutes at this time record it
    for (auto v : lr->values) {
      auto uv = v.getType().dyn_cast<SchedTensorType>().getUniqueId();
      for (unsigned j = lr->start_time; j < lr->end_time; j++)
        non_local_mem_usage_values[j].insert(uv);
    }
  }

#if ENABLE_LOGGING
  std::cout << std::endl;
  std::cout << "Mem Usage: " << std::endl;
  for (auto v : mem_usage)
    std::cout << v << ",";
  std::cout << std::endl;
  std::cout << std::endl;
#endif

  for (i = block->begin(); i != block->end(); i++) {
    Operation *kernel_op = &(*i);

    if (not(llvm::isa<kernelir::KernelRegionOp>(kernel_op)))
      continue;

    Operation *major_op = kernelir::getKernelMajorOp(kernel_op);
    if (llvm::isa<scheduleir::MemoryOp>(major_op))
      continue;

    /* Each Kernel gets a unique kernel time to operate within */
    unsigned kernel_time =
        i->getAttrOfType<mlir::IntegerAttr>("current_time").getInt();

    unsigned local_mem_usage = 0;
    SmallVector<Value, 1> kernel_values =
        mlir::kernelir::ExtractAllKernelValues(&(*i));
    for (auto v : kernel_values) {
      SchedTensorType tensor_type = v.getType().dyn_cast<SchedTensorType>();
      if (tensor_type.get_mem_area() != search_mem_area)
        continue;
      local_mem_usage += array_prod(tensor_type.getStorageShape());

      /* Only Record the values that are non local */
      non_local_mem_usage_values[kernel_time].erase(tensor_type.getUniqueId());
    }
    non_local_mem_usage[kernel_time] =
        (mem_usage[kernel_time] - local_mem_usage);

#if ENABLE_LOGGING
    std::cout << "Kernel no of values: " << kernel_values.size() << std::endl;
    std::cout << getOpName(*major_op) << std::endl;
    std::cout << kernel_time << ":" << non_local_mem_usage[kernel_time]
              << " -> " << mem_usage[kernel_time] << ":" << local_mem_usage
              << std::endl;
    std::cout << std::endl;
#endif

    ASSERT_COND(
        (non_local_mem_usage[kernel_time] < 0),
        "SearchEngine::calc_non_local_mem_usage():non_local_mem_usage is < 0");
  }

#if ENABLE_LOGGING
  std::cout << std::endl;
  std::cout << "Non Local Mem Usage: " << std::endl;
  for (auto v : non_local_mem_usage)
    std::cout << v << ",";
  std::cout << std::endl;
  std::cout << std::endl;
#endif

  LOG(ENABLE_LOGGING_SEARCH_ENGINE_VERBOSE_LEVEL_1,
      "SearchEngine::calc_non_local_mem_usage::End");
}

} // namespace planner
} // namespace mlir

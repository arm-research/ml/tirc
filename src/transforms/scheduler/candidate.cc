/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#include "mlir/Dialect/Quant/QuantOps.h"
#include "mlir/Dialect/Quant/QuantTypes.h"
#include "mlir/Dialect/StandardOps/IR/Ops.h"      // from @llvm-project
#include "mlir/IR/AffineMap.h"                    // from @llvm-project
#include "mlir/IR/Attributes.h"                   // from @llvm-project
#include "mlir/IR/BlockAndValueMapping.h"         // from @llvm-project
#include "mlir/IR/Builders.h"                     // from @llvm-project
#include "mlir/IR/Location.h"                     // from @llvm-project
#include "mlir/IR/MLIRContext.h"                  // from @llvm-project
#include "mlir/IR/Operation.h"                    // from @llvm-project
#include "mlir/IR/PatternMatch.h"                 // from @llvm-project
#include "mlir/Interfaces/InferTypeOpInterface.h" // from @llvm-project
#include "mlir/Pass/Pass.h"                       // from @llvm-project
#include "mlir/Transforms/DialectConversion.h"    // from @llvm-project

#include <iostream>
#include <set>
#include <vector>

#include "src/transforms/scheduler/candidate.h"
#include "src/transforms/scheduler/search_engine.h"

namespace mlir {
namespace planner {

PLANS_COLLECTION candidate_to_plans(
    candidate c, std::unordered_map<element_hash, Plan> &powerset_dictionary) {
  // std::cout << "candidate_to_plans::Start" << std::endl;
  PLANS_COLLECTION plans;
  for (auto p : c.p) {
    element_hash hash = get_element_hash(p);
    if (powerset_dictionary.find(hash) != powerset_dictionary.end())
      plans.push_back(powerset_dictionary[hash]);
    else
      FATAL_ERROR(
          "FATAL Error: candidate_to_plans -> element not found in dictionary");
  }
  // std::cout << "candidate_to_plans::End" << std::endl;
  return plans;
}

candidates generate_candidates(partition part) {
  candidates ca;
  uint32_t no_partitions = (1 << part.size() - 1) - 1;
  uint32_t no_elements = part.size();
  for (uint32_t partition_index = 0; partition_index < no_partitions;
       partition_index++) {
    candidate c;
    std::vector<Operation *> p;
    c.p.push_back(p);
    for (uint32_t i = 0; i < no_elements; i++) {
      c.p[c.p.size() - 1].push_back(part[i]);
      if (partition_index & (1 << i)) {
        std::vector<Operation *> p;
        c.p.push_back(p);
      }
    }
    ca.push_back(c);
  }
  candidate c;
  for (uint32_t i = 0; i < no_elements; i++) {
    std::vector<Operation *> p;
    c.p.push_back(p);
    c.p[c.p.size() - 1].push_back(part[i]);
  }
  ca.push_back(c);
  return ca;
}

void printCandidates(candidates ca) {
  std::cout << std::endl;
  std::cout << YELLOW << "Candidates:" << RESET << std::endl;
  for (auto c : ca) {
    std::cout << "{";
    for (auto e : c.p) {
      std::cout << "[";
      for (auto o = e.begin(); o != e.end(); o++) {
        std::cout << *o;
        if (o + 1 != e.end())
          std::cout << " ";
      }
      std::cout << "] ";
    }
    std::cout << "}" << std::endl;
  }
  std::cout << std::endl;
}

void printCandidate(candidate c, std::string label) {
  std::cout << std::endl;
  std::cout << YELLOW << label << RESET << std::endl;
  for (auto p : c.p) {
    std::cout << "[";
    for (auto e : p) {
      std::cout << e << ",";
    }
    std::cout << "]";
  }
  std::cout << std::endl;
  std::cout << std::endl;
}

bool swap(candidate &p1, candidate &p2) {
  cost_value p1_cost_value = p1.metrics.get_cost_value();
  cost_value p2_cost_value = p2.metrics.get_cost_value();
  if (p1_cost_value.total_cost < p2_cost_value.total_cost)
    return true;
  else
    return false;
}

} // namespace planner
} // namespace mlir

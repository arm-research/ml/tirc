/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#ifndef CANDIDATE_H
#define CANDIDATE_H

#include "mlir/Dialect/Quant/QuantOps.h"
#include "mlir/Dialect/Quant/QuantTypes.h"
#include "mlir/Dialect/StandardOps/IR/Ops.h"      // from @llvm-project
#include "mlir/IR/AffineMap.h"                    // from @llvm-project
#include "mlir/IR/Attributes.h"                   // from @llvm-project
#include "mlir/IR/BlockAndValueMapping.h"         // from @llvm-project
#include "mlir/IR/Builders.h"                     // from @llvm-project
#include "mlir/IR/Location.h"                     // from @llvm-project
#include "mlir/IR/MLIRContext.h"                  // from @llvm-project
#include "mlir/IR/Operation.h"                    // from @llvm-project
#include "mlir/IR/PatternMatch.h"                 // from @llvm-project
#include "mlir/Interfaces/InferTypeOpInterface.h" // from @llvm-project
#include "mlir/Pass/Pass.h"                       // from @llvm-project
#include "mlir/Transforms/DialectConversion.h"    // from @llvm-project

#include <iostream>
#include <set>
#include <vector>

#include "src/transforms/scheduler/cost_metrics.h"
#include "src/transforms/scheduler/element.h"
#include "src/transforms/scheduler/partitions.h"
#include "src/transforms/scheduler/plan.h"
#include "src/transforms/scheduler/power_set.h"

namespace mlir {
namespace planner {

class candidate {
public:
  cost_metrics metrics;
  std::vector<std::vector<Operation *>> p;
};
using candidates = std::vector<candidate>;

candidates generate_candidates(partition part);

candidates generate_cost_metrics_for_each_candidate_and_filter(
    candidates ca, std::unordered_map<element_hash, Plan> &powerset_dictionary,
    bool add_output = false);
void sort_candidates(candidates &ca);
PLANS_COLLECTION
candidate_to_plans(candidate c,
                   std::unordered_map<element_hash, Plan> &powerset_dictionary);

void printCandidates(candidates ca);
void printCandidate(candidate c, std::string label);

} // namespace planner
} // namespace mlir

#endif // CANDIDATE_H

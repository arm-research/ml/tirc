/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#include "mlir/Dialect/Quant/QuantOps.h"
#include "mlir/Dialect/Quant/QuantTypes.h"
#include "mlir/Dialect/StandardOps/IR/Ops.h"      // from @llvm-project
#include "mlir/IR/AffineMap.h"                    // from @llvm-project
#include "mlir/IR/Attributes.h"                   // from @llvm-project
#include "mlir/IR/BlockAndValueMapping.h"         // from @llvm-project
#include "mlir/IR/Builders.h"                     // from @llvm-project
#include "mlir/IR/Location.h"                     // from @llvm-project
#include "mlir/IR/MLIRContext.h"                  // from @llvm-project
#include "mlir/IR/Operation.h"                    // from @llvm-project
#include "mlir/IR/PatternMatch.h"                 // from @llvm-project
#include "mlir/Interfaces/InferTypeOpInterface.h" // from @llvm-project
#include "mlir/Pass/Pass.h"                       // from @llvm-project
#include "mlir/Transforms/DialectConversion.h"    // from @llvm-project

#include <iostream>
#include <set>
#include <vector>

#include "src/transforms/scheduler/candidate.h"

namespace mlir {
namespace planner {

candidates generate_cost_metrics_for_each_candidate_and_filter(
    candidates ca, std::unordered_map<element_hash, Plan> &powerset_dictionary,
    bool add_output) {
  // std::cout << "generate_cost_metrics_for_each_candidate_and_filter::Start"
  // << std::endl;
  candidates ca_filtered;
  /* Loop through each candidate */
  for (auto c : ca) {
    cost_metrics cm;
    bool allocated = false;
    /* Loop through each element of the candidate */
    for (auto p : c.p) {
      /* Element Not in Dictionary Handling */
      element_hash hash = get_element_hash(p);
      if (powerset_dictionary.find(hash) != powerset_dictionary.end()) {
        Plan optimal_plan = powerset_dictionary[hash];
        cm = (cm + optimal_plan.metrics);
        allocated = true;
      } else {
        allocated = false;
        break;
      }
    }
    if (allocated) {
      if (add_output)
        cm.memory_footprint[ma_Sram] +=
            get_full_buffer_output(c.p.back(), ma_Sram);
      c.metrics = cm;
      ca_filtered.push_back(c);
    }
  }
  // std::cout << "generate_cost_metrics_for_each_candidate_and_filter::End" <<
  // std::endl;
  return ca_filtered;
}

} // namespace planner
} // namespace mlir

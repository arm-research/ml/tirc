/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#include "src/transforms/scheduler/cost_metrics.h"

namespace mlir {
namespace planner {

double max(double *data, unsigned no_elements) {
  double max = 0;
  for (int i = 0; i < no_elements; i++) {
    if (data[i] > max)
      max = data[i];
  }
  return max;
}

template <std::size_t M, std::size_t N>
double sum(double data[M][N], unsigned x, unsigned y) {
  double sum = 0;
  for (int i = 0; i < x; i++) {
    for (int j = 0; j < y; j++) {
      sum += data[i][j];
    }
  }
  return sum;
}

void reset_bandwidth_array(BANDWIDTH_ARRAY &bw) {
  for (unsigned i = 0; i < ma_size; i++)
    for (unsigned j = 0; j < vp_size; j++)
      for (unsigned k = 0; k < bd_size; k++)
        bw[i][j][k] = 0;
}

void reset_mac_array(MAC_ARRAY &mac) {
  for (unsigned i = 0; i < mc_size; i++)
    mac[i] = 0;
}

void reset_cyles(CYCLES_ARRAY &cycles) {
  for (unsigned i = 0; i < c_size; i++)
    cycles[i] = 0;
}

void reset_memory_footprint(MEMORY_FOOTPRINT_ARRAY &memory_footprint) {
  for (unsigned i = 0; i < ma_size; i++)
    memory_footprint[i] = 0;
}

cost_metrics::cost_metrics() {
  reset_bandwidth_array(bw);
  reset_mac_array(mac);
  reset_cyles(cycles);
  reset_memory_footprint(memory_footprint);

  ifm_read_multiple = 1;
  weight_read_multiple = 0;
}

cost_metrics cost_metrics::operator+(cost_metrics &rhs) {
  cost_metrics p;
  for (unsigned i = 0; i < ma_size; i++)
    for (unsigned j = 0; j < vp_size; j++)
      for (unsigned k = 0; k < bd_size; k++)
        p.bw[i][j][k] = (this->bw[i][j][k] + rhs.bw[i][j][k]);
  for (unsigned i = 0; i < mc_size; i++)
    p.mac[i] = this->mac[i] + rhs.mac[i];
  for (unsigned i = 0; i < c_size; i++)
    p.cycles[i] = this->cycles[i] + rhs.cycles[i];
  for (unsigned i = 0; i < ma_size; i++)
    p.memory_footprint[i] = this->memory_footprint[i] + rhs.memory_footprint[i];
  return p;
}

cost_metrics cost_metrics::operator=(const cost_metrics &rhs) {
  memcpy(this->bw, rhs.bw, ma_size * vp_size * bd_size * sizeof(double));
  memcpy(this->mac, rhs.mac, mc_size * sizeof(double));
  memcpy(this->cycles, rhs.cycles, c_size * sizeof(double));
  memcpy(this->memory_footprint, rhs.memory_footprint,
         ma_size * sizeof(double));
  return *this;
}

cost_metrics::cost_metrics(const cost_metrics &rhs) {
  memcpy(this->bw, rhs.bw, ma_size * vp_size * bd_size * sizeof(double));
  memcpy(this->mac, rhs.mac, mc_size * sizeof(double));
  memcpy(this->cycles, rhs.cycles, c_size * sizeof(double));
  memcpy(this->memory_footprint, rhs.memory_footprint,
         ma_size * sizeof(double));
}

cost_metrics::~cost_metrics() {}

void cost_metrics::update_bw_cycles(
    mlir::ArchitectureConfig::ArchitectureConfigAttr &arch_c,
    BANDWIDTH_ARRAY &bw_scaled) {
  double memory_bandwidths_per_cycle_sram =
      arch_c.memory_bandwidths_per_cycle_Sram().getValueAsDouble();
  cycles[c_SramAccess] =
      (sum<vp_size, bd_size>(bw_scaled[ma_Sram], vp_size, bd_size) /
       memory_bandwidths_per_cycle_sram);

  double memory_bandwidths_per_cycle_dram =
      arch_c.memory_bandwidths_per_cycle_Dram().getValueAsDouble();
  cycles[c_DramAccess] =
      (sum<vp_size, bd_size>(bw_scaled[ma_Dram], vp_size, bd_size) /
       memory_bandwidths_per_cycle_dram);

  double memory_bandwidths_per_cycle_OnChipFlash =
      arch_c.memory_bandwidths_per_cycle_OnChipFlash().getValueAsDouble();
  cycles[c_OnChipFlashAccess] =
      (sum<vp_size, bd_size>(bw_scaled[ma_OnChipFlash], vp_size, bd_size) /
       memory_bandwidths_per_cycle_OnChipFlash);

  double memory_bandwidths_per_cycle_OffChipFlash =
      arch_c.memory_bandwidths_per_cycle_OffChipFlash().getValueAsDouble();
  cycles[c_OffChipFlashAccess] =
      (sum<vp_size, bd_size>(bw_scaled[ma_OffChipFlash], vp_size, bd_size) /
       memory_bandwidths_per_cycle_OffChipFlash);

  cycles[c_Max_Component] = max(cycles, c_Max_Component);
}

void cost_metrics::print(
    ostream &stream, std::string label, std::string indentation,
    mlir::ArchitectureConfig::ArchitectureConfigAttr &arch_c) {
  stream << label.c_str() << std::endl;
  double midpoint_inference_time =
      (cycles[c_Max_Component] / arch_c.npu_clock().getInt());
  double midpoint_fps = 0;
  if (midpoint_inference_time >= 0)
    midpoint_fps = 1 / midpoint_inference_time;
  else
    ASSERT_COND(true, "cost_metrics::print::(midpoint_inference_time > 0)");
  // 0. Bandwidth (Input, Output, Weights, Total)
  stream << indentation << BLUE << "Bandwidth Metrics:" << RESET << std::endl;
  for (value_mem_area ma : {ma_Sram, ma_OffChipFlash, ma_Shram}) {
    float input_bw = bw[ma][vp_FeatureMap][bd_Read];
    float weight_bw = bw[ma][vp_Weights][bd_Read];
    float output_bw = bw[ma][vp_FeatureMap][bd_Write];
    float total_bw = sum<vp_size, bd_size>(bw[ma], vp_size, bd_size);
    float average_bandwidth =
        total_bw * midpoint_fps / 1000.0 / 1000.0 / 1000.0;
    stream << indentation << indentation
           << "Memory Area: " << mem_area_to_str(ma)
           << " Input: " << (input_bw / 1000.0 / 1000.0) << " MB/batch"
           << std::endl;
    stream << indentation << indentation
           << "Memory Area: " << mem_area_to_str(ma)
           << " Weight: " << (weight_bw / 1000.0 / 1000.0) << " MB/batch"
           << std::endl;
    stream << indentation << indentation
           << "Memory Area: " << mem_area_to_str(ma)
           << " Output: " << (output_bw / 1000.0 / 1000.0) << " MB/batch"
           << std::endl;
    stream << indentation << indentation
           << "Memory Area: " << mem_area_to_str(ma)
           << " Total: " << (total_bw / 1000.0 / 1000.0) << " MB/batch"
           << std::endl;
    stream << indentation << indentation
           << "Memory Area: " << mem_area_to_str(ma)
           << " Average Bandwidth: " << average_bandwidth << " GB/s"
           << std::endl;
  }
  // 1. Mem Used in each mem MemArea
  stream << indentation << BLUE << "Memory Footprints Metrics:" << RESET
         << std::endl;
  stream << indentation << indentation
         << "Unknown: " << memory_footprint[ma_Unknown] << " KiB" << std::endl;
  stream << indentation << indentation << "Sram: " << memory_footprint[ma_Sram]
         << " KiB" << std::endl;
  stream << indentation << indentation << "Dram: " << memory_footprint[ma_Dram]
         << " KiB" << std::endl;
  stream << indentation << indentation
         << "OnChipFlash: " << memory_footprint[ma_OnChipFlash] << " KiB"
         << std::endl;
  stream << indentation << indentation
         << "OffChipFlash: " << memory_footprint[ma_OffChipFlash] << " KiB"
         << std::endl;
  stream << indentation << indentation
         << "Shram: " << memory_footprint[ma_Shram] << " KiB" << std::endl;
  // 2. Cycles, Neural Macs, Hardware Macs, (MACs/batch, Tops/s), Inference
  stream << indentation << BLUE << "Compute Metrics & Cyles:" << RESET
         << std::endl;
  stream << indentation << indentation
         << "Compute Cycles Npu (DPU+OutputUnit): " << cycles[c_Npu]
         << std::endl;
  stream << indentation << indentation
         << "Compute Cycles Npu (OutputUnit): " << cycles[c_Npu_ElementWise]
         << std::endl;
  stream << indentation << indentation
         << "Compute Cycles Cpu: " << cycles[c_Cpu] << std::endl;
  stream << indentation << indentation
         << "R/W Cycles SramAccess: " << cycles[c_SramAccess] << std::endl;
  stream << indentation << indentation
         << "R/W Cycles DramAccess: " << cycles[c_DramAccess] << std::endl;
  stream << indentation << indentation
         << "R/W Cycles OnChipFlashAccess: " << cycles[c_OnChipFlashAccess]
         << std::endl;
  stream << indentation << indentation
         << "R/W Cycles OffChipFlashAccess: " << cycles[c_OffChipFlashAccess]
         << std::endl;
  stream << indentation << indentation << RED
         << "Cycles Max Component: " << RESET << cycles[c_Max_Component]
         << std::endl;
  stream << indentation << indentation
         << "Neural network macs: " << mac[mc_NeuralNetworkMacs] << std::endl;
  stream << indentation << indentation << "Network Tops/s: "
         << mac[mc_NeuralNetworkMacs] * 2 * midpoint_fps / 1e12 << std::endl;
  stream << indentation << indentation
         << "Hardware macs (DPU Only): " << mac[mc_HardwareMacs] << std::endl;
  stream << indentation << indentation << "Hardware Tops/s: "
         << mac[mc_HardwareMacs] * 2 * midpoint_fps / 1e12 << std::endl;
  // 3. Final Netwrk Performance
  stream << indentation << YELLOW << "Inference Time (ms): " << RESET
         << midpoint_inference_time * 1000 << std::endl;
  stream << indentation << YELLOW << "Inferences/s: " << RESET << midpoint_fps
         << std::endl;
}

double weight_bw(BANDWIDTH_ARRAY &bw) {
  double sum_total = 0.0;
  for (unsigned j = 0; j < vp_size; j++) {
    for (unsigned k = 0; k < bd_size; k++) {
      sum_total += (bw[ma_Sram][j][k] * BANDWIDTH_SRAM_WEIGHT);
      sum_total += (bw[ma_Dram][j][k] * BANDWIDTH_DRAM_WEIGHT);
      sum_total += (bw[ma_OnChipFlash][j][k] * BANDWIDTH_OnChipFlash_WEIGHT);
      sum_total += (bw[ma_OffChipFlash][j][k] * BANDWIDTH_OffChipFlash_WEIGHT);
    }
  }
}

cost_value cost_metrics::get_cost_value() {
  calculate_cost_value();
  return cost;
}

void cost_metrics::calculate_cost_value() {
  cost.weighted_bandwidth_cycles = weight_bw(bw);
  cost.weighted_compute_cycles = (cycles[c_Max_Component] * CYCLES_WEIGHT);
  cost.weighted_sram_used = (memory_footprint[ma_Sram] * MAX_SRAM_USED_WEIGHT);
  cost.total_cost = (cost.weighted_bandwidth_cycles +
                     cost.weighted_compute_cycles + cost.weighted_sram_used);
}

} // namespace planner
} // namespace mlir

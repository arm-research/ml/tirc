/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#ifndef COST_METRICS_H
#define COST_METRICS_H

#include "mlir/Dialect/Quant/QuantOps.h"
#include "mlir/Dialect/Quant/QuantTypes.h"
#include "mlir/Dialect/StandardOps/IR/Ops.h"      // from @llvm-project
#include "mlir/IR/AffineMap.h"                    // from @llvm-project
#include "mlir/IR/Attributes.h"                   // from @llvm-project
#include "mlir/IR/BlockAndValueMapping.h"         // from @llvm-project
#include "mlir/IR/Builders.h"                     // from @llvm-project
#include "mlir/IR/Location.h"                     // from @llvm-project
#include "mlir/IR/MLIRContext.h"                  // from @llvm-project
#include "mlir/IR/Operation.h"                    // from @llvm-project
#include "mlir/IR/PatternMatch.h"                 // from @llvm-project
#include "mlir/Interfaces/InferTypeOpInterface.h" // from @llvm-project
#include "mlir/Pass/Pass.h"                       // from @llvm-project
#include "mlir/Transforms/DialectConversion.h"    // from @llvm-project

#include "src/ir/architecture_config/architecture_config.h"
#include "src/utils/colors.h"

#include "src/utils/dag.h"

#include "src/ir/cascade_ir.h"
#include "src/ir/kernel_ir.h"

namespace mlir {
namespace planner {

#define ENABLE_PERFORMANCE_MODEL_PARTIAL_PLANS 0
// Used in performance model: If set to 1 the cost funtion will try and choose
// the biggest block on Elementwise Ops
#define ENABLE_BLK_SIZE_MINIMIZATION 0

#define MAX_SRAM_USED_WEIGHT 1000
#define CYCLES_WEIGHT 40
#define BANDWIDTH_SRAM_WEIGHT 1.0
#define BANDWIDTH_DRAM_WEIGHT 10.0
#define BANDWIDTH_OnChipFlash_WEIGHT 2.0
#define BANDWIDTH_OffChipFlash_WEIGHT 20.0

using BANDWIDTH_ARRAY = double[ma_size][vp_size][bd_size];
using MAC_ARRAY = double[mc_size];
using CYCLES_ARRAY = double[c_size];
using MEMORY_FOOTPRINT_ARRAY = double[ma_size];

struct cost_value {
  double weighted_bandwidth_cycles;
  double weighted_compute_cycles;
  double weighted_sram_used;
  double total_cost;
  cost_value() {
    weighted_bandwidth_cycles = 1.0;
    weighted_compute_cycles = 1.0;
    weighted_sram_used = 1.0;
    total_cost = 1.0;
  }
};

void reset_bandwidth_array(BANDWIDTH_ARRAY &bw);
void reset_mac_array(MAC_ARRAY &mac);
void reset_cyles(CYCLES_ARRAY &cycles);
void reset_memeory_footprint(MEMORY_FOOTPRINT_ARRAY &memory_footprint);

double weight_bw(BANDWIDTH_ARRAY &bw);

class cost_metrics {
public:
  cost_metrics();
  cost_metrics(const cost_metrics &rhs);
  cost_metrics operator=(const cost_metrics &rhs);
  ~cost_metrics();

  void print(ostream &stream, std::string label, std::string indentation,
             mlir::ArchitectureConfig::ArchitectureConfigAttr &arch_c);

  void
  update_bw_cycles(mlir::ArchitectureConfig::ArchitectureConfigAttr &arch_c,
                   BANDWIDTH_ARRAY &bw_scaled);

  cost_metrics operator+(cost_metrics &rhs);

  BANDWIDTH_ARRAY bw;
  MAC_ARRAY mac;
  CYCLES_ARRAY cycles;
  MEMORY_FOOTPRINT_ARRAY memory_footprint;

  uint32_t ifm_read_multiple;
  uint32_t weight_read_multiple;

  cost_value get_cost_value();

private:
  cost_value cost;
  void calculate_cost_value();
};

} // namespace planner
} // namespace mlir

#endif // COST_METRICS_H

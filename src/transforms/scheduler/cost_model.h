/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

/*
    This is an abstract interface to a hardware cost_MODEL model allowing
  multiple architectures to be integrated

    NPU cost estimation functions to estimate cost of a Pass and CascadedPass.
  Uses a model that takes the maximum of the 'cycles required for bandwidth' and
  'cycles required for computing'.

    Called during scheduling to evaluate different proposals, as well as
  post-scheduling to provide a final cost estimate.
*/

#ifndef COST_MODEL_H
#define COST_MODEL_H

#include "mlir/Dialect/Quant/QuantOps.h"
#include "mlir/Dialect/Quant/QuantTypes.h"
#include "mlir/Dialect/StandardOps/IR/Ops.h"      // from @llvm-project
#include "mlir/IR/AffineMap.h"                    // from @llvm-project
#include "mlir/IR/Attributes.h"                   // from @llvm-project
#include "mlir/IR/BlockAndValueMapping.h"         // from @llvm-project
#include "mlir/IR/Builders.h"                     // from @llvm-project
#include "mlir/IR/Location.h"                     // from @llvm-project
#include "mlir/IR/MLIRContext.h"                  // from @llvm-project
#include "mlir/IR/Operation.h"                    // from @llvm-project
#include "mlir/IR/PatternMatch.h"                 // from @llvm-project
#include "mlir/Interfaces/InferTypeOpInterface.h" // from @llvm-project
#include "mlir/Pass/Pass.h"                       // from @llvm-project
#include "mlir/Transforms/DialectConversion.h"    // from @llvm-project

#include "src/ir/architecture_config/architecture_config.h"

#include "src/utils/dag.h"

#include "src/ir/cascade_ir.h"
#include "src/ir/kernel_ir.h"

#include "src/transforms/kernel_packing/kernelInfo.h"
#include "src/transforms/scheduler/cost_metrics.h"
#include "src/transforms/scheduler/value_rewrite.h"

#include "src/utils/colors.h"

#include <vector>

namespace mlir {
namespace planner {

class cost_model {
public:
  virtual cost_metrics cost_metrics_for_kernel(
      Operation *op, mlir::ArchitectureConfig::ArchitectureConfigAttr &arch_c,
      std::unordered_map<Operation *, kernelInfo> &adjacency_table,
      NpuBlockConfig block_config = {},
      std::vector<value_rewrite> rewrites = {}) = 0;
  virtual cost_metrics cost_metrics_for_cascade(
      Operation *op,
      mlir::ArchitectureConfig::ArchitectureConfigAttr &arch_c) = 0;

  virtual cost_metrics cost_metrics_for_npu_region(
      Operation *op,
      mlir::ArchitectureConfig::ArchitectureConfigAttr &arch_c) = 0;
  virtual cost_metrics cost_metrics_for_cpu_operation(
      Operation *op,
      mlir::ArchitectureConfig::ArchitectureConfigAttr &arch_c) = 0;

  virtual void print_module_stats(
      ostream &stream, mlir::OwningModuleRef &module,
      mlir::ArchitectureConfig::ArchitectureConfigAttr &arch_c) = 0;
};

} // namespace planner
} // namespace mlir

#include "src/transforms/scheduler/u55_cost_model/cost_model.h"

#endif // COST_MODEL_H

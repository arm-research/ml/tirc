/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#include "src/transforms/scheduler/search_engine.h"

namespace mlir {
namespace planner {

void SearchEngine::culling_plans(int64_t size, cost_model *model) {
  int32_t arch_sram = this->arch_c.sram_size().getInt();
  for (auto &p : partition_planscollectionlookup) {
    // 0. Estimate Performance
    p.second.estimate_plans_performance(model, arch_c, thread_pool,
                                        adjacency_table);

    // print_plans(partition_planscollectionlookup, true);

    // 1. Remove any plans that are over the SRAM
    p.second.remove_invalid(arch_sram);

    // 2. Sort based on metrics
    p.second.sort();

    // 3. Culling
    p.second.culling(size);
  }

  // Delete any patterns with no plans as these will never be needed
  for (auto it = partition_planscollectionlookup.begin();
       it != partition_planscollectionlookup.end();) {
    if (it->second.size() == 0)
      it = partition_planscollectionlookup.erase(it);
    else
      it++;
  }
}

void SearchEngine::culling_partial_plans(int64_t size, cost_model *model) {
  int32_t arch_sram = this->arch_c.sram_size().getInt();
  for (auto &p : partition_partialplanscollectionlookup) {
    // 0. Estimate Performance
    p.second.estimate_partial_plans_performance(model, arch_c, thread_pool,
                                                adjacency_table);

    // 1. Remove any plans that are over the SRAM
    p.second.remove_invalid(arch_sram);

    // 2. Sort based on metrics
    p.second.sort();

    // 3. Culling
    p.second.culling(size);
  }
}

} // namespace planner
} // namespace mlir

/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#include "src/transforms/scheduler/element.h"

namespace mlir {
namespace planner {

element_hash get_element_hash(element ele) {
  stringstream ss;
  for (auto e : ele)
    ss << e;
  return ss.str();
}

ostream &operator<<(ostream &os, const element &ele) {
  os << "[";
  for (auto e : ele)
    os << e << ",";
  os << "]";
}

uint64_t get_full_buffer_input(element e, value_mem_area search_mem_area) {
  uint64_t sram_used = 0;
  for (Operation *op : e) {
    for (auto value : op->getOperands()) {
      Operation *producer = value.getDefiningOp();
      if (std::find(e.begin(), e.end(), producer) == e.end()) {
        // std::cout << "Input Found" << std::endl;
        SchedTensorType t = value.getType().dyn_cast<SchedTensorType>();
        if (t.get_mem_area() == search_mem_area)
          sram_used += array_prod(t.getStorageShape());
      }
    }
  }
  return sram_used;
}

uint64_t get_full_buffer_output(element e, value_mem_area search_mem_area) {
  uint64_t sram_used = 0;
  for (Operation *op : e) {
    for (auto value : op->getResults()) {
      for (Operation *consumer : value.getUsers()) {
        if (std::find(e.begin(), e.end(), consumer) == e.end()) {
          // std::cout << "Output Found" << std::endl;
          SchedTensorType t = value.getType().dyn_cast<SchedTensorType>();
          if (t.get_mem_area() == search_mem_area)
            sram_used += array_prod(t.getStorageShape());

          break; /* Only record SRAM Once so move to next value: This needs to
                    be debugged more when handling residuals */
        }
      }
    }
  }
  return sram_used;
}

} // namespace planner
} // namespace mlir

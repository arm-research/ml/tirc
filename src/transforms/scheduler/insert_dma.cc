/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#include "src/transforms/scheduler/insert_dma.h"

namespace mlir {
namespace planner {

void insertDMA(value_rewrite vr) {
  // Create Type for output (Unique)
  SchedTensorType dma_result_type = SchedTensorType::get(
      vr.new_shape, vr.type.getElementType(), getUniqueId());
  Operation *consuming_op;

  uint64_t operand_index;
  unsigned int count = 0;
  for (Operation *consumer : vr.value.getUsers()) {
    consuming_op = consumer;
    for (int ope = 0; ope < consumer->getNumOperands(); ope++) {
      Value v1 = consumer->getOperand(ope);
      if (v1 == vr.value)
        operand_index = ope;
    }
    count++;
  }
  Block *block = consuming_op->getBlock();
  if (count != 1)
    FATAL_ERROR(
        "insertDMA: Trying to insert a DMA on a value with multiple consumers");

  mlir::OpBuilder builder(consuming_op->getContext());
  mlir::Location loc = builder.getUnknownLoc();

  // Insert a New Op that connects that consumes thevalue that needs to be moved
  // across MemAreas
  llvm::iplist<Operation>::iterator insertOp(consuming_op);
  builder.setInsertionPoint(block, insertOp);

  Operation *op_dma =
      builder.create<scheduleir::DmaOp>(loc, dma_result_type, vr.value);

  op_dma->setAttr("block_type", builder.getI64IntegerAttr(int(Default)));
  op_dma->setAttr("is_unary", builder.getBoolAttr(true));

  // std::cout << smallvector_to_string(vr.new_shape) << std::endl;
  // std::cout << smallvector_to_string(vr.new_storage_shape) << std::endl;
  // std::cout <<
  // op_dma->getResult(0).getType().dyn_cast<mlir::SchedTensorType>().getUniqueId()
  // << std::endl;

  mlir::scheduleir::SetParameters(op_dma->getResult(0), vr.type.get_purpose(),
                                  vr.new_sub_purpose, vr.new_format, ma_Sram,
                                  mt_Scratch);
  op_dma->getResult(0)
      .getType()
      .dyn_cast<mlir::SchedTensorType>()
      .setStorageShape(vr.new_storage_shape);

  SmallVector<Value, 1> replacment_operand;
  replacment_operand.push_back(op_dma->getResult(0));
  consuming_op->setOperands(operand_index, 1, replacment_operand);
}

} // namespace planner
} // namespace mlir

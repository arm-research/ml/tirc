/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#include "src/transforms/scheduler/partitions.h"
#include "src/transforms/scheduler/search_engine.h"

namespace mlir {
namespace planner {

uint64_t SearchEngine::get_stationary_size(SchedTensorType t,
                                           value_mem_area search_mem_area) {
  if (this->search_mem_area == search_mem_area)
    return array_prod(t.getStorageShape());
  else
    return 0;
}

bool SearchEngine::operation_in_partition(partition p, Operation *op) {
  if (std::find(p.begin(), p.end(), op) == p.end())
    return false;
  else
    return true;
}

uint64_t SearchEngine::get_input_partition(partition &pa) {
  uint64_t sram_size = 0;
  for (Operation *op : pa) {
    for (auto p : op->getOperands()) {
      if (llvm::isa<scheduleir::MemoryOp>(kernelir::getKernelMajorOp(op)))
        return 0;

      SchedTensorType t = p.getType().dyn_cast<SchedTensorType>();
      Operation *def_op = p.getDefiningOp();
      // std::cout << "Input Tensor: " << op << " " << t.getShape()[0] << " " <<
      // t.getShape()[1] << " " << t.getShape()[2] << " " << t.getShape()[3] <<
      // std::endl;
      if (def_op == nullptr)
        sram_size += get_stationary_size(t, t.get_mem_area());
      else if (not(operation_in_partition(pa, def_op)))
        sram_size += get_stationary_size(t, t.get_mem_area());
    }
  }
  return sram_size;
}

uint64_t SearchEngine::get_output_partition(partition &pa) {
  uint64_t sram_size = 0;
  for (Operation *op : pa) {
    if (llvm::isa<scheduleir::MemoryOp>(kernelir::getKernelMajorOp(op)))
      return 0;
    kernelInfo k = adjacency_table[op];
    for (auto c : k.consumers) {
      SchedTensorType t = c.value.getType().dyn_cast<SchedTensorType>();
      // std::cout << "Output Tensor: " << op << " " << t.getShape()[0] << " "
      // << t.getShape()[1] << " " << t.getShape()[2] << " " << t.getShape()[3]
      // << std::endl;
      if (not(operation_in_partition(pa, c.external_operation))) {
        sram_size += get_stationary_size(t, t.get_mem_area());
        ASSERT_COND(
            c.operation->getNumResults() > 1,
            "get_output_partition::Detected a Node with more than one output");
        break;
      }
    }
  }
  return sram_size;
}

void SearchEngine::check_partition_endings(
    std::vector<partition> p_network,
    mlir::ArchitectureConfig::ArchitectureConfigAttr &arch_c) {
  /* ToDo: This should be using the pinning tensor allocation done just before
   * this */

  int32_t size_limit = arch_c.sram_size().getInt();

  for (partition &p : p_network) {
    // 0. Check SRAM Input of Partitions fit in SRAM as full tensors
    uint64_t input_sram = get_input_partition(p);
    // 1. Check SRAM Output of Partitions fit in SRAM as full tensors
    uint64_t output_sram = get_output_partition(p);

    /* std::cout << "input sram: " << input_sram << " output sram: " <<
     * output_sram << std::endl; */

    /* std::cout << "Input SRAM: " << input_sram << " Output SRAM: " <<
     * output_sram << " Non Local Memory: " << non_local_memory << std::endl; */
    if ((input_sram + output_sram) > size_limit)
      FATAL_ERROR("SearchEngine::check_partition_endings::Failed fitting the "
                  "IO for this paritition");
  }
}

void printPartitions(std::vector<partition> &pa) {
  std::cout << std::endl;
  std::cout << YELLOW << "Partitions:" << RESET << std::endl;
  for (auto p : pa) {
    std::cout << "{";
    for (auto e : p)
      std::cout << e << ",";
    std::cout << "}" << std::endl;
  }
  std::cout << std::endl;
}

void printPartition(partition &pa) {
  std::cout << YELLOW << "Partition: " << RESET << "{";
  for (auto e : pa)
    std::cout << e << ",";
  std::cout << "}" << std::endl;
}

/* https://www.boost.org/doc/libs/1_55_0/libs/graph/doc/topological_sort.html */
/* https://www.boost.org/doc/libs/1_55_0/libs/graph/doc/depth_first_search.html
 */

partitions create_partitions(Block *block, bool disable_cascade) {
  dag_graph g;
  g.LoadBlock2DAGRepresentation(*block);

  std::vector<OP_INDEX> order_ops;
  g.TopologicalSort(order_ops);

  partitions pa;
  if (not(disable_cascade)) {
    partition p;
    for (auto op_index : order_ops) {
      Operation *i = g.get_index_2_operation(op_index);
      std::vector<OP_INDEX> consumers = g.GetConsumers(op_index);
      std::vector<OP_INDEX> producers = g.GetProducers(op_index);
      if (llvm::isa<kernelir::KernelRegionOp>(i)) {
        bool consumers_multi_input = false;
        for (auto c : consumers)
          consumers_multi_input |= (g.GetProducers(c).size() > 1);

        if (producers.size() > 1) {
          /* Place it in a single Partition */
          pa.push_back(p);
          p.clear();
          p.push_back(i);
          pa.push_back(p);
          p.clear();
        } else if (consumers.size() > 1) {
          /* End it here but include it in the current partition */
          p.push_back(i);
          pa.push_back(p);
          p.clear();
        } else if (consumers_multi_input) {
          p.push_back(i);
          pa.push_back(p);
          p.clear();
        } else
          p.push_back(i);
      }
    }
    pa.push_back(p);
  } else {
    partition p;
    for (auto op_index : order_ops) {
      Operation *i = g.get_index_2_operation(op_index);
      if (llvm::isa<kernelir::KernelRegionOp>(i)) {
        p.push_back(i);
        pa.push_back(p);
        p.clear();
      }
    }
  }

  partitions pa_final;
  for (auto p : pa) {
    if (p.size() > 0)
      pa_final.push_back(p);
  }
  return pa_final;
}

void print_bit_candidate(std::vector<BitVector> &candidate) {
  std::cout << MAGENTA << "BitVector (vector): [";
  for (auto bc : candidate)
    std::cout << bc << ",";
  std::cout << "]" << RESET << std::endl;
}

class partition_search {
public:
  partition_search(uint64_t size) {
    success = false;
    partition_plans_starting_at_index.clear();
    partition_plans_starting_at_index.resize(size);
  }

  void compute_partition_plans_starting_point(
      PlansCollectionLookup &planscollectionlookup) {
    // using PlansCollectionLookup = std::unordered_map<BitVector,
    // PLANS_COLLECTION>;
    for (auto pcl : planscollectionlookup) {
      unsigned lowest_idx = pcl.first.bsf();
      partition_plans_starting_at_index[lowest_idx].insert(pcl.first);
    }
  }

  void search(BitVector &mask, PlansCollectionLookup &planscollectionlookup) {
    recursive_search(mask, planscollectionlookup);

    for (auto c : best_decomposition[mask])
      candidate.push_back(c);

    BitVector check = BitVector::zeros(mask.get_n_bits());
    for (auto c : candidate) {
      // All decompositions need to be the same width
      if (c.get_n_bits() != mask.get_n_bits()) {
        success = false;
        return;
      }

      for (int i = 0; i < mask.get_n_bits(); i++) {
        // If this decomposition sets a node that has already been set by
        // another decomposition
        if ((c[i] == true) and (check[i] == true)) {
          success = false;
          return;
        }

        // Register nodes set by this decomposition
        if (c[i] == true)
          check.set(i, 1);
      }
    }

    success = false;
    // Finally we should have been able to rebuild the decomposition
    if (check == mask)
      success = true;
  }

  list<BitVector>
  recursive_search(BitVector &mask,
                   PlansCollectionLookup &planscollectionlookup) {
    // Dynamic Programming Menomization
    if (best_decomposition.count(mask))
      return best_decomposition[mask];

    unsigned lowest_idx = mask.bsf();

    list<BitVector> leaf;
    for (const auto &piece_mask :
         partition_plans_starting_at_index[lowest_idx]) {
      leaf.clear();
      if (planscollectionlookup.find(piece_mask) !=
          planscollectionlookup.end()) {
        BitVector remainder_mask = mask & ~piece_mask;
        if (remainder_mask != BitVector::zeros(remainder_mask.get_n_bits())) {
          leaf = recursive_search(remainder_mask, planscollectionlookup);
          if (leaf.size() == 0)
            continue;
          else {
            leaf.push_front(piece_mask);
            break;
          }
        } else
          leaf.push_front(piece_mask);
      }
    }

    list<BitVector> &partition_candidate = best_decomposition[mask];
    partition_candidate = leaf;
    return partition_candidate;
  }

  bool get_Solution(std::vector<BitVector> &candidate) {
    candidate = this->candidate;
    return success;
  }

private:
  absl::node_hash_map<BitVector, list<BitVector>>
      best_decomposition; /* Use for the dynamic Search Menomization */
  std::vector<std::set<BitVector>> partition_plans_starting_at_index;

  std::vector<BitVector> candidate;
  bool success;
};

std::vector<BitVector>
SearchEngine::construct_partition_solution(partition &part) {
  std::vector<BitVector> candidate;

  BitVector part_mask = BitVector::ones(part.size());
  // Quick  if we can cascade the partition: Partition All Cascaded
  /*
      if (partition_planscollectionlookup.find(part_mask) !=
     partition_planscollectionlookup.end())
      {
          if (partition_planscollectionlookup[part_mask].size() > 0)
          {
              final_plans.push_back(partition_planscollectionlookup[part_mask].front());
              candidate.push_back(part_mask);
              return candidate;
          }
      }
  */

  partition_search ps(part.size());
  ps.compute_partition_plans_starting_point(partition_planscollectionlookup);
  ps.search(part_mask, partition_planscollectionlookup);
  if (not(ps.get_Solution(candidate))) {
    printPartition(part);
    FATAL_ERROR("Scheduler could not find a solution for this partition !!!!");
  }

  for (auto c : candidate) {
    if (partition_planscollectionlookup.find(c) ==
        partition_planscollectionlookup.end()) {
      FATAL_ERROR("Scheduler could not find part of the solution for this "
                  "partition !!!!");
    } else
      final_plans.push_back(partition_planscollectionlookup[c].front());
  }

  return candidate;
}

} // namespace planner
} // namespace mlir

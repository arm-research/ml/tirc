/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#include "src/transforms/scheduler/plan.h"
#include "src/transforms/scheduler/cost_metrics.h"
#include "src/transforms/scheduler/search_engine.h"

#include "src/utils/threading.h"

#define ENABLE_IFM_OFM_OVERLAP 0

namespace mlir {
namespace planner {

/********************/
/* PLANS COLLECTION */
/********************/

bool swap_plans(Plan &p1, Plan &p2) {
  /*
     Returns true if the first argument goes before the second argument in the
     strict weak ordering it defines, and false otherwise. This shall be a
     function pointer or a function object.
   */
  cost_value p1_cost_value = p1.metrics.get_cost_value();
  cost_value p2_cost_value = p2.metrics.get_cost_value();
  if (p1_cost_value.total_cost < p2_cost_value.total_cost)
    return true;
  else
    return false;
}

void PLANS_COLLECTION::sort() { _plans.sort(swap_plans); }

void PLANS_COLLECTION::culling(int64_t size) {
  if (size == -1)
    return;
  auto start = _plans.begin();
  std::advance(start, size);
  auto end = _plans.end();
  _plans.erase(start, end);
}

void PLANS_COLLECTION::remove_invalid(int64_t size) {
  // 1. Remove any plans that are over the SRAM
  auto iter = _plans.begin();
  auto end = _plans.end();
  while (iter != end) {
    if (iter->sram_used > size)
      iter = _plans.erase(iter);
    else
      ++iter;
  }
}

void plan_perfomance(
    std::vector<Plan *> &plans_data, int32_t start, int32_t end,
    cost_model *model,
    std::unordered_map<Operation *, kernelInfo> &adjacency_table,
    mlir::ArchitectureConfig::ArchitectureConfigAttr &arch_c) {
  for (auto i = start; i < end; i++) {
    Partial_Plan *plan = plans_data[i];
    for (pKernelInfo pK : plan->kernels) {
      cost_metrics cm = model->cost_metrics_for_kernel(
          pK.kernel, arch_c, adjacency_table, pK.block_config, plan->rewrites);
      plan->metrics = plan->metrics + cm;
    }
    plan->metrics.memory_footprint[ma_Sram] = plan->sram_used;
  }
}
void PLANS_COLLECTION::estimate_plans_performance(
    cost_model *model, mlir::ArchitectureConfig::ArchitectureConfigAttr &arch_c,
    llvm::ThreadPool &thread_pool,
    std::unordered_map<Operation *, kernelInfo> &adjacency_table) {
#if MULTI_THREAD
  int no_threads =
      std::min(thread_pool.getThreadCount(), uint32_t(MULTI_THREAD_MAX));
  std::vector<std::thread> threads;
  std::vector<Plan *> plans_data;
  for (auto &plan : _plans)
    plans_data.push_back(&plan);
  if (plans_data.size() == 0)
    return;
  std::vector<int32_t> thread_start;
  std::vector<int32_t> thread_end;
  workload_distribution(plans_data.size(), thread_start, thread_end,
                        MULTI_THREAD_MAX);
  /*
  for (int i = 0; i < thread_start.size(); i++)
      threads.push_back(std::thread(plan_perfomance, std::ref(plans_data),
  thread_start[i], thread_end[i], model, std::ref(arch_c))); for (auto &th :
  threads) { th.join();
  }
  */
  for (int i = 0; i < thread_start.size(); i++)
    thread_pool.async(plan_perfomance, std::ref(plans_data), thread_start[i],
                      thread_end[i], model, std::ref(adjacency_table),
                      std::ref(arch_c));
  thread_pool.wait();
#else
  for (auto &plan : _plans) {
    for (pKernelInfo pK : plan.kernels) {
      cost_metrics cm = model->cost_metrics_for_kernel(
          pK.kernel, arch_c, adjacency_table, pK.block_config, plan.rewrites);
      plan.metrics = plan.metrics + cm;
    }
    plan.metrics.memory_footprint[ma_Sram] = plan.sram_used;
  }
#endif
}

/****************************/
/* PARTIAL PLANS COLLECTION */
/****************************/

bool swap_partial_plans(Partial_Plan &p1, Partial_Plan &p2) {
  /*
     Returns true if the first argument goes before the second argument in the
     strict weak ordering it defines, and false otherwise. This shall be a
     function pointer or a function object.
   */
#if ENABLE_PERFORMANCE_MODEL_PARTIAL_PLANS
  cost_value p1_cost_value = p1.metrics.get_cost_value();
  cost_value p2_cost_value = p2.metrics.get_cost_value();
  if (p1_cost_value.total_cost < p2_cost_value.total_cost)
#else
  if (p1.metrics.memory_footprint[ma_Sram] <
      p2.metrics.memory_footprint[ma_Sram])
#endif
    return true;
  else
    return false;
}

void PARTIAL_PLANS_COLLECTION::sort() {
  _partial_plans.sort(swap_partial_plans);
}

void PARTIAL_PLANS_COLLECTION::remove_invalid(int64_t size) {
  auto iter = _partial_plans.begin();
  auto end = _partial_plans.end();
  while (iter != end) {
    if (iter->sram_used > size)
      iter = _partial_plans.erase(iter);
    else
      ++iter;
  }
}

void PARTIAL_PLANS_COLLECTION::culling(int64_t size) {
  if (size == -1)
    return;
  auto start = _partial_plans.begin();
  std::advance(start, size);
  auto end = _partial_plans.end();
  _partial_plans.erase(start, end);
}

void partial_plan_perfomance(
    std::vector<Partial_Plan *> &plans_data, int32_t start, int32_t end,
    cost_model *model,
    std::unordered_map<Operation *, kernelInfo> &adjacency_table,
    mlir::ArchitectureConfig::ArchitectureConfigAttr &arch_c) {
  for (auto i = start; i < end; i++) {
    Partial_Plan *plan = plans_data[i];
    for (pKernelInfo pK : plan->kernels) {
      cost_metrics cm = model->cost_metrics_for_kernel(
          pK.kernel, arch_c, adjacency_table, pK.block_config, plan->rewrites);
      plan->metrics = plan->metrics + cm;
    }
    plan->metrics.memory_footprint[ma_Sram] = plan->sram_used;
  }
}
void PARTIAL_PLANS_COLLECTION::estimate_partial_plans_performance(
    cost_model *model, mlir::ArchitectureConfig::ArchitectureConfigAttr &arch_c,
    llvm::ThreadPool &thread_pool,
    std::unordered_map<Operation *, kernelInfo> &adjacency_table) {
#if MULTI_THREAD
  int no_threads =
      std::min(thread_pool.getThreadCount(), uint32_t(MULTI_THREAD_MAX));
  std::vector<std::thread> threads;
  std::vector<Partial_Plan *> plans_data;
  for (auto &plan : _partial_plans)
    plans_data.push_back(&plan);
  if (plans_data.size() == 0)
    return;
  std::vector<int32_t> thread_start;
  std::vector<int32_t> thread_end;
  workload_distribution(plans_data.size(), thread_start, thread_end,
                        MULTI_THREAD_MAX);
  /*
  for (int i = 0; i < thread_start.size(); i++)
      threads.push_back(std::thread(partial_plan_perfomance,
  std::ref(plans_data), thread_start[i], thread_end[i], model,
  std::ref(arch_c))); for (auto &th : threads) { th.join();
  }
  */
  for (int i = 0; i < thread_start.size(); i++)
    thread_pool.async(partial_plan_perfomance, std::ref(plans_data),
                      thread_start[i], thread_end[i], model,
                      std::ref(adjacency_table), std::ref(arch_c));
  thread_pool.wait();
#else
  for (auto &plan : _partial_plans) {
    for (pKernelInfo pK : plan.kernels) {
      cost_metrics cm = model->cost_metrics_for_kernel(
          pK.kernel, arch_c, adjacency_table, pK.block_config, plan.rewrites);
      plan.metrics = plan.metrics + cm;
    }
    plan.metrics.memory_footprint[ma_Sram] = plan.sram_used;
  }
#endif
}

/********/
/* PLAN */
/********/

Plan::Plan() {
  plan_type = ::mlir::cascadeir::PlanType::UNDEFINED;
  sram_used = 0;
}

Plan::Plan(Partial_Plan p) {
  this->plan_type = p.plan_type;
  this->kernels = p.kernels;
  this->sram_used = p.sram_used;
  this->rewrites = p.rewrites;
  this->ifm_ofm_enable = p.ifm_ofm_enable;
}

/****************/
/* Partial Plan */
/****************/

Partial_Plan::Partial_Plan() {
  plan_type = ::mlir::cascadeir::PlanType::UNDEFINED;
  sram_used = 0;
  ifm_ofm_enable = false;
}

/*********/
/* Utils */
/*********/
void print_plans(PlansCollectionLookup &planscollectionlookup, bool detailed) {
  for (auto p : planscollectionlookup) {
    std::cout << "BitVector: " << p.first
              << " No of Plans: " << (int)p.second.size() << std::endl;
    if (detailed) {
      for (auto p : p.second._plans) {
        std::cout << "    PlanType: " << ConvertToString(p.plan_type).str()
                  << " Sram Used: " << p.sram_used
                  << " Cost Metric: " << p.metrics.get_cost_value().total_cost
                  << std::endl;
        /*
        std::cout << "    c_Npu: "                << p.metrics.cycles[c_Npu] <<
        std::endl; std::cout << "    c_Npu_ElementWise: "    <<
        p.metrics.cycles[c_Npu_ElementWise] << std::endl; std::cout << "
        c_SramAccess: "         << p.metrics.cycles[c_SramAccess] << std::endl;
        std::cout << "    c_TotalPerPass: "       <<
        p.metrics.cycles[c_TotalPerPass] << std::endl; std::cout << "
        c_DramAccess: "         << p.metrics.cycles[c_DramAccess] << std::endl;
        std::cout << "    c_OnChipFlashAccess: "  <<
        p.metrics.cycles[c_OnChipFlashAccess] << std::endl; std::cout << "
        c_OffChipFlashAccess: " << p.metrics.cycles[c_OffChipFlashAccess] <<
        std::endl; std::cout << "    c_Max_Component: "      <<
        p.metrics.cycles[c_Max_Component] << std::endl;
        */
        for (auto k : p.kernels)
          std::cout << "        [" << k.block_config[0] << " "
                    << k.block_config[1] << " " << k.block_config[2] << " "
                    << k.block_config[3] << "]" << std::endl;
      }
    }
  }
  std::cout << std::endl;
}

void print_partial_plans(
    PartialPlansCollectionLookup &partialplanscollectionlookup, bool detailed) {
  for (auto p : partialplanscollectionlookup) {
    std::cout << "BitVector: " << p.first
              << " No of Partial Plans: " << p.second.size() << std::endl;
    if (detailed) {
      for (auto p : p.second._partial_plans)
        std::cout << "    PlanType: " << ConvertToString(p.plan_type).str()
                  << " Sram Used: " << p.sram_used << std::endl;
    }
  }
  std::cout << std::endl;
}

void printPlans(PLANS_COLLECTION &plans, bool cost) {
  std::cout << std::endl;
  for (auto &plan : plans._plans) {
    stringstream ss_kernels;
    stringstream ss_block_configs;
    for (auto kp : plan.kernels) {
      ss_kernels << kp.kernel << ",";
      ss_block_configs << smallvector_to_string(kp.block_config) << ",";
    }

    cost_value cv = plan.metrics.get_cost_value();
    std::cout << std::left << std::setfill(' ') << BLUE
              << "Plan Type: " << RESET << std::setw(20)
              << ConvertToString(plan.plan_type).str() << BLUE
              << " No Kernels: " << RESET << std::setw(5) << plan.kernels.size()
              << BLUE << " Kernels: " << RESET
              << std::setw(ss_kernels.str().size() + 2) << ss_kernels.str()
              << BLUE << " BlockConfigs: " << RESET
              << std::setw(ss_block_configs.str().size() + 2)
              << ss_block_configs.str() << BLUE << " SramUsed: " << RESET
              << std::setw(10) << plan.sram_used << BLUE
              << " Ifm-Ofm Overlap: " << RESET << std::setw(10)
              << plan.ifm_ofm_enable << std::endl;
  }
}

void printPlansCascades(PLANS_COLLECTION &plans, bool cost) {
  std::cout << std::endl;
  for (auto &plan : plans._plans) {
    if (plan.kernels.size() > 1) {
      cost_value cv = plan.metrics.get_cost_value();
      std::cout << std::left << std::setfill(' ') << BLUE
                << "Plan Type: " << RESET << std::setw(20)
                << ConvertToString(plan.plan_type).str() << BLUE
                << " No Kernels: " << RESET << std::setw(5)
                << plan.kernels.size() << std::endl;

      for (auto kp : plan.kernels) {
        mlir::utils::FilterMetaData filter =
            kernelir::getKernelFilter(kp.kernel);
        SmallVector<Value, 1> inputs =
            kernelir::ExtractKernelInputValues(kp.kernel);
        SmallVector<Value, 1> outputs =
            kernelir::ExtractKernelOutputValues(kp.kernel);
        Operation *major_op = kernelir::getKernelMajorOp(kp.kernel);
        printOp(*major_op);

        auto input_shape =
            inputs[0].getType().dyn_cast<mlir::SchedTensorType>().getShape();
        std::cout << "        Input: " << input_shape[0] << " "
                  << input_shape[1] << " " << input_shape[2] << " "
                  << input_shape[3] << std::endl;

        auto output_shape =
            outputs[0].getType().dyn_cast<mlir::SchedTensorType>().getShape();
        std::cout << "        Output: " << output_shape[0] << " "
                  << output_shape[1] << " " << output_shape[2] << " "
                  << output_shape[3] << std::endl;
      }
      std::cout << std::endl;
    }
  }
  std::cout << std::endl;
}

/* Search Engine */

SmallVector<value_unique_id>
SearchEngine::getPartialPlanInputs(partition &p, BitVector &plan_bit_vector) {
  SmallVector<value_unique_id> inputs;
  bool previous = false;
  uint64_t previous_index = 0;
  int32_t num_bits = plan_bit_vector.count();
  for (int i = 0; i < num_bits; i++) {
    bool current = plan_bit_vector[i];
    uint64_t current_index = i;
    if ((previous == false) and (current == true)) {
      Operation *source = p[previous_index];
      Operation *destination = p[current_index];
      auto input_tensors = adjacency_table[destination].input_tensors;
      if (p.size() > 1)
        ASSERT_COND(
            input_tensors.size() != 1,
            "SearchEngine::getPartialPlanInputs::Op with multiple inputs");
      for (auto t : input_tensors)
        inputs.push_back(t.getUniqueId());
    }
  }
  return inputs;
}

SmallVector<value_unique_id>
SearchEngine::getPartialPlanOutputs(partition &p, BitVector &plan_bit_vector) {
  SmallVector<value_unique_id> outputs;
  bool next = false;
  uint64_t next_index = 0;
  uint32_t num_bits = plan_bit_vector.count();
  for (int i = 0; i < num_bits; i++) {
    bool current = plan_bit_vector[i];
    uint64_t current_index = i;
    next = false;
    uint64_t next_index = current_index;
    if (i < (num_bits - 1)) {
      next = plan_bit_vector[i + 1];
      next_index = i + 1;
    }

    if ((current == true) and (next == false)) {
      // Transition {1 -> 0} indicates an output from the plan
      Operation *source = p[current_index];
      Operation *destination = p[next_index];
      auto output_tensors = adjacency_table[source].output_tensors;
      ASSERT_COND(
          output_tensors.size() != 1,
          "SearchEngine::getPartialPlanOutputs::Op with multiple outputs");
      for (auto t : output_tensors)
        outputs.push_back(t.getUniqueId());
    }
  }
  return outputs;
}

uint64_t SearchEngine::ExtractPartialPlanFootprint(partition &p,
                                                   BitVector &plan_bit_vector) {
  bool previous = false;
  uint64_t previous_index = 0;
  bool next = false;
  uint64_t next_index = 0;

  uint64_t io_sram = 0;
  uint32_t num_bits = plan_bit_vector.count();

  std::vector<std::pair<value_unique_id, uint64_t>> inputs;
  for (int i = 0; i < num_bits; i++) {
    bool current = plan_bit_vector[i];
    uint64_t current_index = i;

    next = false;
    uint64_t next_index = current_index;
    if (i < (num_bits - 1)) {
      next = plan_bit_vector[i + 1];
      next_index = i + 1;
    }
    // std::cout << "Values: " << previous << " " << current << " " << next <<
    // std::endl; std::cout << "Index: "  << previous_index << " " <<
    // current_index << " " << next_index << std::endl;

    if ((previous == false) and (current == true)) {
      // Transition {0 -> 1} indicates an input into the plan
      Operation *source = p[previous_index];
      Operation *destination = p[current_index];
      auto input_tensors = adjacency_table[destination].input_tensors;
      if (p.size() > 1)
        ASSERT_COND(input_tensors.size() != 1,
                    "SearchEngine::ExtractPartialPlanFootprint::Op with "
                    "multiple inputs");
      for (auto t : input_tensors) {
        if (t.get_mem_area() == search_mem_area) {
          io_sram += array_prod(t.getStorageShape());
          /* Track if a tensor is alreay registered as an input */
          inputs.push_back(
              std::make_pair(t.getUniqueId(), array_prod(t.getStorageShape())));
        }
      }
    }

    if ((current == true) and (next == false)) {
      // Transition {1 -> 0} indicates an output from the plan
      Operation *source = p[current_index];
      Operation *destination = p[next_index];
      auto output_tensors = adjacency_table[source].output_tensors;
      ASSERT_COND(output_tensors.size() != 1,
                  "SearchEngine::ExtractPartialPlanFootprint::Op with multiple "
                  "outputs");
      for (auto t : output_tensors) {
        if (t.get_mem_area() == search_mem_area)
          io_sram += array_prod(t.getStorageShape());
      }

      unsigned kernel_time =
          source->getAttrOfType<mlir::IntegerAttr>("current_time").getInt();

      io_sram += non_local_mem_usage[kernel_time];
      /*
         If this plan is going to fail we try and be more clever with non local
         memry usage and check that we have not allocated the inputs multiple
         times. We unleash these on all the plans and not on just once which
         will fail culling but for now limitigin to allow headroom in allocation
       */
      if ((inputs.size() == 1) and (io_sram > sram_size)) {
        auto &non_local_values = non_local_mem_usage_values[kernel_time];
        auto iter = non_local_values.find(inputs.front().first);
        if (iter != non_local_values.end())
          io_sram -= inputs.front().second;
      }
    }
    previous = current;
    previous_index = current_index;
  }
  // std::cout << std::endl;
  return io_sram;
}

void SearchEngine::close_plans(partition &part) {
  // Convert each
  for (auto &p : partition_partialplanscollectionlookup) {
    auto plans_collection_lookup = partition_planscollectionlookup.emplace(
        std::make_pair(p.first, PLANS_COLLECTION()));

    auto bitvector = p.first;

    /* Calculate the Stationary Input & Outputs Tensors (Do not need to be
     * NHWC16) */
    uint64_t io_sram = ExtractPartialPlanFootprint(part, bitvector);

    for (auto &partial_plan : p.second._partial_plans) {
      // 0. Update the SRAM for each partial plan
      partial_plan.sram_used += io_sram;
      partial_plan.ifm_ofm_enable = false;
#if ENABLE_IFM_OFM_OVERLAP
      // 1. Apply IFM OFM OVerlap if possible to ifm-ofm overlay
      if (partial_plan.sram_used > sram_size) {
        if (partial_plan.kernels.size() == 1) {
          if (partial_plan.plan_type ==
              mlir::cascadeir::PlanType::IFM_UNDEFINED_WEIGHT_STATIONARY) {
            auto op = partial_plan.kernels.front().kernel;
            auto inputs = adjacency_table[op].inputs;
            auto outputs = adjacency_table[op].outputs;
            auto input_tensors = adjacency_table[op].input_tensors;
            auto output_tensors = adjacency_table[op].output_tensors;
            ASSERT_COND(
                output_tensors.size() != 1,
                "SearchEngine::IFM-OFM Overlap::Op with multiple inputs");
            auto kernel_filter = adjacency_table[op].KernelFilter;
            auto kernel_major_op = adjacency_table[op].major_op;
            // 0. Input needs to have 1 use otherwise we cannot overlap the
            // output on the input
            // 1. Output can't be slice viewed otherwise we cannot overlap the
            // output on the input
            if (adjacency_table[op].inputs[0].hasOneUse() and
                not(llvm::isa<scheduleir::SliceViewOp>(
                    adjacency_table[op].outputs[0].getDefiningOp()))) {
              // Both need to be in the same SRAM MemArea in order to be
              // overlapped
              if (input_tensors[0].get_mem_area() ==
                  output_tensors[0].get_mem_area() == ma_Sram) {
                if (llvm::isa<scheduleir::AddOp>(kernel_major_op)) {
                  partial_plan.sram_used -=
                      array_prod(input_tensors[1].getStorageShape());
                  partial_plan.ifm_ofm_enable = true;
                  // std::cout << "IFM-OFM Overlap Detected" << std::endl;
                }
              }
            }
          }
        }
      }
#endif
      // 2. Convert to Plans
      plans_collection_lookup.first->second._plans.emplace(
          plans_collection_lookup.first->second._plans.end(),
          Plan(partial_plan));
    }
  }

  // Update th PLan Types
  for (auto &p : partition_planscollectionlookup) {
    for (auto &plan : p.second._plans) {
      if (plan.plan_type ==
          ::mlir::cascadeir::PlanType::IFM_UNDEFINED_WEIGHT_STREAMING) {
        if (plan.kernels.size() > 1)
          plan.plan_type =
              ::mlir::cascadeir::PlanType::IFM_STREAMING_WEIGHT_STREAMING;
        else
          plan.plan_type =
              ::mlir::cascadeir::PlanType::IFM_STATIONARY_WEIGHT_STREAMING;
      }

      if (plan.plan_type ==
          ::mlir::cascadeir::PlanType::IFM_UNDEFINED_WEIGHT_STATIONARY) {
        if (plan.kernels.size() > 1)
          plan.plan_type =
              ::mlir::cascadeir::PlanType::IFM_STREAMING_WEIGHT_STATIONARY;
        else
          plan.plan_type =
              ::mlir::cascadeir::PlanType::IFM_STATIONARY_WEIGHT_STATIONARY;
      }
    }
  }
}

void SearchEngine::merge_partial_plans(
    partition &p, TensorAnchor &ts, BitVector producer_bitvector,
    PARTIAL_PLANS_COLLECTION &producer_partial_plans,
    BitVector consumer_bitvector,
    PARTIAL_PLANS_COLLECTION &consumer_partial_plans,
    BitVector merged_bitvector,
    PARTIAL_PLANS_COLLECTION &merged_partial_plans) {
  for (auto producer_partial_plan : producer_partial_plans._partial_plans) {
    if (producer_partial_plan.plan_type ==
        ::mlir::cascadeir::PlanType::IFM_UNDEFINED_WEIGHT_STREAMING)
      continue;

    for (auto consumer_partial_plan : consumer_partial_plans._partial_plans) {
      if (consumer_partial_plan.plan_type ==
          ::mlir::cascadeir::PlanType::IFM_UNDEFINED_WEIGHT_STREAMING)
        continue;
      if (producer_partial_plan.plan_type != consumer_partial_plan.plan_type)
        continue;

      Operation *kernel_1 = p[producer_bitvector.bsr()];
      FilterMetaData kernel_1_metadata = adjacency_table[kernel_1].KernelFilter;
      NpuBlockConfig block_config_ke1 =
          producer_partial_plan.kernels.back().block_config;
      NpuBlockType kernel_1_primay_op_block_type =
          adjacency_table[kernel_1].BlockType;

      Operation *kernel_2 = p[consumer_bitvector.bsf()];
      FilterMetaData kernel_2_metadata = adjacency_table[kernel_2].KernelFilter;
      NpuBlockConfig block_config_ke2 =
          consumer_partial_plan.kernels.front().block_config;
      NpuBlockType kernel_2_primay_op_block_type =
          adjacency_table[kernel_2].BlockType;

      SmallVector<int64_t, 2> rolling_buffer_dims =
          SearchEngine::rolling_buffer_dims_from_kernels(
              ts.get_SchedTensorType(), ts.get_tensor_depth(),
              ts.get_tensor_bits(), kernel_1, kernel_1_metadata,
              block_config_ke1, kernel_1_primay_op_block_type, kernel_2,
              kernel_2_metadata, block_config_ke2,
              kernel_2_primay_op_block_type);

      uint64_t sram_used = storage_size_for_sub_purpose(
          ts.get_SchedTensorType(), arch_c, vsp_RollingBufferY,
          rolling_buffer_dims[0]);

      Partial_Plan partial_plan;

      partial_plan.plan_type = producer_partial_plan.plan_type;

      for (auto kern : producer_partial_plan.kernels)
        partial_plan.kernels.push_back(kern);
      for (auto kern : consumer_partial_plan.kernels)
        partial_plan.kernels.push_back(kern);

      partial_plan.sram_used = producer_partial_plan.sram_used +
                               consumer_partial_plan.sram_used + sram_used;

      for (auto rewrite : producer_partial_plan.rewrites)
        partial_plan.rewrites.push_back(rewrite);
      value_rewrite ts_rolling_buffer;
      ts_rolling_buffer.rewrite_op = ChangeTensorSubPurpose;
      ts_rolling_buffer.type = ts.get_SchedTensorType();
      ts_rolling_buffer.value = ts.get_Value();
      ts_rolling_buffer.new_sub_purpose = vsp_RollingBufferY;
      auto s = ts.get_SchedTensorType().getStorageShape();

      ts_rolling_buffer.new_storage_shape = {s[0], rolling_buffer_dims[0], s[2],
                                             round_up<int64_t>(s[3], 16)};
      ts_rolling_buffer.new_format = vf_NHCWB16;
      ts_rolling_buffer.insert_dma = false;
      partial_plan.rewrites.push_back(ts_rolling_buffer);
      for (auto rewrite : consumer_partial_plan.rewrites)
        partial_plan.rewrites.push_back(rewrite);

      merged_partial_plans._partial_plans.push_back(partial_plan);
    }
  }
}

bool SearchEngine::grow_plans(partition &p) {
  // std::cout << "grow_plans::Start" << std::endl;
  uint64_t iteration = 0;

  bool all_anchors_closed = true;

  std::vector<std::pair<value_unique_id, BitVector>> consumers_to_add;
  std::vector<std::pair<value_unique_id, BitVector>> producers_to_add;

  for (auto &ts : partition_tensoranchors) {
    auto producers = ts.second.get_producers();
    auto consumers = ts.second.get_consumers();

    // std::cout << "Start Tensor Anchor: " << std::endl;
    for (auto producer_bitvector : producers) {
      for (auto consumer_bitvector : consumers) {
        ASSERT_COND(producer_bitvector.get_n_bits() !=
                        consumer_bitvector.get_n_bits(),
                    "SearchEngine::grow_plans() producer&consumer bitvector "
                    "len do not match");
        auto merged_bitvector =
            (BitVector::zeros(producer_bitvector.get_n_bits()) |
             producer_bitvector | consumer_bitvector);

        if (partition_partialplanscollectionlookup.find(merged_bitvector) !=
            partition_partialplanscollectionlookup.end())
          continue;

        /*
        iteration++;
        std::cout << iteration << " Producer: " << producer_bitvector << "
        Consumer: " << consumer_bitvector << " Merged: " << merged_bitvector
                  << " producers size: " << producers.size()
                  << " consumers size: " << consumers.size()
                  << std::endl;
        */

        auto &producer_partial_plans =
            partition_partialplanscollectionlookup[producer_bitvector];
        auto &consumer_partial_plans =
            partition_partialplanscollectionlookup[consumer_bitvector];
        PARTIAL_PLANS_COLLECTION merged_partial_plans;

        // Merge the PARTIAL_PLAN_COLLECTIONS calculating the interediate
        // rolling buffers and creating the rewrites too
        merge_partial_plans(p, ts.second, producer_bitvector,
                            producer_partial_plans, consumer_bitvector,
                            consumer_partial_plans, merged_bitvector,
                            merged_partial_plans);

        // Register the new merged bitvector and plans in the lookup
        partition_partialplanscollectionlookup[merged_bitvector] =
            merged_partial_plans;

        // Update the Tensor Anchors that need a new consumer
        SmallVector<value_unique_id> inputs =
            getPartialPlanInputs(p, merged_bitvector);
        for (auto i : inputs) {
          if (partition_tensoranchors.find(i) != partition_tensoranchors.end())
            consumers_to_add.emplace_back(i, merged_bitvector);
        }
        SmallVector<value_unique_id> outputs =
            getPartialPlanOutputs(p, merged_bitvector);
        for (auto o : outputs) {
          if (partition_tensoranchors.find(o) != partition_tensoranchors.end())
            producers_to_add.emplace_back(o, merged_bitvector);
        }

        // Indicate we did a growth so we may need to loop through the tensor
        // anchors again
        all_anchors_closed = false;
      }
    }
    // std::cout << "End Tensor Anchor: " << std::endl;
  }

  /* Delayed Update: Otherwise the Loop grows indefinitely. We want a clear
   * {growth, culling} cycle */
  for (auto c : consumers_to_add)
    partition_tensoranchors.at(c.first).add_consumer(c.second);
  for (auto p : producers_to_add)
    partition_tensoranchors.at(p.first).add_producer(p.second);
  // std::cout << "grow_plans::End" << std::endl;

  // Return true to indicate we don't have any more partial plans to grow
  return all_anchors_closed;
}

SmallVector<int64_t, 2> SearchEngine::rolling_buffer_dims_from_kernels(
    SchedTensorType tensor, unsigned tensor_depth, unsigned tensor_bits,
    Operation *kernel_1, FilterMetaData kernel_1_metadata,
    NpuBlockConfig block_config_ke1, NpuBlockType kernel_1_primay_op_block_type,
    Operation *kernel_2, FilterMetaData kernel_2_metadata,
    NpuBlockConfig block_config_ke2,
    NpuBlockType kernel_2_primay_op_block_type) {
  BlockStruct ofm_block(block_config_ke2[1], block_config_ke2[0],
                        block_config_ke2[3]);

  unsigned ifm_block_depth = 0;
  if ((kernel_2_primay_op_block_type == ConvolutionMxN) or
      (kernel_2_primay_op_block_type == VectorProduct))
    ifm_block_depth = calc_ifm_block_depth(arch_c, tensor_depth, tensor_bits);
  else
    ifm_block_depth = block_config_ke2[3];
  BlockStruct ifm_block =
      get_ifm_block_size(arch_c, ifm_block_depth, ofm_block, kernel_2_metadata);
  uint64_t height = round_up<uint64_t>(ifm_block.height + block_config_ke1[0],
                                       block_config_ke1[0]);
  uint64_t width = ifm_block.width;
  return {height, width};
}

} // namespace planner
} // namespace mlir

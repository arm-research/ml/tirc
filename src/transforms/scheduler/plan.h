/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#ifndef PLAN_H
#define PLAN_H

#include "src/utils/bitvector.h"
#include "src/utils/dag.h"

#include "src/transforms/scheduler/cost_model.h"

#include "llvm/Support/ThreadPool.h"

namespace mlir {
namespace planner {

class pKernelInfo {
public:
  Operation *kernel;
  NpuBlockConfig block_config;
  SmallVector<int64_t, 2> rolling_buffer_dims;
  pKernelInfo(Operation *kernel, NpuBlockConfig block_config,
              SmallVector<int64_t, 2> rolling_buffer_dims = {-1, -1}) {
    this->kernel = kernel;
    this->block_config = block_config;
    this->rolling_buffer_dims = rolling_buffer_dims;
  }
};

/* (A) Partial Plan */
class Partial_Plan {
public:
  Partial_Plan();
  mlir::cascadeir::PlanType plan_type;
  list<pKernelInfo> kernels;
  uint64_t sram_used;
  std::vector<value_rewrite> rewrites;
  bool ifm_ofm_enable;
  cost_metrics metrics;
};

class PARTIAL_PLANS_COLLECTION {
public:
  void push_back(Partial_Plan p) { _partial_plans.push_back(p); };
  void sort();
  void culling(int64_t size);
  void remove_invalid(int64_t size);
  int64_t size() { return _partial_plans.size(); }
  void clear() { _partial_plans.clear(); }
  Partial_Plan front() { return _partial_plans.front(); }
  void estimate_partial_plans_performance(
      cost_model *model,
      mlir::ArchitectureConfig::ArchitectureConfigAttr &arch_c,
      llvm::ThreadPool &thread_pool,
      std::unordered_map<Operation *, kernelInfo> &adjacency_table);
  std::list<Partial_Plan> _partial_plans;
};

using PartialPlansCollectionLookup =
    std::unordered_map<BitVector, PARTIAL_PLANS_COLLECTION>;

/* (B) Complete Plan */
class Plan : public Partial_Plan {
public:
  Plan();
  Plan(Partial_Plan p);
};

class PLANS_COLLECTION {
public:
  void push_back(Plan p) { _plans.push_back(p); };
  void sort();
  void culling(int64_t size);
  void remove_invalid(int64_t size);
  int64_t size() { return _plans.size(); }
  void clear() { _plans.clear(); }
  Partial_Plan front() { return _plans.front(); }
  void estimate_plans_performance(
      cost_model *model,
      mlir::ArchitectureConfig::ArchitectureConfigAttr &arch_c,
      llvm::ThreadPool &thread_pool,
      std::unordered_map<Operation *, kernelInfo> &adjacency_table);
  std::list<Plan> _plans;
};

using PlansCollectionLookup = std::unordered_map<BitVector, PLANS_COLLECTION>;

/* Utils */
void print_partial_plans(
    PartialPlansCollectionLookup &partialplanscollectionlookup,
    bool detailed = false);
void print_plans(PlansCollectionLookup &planscollectionlookup,
                 bool detailed = false);
void printPlans(PLANS_COLLECTION &plans, bool cost = true);
void printPlansCascades(PLANS_COLLECTION &plans, bool cost = true);
void pair_values(mlir::Value &t0, mlir::Value &t1, int32_t index);

} // namespace planner
} // namespace mlir

#endif // PLAN_H

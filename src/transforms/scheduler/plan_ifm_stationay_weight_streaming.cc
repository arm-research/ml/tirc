/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#include "src/transforms/scheduler/cost_metrics.h"
#include "src/transforms/scheduler/search_engine.h"

namespace mlir {
namespace planner {

bool equal(NpuBlockConfig a, NpuBlockConfig b) {
  if ((a[0] == b[0]) and (a[1] == b[1]) and (a[2] == b[2]) and (a[3] == b[3]))
    return true;
  else
    return false;
}

void SearchEngine::single_calculate_weight_streaming(partition p) {
  for (Operation *op : p) {
    // If no Weights present no need to do if try and build weight streaming
    // plans
    auto &weight = adjacency_table[op].weights;
    if (weight.size() == 0)
      continue;

    BitVector &b = partition_bitvectorlookup.at(op);
    PARTIAL_PLANS_COLLECTION *partial_plans;
    if (partition_partialplanscollectionlookup.find(b) !=
        partition_partialplanscollectionlookup.end())
      partial_plans = &(partition_partialplanscollectionlookup.at(b));
    else
      partial_plans = &((*(partition_partialplanscollectionlookup
                               .emplace(b, PARTIAL_PLANS_COLLECTION())
                               .first))
                            .second);

    std::vector<NpuBlockConfig> all_block_configs;
    auto height = adjacency_table[op].input_tensors[0].getShape()[1];
    auto width = adjacency_table[op].input_tensors[0].getShape()[2];
    for (auto c : adjacency_table[op].all_block_configs)
      all_block_configs.push_back(c);
    if (all_block_configs.size() == 0)
      continue;
    ASSERT_COND(all_block_configs.size() == 0,
                "SearchEngine::search_weight_streaming_body::No Block Configs "
                "Detected for this Kernel");

    unsigned sram_used = 0;

    bool valid_weight_rewrite = false;
    bool valid_bias_rewrite = false;

    value_rewrite bias_rewrite;
    auto &bias = adjacency_table[op].bias;
    auto &bias_tensors = adjacency_table[op].bias_tensors;
    for (uint32_t idx = 0; idx < bias.size(); idx++) {
      bias_rewrite.rewrite_op = ChangeTensorSubPurpose;
      bias_rewrite.type = bias_tensors[idx];
      bias_rewrite.value = bias[idx];
      bias_rewrite.insert_dma = true;
      bias_rewrite.new_sub_purpose = vsp_Standard;
      bias_rewrite.new_shape = {bias_tensors[idx].getShape()[0]};
      bias_rewrite.new_storage_shape = {bias_tensors[idx].getStorageShape()[0]};
      bias_rewrite.new_format = vf_NHWC;
      valid_bias_rewrite = true;
    }

    auto &weight_tensors = adjacency_table[op].weight_tensors;
    for (NpuBlockConfig block_config : all_block_configs) {
      sram_used = 0;

      value_rewrite weight_rewrite;
      valid_weight_rewrite = false;
      for (uint32_t idx = 0; idx < weight.size(); idx++) {
        uint64_t storage = storage_size_for_sub_purpose(
            weight_tensors[idx], arch_c, vsp_DoubleBuffer, block_config[3]);
        sram_used += storage;

        weight_rewrite.rewrite_op = ChangeTensorSubPurpose;
        weight_rewrite.type = weight_tensors[idx];
        weight_rewrite.value = weight[idx];
        weight_rewrite.insert_dma = true;
        weight_rewrite.new_sub_purpose = vsp_DoubleBuffer;
        weight_rewrite.new_shape = {weight_tensors[idx].getShape()[0],
                                    weight_tensors[idx].getShape()[1],
                                    weight_tensors[idx].getShape()[2],
                                    weight_tensors[idx].getShape()[3]};
        weight_rewrite.new_storage_shape = {1, 1, 1, (int64_t)storage};
        if (llvm::isa<scheduleir::DepthwiseConv2DOp>(
                kernelir::getKernelMajorOp(op)))
          weight_rewrite.new_format = vf_HWOI;
        else
          weight_rewrite.new_format = vf_HWIO;
        valid_weight_rewrite = true;
      }

      for (auto t : bias_tensors)
        sram_used += array_prod(t.getStorageShape());

      Partial_Plan partial_plan;
      partial_plan.plan_type =
          ::mlir::cascadeir::PlanType::IFM_UNDEFINED_WEIGHT_STREAMING;
      partial_plan.kernels.push_back(pKernelInfo(op, block_config));
      partial_plan.sram_used = sram_used;
      if (valid_weight_rewrite)
        partial_plan.rewrites.push_back(weight_rewrite);
      if (valid_bias_rewrite)
        partial_plan.rewrites.push_back(bias_rewrite);
      partial_plans->push_back(partial_plan);
    }
  }
}

} // namespace planner
} // namespace mlir

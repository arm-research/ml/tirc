/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#include "absl/memory/memory.h"
#include "mlir/Dialect/Quant/QuantOps.h"
#include "mlir/Dialect/Quant/QuantTypes.h"
#include "mlir/Dialect/StandardOps/IR/Ops.h"      // from @llvm-project
#include "mlir/IR/AffineMap.h"                    // from @llvm-project
#include "mlir/IR/Attributes.h"                   // from @llvm-project
#include "mlir/IR/BlockAndValueMapping.h"         // from @llvm-project
#include "mlir/IR/Builders.h"                     // from @llvm-project
#include "mlir/IR/Location.h"                     // from @llvm-project
#include "mlir/IR/MLIRContext.h"                  // from @llvm-project
#include "mlir/IR/Operation.h"                    // from @llvm-project
#include "mlir/IR/PatternMatch.h"                 // from @llvm-project
#include "mlir/Interfaces/InferTypeOpInterface.h" // from @llvm-project
#include "mlir/Pass/Pass.h"                       // from @llvm-project
#include "mlir/Transforms/DialectConversion.h"    // from @llvm-project

#include "src/transforms/passes.h"

#include "src/ir/kernel_ir.h"
#include "src/ir/schedule_ir.h"

#include "src/utils/cluster_ir.h"
#include "src/utils/dag.h"

#include "src/ir/architecture_config/architecture_config.h"
#include "src/ir/compiler_config/compiler_config.h"

#include "src/utils/checkIR.h"
#include "src/utils/printIR.h"

#include "src/transforms/scheduler/scheduler.h"

using namespace std;
#include <iostream>

#define PASS_NAME "planner-scheduling"
#define DEBUG_TYPE PASS_NAME

namespace mlir {
namespace planner {
namespace {

/* Run on Rest of IR and pack the remaining Ops into Passes*/
int ScheduleOps(FuncOp function, MLIRContext *context) {
  Region *region = function.getCallableRegion();
  Block &block = region->front();
  llvm::iplist<Operation>::iterator i;

  Builder builder(context);

  for (i = block.begin(); i != block.end(); i++) {
    if (llvm::isa<scheduleir::NpuRegionOp>(i)) {
      Operation *op = &(*i);

      mlir::ArchitectureConfig::ArchitectureConfigAttr architecture_config =
          AccessArchitectureConfigAttribute_From_NpuRegion(op);

      mlir::CompilerConfig::CompilerConfigAttr compiler_config =
          AccessCompilerConfigAttribute_From_NpuRegion(op);

      Region &region_instruction = i->getRegion(0);
      Block &block_npu_region = region_instruction.front();

      /* Schedule the Passes into CLusters */
      std::vector<cascadeInfo> cascades;
      Scheduler(context, block_npu_region, architecture_config, compiler_config,
                cascades);

      std::vector<Operation *> order_ops_filtered_constant_ops;

      for (cascadeInfo &c : cascades) {
        /* The cluster instruction is placed at the location of the first OP */
        Operation *clusterOp;
        if (Cluster(c.kernels, order_ops_filtered_constant_ops,
                    block_npu_region, cluster_op::cluster_region_op, context,
                    clusterOp) != 0) {
          std::cout << "PlannerScheduler:::Clustering failed" << std::endl;
          return 1;
        }
        clusterOp->setAttr("name", builder.getStringAttr(c.name));
        clusterOp->setAttr("plan_type",
                           builder.getI32IntegerAttr(int(c.plan.plan_type)));
        clusterOp->setAttr("current_time", builder.getI32IntegerAttr(0));
      }
    }
  }

  // Check All Ops in Npu Block are KernelIR, CastIn or CastOut
  for (i = block.begin(); i != block.end(); i++) {
    if (llvm::isa<scheduleir::NpuRegionOp>(i)) {
      Region &region_instruction = i->getRegion(0);
      Block &block_instruction = region_instruction.front();
      llvm::iplist<Operation> &operations_npu_region =
          block_instruction.getOperations();
      for (auto &op : operations_npu_region) {
        if (llvm::isa<cascadeir::CascadeRegionOp>(op))
          continue;
        else if (llvm::isa<scheduleir::CastInOp>(op))
          continue;
        else if (llvm::isa<scheduleir::YieldOp>(op))
          continue;
        else if (llvm::isa<scheduleir::CastOutOp>(op))
          continue;
        else {
          printOp(op);
          FATAL_ERROR("Fatal Error Non-Kernel Op found after Scheduler");
        }
      }
    }
  }
  return 0;
}

class PlannerScheduler : public PassWrapper<PlannerScheduler, FunctionPass> {
public:
  explicit PlannerScheduler() {}

  StringRef getArgument() const final {
    // This is the argument used to refer to the pass in
    // the textual format (on the commandline for example).
    return PASS_NAME;
  }
  StringRef getDescription() const final {
    // This is a brief description of the pass.
    return "Apply Scheduling to IR";
  }

  void runOnFunction() override;
};

void PlannerScheduler::runOnFunction() {

  auto function = getFunction();
  auto *ctx = &getContext();

  if (function.getName() == "main") {
    if (ScheduleOps(function, ctx) > 0)
      signalPassFailure();
  }
}

} // anonymous namespace

std::unique_ptr<OperationPass<FuncOp>> CreatePlannerScheduler() {
  return absl::make_unique<PlannerScheduler>();
}

static PassRegistration<PlannerScheduler> pass;
} // namespace planner
} // namespace mlir

/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#include "src/transforms/scheduler/power_set.h"
#include "src/transforms/scheduler/search_engine.h"

#include <iomanip>
#include <iostream>
#include <vector>
using namespace std;

namespace mlir {
namespace planner {

powerset generate_powerset(partition part) {
  powerset ps;
  for (int64_t end = part.size(); end >= 0; end--) {
    for (int64_t start = 0; start <= end; start++) {
      std::vector<Operation *> set;
      for (int i = start; i < end; i++)
        set.push_back(part[i]);
      if (set.size() > 0)
        ps.push_back(set);
    }
  }
  return ps;
}

void printPowerSet(powerset ps) {
  std::cout << std::endl;
  std::cout << YELLOW << "PowerSet:" << RESET << std::endl;
  for (auto set : ps) {
    std::cout << "{";
    for (auto x : set) {
      std::cout << x << " ";
    }
    std::cout << "}" << std::endl;
  }
  std::cout << std::endl;
}

void SearchEngine::generate_bitvectors(partition &p) {
  size_t num_bits = p.size();
  int64_t idx = 0;
  for (auto e : p)
    partition_bitvectorlookup.emplace(
        std::make_pair(e, BitVector::singleton_set(num_bits, idx++)));
}

} // namespace planner
} // namespace mlir

/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#include "src/transforms/scheduler/scheduler.h"
#include "src/transforms/scheduler/plan.h"
#include "src/transforms/scheduler/search_engine.h"

namespace mlir {
namespace planner {

unsigned Scheduler(MLIRContext *ctx, Block &block,
                   mlir::ArchitectureConfig::ArchitectureConfigAttr &arch_c,
                   mlir::CompilerConfig::CompilerConfigAttr &compiler_c,
                   std::vector<cascadeInfo> &cascades,
                   SchedulerAlgorithm sched_algo) {
  switch (sched_algo) {
  case SearchBottomUp: {
    SearchEngine search_engine(block, arch_c, compiler_c, cascades, ctx);
    return search_engine.search();
  }
  }
}

} // namespace planner
} // namespace mlir

/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#include "src/transforms/scheduler/search_engine.h"

namespace mlir {
namespace planner {

#define ENABLE_LOGGING 0
#define ENABLE_TIMING 0

#define ENABLE_LOGGING_FINAL_PLANS 0

#define SIZE_1 1
#define SIZE_5 5
#define SIZE_10 10
#define SIZE_UNLIMITED -1

#define MAX_NO_GROWTH_ITERATIONS 100

SearchEngine::SearchEngine(
    Block &block, mlir::ArchitectureConfig::ArchitectureConfigAttr &arch_c,
    mlir::CompilerConfig::CompilerConfigAttr &compiler_c,
    std::vector<cascadeInfo> &cascades, MLIRContext *ctx) {
  this->block = &block;
  this->arch_c = arch_c;
  this->compiler_c = compiler_c;
  this->cascades = &cascades;
  this->context = ctx;
  search_mem_area = ma_Sram;

  this->disable_cascade = compiler_c.disable_cascade().getValue();

  this->disable_weight_streaming =
      compiler_c.disable_weight_streaming().getValue();
  this->disable_stationary = compiler_c.disable_stationary().getValue();

  this->disable_block_config_search =
      compiler_c.disable_block_config_search().getValue();

  ArrayRef<mlir::Attribute> bc = compiler_c.custom_block_config().getValue();
  this->custom_block_config = {
      *(bc[0].dyn_cast<mlir::IntegerAttr>().getValue().getRawData()),
      *(bc[1].dyn_cast<mlir::IntegerAttr>().getValue().getRawData()),
      *(bc[2].dyn_cast<mlir::IntegerAttr>().getValue().getRawData()),
      *(bc[3].dyn_cast<mlir::IntegerAttr>().getValue().getRawData())};

  this->sram_size = this->arch_c.sram_size().getInt();

  cascade_counter = 0;
}

void SearchEngine::generate_cascades() {
  LOG(ENABLE_LOGGING_SEARCH_ENGINE_VERBOSE_LEVEL_1,
      "SearchEngine::generate_cascades()::Start");

  Builder builder(this->context);
  /* Here we have already ensured that a Kernel only belongs to a single plan
   * i.e we have applied final filtering */
  for (auto plan : final_plans._plans) {
    cascadeInfo cascadeINFO;
    cascadeINFO.name = "CASCADE_" + to_string(cascade_counter);
    cascadeINFO.plan = plan;
    cascadeINFO.current_time = 0;
    for (pKernelInfo pK : plan.kernels) {
      cascadeINFO.kernels.push_back(pK.kernel);
      if (not(kernelir::isKernelMemoryOp(pK.kernel))) {
        /* Create the Shared Buffer Configuration that is actually going to be
         * used for this Pass */
        SharedBufferAllocation sba =
            create_shared_buffer_allocation_for_kernel_and_try_block_config(
                pK.kernel, pK.block_config);
        pK.kernel->setAttr("block_config",
                           builder.getI32ArrayAttr(pK.block_config));
        pK.kernel->setAttr(
            "shared_buffer_banks_locations",
            builder.getI32ArrayAttr(sba.shared_buffer_bank_locations));
        pK.kernel->setAttr(
            "shared_buffer_banks_required",
            builder.getI32ArrayAttr(sba.shared_buffer_banks_required));
        pK.kernel->setAttr("shared_buffer_use_accumulator_element",
                           builder.getI32IntegerAttr(
                               sba.shared_buffer_use_accumulator_element));
        pK.kernel->setAttr(
            "shared_buffer_ifm_count",
            builder.getI32IntegerAttr(sba.shared_buffer_ifm_count));
      }
    }
    this->cascades->push_back(cascadeINFO);
// Apply re-writes (Nothing needed for Stationary)
// Re-Writes can be applied here given that types from the kernels will be
// inherited by the cascade (MLIR Type system) Apply Set Format, Sub-purpose
// ReWrites accroding to the Final Cascade
//    To the Kernels. When creating the cascade these parameters will be
//    inherited A Cascade is assigned a Plan; A Plan changes the nature of the
//    tensors belonging to that cascade and this is done in ir_rewrite
//      * PLan Weight Re_Writes
//      * Plan Input Re-Writes
//      * Plan Output Re-ReWrites
//      * Plan Intermediate Re_Writes
#if ENABLE_LOGGING
    std::cout << "ReWrites to Apply: " << plan.rewrites.size() << std::endl;
#endif

    for (value_rewrite rewrite : plan.rewrites) {
      if (rewrite.insert_dma) {
        // Insert a DMA between the Value and the consuming OP and apply changes
        // the value comingout of the DMA Op
        insertDMA(rewrite);
      } else {
        rewrite.type.setStorageShape(rewrite.new_storage_shape);
        rewrite.type.set_sub_purpose(rewrite.new_sub_purpose);
        rewrite.type.set_format(rewrite.new_format);
      }
    }

    if (plan.ifm_ofm_enable) {
      /*
         Be carefull as this does not update everything and needs
         double checking when more complicated scenarios are implemented
         Also across memory ops e.g concat this breaks if we try to pair the
         underlying types
       */
      mlir::Value t0 = adjacency_table[plan.kernels.front().kernel].inputs[0];
      mlir::Value t1 =
          adjacency_table[plan.kernels.front().kernel].major_op->getResult(0);
      mlir::Value t2 = adjacency_table[plan.kernels.front().kernel].outputs[0];
      mlir::scheduleir::pair_values(t0, t1, t2, 1);
    }
  }

  LOG(ENABLE_LOGGING_SEARCH_ENGINE_VERBOSE_LEVEL_1,
      "SearchEngine::generate_cascades()::End");
}

/******************************************************************************/
/*                          SEARCH                                            */
/******************************************************************************/

void SearchEngine::search_graph() {
  LOG(ENABLE_LOGGING_SEARCH_ENGINE_VERBOSE_LEVEL_1,
      "SearchEngine::search_graph()::Start");

  u55_cost_model model;
  final_plans.clear();

  START_TIMING
  generate_adjacency_table(16);
  END_TIMING("GenerateAdjacencyTable")

  START_TIMING
  calc_non_local_mem_usage();
  END_TIMING("CalculateNonLocalMemUsage")

  START_TIMING
  std::set<value_mem_type> target_value_mem_type_set_intermettent = {
      mt_Scratch, mt_Scratch_fast};
  LiveRangeGraph lrg;
  LivenessAnalysis_Kernels(lrg, block, value_mem_area::ma_Sram,
                           target_value_mem_type_set_intermettent, context,
                           arch_c);
  pin_tensors(lrg, arch_c);
  END_TIMING("PinTensors")

  // Create Partitions (Controls the extend of Cascading and where we need to
  // introduce limitations)
  std::vector<partition> part_network;
  START_TIMING
  part_network = create_partitions(block, this->disable_cascade);
  END_TIMING("PartitionCreation")
#if ENABLE_LOGGING
  printPartitions(part_network);
#endif

  /*
     All this is checking at this point is that the input and output tensors can
     be held in SRAM as full tensors it does not check that weights within a
     partition can be all stationary or that non local memory can be satisfied.
   */
  /*
  START_TIMING
  check_partition_endings(part_network, arch_c);
  END_TIMING("CheckPartitionEndings")
  */

#if ENABLE_LOGGING
  std::cout << std::endl
            << YELLOW << "No of Partitions to search: " << part_network.size()
            << RESET << std::endl;
#endif

  START_TIMING
#if (ENABLE_LOGGING || ENABLE_TIMING)
  std::cout << std::endl;
#endif
  for (auto part : part_network) {
#if (ENABLE_LOGGING || ENABLE_TIMING)
    std::cout << std::endl;
#endif

    /* Memory Ops are not scheduled unless doing cascaded_branching*/
    if (part.size() == 1) {
      if (kernelir::isKernelMemoryOp(part[0])) {
        Plan chosen_plan;
        pKernelInfo k(part[0], {});
        chosen_plan.kernels.push_back(k);
        chosen_plan.plan_type = ::mlir::cascadeir::PlanType::MEMORY;
        final_plans.push_back(chosen_plan);
        continue;
      }
    }

    /*
     ** In IFM Streaming Partial Plans are chosen on minimum SRAM
     ** The final plan selection for each Candidate is chosen using the
     *performance model
     */

#if ENABLE_LOGGING
    std::cout << "Searching Next Partition: " << std::endl;
    printPartition(part);
#endif

    // Generate Simple data structures to track search algorith
    partition_bitvectorlookup.clear();
    partition_partialplanscollectionlookup.clear();
    partition_planscollectionlookup.clear();
    partition_tensoranchors.clear();

    START_TIMING
    generate_bitvectors(part);
    END_TIMING_GREEN("PartitionSearch::BitmaskGeneration")

    // Generate Single Layer Plans (Corrently assumes no DMA Insertion on Weight
    // and Biases)
    START_TIMING
    if (not(this->disable_stationary))
      single_calculate_weight_stationary(part);
    if (not(this->disable_weight_streaming))
      single_calculate_weight_streaming(part);
    culling_partial_plans(SIZE_UNLIMITED, &model);
    END_TIMING_GREEN("PartitionSearch::SingleNodePlanCreation")

#if ENABLE_LOGGING
    std::cout << std::endl;
    std::cout << GREEN << "Single Node Partial Plans: " << RESET << std::endl;
    print_partial_plans(partition_partialplanscollectionlookup);
#endif

    if (not(this->disable_cascade)) {
      // Generate Tensor Anchors around whch we build the cascades
      START_TIMING
      generate_tensor_anchors(part);
      END_TIMING_GREEN("PartitionSearch::GenerateTensorAnchors")

#if ENABLE_LOGGING
      print_tensor_anchors(partition_tensoranchors);
#endif

      START_TIMING
      bool all_anchors_closed = false;
      uint64_t iterations = 0;
      do {
        all_anchors_closed = grow_plans(part);
        culling_partial_plans(SIZE_5, &model);
        iterations++;
#if ENABLE_LOGGING
        std::cout << RED << "Growth Iteration: " << RESET << iterations
                  << std::endl;
#endif
      } while ((iterations < MAX_NO_GROWTH_ITERATIONS) and
               (not(all_anchors_closed)));
      END_TIMING_GREEN("PartitionSearch::Growing Partial Plans")
    }

#if ENABLE_LOGGING
    std::cout << std::endl;
    std::cout << GREEN << "Post Search Partial Plans: " << RESET << std::endl;
    print_partial_plans(partition_partialplanscollectionlookup);
#endif

    /*
       Go through the partial set and close all the partial plans
       ** Plans & Partial Plans will have updated SRAM at this point
          insert any needed re-writes as we close the plans
       ** Also calculate the IFM-OFM Overlap where possible
     */

    START_TIMING
    close_plans(part);
    culling_plans(SIZE_1, &model);
    END_TIMING_GREEN("PartitionSearch::ClosePlans")

#if ENABLE_LOGGING
    std::cout << std::endl;
    std::cout << GREEN << "Closed Plans: " << RESET << std::endl;
    print_plans(partition_planscollectionlookup);
#endif

    /* For the partition piece together a solution and setup the needed Tensor
     * and DMA Insertion Re-Writes*/
    std::vector<BitVector> solution;
    START_TIMING
    solution = construct_partition_solution(part);
    END_TIMING_GREEN("PartitionSearch::ConstructSolution")

#if ENABLE_LOGGING
    print_bit_candidate(solution);
#endif
  }
#if (ENABLE_LOGGING | ENABLE_TIMING)
  std::cout << std::endl;
#endif
  END_TIMING("NetworkSearch")

#if (ENABLE_LOGGING || ENABLE_LOGGING_FINAL_PLANS)
  printPlans(final_plans);
  printPlansCascades(final_plans);
#endif

  // Generate the final Cascade Objects & Apply needed tensor re-writes
  START_TIMING
  generate_cascades();
  END_TIMING("GenerateCascades")

  LOG(ENABLE_LOGGING_SEARCH_ENGINE_VERBOSE_LEVEL_1,
      "SearchEngine::search_graph()::End");
}

/******************************************************************************/
/*                            SEARCH                                          */
/******************************************************************************/

unsigned SearchEngine::search() {
  LOG(ENABLE_LOGGING_SEARCH_ENGINE_VERBOSE_LEVEL_1,
      "LOGGING_SEARCH_ENGINE::Start ");

  // 0. Validate that the Block is made up of just Kernels
  llvm::iplist<Operation>::iterator i;
  for (i = block->begin(); i != block->end(); i++) {
    if (llvm::isa<kernelir::KernelRegionOp>(i))
      continue;
    else if (llvm::isa<scheduleir::YieldOp>(i))
      continue;
    else if (llvm::isa<scheduleir::CastInOp>(i))
      continue;
    else if (llvm::isa<scheduleir::CastOutOp>(i))
      continue;
    else
      ASSERT_COND(
          true,
          "SearchEngine::search::Block Should only contain KernelRegionOp");
  }

  PLANS_COLLECTION final_plans;

  search_graph();

  LOG(ENABLE_LOGGING_SEARCH_ENGINE_VERBOSE_LEVEL_1,
      "LOGGING_SEARCH_ENGINE::End ");
  return 0;
}

} // namespace planner
} // namespace mlir

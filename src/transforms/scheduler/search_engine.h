/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#ifndef SEARCH_ENGINE_H
#define SEARCH_ENGINE_H

#include "mlir/Dialect/Quant/QuantOps.h"
#include "mlir/Dialect/Quant/QuantTypes.h"
#include "mlir/Dialect/StandardOps/IR/Ops.h"      // from @llvm-project
#include "mlir/IR/AffineMap.h"                    // from @llvm-project
#include "mlir/IR/Attributes.h"                   // from @llvm-project
#include "mlir/IR/BlockAndValueMapping.h"         // from @llvm-project
#include "mlir/IR/Builders.h"                     // from @llvm-project
#include "mlir/IR/Location.h"                     // from @llvm-project
#include "mlir/IR/MLIRContext.h"                  // from @llvm-project
#include "mlir/IR/Operation.h"                    // from @llvm-project
#include "mlir/IR/PatternMatch.h"                 // from @llvm-project
#include "mlir/Interfaces/InferTypeOpInterface.h" // from @llvm-project
#include "mlir/Pass/Pass.h"                       // from @llvm-project
#include "mlir/Transforms/DialectConversion.h"    // from @llvm-project

#include "llvm/Support/ThreadPool.h"

#include "src/ir/cascade_ir.h"
#include "src/ir/kernel_ir.h"
#include "src/ir/sched_tensor/SchedTensorType.h"

#include "src/utils/assert.h"
#include "src/utils/bitvector.h"
#include "src/utils/colors.h"
#include "src/utils/ir_utils.h"
#include "src/utils/logging.h"
#include "src/utils/timing.h"

#include "src/transforms/kernel_packing/kernelInfo.h"

#include "src/transforms/memory_organizer/GreedyAllocator.h"
#include "src/transforms/memory_organizer/HillClimbSearchAllocator.h"
#include "src/transforms/memory_organizer/LinearAllocator.h"
#include "src/transforms/memory_organizer/LiveRange.h"
#include "src/transforms/memory_organizer/LiveRangeGraph.h"
#include "src/transforms/memory_organizer/LivenessAnalysis.h"
#include "src/transforms/memory_organizer/PinTensors.h"

#include "src/transforms/scheduler/candidate.h"
#include "src/transforms/scheduler/cascadeInfo.h"
#include "src/transforms/scheduler/cost_model.h"
#include "src/transforms/scheduler/insert_dma.h"
#include "src/transforms/scheduler/partitions.h"
#include "src/transforms/scheduler/plan.h"
#include "src/transforms/scheduler/power_set.h"
#include "src/transforms/scheduler/shared_buffer_allocation/shared_buffer_allocation.h"
#include "src/transforms/scheduler/tensor_anchors.h"

#include <bits/stdc++.h>
#include <unordered_map>

using namespace std;
using namespace mlir::utils;

namespace mlir {
namespace planner {

struct op_pair {
  mlir::Operation *op;
  kernelInfo k;
};

class SearchEngine {
public:
  SearchEngine(Block &block,
               mlir::ArchitectureConfig::ArchitectureConfigAttr &arch_c,
               mlir::CompilerConfig::CompilerConfigAttr &compiler_c,
               std::vector<cascadeInfo> &cascades, MLIRContext *ctx);

  unsigned search();

private:
  SearchEngine();

  Block *block;
  mlir::ArchitectureConfig::ArchitectureConfigAttr arch_c;
  uint64_t sram_size;
  mlir::CompilerConfig::CompilerConfigAttr compiler_c;
  std::vector<cascadeInfo> *cascades;
  MLIRContext *context;
  value_mem_area search_mem_area;
  bool disable_cascade;
  bool disable_weight_streaming;
  bool disable_stationary;
  bool disable_block_config_search;
  NpuBlockConfig custom_block_config;

  void search_graph();
  PLANS_COLLECTION final_plans;

  std::unordered_map<Operation *, kernelInfo> adjacency_table;
  void print_adjacency_table(element e);
  void generate_adjacency_table(uint32_t block_config_limit = 16);
  friend void generate_adjacency_ops(SearchEngine *engine,
                                     std::vector<op_pair> &ops, int32_t start,
                                     int32_t end, uint32_t block_config_limit);

  std::vector<int64_t> mem_usage;
  std::vector<int64_t> non_local_mem_usage;
  std::vector<set<value_unique_id>> non_local_mem_usage_values;
  void calc_non_local_mem_usage();

  void find_suitable_block_configs(SharedBufferAllocation alloc,
                                   std::vector<NpuBlockConfig> &block_configs,
                                   uint32_t block_config_limit);
  void find_block_configs_suitable_for_kernel(
      Operation *op, std::vector<NpuBlockConfig> &block_configs,
      uint32_t block_config_limit);
  void generate_block_structs(SharedBufferAllocation &alloc,
                              std::vector<BlockStruct> &block_configs);
  SharedBufferAllocation
  create_shared_buffer_allocation_for_kernel_and_try_block_config(
      Operation *op, NpuBlockConfig block_config);

  uint64_t get_input_partition(partition &p);
  uint64_t get_output_partition(partition &p);
  uint64_t get_stationary_size(SchedTensorType t,
                               value_mem_area search_mem_area);
  bool operation_in_partition(partition p, Operation *op);
  void check_partition_endings(
      std::vector<partition> p_network,
      mlir::ArchitectureConfig::ArchitectureConfigAttr &arch_c);

  void generate_cascades();
  int32_t cascade_counter;

  SmallVector<int64_t, 2> rolling_buffer_dims_from_kernels(
      SchedTensorType tensor, unsigned tensor_depth, unsigned tensor_bits,
      Operation *kernel_1, FilterMetaData kernel_1_metadata,
      NpuBlockConfig block_config_ke1,
      NpuBlockType kernel_1_primay_op_block_type, Operation *kernel_2,
      FilterMetaData kernel_2_metadata, NpuBlockConfig block_config_ke2,
      NpuBlockType kernel_2_primay_op_block_type);

  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  // Scheduling search for this power set element
  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  BitVectorLookup partition_bitvectorlookup;

  PartialPlansCollectionLookup partition_partialplanscollectionlookup;
  PlansCollectionLookup partition_planscollectionlookup;

  TensorAnchors partition_tensoranchors;

  void generate_tensor_anchors(partition &p);
  void generate_bitvectors(partition &p);

  void single_calculate_weight_stationary(partition p);
  void single_calculate_weight_streaming(partition p);

  void culling_plans(int64_t size, cost_model *model);
  void culling_partial_plans(int64_t size, cost_model *model);

  bool grow_plans(partition &p);
  void close_plans(partition &p);

  std::vector<BitVector> construct_partition_solution(partition &p);

  // Placed in Class Interface all use adjacency table
  uint64_t ExtractPartialPlanFootprint(partition &part,
                                       BitVector &plan_bit_vector);
  SmallVector<value_unique_id> getPartialPlanInputs(partition &p,
                                                    BitVector &plan_bit_vector);
  SmallVector<value_unique_id>
  getPartialPlanOutputs(partition &p, BitVector &plan_bit_vector);
  void merge_partial_plans(partition &p, TensorAnchor &ts,
                           BitVector producer_bitvector,
                           PARTIAL_PLANS_COLLECTION &producer_partial_plans,
                           BitVector consumer_bitvector,
                           PARTIAL_PLANS_COLLECTION &consumer_partial_plans,
                           BitVector merged_bitvector,
                           PARTIAL_PLANS_COLLECTION &merged_partial_plans);
  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  llvm::ThreadPool thread_pool;
};

bool swap_block_config(NpuBlockConfig &p1, NpuBlockConfig &p2);

} // namespace planner
} // namespace mlir

#endif // SEARCH_ENGINE_H

/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#include "src/transforms/scheduler/shared_buffer_allocation/shared_buffer_allocation.h"
#include "src/transforms/scheduler/search_engine.h"

namespace mlir {
namespace planner {

bool is_compatible(SharedBufferAllocation &first,
                   SharedBufferAllocation &second) {
  // See if the bank allocations of two convolutions are compatible
  // so that they can run back-to-back without a fence in between
  std::set<SharedBufferArea> first_set = {SharedBufferArea::OFM,
                                          SharedBufferArea::ACCUMULATORS};
  std::set<SharedBufferArea> second_set = {SharedBufferArea::IFM,
                                           SharedBufferArea::WEIGHTS};
  std::vector<bool> first_mask = first.generate_used_mask(first_set);
  std::vector<bool> second_mask = second.generate_used_mask(second_set);
  for (uint32_t i = 0; i < first_mask.size(); i++) {
    bool cond = (first_mask[i] & second_mask[i]);
    if (cond)
      return false;
  }
  return true;
}

bool _all_fms_have_quant(SmallVector<Value, 1> ifm_tensors,
                         SmallVector<Value, 1> ofm_tensors) {
  true;
  // Mixed Precision Networks not yet supported
}

SharedBufferAllocation shared_buffer_allocation_for_kernel(
    mlir::ArchitectureConfig::ArchitectureConfigAttr arch, Operation *op) {
  // std::cout << "shared_buffer_allocation_for_kernel::Start" << std::endl;

  SmallVector<Value, 1> ifm_tensors =
      mlir::kernelir::ExtractKernelInputValues(op);
  SmallVector<Value, 1> ofm_tensors =
      mlir::kernelir::ExtractKernelOutputValues(op);

  ASSERT_COND((ifm_tensors.size() == 0),
              "shared_buffer_allocation_for_kernel ifm_tensors size == 0");
  ASSERT_COND((ofm_tensors.size() == 0),
              "shared_buffer_allocation_for_kernel ofm_tensors size == 0");

  bool uses_lut = false;
  NpuBlockType npu_block_type = mlir::kernelir::getKernelBlockType(op);
  ArrayRef<int64_t> ofm_shape =
      ofm_tensors[0].getType().dyn_cast<mlir::SchedTensorType>().getShape();
  bool all_fms_have_quant = _all_fms_have_quant(ifm_tensors, ofm_tensors);

  bool is_elementwise = (npu_block_type == ElementWise);

  FilterMetaData filter = mlir::kernelir::getKernelFilter(op);

  mlir::SchedTensorType ifm_type_0 =
      ifm_tensors[0].getType().dyn_cast<mlir::SchedTensorType>();

  resampling_mode ifm_resampling_mode = ifm_type_0.get_resampling_mode();
  int32_t ifm_bits = getBitDepthType(ifm_type_0);
  ArrayRef<int64_t> ifm_shape = ifm_type_0.getShape();

  ASSERT_COND((ifm_shape.size() == 0),
              "shared_buffer_allocation_for_kernel ifm_shape size != 4");
  int32_t ifm_depth = ifm_shape[3];

  int32_t ifm_count = 1;
  if (is_elementwise) {
    ifm_count = 2;
    if (ifm_tensors.size() <= 1)
      ifm_count = 1;
  }

  // std::cout << "shared_buffer_allocation_for_kernel::End" << std::endl;
  return SharedBufferAllocation(arch, filter, uses_lut, npu_block_type,
                                all_fms_have_quant, ifm_resampling_mode,
                                ifm_bits, ifm_depth, ifm_count, ofm_shape);
}

/*************************************************************************************************/

void SearchEngine::generate_block_structs(
    SharedBufferAllocation &alloc,
    std::vector<BlockStruct> &block_structs_range) {
  // Constrain the search space if the OFM is smaller than the max block size
  // - Add other block search constraints here if required
  uint64_t max_block_height, max_block_width, max_block_depth;
  if (alloc.ofm_shape.size() == 2)
    max_block_height = max_block_width = alloc.ofm_shape[0];
  else {
    max_block_width = alloc.ofm_shape[alloc.ofm_shape.size() - 2];
    max_block_height = alloc.ofm_shape[alloc.ofm_shape.size() - 3];
  }
  // Common block depth
  max_block_depth = alloc.ofm_shape[alloc.ofm_shape.size() - 1];

  BlockStruct ofm_block_max = get_ofm_block_max();

  AcceleratorConfig accel_config =
      (AcceleratorConfig)(this->arch_c.accel_config().getInt());
  HWConfig hw_config = get_acclerator_config(accel_config);

  // Constrain to valid ranges before search
  max_block_width = min(ofm_block_max.width, max_block_width);
  max_block_height = min(ofm_block_max.height, max_block_height);
  max_block_depth = min(ofm_block_max.depth, max_block_depth);

  int32_t OFMSplitDepth = arch_c.OFMSplitDepth().getInt();

  // Try a range of block shapes against this pass
  for (uint64_t w = hw_config.ofm_ublock.width;
       w < (max_block_width + hw_config.ofm_ublock.width);
       w += hw_config.ofm_ublock.width) {
    for (uint64_t h = hw_config.ofm_ublock.height;
         h < (max_block_height + hw_config.ofm_ublock.height);
         h += hw_config.ofm_ublock.height) {
      // Try valid OFM block depths
      for (uint64_t c = hw_config.ofm_ublock.depth;
           c < (max_block_depth + hw_config.ofm_ublock.depth);
           c += hw_config.ofm_ublock.depth) {
        // OFM block depth has the constraint that if it causes the OFM to be
        // split, it must be a multiple of the OFM split size
        if ((c >= max_block_depth) or
            (c < max_block_depth and ((c % OFMSplitDepth) == 0))) {
          BlockStruct block(w, h, c);
          block_structs_range.push_back(block);
        }
      }
    }
  }
}

bool swap_block_struct(BlockStruct &p1, BlockStruct &p2) {
  /*
     Returns true if the first argument goes before the second argument in the
     strict weak ordering it defines, and false otherwise. This shall be a
     function pointer or a function object.
   */
  int64_t p1_cost = (((p1.width * p1.height) << 8) | p1.depth);
  int64_t p2_cost = (((p2.width * p2.height) << 8) | p2.depth);
  if (p1_cost < p2_cost)
    return true;
  else
    return false;
}

bool swap_block_config(NpuBlockConfig &p1, NpuBlockConfig &p2) {
  /*
     Returns true if the first argument goes before the second argument in the
     strict weak ordering it defines, and false otherwise. This shall be a
     function pointer or a function object.
   */
  int64_t p1_cost = (((p1[0] * p1[1]) << 8) | p1[3]);
  int64_t p2_cost = (((p2[0] * p2[1]) << 8) | p2[3]);
  if (p1_cost < p2_cost)
    return true;
  else
    return false;
}

void SearchEngine::find_suitable_block_configs(
    SharedBufferAllocation alloc, std::vector<NpuBlockConfig> &block_configs,
    uint32_t block_config_limit) {
  if (this->disable_block_config_search) {
    BlockStruct block;
    block.height = this->custom_block_config[0];
    block.width = this->custom_block_config[1];
    block.depth = this->custom_block_config[2];
    NpuBlockConfig def = alloc.try_to_allocate(block);
    if (def.size() == 0) {
      std::cout
          << "Fatal "
             "Error::SearchEngine::find_block_configs_suitable_for_pass_and_"
             "shared_buffer::Custom Block does not fit for this Operator"
          << std::endl;
      exit(1);
    }
    block_configs.push_back(this->custom_block_config);
  }

  std::vector<BlockStruct> block_structs_range;
  generate_block_structs(alloc, block_structs_range);

  // Sort them block_configs_range so we only test what we need
  std::sort(block_structs_range.begin(), block_structs_range.end(),
            swap_block_struct);

  int32_t begin = 0;
  int32_t end = block_structs_range.size() - 1;
  int32_t next = begin;
  // std::cout << begin << " " << end << " " << next << std::endl;

  bool finished = true;
  bool toggle = true;
  do {
    // std::cout << toggle << " " << next << "->" <<
    // block_structs_range[next].width << " " <<
    // block_structs_range[next].height
    // << " " << block_structs_range[next].depth << std::endl;
    NpuBlockConfig def = alloc.try_to_allocate(block_structs_range[next]);
    if (def.size() != 0)
      block_configs.push_back(def);

    if (toggle) {
      next = begin;
      begin = begin + 1;
      toggle = false;
    } else {
      next = end;
      end = end - 1;
      toggle = true;
    }

    if (begin == end)
      finished = false;
    if (block_configs.size() == (block_config_limit * 2))
      finished = false;
    if ((next < 0) or (next >= block_structs_range.size()))
      finished = false;
  } while (finished);

  if (block_configs.size() == 0)
    FATAL_ERROR("Fatal "
                "Error::SearchEngine::find_block_configs_suitable_for_pass_"
                "and_shared_buffer::No Block Configs generated that can fit "
                "this Operator");
}

void SearchEngine::find_block_configs_suitable_for_kernel(
    Operation *op, std::vector<NpuBlockConfig> &block_configs,
    uint32_t block_config_limit) {
  SharedBufferAllocation alloc =
      shared_buffer_allocation_for_kernel(arch_c, op);
  return find_suitable_block_configs(alloc, block_configs, block_config_limit);
}

SharedBufferAllocation
SearchEngine::create_shared_buffer_allocation_for_kernel_and_try_block_config(
    Operation *op, NpuBlockConfig block_config) {
  SharedBufferAllocation alloc =
      shared_buffer_allocation_for_kernel(arch_c, op);
  auto block_config_generated = alloc.try_to_allocate(
      BlockStruct(block_config[1], block_config[0], block_config[3]));
  if (block_config_generated.size() == 0)
    FATAL_ERROR("create_shared_buffer_allocation_for_kernel_and_try_block_"
                "config cannot allocate");
  return alloc;
}

} // namespace planner
} // namespace mlir

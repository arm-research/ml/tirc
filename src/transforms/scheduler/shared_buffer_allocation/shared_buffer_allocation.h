/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#ifndef SHARED_BUFFER_ALLOCATION_H
#define SHARED_BUFFER_ALLOCATION_H

#include "src/utils/assert.h"
#include "src/utils/data_structure_utils.h"

#include "src/ir/architecture_config/architecture_config.h"

#include <set>
using namespace std;
using namespace mlir::utils;

namespace mlir {
namespace planner {

class SharedBufferAllocation {
public:
  SharedBufferAllocation(mlir::ArchitectureConfig::ArchitectureConfigAttr arch,
                         FilterMetaData filter, bool uses_lut,
                         NpuBlockType npu_block_type, bool all_fms_have_quant,
                         resampling_mode ifm_resampling_mode, int32_t ifm_bits,
                         int32_t ifm_depth, int32_t ifm_count,
                         ArrayRef<int64_t> ofm_shape) {
    // std::cout << "SharedBufferAllocation()::Start" << std::endl;
    this->arch = arch;

    for (uint32_t i = 0; i < uint32_t(SharedBufferArea::SIZE); i++) {
      shared_buffer_bank_locations.push_back(0);
      shared_buffer_banks_required.push_back(0);
    }

    this->filter = filter;

    this->is_elementwise = (npu_block_type == NpuBlockType::ElementWise);
    this->uses_lut = uses_lut;
    this->shared_buffer_ifm_count = ifm_count;

    this->is_equal_depth_op =
        is_elementwise or
        (npu_block_type == NpuBlockType::ConvolutionDepthWise) or
        (npu_block_type == NpuBlockType::Pooling);

    this->shared_buffer_use_accumulator_element = SHRAMElements::Acc32;
    if (is_elementwise)
      this->shared_buffer_use_ifm_element = SHRAMElements::IFM8_Elementwise;
    else
      this->shared_buffer_use_ifm_element = SHRAMElements::IFM8;

    this->ifm_resampling_mode = ifm_resampling_mode;
    this->ifm_bits = ifm_bits;
    this->ifm_depth = ifm_depth;

    if (this->ifm_bits == 16) {
      if ((npu_block_type != NpuBlockType::Pooling) and all_fms_have_quant)
        this->shared_buffer_use_accumulator_element = SHRAMElements::Acc40;
      this->shared_buffer_use_ifm_element =
          SHRAMElements(uint32_t(this->shared_buffer_use_ifm_element) + 1);
    } else if (this->ifm_bits == 32) {
      this->shared_buffer_use_ifm_element = SHRAMElements::IFM32;
    } else
      ASSERT_COND(this->ifm_bits != 8,
                  "SharedBufferAllocation::Unexpected IFM bitdepth");

    this->ifm_block_depth =
        calc_ifm_block_depth(this->arch, this->ifm_depth, this->ifm_bits);

    this->ofm_shape = ofm_shape;

    shared_buffer_banks_required[int32_t(SharedBufferArea::WEIGHTS)] =
        arch.shram_reserved_weight_banks().getInt();
    shared_buffer_banks_required[int32_t(SharedBufferArea::OFM)] =
        arch.shram_reserved_output_banks().getInt();
    // std::cout << "SharedBufferAllocation()::End" << std::endl;
  }

  NpuBlockConfig try_to_allocate(BlockStruct ofm_block) {
    // std::cout << "Try to Allocate Block: <" <<ofm_block.height << " " <<
    // ofm_block.width << " " << ofm_block.depth << ">" << std::endl;
    // Get IFM block configuration
    uint32_t ifm_block_depth_2 =
        (is_equal_depth_op ? ofm_block.depth : this->ifm_block_depth);
    BlockStruct ifm_block = get_ifm_block_size(
        arch, ifm_block_depth_2, ofm_block, filter, ifm_resampling_mode);

    SHRAMBlockConfig ifm_config = get_block_config(
        arch, ifm_block.width, ifm_block.height, ifm_block.depth);

    // Get OFM block configuration
    SHRAMBlockConfig ofm_config = get_block_config(
        arch, ofm_block.width, ofm_block.height, ofm_block.depth);

    uint32_t acc_banks =
        ofm_config.banks[shared_buffer_use_accumulator_element];

    // 0. Weights and OFM Do not change based on the Block Shape Selected
    // 1. Set bank counts for IFM and Accumulator
    shared_buffer_banks_required[int32_t(SharedBufferArea::IFM)] =
        (ifm_config.banks[shared_buffer_use_ifm_element] *
         this->shared_buffer_ifm_count);
    shared_buffer_banks_required[int32_t(SharedBufferArea::ACCUMULATORS)] =
        (this->is_elementwise ? 0 : acc_banks);

    // std::cout << "Allocated: <" <<ofm_block.height << " " << ofm_block.width
    // << " " << ifm_block.depth << " " << ofm_block.depth << ">" << std::endl;

    // Validating calculates bank layout and returns validity
    if (is_valid())
      return {(int)ofm_block.height, (int)ofm_block.width, (int)ifm_block.depth,
              (int)ofm_block.depth};
    else
      return {};
  }

  bool is_valid() {
    // Assign zero-based bank starts (first element remains zero)
    /*
    self.bank_locations[1:] = np.cumsum(self.banks_required)[:-1]
     */

    std::vector<int32_t> cumalitive_sum;
    int32_t cum_sum = shared_buffer_banks_required[0];
    cumalitive_sum.push_back(cum_sum);
    for (int i = 1; i < shared_buffer_banks_required.size(); i++) {
      cum_sum += shared_buffer_banks_required[i];
      cumalitive_sum.push_back(cum_sum);
    }
    for (int i = 1; i < shared_buffer_banks_required.size(); i++)
      shared_buffer_bank_locations[i] = cumalitive_sum[i - 1];

    uint32_t asb = available_shram_banks(arch, uses_lut);

    // Accumulator area is measured from the end of the buffer
    shared_buffer_bank_locations[(int32_t)SharedBufferArea::ACCUMULATORS] =
        (asb -
         shared_buffer_banks_required[(int32_t)SharedBufferArea::ACCUMULATORS]);
    int32_t ifm_end =
        shared_buffer_bank_locations[(int32_t)SharedBufferArea::IFM] +
        shared_buffer_banks_required[(int32_t)SharedBufferArea::IFM];

    // printVector(shared_buffer_banks_required); // [2,0,0,16,] -> [ 2.  0.
    // 8. 16.] printVector(shared_buffer_bank_locations); // [0,2,2,30,] -> [ 0.
    // 2.  2. 30.] std::cout << "ifm_end: " << ifm_end << " " <<
    // shared_buffer_bank_locations[(int32_t)SharedBufferArea::ACCUMULATORS] <<
    // std::endl; exit(1);
    return (
        ifm_end <=
        shared_buffer_bank_locations[(int32_t)SharedBufferArea::ACCUMULATORS]);
  }

  std::vector<bool> generate_used_mask(std::set<SharedBufferArea> active_set) {
    std::vector<bool> mask;
    for (int i = 0; i < arch.shram_total_banks().getInt(); i++)
      mask.push_back(false);
    for (auto kind : active_set) {
      uint32_t start = shared_buffer_bank_locations[(int)kind];
      uint32_t end = start + int(shared_buffer_banks_required[(int)kind]);
      for (int i = start; i < end; i++)
        mask[i] = true;
    }
    return mask;
  }

  mlir::ArchitectureConfig::ArchitectureConfigAttr arch;

  FilterMetaData filter;
  bool uses_lut;
  NpuBlockType npu_block_type;
  bool all_fms_have_quant;
  resampling_mode ifm_resampling_mode;
  int32_t ifm_bits;
  int32_t ifm_depth;

  ArrayRef<int64_t> ofm_shape;
  int32_t ifm_block_depth;
  bool is_equal_depth_op;

  SmallVector<int32_t, 1> shared_buffer_bank_locations;
  SmallVector<int32_t, 1> shared_buffer_banks_required;
  SHRAMElements shared_buffer_use_accumulator_element;
  SHRAMElements shared_buffer_use_ifm_element;
  int32_t shared_buffer_ifm_count;

  bool is_elementwise;

private:
  SharedBufferAllocation();
};

bool is_compatible(SharedBufferAllocation &first,
                   SharedBufferAllocation &second);

} // namespace planner
} // namespace mlir

#endif // SHARED_BUFFER_ALLOCATION_H

/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#ifndef TENSOR_ANCHORS_H
#define TENSOR_ANCHORS_H

#include "src/transforms/scheduler/partitions.h"

#include "src/utils/bitvector.h"

#include "mlir/Dialect/Quant/QuantOps.h"
#include "mlir/Dialect/Quant/QuantTypes.h"
#include "mlir/Dialect/StandardOps/IR/Ops.h"      // from @llvm-project
#include "mlir/IR/AffineMap.h"                    // from @llvm-project
#include "mlir/IR/Attributes.h"                   // from @llvm-project
#include "mlir/IR/BlockAndValueMapping.h"         // from @llvm-project
#include "mlir/IR/Builders.h"                     // from @llvm-project
#include "mlir/IR/Location.h"                     // from @llvm-project
#include "mlir/IR/MLIRContext.h"                  // from @llvm-project
#include "mlir/IR/Operation.h"                    // from @llvm-project
#include "mlir/IR/PatternMatch.h"                 // from @llvm-project
#include "mlir/Interfaces/InferTypeOpInterface.h" // from @llvm-project
#include "mlir/Pass/Pass.h"                       // from @llvm-project
#include "mlir/Transforms/DialectConversion.h"    // from @llvm-project

namespace mlir {
namespace planner {

class TensorAnchor {
public:
  TensorAnchor(mlir::Value v) {
    this->v = v;
    this->t = v.getType().dyn_cast<mlir::SchedTensorType>();
    this->id = this->t.getUniqueId();

    this->tensor_depth = this->t.getShape()[3];
    this->tensor_bits = getBitDepthType(this->t);
  };

  void set_consumers(std::list<BitVector> &consumers) {
    this->consumers = consumers;
  };
  void set_producers(std::list<BitVector> &producers) {
    this->producers = producers;
  };

  void add_consumer(BitVector consumer) {
    this->consumers.push_back(consumer);
  };
  void add_producer(BitVector producer) {
    this->producers.push_back(producer);
  };

  std::list<BitVector> get_consumers() { return consumers; }

  std::list<BitVector> get_producers() { return producers; }

  value_unique_id get_id() { return id; };
  mlir::SchedTensorType get_SchedTensorType() { return t; };
  mlir::Value get_Value() { return v; };
  unsigned get_tensor_depth() { return tensor_depth; };
  unsigned get_tensor_bits() { return tensor_bits; };

private:
  TensorAnchor();

  std::list<BitVector> producers;
  std::list<BitVector> consumers;

  value_unique_id id;
  mlir::Value v;
  mlir::SchedTensorType t;

  unsigned tensor_depth;
  unsigned tensor_bits;
};

using TensorAnchors = std::unordered_map<value_unique_id, TensorAnchor>;

void print_tensor_anchors(TensorAnchors tensoranchors);

} // namespace planner
} // namespace mlir

#endif // TENSOR_ANCHORS_H

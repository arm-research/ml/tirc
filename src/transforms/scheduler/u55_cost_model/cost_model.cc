/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#include "src/transforms/scheduler/u55_cost_model/cost_model.h"
#include "src/transforms/scheduler/cost_metrics.h"
#include "src/transforms/scheduler/u55_cost_model/cost_model_util.h"

#include "src/utils/printIR.h"

namespace mlir {
namespace planner {

value_rewrite search_rewrite(std::vector<value_rewrite> &rewrites,
                             mlir::SchedTensorType tens_t) {
  value_rewrite vr;
  vr.insert_dma = false;
  for (auto v : rewrites)
    if (tens_t.getUniqueId() == v.type.getUniqueId())
      return v;
  return vr;
}

void add_tensor_bandwidth_read(
    cost_metrics &kernel_metrics, BANDWIDTH_ARRAY &scaled_bws,
    mlir::SchedTensorType tens_t, double multiple,
    mlir::ArchitectureConfig::ArchitectureConfigAttr &arch_c,
    BlockStruct ofm_block) {
  double bw = tensor_bandwidth(tens_t) * multiple;
  kernel_metrics.bw[tens_t.get_mem_area()][tens_t.get_purpose()][bd_Read] += bw;
  scaled_bws[tens_t.get_mem_area()][tens_t.get_purpose()][bd_Read] += bw;
  /*
      estimate_memory_transfer_efficiency(arch_c, tens_t.get_mem_area(),
                                          bd_Read, tens_t, ofm_block);
   */
}

void add_tensor_bandwidth_write(
    cost_metrics &kernel_metrics, BANDWIDTH_ARRAY &scaled_bws,
    mlir::SchedTensorType tens_t, double multiple,
    mlir::ArchitectureConfig::ArchitectureConfigAttr &arch_c,
    BlockStruct ofm_block) {
  double bw = tensor_bandwidth(tens_t) * multiple;
  kernel_metrics.bw[tens_t.get_mem_area()][tens_t.get_purpose()][bd_Write] +=
      bw;
  scaled_bws[tens_t.get_mem_area()][tens_t.get_purpose()][bd_Write] += bw;
  /*
      estimate_memory_transfer_efficiency(arch_c, tens_t.get_mem_area(),
                                          bd_Read, tens_t, ofm_block);
   */
}

void add_tensor_bandwidth_sram_read(
    cost_metrics &kernel_metrics, BANDWIDTH_ARRAY &scaled_bws,
    mlir::SchedTensorType tens_t, double multiple,
    mlir::ArchitectureConfig::ArchitectureConfigAttr &arch_c,
    BlockStruct ofm_block) {
  double bw = tensor_bandwidth(tens_t) * multiple;
  kernel_metrics.bw[ma_Sram][tens_t.get_purpose()][bd_Read] += bw;
  scaled_bws[ma_Sram][tens_t.get_purpose()][bd_Read] += bw;
  /*
      estimate_memory_transfer_efficiency(arch_c, tens_t.get_mem_area(),
                                          bd_Read, tens_t, ofm_block);
  */
}

cost_metrics u55_cost_model::cost_metrics_for_kernel(
    Operation *op, mlir::ArchitectureConfig::ArchitectureConfigAttr &arch_c,
    std::unordered_map<Operation *, kernelInfo> &adjacency_table,
    NpuBlockConfig overwite_block_config, std::vector<value_rewrite> rewrites) {
  LOG(ENABLE_LOGGING_SEARCH_ENGINE_VERBOSE_LEVEL_1,
      "u55_cost_model::cost_metrics_for_kernel()::Start");

  Operation *kernel_op_major;
  mlir::utils::FilterMetaData filter;

  SmallVector<Value, 1> input_tensors;
  SmallVector<Value, 1> output_tensors;
  SmallVector<Value, 1> bias_tensors;
  SmallVector<Value, 1> intermediate_tensors;
  SmallVector<Value, 1> weight_tensors;

  mlir::SchedTensorType ifm_1_tensor;
  mlir::SchedTensorType ifm_2_tensor;
  mlir::SchedTensorType scale_tensor;
  mlir::SchedTensorType ofm_tensor;
  mlir::SchedTensorType weight_tensor;

  bool kernel_has_weight_tensor;
  bool kernel_has_bias_tensor;

  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  /* Stage 0: Parameter Extraction */
  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  cost_metrics kernel_metrics;
  if (not(llvm::isa<kernelir::KernelRegionOp>(*op)))
    return kernel_metrics;
  kernelir::KernelRegionOp kernel_op = dyn_cast<kernelir::KernelRegionOp>(op);

  if (adjacency_table.find(op) != adjacency_table.end()) {
    auto &entry = adjacency_table[op];

    kernel_op_major = entry.major_op;
    filter = entry.KernelFilter;

    input_tensors = entry.inputs;
    output_tensors = entry.outputs;
    bias_tensors = entry.bias;
    weight_tensors = entry.weights;

    ifm_1_tensor = entry.input_tensors[0];

    if (input_tensors.size() > 1)
      ifm_2_tensor = entry.input_tensors[1];

    if (bias_tensors.size() > 0)
      scale_tensor = entry.bias_tensors[0];

    ofm_tensor = entry.output_tensors[0];

    kernel_has_weight_tensor = (weight_tensors.size() > 0);
    kernel_has_bias_tensor = (bias_tensors.size() > 0);
    if (kernel_has_weight_tensor)
      weight_tensor = entry.weight_tensors[0];
  } else {
    if (adjacency_table.size() > 0)
      FATAL_ERROR("u55_cost_model::cost_metrics_for_kernel::Op not found in "
                  "Adjacency table and the size is not 0");

    kernel_op_major = getKernelMajorOp(kernel_op);
    filter = kernelir::getKernelFilter(op);

    input_tensors = mlir::kernelir::ExtractKernelInputValues(op);
    output_tensors = mlir::kernelir::ExtractKernelOutputValues(op);
    bias_tensors = mlir::kernelir::ExtractKernelBiasValues(op);
    weight_tensors = mlir::kernelir::ExtractKernelWeightValues(op);

    ifm_1_tensor = input_tensors[0].getType().dyn_cast<mlir::SchedTensorType>();

    if (input_tensors.size() > 1)
      ifm_2_tensor =
          input_tensors[1].getType().dyn_cast<mlir::SchedTensorType>();

    if (bias_tensors.size() > 0)
      scale_tensor =
          bias_tensors[0].getType().dyn_cast<mlir::SchedTensorType>();

    ofm_tensor = output_tensors[0].getType().dyn_cast<mlir::SchedTensorType>();

    kernel_has_weight_tensor = (weight_tensors.size() > 0);
    kernel_has_bias_tensor = (bias_tensors.size() > 0);
    if (kernel_has_weight_tensor)
      weight_tensor =
          weight_tensors[0].getType().dyn_cast<mlir::SchedTensorType>();
  }

  if (llvm::isa<scheduleir::MemoryOp>(kernel_op_major))
    return kernel_metrics;

  NpuBlockConfig block_config = overwite_block_config;
  if (overwite_block_config.size() == 0)
    block_config = ArrayAttr_to_SmallVector(kernel_op.block_config());
  if (block_config.size() == 0)
    FATAL_ERROR("u55_cost_model::cost_metrics_for_kernel::block_config size "
                "is zero");

  BlockStruct ofm_block(block_config[1], block_config[0], block_config[3]);
  BlockStruct ifm_block(block_config[1], block_config[0], block_config[3]);

  NpuBlockType npu_block_type = (NpuBlockType)kernel_op.block_type();

  /* Used for Cycle Calculation */
  ASSERT_COND(((ifm_1_tensor.get_format() != vf_NHWC) and
               (ifm_1_tensor.get_format() != vf_NHCWB16)),
              "u55_cost_model::cost_metrics_for_kernel::ifm_1_tensor_shape "
              "expected to be NHWC or NHWC16");

  if (input_tensors.size() > 1) {
    ASSERT_COND(((ifm_2_tensor.get_format() != vf_NHWC) and
                 (ifm_2_tensor.get_format() != vf_NHCWB16)),
                "u55_cost_model::cost_metrics_for_kernel::ifm_2_tensor_"
                "shape expected to be NHWC or NHWC16");
  }

  ASSERT_COND(((ofm_tensor.get_format() != vf_NHWC) and
               (ofm_tensor.get_format() != vf_NHCWB16)),
              "u55_cost_model::cost_metrics_for_kernel::ofm_tensor expected "
              "to be NHWC or NHWC16");

  SmallVector<int64_t, 4> ifm_1_tensor_shape =
      arrayref_2_smallvector(ifm_1_tensor.getShape());
  SmallVector<int64_t, 4> ofm_tensor_shape =
      arrayref_2_smallvector(ofm_tensor.getShape());
  SmallVector<int64_t, 4> weight_tensor_shape;

  if (kernel_has_weight_tensor) {
    ASSERT_COND(((weight_tensor.get_format() != vf_HWIO) and
                 (weight_tensor.get_format() != vf_HWOI)),
                "u55_cost_model::cost_metrics_for_kernel::Weight Tensor "
                "expected to be HWIO");
    weight_tensor_shape = arrayref_2_smallvector(weight_tensor.getShape());
    ASSERT_COND(weight_tensor_shape.size() != 4,
                "u55_cost_model::cost_metrics_for_kernel::Weight Tensor "
                "shape size is not equal to 4");
  } else
    weight_tensor_shape = {};

  ofm_block.depth = min(ofm_block.depth, (uint64_t)ofm_tensor.getShape()[3]);
  ofm_block.width = min(ofm_block.width, (uint64_t)ofm_tensor.getShape()[2]);
  ofm_block.height = min(ofm_block.height, (uint64_t)ofm_tensor.getShape()[1]);

  NpuBlockTraversal block_traversal;
  if (npu_block_type == NpuBlockType::ReduceSum)
    block_traversal = NpuBlockTraversal::tbt_DepthFirst;
  else if ((npu_block_type == NpuBlockType::ConvolutionMxN) ||
           (npu_block_type == NpuBlockType::ConvolutionDepthWise) ||
           (npu_block_type == NpuBlockType::VectorProduct))
    block_traversal = weight_tensor.get_NpuBlockTraversal();
  else
    block_traversal = NpuBlockTraversal::tbt_Default;

  int64_t ifm_block_depth = get_ifm_block_depth(
      npu_block_type, ifm_1_tensor.getShape()[3], getBitDepthType(ifm_1_tensor),
      block_traversal, ofm_block.depth);
  ifm_block = get_ifm_block_size(arch_c, ifm_block_depth, ofm_block, filter,
                                 ifm_1_tensor.get_resampling_mode());
  ifm_block.width = min(ifm_block.width, (uint64_t)ifm_1_tensor.getShape()[2]);
  ifm_block.height =
      min(ifm_block.height, (uint64_t)ifm_1_tensor.getShape()[1]);

#define ENABLE_COMPUTE_CALCULATION 1
#define ENABLE_BANDWIDTH_CALCULATION 1

#if ENABLE_COMPUTE_CALCULATION
  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  /* Stage 1: Calculate Cycles */
  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  LOG(ENABLE_LOGGING_SEARCH_ENGINE_VERBOSE_LEVEL_1,
      "u55_cost_model::cost_metrics_for_kernel()::Calculating Compute "
      "Cycles");

  if ((npu_block_type == NpuBlockType::ConvolutionMxN) or
      (npu_block_type == NpuBlockType::ConvolutionDepthWise) or
      (npu_block_type == NpuBlockType::VectorProduct) or
      (npu_block_type == NpuBlockType::Pooling) or
      (npu_block_type == NpuBlockType::ReduceSum)) {
    ifm_1_tensor_shape[1] =
        ifm_1_tensor_shape[1] + (filter.padding.top + filter.padding.bottom);
    ifm_1_tensor_shape[2] =
        ifm_1_tensor_shape[2] + (filter.padding.left + filter.padding.right);

    SmallVector<int64_t, 4> weight_tensor_bandwidth_shape;
    int64_t weight_tensor_element_size;
    double weight_tensor_bandwidth_compression_scale;

    int64_t nn_ops;

    if (npu_block_type != NpuBlockType::Pooling) {
      if (npu_block_type == NpuBlockType::ReduceSum) {
        weight_tensor_shape = {1, 1, ifm_1_tensor_shape[3],
                               ifm_1_tensor_shape[3]};
        weight_tensor_bandwidth_shape = {0, 0, 0, 0};
        weight_tensor_element_size = 0;
        weight_tensor_bandwidth_compression_scale = 0.0;
      } else {
        // For Vector product, weight format of IO is extended to HWIO, with
        // H=W=1
        weight_tensor_bandwidth_shape =
            arrayref_2_smallvector(weight_tensor.getBandwidthShape());
        weight_tensor_element_size = getNoBytesType(weight_tensor);
        weight_tensor_bandwidth_compression_scale =
            weight_tensor.get_bandwidth_compression_scale();
      }
      nn_ops = (int(int(ofm_tensor_shape[0]) * int(ofm_tensor_shape[1]) *
                    int(ofm_tensor_shape[2]) * int(weight_tensor_shape[0]) *
                    int(weight_tensor_shape[1]) * int(weight_tensor_shape[2]) *
                    int(weight_tensor_shape[3])));
    } else {
      weight_tensor_shape = {filter.height, filter.width, 1,
                             ifm_1_tensor_shape[3]};
      weight_tensor_bandwidth_shape = weight_tensor_shape;
      weight_tensor_element_size = 0;
      weight_tensor_bandwidth_compression_scale = 0.0;
      nn_ops = 0;
    }

    /*******************************************************************************************************/
    /* Calculate R/W ReWrites for certain tensors */
    /*******************************************************************************************************/
    ArrayRef<int64_t> kernel_dims = {weight_tensor_shape[0],
                                     weight_tensor_shape[1]};
    SmallVector<int64_t, 2> sub_kernel_limits =
        get_sub_kernel_limits(npu_block_type);
    // Count the sub kernels in case of kernel decomposition e.g 9x9; the IFM
    // block needs to be refetched for each of them
    int64_t n_sub_kernels_y =
        round_up_divide<int64_t>(kernel_dims[0], sub_kernel_limits[0]);
    int64_t n_sub_kernels_x =
        round_up_divide<int64_t>(kernel_dims[1], sub_kernel_limits[1]);
    int64_t n_sub_kernels = (n_sub_kernels_y * n_sub_kernels_x);
    int64_t n_full_depth_stages = round_up_divide<int64_t>(
        weight_tensor_bandwidth_shape[3], ofm_block.depth);
    if ((npu_block_type == NpuBlockType::ConvolutionDepthWise) or
        (npu_block_type == NpuBlockType::Pooling))
      n_full_depth_stages = 1; // force to no reread
    if (npu_block_type == NpuBlockType::ElementWise)
      kernel_metrics.ifm_read_multiple = 1;
    else
      kernel_metrics.ifm_read_multiple = (n_sub_kernels * n_full_depth_stages);
    // For every Block in the Output we need to re-read the weights
    if (kernel_has_weight_tensor)
      kernel_metrics.weight_read_multiple =
          round_up_divide<int64_t>(ofm_tensor_shape[1], ofm_block.height) *
          round_up_divide<int64_t>(ofm_tensor_shape[2], ofm_block.width);
    /*******************************************************************************************************/

    kernel_metrics.mac[mc_NeuralNetworkMacs] = nn_ops;

    // Calls also the estimate_output_cycles
    estimate_conv_pooling_cycles(
        arch_c, adjacency_table, npu_block_type, kernel_op, ifm_block,
        ofm_block, block_traversal, filter, ifm_1_tensor, ofm_tensor,
        scale_tensor, kernel_metrics.cycles[Cycles::c_Npu],
        kernel_metrics.mac[mc_HardwareMacs]);
  } else if (npu_block_type == NpuBlockType::ElementWise) {
    // Add Operation when the output unit is used in standalone model
    bool use_acc_40bits = false;
    int64_t num_elems = ofm_tensor.getNumElements();
    estimate_output_cycles(arch_c, adjacency_table, npu_block_type, kernel_op,
                           ofm_tensor, ifm_1_tensor, ifm_2_tensor, ofm_block,
                           use_acc_40bits, num_elems,
                           kernel_metrics.cycles[Cycles::c_Npu_ElementWise]);
    kernel_metrics.mac[mc_HardwareMacs] = 0;

#if ENABLE_BLK_SIZE_MINIMIZATION
    // We add a penalty for ecery block we need to process. Since the
    // weight_read_multiple will be 0 here, we stil would like to choose the
    // bloch shape that gives us the least amount of block iterations. 100 is
    // chosen speculaively at the moment.
    auto ofm_tens_shape = ofm_tensor.getShape();
    double num_ofm_blk =
        (round_up_divide<uint64_t>(ofm_tens_shape[1], ofm_block.height) *
         round_up_divide<uint64_t>(ofm_tens_shape[2], ofm_block.width) *
         round_up_divide<uint64_t>(ofm_tens_shape[3], ofm_block.depth));
    kernel_metrics.cycles[Cycles::c_Npu_ElementWise] += (num_ofm_blk * 100);
#endif
  }
#endif

  BANDWIDTH_ARRAY scaled_bws;
  reset_bandwidth_array(scaled_bws);

#if ENABLE_BANDWIDTH_CALCULATION
  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  /* Stage 2: Calculate Bandwidth applying Rewrites if needed */
  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  LOG(ENABLE_LOGGING_SEARCH_ENGINE_VERBOSE_LEVEL_1,
      "u55_cost_model::cost_metrics_for_kernel()::Calculating Bandwidth "
      "Cycles");

  /*
     0. kenel_metrics.bw are the actual bandwidth footprint (bytes) of moving a
     tensor in and out ot a mem_area. Rolling Buffers do not really matter here
     as the tensor needs to move in its entirity anyway
     1. scaled_bw are what ae used to calculate the cycles so update summary
     needs to take these as bw and theseneed t consider how many cycles we loose
     because of inefficeinces in the memory system
   */

  /* ReWrites do not need to be applied to the IFM as currently no DMA Ops are
   * added to them */

  // 2. Calculate all Bandwidths for tensors
  //     A. Adds Bandwidth reading from the Input Tensors
  add_tensor_bandwidth_read(kernel_metrics, scaled_bws, ifm_1_tensor,
                            kernel_metrics.ifm_read_multiple, arch_c,
                            ofm_block);
  if (input_tensors.size() > 1)
    add_tensor_bandwidth_read(kernel_metrics, scaled_bws, ifm_2_tensor, 1.0,
                              arch_c, ofm_block);

  //     B. Adds Bandwidth reading from the Output Tensors
  add_tensor_bandwidth_write(kernel_metrics, scaled_bws, ofm_tensor, 1.0,
                             arch_c, ofm_block);

  //     D. Adds Bandwidth reading for Weights:
  if (kernel_has_weight_tensor) {
    float weight_read_multiple = kernel_metrics.weight_read_multiple;
    // Check if we are adding a DMA with this
    auto vr_weight = search_rewrite(rewrites, weight_tensor);
    if (vr_weight.insert_dma)
      add_tensor_bandwidth_sram_read(kernel_metrics, scaled_bws, weight_tensor,
                                     weight_read_multiple, arch_c, ofm_block);
    // Add Secondary Storage
    add_tensor_bandwidth_read(kernel_metrics, scaled_bws, weight_tensor, 1.0,
                              arch_c, ofm_block);
  }

  //     E. Adds Bandwidth reading for Biases:
  if (kernel_has_bias_tensor) {
    // Check if we are adding a DMA with this strategy
    auto vr_scale = search_rewrite(rewrites, scale_tensor);
    if (vr_scale.insert_dma)
      add_tensor_bandwidth_sram_read(kernel_metrics, scaled_bws, scale_tensor,
                                     1.0, arch_c, ofm_block);
    // Add Secondary Storage
    add_tensor_bandwidth_read(kernel_metrics, scaled_bws, scale_tensor, 1.0,
                              arch_c, ofm_block);
  }

#endif

  kernel_metrics.update_bw_cycles(arch_c, scaled_bws);

  LOG(ENABLE_LOGGING_SEARCH_ENGINE_VERBOSE_LEVEL_1,
      "u55_cost_model::cost_metrics_for_kernel()::End");
  return kernel_metrics;
}

cost_metrics u55_cost_model::cost_metrics_for_cascade(
    Operation *op, mlir::ArchitectureConfig::ArchitectureConfigAttr &arch_c) {
  LOG(ENABLE_LOGGING_SEARCH_ENGINE_VERBOSE_LEVEL_1,
      "u55_cost_model::cost_metrics_for_cascade()::Start");

  cost_metrics cascade_region_metrics;
  if (not(llvm::isa<cascadeir::CascadeRegionOp>(*op)))
    return cascade_region_metrics;

  Region &region_instruction = op->getRegion(0);
  Block &block_cascade = region_instruction.front();

  for (llvm::iplist<Operation>::iterator i = block_cascade.begin();
       i != block_cascade.end(); i++) {
    Operation *op_k = &(*i);

    // Empty Adjacency table so everything calculated on the fly
    std::unordered_map<Operation *, kernelInfo> adjacency_table;
    cost_metrics pm = cost_metrics_for_kernel(op_k, arch_c, adjacency_table);
    cascade_region_metrics = cascade_region_metrics + pm;
  }

  LOG(ENABLE_LOGGING_SEARCH_ENGINE_VERBOSE_LEVEL_1,
      "u55_cost_model::cost_metrics_for_cascade()::End");
  return cascade_region_metrics;
}

cost_metrics u55_cost_model::cost_metrics_for_npu_region(
    Operation *op, mlir::ArchitectureConfig::ArchitectureConfigAttr &arch_c) {
  ASSERT_COND(
      not(llvm::isa<scheduleir::NpuRegionOp>(*op)),
      "u55_cost_model::cost_metrics_for_npu_region::NpuRegion Expected");

  Region &region_instruction = op->getRegion(0);
  Block &block_npu_region = region_instruction.front();

  cost_metrics npu_region_metrics;

  for (llvm::iplist<Operation>::iterator i = block_npu_region.begin();
       i != block_npu_region.end(); i++) {
    Operation *op_c = &(*i);
    cost_metrics pm = cost_metrics_for_cascade(op_c, arch_c);
    npu_region_metrics = npu_region_metrics + pm;
  }

  return npu_region_metrics;
}

cost_metrics u55_cost_model::cost_metrics_for_cpu_operation(
    Operation *op, mlir::ArchitectureConfig::ArchitectureConfigAttr &arch_c) {
  cost_metrics metrics;
  return metrics;
}

void u55_cost_model::print_module_stats(
    ostream &stream, mlir::OwningModuleRef &module,
    mlir::ArchitectureConfig::ArchitectureConfigAttr &arch_c) {
  stream << CYAN
         << "#############################################################"
            "################"
         << RESET << std::endl;
  stream << CYAN
         << "#                           Network cost                     "
            "               #"
         << RESET << std::endl;
  stream << CYAN
         << "#############################################################"
            "################"
         << RESET << std::endl;
  Block *block = module.get().getBody();
  Block *function_block =
      &(dyn_cast<FuncOp>(block->begin()).getCallableRegion()->front());

  // System
  std::cout << BOLDGREEN << "Architecture Config:" << RESET << std::endl;
  std::string accelerator_config =
      ConvertToString((AcceleratorConfig)arch_c.accel_config().getInt())
          .c_str();
  stream << "Accelerator configuration: " << accelerator_config << std::endl;
  std::string system_config = arch_c.system_config().getValue().str();
  stream << "System configuration: " << system_config << std::endl;
  std::string npu_clock = std::to_string(arch_c.npu_clock().getInt() / 1000000);
  stream << "Accelerator Clock: " << npu_clock << "Mhz" << std::endl;
  // Architecture Bandwidth Available
  std::cout << BOLDGREEN << "Architecture Bandwidth:" << RESET << std::endl;
  double sram_bandwidth =
      (arch_c.memory_bandwidths_per_second_Sram().getValueAsDouble()) / 1000.0 /
      1000 / 1000;
  double dram_bandwidth =
      (arch_c.memory_bandwidths_per_second_Dram().getValueAsDouble()) / 1000.0 /
      1000 / 1000;
  double on_chip_flash_bandwidth =
      (arch_c.memory_bandwidths_per_second_OnChipFlash().getValueAsDouble()) /
      1000.0 / 1000 / 1000;
  double off_chip_flash_bandwidth =
      (arch_c.memory_bandwidths_per_second_OffChipFlash().getValueAsDouble()) /
      1000.0 / 1000 / 1000;
  std::cout << "Design peak SRAM bandwidth " + std::to_string(sram_bandwidth) +
                   " GB/s"
            << std::endl;
  std::cout << "Design peak DRAM bandwidth " + std::to_string(dram_bandwidth) +
                   " GB/s"
            << std::endl;
  std::cout << "Design peak OnChipFlash bandwidth " +
                   std::to_string(on_chip_flash_bandwidth) + " GB/s"
            << std::endl;
  std::cout << "Design peak OffChipFlash bandwidth " +
                   std::to_string(off_chip_flash_bandwidth) + " GB/s"
            << std::endl;
  // Number of specific Operations
  std::cout << BOLDGREEN << "Network Scheduling:" << RESET << std::endl;
  unsigned no_cpu_operatons = 0;
  unsigned no_npu_operatons = 0;
  unsigned no_kernels = 0;
  unsigned no_cascades = 0;
  for (llvm::iplist<Operation>::iterator i = function_block->begin();
       i != function_block->end(); i++) {
    Operation *op = &(*i);
    if (llvm::isa<scheduleir::NpuRegionOp>(*op)) {
      no_npu_operatons += NpuRegion_NoofOperations(op);
      no_kernels += NpuRegion_NoofKernels(op);
      no_cascades += NpuRegion_NoofCascades(op);
    } else if (is_cpu_node_no_return(op))
      no_cpu_operatons++;
  }
  stream << "No of CPU Operations: " << no_cpu_operatons << std::endl;
  stream << "No of NPU Operations (Not all are executable): "
         << no_npu_operatons << std::endl;
  stream << "No of Kernels: " << no_kernels << std::endl;
  stream << "No of Cascades: " << no_cascades << std::endl;
  // Actual Network Parameters
  // std::cout << BOLD(FGRN("Network cost Metrics (Topologically Sorted):")) <<
  // std::endl;
  cost_metrics total_network_metrics;
  for (llvm::iplist<Operation>::iterator i = function_block->begin();
       i != function_block->end(); i++) {
    Operation *op = &(*i);

    cost_metrics metrics;
    if (llvm::isa<scheduleir::NpuRegionOp>(*op)) {
      metrics = cost_metrics_for_npu_region(op, arch_c);
      // metrics.print(stream, FYEL("NPURegion cost: "), "\t", arch_c);
    } else {
      metrics = cost_metrics_for_cpu_operation(op, arch_c);
      // metrics.print(stream, "CPU Node cost: " + getOpName(*op), "\t");
      // std::cout << FYEL("CPU Node cost: ") << std::endl;
      // std::cout << "\t" << getOpName(*op) << std::endl;
      // std::cout << "\t" << BOLD(FRED("CPU cost OPERATION ESTIMATION IS NOT
      // IMPLEMENTED !!!")) << std::endl;
    }
    total_network_metrics = total_network_metrics + metrics;
  }
  total_network_metrics.print(stream, "Total Network cost: ", "\t", arch_c);

  stream << BOLDCYAN
         << "#############################################################"
            "################"
         << RESET << std::endl;
  stream << std::endl;
}

} // namespace planner
} // namespace mlir

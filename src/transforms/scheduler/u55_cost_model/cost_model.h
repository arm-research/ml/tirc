/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#ifndef U55_COST_MODEL_H
#define U55_COST_MODEL_H

#include "src/ir/architecture_config/architecture_config.h"

#include "src/transforms/kernel_packing/kernelInfo.h"
#include "src/transforms/scheduler/cost_model.h"

#include <vector>
using namespace std;

namespace mlir {
namespace planner {

class u55_cost_model : public cost_model {
public:
  cost_metrics cost_metrics_for_kernel(
      Operation *op, mlir::ArchitectureConfig::ArchitectureConfigAttr &arch_c,
      std::unordered_map<Operation *, kernelInfo> &adjacency_table,
      NpuBlockConfig block_config = {},
      std::vector<value_rewrite> rewrites = {});

  cost_metrics cost_metrics_for_cascade(
      Operation *op, mlir::ArchitectureConfig::ArchitectureConfigAttr &arch_c);

  cost_metrics cost_metrics_for_npu_region(
      Operation *op, mlir::ArchitectureConfig::ArchitectureConfigAttr &arch_c);
  cost_metrics cost_metrics_for_cpu_operation(
      Operation *op, mlir::ArchitectureConfig::ArchitectureConfigAttr &arch_c);

  void
  print_module_stats(ostream &stream, mlir::OwningModuleRef &module,
                     mlir::ArchitectureConfig::ArchitectureConfigAttr &arch_c);
};

} // namespace planner
} // namespace mlir

#endif // U55_COST_MODEL

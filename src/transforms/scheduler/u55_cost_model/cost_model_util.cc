/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#include "src/transforms/scheduler/u55_cost_model/cost_model_util.h"
#include "src/transforms/scheduler/cost_metrics.h"

namespace mlir {
namespace planner {

/////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                      MEMORY //
/////////////////////////////////////////////////////////////////////////////////////////////////////////

double estimate_memory_transfer_efficiency(
    mlir::ArchitectureConfig::ArchitectureConfigAttr &arch,
    value_mem_area mem_area, BandwidthDirection direction,
    mlir::SchedTensorType tensor, BlockStruct block_size, float replace_bw) {
  return tensor_bandwidth(tensor);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                      COMPUTE //
/////////////////////////////////////////////////////////////////////////////////////////////////////////

/*
def get_minimal_cmd_cycles(arch, ifm_tensor, ofm_tensor, ifm_blk: Block,
ofm_blk: Block, output_cycles, ifm_shape4D, ofm_shape4D, dpu_cycles=0):
    ifm_tens_blk = Tensor((1, ifm_blk.height, ifm_blk.width, ifm_blk.depth),
ifm_tensor.dtype, "ifm_blk") ofm_tens_blk = Tensor((1, ofm_blk.height,
ofm_blk.width, ofm_blk.depth), ofm_tensor.dtype, "ofm_blk") cycles_ifm_blk = (
        estimate_memory_transfer_efficiency(
            arch, ifm_tensor.mem_area, BandwidthDirection.Read, ifm_tens_blk,
ifm_blk, shape4D=ifm_shape4D
        )
        / arch.memory_bandwidths_per_cycle[ifm_tensor.mem_area]
    )
    cycles_ofm_blk = (
        estimate_memory_transfer_efficiency(
            arch, ofm_tensor.mem_area, BandwidthDirection.Write, ofm_tens_blk,
ofm_blk, shape4D=ofm_shape4D
        )
        / arch.memory_bandwidths_per_cycle[ofm_tensor.mem_area]
    )
    return (
        arch.memory_latency[ifm_tensor.mem_area][BandwidthDirection.Read]
        + cycles_ifm_blk
        + dpu_cycles
        + output_cycles
        + arch.memory_latency[ofm_tensor.mem_area][BandwidthDirection.Write]
        + cycles_ofm_blk
    ) / 4
*/

void estimate_output_cycles(
    mlir::ArchitectureConfig::ArchitectureConfigAttr &arch,
    std::unordered_map<Operation *, kernelInfo> &adjacency_table,
    NpuBlockType npu_block_type, Operation *kernel,
    mlir::SchedTensorType ofm_tensor, mlir::SchedTensorType ifm_1_tensor,
    mlir::SchedTensorType ifm_2_tensor, BlockStruct ofm_block,
    bool use_acc_40bits, int64_t num_elems, double &cycles) {
  /* Be carefull if using ifm_2_tensor as in estimate_conv_pooling_cycles being
   * passed around wrongly */
  /*
    ASSERT_COND(
        getBitDepthType(ifm_1_tensor) == 16,
        "estimate_conv_pooling_cycles::Performance model not tuned for 16-bit");
    ASSERT_COND(
        npu_block_type == NpuBlockType::Default,
        "estimate_conv_pooling_cycles::NpuBlockType::Default not supported");
    ASSERT_COND(
        npu_block_type == NpuBlockType::ReduceSum,
        "estimate_conv_pooling_cycles::NpuBlockType::ReduceSum not supported");
    ASSERT_COND(
        npu_block_type == NpuBlockType::Memory,
        "estimate_conv_pooling_cycles::NpuBlockType::Memory not supported");
  */

  int32_t output_perf_index = 0;
  int32_t activation_perf_index = 0;

  auto output_cycles_per_elem = get_output_cycles_tables(arch);
  auto activation_cycles_per_elem = get_activation_cycles_tables(arch);

  Operation *kernel_op_major;
  SmallVector<Operation *, 1> act_op;
  if (adjacency_table.find(kernel) != adjacency_table.end()) {
    auto &entry = adjacency_table[kernel];
    kernel_op_major = entry.major_op;
    act_op = entry.act_ops;
  } else {
    kernel_op_major = mlir::kernelir::getKernelMajorOp(kernel);
    act_op = mlir::kernelir::getKernelActivationsOps(kernel);
  }

  if (((npu_block_type == NpuBlockType::ConvolutionMxN) ||
       (npu_block_type == NpuBlockType::ConvolutionDepthWise) ||
       (npu_block_type == NpuBlockType::VectorProduct) ||
       (npu_block_type == NpuBlockType::ReduceSum) ||
       (npu_block_type == NpuBlockType::Pooling)) and
      use_acc_40bits)
    output_perf_index = 3;
  else if (llvm::isa<scheduleir::AddOp>(kernel_op_major))
    // Advanced Add/Sub
    output_perf_index = 5;
  else if (llvm::isa<scheduleir::MaxPool2dOp>(kernel_op_major))
    output_perf_index = 6;
  else if (llvm::isa<scheduleir::AvgPool2dOp>(kernel_op_major))
    output_perf_index = 6;
  else
    output_perf_index = 7;

  if (act_op.size() == 0)
    activation_perf_index = 2;
  else {
    if (llvm::isa<scheduleir::ClampOp>(act_op[0]))
      activation_perf_index = 1;
    else
      FATAL_ERROR("estimate_output_cycles::unknown activation");
  }

  double cycle_per_elem =
      max(output_cycles_per_elem[output_perf_index],
          activation_cycles_per_elem[activation_perf_index]);

#if 0
  if primary_op.type.is_elementwise_op() and block_config is not None:
      num_elems_blk = block_config.width * block_config.height *
  block_config.depth cycle_cmd = get_minimal_cmd_cycles( arch, ifm_tensor,
          ofm_tensor,
          block_config,
          block_config,
          num_elems_blk * cycle_per_elem,
          primary_op.ifm_shapes[0],
          primary_op.ofm_shapes[0],
      )
      cycle_per_elem = max(cycle_per_elem, cycle_cmd / num_elems_blk)
#endif

  cycles = (num_elems * cycle_per_elem);
}

void estimate_conv_pooling_cycles(
    mlir::ArchitectureConfig::ArchitectureConfigAttr &arch,
    std::unordered_map<Operation *, kernelInfo> &adjacency_table,
    NpuBlockType npu_block_type, Operation *kernel, BlockStruct &ifm_block,
    BlockStruct &ofm_block, NpuBlockTraversal block_traversal,
    mlir::utils::FilterMetaData &filter, mlir::SchedTensorType &ifm_tensor,
    mlir::SchedTensorType &ofm_tensor, mlir::SchedTensorType &scale_tensor,
    double &total_cycles, double &hardware_cycles) {

  /*
    ASSERT_COND(
        getBitDepthType(ifm_tensor) == 16,
        "estimate_conv_pooling_cycles::Performance model not tuned for 16-bit");
    ASSERT_COND(
        arch.no_cores().getInt() != 1,
        "estimate_conv_pooling_cycles::Performance model only for single core");
    ASSERT_COND(
        npu_block_type == NpuBlockType::Default,
        "estimate_conv_pooling_cycles::NpuBlockType::Default not supported");
    ASSERT_COND(
        npu_block_type == NpuBlockType::ReduceSum,
        "estimate_conv_pooling_cycles::NpuBlockType::ReduceSum not supported");
    ASSERT_COND(
        npu_block_type == NpuBlockType::ElementWise,
        "estimate_conv_pooling_cycles::NpuBlockType::Elementwise not
    supported"); ASSERT_COND( npu_block_type == NpuBlockType::Memory,
        "estimate_conv_pooling_cycles::NpuBlockType::Memory not supported");
    ASSERT_COND(
        is_u65_system(arch),
        "estimate_conv_pooling_cycles::NpuBlockType::u65 System not Supported");
  */

  total_cycles = 0;
  hardware_cycles = 0;

  bool ifm_tensor_16bit = false;
  HWConfig hw_config =
      get_acclerator_config((AcceleratorConfig)arch.accel_config().getInt());

  auto ofm_ublock = hw_config.ofm_ublock;
  auto ifm_tens_shape = ifm_tensor.getShape();
  auto ofm_tens_shape = ofm_tensor.getShape();

  // Optimisation only applies for even width tensors
  if ((ofm_ublock.height == 2) and
      ((npu_block_type == NpuBlockType::ConvolutionMxN) or
       (npu_block_type == NpuBlockType::ConvolutionDepthWise) or
       (npu_block_type == NpuBlockType::VectorProduct)) and
      (ofm_tens_shape[1] == 1) and (ofm_tens_shape[2] % 2 == 0) and
      filter.height == 1) {
    ofm_ublock.width = 4;
    ofm_ublock.height = 1;
    ofm_block.height = 1;
  }

  uint64_t num_ublk_x =
      round_up_divide<uint64_t>(ofm_block.width, ofm_ublock.width);
  uint64_t num_ublk_y = ofm_block.height / ofm_ublock.height;
  uint64_t num_ublk_xy = num_ublk_x * num_ublk_y;
  uint64_t num_ublk_z = ofm_block.depth / ofm_ublock.depth;
  uint64_t num_elems_blk = ofm_block.width * ofm_block.height * ofm_block.depth;

  bool use_acc_40bits = false;
#if 0
      use_acc_40bits = is_acc_40bits_used(npu_block_type, ifm_tensor,
     ofm_tensor);
#endif

  auto sub_kernel_limits = get_sub_kernel_limits(npu_block_type);
  auto n_sub_kernels_y =
      round_up_divide<uint64_t>(filter.height, sub_kernel_limits[0]);
  auto n_sub_kernels_x =
      round_up_divide<uint64_t>(filter.width, sub_kernel_limits[1]);

  std::vector<uint64_t> num_kernel_elems_v;
  for (int y = 0; y < n_sub_kernels_y; y++) {
    for (int x = 0; x < n_sub_kernels_x; x++) {
      uint64_t sub_kernel_x =
          min((filter.width - x * sub_kernel_limits[1]), sub_kernel_limits[1]);
      uint64_t sub_kernel_y =
          min((filter.height - y * sub_kernel_limits[0]), sub_kernel_limits[0]);
      num_kernel_elems_v.push_back(sub_kernel_x * sub_kernel_y);
    }
  }
  /* std::cout << "Filter height: " << filter.height << " Filter Width: " <<
   * filter.width << std::endl; */

  double cycles_wb = 32 * ofm_ublock.depth / 8;
  double cycles_output_blk = 0;
  double cycles_dpu_blk = 0;
  double cycles_bias_blk = 0;

  for (uint64_t num_kernel_elems : num_kernel_elems_v) {
    double cycles = 0;
    int64_t num_kernel_steps = 0;

    if (npu_block_type == NpuBlockType::Pooling) {
      num_kernel_steps = 1;
      cycles = max((long unsigned int)4, num_kernel_elems) * num_ublk_xy *
               num_ublk_z;
      if (ifm_tensor_16bit and
          ((AcceleratorConfig)arch.accel_config().getInt() != u55_32))
        cycles *= 2;
    } else if (npu_block_type == NpuBlockType::ConvolutionDepthWise) {
      cycles = 4 * num_ublk_xy;
      if (ifm_tensor_16bit)
        cycles *= 2;
      num_kernel_steps = round_up_divide<uint64_t>(num_kernel_elems, 4);
      cycles = max(cycles_wb, cycles) * num_kernel_steps * num_ublk_z;
    } else if (((npu_block_type == NpuBlockType::ConvolutionMxN) and
                (block_traversal != tbt_PartKernelFirst)) or
               (npu_block_type == NpuBlockType::VectorProduct) or
               (npu_block_type == NpuBlockType::ReduceSum)) {
      num_kernel_steps = num_kernel_elems;
      cycles = max(cycles_wb, double(4 * num_ublk_xy)) * num_kernel_steps *
               num_ublk_z;
    } else {
      ASSERT_COND(block_traversal != tbt_PartKernelFirst,
                  "estimate_conv_pooling_cycles::BlockTraversal expected to be "
                  "PartKernelFirst");
      uint64_t divider = 0;
      if (ifm_tensor_16bit)
        divider = 2;
      else
        divider = 4;
      num_kernel_steps = round_up_divide<uint64_t>(num_kernel_elems, divider);
      cycles = max(cycles_wb, double(4 * num_ublk_xy)) *
               (num_kernel_steps *
                round_up_divide<uint64_t>(ifm_block.depth, 8) * num_ublk_z);
    }

    double delay_cycles = 0;

    if ((AcceleratorConfig)arch.accel_config().getInt() == u55_32) {
      uint64_t delay;
      if (use_acc_40bits)
        delay = 7;
      else
        delay = 3;
      if (num_ublk_x == 1 and num_ublk_y == 1) {
        if (num_ublk_z == 1)
          delay_cycles = delay * num_kernel_steps;
        else if (num_kernel_steps > 1)
          delay_cycles = delay * (num_kernel_steps - 1) * num_ublk_z;
      }
      if ((num_ublk_x == 1 or num_ublk_y == 1) and num_ublk_z > 1 and
          use_acc_40bits)
        delay_cycles += delay * num_ublk_z;
    } else {
      uint64_t delay;
      if (use_acc_40bits and
          (((AcceleratorConfig)arch.accel_config().getInt() == u55_64) or
           ((AcceleratorConfig)arch.accel_config().getInt() == u55_128)))
        delay = 3;
      else
        delay = 2;
      if (num_ublk_x == 1 and num_ublk_y == 1) {
        if (num_ublk_z == 1)
          delay_cycles = delay * num_kernel_steps;
        else if (num_kernel_steps > 1)
          delay_cycles = delay * (num_kernel_steps - 1) * num_ublk_z;
      }
    }

    if ((npu_block_type == NpuBlockType::ConvolutionMxN) and
        (block_traversal == tbt_PartKernelFirst))
      delay_cycles *= round_up_divide<uint64_t>(ifm_block.depth, 8);

    cycles_dpu_blk += cycles;
    cycles_dpu_blk += delay_cycles;
  }

  if ((npu_block_type == NpuBlockType::ConvolutionMxN) and
      (npu_block_type == NpuBlockType::VectorProduct) and
      (npu_block_type == NpuBlockType::ReduceSum))
    cycles_dpu_blk *=
        double(round_up_divide<uint64_t>(ifm_tens_shape[3], ifm_block.depth));

  double num_ofm_blk =
      (round_up_divide<uint64_t>(ofm_tens_shape[1], ofm_block.height) *
       round_up_divide<uint64_t>(ofm_tens_shape[2], ofm_block.width) *
       round_up_divide<uint64_t>(ofm_tens_shape[3], ofm_block.depth));

  estimate_output_cycles(arch, adjacency_table, npu_block_type, kernel,
                         ofm_tensor, ifm_tensor, ifm_tensor, ofm_block,
                         use_acc_40bits, num_elems_blk, cycles_output_blk);

  // All Block Config that reach Here apart from Pooling will have a scale
  if (npu_block_type != NpuBlockType::Pooling) {
    auto a = scale_tensor.get_mem_area();
    auto arch_memory_latency = get_memory_latency_u55(a, bd_Read);
    cycles_bias_blk = (10 * min(ofm_block.depth, (uint64_t)ofm_tens_shape[3]) *
                       arch_memory_latency / 256);
  }

#if 0
      cycle_cmd = get_minimal_cmd_cycles(
          arch,
          ifm_tensor,
          ofm_tensor,
          ifm_block,
          ofm_block,
          cycles_dpu_blk,
          ifm_tens_shape,
          ofm_tens_shape,
          cycles_output_blk,
      )
      cycles_dpu_blk = max(cycles_dpu_blk, cycles_cmd)
      cycles_output_blk = max(cycles_output_blk, cycles_cmd)
#endif

  cycles_output_blk = max(cycles_output_blk, cycles_bias_blk);

  // We add a penalty for ecery block we need to process. Since the
  // weight_read_multiple will be 0 here, we stil would like to choose the bloch
  // shape that gives us the least amount of block iterations. 100 is chosen
  // speculaively at the moment.
  double blk_penalty = 0;
#if ENABLE_BLK_SIZE_MINIMIZATION
  if (npu_block_type == NpuBlockType::Pooling)
    blk_penalty = num_ofm_blk * 100;
#endif

  if (cycles_dpu_blk > cycles_output_blk)
    total_cycles =
        (cycles_dpu_blk * num_ofm_blk + cycles_output_blk + blk_penalty);
  else
    total_cycles =
        (cycles_output_blk * num_ofm_blk + cycles_dpu_blk + blk_penalty);
}

} // namespace planner
} // namespace mlir

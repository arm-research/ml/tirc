/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#ifndef U55__COST_MODEL_UTIL_H
#define U55__COST_MODEL_UTIL_H

#include "mlir/Dialect/Quant/QuantOps.h"
#include "mlir/Dialect/Quant/QuantTypes.h"
#include "mlir/Dialect/StandardOps/IR/Ops.h"      // from @llvm-project
#include "mlir/IR/AffineMap.h"                    // from @llvm-project
#include "mlir/IR/Attributes.h"                   // from @llvm-project
#include "mlir/IR/BlockAndValueMapping.h"         // from @llvm-project
#include "mlir/IR/Builders.h"                     // from @llvm-project
#include "mlir/IR/Location.h"                     // from @llvm-project
#include "mlir/IR/MLIRContext.h"                  // from @llvm-project
#include "mlir/IR/Operation.h"                    // from @llvm-project
#include "mlir/IR/PatternMatch.h"                 // from @llvm-project
#include "mlir/Interfaces/InferTypeOpInterface.h" // from @llvm-project
#include "mlir/Pass/Pass.h"                       // from @llvm-project
#include "mlir/Transforms/DialectConversion.h"    // from @llvm-project

#include "src/ir/architecture_config/architecture_config.h"
#include "src/utils/ir_utils.h"
#include "src/utils/numeric_utils.h"

#include "src/ir/kernel_ir.h"
#include "src/transforms/kernel_packing/kernelInfo.h"

#include <set>

namespace mlir {
namespace planner {

void estimate_conv_pooling_cycles(
    mlir::ArchitectureConfig::ArchitectureConfigAttr &arch,
    std::unordered_map<Operation *, kernelInfo> &adjacency_table,
    NpuBlockType npu_block_type, Operation *kernel, BlockStruct &ifm_block,
    BlockStruct &ofm_block, NpuBlockTraversal block_traversal,
    mlir::utils::FilterMetaData &filter, mlir::SchedTensorType &ifm_tensor,
    mlir::SchedTensorType &ofm_tensor, mlir::SchedTensorType &scale_tensor,
    double &cycles, double &hardware_cycles);

void estimate_output_cycles(
    mlir::ArchitectureConfig::ArchitectureConfigAttr &arch,
    std::unordered_map<Operation *, kernelInfo> &adjacency_table,
    NpuBlockType npu_block_type, Operation *kernel,
    mlir::SchedTensorType ofm_tensor, mlir::SchedTensorType ifm_1_tensor,
    mlir::SchedTensorType ifm_2_tensor, BlockStruct ofm_block,
    bool use_acc_40bits, int64_t num_elems, double &cycles);

double estimate_memory_transfer_efficiency(
    mlir::ArchitectureConfig::ArchitectureConfigAttr &arch,
    value_mem_area mem_area, BandwidthDirection direction,
    mlir::SchedTensorType tensor, BlockStruct block_size, float replace_bw = 0);

} // namespace planner
} // namespace mlir

#endif // U55__COST_MODEL_UTIL_H

/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#ifndef VALUE_REWRITE_H
#define VALUE_REWRITE_H

#include "src/utils/bitvector.h"
#include "src/utils/dag.h"

namespace mlir {
namespace planner {

enum value_rewrite_ops { ChangeTensorSubPurpose };
struct value_rewrite {
  value_rewrite_ops rewrite_op;
  mlir::SchedTensorType type;
  mlir::Value value;
  bool insert_dma;
  SmallVector<int64_t, 4> new_shape;
  SmallVector<int64_t, 4> new_storage_shape;
  value_format new_format;
  value_sub_purpose new_sub_purpose;
};

} // namespace planner
} // namespace mlir

#endif // VALUE_REWRITE

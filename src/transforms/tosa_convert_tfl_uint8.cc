/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#include <climits>
#include <cstddef>
#include <cstdint>
#include <iterator>
#include <numeric>

#include "mlir/Dialect/Tosa/IR/TosaOps.h"               // from @llvm-project
#include "mlir/Dialect/Tosa/Utils/QuantUtils.h"         // from @llvm-project
#include "mlir/IR/Builders.h"                           // from @llvm-project
#include "mlir/IR/BuiltinAttributes.h"                  // from @llvm-project
#include "mlir/IR/BuiltinTypes.h"                       // from @llvm-project
#include "mlir/IR/PatternMatch.h"                       // from @llvm-project
#include "mlir/Pass/PassRegistry.h"                     // from @llvm-project
#include "mlir/Support/LogicalResult.h"                 // from @llvm-project
#include "mlir/Transforms/GreedyPatternRewriteDriver.h" // from @llvm-project
#include "tensorflow/compiler/mlir/lite/ir/tfl_ops.h"
#include "tensorflow/compiler/mlir/tosa/transforms/legalize_common.h"
#include "tensorflow/compiler/mlir/tosa/transforms/legalize_utils.h"
#include "tensorflow/compiler/mlir/tosa/transforms/passes.h"

#include "src/transforms/passes.h"
#include "src/utils/ir_utils.h"

#define PASS_NAME "tosa-convert-tfl-uint8"
#define DEBUG_TYPE PASS_NAME

#define ENABLE_LOGGING 0

namespace mlir {
namespace tosa {
namespace {
#define GEN_PASS_CLASSES
#include "tensorflow/compiler/mlir/tosa/transforms/passes.h.inc"

// Performs lowering to TOSA dialect.
class ConvertUint8ToInt8LM
    : public TosaConvertTFLUint8PassBase<ConvertUint8ToInt8LM> {
public:
  explicit ConvertUint8ToInt8LM() {}

  StringRef getArgument() const final {
    // This is the argument used to refer to the pass in
    // the textual format (on the commandline for example).
    return PASS_NAME;
  }
  StringRef getDescription() const final {
    // This is a brief description of the pass.
    return "Convert uint8 graph to int8.";
  }

  void runOnFunction() override;

  std::unordered_map<std::string, mlir::utils::Legalization>
      legalization_maplist;
};

std::unordered_map<mlir::Operation *, std::vector<mlir::Type>> cache;

mlir::utils::Legalization
getLegalization(std::unordered_map<std::string, mlir::utils::Legalization>
                    &legalization_maplist,
                std::string op_name) {
  if (legalization_maplist.find(op_name) == legalization_maplist.end()) {
    std::cout
        << op_name
        << " ConvertTFLUint8Pass::runOnFunction()::getLegalization() fatal "
           "error TFLite operation not found"
        << std::endl;
    exit(1);
  }
  return legalization_maplist[op_name];
}

mlir::utils::Legalization
getLegalization(std::unordered_map<std::string, mlir::utils::Legalization>
                    &legalization_maplist,
                Operation &op) {
  std::string op_name = op.getName().getStringRef().str();
  if (op_name == "tfl.pseudo_qconst")
    return mlir::utils::Legalization::TFLite; // This conversion is done as an
                                              // Operand of the actual Ops
  else if (op_name == "tfl.conv_2d")
    return getLegalization(legalization_maplist, "TFLConv2D");
  else if (op_name == "tfl.softmax")
    return getLegalization(legalization_maplist, "TFLSoftmax");
  else if (op_name == "tfl.max_pool_2d")
    return getLegalization(legalization_maplist, "TFLMaxPool2D");
  else if (op_name == "tfl.average_pool_2d")
    return getLegalization(legalization_maplist, "TFLAveragePool2D");
  else if (op_name == "tfl.fully_connected")
    return getLegalization(legalization_maplist, "TFLFullyConnected");
  else if (op_name == "tfl.reshape")
    return getLegalization(legalization_maplist, "TFLReshape");
  else if (op_name == "tfl.depthwise_conv_2d")
    return getLegalization(legalization_maplist, "TFLDepthwiseConv2D");
  else if (op_name == "tfl.transpose_conv")
    return getLegalization(legalization_maplist, "TFLTransposeConv");
  else if (op_name == "tfl.add")
    return getLegalization(legalization_maplist, "TFLAdd");
  else if (op_name == "tfl.mul")
    return getLegalization(legalization_maplist, "TFLMul");
  else if (op_name == "tfl.concatenation")
    return getLegalization(legalization_maplist, "TFLConcatenation");
  else if (op_name == "tfl.quantize")
    return getLegalization(legalization_maplist, "TFLQuantize");
  else if (op_name == "tfl.squeeze")
    return getLegalization(legalization_maplist, "TFLSqueeze");
  else if (op_name == "tfl.reshape")
    return getLegalization(legalization_maplist, "TFLReshape");
  else if (op_name == "arith.constant")
    return mlir::utils::Legalization::TFLite;
  else if (op_name == "std.return")
    return mlir::utils::Legalization::TFLite;
  else if (op_name == "std.constant")
    return mlir::utils::Legalization::TFLite;
  else {
    std::cout << op_name << std::endl;
    FATAL_ERROR("tosa_convert_tfl_uint8::TFlite node not supported");
  }
}

void insertRescale_Int8_2_UInt8(mlir::FuncOp &function, Block &bb,
                                Value &input_val, Type output_type,
                                Operation *consuming_op, OpBuilder &builder) {
  if (not(input_val.hasOneUse()))
    FATAL_ERROR("insertRescale_Int8_2_UInt8::Expected Single Use");

  auto int8_output_type =
      input_val.getType().dyn_cast<mlir::RankedTensorType>();
  auto int8_output_element_type =
      int8_output_type.getElementType()
          .dyn_cast<mlir::quant::UniformQuantizedType>();

  auto uint8_output_type = output_type.dyn_cast<mlir::RankedTensorType>();
  auto uint8_output_element_type =
      uint8_output_type.getElementType()
          .dyn_cast<mlir::quant::UniformQuantizedType>();

  int32_t int8_zp = int8_output_element_type.getZeroPoint();
  int32_t uint8_zp = uint8_output_element_type.getZeroPoint();

  // Sanity check if uint8/int8's scale and zeropoint match.
  if (((uint8_zp - int8_zp) != 128) || (int8_output_element_type.getScale() !=
                                        uint8_output_element_type.getScale()))
    return FATAL_ERROR(
        "convert_uint8_to_int8: scale mismatch at the output tensors");

  builder.setInsertionPointAfter(input_val.getDefiningOp());
  auto rescale_op = builder.create<tosa::RescaleOp>(
      function.getLoc(), output_type, input_val,
      builder.getI32IntegerAttr(int8_zp), builder.getI32IntegerAttr(uint8_zp),
      builder.getI32ArrayAttr({1 << 30}), builder.getI32ArrayAttr({30}),
      builder.getBoolAttr(true), builder.getBoolAttr(false),
      builder.getBoolAttr(false));

  input_val.replaceAllUsesExcept(rescale_op.getResult(), rescale_op);
}

void insertRescale_UInt8_2_Int8(mlir::FuncOp &function, Block &bb,
                                Value &input_val, OpBuilder &builder) {
  auto uint8_type = input_val.getType().dyn_cast<mlir::RankedTensorType>();
  auto uint8_element_type =
      uint8_type.getElementType().dyn_cast<mlir::quant::UniformQuantizedType>();

  double type_range_min =
      static_cast<double>(uint8_element_type.getStorageTypeMin() -
                          uint8_element_type.getZeroPoint()) *
      uint8_element_type.getScale();
  double type_range_max =
      static_cast<double>(uint8_element_type.getStorageTypeMax() -
                          uint8_element_type.getZeroPoint()) *
      uint8_element_type.getScale();
  bool narrow_range =
      uint8_element_type.getStorageTypeMin() == 1 ? true : false;

  builder.setInsertionPointAfterValue(input_val);
  Type int8_type = RankedTensorType::get(
      uint8_type.getShape(),
      buildQTypeFromMinMax(
          builder, uint8_element_type.getExpressedType(),
          builder.getF64FloatAttr(type_range_min),
          builder.getF64FloatAttr(type_range_max),
          builder.getI32IntegerAttr(
              uint8_element_type.getStorageTypeIntegralWidth()),
          0, true /* signed */, builder.getBoolAttr(narrow_range)));

  int32_t uint8_zp = uint8_element_type.getZeroPoint();
  int32_t int8_zp = uint8_zp - 128;

  auto rescale_op = builder.create<tosa::RescaleOp>(
      function.getLoc(), int8_type, input_val,
      builder.getI32IntegerAttr(uint8_zp), builder.getI32IntegerAttr(int8_zp),
      builder.getI32ArrayAttr({1 << 30}), builder.getI32ArrayAttr({30}),
      builder.getBoolAttr(true), builder.getBoolAttr(false),
      builder.getBoolAttr(false));

  input_val.replaceAllUsesExcept(rescale_op.getResult(), rescale_op);
}

LogicalResult convert_graph_uint8_tensor(
    std::unordered_map<std::string, mlir::utils::Legalization>
        &legalization_maplist,
    mlir::MLIRContext &context, mlir::FuncOp &function) {
  size_t num_blocks_in_main = 0;
  mlir::Region *region = function.getCallableRegion();
  OpBuilder builder(&context);

  auto tmp_const_type = RankedTensorType::get({1}, builder.getIntegerType(8));
  auto tmp_const_attr =
      DenseElementsAttr::get(tmp_const_type, {static_cast<uint8_t>(0)});

  for (mlir::Block &bb : region->getBlocks()) {
    // Always have one block for each region right now.
    num_blocks_in_main++;
    if (num_blocks_in_main > 1) {
      return function.emitError("Invalid MLIR: multiple blocks in a region");
    }

    if (!bb.isEntryBlock()) {
      return function.emitError("Invalid MLIR: block must be entry block");
    }

    bool u8_network = false;

    auto terminator = bb.getTerminator();
    SmallVector<Type, 4> output_types;
    for (Value val : terminator->getOperands()) {
      output_types.push_back(val.getType());
    }
    ASSERT_COND(output_types.size() != 1, "Single Output Graph Expected");

    // A. Apply conversion to any outputs and const operands
    for (auto &op : bb) {
      if (getLegalization(legalization_maplist, op) ==
          mlir::utils::Legalization::TosaCommon) {
        // Convert Output to int8
        for (Value output_val : op.getResults()) {
          // Skip if output value is not RankedTensorType.
          auto output_type =
              output_val.getType().dyn_cast<mlir::RankedTensorType>();
          if (!output_type)
            continue;
          // Skip if output value is not per-tensor quantized element type.
          auto output_element_type =
              output_type.getElementType()
                  .dyn_cast<mlir::quant::UniformQuantizedType>();
          if (!output_element_type)
            continue;
          // Skip if output is not uint8.
          if (output_element_type.isSigned() ||
              output_element_type.getStorageTypeIntegralWidth() != 8)
            continue;

#if ENABLE_LOGGING
          std::cout << "Convert to uint8: " << op.getName().getStringRef().str()
                    << std::endl;
#endif
          u8_network = true;

          cache[&op].push_back(output_type);

          double type_range_min =
              static_cast<double>(output_element_type.getStorageTypeMin() -
                                  output_element_type.getZeroPoint()) *
              output_element_type.getScale();
          double type_range_max =
              static_cast<double>(output_element_type.getStorageTypeMax() -
                                  output_element_type.getZeroPoint()) *
              output_element_type.getScale();
          bool narrow_range =
              output_element_type.getStorageTypeMin() == 1 ? true : false;
          Type new_type = RankedTensorType::get(
              output_type.getShape(),
              buildQTypeFromMinMax(
                  builder, output_element_type.getExpressedType(),
                  builder.getF64FloatAttr(type_range_min),
                  builder.getF64FloatAttr(type_range_max),
                  builder.getI32IntegerAttr(
                      output_element_type.getStorageTypeIntegralWidth()),
                  0, true /* signed */, builder.getBoolAttr(narrow_range)));
          output_val.setType(new_type);
        }

        // Convert Const Operands to int8
        for (Value operand : op.getOperands()) {
          // Skip if operand value is not RankedTensorType.
          auto output_type =
              operand.getType().dyn_cast<mlir::RankedTensorType>();
          if (!output_type)
            continue;
          // Skip if operand value is not per-tensor quantized element type.
          auto output_element_type =
              output_type.getElementType()
                  .dyn_cast<mlir::quant::UniformQuantizedType>();
          if (!output_element_type)
            continue;
          // Skip if operand is not uint8.
          if (output_element_type.isSigned() ||
              output_element_type.getStorageTypeIntegralWidth() != 8)
            continue;

          if (operand.getDefiningOp() == nullptr)
            continue;

          if (llvm::isa<TFL::QConstOp>(operand.getDefiningOp())) {
#if ENABLE_LOGGING
            std::cout << "   Converting Constant " << std::endl;
#endif

            auto tfl_qconst_op = cast<TFL::QConstOp>(operand.getDefiningOp());
            mlir::DenseElementsAttr src_dense_attr =
                tfl_qconst_op.value().cast<DenseElementsAttr>();

            double type_range_min =
                static_cast<double>(output_element_type.getStorageTypeMin() -
                                    output_element_type.getZeroPoint()) *
                output_element_type.getScale();
            double type_range_max =
                static_cast<double>(output_element_type.getStorageTypeMax() -
                                    output_element_type.getZeroPoint()) *
                output_element_type.getScale();
            bool narrow_range =
                output_element_type.getStorageTypeMin() == 1 ? true : false;

            auto dst_qconst_type = TypeAttr::get(RankedTensorType::get(
                output_type.getShape(),
                buildQTypeFromMinMax(
                    builder, output_element_type.getExpressedType(),
                    builder.getF64FloatAttr(type_range_min),
                    builder.getF64FloatAttr(type_range_max),
                    builder.getI32IntegerAttr(
                        output_element_type.getStorageTypeIntegralWidth()),
                    0, true /* signed */, builder.getBoolAttr(narrow_range))));

            Type dst_dense_element_type = builder.getIntegerType(8);
            llvm::function_ref<APInt(const APInt &)> mapping =
                [](const APInt &in) -> APInt {
              int64_t in_i64 = in.getLimitedValue();
              int64_t out_i64 = in_i64 - 128;
              return APInt(8, out_i64, true);
            };

            auto dst_dense_attr =
                src_dense_attr.mapValues(dst_dense_element_type, mapping);

            llvm::iplist<Operation>::iterator insertPoint(
                operand.getDefiningOp());
            builder.setInsertionPoint(&bb, insertPoint);

            auto new_tfl_qconst_op = builder.create<TFL::QConstOp>(
                tfl_qconst_op->getLoc(), dst_qconst_type, dst_dense_attr);

            tfl_qconst_op->getResult(0).replaceAllUsesWith(
                new_tfl_qconst_op->getResult(0));

            tfl_qconst_op->dropAllReferences();
            tfl_qconst_op->dropAllDefinedValueUses();
            tfl_qconst_op->erase();
          }
        }
      }
    }

    for (auto &op : bb) {
      // B. Insert Rescale (uint8 and int8) on any inputs from CPU Node or Basic
      // Block that is int 8 C. Insert Rescale (int8 to uint8) on any outputs
      // from NPU Node to CPU Node that is uint8

      if (llvm::isa<TFL::QConstOp>(op))
        continue;
      if (llvm::isa<TFL::ConstOp>(op))
        continue;
      std::string op_name = op.getName().getStringRef().str();
      if (op_name == "std.constant")
        continue;
      if (op_name == "arith.constant")
        continue;

      if (op.getNumResults() == 0)
        continue;

      for (Value operand : op.getOperands()) {
        if (operand.getDefiningOp() != nullptr) {
          if (llvm::isa<TFL::QConstOp>(operand.getDefiningOp()))
            continue;
          if (llvm::isa<TFL::ConstOp>(operand.getDefiningOp()))
            continue;
          if (operand.getDefiningOp()->getName().getStringRef().str() ==
              "std.constant")
            continue;
          if (operand.getDefiningOp()->getName().getStringRef().str() ==
              "arith.constant")
            continue;
        }

        value_datatype operand_type = getDataType(operand.getType());
        value_datatype result_type = getDataType(op.getResult(0).getType());
        if (operand_type != result_type) {
          if ((operand_type == int8) and (result_type == uint8)) {
            mlir::Type output_type =
                cache[ExtractProducers(&op)[0].external_operation][0];
            insertRescale_Int8_2_UInt8(function, bb, operand, output_type, &op,
                                       builder);
          } else if ((operand_type == uint8) and (result_type == int8))
            insertRescale_UInt8_2_Int8(function, bb, operand, builder);
          else {
            std::cout << "operand type: " << operand_type
                      << " result type: " << result_type << std::endl;
            FATAL_ERROR("tosa_convert_tfl_uint8::(1)::Illegal Situation");
          }
#if ENABLE_LOGGING
          std::cout << "(1) Insert Rescale: "
                    << op.getName().getStringRef().str() << " " << operand_type
                    << " " << result_type << std::endl;
#endif
        }
      }
    }

    // Check that we have placed a rescale at output if needed
    if (u8_network) {
      Operation &op = bb.back();
      for (Value operand : op.getOperands()) {
        value_datatype operand_type = getDataType(operand.getType());
        value_datatype result_type = getDataType(output_types[0]);
        if (operand_type != result_type) {
          if ((operand_type != int8) or (result_type != uint8))
            FATAL_ERROR("tosa_convert_tfl_uint8::(2)::Illegal Situation");
          insertRescale_Int8_2_UInt8(function, bb, operand, output_types[0],
                                     &op, builder);
#if ENABLE_LOGGING
          std::cout << "(2) Insert Rescale: "
                    << op.getName().getStringRef().str() << " " << operand_type
                    << std::endl;
#endif
        }
      }
    }
  }
  return success();
}

void ConvertUint8ToInt8LM::runOnFunction() {
  OwningRewritePatternList patterns(&getContext());
  auto &ctx = getContext();
  auto func = getFunction();

  // Replace uint8 tensors in the graph and insert rescale as needed.
  (void)convert_graph_uint8_tensor(legalization_maplist, ctx, func);
}

} // anonymous namespace

std::unique_ptr<OperationPass<FuncOp>> createConvertTFLUint8Pass(
    std::unordered_map<std::string, mlir::utils::Legalization>
        legalization_maplist) {
  auto p = std::make_unique<ConvertUint8ToInt8LM>();
  p->legalization_maplist = legalization_maplist;
  return p;
}

static PassRegistration<ConvertUint8ToInt8LM> pass;

} // namespace tosa

} // namespace mlir

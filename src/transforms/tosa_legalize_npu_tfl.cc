/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

// Legalize TensorFlow Lite to TOSA

#include <climits>
#include <cstddef>
#include <cstdint>
#include <fstream>
#include <iterator>
#include <numeric>
#include <unordered_set>

#include "mlir/Dialect/Tosa/IR/TosaOps.h"
#include "mlir/Support/LLVM.h"
#include "mlir/Transforms/GreedyPatternRewriteDriver.h"

#include "tensorflow/compiler/mlir/lite/ir/tfl_ops.h"
#include "tensorflow/compiler/mlir/tosa/transforms/legalize_common.h"
#include "tensorflow/compiler/mlir/tosa/transforms/legalize_utils.h"

#include "src/transforms/legalization/tosa_npu/open_source_legalization.h"
#include "src/transforms/passes.h"

#define PASS_NAME "tosa-legalize-npu-tfl"
#define DEBUG_TYPE PASS_NAME
#define HARDSWISH_EXPLICIT_RESCALING false

using namespace mlir::utils;

namespace mlir {
namespace tosa {
namespace {

// Performs lowering to TOSA dialect.
class LegalizeNPUTFL : public PassWrapper<LegalizeNPUTFL, FunctionPass> {
public:
  explicit LegalizeNPUTFL() {}

  StringRef getArgument() const final {
    // This is the argument used to refer to the pass in
    // the textual format (on the commandline for example).
    return PASS_NAME;
  }
  StringRef getDescription() const final {
    // This is a brief description of the pass.
    return "Legalize from TensorFlow Lite to TOSA dialect";
  }

  void runOnFunction() override;

  std::unordered_map<std::string, Legalization> legalization_maplist;
};

#include "src/transforms/open_source_npu_tfl_legalize_patterns.inc"

Legalization getLegalization(
    std::unordered_map<std::string, Legalization> &legalization_maplist,
    std::string op_name) {
  if (legalization_maplist.find(op_name) == legalization_maplist.end()) {
    std::cout << op_name
              << " LegalizeNPUTFL::runOnFunction()::getLegalization() fatal "
                 "error TFLite operation not found"
              << std::endl;
    exit(1);
  }
  return legalization_maplist[op_name];
}

void LegalizeNPUTFL::runOnFunction() {
  auto func = getFunction();
  RewritePatternSet patterns(func.getContext());
  MLIRContext *ctx = func.getContext();

#define DEF_PATTERN_INSERT(PAT) patterns.insert<Convert##PAT##Op>(ctx);
#define DEF_PATTERN_INSERT_AG(PAT) patterns.insert<Generated##PAT>(ctx);

#define DEF_FUNCTION(LEG_MAP, PAT)                                             \
  Legalization ls##PAT = getLegalization(LEG_MAP, #PAT);                       \
  if (ls##PAT == mlir::utils::Legalization::TosaCommon) {                      \
    DEF_PATTERN_INSERT(PAT);                                                   \
  } else if (ls##PAT == mlir::utils::Legalization::ScheduleIRCommon) {         \
  } else if (ls##PAT == mlir::utils::Legalization::Custom) {                   \
  } else if (ls##PAT == mlir::utils::Legalization::TFLite) {                   \
  }

#define DEF_FUNCTION_AG(LEG_MAP, OPE, PAT)                                     \
  Legalization ls##PAT = getLegalization(LEG_MAP, #OPE);                       \
  if (ls##PAT == mlir::utils::Legalization::TosaCommon) {                      \
    DEF_PATTERN_INSERT_AG(PAT);                                                \
  } else if (ls##PAT == mlir::utils::Legalization::ScheduleIRCommon) {         \
  } else if (ls##PAT == mlir::utils::Legalization::Custom) {                   \
  } else if (ls##PAT == mlir::utils::Legalization::TFLite) {                   \
  }

  // These autogenerated signature are missing/renamed after upstream sync
  // TODO: Bring these back
  DEF_FUNCTION_AG(legalization_maplist, TFLAbs, Convert0);
  DEF_FUNCTION_AG(legalization_maplist, TFLCeil, Convert1);
  DEF_FUNCTION_AG(legalization_maplist, TFLFloor, Convert2);
  DEF_FUNCTION_AG(legalization_maplist, TFLExp, Convert3);
  DEF_FUNCTION_AG(legalization_maplist, TFLLog, Convert4);
  DEF_FUNCTION_AG(legalization_maplist, TFLRsqrt, Convert5);
  DEF_FUNCTION_AG(legalization_maplist, TFLLogicalNot, Convert6);
  DEF_FUNCTION_AG(legalization_maplist, TFLCast, Convert7);
  DEF_FUNCTION_AG(legalization_maplist, QuantStats, Convert8);
  DEF_FUNCTION_AG(legalization_maplist, TFLLogicalAnd, Convert9);
  DEF_FUNCTION_AG(legalization_maplist, TFLLogicalOr, Convert10);
  DEF_FUNCTION_AG(legalization_maplist, TFLPow, Convert11);

  DEF_FUNCTION(legalization_maplist, TFLRelu);
  DEF_FUNCTION(legalization_maplist, TFLRelu6);
  DEF_FUNCTION(legalization_maplist, TFLEqual);
  DEF_FUNCTION(legalization_maplist, TFLNotEqual);
  DEF_FUNCTION(legalization_maplist, TFLGreater);
  DEF_FUNCTION(legalization_maplist, TFLGreaterEqual);
  DEF_FUNCTION(legalization_maplist, TFLAdd);
  DEF_FUNCTION(legalization_maplist, TFLSub);
  DEF_FUNCTION(legalization_maplist, TFLMul);
  DEF_FUNCTION(legalization_maplist, TFLSquare);
  DEF_FUNCTION(legalization_maplist, TFLSquaredDifference);
  DEF_FUNCTION(legalization_maplist, TFLDiv);
  DEF_FUNCTION(legalization_maplist, TFLMaximum);
  DEF_FUNCTION(legalization_maplist, TFLMinimum);
  DEF_FUNCTION(legalization_maplist, TFLFloorMod);
  DEF_FUNCTION(legalization_maplist, TFLFloorDiv);
  DEF_FUNCTION(legalization_maplist, TFLAddN);
  DEF_FUNCTION(legalization_maplist, TFLAveragePool2D);
  DEF_FUNCTION(legalization_maplist, TFLMaxPool2D);
  DEF_FUNCTION(legalization_maplist, TFLConcatenation);
  DEF_FUNCTION(legalization_maplist, TFLReshape);
  DEF_FUNCTION(legalization_maplist, TFLRank);
  DEF_FUNCTION(legalization_maplist, TFLShape);
  DEF_FUNCTION(legalization_maplist, TFLExpandDims);
  DEF_FUNCTION(legalization_maplist, TFLSqueeze);
  DEF_FUNCTION(legalization_maplist, TFLFill);
  DEF_FUNCTION(legalization_maplist, TFLElu);
  DEF_FUNCTION(legalization_maplist, TFLSoftmax);
  DEF_FUNCTION(legalization_maplist, TFLSqrt);
  DEF_FUNCTION(legalization_maplist, TFLLogSoftmax);
  DEF_FUNCTION(legalization_maplist, TFLReduceAny);
  DEF_FUNCTION(legalization_maplist, TFLReduceMax);
  DEF_FUNCTION(legalization_maplist, TFLReduceMin);
  DEF_FUNCTION(legalization_maplist, TFLMean);
  DEF_FUNCTION(legalization_maplist, TFLReduceProd);
  DEF_FUNCTION(legalization_maplist, TFLSum);
  DEF_FUNCTION(legalization_maplist, TFLConv2D);
  DEF_FUNCTION(legalization_maplist, TFLTransposeConv);
  DEF_FUNCTION(legalization_maplist, TFLDepthwiseConv2D);
  DEF_FUNCTION(legalization_maplist, TFLFullyConnected);
  DEF_FUNCTION(legalization_maplist, TFLSplit);
  DEF_FUNCTION(legalization_maplist, TFLSplitV);
  DEF_FUNCTION(legalization_maplist, TFLPack);
  DEF_FUNCTION(legalization_maplist, TFLUnpack);
  DEF_FUNCTION(legalization_maplist, TFLTranspose);
  DEF_FUNCTION(legalization_maplist, TFLTile);
  DEF_FUNCTION(legalization_maplist, TFLSlice);
  DEF_FUNCTION(legalization_maplist, TFLStridedSlice);
  DEF_FUNCTION(legalization_maplist, TFLZerosLike);
  DEF_FUNCTION(legalization_maplist, TFLHardSwish);
  DEF_FUNCTION(legalization_maplist, TFLLess);
  DEF_FUNCTION(legalization_maplist, TFLLessEqual);
  DEF_FUNCTION(legalization_maplist, TFLPad);
  DEF_FUNCTION(legalization_maplist, TFLResizeBilinear);
  DEF_FUNCTION(legalization_maplist, TFLResizeNearestNeighbor);
  DEF_FUNCTION(legalization_maplist, TFLSelect);
  DEF_FUNCTION(legalization_maplist, TFLSelectV2);
  DEF_FUNCTION(legalization_maplist, TFLSpaceToBatchNd);
  DEF_FUNCTION(legalization_maplist, TFLBatchToSpaceNd);
  DEF_FUNCTION(legalization_maplist, TFLSpaceToDepth);
  DEF_FUNCTION(legalization_maplist, TFLDepthToSpace);
  DEF_FUNCTION(legalization_maplist, TFLLogistic);
  DEF_FUNCTION(legalization_maplist, TFLTanh);
  DEF_FUNCTION(legalization_maplist, TFLPRelu);
  DEF_FUNCTION(legalization_maplist, TFLLeakyRelu);
  DEF_FUNCTION(legalization_maplist, TFLNeg);
  DEF_FUNCTION(legalization_maplist, TFLYield);
  DEF_FUNCTION(legalization_maplist, TFLCustom);
  DEF_FUNCTION(legalization_maplist, TFLReverseV2);
  DEF_FUNCTION(legalization_maplist, TFLQuantize);
  DEF_FUNCTION(legalization_maplist, TFLDequantize);
  DEF_FUNCTION(legalization_maplist, TFLQConst);
  DEF_FUNCTION(legalization_maplist, TFLGather);
  DEF_FUNCTION(legalization_maplist, TFLGatherNd);
  DEF_FUNCTION(legalization_maplist, TFLOneHot);
  populateWithGenerated(patterns);

  (void)applyPatternsAndFoldGreedily(func, std::move(patterns));
}
} // namespace

std::unique_ptr<OperationPass<FuncOp>> createLegalizeTosaNPUTFLPass(
    std::unordered_map<std::string, mlir::utils::Legalization>
        legalization_maplist) {
  auto p = std::make_unique<LegalizeNPUTFL>();
  p->legalization_maplist = legalization_maplist;
  return p;
}

static PassRegistration<LegalizeNPUTFL> pass;

} // namespace tosa
} // namespace mlir

/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

// fold concat nodes feeding into each other

#include <climits>
#include <cstddef>
#include <cstdint>
#include <iterator>
#include <numeric>

#include "mlir/Dialect/Quant/FakeQuantSupport.h" // TF:local_config_mlir
#include "mlir/Dialect/Quant/UniformSupport.h"   // TF:local_config_mlir
#include "mlir/Dialect/StandardOps/IR/Ops.h"     // TF:local_config_mlir
#include "mlir/IR/Attributes.h"                  // TF:local_config_mlir
#include "mlir/IR/Diagnostics.h"                 // TF:local_config_mlir
#include "mlir/IR/MLIRContext.h"                 // TF:local_config_mlir
#include "mlir/IR/Matchers.h"                    // TF:local_config_mlir
#include "mlir/IR/Operation.h"                   // TF:local_config_mlir
#include "mlir/IR/PatternMatch.h"                // TF:local_config_mlir
#include "mlir/IR/TypeUtilities.h"               // TF:local_config_mlir
#include "mlir/IR/Types.h"                       // TF:local_config_mlir
#include "mlir/Pass/Pass.h"                      // TF:local_config_mlir
#include "mlir/Support/LLVM.h"                   // TF:local_config_mlir
#include "mlir/Transforms/DialectConversion.h"   // TF:local_config_mlir
#include "mlir/Transforms/GreedyPatternRewriteDriver.h"
#include "llvm/ADT/APInt.h"
#include "llvm/ADT/ArrayRef.h"
#include "llvm/ADT/Optional.h"
#include "llvm/ADT/STLExtras.h"
#include "llvm/ADT/StringSwitch.h"

#include "mlir/Dialect/Tosa/IR/TosaOps.h"
//#include "src/transforms/legalize_utils.h"
#include "mlir/Dialect/Tosa/Utils/QuantUtils.h"
#include "src/transforms/passes.h"

#include "src/utils/ir_utils.h"

#define PASS_NAME "tosa-concat-folding"
#define DEBUG_TYPE PASS_NAME

namespace mlir {

namespace tosa {

namespace {
// Performs lowering to Tosa dialect
class ConcatFoldingTosaPass
    : public PassWrapper<ConcatFoldingTosaPass, FunctionPass> {
public:
  explicit ConcatFoldingTosaPass() {}

  StringRef getArgument() const final {
    // This is the argument used to refer to the pass in
    // the textual format (on the commandline for example).
    return PASS_NAME;
  }
  StringRef getDescription() const final {
    // This is a brief description of the pass.
    return "Improve tosa legalization by folding concats into one";
  }

  void runOnFunction() override;
};

struct OptimizeConcatFoldingeOp : public RewritePattern {
  explicit OptimizeConcatFoldingeOp(MLIRContext *context)
      : RewritePattern(tosa::ConcatOp::getOperationName(), 1, context) {}
  LogicalResult matchAndRewrite(Operation *op,
                                PatternRewriter &rewriter) const override {
    int32_t no_concat_operands = 0;
    int32_t concat_operand_index = 0;
    for (int i = 0; i < op->getNumOperands(); i++) {
      mlir::Value v = op->getOperand(i);
      if (llvm::isa<tosa::ConcatOp>(v.getDefiningOp())) {
        no_concat_operands++;
        concat_operand_index = i;
      }
    }

    if (no_concat_operands > 0) {
      auto concat_prod_op =
          op->getOperand(concat_operand_index).getDefiningOp();

      ASSERT_COND(no_concat_operands > 1,
                  "tosa_optimize_concat_folding::OptimizeConcatFoldingeOp::We "
                  "currently support concat to concat on a singl operand only");
      // std::cout << "Rewriting Concat: " << concat_operand_index << std::endl;
      // std::cout << concat_prod_op->getName().getStringRef().str() <<
      // std::endl;

      SmallVector<Value> inputs;
      for (auto v : concat_prod_op->getOperands())
        inputs.push_back(v);
      for (auto v : op->getOperands()) {
        if (v != op->getOperand(concat_operand_index))
          inputs.push_back(v);
      }

      auto concat_new_op = rewriter.create<tosa::ConcatOp>(
          op->getLoc(), op->getResult(0).getType(), inputs,
          op->getAttrOfType<mlir::IntegerAttr>("axis"));

      rewriter.replaceOp(op, concat_new_op.getResult());
      rewriter.eraseOp(concat_prod_op);

      return success();
    } else
      return failure();
  }
};

void ConcatFoldingTosaPass::runOnFunction() {
  auto *ctx = &getContext();
  auto func = getFunction();

  OwningRewritePatternList patterns(ctx);

  // Add the generated patterns to the list.
  patterns.insert<OptimizeConcatFoldingeOp>(ctx);
  applyPatternsAndFoldGreedily(func, std::move(patterns));
}

} // anonymous namespace

std::unique_ptr<OperationPass<FuncOp>> CreateConcatFoldingTosaPass() {
  return std::make_unique<ConcatFoldingTosaPass>();
}

static PassRegistration<ConcatFoldingTosaPass> pass;

} // namespace tosa

} // namespace mlir

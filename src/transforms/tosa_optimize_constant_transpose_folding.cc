/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

// folding constant + tosa.reshape into constant at compile time

#include <climits>
#include <cstddef>
#include <cstdint>
#include <iterator>
#include <numeric>

#include "mlir/Dialect/Quant/FakeQuantSupport.h" // TF:local_config_mlir
#include "mlir/Dialect/Quant/UniformSupport.h"   // TF:local_config_mlir
#include "mlir/Dialect/StandardOps/IR/Ops.h"     // TF:local_config_mlir
#include "mlir/IR/Attributes.h"                  // TF:local_config_mlir
#include "mlir/IR/Diagnostics.h"                 // TF:local_config_mlir
#include "mlir/IR/MLIRContext.h"                 // TF:local_config_mlir
#include "mlir/IR/Matchers.h"                    // TF:local_config_mlir
#include "mlir/IR/Operation.h"                   // TF:local_config_mlir
#include "mlir/IR/PatternMatch.h"                // TF:local_config_mlir
#include "mlir/IR/TypeUtilities.h"               // TF:local_config_mlir
#include "mlir/IR/Types.h"                       // TF:local_config_mlir
#include "mlir/Pass/Pass.h"                      // TF:local_config_mlir
#include "mlir/Support/LLVM.h"                   // TF:local_config_mlir
#include "mlir/Transforms/DialectConversion.h"   // TF:local_config_mlir
#include "mlir/Transforms/GreedyPatternRewriteDriver.h"
#include "llvm/ADT/APInt.h"
#include "llvm/ADT/ArrayRef.h"
#include "llvm/ADT/Optional.h"
#include "llvm/ADT/STLExtras.h"
#include "llvm/ADT/StringSwitch.h"

#include "mlir/Dialect/Tosa/IR/TosaOps.h"
//#include "src/transforms/legalize_utils.h"
#include "mlir/Dialect/Tosa/Utils/QuantUtils.h"
#include "src/transforms/passes.h"

#include "src/utils/ir_utils.h"

#define PASS_NAME "tosa-constant-transpose-folding"
#define DEBUG_TYPE PASS_NAME

llvm::cl::opt<bool> tosa_force_copy_constant_tosa_folding(
    "tosa_force_copy_constant_tosa_folding", llvm::cl::init(false),
    llvm::cl::desc(
        "When constant folding's input constant has multiple uses, force "
        "to creates new constant. Default: false"));

namespace mlir {

namespace tosa {

namespace {
// Performs lowering to Tosa dialect
class ConstantTransposeFoldingTosaPass
    : public PassWrapper<ConstantTransposeFoldingTosaPass, FunctionPass> {
public:
  explicit ConstantTransposeFoldingTosaPass() {}

  StringRef getArgument() const final {
    // This is the argument used to refer to the pass in
    // the textual format (on the commandline for example).
    return PASS_NAME;
  }
  StringRef getDescription() const final {
    // This is a brief description of the pass.
    return "Improve tosa legalization by folding constant being transposed";
  }

  void runOnFunction() override;
};

struct OptimizeConstantTransposeFoldingeOp : public RewritePattern {
  explicit OptimizeConstantTransposeFoldingeOp(MLIRContext *context)
      : RewritePattern(tosa::DepthwiseConv2DOp::getOperationName(), 1,
                       context) {}
  LogicalResult matchAndRewrite(Operation *op,
                                PatternRewriter &rewriter) const override {
    // Detect [Const->TransPose->Reshape->Depthwise] and replace with
    //        [Const->Depthwise]

    // Detect [Const->TransPose->Depthwise] and replace with
    //        [Const->Depthwise]

    Operation *const_op = nullptr;

    Operation *level_0 = op->getOperand(1).getDefiningOp();
    if (llvm::isa<tosa::TransposeOp>(level_0)) {
      const_op = level_0->getOperand(0).getDefiningOp();
    } else if (llvm::isa<tosa::ReshapeOp>(level_0)) {
      Operation *level_1 = level_0->getOperand(0).getDefiningOp();
      if (llvm::isa<tosa::TransposeOp>(level_1)) {
        const_op = level_1->getOperand(0).getDefiningOp();
      } else
        return failure();
    } else
      return failure();

    if (not(llvm::isa<tosa::ConstOp>(const_op)))
      return failure();

    RankedTensorType output_type =
        const_op->getResult(0).getType().dyn_cast<RankedTensorType>();

    // 3. Make sure that the Output from the Const is ohwi by checking
    //     A. shape[0] == 1
    ASSERT_COND(output_type.getShape()[0] != 1,
                "tosa_optimize_constant_transpose_folding::Shape dimension 0 "
                "is expected to be 1 (Depthwise Multiplier)");

#if 0
    RankedTensorType const_output_l =
        const_op->getResult(0).getType().dyn_cast<RankedTensorType>();
    RankedTensorType transpose_output_l =
        transpose_op->getResult(0).getType().dyn_cast<RankedTensorType>();
    RankedTensorType reshape_output_l =
        op->getResult(0).getType().dyn_cast<RankedTensorType>();
    std::cout << "Const:     " << const_output_l.getShape()[0] << " "
              << const_output_l.getShape()[1] << " "
              << const_output_l.getShape()[2] << " "
              << const_output_l.getShape()[3] << std::endl;
    std::cout << "Transpose: " << transpose_output_l.getShape()[0] << " "
              << transpose_output_l.getShape()[1] << " "
              << transpose_output_l.getShape()[2] << " "
              << transpose_output_l.getShape()[3] << std::endl;
    std::cout << "Reshape:   " << reshape_output_l.getShape()[0] << " "
              << reshape_output_l.getShape()[1] << " "
              << reshape_output_l.getShape()[2] << " "
              << reshape_output_l.getShape()[3] << std::endl;
#endif

    StringRef d("value");
    DenseElementsAttr data =
        const_op->getAttrOfType<::mlir::DenseElementsAttr>(d);
    auto qconst_op =
        rewriter.create<tosa::ConstOp>(op->getLoc(), output_type, data);

    // Replace ReshapeOp with a Const
    rewriter.replaceOp(level_0, {qconst_op->getResult(0)});
    return success();
  }
};

void ConstantTransposeFoldingTosaPass::runOnFunction() {
  auto *ctx = &getContext();
  auto func = getFunction();

  OwningRewritePatternList patterns(ctx);

  // Add the generated patterns to the list.
  patterns.insert<OptimizeConstantTransposeFoldingeOp>(ctx);
  applyPatternsAndFoldGreedily(func, std::move(patterns));
}

} // anonymous namespace

std::unique_ptr<OperationPass<FuncOp>>
CreateConstantTransposeFoldingTosaPass() {
  return std::make_unique<ConstantTransposeFoldingTosaPass>();
}

static PassRegistration<ConstantTransposeFoldingTosaPass> pass;

} // namespace tosa

} // namespace mlir

/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#ifndef ASSERT_H
#define ASSERT_H

#include <iostream>
#include <string>

void end_program(bool condition, std::string message);

void ASSERT_COND(bool condition, std::string message);
//#if ASSERTS_ENABLED
//#define ASSERT_COND(condition, message) end_program(condition, message);
//#else
//#define ASSERT_COND(condition, message) {}
//#endif

#define FATAL_ERROR(message) end_program(true, message);

#endif // ASSERT_H

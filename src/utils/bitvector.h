/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#ifndef BITVECTOR_H
#define BITVECTOR_H

#include <array>
#include <bitset>
#include <cassert>
#include <climits>
#include <cstring>
#include <ostream>
#include <string>
#include <vector>

#include "mlir/Dialect/Quant/QuantOps.h"
#include "mlir/Dialect/Quant/QuantTypes.h"
#include "mlir/Dialect/StandardOps/IR/Ops.h"      // from @llvm-project
#include "mlir/IR/AffineMap.h"                    // from @llvm-project
#include "mlir/IR/Attributes.h"                   // from @llvm-project
#include "mlir/IR/BlockAndValueMapping.h"         // from @llvm-project
#include "mlir/IR/Builders.h"                     // from @llvm-project
#include "mlir/IR/Location.h"                     // from @llvm-project
#include "mlir/IR/MLIRContext.h"                  // from @llvm-project
#include "mlir/IR/Operation.h"                    // from @llvm-project
#include "mlir/IR/PatternMatch.h"                 // from @llvm-project
#include "mlir/Interfaces/InferTypeOpInterface.h" // from @llvm-project
#include "mlir/Pass/Pass.h"                       // from @llvm-project
#include "mlir/Transforms/DialectConversion.h"    // from @llvm-project

class BitVector {
public:
  using BlockType = unsigned long long;

  explicit BitVector(size_t num_bits = 1, BlockType value = 0) {
    setup(unsigned(num_bits));
    vec[0] = value;
    for (unsigned i = 1; i < n_blocks; ++i) {
      vec[i] = 0;
    }
  }

  BitVector(const BitVector &o) {
    setup(o.n_bits);
    std::memcpy(vec, o.vec, n_blocks * sizeof(BlockType));
  }

  BitVector(BitVector &&o) noexcept {
    if (o.vec != &o.vec_storage[0]) {
      vec = o.vec;
      o.vec = nullptr;
    } else {
      vec = &vec_storage[0];
      std::memcpy(vec, o.vec, o.n_blocks * sizeof(BlockType));
    }
    n_bits = o.n_bits;
    n_blocks = o.n_blocks;
  }

  explicit BitVector(const std::string &s, char zero = '0', char one = '1') {
    setup(unsigned(s.size()));
    for (unsigned i = 0; i < n_blocks; ++i) {
      vec[i] = 0;
    }
    unsigned idx = n_bits - 1;
    for (char elem : s) {
      set(idx, elem == one);
      --idx;
    }
  }

  void setup(unsigned num_bits) {
    n_bits = num_bits;
    n_blocks = blocks_for_bits(n_bits);
    if (n_blocks <= vec_storage.size()) {
      vec = &vec_storage[0];
    } else {
      vec = new BlockType[n_blocks];
    }
  }

  BitVector &operator=(BitVector &&o) noexcept {
    if (&o != this) {
      if (vec != &vec_storage[0]) {
        delete[] vec;
      }
      if (o.vec != &o.vec_storage[0]) {
        vec = o.vec;
        o.vec = nullptr;
      } else {
        vec = &vec_storage[0];
        std::memcpy(vec, o.vec, o.n_blocks * sizeof(BlockType));
      }
      n_bits = o.n_bits;
      n_blocks = o.n_blocks;
    }
    return *this;
  }

  BitVector &operator=(const BitVector &o) {
    if (&o != this) {
      if (vec != &vec_storage[0]) {
        delete[] vec;
      }
      setup(o.n_bits);
      std::memcpy(vec, o.vec, n_blocks * sizeof(BlockType));
    }
    return *this;
  }

  ~BitVector() {
    if (vec != &vec_storage[0]) {
      delete[] vec;
    }
  }

  static BitVector zeros(size_t num_bits) { return BitVector(num_bits, 0); }

  static BitVector ones(size_t num_bits) { return ~BitVector(num_bits, 0); }

  static BitVector singleton_set(size_t num_bits, size_t idx) {
    BitVector res(num_bits);
    res.set(idx);
    return res;
  }

  static BitVector all_below(size_t num_bits, size_t idx) {
    BitVector res = singleton_set(num_bits, idx);
    res -= BitVector(num_bits, 1);
    return res;
  }

  BitVector operator~() const {
    BitVector res(*this);
    for (unsigned i = 0; i < n_blocks; ++i) {
      res.vec[i] = ~res.vec[i];
    }
    res.normalise_repr();

    return res;
  }

  BitVector &operator+=(const BitVector &o) {
    assert(n_bits == o.n_bits);
    BlockType carry = 0;
    for (unsigned i = 0; i < n_blocks; ++i) {
      BlockType orig = vec[i];
      vec[i] += o.vec[i] + carry;
      carry = (vec[i] < orig);
    }
    normalise_repr();
    return *this;
  }

  BitVector &operator-=(const BitVector &o) {
    assert(n_bits == o.n_bits);
    BlockType borrow = 0;
    for (unsigned i = 0; i < n_blocks; ++i) {
      BlockType orig = vec[i];
      vec[i] -= o.vec[i] + borrow;
      borrow = (vec[i] > orig);
    }
    normalise_repr();
    return *this;
  }

  BitVector &operator&=(const BitVector &o) {
    assert(n_bits == o.n_bits);
    for (unsigned i = 0; i < n_blocks; ++i) {
      vec[i] &= o.vec[i];
    }
    return *this;
  }

  BitVector &operator|=(const BitVector &o) {
    assert(n_bits == o.n_bits);
    for (unsigned i = 0; i < n_blocks; ++i) {
      vec[i] |= o.vec[i];
    }
    return *this;
  }

  BitVector &operator^=(const BitVector &o) {
    assert(n_bits == o.n_bits);
    for (unsigned i = 0; i < n_blocks; ++i) {
      vec[i] ^= o.vec[i];
    }
    return *this;
  }

  BitVector operator+(const BitVector &o) const {
    assert(n_bits == o.n_bits);
    BitVector res = *this;
    res += o;
    return res;
  }

  BitVector operator-(const BitVector &o) const {
    assert(n_bits == o.n_bits);
    BitVector res = *this;
    res -= o;
    return res;
  }

  BitVector operator&(const BitVector &o) const {
    assert(n_bits == o.n_bits);
    BitVector res = *this;
    res &= o;
    return res;
  }

  BitVector operator|(const BitVector &o) const {
    assert(n_bits == o.n_bits);
    BitVector res = *this;
    res |= o;
    return res;
  }

  BitVector operator^(const BitVector &o) const {
    assert(n_bits == o.n_bits);
    BitVector res = *this;
    res ^= o;
    return res;
  }

  bool operator<(const BitVector &o) const {
    assert(n_bits == o.n_bits);
    for (int i = n_blocks - 1; i >= 0; --i) {
      if (vec[i] != o.vec[i]) {
        return vec[i] < o.vec[i];
      }
    }
    return false;
  }

  size_t count() const { return n_bits; }

  bool operator[](size_t idx) const {
    size_t word = idx / n_bits_per_block;
    size_t offset = idx % n_bits_per_block; // should all turn into bitwise ops
    return bool((vec[word] >> offset) & 0x1);
  }

  bool test(size_t idx) const {
    size_t word = idx / n_bits_per_block;
    size_t offset = idx % n_bits_per_block; // should all turn into bitwise ops
    return bool((vec[word] >> offset) & 0x1);
  }

  void set(size_t idx, bool val = true) {
    size_t word = idx / n_bits_per_block;
    size_t offset = idx % n_bits_per_block; // should all turn into bitwise ops

    BlockType mask = BlockType(1UL) << offset;
    vec[word] = (vec[word] & ~mask) | (BlockType(val) << offset);
  }

  void clear(size_t idx) { set(idx, false); }

  void flip(size_t idx) {
    size_t word = idx / n_bits_per_block;
    size_t offset = idx % n_bits_per_block; // should all turn into bitwise ops

    BlockType mask = BlockType(1UL) << offset;
    vec[word] ^= mask;
  }

  bool operator==(const BitVector &o) const {
    assert(n_bits == o.n_bits);
    for (unsigned i = 0; i < n_blocks; ++i) {
      if (vec[i] != o.vec[i])
        return false;
    }
    return true;
  }

  bool operator!=(const BitVector &o) const { return !(*this == o); }

  bool any() const {
    BlockType res = 0;
    for (unsigned i = 0; i < n_blocks; ++i) {
      res |= vec[i];
    }
    return res != 0;
  }

  bool none() const { return !any(); }

  bool all() const {
    if (n_bits == 0)
      return false;
    constexpr BlockType all_bits = ~BlockType(0);
    for (unsigned i = 0; i < n_blocks - 1; ++i) {
      if (vec[i] != all_bits)
        return false;
    }
    const size_t last_n_bits =
        n_bits % n_bits_per_block; // should all turn into bitwise ops
    const BlockType last_bits = (BlockType(1) << last_n_bits) - 1;
    return vec[n_blocks - 1] == last_bits;
  }

  unsigned bsf() const {
    for (unsigned i = 0; i < n_blocks; ++i) {
      if (vec[i] != 0)
        return unsigned(__builtin_ctzll(vec[i])) + i * n_bits_per_block;
    }
    return ~0U;
  }

  unsigned bsr() const {
    for (int i = int(n_blocks - 1); i >= 0; --i) {
      if (vec[i] != 0)
        return unsigned(i + 1) * (n_bits_per_block) -
               unsigned(__builtin_clzll(vec[i])) - 1;
    }
    return ~0U;
  }

  bool contains(const BitVector &o) const { return (*this & o) == o; }

  size_t popcount() const {
    size_t res = 0;
    for (unsigned i = 0; i < n_blocks; ++i) {
      res += std::bitset<n_bits_per_block>(vec[i]).count();
    }
    return res;
  }

  BlockType to_ulong() const {
    for (unsigned i = 1; i < n_blocks; ++i) {
      assert(vec[i] == 0);
    }
    return vec[0];
  }
  std::string to_string() const { return to_string_explicit(); }

  // std::string to_string_explicit(char zero = '0', char one = '1') const
  //{
  //    std::string res;
  //    for(int i = int(n_bits-1); i >= 0; --i)
  //    {
  //        res += (*this)[unsigned(i)] ? one : zero;
  //    }
  //    return res;
  //}

  std::string to_string_explicit(char zero = '0', char one = '1') const {
    std::string res;
    for (int i = 0; i < n_bits; i++)
      res += (*this)[unsigned(i)] ? one : zero;
    return res;
  }

  size_t num_blocks() const { return n_blocks; }
  BlockType get_block(size_t idx) const { return vec[idx]; }

  size_t get_n_bits() const { return n_bits; }

  /*
    method for iterating through all subsets of full_set. start with a null set,
    call successor until you get null again.

    From
    Rapid Bushy Join-order Optimization with Cartesian Products
    Vance and Maier
    http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.49.6826&rep=rep1&type=pdf

    Also, an easier explanation:
    https://www.chessprogramming.org/Traversing_Subsets_of_a_Set#All_Subsets_of_any_Set
   */
  static BitVector successor(BitVector curr, BitVector full_set) {
    return full_set & (curr - full_set);
  }

  BitVector first_subset() const {
    return next_subset(BitVector::zeros(n_bits));
  }

  BitVector next_subset(BitVector curr_subset) const {
    return *this & (curr_subset - *this);
  }

  bool is_subset_of(const BitVector &full_set) const {
    return (*this & full_set) == *this;
  }

  bool is_singleton() const {
    return (*this & (*this - BitVector(n_bits, 1))).none();
  }

  bool overlaps(const BitVector &o) const {
    assert(n_bits == o.n_bits);
    for (unsigned i = 0; i < n_blocks; ++i) {
      if (vec[i] & o.vec[i])
        return true;
    }
    return false;
  }

private:
  void normalise_repr() {
    const size_t last_n_bits = n_bits % n_bits_per_block;
    if (last_n_bits != 0) {
      const BlockType last_mask = (BlockType(1) << last_n_bits) - 1;
      vec[n_blocks - 1] &= last_mask;
    }
  }

  static unsigned blocks_for_bits(unsigned num_bits) {
    return (num_bits + n_bits_per_block - 1) / n_bits_per_block;
  }

  unsigned n_bits;
  unsigned n_blocks;
  BlockType *vec;
  std::array<BlockType, 6> vec_storage;

  static constexpr size_t n_bits_per_block = sizeof(BlockType) * CHAR_BIT;
};

namespace std {
template <> class hash<BitVector> {
public:
  size_t operator()(const BitVector &bv) const {
    size_t h = 0;
    for (size_t i = 0; i < bv.num_blocks(); ++i) {
      h *= 13;
      h ^= hash<BitVector::BlockType>()(bv.get_block(i));
    }
    return h;
  }
};
} // namespace std

std::ostream &operator<<(std::ostream &os, const BitVector &bv);

using BitVectorLookup = std::map<mlir::Operation *, BitVector>;

#endif // BITVECTOR_H

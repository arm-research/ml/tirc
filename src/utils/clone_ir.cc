/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#include "clone_ir.h"

namespace mlir {
namespace utils {

#define ENABLE_LOGGING 0

// This is expensive and we need to find a better way especially if weights are
// involved not to deep copy !!!
Operation *clone_op(OpBuilder &builder, Block *block,
                    Block::iterator insertPoint, Operation *op,
                    SmallVector<value_slot, TYPICAL_NO_INPUTS> &input_list) {
  builder.setInsertionPoint(block, insertPoint);

  mlir::Location loc = builder.getUnknownLoc();

  Operation *op_new;

  /* SCHEDULE IR OPS */

  if (llvm::isa<scheduleir::YieldOp>(op)) {
#if ENABLE_LOGGING
    std::cout << "Cloning YieldOp" << std::endl;
#endif
    SmallVector<Value, TYPICAL_NO_INPUTS> inputs;
    for (auto vs : input_list)
      inputs.push_back(vs.second);
    op_new = builder.create<scheduleir::YieldOp>(loc, inputs);
  } else if (llvm::isa<scheduleir::DmaOp>(op)) {
#if ENABLE_LOGGING
    std::cout << "Cloning DmaOp" << std::endl;
#endif
    Type resultType = op->getResult(0).getType();
    Value input = input_list[0].second;
    op_new = builder.create<scheduleir::DmaOp>(loc, resultType, input);
  } else if (llvm::isa<scheduleir::SliceViewOp>(op)) {
#if ENABLE_LOGGING
    std::cout << "Cloning SliceViewOp" << std::endl;
#endif
    Type resultType = op->getResult(0).getType();
    Value input = input_list[0].second;
    op_new = builder.create<scheduleir::SliceViewOp>(loc, resultType, input);
  } else if (llvm::isa<scheduleir::MemoryOp>(op)) {
#if ENABLE_LOGGING
    std::cout << "Cloning MemoryOp" << std::endl;
#endif

    Region &region_memory = op->getRegion(0);
    Block &block_memory = region_memory.front();
    llvm::iplist<Operation> &operations_memory_region =
        block_memory.getOperations();

    SmallVector<mlir::Value> inputs;
    SmallVector<mlir::Type> inputs_types;
    for (auto vs : input_list) {
      inputs.push_back(vs.second);
      inputs_types.push_back(vs.second.getType());
    }
    SmallVector<mlir::Type> outputs;
    for (auto vs : op->getOpResults())
      outputs.push_back(vs.getType());
    op_new = builder.create<scheduleir::MemoryOp>(loc, outputs, inputs);
    Region &region_instruction = op_new->getRegion(0);
    Block *block = builder.createBlock(
        &region_instruction, region_instruction.begin(), inputs_types);

    ASSERT_COND(operations_memory_region.size() != 2,
                "clone_ops.cc::A Memory Region Holds should hold two Ops "
                "including a Yield");
    SmallVector<value_slot, TYPICAL_NO_INPUTS> input_list;
    for (int i = 0; i < block->getNumArguments(); i++) {
      value_slot v(i, block->getArgument(i));
      input_list.push_back(v);
    }
    for (auto &op : operations_memory_region) {
      Block::iterator insertPoint = block->end();
      auto new_op = clone_op(builder, block, insertPoint, &op, input_list);
      /* Load outputs of new_op into the input_list */
      input_list.clear();
      for (int i = 0; i < new_op->getNumResults(); i++) {
        value_slot v(i, new_op->getResult(i));
        input_list.push_back(v);
      }
    }
  } else if (llvm::isa<scheduleir::ConcatOp>(op)) {
#if ENABLE_LOGGING
    std::cout << "Cloning ConcatOp" << std::endl;
#endif
    Type resultType = op->getResult(0).getType();
    ::mlir::IntegerAttr axis =
        op->getAttrOfType<::mlir::IntegerAttr>(StringRef("axis"));
    SmallVector<mlir::Value> inputs;
    for (auto i : input_list)
      inputs.push_back(i.second);
    op_new =
        builder.create<scheduleir::ConcatOp>(loc, resultType, inputs, axis);
  } else if (llvm::isa<scheduleir::ReshapeOp>(op)) {
#if ENABLE_LOGGING
    std::cout << "Cloning ReshapeOp" << std::endl;
#endif
    Type resultType = op->getResult(0).getType();
    ::mlir::ArrayAttr new_shape =
        op->getAttrOfType<::mlir::ArrayAttr>(StringRef("new_shape"));
    op_new = builder.create<scheduleir::ReshapeOp>(
        loc, resultType, input_list[0].second, new_shape);
  }

  /* TOSA OPS */

  else if (llvm::isa<tosa::ConstOp>(op)) {
#if ENABLE_LOGGING
    std::cout << "Cloning ConstOp" << std::endl;
#endif
    Type resultType = op->getResult(0).getType();
    DenseElementsAttr data =
        op->getAttrOfType<mlir::DenseElementsAttr>(StringRef("value"));

    op_new = builder.create<tosa::ConstOp>(loc, resultType, data);

    /* Copy over attributes (Compressed Values)*/
    auto attrs_old = op->getAttrDictionary();
    op_new->setAttrs(attrs_old);
  } else if (llvm::isa<tosa::RescaleOp>(op)) {
#if ENABLE_LOGGING
    std::cout << "Cloning RescaleOp" << std::endl;
#endif
    SmallVector<Type, TYPICAL_NO_OUTPUTS> resultTypes;
    resultTypes.push_back(op->getResult(0).getType());
    ::mlir::Value input = input_list[0].second;
    ::mlir::IntegerAttr input_zp =
        op->getAttrOfType<::mlir::IntegerAttr>(StringRef("input_zp"));
    ::mlir::IntegerAttr output_zp =
        op->getAttrOfType<::mlir::IntegerAttr>(StringRef("output_zp"));
    ::mlir::ArrayAttr multiplier =
        op->getAttrOfType<::mlir::ArrayAttr>(StringRef("multiplier"));
    ::mlir::ArrayAttr shift =
        op->getAttrOfType<::mlir::ArrayAttr>(StringRef("shift"));
    ::mlir::BoolAttr scale32 =
        op->getAttrOfType<::mlir::BoolAttr>(StringRef("scale32"));
    ::mlir::BoolAttr double_round =
        op->getAttrOfType<::mlir::BoolAttr>(StringRef("double_round"));
    ::mlir::BoolAttr per_channel =
        op->getAttrOfType<::mlir::BoolAttr>(StringRef("per_channel"));
    op_new = builder.create<tosa::RescaleOp>(
        loc, resultTypes, input, input_zp, output_zp, multiplier, shift,
        scale32, double_round, per_channel);
  } else if (llvm::isa<tosa::Conv2DOp>(op)) {
#if ENABLE_LOGGING
    std::cout << "Cloning Conv2DOp" << std::endl;
#endif
    // ToDo::Input List is it ordered and correct ?
    Type resultType = op->getResult(0).getType();
    Value input = input_list[0].second;
    Value filter = input_list[1].second;
    Value bias = input_list[2].second;
    ::mlir::ArrayAttr stride =
        op->getAttrOfType<::mlir::ArrayAttr>(StringRef("stride"));
    ::mlir::ArrayAttr dilation =
        op->getAttrOfType<::mlir::ArrayAttr>(StringRef("dilation"));
    ::mlir::ArrayAttr pad =
        op->getAttrOfType<::mlir::ArrayAttr>(StringRef("pad"));
    op_new = builder.create<tosa::Conv2DOp>(loc, resultType, input, filter,
                                            bias, pad, stride, dilation);
  } else if (llvm::isa<tosa::DepthwiseConv2DOp>(op)) {
#if ENABLE_LOGGING
    std::cout << "Cloning DepthwiseConst2DOp" << std::endl;
#endif
    // ToDo::Input List is it ordered and correct ?
    Type resultType = op->getResult(0).getType();
    Value input = input_list[0].second;
    Value filter = input_list[1].second;
    Value bias = input_list[2].second;
    ::mlir::ArrayAttr stride =
        op->getAttrOfType<::mlir::ArrayAttr>(StringRef("stride"));
    ::mlir::ArrayAttr dilation =
        op->getAttrOfType<::mlir::ArrayAttr>(StringRef("dilation"));
    ::mlir::ArrayAttr pad =
        op->getAttrOfType<::mlir::ArrayAttr>(StringRef("pad"));
    op_new = builder.create<tosa::DepthwiseConv2DOp>(
        loc, resultType, input, filter, bias, pad, stride, dilation);
  } else if (llvm::isa<tosa::FullyConnectedOp>(op)) {
#if ENABLE_LOGGING
    std::cout << "Cloning FullyConnectedOp" << std::endl;
#endif
    // ToDo::Input List is it ordered and correct ?
    Type resultType = op->getResult(0).getType();
    Value input = input_list[0].second;
    Value filter = input_list[1].second;
    Value bias = input_list[2].second;
    op_new = builder.create<tosa::FullyConnectedOp>(loc, resultType, input,
                                                    filter, bias);
  } else if (llvm::isa<tosa::MaxPool2dOp>(op)) {
#if ENABLE_LOGGING
    std::cout << "Cloning MaxPool2dOp" << std::endl;
#endif
    Type resultType = op->getResult(0).getType();
    Value input = input_list[0].second;
    ::mlir::ArrayAttr kernel =
        op->getAttrOfType<::mlir::ArrayAttr>(StringRef("kernel"));
    ::mlir::ArrayAttr stride =
        op->getAttrOfType<::mlir::ArrayAttr>(StringRef("stride"));
    ::mlir::ArrayAttr pad =
        op->getAttrOfType<::mlir::ArrayAttr>(StringRef("pad"));
    op_new = builder.create<tosa::MaxPool2dOp>(loc, resultType, input, kernel,
                                               stride, pad);
  } else if (llvm::isa<tosa::TransposeOp>(op)) {
#if ENABLE_LOGGING
    std::cout << "Cloning TransposeOp" << std::endl;
#endif
    Type resultType = op->getResult(0).getType();
    Value input = input_list[0].second;
    Value perm = input_list[1].second;
    op_new = builder.create<tosa::TransposeOp>(loc, resultType, input, perm);
  } else if (llvm::isa<tosa::ReshapeOp>(op)) {
#if ENABLE_LOGGING
    std::cout << "Cloning ReshapeOp" << std::endl;
#endif
    Type resultType = op->getResult(0).getType();
    Value input = input_list[0].second;
    ::mlir::ArrayAttr shape =
        op->getAttrOfType<::mlir::ArrayAttr>(StringRef("shape"));
    op_new = builder.create<tosa::ReshapeOp>(loc, resultType, input, shape);
  } else if (llvm::isa<tosa::AvgPool2dOp>(op)) {
#if ENABLE_LOGGING
    std::cout << "Cloning AvgPool2dOp" << std::endl;
#endif
    Type resultType = op->getResult(0).getType();
    Value input = input_list[0].second;
    ::mlir::ArrayAttr kernel =
        op->getAttrOfType<::mlir::ArrayAttr>(StringRef("kernel"));
    ::mlir::ArrayAttr stride =
        op->getAttrOfType<::mlir::ArrayAttr>(StringRef("stride"));
    ::mlir::ArrayAttr pad =
        op->getAttrOfType<::mlir::ArrayAttr>(StringRef("pad"));
    auto quantization_info =
        op->getAttrOfType<::mlir::tosa::UnaryOpQuantizationAttr>(
            StringRef("quantization_info"));
    op_new = builder.create<tosa::AvgPool2dOp>(loc, resultType, input, kernel,
                                               stride, pad, quantization_info);
  } else if (llvm::isa<tosa::AddOp>(op)) {
#if ENABLE_LOGGING
    std::cout << "Cloning AddOp" << std::endl;
#endif
    // ToDo::Input List is it ordered and correct ?
    Type resultType = op->getResult(0).getType();
    op_new = builder.create<tosa::AddOp>(loc, resultType, input_list[0].second,
                                         input_list[1].second);
  } else if (llvm::isa<tosa::ConcatOp>(op)) {
#if ENABLE_LOGGING
    std::cout << "Cloning ConcatOp" << std::endl;
#endif
    Type resultType = op->getResult(0).getType();
    ::mlir::IntegerAttr axis =
        op->getAttrOfType<::mlir::IntegerAttr>(StringRef("axis"));
    SmallVector<mlir::Value> inputs;
    for (auto i : input_list)
      inputs.push_back(i.second);
    op_new = builder.create<tosa::ConcatOp>(loc, resultType, inputs, axis);
  } else if (llvm::isa<tosa::ReluNOp>(op)) {
#if ENABLE_LOGGING
    std::cout << "Cloning ReluNOp" << std::endl;
#endif
    Type resultType = op->getResult(0).getType();
    Value input = input_list[0].second;
    ::mlir::IntegerAttr max_int =
        op->getAttrOfType<::mlir::IntegerAttr>(StringRef("max_int"));
    ::mlir::FloatAttr max_fp =
        op->getAttrOfType<::mlir::FloatAttr>(StringRef("max_fp"));
    op_new =
        builder.create<tosa::ReluNOp>(loc, resultType, input, max_int, max_fp);
  } else if (llvm::isa<tosa::ClampOp>(op)) {
#if ENABLE_LOGGING
    std::cout << "Cloning ClampOp" << std::endl;
#endif
    Type resultType = op->getResult(0).getType();
    Value input = input_list[0].second;
    ::mlir::IntegerAttr min_int =
        op->getAttrOfType<::mlir::IntegerAttr>(StringRef("min_int"));
    ::mlir::IntegerAttr max_int =
        op->getAttrOfType<::mlir::IntegerAttr>(StringRef("max_int"));
    ::mlir::FloatAttr min_fp =
        op->getAttrOfType<::mlir::FloatAttr>(StringRef("min_fp"));
    ::mlir::FloatAttr max_fp =
        op->getAttrOfType<::mlir::FloatAttr>(StringRef("max_fp"));
    op_new = builder.create<tosa::ClampOp>(loc, resultType, input, min_int,
                                           max_int, min_fp, max_fp);
  } else
    ASSERT_COND(
        true,
        "cluster_ops::clone_op::FatalError::Node Cloning Not Supported: " +
            std::string(getOpName(*op)));

  if (op->getAttr("block_type")) {
    unsigned value =
        op->getAttrOfType<mlir::IntegerAttr>("block_type").getInt();
    op_new->setAttr("block_type", builder.getI64IntegerAttr(value));
  }
  if (op->getAttr("is_unary")) {
    unsigned value = op->getAttrOfType<mlir::IntegerAttr>("is_unary").getInt();
    op_new->setAttr("is_unary", builder.getBoolAttr(value));
  }

  return op_new;
}

} // namespace utils
} // namespace mlir

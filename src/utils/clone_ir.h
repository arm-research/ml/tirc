/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#ifndef CLONE_OPS_H
#define CLONE_OPS_H

#include "absl/memory/memory.h"
#include "mlir/IR/AffineMap.h"   // from @llvm-project
#include "mlir/IR/Attributes.h"  // from @llvm-project
#include "mlir/IR/Location.h"    // from @llvm-project
#include "mlir/IR/MLIRContext.h" // from @llvm-project

#include <vector>

#include "src/utils/dag.h"

#include "mlir/Dialect/Tosa/IR/TosaOps.h"
#include "src/ir/cascade_ir.h"
#include "src/ir/kernel_ir.h"
#include "src/ir/llcs_ir.h"
#include "src/ir/schedule_ir.h"

#include "src/ir/architecture_config/architecture_config.h"
#include "src/utils/assert.h"
#include "src/utils/checkIR.h"
#include "src/utils/logging.h"
#include "src/utils/printIR.h"

namespace mlir {
namespace utils {

using value_slot = std::pair<SLOT_INDEX, Value>;

Operation *clone_op(OpBuilder &builder, Block *block,
                    Block::iterator insertPoint, Operation *op,
                    SmallVector<value_slot, TYPICAL_NO_INPUTS> &input_list);

} // namespace utils
} // namespace mlir
#endif // CLONE_OPS_H

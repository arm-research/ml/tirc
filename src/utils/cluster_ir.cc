/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#include "cluster_ir.h"

namespace mlir {
namespace utils {

#define DISABLE_IR_CHECKS 0
#define ENABLE_LOGGING 0

struct ArgumentInfo {
  bool block_or_op;
  Block *external_block;
  Operation *external_op;
  unsigned int external_block_op_index;
  unsigned int block_index;
};
int findBlockArgumentIndex(bool block_or_op, Block *external_block,
                           Operation *external_op,
                           unsigned int external_block_op_index,
                           std::vector<ArgumentInfo> &arguments_origin) {
  for (auto &bao : arguments_origin) {
    if ((bao.block_or_op == block_or_op) and
        (bao.external_block == external_block) and
        (bao.external_op == external_op) and
        (bao.external_block_op_index == external_block_op_index))
      return bao.block_index;
  }
  return -1;
}
mlir::BlockAndValueMapping calculate_input_list(
    Operation *op_old, std::vector<Operation *> &subgraph_vertices,
    Block *block_instruction, std::vector<ArgumentInfo> &arguments_origin,
    std::map<Operation *, Operation *> &cache_subgraph) {
  /* For the op_old which is ging to get cloned each value needs to be replaced
   * by a vlue from the new_ops */
  /* A new Op can either be connecte to the outisde world through the
   * BlockArguments or to a already created Op */

  mlir::BlockAndValueMapping operand_mapping;

  std::vector<connection> producers = ExtractProducers(op_old);
  for (auto producer : producers) {
    Operation *producer_op = producer.external_operation;
    if (producer.is_producer_block_or_operation() == producer_type::block_op) {
      /*
         ** Value is Coming from a BasicBlock Argument outside the Subgraph
         through the cluster block arguments We need to find which block
         argument index to use
      */
      auto index = findBlockArgumentIndex(
          true, producer.block_external_operation, nullptr,
          producer.external_slot_index, arguments_origin);
      if (index == -1)
        FATAL_ERROR("cluster::calculate_input_list()::Block Index not found");
      operand_mapping.map(op_old->getOperand(producer.operation_slot_index),
                          block_instruction->getArgument(index));
    } else if (not(std::find(subgraph_vertices.begin(), subgraph_vertices.end(),
                             producer_op) != subgraph_vertices.end())) {
      /*
         ** Value is Coming from an Op outside the Subgraph through the cluster
         block arguments We need to find which block argument to use
      */
      auto index = findBlockArgumentIndex(
          false, nullptr, producer.external_operation,
          producer.external_slot_index, arguments_origin);
      if (index == -1)
        FATAL_ERROR("cluster::calculate_input_list()::Block Index not found");
      operand_mapping.map(op_old->getOperand(producer.operation_slot_index),
                          block_instruction->getArgument(index));
    } else if (std::find(subgraph_vertices.begin(), subgraph_vertices.end(),
                         producer_op) != subgraph_vertices.end()) {
      /*
          ** Value is Coming from an Op within the Subgraph
             Use the Cache Subgraph to find the Op
      */
      ASSERT_COND(cache_subgraph[producer_op]->getNumResults() != 1,
                  "cluster::calculate_input_list()::cache_subgraph[producer_op]"
                  " has more thann 1 Argument: While this should work we are "
                  "asserting so we validate proper now that we have multi "
                  "ouput operation available");
      operand_mapping.map(op_old->getOperand(producer.operation_slot_index),
                          cache_subgraph[producer_op]->getResult(0));
    }
  }
  return operand_mapping;
}

unsigned Cluster(std::vector<Operation *> &order_ops_filtered,
                 std::vector<Operation *> &order_ops_filtered_constant_ops,
                 Block &block, cluster_op instruction,
                 mlir::MLIRContext *context, Operation *&clusterOp) {
#if ENABLE_LOGGING
  std::cout << "Cluster::Start" << std::endl;
  std::cout << "Clustering Ops:";
  for (auto p : order_ops_filtered)
    std::cout << p << ",";
  std::cout << std::endl;
  std::cout << "Clustering Consts:";
  for (auto p : order_ops_filtered_constant_ops)
    std::cout << p << ",";
  std::cout << std::endl;
#endif

  std::vector<Operation *> subgraph_vertices;
  for (auto op : order_ops_filtered_constant_ops)
    subgraph_vertices.push_back(op);
  for (auto op : order_ops_filtered)
    subgraph_vertices.push_back(op);

#if ENABLE_LOGGING
  std::cout
      << "Identify Input and Output Operands for clustering operation:: Start"
      << std::endl;
#endif

  SmallVector<mlir::Value, TYPICAL_NO_INPUTS> cluster_input_list;
  std::unordered_map<Operation *, unsigned int> cluster_input_index;
  std::vector<ArgumentInfo> input_arguments_origin;
  for (Operation *op : subgraph_vertices) {
    std::vector<connection> producers = ExtractProducers(op);
    for (auto producer : producers) {
      if (producer.is_producer_block_or_operation() ==
          producer_type::block_op) {
        if (findBlockArgumentIndex(true, producer.block_external_operation,
                                   nullptr, producer.external_slot_index,
                                   input_arguments_origin) <
            0) /* block & block index not added Avoiding multiple registrations
                  of the value */
        {
          cluster_input_list.push_back(producer.value);
          ArgumentInfo bao;
          bao.block_or_op = true;
          bao.external_block = producer.block_external_operation;
          bao.external_op = nullptr;
          bao.external_block_op_index = producer.external_slot_index;
          bao.block_index = input_arguments_origin.size();
          input_arguments_origin.push_back(bao);
        }
      } else if (std::find(subgraph_vertices.begin(), subgraph_vertices.end(),
                           producer.external_operation) ==
                 subgraph_vertices.end()) {
        if (findBlockArgumentIndex(false, nullptr, producer.external_operation,
                                   producer.external_slot_index,
                                   input_arguments_origin) <
            0) /* defining_op * slot not added Avoiding multiple registrations
                  of the value */
        {
          cluster_input_list.push_back(producer.value);
          ArgumentInfo bao;
          bao.block_or_op = false;
          bao.external_block = nullptr;
          bao.external_op = producer.external_operation;
          bao.external_block_op_index = producer.external_slot_index;
          bao.block_index = input_arguments_origin.size();
          input_arguments_origin.push_back(bao);
        }
      }
    }
  }
  SmallVector<Type, TYPICAL_NO_INPUTS> cluster_input_list_type;
  for (auto input : cluster_input_list)
    cluster_input_list_type.push_back(input.getType());

  SmallVector<mlir::Value, TYPICAL_NO_INPUTS> cluster_output_list;
  std::vector<ArgumentInfo> output_arguments_origin;
  for (Operation *op : subgraph_vertices) {
    std::vector<connection> consumers = ExtractConsumers(op);
    for (auto connection : consumers) {
      if (std::find(subgraph_vertices.begin(), subgraph_vertices.end(),
                    connection.external_operation) == subgraph_vertices.end()) {
        if (findBlockArgumentIndex(false, nullptr, connection.operation,
                                   connection.operation_slot_index,
                                   output_arguments_origin) <
            0) /* op & slot not added Avoiding multiple registrations of the
                  value */
        {
          cluster_output_list.push_back(connection.value);
          ArgumentInfo bao;
          bao.block_or_op = false;
          bao.external_block = nullptr;
          bao.external_op = connection.operation;
          bao.external_block_op_index = connection.operation_slot_index;
          bao.block_index = output_arguments_origin.size();
          output_arguments_origin.push_back(bao);
        }
      }
    }
  }

#if ENABLE_LOGGING
  if (instruction == cluster_region_op)
    std::cout << std::endl;
  if (instruction == cluster_region_op) {
    for (auto c : cluster_input_list_type) {
      std::cout << "Input: "
                << c.dyn_cast<mlir::SchedTensorType>().getUniqueId()
                << std::endl;
    }
    for (auto c : cluster_output_list_type) {
      std::cout << "Output: "
                << c.dyn_cast<mlir::SchedTensorType>().getUniqueId()
                << std::endl;
    }
  }
  if (instruction == cluster_region_op)
    std::cout << std::endl;

  std::cout << "No of unique Input Values to the cluster Op: "
            << cluster_input_list.size() << std::endl;
  std::cout << "No of unique Output Values to the cluster Op: "
            << cluster_output_list.size() << std::endl;

  std::cout
      << "Identify Input and Output Operands for clustering operation:: End"
      << std::endl;
#endif

  /*
     Create a cluster instruction
     Add the Cluster Instruction to the a specific place in the Basic Block
   */
  OpBuilder op_builder(context);
  mlir::Location loc = op_builder.getUnknownLoc();

  /*
     Make sure the NPU inputs and outputs are the correct type forcing them to
     Ranked Tensor where needed Casts are inserted lower in the code
   */
  SmallVector<Type, TYPICAL_NO_INPUTS> cluster_output_list_type;
  if (instruction == npu_region_op) {
    for (auto output : cluster_output_list) {
      if (output.getType().dyn_cast<mlir::RankedTensorType>()) {
        /* NPU Regions are created in topological sorted way so we should never
         * hit this */
        FATAL_ERROR("cluster.cc: While Clustering a Npu Region the output is "
                    "not SchedTensorType as expected");
      } else {
        auto type_original = output.getType().dyn_cast<mlir::SchedTensorType>();
        mlir::RankedTensorType rt = RankedTensorType::get(
            type_original.getShape(), type_original.getElementType());
        cluster_output_list_type.push_back(rt);
      }
    }

    for (auto v : cluster_input_list)
      if (v.getType().dyn_cast<mlir::SchedTensorType>())
        FATAL_ERROR("cluster.cc: While Clustering a Npu Region the input is "
                    "not RankedTensorType as expected");
  } else {
    for (auto output : cluster_output_list)
      cluster_output_list_type.push_back(output.getType());
  }

  clusterOp = nullptr;
  llvm::iplist<Operation>::iterator insertClusterOp(order_ops_filtered.back());
  op_builder.setInsertionPoint(&block, insertClusterOp);
  switch (instruction) {
  case npu_region_op: {
    clusterOp = op_builder.create<scheduleir::NpuRegionOp>(
        loc, cluster_output_list_type, cluster_input_list);
    break;
  }
  case kernel_region_op: {
    clusterOp = op_builder.create<kernelir::KernelRegionOp>(
        loc, cluster_output_list_type, cluster_input_list);
    break;
  }
  case cluster_region_op: {
    clusterOp = op_builder.create<cascadeir::CascadeRegionOp>(
        loc, cluster_output_list_type, cluster_input_list);
    break;
  }
  }

  Region &region_instruction = clusterOp->getRegion(0);
  Block *block_instruction = op_builder.createBlock(
      &region_instruction, region_instruction.begin(), cluster_input_list_type);

#if ENABLE_LOGGING
  std::cout << "Cluster Op Created" << std::endl;
#endif

  /*
     2. Deep Clone operations into cluster new Basic Block
   */

  llvm::iplist<Operation>::iterator insertClonedOp(block_instruction->end());
  op_builder.setInsertionPoint(block_instruction, insertClonedOp);

  SmallVector<Value, TYPICAL_NO_INPUTS> terminate_input_list;
  std::map<Operation *, Operation *> cache_subgraph;

  // Clone the Constants
  for (auto op_old : order_ops_filtered_constant_ops) {
    mlir::BlockAndValueMapping operand_mapping;
    Operation *op_new = op_builder.clone(*op_old, operand_mapping);

    cache_subgraph[op_old] = op_new;
  }

  // Clone the Ops
  for (auto op_old : order_ops_filtered) {

    /*
        Create Operand Mapping
    */
    mlir::BlockAndValueMapping operand_mapping =
        calculate_input_list(op_old, subgraph_vertices, block_instruction,
                             input_arguments_origin, cache_subgraph);

    /*
        Clone Op
    */
    Operation *op_new = op_builder.clone(*op_old, operand_mapping);

    /*
        If its output are going outside of the subgraph or it is Last Op of the
       the subgraph they must sink into the Yield Op (terminate_input_list) 0. A
       Result value from op_new could not be going outside the Subgraph. [do not
       add]
            1. A Result value from op_new could be going outside the Subgraph
       only [Okay we add it once]
            2. A Result value from op_new could be going outside the Subgraph
       and inside the subgraph [Okay we add it once]
            3. A Result value frm op_new could be going outside the Subgraph to
       N seperate nodex [Make sure we do not add multiple times]
    */
    for (unsigned int i = 0; i < op_old->getNumResults(); i++) {
      mlir::Value v = op_old->getResult(i);
      /* Is this Result every consumed outside the Subgraph */
      for (mlir::Operation *user_op : v.getUsers()) {
        if (std::find(subgraph_vertices.begin(), subgraph_vertices.end(),
                      user_op) == subgraph_vertices.end()) {
          terminate_input_list.push_back(op_new->getResult(i));
          break;
        }
      }
    }

    /*
        Store in Cache
    */
    cache_subgraph[op_old] = op_new;
  }

#if ENABLE_LOGGING
  std::cout << "Cluster Ops Cloned" << std::endl;
#endif

  // Insert a Yield Command
  /* Generating Clusters with no output is not a use case at the moment so
   * leaving this enabled */
  ASSERT_COND(terminate_input_list.size() == 0,
              "cluster_ops.cc::terminate_input_list.size() == 0");
  Operation *op_terminate =
      op_builder.create<scheduleir::YieldOp>(loc, terminate_input_list);

  /*
    3. Update the connections (consumers) that used to connect to
       the operations that moved into clusterOp
       Connect clusterOp to it consumers
       This will automatically also update the final scheduleir.yield
       Needs to be done after cloning of Ops so we can detect the
    terminate_input_list
   */
  unsigned int index = 0;
  for (auto v : cluster_output_list)
    v.replaceAllUsesWith(clusterOp->getResult(index++));

/* 4. Delete the Operations from original block: They all have been cloned into
      the new instruction region and are not needed to be referenced any more
      Also consumers have been updated
 */
#if ENABLE_LOGGING
  std::cout << "Cluster::Delete::Start" << std::endl;
#endif
  for (auto op_old : subgraph_vertices) {
    op_old->dropAllReferences();
    op_old->dropAllDefinedValueUses();
    op_old->erase(); /* Remove this operation from its parent block and delete
                        it */
  }
#if ENABLE_LOGGING
  std::cout << "Cluster::Delete::End" << std::endl;
#endif

/****
  At this point the IR should be stable, lets check even tough the end of the
 kernel will also do IR validation for us
 *****/
#if DISABLE_IR_CHECKS
  ASSERT_COND(checkIR(block), "Cluster::Fatal Error in IR Chec:End");
#endif

  // printIR(block, mlir::utils::info_option::op_tensors);

#if ENABLE_LOGGING
  std::cout << "Cluster::End" << std::endl << std::endl;
#endif

  if (instruction == npu_region_op) {
    /* Insert Cast Ins between the Block arguments and the op users if needed */
    for (mlir::Value v : block_instruction->getArguments()) {
      auto type_original = v.getType().dyn_cast<mlir::RankedTensorType>();
      if (type_original) {
        std::vector<connection> consumers = ExtractConsumers(v);
        for (auto &c : consumers) {
          Operation *consuming_op = c.external_operation;
          mlir::Location loc = op_builder.getUnknownLoc();
          SmallVector<int64_t> shape =
              arrayref_2_smallvector(type_original.getShape());
          auto resultType = SchedTensorType::get(
              shape, type_original.getElementType(), getUniqueId());

          op_builder.setInsertionPoint(consuming_op);
          auto castIn_new =
              op_builder.create<mlir::scheduleir::CastInOp>(loc, resultType, v);
          consuming_op->setOperands(c.external_slot_index, 1,
                                    {castIn_new.getResult()});
        }
      }
    }

    /* Insert Cast Outs between the ops and the yield */
    for (int idx = 0; idx < op_terminate->getNumOperands(); idx++) {
      auto v = op_terminate->getOperand(idx);
      auto type_original = v.getType().dyn_cast<SchedTensorType>();
      if (type_original) {
        // Insert a Cast to SchedTensorType
        mlir::Location loc = op_builder.getUnknownLoc();
        SmallVector<int64_t> shape =
            arrayref_2_smallvector(type_original.getShape());
        auto resultType =
            RankedTensorType::get(shape, type_original.getElementType());
        op_builder.setInsertionPointAfter(op_terminate);
        auto castOut_new =
            op_builder.create<mlir::scheduleir::CastOutOp>(loc, resultType, v);
        op_terminate->setOperands(idx, 1, {castOut_new.getResult()});
      }
    }
  }
  return 0;
}

unsigned Cluster(dag_graph &dag_global,
                 std::vector<OP_INDEX> &order_ops_filtered,
                 std::vector<OP_INDEX> &order_ops_filtered_constant_ops,
                 Block &block, cluster_op instruction,
                 mlir::MLIRContext *context, Operation *&clusterOp) {
  std::vector<Operation *> ops_order_ops_filtered;
  std::vector<Operation *> ops_order_ops_filtered_constant_ops;

  for (auto index : order_ops_filtered)
    ops_order_ops_filtered.push_back(dag_global.get_index_2_operation(index));
  for (auto index : order_ops_filtered_constant_ops)
    ops_order_ops_filtered_constant_ops.push_back(
        dag_global.get_index_2_operation(index));

  return Cluster(ops_order_ops_filtered, ops_order_ops_filtered_constant_ops,
                 block, instruction, context, clusterOp);
}

} // namespace utils
} // namespace mlir

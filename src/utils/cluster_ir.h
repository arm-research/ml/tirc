/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#ifndef CLUSTER_OPS_H
#define CLUSTER_OPS_H

#include "absl/memory/memory.h"
#include "mlir/IR/AffineMap.h"   // from @llvm-project
#include "mlir/IR/Attributes.h"  // from @llvm-project
#include "mlir/IR/Location.h"    // from @llvm-project
#include "mlir/IR/MLIRContext.h" // from @llvm-project

#include <vector>

#include "src/utils/dag.h"

#include "src/ir/cascade_ir.h"
#include "src/ir/kernel_ir.h"
#include "src/ir/llcs_ir.h"
#include "src/ir/schedule_ir.h"

#include "src/ir/architecture_config/architecture_config.h"
#include "src/utils/assert.h"
#include "src/utils/checkIR.h"
#include "src/utils/logging.h"
#include "src/utils/printIR.h"

#include "src/utils/clone_ir.h"

namespace mlir {
namespace utils {

enum cluster_op { npu_region_op, kernel_region_op, cluster_region_op };

/*
   Take a Series of topologically sorted Operators in the Block and cluster them
   together performing hoisting and clustering into a specific Op which is at a
   higher level in the IR hierarchy
*/
unsigned Cluster(std::vector<Operation *> &order_ops_filtered,
                 std::vector<Operation *> &order_ops_filtered_constant_ops,
                 Block &block, cluster_op instruction,
                 mlir::MLIRContext *context, Operation *&clusterOp);

unsigned Cluster(dag_graph &dag, std::vector<OP_INDEX> &order_ops_filtered,
                 std::vector<OP_INDEX> &order_ops_filtered_constant_ops,
                 Block &block, cluster_op instruction,
                 mlir::MLIRContext *context, Operation *&clusterOp);

} // namespace utils
} // namespace mlir
#endif // CLUSTER_OPS_H

/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#include "dag.h"

namespace mlir {

/********************************************************************************************/
/*                              Graph Utilities */
/********************************************************************************************/
std::vector<OP_INDEX> dag_graph::GetConsumers(OP_INDEX op_index) {
  std::vector<OP_INDEX> v;
  typename boost::graph_traits<DirectedGraph>::out_edge_iterator eo, eo_end;
  boost::graph_traits<DirectedGraph>::vertex_descriptor src =
      vertex(op_index, *graph);
  for (boost::tie(eo, eo_end) = boost::out_edges(src, *graph); eo != eo_end;
       eo++) {
    unsigned s = source(*eo, *graph);
    unsigned t = target(*eo, *graph);
    v.push_back(t);
  }
  return v;
}

std::vector<OP_INDEX> dag_graph::GetProducers(OP_INDEX op_index) {
  std::vector<OP_INDEX> v;
  typename boost::graph_traits<DirectedGraph>::in_edge_iterator eo, eo_end;
  boost::graph_traits<DirectedGraph>::vertex_descriptor src =
      vertex(op_index, *graph);
  for (boost::tie(eo, eo_end) = boost::in_edges(src, *graph); eo != eo_end;
       eo++) {
    unsigned s = source(*eo, *graph);
    unsigned t = target(*eo, *graph);
    v.push_back(s);
  }
  return v;
}

int dag_graph::LoadBlock2DAGRepresentation(Block &block, bool skip_const) {
  std::vector<Edge> edgeVec;
  llvm::iplist<Operation>::iterator i;
  std::map<unsigned, bool> no_vertex;
  unsigned int vertex_index = 0;
  // Add Operations (First level)
  for (i = block.begin(); i != block.end(); i++) {
    Operation *op = &(*i);
    if (skip_const and llvm::isa<scheduleir::ConstOp>(op))
      continue;
    auto index = addOperation(op);
  }
  // Add Edges
  for (i = block.begin(); i != block.end(); i++) {
    Operation *op = &(*i);
    if (skip_const and llvm::isa<scheduleir::ConstOp>(op))
      continue;
    for (auto consumer : ExtractConsumers(op)) {
      OP_INDEX op_index = get_operation_2_index(consumer.operation);
      OP_INDEX consumer_op_index =
          get_operation_2_index(consumer.external_operation);
      edgeVec.push_back(Edge(op_index, consumer_op_index));
      no_vertex[op_index] = true;
    }
  }
  // Create the graph
  DirectedGraph *graph =
      new DirectedGraph(edgeVec.begin(), edgeVec.end(), no_vertex.size());
  /* This will fail if you try and reset a graph */
  set_graph(graph);

  /*
    {
      std::list<OP_INDEX> order_ops;
      TopologicalSort(order_ops);
      for (auto i: order_ops)
         std::cout << i << ",";
      std::cout << std::endl;
      exit(1);
    }
  */
  return 0;
}

int dag_graph::TopologicalSort(std::vector<OP_INDEX> &order_ops) {
  std::deque<int> topo_order;
  boost::topological_sort(*(get_graph()), std::front_inserter(topo_order));
  for (std::deque<int>::const_iterator i = topo_order.begin();
       i != topo_order.end(); ++i) {
    order_ops.push_back(*i); /* Vertex Index -> Op Index */
  }
  return 0;
}

int dag_graph::TopologicalSort(std::list<OP_INDEX> &order_ops) {
  std::deque<int> topo_order;
  boost::topological_sort(*(get_graph()), std::front_inserter(topo_order));
  for (std::deque<int>::const_iterator i = topo_order.begin();
       i != topo_order.end(); ++i) {
    order_ops.push_back(*i); /* Vertex Index -> Op Index */
  }
  return 0;
}

void dag_graph::print_topological_sort() {
  std::list<OP_INDEX> order_ops;
  TopologicalSort(order_ops);
  for (auto op_index : order_ops)
    std::cout << op_index << ":"
              << get_index_2_operation(op_index)->getName().getStringRef().str()
              << std::endl;
}

bool is_cpu_node_no_return(Operation *op) {
  std::string op_dialect = op->getDialect()->getNamespace().str();
  std::string op_name = op->getName().getStringRef().str();
  if (op_dialect == "scheduleir" or (op_name == "std.return"))
    return false;
  else
    return true;
}

bool is_cpu_node(Operation *op) {
  std::string op_dialect = op->getDialect()->getNamespace().str();
  if (op_dialect == "scheduleir")
    return false;
  else
    return true;
}

} // namespace mlir

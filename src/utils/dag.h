/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#ifndef DAG_H
#define DAG_H

#include "absl/memory/memory.h"
#include "mlir/IR/AffineMap.h"   // from @llvm-project
#include "mlir/IR/Attributes.h"  // from @llvm-project
#include "mlir/IR/Location.h"    // from @llvm-project
#include "mlir/IR/MLIRContext.h" // from @llvm-project

#include <algorithm>
#include <iostream>
#include <map>
#include <string>
#include <utility>
#include <vector>

#include "boost/graph/adjacency_list.hpp"
#include "boost/graph/graph_traits.hpp"
#include "boost/graph/topological_sort.hpp"

#include "src/utils/assert.h"
#include "src/utils/ir_utils.h"
#include "src/utils/logging.h"

#include "src/ir/schedule_ir.h"
#include "tensorflow/compiler/mlir/lite/ir/tfl_ops.h"

using namespace boost;
using namespace std;

namespace mlir {

using OP_INDEX = unsigned;
using SLOT_INDEX = unsigned;

typedef adjacency_list<vecS, vecS, bidirectionalS> DirectedGraph;
typedef std::pair<int, int> Edge;

struct node_data {
  bool valid;
};

struct edge_data {
  bool valid;
};

bool is_cpu_node_no_return(Operation *op);
bool is_cpu_node(Operation *op);

class dag_graph {
public:
  /* Generate a DAG DataStructure Representation for a given Block */
  int LoadBlock2DAGRepresentation(Block &block, bool skip_const = false);

  dag_graph() {
    graph = nullptr;
    next_count = 0;
  }

  ~dag_graph() { delete graph; }

  std::vector<OP_INDEX> GetConsumers(OP_INDEX op_index);
  std::vector<OP_INDEX> GetProducers(OP_INDEX op_index);

  /*
     Get the TopologicalSort starting from a specific node
       0: default from root node
     dag represents a given Block
   */
  int TopologicalSort(std::vector<OP_INDEX> &order_ops);
  int TopologicalSort(std::list<OP_INDEX> &order_ops);

  void print_topological_sort();

  int addOperation(Operation *op) {
    mapping_operation_2_index[op] = next_count++;
    return mapping_operation_2_index[op];
  }
  OP_INDEX getOperationIndex(Operation *op) {
    return mapping_operation_2_index[op];
  }

  OP_INDEX get_operation_2_index(Operation *op) {
    if (mapping_operation_2_index.find(op) == mapping_operation_2_index.end()) {
      ASSERT_COND(
          true,
          "Critical Error::dag_graph::get_operation_2_index:: op not found");
    }
    return mapping_operation_2_index[op];
  }

  Operation *get_index_2_operation(OP_INDEX index) {
    for (auto op_i : mapping_operation_2_index) {
      if (op_i.second == index) {
        return op_i.first;
      }
    }
    ASSERT_COND(
        true,
        "Critical Error::dag_graph::get_index_2_operation:: index not found");
    return nullptr;
  }

private:
  DirectedGraph *get_graph() {
    ASSERT_COND(
        this->graph == nullptr,
        "dag.h::get_graph failed pointer null indicating no Bock was Loaded");
    return this->graph;
  }

  void set_graph(DirectedGraph *g) {
    ASSERT_COND(this->graph != nullptr,
                "dag.h::set_graph failed pointer not null indicating you "
                "already have a loaded block");
    this->graph = g;
  }

  /* Using Boost to get all the graph theory algorithms. This needs to be
   * replaced with an internal implementation */
  DirectedGraph *graph;
  /* Needed since some MLIR core currently uses a value of 5 for stride*/
  /* Try to remove the need for this mapping */

  unsigned int next_count = 0;
  std::map<Operation *, OP_INDEX>
      mapping_operation_2_index; /* We track a uniue Index for each Operator:
                                    Operation Index -> Operation Pointer */
};

} // namespace mlir
#endif // DAG_H

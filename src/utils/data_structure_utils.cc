/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#ifndef DATA_STRUCTURE_UTILS_H
#define DATA_STRUCTURE_UTILS_H

//#include "absl/memory/memory.h"
#include "mlir/Dialect/Quant/QuantOps.h"
#include "mlir/Dialect/Quant/QuantTypes.h"
#include "mlir/Dialect/StandardOps/IR/Ops.h"      // from @llvm-project
#include "mlir/IR/AffineMap.h"                    // from @llvm-project
#include "mlir/IR/Attributes.h"                   // from @llvm-project
#include "mlir/IR/BlockAndValueMapping.h"         // from @llvm-project
#include "mlir/IR/Builders.h"                     // from @llvm-project
#include "mlir/IR/Location.h"                     // from @llvm-project
#include "mlir/IR/MLIRContext.h"                  // from @llvm-project
#include "mlir/IR/Operation.h"                    // from @llvm-project
#include "mlir/IR/PatternMatch.h"                 // from @llvm-project
#include "mlir/Interfaces/InferTypeOpInterface.h" // from @llvm-project
#include "mlir/Pass/Pass.h"                       // from @llvm-project
#include "mlir/Transforms/DialectConversion.h"    // from @llvm-project

#include <algorithm>
#include <iostream>
#include <map>
#include <string>
#include <utility>
#include <vector>

#include "src/utils/assert.h"

using namespace std;

namespace mlir {
namespace utils {

SmallVector<int32_t, 1> ArrayAttr_to_SmallVector(ArrayAttr array_attr) {
  SmallVector<int32_t, 1> array_vector;
  for (uint32_t i = 0; i < array_attr.size(); i++) {
    array_vector.push_back(array_attr[i].dyn_cast<IntegerAttr>().getInt());
  }
  return array_vector;
}

std::vector<int32_t> ArrayAttr_to_Vector(ArrayAttr array_attr) {
  std::vector<int32_t> array_vector;
  for (uint32_t i = 0; i < array_attr.size(); i++) {
    array_vector.push_back(array_attr[i].dyn_cast<IntegerAttr>().getInt());
  }
  return array_vector;
}

std::string array_ref_to_string(ArrayRef<int64_t> shape) {
  stringstream shape_ss;
  {
    if (shape.size() == 4)
      shape_ss << "[" << shape[0] << "," << shape[1] << "," << shape[2] << ","
               << shape[3] << "]";
    else if (shape.size() == 1)
      shape_ss << "[" << shape[0] << "]";
    else
      ASSERT_COND(true, "array_ref_to_string_stream:: Unknown tensor Size");
  }
  return shape_ss.str();
}

std::string vector_to_string(std::vector<int32_t> shape) {
  stringstream shape_ss;
  {
    if (shape.size() == 4)
      shape_ss << "[" << shape[0] << "," << shape[1] << "," << shape[2] << ","
               << shape[3] << "]";
    else if (shape.size() == 1)
      shape_ss << "[" << shape[0] << "]";
    else
      ASSERT_COND(true, "vector_to_string:: Unknown tensor Size");
  }
  return shape_ss.str();
}

std::string smallvector_to_string(SmallVector<int32_t, 4> shape) {
  stringstream shape_ss;
  {
    if (shape.size() == 4)
      shape_ss << "[" << shape[0] << "," << shape[1] << "," << shape[2] << ","
               << shape[3] << "]";
    else if (shape.size() == 1)
      shape_ss << "[" << shape[0] << "]";
    else if (shape.size() == 0)
      shape_ss << "[]";
    else
      ASSERT_COND(true, "vector_to_string:: Unknown tensor Size");
  }
  return shape_ss.str();
}

std::string smallvector_to_string(SmallVector<int64_t, 4> shape) {
  stringstream shape_ss;
  {
    if (shape.size() == 4)
      shape_ss << "[" << shape[0] << "," << shape[1] << "," << shape[2] << ","
               << shape[3] << "]";
    else if (shape.size() == 1)
      shape_ss << "[" << shape[0] << "]";
    else if (shape.size() == 0)
      shape_ss << "[]";
    else
      ASSERT_COND(true, "vector_to_string:: Unknown tensor Size");
  }
  return shape_ss.str();
}

} // namespace utils
} // namespace mlir

#endif // DATA_STRUCTURE_UTILS_H

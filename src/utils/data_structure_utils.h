/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#ifndef DATA_STRUCTURE_UTILS_H
#define DATA_STRUCTURE_UTILS_H

//#include "absl/memory/memory.h"
#include "mlir/Dialect/Quant/QuantOps.h"
#include "mlir/Dialect/Quant/QuantTypes.h"
#include "mlir/Dialect/StandardOps/IR/Ops.h"      // from @llvm-project
#include "mlir/IR/AffineMap.h"                    // from @llvm-project
#include "mlir/IR/Attributes.h"                   // from @llvm-project
#include "mlir/IR/BlockAndValueMapping.h"         // from @llvm-project
#include "mlir/IR/Builders.h"                     // from @llvm-project
#include "mlir/IR/Location.h"                     // from @llvm-project
#include "mlir/IR/MLIRContext.h"                  // from @llvm-project
#include "mlir/IR/Operation.h"                    // from @llvm-project
#include "mlir/IR/PatternMatch.h"                 // from @llvm-project
#include "mlir/Interfaces/InferTypeOpInterface.h" // from @llvm-project
#include "mlir/Pass/Pass.h"                       // from @llvm-project
#include "mlir/Transforms/DialectConversion.h"    // from @llvm-project

#include <algorithm>
#include <iostream>
#include <map>
#include <string>
#include <utility>
#include <vector>

#include "src/utils/assert.h"

using namespace std;

namespace mlir {
namespace utils {

struct PointXY {
  int32_t x = 0;
  int32_t y = 0;
};

struct PointXYZ {
  int32_t x = 0;
  int32_t y = 0;
  int32_t z = 0;
};

struct PointPad {
  int32_t top = 0;
  int32_t bottom = 0;
  int32_t left = 0;
  int32_t right = 0;
};

struct SliceViewMetaData {
  int32_t axis = 0;
  int32_t offset = 0;
  SliceViewMetaData(int32_t axis, int32_t offset)
      : axis(axis), offset(offset) {}
};

struct FilterMetaData {
  int32_t width = 0;
  int32_t height = 0;
  PointXY stride;
  PointXY dilation;
  PointPad padding;
  FilterMetaData(int32_t w, int32_t h, int32_t sx = 1, int32_t sy = 1,
                 int32_t dx = 1, int32_t dy = 1, int32_t pv = 1, int32_t px = 1,
                 int32_t py = 1, int32_t pw = 1)
      : width(w),
        height(h), stride{sx, sy}, dilation{dx, dy}, padding{pv, px, py, pw} {}
  FilterMetaData() {}
};

template <typename T>
inline std::vector<T> create_vector(int32_t value, int32_t no_entries) {
  std::vector<T> data;
  for (int i = 0; i < no_entries; i++)
    data.push_back(data[i]);
  return data;
}

template <typename T> inline T small_vector_prod(SmallVector<T, 5> data) {
  T prod = 1;
  for (int i = 0; i < data.size(); i++)
    prod = (prod * data[i]);
  return prod;
}

template <typename T> inline T vector_prod(std::vector<T> data) {
  T prod = 1;
  for (int i = 0; i < data.size(); i++)
    prod = (prod * data[i]);
  return prod;
}

template <typename T> inline T array_ref_prod(ArrayRef<T> data) {
  T prod = 1;
  for (int i = 0; i < data.size(); i++)
    prod = (prod * data[i]);
  return prod;
}

SmallVector<int32_t, 1> ArrayAttr_to_SmallVector(ArrayAttr array_attr);
std::vector<int32_t> ArrayAttr_to_Vector(ArrayAttr array_attr);
std::string array_ref_to_string(ArrayRef<int64_t> shape);
std::string vector_to_string(std::vector<int32_t> shape);
std::string smallvector_to_string(SmallVector<int32_t, 4> shape);
std::string smallvector_to_string(SmallVector<int64_t, 4> shape);

} // namespace utils
} // namespace mlir

#endif // DATA_STRUCTURE_UTILS_H

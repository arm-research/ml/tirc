/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#include "src/utils/ir_sort.h"

namespace mlir {
namespace utils {

int ir_sort(Operation &op) {
  Builder builder(op.getContext());
  OpBuilder op_builder(op.getContext());

  mlir::ArchitectureConfig::ArchitectureConfigAttr arch_c =
      AccessArchitectureConfigAttribute_From_NpuRegion(&op);

  mlir::CompilerConfig::CompilerConfigAttr compiler_config =
      AccessCompilerConfigAttribute_From_NpuRegion(&op);

  Region &region_instruction = op.getRegion(0);
  Block &block_old = region_instruction.front();

  ir_sort(block_old, region_instruction, arch_c, compiler_config, builder,
          op_builder);
}

int ir_sort(FuncOp &function, MLIRContext *ctx,
            mlir::ArchitectureConfig::ArchitectureConfigAttr &arch_c,
            mlir::CompilerConfig::CompilerConfigAttr &compiler_config) {
  Region *region = function.getCallableRegion();
  llvm::iplist<Block> &blocks = region->getBlocks();
  Block &block = region->front();

  Builder builder(ctx);
  OpBuilder op_builder(ctx);
  ir_sort(block, *region, arch_c, compiler_config, builder, op_builder);
}

int ir_sort(Block &block_old, Region &region_instruction,
            mlir::ArchitectureConfig::ArchitectureConfigAttr &arch_c,
            mlir::CompilerConfig::CompilerConfigAttr &compiler_config,
            Builder &builder, OpBuilder &op_builder) {
  // Topological Sort of the first level of the NpuRegionOp using
  dag_graph dag;
  if (dag.LoadBlock2DAGRepresentation(block_old) != 0) {
    std::cout
        << "PlannerExtractNpuRegions::ConvertBlock2DAGRepresentation failed"
        << std::endl;
    return 1;
  }

  std::vector<OP_INDEX> order_ops;
  dag.TopologicalSort(order_ops);

  // 0. Create a new block with similar Input/Output Operands
  SmallVector<Type, TYPICAL_NO_INPUTS> input_list_type;
  for (auto i = 0; i < block_old.getNumArguments(); i++)
    input_list_type.push_back(block_old.getArgument(i).getType());

  Block *block_new = op_builder.createBlock(
      &region_instruction, region_instruction.begin(), input_list_type);

  std::unordered_map<int, mlir::Value> mapping_block;
  std::unordered_map<Operation *, std::vector<mlir::Value>>
      mapping_ops_operands;
  std::unordered_map<Operation *, Operation *> mapping_ops;

  for (auto b = 0; b < block_new->getNumArguments(); b++)
    mapping_block[b] = block_new->getArgument(b);

  for (auto index : order_ops) {
    Operation *op_old = dag.get_index_2_operation(index);

    auto producers = ExtractProducers(op_old);

    mlir::BlockAndValueMapping operand_mapping;
    for (auto producer : producers) {
      if (producer.is_producer_block_or_operation() == producer_type::block_op)
        operand_mapping.map(producer.value,
                            mapping_block[producer.external_slot_index]);
      else {
        Operation *op_producer_new = mapping_ops[producer.external_operation];
        mlir::Value v =
            mapping_ops_operands[op_producer_new][producer.external_slot_index];
        operand_mapping.map(producer.value, v);
      }
    }

    llvm::iplist<Operation>::iterator insertOp(block_new->end());
    op_builder.setInsertionPoint(block_new, insertOp);
    Operation *op_new = op_builder.clone(*op_old, operand_mapping);

    mapping_ops[op_old] = op_new;

    std::vector<mlir::Value> results;
    for (int r = 0; r < op_new->getNumResults(); r++)
      results.push_back(op_new->getResult(r));
    mapping_ops_operands[op_new] = results;
  }

  // 2. Delete Operations in old block
  for (auto index = order_ops.rbegin(); index != order_ops.rend(); ++index) {
    Operation *op_old = dag.get_index_2_operation(*index);
    op_old->dropAllReferences();
    op_old->dropAllDefinedValueUses();
    op_old->erase(); /* Remove this operation from its parent block and
                        delete it */
  }
  // 3. Delete Old Block
  block_old.erase();

  return 0;
}

} // namespace utils
} // namespace mlir

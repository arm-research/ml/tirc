/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#ifndef IR_SORT_H
#define IR_SORT_H

// #include "absl/memory/memory.h"
#include "mlir/Dialect/Quant/QuantOps.h"
#include "mlir/Dialect/Quant/QuantTypes.h"
#include "mlir/IR/AffineMap.h"                    // from @llvm-project
#include "mlir/IR/Attributes.h"                   // from @llvm-project
#include "mlir/IR/BlockAndValueMapping.h"         // from @llvm-project
#include "mlir/IR/Builders.h"                     // from @llvm-project
#include "mlir/IR/Location.h"                     // from @llvm-project
#include "mlir/IR/MLIRContext.h"                  // from @llvm-project
#include "mlir/IR/Operation.h"                    // from @llvm-project
#include "mlir/IR/PatternMatch.h"                 // from @llvm-project
#include "mlir/Interfaces/InferTypeOpInterface.h" // from @llvm-project
#include "mlir/Pass/Pass.h"                       // from @llvm-project
#include "mlir/Transforms/DialectConversion.h"    // from @llvm-project

#include <algorithm>
#include <iostream>
#include <map>
#include <string>
#include <utility>
#include <vector>

#include "src/ir/architecture_config/architecture_config.h"
#include "src/ir/cascade_ir.h"
#include "src/ir/kernel_ir.h"
#include "src/ir/schedule_ir.h"

#include "src/utils/assert.h"
#include "src/utils/clone_ir.h"
#include "src/utils/dag.h"
#include "src/utils/data_structure_utils.h"
#include "src/utils/ir_sort.h"
#include "src/utils/ir_utils.h"
#include "src/utils/printIR.h"
#include "src/utils/unique_id.h"

using namespace std;

namespace mlir {
namespace utils {

/* All these functions assume Regions with Single Blocks */

/*  Sorts the single block inside the region contained by the Operation */
int ir_sort(Operation &op);

/*  Sorts the single block inside the region contained in the Functon */
int ir_sort(FuncOp &function, MLIRContext *ctx,
            mlir::ArchitectureConfig::ArchitectureConfigAttr &arch_c,
            mlir::CompilerConfig::CompilerConfigAttr &compiler_config);

/*
   Sorts a Block within a specific region
   Places the New Sorted Block at the beginning of the region
   and deletes old block
*/
int ir_sort(Block &block_old, Region &region_instruction,
            mlir::ArchitectureConfig::ArchitectureConfigAttr &arch_c,
            mlir::CompilerConfig::CompilerConfigAttr &compiler_config,
            Builder &builder, OpBuilder &op_builder);

} // namespace utils
} // namespace mlir

#endif // IR_SORT

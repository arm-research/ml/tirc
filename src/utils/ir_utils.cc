/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#include "src/utils/ir_utils.h"

namespace mlir {
namespace utils {

/*
   Returns Detailed Information of the Consumers of an Value
   If a Value goes to the same Operation more than once this is
   treated as multiple individual connections
   from the value to the operation
*/
std::vector<connection> ExtractConsumers(Value v) {
  std::vector<connection> consumers;
  std::set<mlir::Operation *> unique_consumers;
  for (Operation *consumer : v.getUsers()) {
    unique_consumers.insert(consumer);
  }
  for (Operation *consumer : unique_consumers) {
    unsigned external_slot_index;
    for (int ope = 0; ope < consumer->getNumOperands(); ope++) {
      Value v1 = consumer->getOperand(ope);
      if (v1 == v) {
        connection oper_val(ope, -1, nullptr, consumer, nullptr, v,
                            operation_2_consumer);
        consumers.push_back(oper_val);
      }
    }
  }
  return consumers;
}

/*
   Returns Detailed Information of the Consumers of an Operation
   If a Value goes to the same Operation more than once this is
   treated as multiple individual connections
   from the operation to the operation
*/
std::vector<connection> ExtractConsumers(Operation *i) {
  std::vector<connection> consumers;
  unsigned no_results = i->getNumResults();
  for (unsigned int r = 0; r < no_results; r++) {
    OpResult result = i->getResult(r);
    std::set<mlir::Operation *> unique_consumers;
    for (Operation *consumer : result.getUsers()) {
      unique_consumers.insert(consumer);
    }
    for (Operation *consumer : unique_consumers) {
      for (int ope = 0; ope < consumer->getNumOperands(); ope++) {
        Value v1 = consumer->getOperand(ope);
        if (v1 == result) {
          connection oper_val(ope, r, i, consumer, nullptr, result,
                              operation_2_consumer);
          consumers.push_back(oper_val);
        }
      }
    }
  }
  return consumers;
}

/*
   Returns Detailed Information of the Producers of an Operation
   If a Value comes from the same Operation and more than once this is
   treated as multiple individual connections
   from the operation to the operation
*/
std::vector<connection> ExtractProducers(Operation *i) {
  std::vector<connection> producers;
  for (unsigned int p = 0; p < i->getNumOperands(); p++) {
    Value input = i->getOperand(p);
    Operation *op_con = input.getDefiningOp();
    if (op_con != nullptr) {
      /* Producer is from an Op */
      OpResult result = input.cast<OpResult>();
      Operation *producer = result.getOwner();
      unsigned external_slot_index = 9999999;
      for (int ope = 0; ope < producer->getNumResults(); ope++) {
        Value v1 = producer->getResult(ope);
        if (v1 == result) {
          external_slot_index = ope;
          break;
        }
      }
      ASSERT_COND(external_slot_index == 9999999,
                  "ExtractProducers::Slot not found in Operator");
      unsigned operation_slot_index = p;
      connection oper_val(external_slot_index, operation_slot_index, i,
                          producer, nullptr, input, producer_2_operation);
      producers.push_back(oper_val);
    } else {
      BlockArgument block_argument = input.cast<BlockArgument>();
      Block *producer = block_argument.getOwner();
      unsigned external_slot_index = 9999999;
      for (int ope = 0; ope < producer->getNumArguments(); ope++) {
        BlockArgument v1 = producer->getArgument(ope);
        if (v1 == block_argument) {
          external_slot_index = ope;
          break;
        }
      }
      ASSERT_COND(external_slot_index == 9999999,
                  "ExtractProducers::Slot not found in Basic Block");
      unsigned operation_slot_index = p;
      connection oper_val(external_slot_index, operation_slot_index, i, nullptr,
                          producer, input, producer_2_operation);
      producers.push_back(oper_val);
    }
  }
  return producers;
}

/******************************************************************************/

llvm::iplist<Operation> &get_operations(Operation &op, bool &failure) {
  failure = false;
  if (op.getNumRegions() > 1) {
    std::cout << "PlannerWeightCompression::More than a single region"
              << std::endl;
    failure = true;
  }
  Region &region = op.getRegion(0);
  llvm::iplist<Block> &blocks = region.getBlocks();
  if (blocks.size() > 1) {
    std::cout << "PlannerWeightCompression::More than a single block"
              << std::endl;
    failure = true;
  }
  return region.front().getOperations();
}

unsigned NpuRegion_NoofOperations(Operation *op) {
  unsigned count = 0;
  Region &region_instruction = op->getRegion(0);
  Block &block_npu_region = region_instruction.front();
  for (llvm::iplist<Operation>::iterator i = block_npu_region.begin();
       i != block_npu_region.end(); i++) {
    if (llvm::isa<cascadeir::CascadeRegionOp>(i)) {
      Region &region_instruction = i->getRegion(0);
      Block &block_cascade = region_instruction.front();
      for (llvm::iplist<Operation>::iterator j = block_cascade.begin();
           j != block_cascade.end(); j++) {
        if (llvm::isa<kernelir::KernelRegionOp>(j)) {
          Region &region_instruction = j->getRegion(0);
          Block &block_kernel = region_instruction.front();
          for (llvm::iplist<Operation>::iterator k = block_kernel.begin();
               k != block_kernel.end(); k++) {
            count++;
          }
        }
      }
    }
  }
  return count;
}

unsigned NpuRegion_NoofKernels(Operation *op) {
  unsigned count = 0;
  Region &region_instruction = op->getRegion(0);
  Block &block_npu_region = region_instruction.front();
  for (llvm::iplist<Operation>::iterator i = block_npu_region.begin();
       i != block_npu_region.end(); i++) {
    if (llvm::isa<cascadeir::CascadeRegionOp>(i)) {
      Region &region_instruction = i->getRegion(0);
      Block &block_cascade = region_instruction.front();
      for (llvm::iplist<Operation>::iterator j = block_cascade.begin();
           j != block_cascade.end(); j++) {
        if (llvm::isa<kernelir::KernelRegionOp>(j))
          count++;
      }
    }
  }
  return count;
}

unsigned NpuRegion_NoofCascades(Operation *op) {
  unsigned count = 0;
  Region &region_instruction = op->getRegion(0);
  Block &block_npu_region = region_instruction.front();
  for (llvm::iplist<Operation>::iterator i = block_npu_region.begin();
       i != block_npu_region.end(); i++) {
    if (llvm::isa<cascadeir::CascadeRegionOp>(i))
      count++;
  }
  return count;
}

NpuBlockType getOpBlockType(Operation *op) {
  mlir::Attribute attr = op->getAttr("block_type");
  if (attr)
    return (NpuBlockType)attr.dyn_cast<mlir::IntegerAttr>().getInt();
  else
    return Default;
}

NpuBlockType getBlockType(Operation *op) {
  NpuBlockType block_type;
  if (llvm::isa<scheduleir::Conv2DOp>(op))
    block_type = ConvolutionMxN;
  else if (llvm::isa<scheduleir::DepthwiseConv2DOp>(op))
    block_type = ConvolutionDepthWise;
  else if (llvm::isa<scheduleir::FullyConnectedOp>(op))
    block_type = VectorProduct;
  else if (llvm::isa<scheduleir::MaxPool2dOp>(op))
    block_type = Pooling;
  else if (llvm::isa<scheduleir::AvgPool2dOp>(op))
    block_type = Pooling;
  else if (llvm::isa<scheduleir::RescaleOp>(op))
    block_type = ElementWise;
  else if (llvm::isa<scheduleir::AddOp>(op))
    block_type = ElementWise;
  else if (llvm::isa<scheduleir::ConcatOp>(op))
    block_type = ElementWise;
  else if (llvm::isa<scheduleir::YieldOp>(op))
    block_type = Default;
  else {
    std::cout << "planner_ir_format::FatalError::Node Not Supported"
              << std::endl; // Chanhe to Faulty Assert
    exit(1);
  }
}

bool consumed_only_by_dma_Op(::mlir::Value v) {
  ASSERT_COND(not(v.hasOneUse()), "consumed_only_by_dma_Op::More than one "
                                  "consume for Value you are trying to check");
  for (Operation *user : v.getUsers())
    return llvm::isa<scheduleir::DmaOp>(user);
}

bool produced_by_dma_Op(::mlir::Value v) {
  Operation *defining_op = v.getDefiningOp();
  return llvm::isa<scheduleir::DmaOp>(defining_op);
}

unsigned array_prod(ArrayRef<int64_t> s) {
  if (s.size() == 0)
    return 0;
  else if (s.size() == 1)
    return s[0];
  else if (s.size() == 2)
    return s[0] * s[1];
  else if (s.size() == 4)
    return s[0] * s[1] * s[2] * s[3];
  else {
    std::cout << "array_prod::Unknown size"
              << std::endl; // Chanhe to Faulty Assert
    exit(1);
  }
}

unsigned array_prod(SmallVector<int64_t, 4> s) {
  if (s.size() == 0)
    return 0;
  else if (s.size() == 1)
    return s[0];
  else if (s.size() == 2)
    return s[0] * s[1];
  else if (s.size() == 4)
    return s[0] * s[1] * s[2] * s[3];
  else {
    std::cout << "array_prod::Unknown size"
              << std::endl; // Chanhe to Faulty Assert
    exit(1);
  }
}

SmallVector<int64_t, 4> arrayref_2_smallvector(ArrayRef<int64_t> s) {
  SmallVector<int64_t, 4> values;
  for (int i = 0; i < s.size(); i++)
    values.push_back(s[i]);
  return values;
}

void clone_scheduler_metadata(Value src, Value dst,
                              ::mlir::cascadeir::PlanType plan_type,
                              bool copy_storage_shape) {
  mlir::SchedTensorType src_t = src.getType().dyn_cast<mlir::SchedTensorType>();
  mlir::SchedTensorType dst_t = dst.getType().dyn_cast<mlir::SchedTensorType>();

  if (copy_storage_shape) {
    SmallVector<int64_t, 4> storage_shape;
    for (auto i : src_t.getStorageShape())
      storage_shape.push_back(i);
    dst_t.setStorageShape(storage_shape);
  }

  SmallVector<int64_t, 4> bandwidth_shape;
  for (auto i : src_t.getBandwidthShape())
    bandwidth_shape.push_back(i);
  dst_t.setBandwidthShape(bandwidth_shape);

  /* Not copying MemArea as that is up to the scheduler to configure through a
   * rewrite     */
  /* Not copying MemType as that is up to the scheduler to configure through a
   * rewrite     */
  /* Not copying Purpose as that is up to the scheduler to configure through a
   * rewrite     */
  /* Not copying Sub Purpose as that is up to the scheduler to configure through
   * a rewrite */
  /* Not copying format as that is up to the scheduler to configure through a
   * rewrite      */

  dst_t.set_alignment(src_t.get_alignment());
  dst_t.set_storage_rounding_quantum(src_t.get_storage_rounding_quantum());
  dst_t.set_brick_size(src_t.get_brick_size());
  dst_t.set_NpuBlockTraversal(src_t.get_NpuBlockTraversal());
  dst_t.set_resampling_mode(src_t.get_resampling_mode());
  dst_t.set_avoid_NHCWB16(src_t.get_avoid_NHCWB16());

  dst_t.set_compression_scale_for_worst_weight_stream(
      src_t.get_compression_scale_for_worst_weight_stream());
  dst_t.set_weight_transpose_depthwise(src_t.get_weight_transpose_depthwise());
  dst_t.set_storage_compression_scale(src_t.get_storage_compression_scale());
  dst_t.set_bandwidth_compression_scale(
      src_t.get_bandwidth_compression_scale());

  dst_t.set_weight_compression_scales(src_t.get_weight_compression_scales());
  dst_t.set_weight_compression_config(src_t.get_weight_compression_config());
  dst_t.set_weight_compressed_offsets(src_t.get_weight_compressed_offsets());
  dst_t.set_compressed_values_substream_offsets(
      src_t.get_compressed_values_substream_offsets());
}

std::vector<double> getScale(Type t) {
  std::vector<double> scale_vector;
  if (t.isa<mlir::RankedTensorType>()) {
    RankedTensorType v = t.dyn_cast<mlir::RankedTensorType>();
    if (v.getElementType().isa<mlir::quant::UniformQuantizedPerAxisType>()) {
      mlir::quant::UniformQuantizedPerAxisType v_element =
          v.getElementType()
              .dyn_cast<mlir::quant::UniformQuantizedPerAxisType>();
      ArrayRef<double> scale = v_element.getScales();
      for (int i = 0; i < scale.size(); i++)
        scale_vector.push_back(scale[i]);
    } else if (v.getElementType().isa<mlir::quant::UniformQuantizedType>()) {
      mlir::quant::UniformQuantizedType v_element =
          v.getElementType().dyn_cast<mlir::quant::UniformQuantizedType>();
      scale_vector.push_back(v_element.getScale());
    }
    // else
    //    std::cout << "Warning:Rankedtensor::getScale::Unknown Element Type" <<
    //    std::endl;
  } else if (t.isa<mlir::SchedTensorType>()) {
    SchedTensorType v = t.dyn_cast<mlir::SchedTensorType>();
    if (v.getElementType().isa<mlir::quant::UniformQuantizedPerAxisType>()) {
      mlir::quant::UniformQuantizedPerAxisType v_element =
          v.getElementType()
              .dyn_cast<mlir::quant::UniformQuantizedPerAxisType>();
      ArrayRef<double> scale = v_element.getScales();
      for (int i = 0; i < scale.size(); i++)
        scale_vector.push_back(scale[i]);
    } else if (v.getElementType().isa<mlir::quant::UniformQuantizedType>()) {
      mlir::quant::UniformQuantizedType v_element =
          v.getElementType().dyn_cast<mlir::quant::UniformQuantizedType>();
      scale_vector.push_back(v_element.getScale());
    }
    // else
    //    std::cout << "Warning:SchedTensorType::getScale::Unknown Element Type"
    //    << std::endl;
  }

  return scale_vector;

  ASSERT_COND(true, "getScale::Needs to be run on mlir::RankedTensorType or "
                    "mlir::SchedTensorType");
}

std::vector<int64_t> getZeroPoint(Type t) {
  std::vector<int64_t> zero_point_vector;
  if (t.isa<mlir::RankedTensorType>()) {
    RankedTensorType v = t.dyn_cast<mlir::RankedTensorType>();
    if (v.getElementType().isa<mlir::quant::UniformQuantizedPerAxisType>()) {
      mlir::quant::UniformQuantizedPerAxisType v_element =
          v.getElementType()
              .dyn_cast<mlir::quant::UniformQuantizedPerAxisType>();
      ArrayRef<int64_t> zero_point = v_element.getZeroPoints();
      for (int i = 0; i < zero_point.size(); i++)
        zero_point_vector.push_back(zero_point[i]);
    } else if (v.getElementType().isa<mlir::quant::UniformQuantizedType>()) {
      mlir::quant::UniformQuantizedType v_element =
          v.getElementType().dyn_cast<mlir::quant::UniformQuantizedType>();
      zero_point_vector.push_back(v_element.getZeroPoint());
    }
    // else
    //    std::cout << "Warning:Rankedtensor::getZeroPoint::Unknown Element
    //    Type" << std::endl;
  } else if (t.isa<mlir::SchedTensorType>()) {
    SchedTensorType v = t.dyn_cast<mlir::SchedTensorType>();
    if (v.getElementType().isa<mlir::quant::UniformQuantizedPerAxisType>()) {
      mlir::quant::UniformQuantizedPerAxisType v_element =
          v.getElementType()
              .dyn_cast<mlir::quant::UniformQuantizedPerAxisType>();
      ArrayRef<int64_t> zero_point = v_element.getZeroPoints();
      for (int i = 0; i < zero_point.size(); i++)
        zero_point_vector.push_back(zero_point[i]);
    } else if (v.getElementType().isa<mlir::quant::UniformQuantizedType>()) {
      mlir::quant::UniformQuantizedType v_element =
          v.getElementType().dyn_cast<mlir::quant::UniformQuantizedType>();
      zero_point_vector.push_back(v_element.getZeroPoint());
    }
    // else
    //    std::cout << "Warning:SchedTensorType::getZeroPoint::Unknown Element
    //    Type" << std::endl;
  }

  return zero_point_vector;

  ASSERT_COND(true, "getZeroPoint::Needs to be run on mlir::RankedTensorType "
                    "or mlir::SchedTensorType");
}

bool getIsSymmetric(Type t) {
  std::vector<int64_t> zero_point_values = getZeroPoint(t);
  bool symm = true;
  for (auto z : zero_point_values) {
    if (z != 0)
      symm = false;
  }
  return symm;
}

bool getIsPerChannel(Type t) {
  if (t.isa<mlir::RankedTensorType>()) {
    RankedTensorType v = t.dyn_cast<mlir::RankedTensorType>();
    if (v.getElementType().isa<mlir::quant::UniformQuantizedPerAxisType>())
      return true;
    else if (v.getElementType().isa<mlir::quant::UniformQuantizedType>())
      return false;
    else
      return false;
  } else if (t.isa<mlir::SchedTensorType>()) {
    SchedTensorType v = t.dyn_cast<mlir::SchedTensorType>();
    if (v.getElementType().isa<mlir::quant::UniformQuantizedPerAxisType>())
      return true;
    else if (v.getElementType().isa<mlir::quant::UniformQuantizedType>())
      return false;
    else
      return false;
  }
  return false;
}

int32_t getNoBytesType(mlir::SchedTensorType t) {
  if (t.getElementType().isa<mlir::IntegerType>())
    return (t.getElementType()
                .dyn_cast<mlir::IntegerType>()
                .getIntOrFloatBitWidth() /
            8);
  else if (t.getElementType().isa<mlir::quant::QuantizedType>())
    return (t.getElementType()
                .dyn_cast<mlir::quant::QuantizedType>()
                .getStorageTypeIntegralWidth() /
            8);
}

int32_t getBitDepthType(Type t) {
  if (t.isa<mlir::RankedTensorType>()) {
    RankedTensorType v = t.dyn_cast<mlir::RankedTensorType>();
    if (v.getElementType().isa<mlir::IntegerType>())
      return v.getElementType()
          .dyn_cast<mlir::IntegerType>()
          .getIntOrFloatBitWidth();
    else if (v.getElementType().isa<mlir::quant::QuantizedType>())
      return v.getElementType()
          .dyn_cast<mlir::quant::QuantizedType>()
          .getStorageTypeIntegralWidth();
    else
      ASSERT_COND(true, "getBitDepthType::Unknown Element Type");
  } else if (t.isa<mlir::SchedTensorType>()) {
    SchedTensorType v = t.dyn_cast<mlir::SchedTensorType>();
    if (v.getElementType().isa<mlir::IntegerType>())
      return v.getElementType()
          .dyn_cast<mlir::IntegerType>()
          .getIntOrFloatBitWidth();
    else if (v.getElementType().isa<mlir::quant::QuantizedType>())
      return v.getElementType()
          .dyn_cast<mlir::quant::QuantizedType>()
          .getStorageTypeIntegralWidth();
    else
      ASSERT_COND(true, "getBitDepthType::Unknown Element Type");
  }
  ASSERT_COND(true, "getBitDepthType::Needs to be run on "
                    "mlir::RankedTensorType or mlir::SchedTensorType");
}

bool getSigned(Type t) {
  if (t.isa<mlir::RankedTensorType>()) {
    RankedTensorType v = t.dyn_cast<mlir::RankedTensorType>();
    if (v.getElementType().isa<mlir::IntegerType>())
      return (!v.getElementType()
                   .dyn_cast<mlir::IntegerType>()
                   .isUnsignedInteger());
    else if (v.getElementType().isa<mlir::quant::QuantizedType>())
      return v.getElementType()
          .dyn_cast<mlir::quant::QuantizedType>()
          .isSigned();
    else
      ASSERT_COND(true, "getSigned::Unknown Element Type");
  } else if (t.isa<mlir::SchedTensorType>()) {
    SchedTensorType v = t.dyn_cast<mlir::SchedTensorType>();
    if (v.getElementType().isa<mlir::IntegerType>())
      return (!v.getElementType()
                   .dyn_cast<mlir::IntegerType>()
                   .isUnsignedInteger());
    else if (v.getElementType().isa<mlir::quant::QuantizedType>())
      return v.getElementType()
          .dyn_cast<mlir::quant::QuantizedType>()
          .isSigned();
    else
      ASSERT_COND(true, "getSigned::Unknown Element Type");
  }
  ASSERT_COND(true, "getSigned::Needs to be run on mlir::RankedTensorType or "
                    "mlir::SchedTensorType");
}

// int32 and uint32 are ambigious.
//   ** Bias is detected as u32 but is really i32
//   ** Output of Conv is detected as u32 but is really i32
value_datatype getDataType(Type t) {
  bool Signed = getSigned(t);
  int32_t bitdepth = getBitDepthType(t);

  // std::cout << "Signed: " << Signed << " BitDepth: " << bitdepth <<
  // std::endl;

  if ((bitdepth == 8) and Signed)
    return int8;
  else if ((bitdepth == 8) and not(Signed))
    return uint8;
  else if ((bitdepth == 16) and Signed)
    return int16;
  else if ((bitdepth == 16) and not(Signed))
    return uint16;
  else
    return int32;
}

value_format getFormat(Type t) {
  ASSERT_COND(t.isa<mlir::SchedTensorType>(),
              "getFormat::Needs to be run on  mlir::SchedTensorType");
  SchedTensorType v = t.dyn_cast<mlir::SchedTensorType>();
  if (v)
    return t.dyn_cast<mlir::SchedTensorType>().get_format();
  else
    ASSERT_COND(true, "getFormat::Only works on SchedTensorType");
}

uint64_t storage_size_for_sub_purpose(
    SchedTensorType t, mlir::ArchitectureConfig::ArchitectureConfigAttr arch_c,
    value_sub_purpose vsp, signed param_a, signed param_b) {
  SmallVector<int64_t, 1> alt_shape =
      mlir::scheduleir::storage_shape_for_sub_purpose(t.getShape(), vsp,
                                                      param_a, param_b);

  // std::cout << t.getShape()[0] << t.getShape()[1] << t.getShape()[2] <<
  // t.getShape()[3] << std::endl; std::cout << alt_shape[0]    << alt_shape[1]
  // << alt_shape[2]    << alt_shape[3] << std::endl;

  float raw_size;
  if (vsp == vsp_DoubleBuffer) {
    int64_t elems = array_prod(alt_shape);
    if (elems == 0)
      return 0;
    float elem_no_bytes = getNoBytesType(t);
    float compression_scale_for_worst_weight_stream =
        t.get_compression_scale_for_worst_weight_stream();
    float weight_estimation_scaling = 1.0;
    raw_size =
        (elems * elem_no_bytes * compression_scale_for_worst_weight_stream *
         weight_estimation_scaling);

    // Maker sure we are and wil remained alligned
    while ((uint64_t(raw_size) % 16) != 0)
      raw_size++;
    while ((uint64_t(std::floor(raw_size / 2)) % 16) != 0)
      raw_size++;
    while ((uint64_t(std::ceil(raw_size / 2)) % 16) != 0)
      raw_size++;
  } else {
    // Rolling buffers are used for intermediate data in ifm streaming
    // These will all use the NHCWB16 format, and need to be aligned to 16 in
    // the C-dimension
    if (alt_shape[alt_shape.size() - 1] % 16 != 0)
      alt_shape[alt_shape.size() - 1] =
          round_up<int64_t>(alt_shape[alt_shape.size() - 1], 16);

    int64_t elems = array_prod(alt_shape);
    if (elems == 0)
      return 0;
    float elem_no_bytes = getNoBytesType(t);
    raw_size = (elems * elem_no_bytes);
  }

  uint64_t rounded_size =
      round_up<uint64_t>(uint64_t(ceil(raw_size)), t.get_alignment());
  return rounded_size;
}

float tensor_bandwidth(mlir::SchedTensorType t) {
  ArrayRef<int64_t> bandwidth_shape = t.getBandwidthShape();
  float elems = array_prod(bandwidth_shape);
  if (elems == 0.0)
    return 0.0;
  float element_size = getNoBytesType(t);
  return elems * element_size * t.get_bandwidth_compression_scale();
}

float tensor_size(mlir::SchedTensorType t, SmallVector<int64_t, 4> shape) {
  float elems = array_prod(shape);
  if (elems == 0.0)
    return 0.0;
  float element_size = getNoBytesType(t);
  return elems * element_size * t.get_bandwidth_compression_scale();
}

FilterMetaData getScheduleIRFilter(Operation *op) {
  int32_t w = 0;
  int32_t h = 0;
  int32_t sx = 1;
  int32_t sy = 1;
  int32_t dx = 1;
  int32_t dy = 1;
  int32_t top = 0;
  int32_t bottom = 0;
  int32_t left = 0;
  int32_t right = 0;
  if ((llvm::isa<scheduleir::Conv2DOp>(*op)) ||
      (llvm::isa<scheduleir::DepthwiseConv2DOp>(*op)) ||
      (llvm::isa<scheduleir::FullyConnectedOp>(*op))) {
    std::vector<int32_t> strides =
        ArrayAttr_to_Vector(op->getAttrOfType<ArrayAttr>("stride"));
    sx = strides[1];
    sy = strides[0];
    std::vector<int32_t> dilation =
        ArrayAttr_to_Vector(op->getAttrOfType<ArrayAttr>("dilation"));
    dx = dilation[1];
    dy = dilation[0];
    ;
    std::vector<int32_t> padding =
        ArrayAttr_to_Vector(op->getAttrOfType<ArrayAttr>("pad"));
    top = padding[0];
    bottom = padding[1];
    left = padding[2];
    right = padding[3];

    ArrayRef<int64_t> filter_shape;
    if (op->getOperand(1).getType().isa<mlir::RankedTensorType>()) {
      filter_shape =
          op->getOperand(1).getType().dyn_cast<RankedTensorType>().getShape();
      w = filter_shape[2];
      h = filter_shape[1];
    } else {
      mlir::SchedTensorType type =
          op->getOperand(1).getType().dyn_cast<SchedTensorType>();
      filter_shape = type.getShape();
      if (type.get_format() == value_format::vf_HWIO) {
        w = filter_shape[1];
        h = filter_shape[0];
      } else if (type.get_format() == value_format::vf_HWOI) {
        w = filter_shape[1];
        h = filter_shape[0];
      } else if ((type.get_format() == value_format::vf_WeightCompressed) and
                 (llvm::isa<scheduleir::DepthwiseConv2DOp>(*op))) {
        w = filter_shape[1];
        h = filter_shape[0]; // Assumed HWOI
      } else if ((type.get_format() == value_format::vf_WeightCompressed) and
                 (llvm::isa<scheduleir::Conv2DOp>(*op))) {
        w = filter_shape[1];
        h = filter_shape[0]; // Assumed HWIO
      } else
        FATAL_ERROR(
            "FilterMetaData getScheduleIRFilter unknown weight tensor format");
    }
  } else if ((llvm::isa<scheduleir::AvgPool2dOp>(*op)) ||
             (llvm::isa<scheduleir::MaxPool2dOp>(*op))) {
    std::vector<int32_t> kernel_size =
        ArrayAttr_to_Vector(op->getAttrOfType<ArrayAttr>("kernel"));
    w = kernel_size[1];
    h = kernel_size[0];
    std::vector<int32_t> strides =
        ArrayAttr_to_Vector(op->getAttrOfType<ArrayAttr>("stride"));
    sx = strides[1];
    sy = strides[0];
    std::vector<int32_t> padding =
        ArrayAttr_to_Vector(op->getAttrOfType<ArrayAttr>("pad"));
    top = padding[0];
    bottom = padding[1];
    left = padding[2];
    right = padding[3];
  }
  return FilterMetaData(w, h, sx, sy, dx, dy, top, bottom, left, right);
}

} // namespace utils
} // namespace mlir

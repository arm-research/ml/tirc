/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#include "json_writer.h"

namespace mlir {
namespace utils {

void ClearIfEmptyandClear(Json::Value &O) {
  if (O.size() == 0)
    O = Json::Value(Json::arrayValue);
}

std::string pointer_2_string(void const *p) {
  std::ostringstream address;
  address << p;
  return address.str();
}

Json::Value add_cascade(string name, std::vector<string> &kernels) {
  Json::Value Cascade;
  Json::Value Cascade_Kern;
  for (auto k : kernels)
    Cascade_Kern.append(k.c_str());
  ClearIfEmptyandClear(Cascade_Kern);
  Cascade["name"] = name.c_str();
  Cascade["kernels"] = Cascade_Kern;
  return Cascade;
}

Json::Value add_kernel(string name, std::vector<string> &preds,
                       std::vector<string> &succs, std::vector<string> &ops,
                       std::string cascade, std::string primary_op) {
  Json::Value Kernel;

  Json::Value Kernel_Pred;
  for (auto p : preds) {
    if (p == "0")
      continue;
    Kernel_Pred.append(p.c_str());
  }

  Json::Value Kernel_Succ;
  for (auto s : succs)
    Kernel_Succ.append(s.c_str());

  Json::Value Kernel_Ops;
  for (auto k : ops)
    Kernel_Ops.append(k.c_str());

  ClearIfEmptyandClear(Kernel_Pred);
  ClearIfEmptyandClear(Kernel_Succ);
  ClearIfEmptyandClear(Kernel_Ops);

  Kernel["predecessors"] = Kernel_Pred;
  Kernel["successors"] = Kernel_Succ;
  Kernel["ops"] = Kernel_Ops;

  Kernel["name"] = name.c_str();

  Kernel["is_element_wise"] = "False";
  Kernel["ip_unit"] = "Npu";
  Kernel["compute_unit"] = "Dpu";
  Kernel["compute_sub_unit"] = "NotDefined";
  Kernel["npu_block_type"] = "NotDefined";

  Kernel["primary_op"] = primary_op;
  Kernel["cascade"] = cascade.c_str();

  return Kernel;
}

Json::Value add_operation(string name, string type) {
  Json::Value Operation;
  Operation["name"] = name.c_str();
  Operation["type"] = type.c_str();
  return Operation;
}

Json::Value to_json(mlir::FuncOp function, std::string name) {
  Json::Value program_json;
  program_json["name"] = name.c_str();
  program_json["batch_size"] = 1;

  Region *region = function.getCallableRegion();
  llvm::iplist<Block> &blocks = region->getBlocks();
  if (blocks.size() > 1) {
    std::cout << "PlannerExtractNpuRegions:::More than a single block in main "
                 "function callable region"
              << std::endl;
    return 1;
  }
  Json::Value cascades;
  Json::Value Kernels;
  Json::Value Operations;

  map<Operation *, std::string> cache_cascade_yield;

  Block &block_func = region->front();

  llvm::iplist<Operation>::iterator i;
  for (i = block_func.begin(); i != block_func.end(); i++) {
    if (llvm::isa<scheduleir::NpuRegionOp>(i)) {
      Block &block_npureg = i->getRegion(0).front();
      llvm::iplist<Operation>::iterator c;
      for (c = block_npureg.begin(); c != block_npureg.end(); c++) {
        // Check all Operations in NpuRegionOp are Cascades
        if (not(llvm::isa<cascadeir::CascadeRegionOp>(c)))
          continue;

        // Pointer over Cascake
        Operation *op_c = &(*c);
        string name_c =
            pointer_2_string(op_c); // cascade_attr.Name().getValue().str();
        std::vector<string> cascade_kernels;
        Block &block_cascade = op_c->getRegion(0).front();

        std::string name_bb;
        std::string name_yield;
        {
          std::string primary_op;
          // Cascade producers to Kernel Argument
          std::vector<string> preds_bb;
          std::vector<string> succs_bb;
          std::vector<connection> producers_cascade = ExtractProducers(op_c);
          for (auto pc : producers_cascade) {
            if (pc.is_producer_block_or_operation() == operation_op) {
              if (llvm::isa<scheduleir::CastInOp>(pc.external_operation))
                continue;
              auto i = cache_cascade_yield.find(pc.external_operation);
              ASSERT_COND((i == cache_cascade_yield.end()),
                          "json_writer::Cascade External Operation not found");
              preds_bb.push_back(cache_cascade_yield[pc.external_operation]);
            }
          }
          // Add to this cascade the Input Kernel that will receive other Output
          // Kernels
          name_bb = name_c + std::string("BasicBlockArguments");
          // 0. Cascade Connections
          std::vector<string> kernel_ops_bb;
          primary_op = "BasicBlockArguments";
          Json::Value operation_bb =
              add_operation(name_bb, "BasicBlockArguments");
          Operations.append(operation_bb);
          kernel_ops_bb.push_back(name_bb);
          cascade_kernels.push_back(name_bb);
          Json::Value kernel_bb = add_kernel(name_bb, preds_bb, succs_bb,
                                             kernel_ops_bb, name_c, primary_op);
          Kernels.append(kernel_bb);
          std::vector<string> preds_empty;
          std::vector<string> succs_empty;
          // Add to this cascade Output Kernel that will connect to other Input
          // Kernels
          name_yield = name_c + std::string("Yield");
          std::vector<string> kernel_ops_y;
          primary_op = "schedulir.yield";
          Json::Value operation_y =
              add_operation(name_yield, "schedulir.yield");
          Operations.append(operation_y);
          kernel_ops_y.push_back(name_yield);
          cascade_kernels.push_back(name_yield);
          Json::Value kernel_y =
              add_kernel(name_yield, preds_empty, succs_empty, kernel_ops_y,
                         name_c, primary_op);
          Kernels.append(kernel_y);
        }
        cache_cascade_yield[op_c] = name_yield;

        // Iterate over kernels for this cascade
        llvm::iplist<Operation>::iterator k;
        for (k = block_cascade.begin(); k != block_cascade.end(); k++) {
          if (not(llvm::isa<kernelir::KernelRegionOp>(k)))
            continue;
          Operation *op_k = &(*k);
          cascade_kernels.push_back(pointer_2_string(op_k));
          string name_k = pointer_2_string(op_k);
          std::vector<string> preds_k;
          // 0. Connections across Kernels
          std::vector<connection> producers =
              ExtractProducers_ofType<kernelir::KernelRegionOp>(op_k);
          for (auto p : producers)
            preds_k.push_back(pointer_2_string(p.external_operation));
          std::vector<string> succs_k;
          std::vector<connection> consumers =
              ExtractConsumers_ofType<kernelir::KernelRegionOp>(op_k);
          for (auto c : consumers)
            succs_k.push_back(pointer_2_string(c.external_operation));
          // 1. Connection from BasicBlockArguments
          std::vector<connection> producers_bb = ExtractProducers(op_k);
          for (auto p : producers_bb) {
            if (p.is_producer_block_or_operation() == block_op) {
              preds_k.push_back(name_bb);
            }
          }
          // 2. Connections to Yield
          std::vector<connection> consumers_yield =
              ExtractConsumers_ofType<scheduleir::YieldOp>(op_k);
          for (auto c : consumers_yield)
            succs_k.push_back(name_yield);
          std::vector<string> kernel_ops;
          Block &block_kernel = op_k->getRegion(0).front();
          llvm::iplist<Operation>::iterator o;
          for (o = block_kernel.begin(); o != block_kernel.end(); o++) {
            Operation *op = &(*o);
            if (llvm::isa<scheduleir::YieldOp>(op))
              continue;
            kernel_ops.push_back(pointer_2_string(op));
            string name_o = pointer_2_string(op);
            string type_o = op->getName().getStringRef().str();
            // 3. Add Operators in kernels
            Json::Value operation = add_operation(name_o, type_o);
            Operations.append(operation);
          }
          std::string primary_op =
              pointer_2_string(mlir::kernelir::getKernelMajorOp(op_k));
          // 2. Add kernels in Cascade
          Json::Value kernel = add_kernel(name_k, preds_k, succs_k, kernel_ops,
                                          name_c, primary_op);
          Kernels.append(kernel);
        }
        Json::Value cascade = add_cascade(name_c, cascade_kernels);
        cascades.append(cascade);
      }
    }
  }

  ClearIfEmptyandClear(cascades);
  ClearIfEmptyandClear(Kernels);
  ClearIfEmptyandClear(Operations);

  Json::Value IR;
  Json::Value ir;
  ir["cascades"] = cascades;
  ir["kernels"] = Kernels;
  ir["operations"] = Operations;
  IR.append(ir);
  program_json["ir"] = IR;
  return program_json;
}

Json::Value to_json(mlir::ArchitectureConfig::ArchitectureConfigAttr &attr) {
  Json::Value arch_json;
  arch_json["accelerator_config"] =
      ConvertToString((AcceleratorConfig)attr.accel_config().getInt()).c_str();
  arch_json["n_npu_cores"] = 1;
  arch_json["npu_clock"] = 1;
  return arch_json;
}

void write_json(mlir::OwningModuleRef &module, std::string filename,
                std::string name,
                mlir::ArchitectureConfig::ArchitectureConfigAttr
                    &architecture_config_attr) {
  /* Extract Function Op */
  mlir::FuncOp function;
  Block *block = module.get().getBody();
  llvm::iplist<Operation> &operations = block->getOperations();
  if (operations.size() > 2)
    assert(true);
  for (auto &op : operations) {
    OperationName name = op.getName();
    if (llvm::isa<FuncOp>(&function)) {
      function = cast<FuncOp>(op);
      break;
    }
  }

  Json::Value program;

  program["program"] = to_json(function, name);
  program["architecture_features"] = to_json(architecture_config_attr);

  Json::StreamWriterBuilder builder;
  const std::string json_file = Json::writeString(builder, program);

  std::ofstream out1(filename.c_str());
  out1 << json_file;
  out1.close();
}

} // namespace utils
} // namespace mlir

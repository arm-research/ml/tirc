/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#ifndef NUMERIC_UTILS_H
#define NUMERIC_UTILS_H

#include <algorithm>
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>

using namespace std;

namespace mlir {
namespace utils {

template <typename T> inline T round_up(T a, T b) {
  double af = a;
  double bf = b;
  double rf = (floor((af + bf - 1.0) / bf) * bf);
  return (T)rf;
}

template <typename T> inline std::vector<T> round_up(T a, std::vector<T> b) {
  std::vector<T> result;
  for (int i = 0; i < b.size(); i++) {
    double af = a;
    double bf = b[i];
    double rf = (floor((af + bf - 1.0) / bf) * bf);
    result.push_back(rf);
  }
  return result;
}

template <typename T>
inline std::vector<T> round_up(std::vector<T> a, std::vector<T> b) {
  std::vector<T> result;
  for (int i = 0; i < b.size(); i++) {
    double af = a[i];
    double bf = b[i];
    double rf = (floor((af + bf - 1.0) / bf) * bf);
    result.push_back(rf);
  }
  return result;
}

template <typename T> inline T round_up_divide(T a, T b) {
  double af = a;
  double bf = b;
  double rf = floor((af + bf - 1.0) / bf);
  return rf;
}

template <typename T>
inline std::vector<T> round_up_divide(std::vector<T> a, T b) {
  std::vector<T> result;
  for (int i = 0; i < a.size(); i++) {
    double af = a[i];
    double bf = b;
    double rf = floor((af + bf - 1.0) / bf);
    result.push_back(rf);
  }
  return result;
  // return (a + b - 1) // b
}

template <typename T> inline std::vector<T> mul(T a, std::vector<T> b) {
  std::vector<T> result;
  for (int i = 0; i < b.size(); i++)
    result.push_back((a * b[i]));
  return result;
}

template <typename T>
inline std::vector<T> mul(std::vector<T> a, std::vector<T> b) {
  std::vector<T> result;
  for (int i = 0; i < b.size(); i++)
    result.push_back((a[i] * b[i]));
  return result;
}

} // namespace utils
} // namespace mlir

#endif // NUMERIC_UTILS_H

/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#include "src/utils/printIR.h"

namespace mlir {
namespace utils {

std::string getOpName(Operation &op) {
  return op.getName().getStringRef().str();
}

template <typename T> bool checkTensor_Type(Type t) {
  T v = t.dyn_cast<T>();
  if (!v)
    return false;
  return true;
}

void printTensor_Type(mlir::Type t, string indentation) {
  if (checkTensor_Type<mlir::RankedTensorType>(t)) {
    mlir::RankedTensorType tp = t.dyn_cast<mlir::RankedTensorType>();

    stringstream shape_ss;
    {
      ArrayRef<int64_t> shape = tp.getShape();
      if (shape.size() == 4)
        shape_ss << "[" << shape[0] << "," << shape[1] << "," << shape[2] << ","
                 << shape[3] << "]";
      else if (shape.size() == 1)
        shape_ss << "[" << shape[0] << "]";
      else
        ASSERT_COND(true, "printTensor:: Unknown tensor Size");
    }

    auto dataType = getDataType(t);
    std::cout << std::left << std::setfill(' ') << indentation << GREEN
              << "RankedTensorType" << std::endl;
    std::cout << std::left << std::setfill(' ') << indentation << BLUE
              << "Shape: " << RESET << std::setw(14) << shape_ss.str() << BLUE
              << " DataType: " << RESET << std::setw(10)
              << datatype_to_str(dataType) << std::endl;
  } else if (checkTensor_Type<mlir::SchedTensorType>(t)) {
    mlir::SchedTensorType tp = t.dyn_cast<mlir::SchedTensorType>();

    stringstream shape_ss;
    {
      ArrayRef<int64_t> shape = tp.getShape();
      if (shape.size() == 4)
        shape_ss << "[" << shape[0] << "," << shape[1] << "," << shape[2] << ","
                 << shape[3] << "]";
      else if (shape.size() == 1)
        shape_ss << "[" << shape[0] << "]";
      else
        ASSERT_COND(true, "printTensor:: Unknown tensor Size");
    }

    stringstream shape_storage_ss;
    {
      ArrayRef<int64_t> shape = tp.getStorageShape();
      if (shape.size() == 4)
        shape_storage_ss << "[" << shape[0] << "," << shape[1] << ","
                         << shape[2] << "," << shape[3] << "]";
      else if (shape.size() == 1)
        shape_storage_ss << "[" << shape[0] << "]";
      else
        shape_storage_ss << "[]";
    }

    int64_t tensor_address = tp.get_address();
    stringstream ss;
    if (tensor_address < 0)
      ss << "--";
    else
      ss << "0x" << std::hex << tensor_address << std::dec;

    auto dataType = getDataType(t);
    std::cout << std::left
              << std::setfill(' ')
              //<< indentation
              << GREEN << "SchedTensorType" << std::endl;
    std::cout << std::left
              << std::setfill(' ')
              //<< indentation
              << BLUE << "ID: " << RESET << std::setw(4) << tp.getUniqueId()
              << BLUE << "Shape: " << RESET << std::setw(14) << shape_ss.str()
              << BLUE << " DataType: " << RESET << std::setw(10)
              << datatype_to_str(dataType) << BLUE << " StorageShape: " << RESET
              << std::setw(14) << shape_storage_ss.str() << BLUE
              << " MemArea: " << RESET << std::setw(15)
              << mem_area_to_str(value_mem_area(tp.get_mem_area())) << BLUE
              << " MemType: " << RESET << std::setw(15)
              << mem_type_to_str(value_mem_type(tp.get_mem_type())) << BLUE
              << " Format: " << RESET << std::setw(5)
              << format_to_str(value_format(tp.get_format())) << BLUE
              << " Purpose: " << RESET << std::setw(10)
              << purpose_to_str(value_purpose(tp.get_purpose())) << BLUE
              << " SubPurpose: " << RESET << std::setw(9)
              << sub_purpose_to_str(value_sub_purpose(tp.get_sub_purpose()))
              << BLUE << " Address: " << RESET << std::setw(8) << ss.str()
              << BLUE << " Start: " << RESET << std::setw(4)
              << tp.get_start_time() << BLUE << " End: " << RESET
              << std::setw(4) << tp.get_end_time() << std::endl;
  } else
    ASSERT_COND(
        true,
        "Fatal Error in orint Tensor. Tensor not Ranked or SchedTensorType");
}

void printTensor(mlir::Value t, string indentation) {
  // t.dump();
  // std::cout << std::endl;
  printTensor_Type(t.getType(), indentation);
}

void printTensorUniqueId(mlir::Value v, string indentation) {
  auto t = v.getType();
  if (checkTensor_Type<mlir::SchedTensorType>(t)) {
    mlir::SchedTensorType tp = t.dyn_cast<mlir::SchedTensorType>();

    stringstream shape_ss;
    {
      ArrayRef<int64_t> shape = tp.getShape();
      if (shape.size() == 4)
        shape_ss << "[" << shape[0] << "," << shape[1] << "," << shape[2] << ","
                 << shape[3] << "]";
      else if (shape.size() == 1)
        shape_ss << "[" << shape[0] << "]";
      else
        ASSERT_COND(true, "printTensor:: Unknown tensor Size");
    }

    stringstream shape_storage_ss;
    {
      ArrayRef<int64_t> shape = tp.getStorageShape();
      if (shape.size() == 4)
        shape_storage_ss << "[" << shape[0] << "," << shape[1] << ","
                         << shape[2] << "," << shape[3] << "]";
      else if (shape.size() == 1)
        shape_storage_ss << "[" << shape[0] << "]";
      else
        shape_storage_ss << "[]";
    }

    int64_t tensor_address = tp.get_address();
    stringstream ss;
    if (tensor_address < 0)
      ss << "--";
    else
      ss << "0x" << std::hex << tensor_address << std::dec;

    std::cout << indentation << GREEN << "SchedTensorType " << BLUE
              << "ID: " << RESET << std::setw(4) << tp.getUniqueId() << BLUE
              << " Shape: " << RESET << std::setw(14) << shape_ss.str() << BLUE
              << " StorageShape: " << RESET << std::setw(14)
              << shape_storage_ss.str() << BLUE << " Address: " << RESET
              << std::setw(8) << ss.str() << BLUE << " Memory Op Io: " << RESET
              << std::setw(1) << tp.get_io_memory_op() << BLUE
              << " DataType: " << RESET << std::setw(1) << getDataType(tp)
              << std::endl;
  } else if (checkTensor_Type<mlir::RankedTensorType>(t)) {
    mlir::RankedTensorType tp = t.dyn_cast<mlir::RankedTensorType>();

    stringstream shape_ss;
    {
      ArrayRef<int64_t> shape = tp.getShape();
      if (shape.size() == 4)
        shape_ss << "[" << shape[0] << "," << shape[1] << "," << shape[2] << ","
                 << shape[3] << "]";
      else if (shape.size() == 2)
        shape_ss << "[" << shape[0] << "," << shape[1] << "]";
      else if (shape.size() == 1)
        shape_ss << "[" << shape[0] << "]";
      else {
        std::cout << shape.size() << std::endl;
        ASSERT_COND(true, "printTensor:: Unknown tensor Size");
      }
    }

    auto dataType = getDataType(t);
    std::cout << indentation << GREEN << "RankedTensorType " << BLUE
              << "Shape: " << RESET << std::setw(14) << shape_ss.str() << BLUE
              << " DataType: " << RESET << std::setw(10) << getDataType(tp)
              << std::endl;
  }
}

void printOp(Operation &op, unsigned level, info_option option) {
  std::vector<std::string> indentation_options = {
      "\t", "\t\t", "\t\t\t", "\t\t\t\t", "\t\t\t\t\t", "\t\t\t\t\t\t"};
  std::string indentation = indentation_options[level];

  if (option == op_tensors) {
    std::cout << indentation << RED << level
              << "::" << op.getName().getStringRef().str() << RESET
              << std::endl;
    std::string op_dialect = op.getDialect()->getNamespace().str();
    if (llvm::isa<cascadeir::CascadeRegionOp>(op)) {
      std::cout << YELLOW << indentation << "Inputs:" << RESET << std::endl;
      SmallVector<Value, 1> inputs =
          mlir::cascadeir::ExtractCascadeInputValues(&op);
      for (auto input : inputs)
        printTensorUniqueId(input, indentation);

      std::cout << YELLOW << indentation << "Outputs:" << RESET << std::endl;
      SmallVector<Value, 1> outputs =
          mlir::cascadeir::ExtractCascadeOutputValues(&op);
      for (auto output : outputs)
        printTensorUniqueId(output, indentation);
    }
    if (llvm::isa<kernelir::KernelRegionOp>(op)) {
      std::cout << indentation << "Inputs:" << std::endl;
      SmallVector<Value, 1> inputs =
          mlir::kernelir::ExtractKernelInputValues(&op);
      for (auto input : inputs)
        printTensorUniqueId(input, indentation);

      std::cout << indentation << "Outputs:" << std::endl;
      SmallVector<Value, 1> outputs =
          mlir::kernelir::ExtractKernelOutputValues(&op);
      for (auto output : outputs)
        printTensorUniqueId(output, indentation);
    }
    if ((op_dialect == "tosa") or (op_dialect == "scheduleir") or
        (op_dialect == "tfl") or llvm::isa<scheduleir::DmaOp>(op) or
        llvm::isa<scheduleir::MemoryOp>(op) or
        llvm::isa<scheduleir::SliceViewOp>(op)) {
      std::cout << indentation << "Inputs:" << std::endl;
      for (auto input : op.getOperands())
        printTensorUniqueId(input, indentation);
      std::cout << indentation << "Outputs:" << std::endl;
      for (auto output : op.getResults())
        printTensorUniqueId(output, indentation);
    }
  } else if (option == kernel_tensors) {
    if (level >= 3)
      return;

    std::cout << indentation << level
              << "::" << op.getName().getStringRef().str() << std::endl;
    if (llvm::isa<kernelir::KernelRegionOp>(op)) {
      std::cout << indentation << "\t"
                << "Inputs:" << std::endl;
      SmallVector<Value, 1> inputs =
          mlir::kernelir::ExtractKernelInputValues(&op);
      for (auto input : inputs)
        printTensor(input, indentation + std::string("\t"));

      std::cout << indentation << "\t"
                << "Outputs:" << std::endl;
      SmallVector<Value, 1> outputs =
          mlir::kernelir::ExtractKernelOutputValues(&op);
      for (auto output : outputs)
        printTensor(output, indentation + std::string("\t"));

      std::cout << indentation << "\t"
                << "Weights (2nd Storage):" << std::endl;
      SmallVector<Value, 1> weights =
          mlir::kernelir::ExtractKernelWeightValues(&op);
      for (auto weight : weights)
        printTensor(weight, indentation + std::string("\t"));

      std::cout << indentation << "\t"
                << "Bias (2nd Storage):" << std::endl;
      SmallVector<Value, 1> biases =
          mlir::kernelir::ExtractKernelBiasValues(&op);
      for (auto bias : biases)
        printTensor(bias, indentation + std::string("\t"));

      std::cout << indentation << "\t"
                << "Intermediates:" << std::endl;
      SmallVector<Value, 1> intermediates =
          mlir::kernelir::ExtractKernelIntermediateValues(&op);
      for (auto intermediate : intermediates)
        printTensor(intermediate, indentation + std::string("\t"));
    }
  } else if (option == cascade_tensors) {
    if (level >= 3)
      return;

    std::cout << indentation << level
              << "::" << op.getName().getStringRef().str() << std::endl;
    if (llvm::isa<cascadeir::CascadeRegionOp>(op)) {
      std::cout << YELLOW << indentation << "\t"
                << "Inputs:" << RESET << std::endl;
      SmallVector<Value, 1> inputs =
          mlir::cascadeir::ExtractCascadeInputValues(&op);
      for (auto input : inputs)
        printTensor(input, indentation + std::string("\t"));

      std::cout << YELLOW << indentation << "\t"
                << "Outputs:" << RESET << std::endl;
      SmallVector<Value, 1> outputs =
          mlir::cascadeir::ExtractCascadeOutputValues(&op);
      for (auto output : outputs)
        printTensor(output, indentation + std::string("\t"));

      std::cout << YELLOW << indentation << "\t"
                << "Weights (2nd Storage):" << RESET << std::endl;
      SmallVector<Value, 1> weights =
          mlir::cascadeir::ExtractCascadeWeightValues(&op);
      for (auto weight : weights)
        printTensor(weight, indentation + std::string("\t"));

      std::cout << YELLOW << indentation << "\t"
                << "Bias (2nd Storage):" << RESET << std::endl;
      SmallVector<Value, 1> biases =
          mlir::cascadeir::ExtractCascadeBiasValues(&op);
      for (auto bias : biases)
        printTensor(bias, indentation + std::string("\t"));

      std::cout << YELLOW << indentation << "\t"
                << "Intermediates:" << RESET << std::endl;
      SmallVector<Value, 1> intermediates =
          mlir::cascadeir::ExtractCascadeIntermediateValues(&op);
      for (auto intermediate : intermediates)
        printTensor(intermediate, indentation + std::string("\t"));
    }
  } else if (option == op_detailed_info) {
    if (llvm::isa<hlcs::KernelInfoOp>(op)) {
      std::string kernel_name =
          op.getAttrOfType<mlir::StringAttr>("kernel_name").getValue().str();
      uint32_t primary_op =
          op.getAttrOfType<mlir::IntegerAttr>("kernel_primary_op").getInt();
      std::cout << indentation << level
                << "::" << op.getName().getStringRef().str() << "::" << &op
                << " Kernel Name: " << kernel_name
                << " Primary Op: " << primary_op << std::endl;
    } else if (llvm::isa<hlcs::OperatorInfoOp>(op)) {
      std::string operator_name =
          op.getAttrOfType<mlir::StringAttr>("operator_name").getValue().str();
      std::string operator_type =
          op.getAttrOfType<mlir::StringAttr>("operator_type").getValue().str();
      std::cout << indentation << level
                << "::" << op.getName().getStringRef().str() << "::" << &op
                << " Operator Name: " << operator_name
                << " Operator Type: " << operator_type << std::endl;
    } else if (llvm::isa<hlcs::TensorInfoOp>(op)) {
      std::string tensor_label =
          op.getAttrOfType<mlir::StringAttr>("label").getValue().str();
      std::string tensor_dtype =
          op.getAttrOfType<mlir::StringAttr>("dtype").getValue().str();
      uint32_t tensor_mem_area =
          op.getAttrOfType<mlir::IntegerAttr>("mem_area").getInt();
      uint32_t tensor_mem_type =
          op.getAttrOfType<mlir::IntegerAttr>("mem_type").getInt();
      uint32_t tensor_format =
          op.getAttrOfType<mlir::IntegerAttr>("format").getInt();
      uint32_t tensor_purpose =
          op.getAttrOfType<mlir::IntegerAttr>("purpose").getInt();
      uint32_t tensor_sub_purpose =
          op.getAttrOfType<mlir::IntegerAttr>("sub_purpose").getInt();

      stringstream shape_ss;
      {
        ArrayAttr sh = op.getAttrOfType<mlir::ArrayAttr>("shape");
        std::vector<int32_t> shape = ArrayAttr_to_Vector(sh);
        if (shape.size() == 4)
          shape_ss << "[" << shape[0] << "," << shape[1] << "," << shape[2]
                   << "," << shape[3] << "]";
        else if (shape.size() == 1)
          shape_ss << "[" << shape[0] << "]";
        else
          ASSERT_COND(true, "printTensor:: Unknown tensor Size");
      }

      int64_t tensor_address =
          op.getAttrOfType<mlir::IntegerAttr>("address").getInt();
      stringstream ss;
      if (tensor_address < 0)
        ss << "--";
      else
        ss << "0x" << std::hex << tensor_address << std::dec;

      std::cout << std::left << std::setfill(' ') << indentation << level
                << "::TensorInfo::" << &op << BLUE << " Tensor Label " << RESET
                << std::setw(7) << tensor_label << BLUE
                << " Mem Area: " << RESET << std::setw(12)
                << mem_area_to_str(value_mem_area(tensor_mem_area)) << BLUE
                << " Mem Type: " << RESET << std::setw(12)
                << mem_type_to_str(value_mem_type(tensor_mem_type)) << BLUE
                << " Shape: " << RESET << std::setw(12) << shape_ss.str()
                << BLUE << " Format: " << RESET << std::setw(6)
                << format_to_str(value_format(tensor_format)) << BLUE
                << " DataType: " << RESET << std::setw(8) << tensor_dtype
                << BLUE << " Purpose: " << RESET << std::setw(10)
                << purpose_to_str(value_purpose(tensor_purpose)) << BLUE
                << " SubPurpose: " << RESET << std::setw(10)
                << sub_purpose_to_str(value_sub_purpose(tensor_sub_purpose))
                << BLUE << " Address: " << RESET << std::setw(12) << ss.str();
    } else if (llvm::isa<hlcs::CommandListOp>(op)) {
      std::string plan_type =
          op.getAttrOfType<mlir::StringAttr>("plan_type").getValue().str();
      std::cout << indentation << level
                << "::" << op.getName().getStringRef().str() << "::" << &op
                << " Plan_type: " << plan_type << std::endl;
    } else if (llvm::isa<hlcs::StripeMemCmd>(op)) {
      std::string tensor_label = op.getOperand(0)
                                     .getDefiningOp()
                                     ->getAttrOfType<mlir::StringAttr>("label")
                                     .getValue()
                                     .str();

      ArrayAttr start_coord = op.getAttrOfType<ArrayAttr>("start_coord");
      stringstream start;
      if (start_coord.size() == 4) {
        start << "[";
        start << start_coord[0].dyn_cast<IntegerAttr>().getInt() << ",";
        start << start_coord[1].dyn_cast<IntegerAttr>().getInt() << ",";
        start << start_coord[2].dyn_cast<IntegerAttr>().getInt() << ",";
        start << start_coord[3].dyn_cast<IntegerAttr>().getInt();
        start << "]";
      } else if (start_coord.size() == 1) {
        start << "[";
        start << start_coord[0].dyn_cast<IntegerAttr>().getInt();
        start << "]";
      }

      ArrayAttr end_coord = op.getAttrOfType<ArrayAttr>("end_coord");
      stringstream end;
      if (end_coord.size() == 4) {
        end << "[";
        end << end_coord[0].dyn_cast<IntegerAttr>().getInt() << ",";
        end << end_coord[1].dyn_cast<IntegerAttr>().getInt() << ",";
        end << end_coord[2].dyn_cast<IntegerAttr>().getInt() << ",";
        end << end_coord[3].dyn_cast<IntegerAttr>().getInt();
        end << "]";
      } else if (end_coord.size() == 1) {
        end << "[";
        end << end_coord[0].dyn_cast<IntegerAttr>().getInt();
        end << "]";
      }

      std::cout << indentation << level << "::StripeMemCmd::" << &op
                << " Tensor Label " << tensor_label
                << " Start Coordinate: " << start.str()
                << " End Coordinate: " << end.str() << std::endl;
    } else if (llvm::isa<hlcs::DmaCmd>(op)) {
      std::cout << indentation << level
                << "::" << op.getName().getStringRef().str() << "::" << &op
                << std::endl;
    } else if (llvm::isa<hlcs::NpuStripeCmd>(op)) {
      std::cout << indentation << level
                << "::" << op.getName().getStringRef().str() << "::" << &op
                << std::endl;
    } else
      std::cout << indentation << level
                << "::" << op.getName().getStringRef().str() << "::" << &op
                << std::endl;
  } else if (option == connections) {
    if (llvm::isa<kernelir::KernelRegionOp>(op)) {
      Operation *op_major = kernelir::getKernelMajorOp(&op);
      std::cout << indentation << YELLOW << level
                << "::" << op.getName().getStringRef().str() << "::" << &op
                << "::" << op_major->getName().getStringRef().str() << RESET
                << std::endl;
    } else
      std::cout << indentation << GREEN << level
                << "::" << op.getName().getStringRef().str() << RESET
                << std::endl;
    if (op.getNumRegions() == 1) {
      Region &region_instruction = op.getRegion(0);
      if (region_instruction.getBlocks().size() == 1) {
        Block &block = region_instruction.front();
        std::cout << indentation
                  << "    No of Operations: " << block.getOperations().size()
                  << std::endl;
      }
    }

    std::vector<connection> producers = ExtractProducers(&op);
    std::cout << indentation << "    No of producers: " << producers.size()
              << std::endl;
    for (auto &producer : producers) {
      Operation *op = producer.operation;
      Operation *external = producer.external_operation;
      if (producer.external_operation == nullptr)
        std::cout << indentation
                  << "    Basic Block Inputs:" << producer.external_slot_index
                  << "->" << op << std::endl;
      else
        std::cout << indentation << "    Operation Inputs:" << external << "->"
                  << op << std::endl;
    }

    std::vector<connection> consumers = ExtractConsumers(&op);
    std::cout << indentation << "    No of consumers: " << consumers.size()
              << std::endl;
    for (auto &consumer : consumers) {
      Operation *op = consumer.operation;
      Operation *external = consumer.external_operation;
      std::cout << indentation << "    Outputs:" << op << "->" << external
                << std::endl;
    }
  } else if (option == tensor_implementations) {
    std::cout << indentation << GREEN << level
              << "::" << op.getName().getStringRef().str() << RESET
              << std::endl;

    std::cout << indentation << "    Inputs: " << std::endl;
    for (mlir::Value v : op.getOperands()) {
      if (v.getType().dyn_cast<mlir::SchedTensorType>())
        std::cout << indentation << indentation << "SchedTensorType"
                  << std::endl;
      else
        std::cout << indentation << indentation << "RankedTensorType"
                  << std::endl;
    }
    std::cout << indentation << "    Outputs: " << std::endl;
    for (mlir::Value v : op.getResults()) {
      if (v.getType().dyn_cast<mlir::SchedTensorType>())
        std::cout << indentation << indentation << "SchedTensorType"
                  << std::endl;
      else
        std::cout << indentation << indentation << "RankedTensorType"
                  << std::endl;
    }
  } else {
    std::cout << indentation << level
              << "::" << op.getName().getStringRef().str() << "::" << &op
              << std::endl;
    // auto buf_idx = op.getAttrOfType<IntegerAttr>("buf_idx");
    // if (buf_idx)
    //    std::cout << indentation << buf_idx.getInt() << std::endl;
  }
}

void printNextlevelOp(Operation &op, unsigned level, info_option option,
                      unsigned exit_level) {
  if (level > exit_level)
    return;
  if (op.getNumRegions() > 0) {
    Region &region = op.getRegion(0);
    if (region.getBlocks().size() > 0) {
      Block &block = region.front();
      llvm::iplist<Operation> &operations_level_1 = block.getOperations();
      for (auto &op_1 : operations_level_1) {
        printOp(op_1, level, option);
        printNextlevelOp(op_1, level + 1, option, exit_level);
      }
    }
  }
}

void printIR(Block &block, info_option option, unsigned exit_level) {
  std::cout << "printIR::Start " << std::endl;
  llvm::iplist<Operation> &operations = block.getOperations();
  for (auto &op : operations) {
    printOp(op, 0, option);
    printNextlevelOp(op, 1, option, exit_level);
  }
  std::cout << "printIR::End " << std::endl;
}

void PrintLiveness(Liveness &liveness) {
  std::cout << "Op Liveness::Start " << std::endl;
  // liveness.print(std::cout);
  liveness.dump();
#if 0
        Operation *op = liveness.getOperation();
        if (op.getNumRegions() > 0)
        {
            Region &region = op.getRegion(0);
            if (region.getBlocks().size() > 0)
            {
                Block  &block  = region.front();

                auto &allInValues = liveness.getLiveIn(block);
                auto &allOutValues = liveness.getLiveOut(block);

                auto allOperationsInWhichValueIsLive = liveness.resolveLiveness(value);

                bool lastUse = liveness.isLastUse(value, operation);
            }
        }
#endif
  std::cout << "Op Liveness::Start " << std::endl;
}

} // namespace utils
} // namespace mlir

/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#ifndef PRINT_IR_H
#define PRINT_IR_H

#include "absl/memory/memory.h"
#include "mlir/Dialect/Quant/QuantOps.h"
#include "mlir/Dialect/Quant/QuantTypes.h"
#include "mlir/Dialect/StandardOps/IR/Ops.h"      // from @llvm-project
#include "mlir/IR/AffineMap.h"                    // from @llvm-project
#include "mlir/IR/Attributes.h"                   // from @llvm-project
#include "mlir/IR/BlockAndValueMapping.h"         // from @llvm-project
#include "mlir/IR/Builders.h"                     // from @llvm-project
#include "mlir/IR/Location.h"                     // from @llvm-project
#include "mlir/IR/MLIRContext.h"                  // from @llvm-project
#include "mlir/IR/Operation.h"                    // from @llvm-project
#include "mlir/IR/PatternMatch.h"                 // from @llvm-project
#include "mlir/Interfaces/InferTypeOpInterface.h" // from @llvm-project
#include "mlir/Pass/Pass.h"                       // from @llvm-project
#include "mlir/Transforms/DialectConversion.h"    // from @llvm-project

#include "src/utils/dag.h"

#include "src/transforms/kernel_packing/kernel_packing.h"
#include "src/transforms/scheduler/plan.h"
#include "src/transforms/scheduler/scheduler.h"

#include "src/utils/assert.h"
#include "src/utils/checkIR.h"
#include "src/utils/colors.h"
#include "src/utils/ir_utils.h"
#include "src/utils/logging.h"

#include "mlir/Analysis/Liveness.h"

#include "src/ir/cascade_ir.h"
#include "src/ir/hlcs_ir.h"
#include "src/ir/kernel_ir.h"
#include "src/ir/schedule_ir.h"

#include "src/ir/architecture_config/architecture_config.h"

#include <iomanip>

using namespace std;
using namespace mlir::planner;

namespace mlir {
namespace utils {
enum info_option {
  off,
  op_detailed_info,
  connections,
  tensor_implementations,
  op_tensors,
  kernel_tensors,
  cascade_tensors,
};

std::string getOpName(Operation &op);

template <typename T> bool checkTensor_Type(Type t);

void printTensor_Type(mlir::Type t, string indentation);
void printTensor(mlir::Value t, string indentation);
void printOp(Operation &op, unsigned level = 0, info_option option = off);
void printNextlevelOp(Operation &op, unsigned level, info_option option,
                      unsigned exit_level);
void printIR(Block &block, info_option option = off, unsigned exit_level = 100);
void PrintLiveness(Liveness &liveness);
} // namespace utils
} // namespace mlir
#endif // PRINT_IR_H

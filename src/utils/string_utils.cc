/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#include "src/utils/string_utils.h"
namespace mlir {

namespace utils {

void split_string(std::vector<std::string> &result, const std::string &data,
                  const char deliminator) {
  std::stringstream ss;
  ss << data;
  std::string entry;
  while (std::getline(ss, entry, deliminator)) {
    result.push_back(entry);
  }
}

void split_and_do(const std::string &data, char deliminator,
                  std::function<void(const std::string &)> &&exec) {
  std::stringstream ss(data);
  std::string token;
  while (std::getline(ss, token, deliminator)) {
    exec(token);
  }
}

std::string remove(const std::string &data, const char *chars) {
  std::string str(data);
  std::string del(chars);
  auto need_to_del = [&del](const char &c) {
    return del.find(c) != std::string::npos;
  };
  str.erase(remove_if(str.begin(), str.end(), need_to_del), str.end());
  return str;
}

std::vector<int32_t> str_to_ivec(const std::string &data) {
  std::vector<int32_t> ivec;
  string str = remove(data, "()[]\' ");
  split_and_do(str, ',',
               [&](const std::string &key) { ivec.push_back(std::stoi(key)); });
  return std::move(ivec);
}

std::vector<std::vector<int32_t>> str_to_ivecvec(const std::string &data) {
  std::vector<std::vector<int32_t>> ivecvec;
  if (data.empty() || "None" == data || "[]" == data)
    return std::move(ivecvec);
  size_t size = data.size();
  string str = data.substr(1, size - 2);
  split_and_do(str, ']', [&](const std::string &key) {
    if (key.empty())
      return;
    string clean_key = key;
    if (key[0] == ',')
      clean_key = key.substr(2);
    else if (key[0] == '[')
      clean_key = key.substr(1);
    ivecvec.push_back(str_to_ivec(clean_key));
  });
  return std::move(ivecvec);
}

void split_string(std::vector<int> &result, const std::string &data,
                  const char deliminator) {
  std::vector<std::string> result_string;
  split_string(result_string, data, deliminator);
  for (auto r : result_string)
    result.push_back(std::stoi(r));
}

} // namespace utils
} // namespace mlir

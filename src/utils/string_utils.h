/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#ifndef STRING_UTILS_H
#define STRING_UTILS_H

#include <bits/stdc++.h>
#include <fstream>
#include <functional>
#include <iostream>
#include <vector>

using namespace std;

namespace mlir {
namespace utils {
void split_string(std::vector<std::string> &result, const std::string &data,
                  const char deliminator);
void split_and_do(const std::string &data, char deliminator,
                  std::function<void(const std::string &)> &&exec);

std::string remove(const std::string &data, const char *chars);

std::vector<int32_t> str_to_ivec(const std::string &data);
std::vector<std::vector<int32_t>> str_to_ivecvec(const std::string &data);
void split_string(std::vector<int> &result, const std::string &data,
                  const char deliminator);
} // namespace utils
} // namespace mlir
#endif // STRING_UTILS

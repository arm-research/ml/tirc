/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#include "src/utils/threading.h"

namespace mlir {
namespace utils {

void workload_distribution(int32_t no_work_items,
                           std::vector<int32_t> &thread_start,
                           std::vector<int32_t> &thread_end,
                           int32_t no_threads) {
  float stride_v = (no_work_items / no_threads);
  if (stride_v < 1.0) {
    int32_t no_threads_local = no_threads;
    do {
      no_threads_local--;
      if (no_threads_local == 0)
        no_threads_local = 1;
      stride_v = (no_work_items / no_threads_local);
    } while (stride_v < 1.0);
  }
  int stride = stride_v;

  int32_t start = 0;
  int32_t end = stride;
  thread_start.push_back(start);
  thread_end.push_back(end);
  do {
    start = end;
    end = end + stride;
    if ((no_work_items - end) < stride)
      end = no_work_items;
    thread_start.push_back(start);
    thread_end.push_back(end);
  } while (end < no_work_items);

  /*
  std::cout << "No Plans: " << no_work_items << " No of threads: " <<
  thread_end.size() << std::endl; for (int i = 0; i < thread_start.size(); i++)
      std::cout << thread_start[i] << " " << thread_end[i] << std::endl;
  */
}

} // namespace utils
} // namespace mlir

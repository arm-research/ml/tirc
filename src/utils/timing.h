/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#ifndef TIMING_H
#define TIMING_H

#include <bits/stdc++.h>
#include <fstream>
#include <functional>
#include <iostream>
#include <vector>

using namespace std;

namespace mlir {
namespace utils {

#define START_TIMING                                                           \
  {                                                                            \
    std::chrono::time_point<std::chrono::high_resolution_clock> start;         \
    if (ENABLE_TIMING) {                                                       \
      start = std::chrono::high_resolution_clock::now();                       \
    }

#define END_TIMING(label)                                                      \
  if (ENABLE_TIMING) {                                                         \
    auto finish = std::chrono::high_resolution_clock::now();                   \
    std::chrono::duration<double> elapsed = finish - start;                    \
    std::cout << YELLOW << "Elapsed Time " << #label << ": "                   \
              << elapsed.count() << "/s" << RESET << std::endl;                \
  }                                                                            \
  }

#define END_TIMING_GREEN(label)                                                \
  if (ENABLE_TIMING) {                                                         \
    auto finish = std::chrono::high_resolution_clock::now();                   \
    std::chrono::duration<double> elapsed = finish - start;                    \
    std::cout << GREEN << "Elapsed Time " << #label << ": " << elapsed.count() \
              << "/s" << RESET << std::endl;                                   \
  }                                                                            \
  }

} // namespace utils
} // namespace mlir

#endif // TIMING_H

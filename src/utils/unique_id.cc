/*
  Copyright (C) 2021 Arm Limited or its affiliates. All rights reserved.

  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the License); you may
  not use this file except in compliance with the License.
  You may obtain a copy of the License at

  www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an AS IS BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#ifndef UNIQUE_ID_H
#define UNIQUE_ID_H

#include "src/utils/unique_id.h"

#include <cstdint>
#include <stdint.h>

using namespace std;

namespace mlir {
namespace utils {

uint64_t id_counter = 1;

uint64_t getUniqueId() { return id_counter++; }

} // namespace utils
} // namespace mlir
#endif // UNIQUE_ID
